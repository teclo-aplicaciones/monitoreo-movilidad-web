angular.module(appTeclo).constant("constante", {
	urlWs: "/monimovi-api_v100r1/"
});
angular.module(appTeclo).factory('config',['$http','$location','constante','$rootScope', function ($http,$location,constante,$rootScope) {

	const PROTOCOL = $location.protocol()+ "://";
	const HOST = location.host;
	const URL = `${PROTOCOL}${HOST}${constante.urlWs}`;
	//const URL = `http://172.18.44.239:9080${constante.urlWs}`;
	const ABSURL = $location.absUrl();
	const CONTEXAPP = ABSURL.split("/")[3];

	return {
		baseUrl:URL,
		absUrl:ABSURL,
		contextApp:CONTEXAPP
	}
}]);