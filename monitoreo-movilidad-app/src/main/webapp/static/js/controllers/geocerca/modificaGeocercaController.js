angular.module(appTeclo).controller("modificaGeocercaController", function($scope, $route, $translate, $window, $rootScope, CatalogoService, MonitoreoService, GeocodingService, GeocercaService, geocercaVO, showAlert) {
	$scope.busquedaVO = {};
	
	$scope.detalleVO = [];
	
	$scope.canEdit = $route.current.params.id ? true : false;
	
	$scope.altaGeocercaVO={
		notificacion:{}
	};
	
	$scope.detalleView={
		rowsPerPage:$scope.detalleVO.length,
		searchSomething:[]
	};
	
	$scope.isConsulta = true;
	
	$scope.listaCatalogo = {
		lista:[]
	}
	
	$scope.gVO = geocercaVO ? [angular.copy(geocercaVO.data)] : null;
	
	$scope.tipoGeocerca = {};
	$scope.tipoBusqueda = {
			lista:[]
	}
	
	$scope.toggleTipo = false;
	
	$scope.geocercas = [];
	
	//CONSULTA
	
	requiredFields = function(form){
		
		angular.forEach(form.$error, function (field) {
            	angular.forEach(field, function(errorField){
            	errorField.$setDirty();
            })
                     
            //$scope.showAviso("Es necesario completar el formulario");
		});
	}
	
	$scope.initC = function(){
		if(GeocercaService.consultaVO.length >0){
			$scope.geocercas = $scope.geocercas != undefined ? GeocercaService.consultaVO : undefined;
		}
	}
	
	$scope.modoBusqueda = function(){
		
		if($scope.busquedaVO.tipoBusca.id != 1){
			$scope.toggleTipo = true;
			GeocercaService.catalogoGeocerca($scope.busquedaVO.tipoBusca.id).success(function(data){
				$scope.tipoBusqueda.lista = data;
				//$scope.tipoBusqueda.valor = $scope.tipoBusqueda.lista[0];
				$scope.tipoBusqueda.valor = {id:-1, nombre:"Seleccione"};
				$('#select2-comboTipoBusquedaGeocerca-container').text($scope.tipoBusqueda.valor.nombre);
				$('#select2-comboTipoBusquedaGeocerca-container').attr('title',$scope.tipoBusqueda.valor.nombre);
			}).error(function(err){
				console.log("error");
			});
			
		}else{
			$scope.toggleTipo = false;
		}
		$scope.geocercas = [];
	}
	
	$scope.buscarCatalogo = function(){
		CatalogoService.catalogoBusqGeocerca().success(function(data){
			$scope.listaCatalogo.lista = data;
			$scope.busquedaVO.tipoBusca = $scope.listaCatalogo.lista[0];
			$('#select2-comboBusquedaGeocerca-container').text($scope.busquedaVO.tipoBusca.nombre);
		}).error(function(err){
			console.log("error");
		});
	}
	
	$scope.consultar = function(tipo, valor){
		if($scope.toggleTipo){
			if(valor)
				GeocercaService.consultaGeocercas(tipo, valor).success(function(data){
					GeocercaService.consultaVO = data;
					$scope.geocercas = GeocercaService.consultaVO;
				}).error(function(err){
					showAlert.error(err.message);
				});
		}else{
			if($scope.formModificaGeocerca.$invalid){
				requiredFields($scope.formModificaGeocerca);
			}else{
				GeocercaService.consultaGeocercas(tipo, valor).success(function(data){
					GeocercaService.consultaVO = data;
					$scope.geocercas = GeocercaService.consultaVO;
				}).error(function(err){
					showAlert.error(err.message);
				});
			}
		}

	}
	
	$scope.cambiarEstadoGeocerca = function (geocerca){
		GeocercaService.cambiarEstadoGeocerca(geocerca.id).success(function(data){
			if(data)
				if(geocerca.activo == 0){
					showAlert.aviso("El registro se ha deshabilitado.");
				}else{
					showAlert.aviso("El registro se ha habilitado.");
				}
		}).error(function(err){
			
		});
	}
	
	$scope.abrirModalDetalle=function(id){
		//Consultar y despues desplegar el modal
		GeocercaService.buscarDetalleGeocerca(id).success(function(data){
			$scope.detalleVO = data;
			$scope.showModalDetalleGeocerca = true;
		}).error(function(err){
			
		})
		
	}
	
	$scope.confirmaElimPerma = function(geo){
		showAlert.confirmacion("¿Estas seguro que desea eliminar '"+geo.nombre+"'?", function(){
			GeocercaService.eliminaGeocercaPerma(geo.id).success(function(data){
				if(data){
					showAlert.aviso("La geocerca '"+geo.nombre+"' se elimino con exito.");
					$scope.geocercas = $scope.geocercas.filter(function(el){
						return el.id != geo.id;
					})
				}
			}).error(function(err){
				showAlert.error(err);
			})
			},null, null, null);
	}
	
	$scope.$on('$locationChangeSuccess', function(event, current, previous) {
		var page = current.split('/')[5];
		if(page !== "modificaAreaGeocerca" && page !== "modificaGeocerca")
			GeocercaService.consultaVO = []
    });
	
	$scope.initC();
	
	//MODIFICACION
	
	requiredFieldsodifica = function(){
		
	    
		angular.forEach($scope.formModificaGeocerca.$error, function (field) {
            	angular.forEach(field, function(errorField){
            	errorField.$setDirty();
            })
		});
	}
	
	$scope.altaGeocercaVO = {
			nombre: "",
			descripcion:"",
			activo:1,
	}
	
	$scope.mapaPuntos = [];

	$scope.listaOpcioNoti = [];

	$scope.listaTipoEnvio={};
	
	$scope.listaDispositivos={
			list:[]
	};
	
	$scope.listaGrupos = {};
	
	$scope.bandera={
			trigger:false
	};
	$scope.bandera2={
			trigger:false
	};
	$scope.showModalAsignacion = false;
	$scope.showModalPuntos = false;
	$scope.toggleDispositivos = false;
	
	$scope.prevToggle=undefined;
	
	$scope.googleVO = {
			busqueda:""
	}
	
	$scope.toggleDireccion = false;
	
	$scope.listaPuntos={
			list:[{items:[1]}]
	};
	
	  /******************************/
	 /** Configuracion de idiomas **/
	/******************************/
	
	$scope.traducir = function(callback){
		$translate(['GEOCERCAS.Consulta.tituloServicio', 
					'GEOCERCAS.Alta.modalDispositivo.label', 
					'GEOCERCAS.Alta.modalDispositivo.labelDisp', 
					'GEOCERCAS.Alta.modalDispositivo.labelAsig',
					'GEOCERCAS.Alta.puntos.labelPnt']).then(function (headline) {
		    $scope.translate ={
		    		pagina: headline['GEOCERCAS.Consulta.tituloServicio'],
		    		titulo: headline['GEOCERCAS.Alta.modalDispositivo.label'],
		    		dispo: headline['GEOCERCAS.Alta.modalDispositivo.labelDisp'],
		    		actua: headline['GEOCERCAS.Alta.modalDispositivo.labelAsig'],
		    		punto: headline['GEOCERCAS.Alta.puntos.labelPnt']
		    }
		    if(callback)
		    	callback()
		}, function (translationId) {
		    $scope.headline = translationId;
		});
	}
	
	$scope.init = function(){
		CatalogoService.buscaCatTipoNotiGeocerca().success(function(data){
			$scope.listaTipoEnvio.lista = data;

			CatalogoService.catalogoTipoGeocerca().success(function(data){
			$scope.tipoGeocerca.lista = data;
			
			//Valida que geocercaVO tenga el valor de la geocerca que se quiere modificar, en el caso que no haya, significa 
			// que se esta consultando de manera global
			if(geocercaVO){
				$scope.altaGeocercaVO.nombre = $scope.gVO[0].nombre;
				$scope.altaGeocercaVO.descripcion = $scope.gVO[0].descripcion;
				$scope.altaGeocercaVO.tipo = $scope.gVO[0].tipo;
				$scope.altaGeocercaVO.grupo = $scope.gVO[0].grupo;
				$scope.tipoGeocerca.valor = $scope.tipoGeocerca.lista.find(function(el){
					if (el.id == $scope.altaGeocercaVO.tipo)
						return el;
				});
				$("#select2-comboTipoGeocerca-container").text($scope.tipoGeocerca.valor.nombre);
				$scope.altaGeocercaVO.notificacion = {};
				$scope.listaOpcioNoti = $scope.gVO[0].notificacion.opciones;
				$scope.listaTipoEnvio.valor = $scope.listaTipoEnvio.lista.find(function(el){
					return el.id == $scope.gVO[0].notificacion.tipoEnvio;
				});
				$("#select2-comboTipoEnvio-container").text($scope.listaTipoEnvio.valor.nombre);
				$scope.shapeLine = $scope.tipoGeocerca.valor.id;
				$scope.altaGeocercaVO.color = $scope.gVO[0].color;
				$scope.customColor = $scope.gVO[0].color;
				$scope.altaGeocercaVO.id = $scope.gVO[0].id;
				$scope.geocercaVO = [];
				$scope.geocercaVO.push({coords:[...$scope.gVO[0].coords], tipo:$scope.altaGeocercaVO.tipo, color: $scope.altaGeocercaVO.color});
				if($scope.altaGeocercaVO.grupo != 0){
					$scope.toggleDispositivos = true;
					$scope.prevToggle = !$scope.toggleDispositivos;
				}
				$scope.abrirModalDispositivo(false);

				//Zoom a la geocerca
				$scope.focusOnMap($scope.geocercaVO[0].coords[0][0], $scope.geocercaVO[0].coords[0][1]);
			}
			
		}).error(function(err){
			
		});
		}).error(function(err){

		});
	}
	
	
	$scope.cambiaTipoGeocerca=function(tipo){
		$scope.shapeLine = $scope.tipoGeocerca.valor.id;
		$scope.altaGeocercaVO.tipo = $scope.tipoGeocerca.valor.id;
		
		//Limpiamos los puntos autogenerados
		$scope.asignarPuntoMapaGeocerca({});
	};
	
	$scope.regresar = function(){
		$window.history.back();
	};
	
	$scope.buscarDispositivoEnGrupo=function(id, callback){
		
		$scope.bandera.trigger=true;
		
		$scope.listaDispositivos.list = [];
		
		$scope.bandera.trigger=true;
		
		GeocercaService.buscarDispositivoEnGrupoParaGeocerca(id).success(function(data){
			if(data.length > 0){
				var tempLista = {
						titulo:$scope.translate.dispo,
						items:[]
				};
				
				tempLista.items = data.map(function(el, idx){
					return {
						idItem:el.id,
						srItem:el.serial,
						txtItem:el.nombre
					};
				});
				
				$scope.bandera.trigger=true;
				
				$scope.listaDispositivos.list.push({
					titulo: $scope.translate.dispo,
					items: tempLista.items
				});
				
				$scope.listaDispositivos.list.push({
					titulo: $scope.translate.actua,
					items:[]
				});
				
				if(callback)
					callback(true)
			}else{
				showAlert.error("No existen dispositivos asociados a este grupo");
			}
		}).error(function(err){
			
		});
	}
	
	$scope.callbackDispositivos = function(allowDevice){
		//Ajustar lista de dispositivos que estan seleccionados
		if($scope.gVO[0].dispositivos.length > 0){
			if($scope.altaGeocercaVO.grupo != 0 && $scope.toggleDispositivos && allowDevice == undefined){
				//$scope.toggleDispositivos = false;
				$scope.listaGrupos.valor = $scope.listaGrupos.lista.find(function(el){ return el.id == $scope.gVO[0].grupo});
				$scope.buscarDispositivoEnGrupo($scope.listaGrupos.valor.id, function(vr){$scope.callbackDispositivos(vr)});
				$('#select2-comboBusquedaFiltroGrupos-container').text($scope.listaGrupos.valor.nombre);
				//$scope.listaDispositivos.list.push();
			}
			if(allowDevice || $scope.toggleDispositivos == false){
				$scope.bandera.trigger = true;
				$scope.listaDispositivos.list[1].items = $scope.listaDispositivos.list[0].items.filter(
						function (el){
							var found = $scope.gVO[0].dispositivos.find(function(elm){
								return el.idItem == elm.idItem;
							}); 
							if(found != undefined)
								$scope.listaDispositivos.list[0].items = $scope.listaDispositivos.list[0].items.filter(
										function (el){
											return el.idItem != found.idItem; 
								});
						return found ? found : null;  
				});
				$scope.bandera.trigger = true;
				
			}
		}
	}
	
	$scope.abrirModalDispositivo=function(force){
		
		$rootScope.$evalAsync(function() {
			$scope.showModalAsignacion = force != undefined ? force:true;
		});
		
		if($scope.prevToggle != $scope.toggleDispositivos)
		{
			$scope.bandera.trigger=true;
			$scope.listaDispositivos.list = [];
			$scope.prevToggle = $scope.toggleDispositivos;
			
			$scope.bandera.trigger=true;
			
			if(!$scope.toggleDispositivos){
				//MonitoreoService.buscarAgrupamiento(3).success(function(data){
				GeocercaService.buscarGeocercaDispositivos().success(function(data){
					var tempLista = {
						titulo:$scope.translate.dispo,
						items:[]
					};
					
					tempLista.items = data.map(function(el, idx){
						return {
							idItem:el.id,
							srItem:el.serial,
							txtItem:el.nombre
						};
					});
						

					$scope.listaDispositivos.list.push({
						titulo: $scope.translate.dispo,
						items: tempLista.items
					});
																																																																																																																																																																																																																																																										
					$scope.listaDispositivos.list.push({
						titulo: $scope.translate.actua,
						items:[]
					});
					
					if(force != undefined)
						$scope.callbackDispositivos();
				}).error(function(err){
					console.log("error");
				});
			}else{
				MonitoreoService.buscarAgrupamiento(1).success(function(data){
					$scope.listaGrupos.lista = data;
					$scope.listaGrupos.lista.unshift({id:0, nombre:"Seleccione"});
					$scope.listaGrupos.valor = $scope.listaGrupos.lista[0];
					
					if(force != undefined)
						$scope.callbackDispositivos();
				}).error(function(err){
					console.log("error");
				});
			}
		}
	}
	
	$scope.abrirModalPuntos = function(){
		if($scope.listaPuntos.list.length >0){
			$scope.showModalPuntos = true;
		}else{
			growl.error('No se ha marcado en el mapa los limites de la geocerca.');
		}
	};
	
	$scope.asignarPuntoMapaGeocerca = function(mapa){
		
		//Limpiamos los puntos
		//$scope.listaPuntos.list[0].items =[];
		$scope.listaPuntos.list =[];
		var lista = [];
		var x;
		var lockList = false;
		var circle = [];
		if(Object.keys(mapa).length !== 0){
			mapa.map(function(el, idx){
				if($scope.tipoGeocerca.valor.id==1){
					if(mapa.length>0){
						el.coords.map(function(crd, ix){
							 if(lockList){
								 x = {lat:circle[1],lng:circle[0],dist:crd[1]}
								 lista.push(x);
								 lockList = false;
							 }else{
								circle = [crd[0], crd[1]];
								lockList = true;
							 }
						 })
					}
				}else{
					if(mapa.length >0){
						 el.coords.map(function(crd, ix){
							x = {lat:crd[1],lng:crd[0]}
							lista.push(x);
						 });
					 }
				}
			});
			$scope.listaPuntos.list = [...lista];
				 /*if($scope.tipoGeocerca.valor.id==1){
					 if(mapa.length >0){
						 el.coords.map(function(crd, ix){
							 if(lockList){
								 x = {idItem:idx,txtItem:'lat:'+circle[1]+', lng:'+circle[0]+', Distancia:'+crd[1]}
								 lista.push(x);
								 lockList = false;
							 }else{
								circle = [crd[0], crd[1]];
								lockList = true;
							 }
						 })
					 }
				 }else{
					 if(mapa.length >0){
						 el.coords.map(function(crd, ix){
							x = {idItem:ix,txtItem:'lat:'+crd[1]+', lng:'+crd[0]}
							lista.push(x);
						 });
					 }
				 }
				
			});
			$scope.bandera2.trigger = true;
			$scope.listaPuntos.list[0].items = [...lista];*/
				
		}else{
			$scope.bandera2.trigger = true;
			//$scope.listaPuntos.list[0].items.push({idItem:1,txtItem:'Vacio'});
			$scope.listaPuntos.list = [];
		}
	};
	
	$scope.guardaGeocerca = function(){
		$scope.wkt = $scope.obtenerWKT();
		if($scope.formAltaGeocerca.$invalid){
			showAlert.requiredFields($scope.formAltaGeocerca);
		}else{
			//Acoplamos el tipo al VO
			$scope.altaGeocercaVO.tipo = $scope.tipoGeocerca.valor.id;
			//Acoplamos el color al VO
			$scope.altaGeocercaVO.color = $scope.customColor;
			//Acoplamos el id de Grupo al VO
			$scope.altaGeocercaVO.grupo = $scope.listaGrupos.valor != undefined ? $scope.listaGrupos.valor.id : 0;
			
			//if($scope.listaDispositivos.list[1] != undefined && $scope.listaDispositivos.list[1].items.length >0){
				//Acoplamos los dispositivos al VO
				$scope.altaGeocercaVO.dispositivos = $scope.listaDispositivos.list[1] ? [...$scope.listaDispositivos.list[1].items] : null;
				if($scope.geocercaVO[0] != undefined){
					//Acoplamos las coordenadas al VO
					$scope.altaGeocercaVO.coords = [...$scope.geocercaVO[0].coords];
					$scope.altaGeocercaVO.wkt = $scope.wkt ? $scope.wkt[0] : "";
					//Acoplamos la config de notificaciones
					$scope.altaGeocercaVO.notificacion.tipoEnvio = $scope.listaTipoEnvio.valor.id;
					$scope.altaGeocercaVO.notificacion.opciones = $scope.listaOpcioNoti;
					if($scope.altaGeocercaVO.wkt.length <4000){
						GeocercaService.actualizaGeocerca($scope.altaGeocercaVO).success(function(data){
							showAlert.aviso("Geocerca actualizada correctamente.");
						}).error(function(err){
							showAlert.error("Hubo un error al actualizar la geocerca.");
							console.log(err);
						})
					}else{
						showAlert.error("La forma geometrica es muy compleja porque tiene muchos trazos, por favor traze una nueva forma geometrica");
					}
				}else{
					//Mensaje de error: cree alguna geocerca
					showAlert.error("No hay trazos en el mapa.");
				}
			/*}else{
				//Mensaje de error: seleccione un dispositivo/grupo
				showAlert.error("No hay dispositivo(s) seleccionado(s)");
			}*/
		}
	}
	
	$scope.buscaDireccion = function(direccion){
		if(direccion.trim() !== ""){
			GeocodingService.getLatLngFromAddress(direccion, function(res){
				$scope.toggleDireccion = false;
				var googleApiResponse = res;
				console.log(googleApiResponse);
				var coords = [[googleApiResponse[0].geometry.location.lng(),googleApiResponse[0].geometry.location.lat()] ];
				$scope.focusOnMap(coords[0][0], coords[0][1]);
				$scope.googleVO.busqueda = "";
			});
		}else{
			showAlert.error("No puede estar vacio el campo");
		}
		
	};
	
	$scope.$watch('geocercaVO', function(newEl, oldEl){
		if(newEl){
			$scope.asignarPuntoMapaGeocerca(newEl);
		}
		//console.log(newEl);
	});
	
	$scope.$watch("$parent.currentLanguage", function(newValue, oldValue) {
		$scope.traducir(null);
	});
	
	  /********************/
	 /** Inicializacion **/
	/********************/
	$scope.traducir($scope.init);
	$scope.buscarCatalogo();
});