angular.module(appTeclo).controller("bajaGeocercaController", function($scope, CatalogoService, GeocercaService, showAlert) {
	$scope.busquedaVO = {};
	
	$scope.listaCatalogo = {
		lista:[]
	}
	
	$scope.tipoGeocerca = [{id:1, nombre:"Circular"},
		 {id:2, nombre:"Polylineal"},
		 {id:3, nombre:"Polygonal"}];
	
	$scope.catalogosId = [3, 4];
	
	requiredFields = function(form){
		
	    
		angular.forEach(form.$error, function (field) {
            	angular.forEach(field, function(errorField){
            	errorField.$setDirty();
            })
                     
            //$scope.showAviso("Es necesario completar el formulario");
		});
	}
	
	function ejemplo(){
		showAlert.aviso("Se elimino.");
	}
	
	
	$scope.confirmacion = function(){
		showAlert.confirmacion("¿Estas seguro de eliminar geocerca?",ejemplo);
	}
	
	$scope.buscarCatalogo = function(){
		CatalogoService.buscarCatalogosPorId($scope.catalogosId).success(function(data){
			$scope.listaCatalogo.lista = data;
			$scope.busquedaVO.tipoBusca = $scope.listaCatalogo.lista[0];
			$('#select2-comboBusquedaGeocerca-container').text($scope.busquedaVO.tipoBusca.nombre);
		}).error(function(err){
			console.log("error");
		});
	}
	
	$scope.consultar = function(tipo, valor){
		if($scope.formBajaGeocerca.$invalid){
			requiredFields($scope.formBajaGeocerca);
		}else{
			GeocercaService.consultaGeocercas(tipo, valor).success(function(data){
				$scope.geocercas = data;
			}).error(function(err){
				alert("error")
			});
		}
	}
	
	$scope.buscarCatalogo();
});