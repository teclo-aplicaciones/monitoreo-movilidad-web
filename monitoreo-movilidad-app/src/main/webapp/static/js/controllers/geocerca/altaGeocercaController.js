angular.module(appTeclo).controller("altaGeocercaController", function($scope, $window,$translate, $rootScope, MonitoreoService, GeocercaService, GeocodingService, CatalogoService, showAlert, growl) {
	
	  /***************/
	 /** Variables **/
	/***************/
	
	$scope.altaGeocercaVO = {
			nombre: "",
			descripcion:"",
			activo:1,
			notificacion:{}
	}

	$scope.listaOpcioNoti = [];
	
	$scope.geocercaVO = [];
	
	$scope.isConsulta = false;
	
	$scope.canEdit = false;
	$scope.tipoGeocerca={}
	
	$scope.mapaPuntos = [];
	
	$scope.listaDispositivos={
			list:[]
	};

	$scope.listaTipoEnvio={};
	
	$scope.listaGrupos = {};
	
	$scope.listaPuntos={
			list:[{items:[]}]
	};
	
	$scope.inputs = {
			busquedaDireccion:false,
			botonBusquedaDireccion:false
	}
	
	$scope.bandera={};
	$scope.bandera2={};
	$scope.prevToggle=undefined;
	$scope.showModalAsignacion = false;
	$scope.showModalPuntos = false;
	
	$scope.customColor = "#F44336";
	$scope.shapeLine = 1;
	
	$scope.toggleDispositivos = false;
	
	$scope.googleVO = {
			busqueda:""
	}
	
	$scope.isSearching = false;
	
	$scope.toggleDireccion = false;
	
	  /******************************/
	 /** Configuracion de idiomas **/
	/******************************/
	
	$scope.traducir = function(callback){
		$translate(['GEOCERCAS.Alta.tituloServicio', 
					'GEOCERCAS.Alta.modalDispositivo.label', 
					'GEOCERCAS.Alta.modalDispositivo.labelDisp', 
					'GEOCERCAS.Alta.modalDispositivo.labelAsig',
					'GEOCERCAS.Alta.puntos.labelPnt']).then(function (headline) {
		    $scope.translate ={
		    		pagina: headline['GEOCERCAS.Alta.tituloServicio'],
		    		titulo: headline['GEOCERCAS.Alta.modalDispositivo.label'],
		    		dispo: headline['GEOCERCAS.Alta.modalDispositivo.labelDisp'],
		    		actua: headline['GEOCERCAS.Alta.modalDispositivo.labelAsig'],
		    		punto: headline['GEOCERCAS.Alta.puntos.labelPnt']
		    }
		    if(callback)
		    	callback()
		}, function (translationId) {
		    $scope.headline = translationId;
		});
	}
	
	  /***************/
	 /** Funciones **/
	/***************/
	
	$scope.init = function(){
		CatalogoService.catalogoTipoGeocerca().success(function(data){
			$scope.tipoGeocerca.lista = data;
			//OfflineService.fetchForOffline($window.location.hash.split("#/")[1],data, "cat1");
			$scope.tipoGeocerca.valor = $scope.tipoGeocerca.lista[0];
			$("#select2-comboTipoGeocerca-container").text($scope.tipoGeocerca.valor.nombre);
		}).error(function(err){
			//Modo fuera de linea prueba
			/*OfflineService.getFromOffline($window.location.hash.split("#/")[1], "cat1").then(function(data){
				$scope.tipoGeocerca.lista = data; 
				$scope.tipoGeocerca.valor = $scope.tipoGeocerca.lista[0];
				$("#select2-comboTipoGeocerca-container").text($scope.tipoGeocerca.valor.nombre);
			});*/
		});
		CatalogoService.buscaCatTipoNotiGeocerca().success(function(data){
			$scope.listaTipoEnvio.lista = data;
			//OfflineService.fetchForOffline($window.location.hash.split("#/")[1],data, "cat2");
			$scope.listaTipoEnvio.valor = $scope.listaTipoEnvio.lista[0];
			$("#select2-comboTipoEnvio-container").text($scope.listaTipoEnvio.valor.nombre);
		}).error(function(err){
			//Modo fuera de linea prueba
			/*OfflineService.getFromOffline($window.location.hash.split("#/")[1], "cat2").then(function(data){
				$scope.listaTipoEnvio.lista = data; 
				$scope.listaTipoEnvio.valor = $scope.listaTipoEnvio.lista[0];
				$("#select2-comboTipoEnvio-container").text($scope.listaTipoEnvio.valor.nombre);
			});*/
		});

		CatalogoService.buscaCatConfigNotiGeocerca().success(function(data){
			$scope.listaOpcioNoti = data;
			//OfflineService.fetchForOffline($window.location.hash.split("#/")[1],data, "cat3");
		}).error(function(err){
			//Modo fuera de linea prueba
			/*OfflineService.getFromOffline($window.location.hash.split("#/")[1], "cat3").then(function(data){
				$scope.listaOpcioNoti = data; 
			});*/
		});

	}
	
	$scope.asignaColor = function(colorNum){
		switch(colorNum){
			case 1: $scope.customColor = "#F44336";break;
			case 2: $scope.customColor = "#4CAF50";break;
			case 3: $scope.customColor = "#2196F3";break;
			case 4: $scope.customColor = "#FFEB3B";break;
			case 5: $scope.customColor = "#00BCD4";break;
			case 6: $scope.customColor = "#E91E63";break;
			case 7: $scope.customColor = "#9E9E9E";break;
			default: $scope.customColor = "#F44336";
		}
		$scope.altaGeocercaVO.color=$scope.customColor;
	};
	
	$scope.abrirModalPuntos = function(){
		if($scope.listaPuntos.list.length >0){
			$scope.showModalPuntos = true;
		}else{
			growl.error('No se ha marcado en el mapa los limites de la geocerca.');
		}
	};

	$scope.abrirModalDispositivo=function(force){
		$rootScope.$evalAsync(function() {
			$scope.showModalAsignacion = force != undefined ? force:true;
		});
		
		if($scope.prevToggle != $scope.toggleDispositivos)
		{
			$scope.bandera.trigger=true;
			$scope.listaDispositivos.list = [];
			$scope.prevToggle = $scope.toggleDispositivos;
			
			if(!$scope.toggleDispositivos){
				//MonitoreoService.buscarAgrupamiento(3).success(function(data){
				GeocercaService.buscarGeocercaDispositivos().success(function(data){
					$scope.bandera.flag=true;
					var tempLista = {
						titulo:$scope.translate.dispo,
						items:[]
					};
					
					tempLista.items = data.map(function(el, idx){
						return {
							idItem:el.id,
							srItem:el.serial,
							txtItem:el.nombre
						};
					});
						

					$scope.listaDispositivos.list.push({
						titulo: $scope.translate.dispo,
						items: tempLista.items
					});
																																																																																																																																																																																																																																																										
					$scope.listaDispositivos.list.push({
						titulo: $scope.translate.actua,
						items:[]
					});
				}).error(function(err){
					console.log("error");
				});
			}else{
				MonitoreoService.buscarAgrupamiento(1).success(function(data){
					$scope.listaGrupos.lista = data;
					/*$scope.listaGrupos.lista.unshift({id:0, nombre:"Seleccionala"});
					$scope.listaGrupos.valor = $scope.listaGrupos.lista[0];*/
				}).error(function(err){
					console.log("error");
				});
			}
		}
	}
	
	$scope.cambiaTipoGeocerca=function(tipo){
		$scope.shapeLine = $scope.tipoGeocerca.valor.id;
		$scope.altaGeocercaVO.tipo = $scope.tipoGeocerca.valor.id;
		
		//Limpiamos los puntos autogenerados
		$scope.asignarPuntoMapaGeocerca({});
	};
	
	$scope.buscarDispositivoEnGrupo=function(id){
		
		$scope.bandera.trigger=true;
		
		$scope.listaDispositivos.list = [];
		
		if(id != undefined){
			GeocercaService.buscarDispositivoEnGrupoParaGeocerca(id).success(function(data){
				if(data.length > 0){
					var tempLista = {
							titulo:$scope.translate.dispo,
							items:[]
					};
					
					tempLista.items = data.map(function(el, idx){
						return {
							idItem:el.id,
							srItem:el.serial,
							txtItem:el.nombre
						};
					});
					
					$scope.bandera.trigger=true;
					
					$scope.listaDispositivos.list.push({
						titulo: $scope.translate.dispo,
						items: tempLista.items
					});
					
					$scope.listaDispositivos.list.push({
						titulo: $scope.translate.actua,
						items:[]
					});
				}else{
					showAlert.error("No existen dispositivos asociados a este grupo");
				}
				
			}).error(function(err){
				
			});
			$scope.bandera.trigger=true;
		}
	}
	
	$scope.asignarPuntoMapaGeocerca = function(mapa){
		
		//Limpiamos los puntos
		//$scope.listaPuntos.list[0].items =[];
		$scope.listaPuntos.list =[];
		var lista = [];
		var x;
		var lockList = false;
		var circle = [];
		if(Object.keys(mapa).length !== 0){
			mapa.map(function(el, idx){
				if($scope.tipoGeocerca.valor.id==1){
					if(mapa.length>0){
						el.coords.map(function(crd, ix){
							 if(lockList){
								 x = {lat:circle[1],lng:circle[0],dist:crd[1]}
								 lista.push(x);
								 lockList = false;
							 }else{
								circle = [crd[0], crd[1]];
								lockList = true;
							 }
						 })
					}
				}else{
					if(mapa.length >0){
						 el.coords.map(function(crd, ix){
							x = {lat:crd[1],lng:crd[0]}
							lista.push(x);
						 });
					 }
				}
			});
			$scope.bandera2.trigger = true;
			$scope.listaPuntos.list = [...lista];
			/*mapa.map(function(el, idx){
				
				 if($scope.tipoGeocerca.valor.id==1){
					 if(mapa.length >0){
						 el.coords.map(function(crd, ix){
							 if(lockList){
								 x = {idItem:idx,txtItem:'lat:'+circle[1]+', lng:'+circle[0]+', Distancia:'+crd[1]}
								 lista.push(x);
								 lockList = false;
							 }else{
								circle = [crd[0], crd[1]];
								lockList = true;
							 }
						 })
					 }
				 }else{
					 if(mapa.length >0){
						 el.coords.map(function(crd, ix){
							x = {idItem:ix,txtItem:'lat:'+crd[1]+', lng:'+crd[0]}
							lista.push(x);
						 });
					 }
					
				 }
				
			});
			$scope.listaPuntos.list[0].items = [...lista];
			*/			
		}else{
			$scope.bandera2.trigger = true;
			//$scope.listaPuntos.list[0].items.push({idItem:1,txtItem:'Vacio'});
			$scope.listaPuntos.list = [];
		}
	};
	
	$scope.guardaGeocerca = function(){
		$scope.wkt = $scope.obtenerWKT();
		if($scope.formAltaGeocerca.$invalid){
			showAlert.requiredFields($scope.formAltaGeocerca);
		}else{
			//Id va vacio, se autoasigna en Back
			$scope.altaGeocercaVO.id = -1;
			//Acoplamos el tipo al VO
			$scope.altaGeocercaVO.tipo = $scope.tipoGeocerca.valor.id;
			//Acoplamos el color al VO
			$scope.altaGeocercaVO.color = $scope.customColor;
			//Acoplamos el id de Grupo al VO
			$scope.altaGeocercaVO.grupo = $scope.listaGrupos.valor != undefined ? $scope.listaGrupos.valor.id : 0;
			
			//if($scope.listaDispositivos.list[1] != undefined && $scope.listaDispositivos.list[1].items.length >0){
				//Acoplamos los dispositivos al VO
				$scope.altaGeocercaVO.dispositivos = $scope.listaDispositivos.list[1] ? [...$scope.listaDispositivos.list[1].items] : null;
				if($scope.geocercaVO[0] != undefined){
					//Acoplamos las coordenadas al VO
					$scope.altaGeocercaVO.coords = [...$scope.geocercaVO[0].coords];
					$scope.altaGeocercaVO.wkt = $scope.wkt ? $scope.wkt[0] : "";
					//Acoplamos la config de notificaciones
					$scope.altaGeocercaVO.notificacion.tipoEnvio = $scope.listaTipoEnvio.valor.id;
					$scope.altaGeocercaVO.notificacion.opciones = $scope.listaOpcioNoti;
					//if($scope.altaGeocercaVO.wkt.length <4000){
						GeocercaService.guardarGeocerca($scope.altaGeocercaVO).success(function(data){
							//Limpiar
							$scope.formAltaGeocerca.$setPristine();
							// reset altaGeocercaVO
							$scope.altaGeocercaVO = {
									nombre: "",
									descripcion:"",
									activo:1,
									notificacion:{}
							}
							$scope.customColor ="#F44336";
							$scope.tipoGeocerca.valor = $scope.tipoGeocerca.lista[0];
							$("#select2-comboTipoGeocerca-container").text($scope.tipoGeocerca.valor.nombre);
							$scope.listaGrupos.valor = $scope.listaGrupos.lista != undefined ? $scope.listaGrupos.lista[0] : undefined;
							$scope.prevToggle = undefined;
							$scope.listaDispositivos={list:[]};
							$scope.geocercaVO = [];
							showAlert.aviso("Geocerca creada correctamente.");
						}).error(function(err){
							showAlert.error("Hubo un error al crear la geocerca.");
							console.log(err);
						})
					/*}else{
						showAlert.error("La forma geometrica es muy compleja porque tiene muchos trazos, por favor traze una nueva forma geometrica");
					}*/
				}else{
					//Mensaje de error: cree alguna geocerca
					showAlert.error("No hay trazos en el mapa.");
				}
			/*}else{
				//Mensaje de error: seleccione un dispositivo/grupo
				showAlert.error("No hay dispositivo(s) seleccionado(s)");
			}*/
		}
	};
	
	$scope.buscaDireccionFocus = function(event){
		if(event.keyCode == 13)
			$scope.buscaDireccion($scope.googleVO.busqueda)
	}
	
	$scope.buscaDireccion = function(direccion){
		if(direccion.trim() !== ""){
			$scope.isSearching = true;
			$scope.inputs.busquedaDireccion = true;
			$scope.inputs.botonBusquedaDireccion = true;
			$scope.googleVO.busqueda = "";
			GeocodingService.getLatLngFromAddress(direccion, function(result, status){
				$rootScope.$evalAsync(function() {
					$scope.toggleDireccion = false;
					$scope.isSearching = false;
					$scope.inputs.busquedaDireccion = false;
					$scope.inputs.botonBusquedaDireccion = false;
				});
				var googleApiResponse = result;
				if(status !== 'ZERO_RESULTS'){
					var coords = [[googleApiResponse[0].geometry.location.lng(),googleApiResponse[0].geometry.location.lat()] ];
					$scope.focusOnMap(coords[0][0], coords[0][1]);
				}else{
					growl.error('No se encontro la ubicación.');
				}
			});
		}else{
			showAlert.error("No puede estar vacio el campo");
		}
	};
	
	  /*************************/
	 /** Oyentes / Listeners **/
	/*************************/
	
	$scope.$watch("$parent.currentLanguage", function(newValue, oldValue) {
		$scope.traducir(null);
	});
	
	$scope.$watch('geocercaVO', function(newEl, oldEl){
		if(newEl){
			$scope.asignarPuntoMapaGeocerca(newEl);
		}
		//console.log(newEl);
	});
	
	  /********************/
	 /** Inicializacion **/
	/********************/
	$scope.traducir($scope.init);
});