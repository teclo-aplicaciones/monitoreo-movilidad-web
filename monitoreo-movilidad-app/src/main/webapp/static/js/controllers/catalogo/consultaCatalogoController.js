angular.module(appTeclo).controller("consultaCatalogoController", function($scope, $translate, $sce, CatalogoService,$sce,consultaDispService,ModalService) {

	/**
	 * Init
	 */
	getAllCatalogs();

	/**
	 * Show/Hide web componentes
	 */
	CatalogoService.catalogoComponenteWeb({
		agregarCatalogo:{},
		editarCatalogo:{},
		activarCatalogo:{}
	},function(data){
		$scope.webComponentes = data;
	});


	/**
	 * Functions
	 */

	function getAllCatalogs(){

		CatalogoService.catalogoComponenteWeb({
			agregarCatalogo:{},
			editarCatalogo:{},
			activarCatalogo:{}
		},function(data){


			CatalogoService.catalogoCatalogo().success(function(data){

				// cambiar catalog si existe back
				if(window.catalogoSelect !== null){
					$scope.catalogoSelect = data.filter(function(x){if(x.id === window.catalogoSelect)return x})[0];
					$scope.cambiarCatalogo();
				}else{
					$scope.catalogoSelect = data[0];
				}

				$scope.catalogoData = data;
				$scope.catalogoSelectData = data;
				angular.element("#select2-consultaCataogoBusqueda-container").html($scope.catalogoSelect.nombre);
			}).error(function(err){
				console.log("error");
			});

			$scope.webComponentes = data;
		});

	}

	/**
	 * Events
	 */

	$scope.activarDesactivarCatalogo = function (activarDesactivarDispositivo,idCatalogo){
		CatalogoService.activarDesactivarDispositivo(
				activarDesactivarDispositivo.id,
				idCatalogo,
				!activarDesactivarDispositivo.active)
		.success(function(data){
			// el compoente switch ya se encarga de hacer el cambio
			//activarDesactivarDispositivo.active = !activarDesactivarDispositivo.active;
		}).error(function(err){
			activarDesactivarDispositivo.active = activarDesactivarDispositivo.active;
		});
	}

	$scope.cambiarCatalogo = function(){
		CatalogoService.catalogoCatalogos($scope.catalogoSelect.id).success(function(data){
			for(var i in data){
				if(data[i].img !== undefined){
					data[i].img = $sce.trustAsHtml(data[i].img);
				}
			}
			$scope.catalogoData = data;
		}).error(function(err){
			console.log("error");
		});
	};

	$scope.showModal = function(type,id){
		 angular.element(".modal").remove();
		 if(type == "add"){
			 window.catalogoSelectId=$scope.catalogoSelect.id;
			 ModalService.showModal({
		          templateUrl: 'views/catalogo/nuevoCatalogo.html',
		          controller: 'nuevoCatalogoController',
		        }).then(function(modal) {
		          modal.element.modal();
		          modal.close.then(function(result) {
			        	if(result){
			        		modal.element.modal('hide');
			        		$('.modal-backdrop').remove();
			        		window.catalogoSelect=$scope.catalogoSelect.id;
			        		getAllCatalogs();
			        	}
			        });
		        });
		 }
		 else if(type == "edit"){
			 window.catalogoSelectId=id;
			 window.catalogoSelect=$scope.catalogoSelect;
			 ModalService.showModal({
		          templateUrl: 'views/catalogo/actualizaCatalogo.html',
		          controller: 'actualizaCatalogoController',
		        }).then(function(modal) {
		          modal.element.modal();
		          modal.close.then(function(result) {
			        	if(result){
			        		modal.element.modal('hide');
			        		$('.modal-backdrop').remove();
			        		window.catalogoSelect=$scope.catalogoSelect.id;
			        		getAllCatalogs();
			        	}
			        });
		        });
		 }
	};



});