angular.module(appTeclo).controller("actualizaCatalogoController", function(
		$scope, $translate, $sce, CatalogoService, $routeParams,showAlert,ModalService,close,
		msjModalFactory,$rootScope,growl
		) {

	$scope.id = parseInt(window.catalogoSelectId);

	$scope.catalogoSelect = {
		nombre : window.catalogoSelect.nombre,
		id : parseInt(window.catalogoSelect.id)
	}

	$scope.catalogoNew = {
		idCatalogo : null,
		id : "",
		cd : "",
		nb : "",
		tx : "",
		img : "",
		param : "",
		idMarca : null,
		idTipoDispositivo : null,
		idSubTipoDispositivo : null,
		idTipoEvento : null,
		idTipoGrupo : null,
		idTipoComando : null,
		idTipoMedio: null
	};

	$scope.catalogoSelectNew = {
		marca : {},
		tipoDispositivo : {},
		subtipoDispositivo : {},
		tipoEvento : {},
		tipoGrupo : {},
		tipoComando : {},
		tipoMedio : {}
	}

	$scope.listas = {
		marca : [],
		tipoDispositivo : [],
		subtipoDispositivo : [],
		tipoEvento : [],
		tipoGrupo : [],
		tipoComando : [],
		tipoMedio : []
	}
	
	$scope.disableSaveBtn=false;

	/**
	 * Init
	 */

	loadSelectOptions();

	CatalogoService.catalogoCatalogoId($scope.id, $scope.catalogoSelect.id).success(function(data) {

		$scope.catalogoNew = data;
		
		$scope.catalogoSelectNew = {
			marca : $scope.listas.marca.filter(function(x){if(x.id === data.idMarca)return x})[0],
			tipoDispositivo : $scope.listas.tipoDispositivo.filter(function(x){if(x.id === data.idTipoDispositivo)return x})[0],
			subtipoDispositivo : $scope.listas.subtipoDispositivo.filter(function(x){if(x.id === data.idSubTipoDispositivo)return x})[0],
			tipoEvento : $scope.listas.tipoEvento.filter(function(x){if(x.id === data.idTipoEvento)return x})[0],
			tipoGrupo : $scope.listas.tipoGrupo.filter(function(x){if(x.id === data.idTipoGrupo)return x})[0],
			tipoComando : $scope.listas.tipoComando.filter(function(x){if(x.id === data.idTipoComando)return x})[0],
			tipoMedio : $scope.listas.tipoMedio.filter(function(x){if(x.id === data.idTipoMedio)return x})[0]
		}
		
		
		if($scope.catalogoSelectNew.marca && $scope.catalogoSelectNew.marca.nombre) 
			angular.element("#select2-smarca-container").html($scope.catalogoSelectNew.marca.nombre);
		if($scope.catalogoSelectNew.tipoDispositivo && $scope.catalogoSelectNew.tipoDispositivo.nombre) 
			angular.element("#select2-stipodispositivo-container").html($scope.catalogoSelectNew.tipoDispositivo.nombre);
		if($scope.catalogoSelectNew.subtipoDispositivo && $scope.catalogoSelectNew.subtipoDispositivo.nombre) 
			angular.element("#select2-ssubtipodispositivo-container").html($scope.catalogoSelectNew.subtipoDispositivo.nombre);
		if($scope.catalogoSelectNew.tipoEvento && $scope.catalogoSelectNew.tipoEvento.nombre) 
			angular.element("#select2-stipoEvento-container").html($scope.catalogoSelectNew.tipoEvento.nombre);
		if($scope.catalogoSelectNew.tipoGrupo && $scope.catalogoSelectNew.tipoGrupo.nombre) 
			angular.element("#select2-stipoGrupo-container").html($scope.catalogoSelectNew.tipoGrupo.nombre);
		if($scope.catalogoSelectNew.tipoComando && $scope.catalogoSelectNew.tipoComando.nombre) 
			angular.element("#select2-stipoComando-container").html($scope.catalogoSelectNew.tipoComando.nombre);
		if($scope.catalogoSelectNew.tipoMedio && $scope.catalogoSelectNew.tipoMedio.nombre) 
			angular.element("#select2-stipoMedio-container").html($scope.catalogoSelectNew.tipoMedio.nombre);	

	}).error(function(err) {
		console.log("error");
	});

	
	/**
	 * Events
	 */
	
	$scope.disableSave = function() {
		switch ($scope.catalogoSelect.id) {
		// CT_TIPO_DISPOSITIVOS
		case 2:
			if( $scope.catalogoNew.cd && $scope.catalogoNew.cd !== "" &&
				$scope.catalogoNew.nb && $scope.catalogoNew.nb !== "" &&
				$scope.catalogoNew.tx && $scope.catalogoNew.tx !== ""
			   ){
				$scope.disableSaveBtn=false;
				return;
			}
			break;

		// CT_MARCAS
		case 3:
			if( $scope.catalogoSelectNew.tipoDispositivo && $scope.catalogoSelectNew.tipoDispositivo['id'] &&
				$scope.catalogoNew.cd && $scope.catalogoNew.cd !== "" &&
				$scope.catalogoNew.nb && $scope.catalogoNew.nb !== "" &&
				$scope.catalogoNew.tx && $scope.catalogoNew.tx !== ""
			   ){
				$scope.disableSaveBtn=false;
				return;
			}
			break;

		// CT_MODELOS
		case 4:
			if( $scope.catalogoSelectNew.subtipoDispositivo && $scope.catalogoSelectNew.subtipoDispositivo['id'] &&
				$scope.catalogoSelectNew.marca && $scope.catalogoSelectNew.marca['id'] &&
				$scope.catalogoNew.cd && $scope.catalogoNew.cd !== "" &&
				$scope.catalogoNew.nb && $scope.catalogoNew.nb !== "" &&
				$scope.catalogoNew.tx && $scope.catalogoNew.tx !== ""
			   ){
				$scope.disableSaveBtn=false;
				return;
			}
			break;

		// CT_TIPO_EVENTOS
		case 5:
			if( $scope.catalogoNew.cd && $scope.catalogoNew.cd !== "" &&
				$scope.catalogoNew.nb && $scope.catalogoNew.nb !== "" &&
				$scope.catalogoNew.tx && $scope.catalogoNew.tx !== "" &&
				$scope.catalogoNew.img && $scope.catalogoNew.img !== ""
			   ){
				$scope.disableSaveBtn=false;
				return;
			}
			break;

		// CT_TIPO_GEOCERCAS
		case 6:
			if( $scope.catalogoNew.cd && $scope.catalogoNew.cd !== "" &&
				$scope.catalogoNew.nb && $scope.catalogoNew.nb !== "" &&
				$scope.catalogoNew.tx && $scope.catalogoNew.tx !== ""
			   ){
				$scope.disableSaveBtn=false;
				return;
			}
			break;

		// CT_EVENTOS
		case 7:
			if( $scope.catalogoSelectNew.tipoEvento && $scope.catalogoSelectNew.tipoEvento['id'] &&
				$scope.catalogoSelectNew.subtipoDispositivo && $scope.catalogoSelectNew.subtipoDispositivo['id'] &&				
				$scope.catalogoNew.cd && $scope.catalogoNew.cd !== "" &&
				$scope.catalogoNew.nb && $scope.catalogoNew.nb !== "" &&
				$scope.catalogoNew.tx && $scope.catalogoNew.tx !== ""
			   ){
				$scope.disableSaveBtn=false;
				return;
			}
			break;

		// CT_GRUPOS
		case 8:
			if( $scope.catalogoSelectNew.tipoGrupo && $scope.catalogoSelectNew.tipoGrupo['id'] &&				
				$scope.catalogoNew.nb && $scope.catalogoNew.nb !== "" &&
				$scope.catalogoNew.tx && $scope.catalogoNew.tx !== ""
			   ){
				$scope.disableSaveBtn=false;
				return;
			}
			break;

		// CT_TIPO_GRUPOS
		case 9:
			if( $scope.catalogoNew.cd && $scope.catalogoNew.cd !== "" &&
				$scope.catalogoNew.nb && $scope.catalogoNew.nb !== "" &&
				$scope.catalogoNew.tx && $scope.catalogoNew.tx !== ""
			   ){
				$scope.disableSaveBtn=false;
				return;
			}
			break;
			
		// CT_TIPO_COMANDOS
		case 10:
			if( $scope.catalogoNew.cd && $scope.catalogoNew.cd !== "" &&
				$scope.catalogoNew.nb && $scope.catalogoNew.nb !== "" &&
				$scope.catalogoNew.tx && $scope.catalogoNew.tx !== ""
			   ){
				$scope.disableSaveBtn=false;
				return;
			}
			break;	
			
		// CT_COMANDOS
		case 11:
			if( $scope.catalogoNew.cd && $scope.catalogoNew.cd !== "" &&
				$scope.catalogoSelectNew.tipoComando && $scope.catalogoSelectNew.tipoComando['id'] &&
			    $scope.catalogoSelectNew.tipoMedio && $scope.catalogoSelectNew.tipoMedio['id']			    
			   ){
				$scope.disableSaveBtn=false;
				return;
			}
			break;	
			
		}
		
		$scope.disableSaveBtn=true;
		
	}

	
	$scope.save = function() {

		$scope.catalogoNew.idCatalogo = $scope.catalogoSelect.id;

		if($scope.catalogoSelectNew.marca !== undefined)
			$scope.catalogoNew.idMarca = $scope.catalogoSelectNew.marca.id;
		if($scope.catalogoSelectNew.tipoDispositivo !== undefined)
			$scope.catalogoNew.idTipoDispositivo = $scope.catalogoSelectNew.tipoDispositivo.id;
		if($scope.catalogoSelectNew.subtipoDispositivo !== undefined)
			$scope.catalogoNew.idSubTipoDispositivo = $scope.catalogoSelectNew.subtipoDispositivo.id;
		if($scope.catalogoSelectNew.tipoEvento !== undefined)
			$scope.catalogoNew.idTipoEvento = $scope.catalogoSelectNew.tipoEvento.id;
		if($scope.catalogoSelectNew.tipoGrupo !== undefined)
			$scope.catalogoNew.idTipoGrupo = $scope.catalogoSelectNew.tipoGrupo.id;
		if($scope.catalogoSelectNew.tipoComando !== undefined)
			$scope.catalogoNew.idTipoComando = $scope.catalogoSelectNew.tipoComando.id;
		if($scope.catalogoSelectNew.tipoMedio !== undefined)
			$scope.catalogoNew.idTipoMedio = $scope.catalogoSelectNew.tipoMedio.id;
		if($scope.catalogoSelectNew.catalogoNew !== undefined)
			$scope.catalogoNew.id = parseInt($scope.catalogoNew.id)

		
		ModalService.showModal({
				templateUrl: 'views/templatemodal/templateModalConfirmacion.html',
		        controller: 'mensajeModalController',
		        inputs:{message: msjModalFactory.getMensaje("¿Esta seguro de actualizar el catálogo?", $rootScope.currentLanguage)}			
		    }).then(function(modal) {
		        modal.element.modal();
		        modal.close.then(function(result) {
		        	if(result){
		        		CatalogoService.actualizaCatalogo($scope.catalogoNew)
		        				.success(function(data) {
		        					close(true);
		        					growl.success(msjModalFactory.getMensaje('Catálogo actualizado', $rootScope.currentLanguage), {title : msjModalFactory.getMensaje('Éxito', $rootScope.currentLanguage)});
		        				}).error(function(err) {
		        					showAlert.error(msjModalFactory.getMensaje("Ocurrio un problema al guardar", $rootScope.currentLanguage));
		        				});
		        		   		
		        	}
		        }); 
		    });	

	}
	
	$scope.back = function(){
		window.catalogoSelect=$scope.catalogoSelect.id;
		window.location='#/consultaCatalogo'
	}
	
	
	/**
	 * util
	 */
	function loadSelectOptions() {

		CatalogoService.catalogoTipoDispositivo().success(function(data) {
			$scope.listas.tipoDispositivo = data;
		}).error(function(err) {
			console.log("error");
		});

		CatalogoService.catalogoMarca().success(function(data) {
			$scope.listas.marca = data;
		}).error(function(err) {
			console.log("error");
		});

		CatalogoService.catalogoSubTipoDispositivoTodos().success(function(data) {
			$scope.listas.subtipoDispositivo = data;
		}).error(function(err) {
			console.log("error");
		});

		CatalogoService.catalogoTipoGrupo().success(function(data) {
			$scope.listas.tipoGrupo = data;
		}).error(function(err) {
			console.log("error");
		});

		CatalogoService.catalogoTipoEvento().success(function(data) {
			$scope.listas.tipoEvento = data;
		}).error(function(err) {
			console.log("error");
		});
		
		CatalogoService.catalogoTipoComando().success(function(data) {
			$scope.listas.tipoComando = data;
		}).error(function(err) {
			console.log("error");
		});

		CatalogoService.catalogoTipoMedio().success(function(data) {
			$scope.listas.tipoMedio = data;
		}).error(function(err) {
			console.log("error");
		});
		
	}

});