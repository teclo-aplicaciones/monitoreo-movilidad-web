angular.module(appTeclo).controller("nuevoCatalogoController", function($scope, $translate, $sce, CatalogoService, showAlert,ModalService,close) {

	/**
	 * Vars
	 */
	$scope.catalogoSelect = [];
	$scope.catalogoSelectData = [];
	$scope.catalogoNew = {
		idCatalogo : null,
		id : "",
		cd : "",
		nb : "",
		tx : "",
		img : "",
		param : "",
		idMarca : null,
		idTipoDispositivo : null,
		idSubTipoDispositivo : null,
		idTipoEvento : null,
		idTipoGrupo : null,
		idTipoComando : null,
		idTipoMedio: null
	};

	$scope.catalogoSelectNew = {
		marca : {},
		tipoDispositivo : {},
		subtipoDispositivo : {},
		tipoEvento : {},
		tipoGrupo : {},
		tipoComando : {},
		tipoMedio : {}
	}

	$scope.listas = {
		marca : [],
		tipoDispositivo : [],
		subtipoDispositivo : [],
		tipoEvento : [],
		tipoGrupo : [],
		tipoComando : [],
		tipoMedio : []
	}
	
	$scope.disableSaveBtn= true;

	/**
	 * Init
	 */

	// get all catalogs
	CatalogoService.catalogoCatalogo().success(function(data) {
		data.shift();// eliminar el catalogo
		$scope.catalogoSelect = data[0];
		$scope.catalogoSelectData = data;
		
		if(window.catalogoSelectId){
			$scope.catalogoSelect = data.filter(function(x){if(x.id === window.catalogoSelectId)return x})[0];
		}
		
	}).error(function(err) {
		console.log("error");
	});

	loadSelectOptions();

	/**
	 * Events
	 */

	$scope.disableSave = function() {
		switch ($scope.catalogoSelect.id) {
		// CT_TIPO_DISPOSITIVOS
		case 2:
			if( $scope.catalogoNew.cd && $scope.catalogoNew.cd !== "" &&
				$scope.catalogoNew.nb && $scope.catalogoNew.nb !== "" &&
				$scope.catalogoNew.tx && $scope.catalogoNew.tx !== ""
			   ){
				$scope.disableSaveBtn=false;
				return;
			}
			break;

		// CT_MARCAS
		case 3:
			if( $scope.catalogoSelectNew.tipoDispositivo && $scope.catalogoSelectNew.tipoDispositivo['id'] &&
				$scope.catalogoNew.cd && $scope.catalogoNew.cd !== "" &&
				$scope.catalogoNew.nb && $scope.catalogoNew.nb !== "" &&
				$scope.catalogoNew.tx && $scope.catalogoNew.tx !== ""
			   ){
				$scope.disableSaveBtn=false;
				return;
			}
			break;

		// CT_MODELOS
		case 4:
			if( $scope.catalogoSelectNew.subtipoDispositivo && $scope.catalogoSelectNew.subtipoDispositivo['id'] &&
				$scope.catalogoSelectNew.marca && $scope.catalogoSelectNew.marca['id'] &&
				$scope.catalogoNew.cd && $scope.catalogoNew.cd !== "" &&
				$scope.catalogoNew.nb && $scope.catalogoNew.nb !== "" &&
				$scope.catalogoNew.tx && $scope.catalogoNew.tx !== ""
			   ){
				$scope.disableSaveBtn=false;
				return;
			}
			break;

		// CT_TIPO_EVENTOS
		case 5:
			if( $scope.catalogoNew.cd && $scope.catalogoNew.cd !== "" &&
				$scope.catalogoNew.nb && $scope.catalogoNew.nb !== "" &&
				$scope.catalogoNew.tx && $scope.catalogoNew.tx !== "" &&
				$scope.catalogoNew.img && $scope.catalogoNew.img !== ""
			   ){
				$scope.disableSaveBtn=false;
				return;
			}
			break;

		// CT_TIPO_GEOCERCAS
		case 6:
			if( $scope.catalogoNew.cd && $scope.catalogoNew.cd !== "" &&
				$scope.catalogoNew.nb && $scope.catalogoNew.nb !== "" &&
				$scope.catalogoNew.tx && $scope.catalogoNew.tx !== ""
			   ){
				$scope.disableSaveBtn=false;
				return;
			}
			break;

		// CT_EVENTOS
		case 7:
			if( $scope.catalogoSelectNew.tipoEvento && $scope.catalogoSelectNew.tipoEvento['id'] &&
				$scope.catalogoSelectNew.subtipoDispositivo && $scope.catalogoSelectNew.subtipoDispositivo['id'] &&				
				$scope.catalogoNew.cd && $scope.catalogoNew.cd !== "" &&
				$scope.catalogoNew.nb && $scope.catalogoNew.nb !== "" &&
				$scope.catalogoNew.tx && $scope.catalogoNew.tx !== ""
			   ){
				$scope.disableSaveBtn=false;
				return;
			}
			break;

		// CT_GRUPOS
		case 8:
			if( $scope.catalogoSelectNew.tipoGrupo && $scope.catalogoSelectNew.tipoGrupo['id'] &&				
				$scope.catalogoNew.nb && $scope.catalogoNew.nb !== "" &&
				$scope.catalogoNew.tx && $scope.catalogoNew.tx !== ""
			   ){
				$scope.disableSaveBtn=false;
				return;
			}
			break;

		// CT_TIPO_GRUPOS
		case 9:
			if( $scope.catalogoNew.cd && $scope.catalogoNew.cd !== "" &&
				$scope.catalogoNew.nb && $scope.catalogoNew.nb !== "" &&
				$scope.catalogoNew.tx && $scope.catalogoNew.tx !== ""
			   ){
				$scope.disableSaveBtn=false;
				return;
			}
			break;
			
		// CT_TIPO_COMANDOS
		case 10:
			if( $scope.catalogoNew.cd && $scope.catalogoNew.cd !== "" &&
				$scope.catalogoNew.nb && $scope.catalogoNew.nb !== "" &&
				$scope.catalogoNew.tx && $scope.catalogoNew.tx !== ""
			   ){
				$scope.disableSaveBtn=false;
				return;
			}
			break;	
			
		// CT_COMANDOS
		case 11:
			if( $scope.catalogoNew.cd && $scope.catalogoNew.cd !== "" &&
				$scope.catalogoSelectNew.tipoComando && $scope.catalogoSelectNew.tipoComando['id'] &&
				$scope.catalogoSelectNew.tipoMedio && $scope.catalogoSelectNew.tipoMedio['id']				    
			   ){
				$scope.disableSaveBtn=false;
				return;
			}
			break;	
		}
		
		$scope.disableSaveBtn=true;
		
	}

	$scope.changeCatalog = function() {
		clean();
		loadSelectOptions();
		$scope.disableSaveBtn=true;
	};

	$scope.save = function() {

		$scope.catalogoNew.idCatalogo = $scope.catalogoSelect.id;

		$scope.catalogoNew.idMarca = $scope.catalogoSelectNew.marca.id;
		$scope.catalogoNew.idTipoDispositivo = $scope.catalogoSelectNew.tipoDispositivo.id;
		$scope.catalogoNew.idSubTipoDispositivo = $scope.catalogoSelectNew.subtipoDispositivo.id;
		$scope.catalogoNew.idTipoEvento = $scope.catalogoSelectNew.tipoEvento.id;
		$scope.catalogoNew.idTipoGrupo = $scope.catalogoSelectNew.tipoGrupo.id;
		$scope.catalogoNew.idTipoComando = $scope.catalogoSelectNew.tipoComando.id;
		$scope.catalogoNew.id = parseInt($scope.catalogoNew.id)
		$scope.catalogoNew.idTipoMedio = $scope.catalogoSelectNew.tipoMedio.id;
		
		ModalService.showModal({
			templateUrl: 'views/templatemodal/templateModalConfirmacion.html',
		        controller: 'mensajeModalController',
		        	inputs:{ message: "¿Esta seguro de agregar el catálogo?"}
		    }).then(function(modal) {
		        modal.element.modal();
		        modal.close.then(function(result) {
		        	if(result){
		        		CatalogoService.agregaCatalogo($scope.catalogoNew).success(function(data) {
		        			$scope.listas.tipoEvento = data;
		        			clean();
		        			loadSelectOptions();
		        			
		        			
		        			
		        			close(true);
		        			showAlert.aviso("Registro agregado correctamente");
		        		}).error(function(err) {
		        			showAlert.error("Ocurrio un problema al guardar");
		        		});        		
		        	}
		        }); 
		    });	
	}

	/**
	 * util
	 */
	function clean() {
		$scope.catalogoNew = {
			id : "",
			cd : "",
			nb : "",
			tx : "",
			img : "",
			param : "",
			idMarca : null,
			idTipoDispositivo : null,
			idSubTipoDispositivo : null,
			idTipoEvento : null,
			idTipoGrupo : null,
			idTipoComando : null,
			idTipoMedio : null
		};
	}

	function loadSelectOptions() {

		CatalogoService.catalogoTipoDispositivo().success(function(data) {
			$scope.listas.tipoDispositivo = data;
		}).error(function(err) {
			console.log("error");
		});

		CatalogoService.catalogoMarca().success(function(data) {
			$scope.listas.marca = data;
		}).error(function(err) {
			console.log("error");
		});

		CatalogoService.catalogoSubTipoDispositivoTodos().success(function(data) {
			$scope.listas.subtipoDispositivo = data;
		}).error(function(err) {
			console.log("error");
		});

		CatalogoService.catalogoTipoGrupo().success(function(data) {
			$scope.listas.tipoGrupo = data;
		}).error(function(err) {
			console.log("error");
		});

		CatalogoService.catalogoTipoEvento().success(function(data) {
			$scope.listas.tipoEvento = data;
		}).error(function(err) {
			console.log("error");
		});
		
		CatalogoService.catalogoTipoComando().success(function(data) {
			$scope.listas.tipoComando = data;
		}).error(function(err) {
			console.log("error");
		});
		
		CatalogoService.catalogoTipoMedio().success(function(data) {
			$scope.listas.tipoMedio = data;
		}).error(function(err) {
			console.log("error");
		});

	}

});