angular.module(appTeclo).controller("actualizaNotificacionController", function($scope, $translate, $rootScope, $window, notificaVO, isNewRegistry, CatalogoService , NotificacionService, showAlert) {
	$scope.listaComboNotifica={}
	$scope.listaComboNotifica.lista = [
		{id:1, nombre:"PUSH"},
		{id:2, nombre:"MAIL"},
		{id:3, nombre:"SMS"}
	];
	
	$scope.listaNotificaDispositivos=[];	
	
	$scope.listaGrupos={};
	
	$scope.bandera = {
			flag:false
	}	

	$scope.fechaProgramadaLimites = {
			format: 'DD/MM/YYYY HH:mm',
	};
	
	$scope.notificaVO = {};
	
	$scope.prevToggle = undefined;
	
	$scope.toggleTipoNotificacion = false;
	
	$scope.notiVO = angular.copy(notificaVO.data);
	
	
	  /******************************/
	 /** Configuracion de idiomas **/
	/******************************/
	
	$scope.traducir = function(callback){
		$translate(['NOTIFICACIONES.Actualiza.modalDispositivo.label', 
					'NOTIFICACIONES.Actualiza.modalDispositivo.label', 
					'NOTIFICACIONES.Actualiza.modalDispositivo.labelDisp', 
					'NOTIFICACIONES.Actualiza.modalDispositivo.labelAsig',
					'MONITOREO.tiempoReal.nombreBox.filtracion.seleccionar']).then(function (headline) {
		    $scope.translate ={
		    		titulo: headline['NOTIFICACIONES.Actualiza.modalDispositivo.label'],
		    		dispo: headline['NOTIFICACIONES.Actualiza.modalDispositivo.labelDisp'],
		    		actua: headline['NOTIFICACIONES.Actualiza.modalDispositivo.labelAsig'],
		    		seleccione:headline['MONITOREO.tiempoReal.nombreBox.filtracion.seleccionar']
		    }
		    if(callback)
		    	callback()
		}, function (translationId) {
		    $scope.headline = translationId;
		});
	};
	
	  /***************/
	 /** Funciones **/
	/***************/
	
	$scope.init = function(){
		CatalogoService.buscaCatTipoNotifica().success(function(data){
			$scope.listaComboNotifica.lista = data;
			//$scope.listaComboNotifica.valor = $scope.listaComboNotifica.lista[0];
			
			//Valida que notiVO tenga el valor de la notificacion para rellenar los campos 
			// que se van a actualizar
			if(notificaVO){
				$scope.notificaVO.id = $scope.notiVO.id;
				$scope.notificaVO.nombre = $scope.notiVO.nombre;
				$scope.notificaVO.mensaje = $scope.notiVO.mensaje;
				$scope.notificaVO.tipo = $scope.notiVO.tipo;
				$scope.notificaVO.fecha = $scope.notiVO.fecha;
				//var fecha =$scope.notificaVO.fecha.replace(' ','/').split('/');
				$scope.fechaProgramadaLimites = {
					format: 'DD/MM/YYYY HH:mm',
					defaultDate: moment($scope.notificaVO.fecha,'DD/MM/YYYY HH:mm'),
					minDate: moment()
				};
				//$scope.fechaProgramadaLimites.defaultDate = moment(angular.copy($scope.notificaVO.fecha), "DD/MM/YYYY HH:mm");
				//$scope.fechaProgramadaLimites.minDate = new Date($scope.fechaProgramadaLimites.defaultDate.getTime()-1000); 
				//$('#dateFechaEnvio').text($scope.notificaVO.fecha);
				$scope.notificaVO.grupo = $scope.notiVO.grupo;
				//$scope.dateFechaEnvio = angular.copy($scope.notificaVO.fecha);
				//$scope.dateFechaEnvio = moment(angular.copy($scope.notificaVO.fecha), "DD/MM/YYYY HH:mm");
				 
				$scope.listaComboNotifica.valor = $scope.listaComboNotifica.lista.find(function(el){
					if(el.id == $scope.notificaVO.tipo)
						return el;
				});
				$('#select2-comboTipoNotificacion-container').text($scope.listaComboNotifica.valor.nombre==undefined ? 'Desconocido':$scope.listaComboNotifica.valor.nombre);
				
				$scope.toggleTipoNotificacion = $scope.notificaVO.grupo != 0 ? true:false;
				if($scope.notificaVO.grupo != 0){
					$scope.toggleTipoNotificacion = true;
					$scope.prevToggle = !$scope.toggleTipoNotificacion;
				}
				$scope.abrirModalDispositivo(false);
			}
		}).error(function(err){
				
		});
	}
	
	$scope.toggleTipoAsignacion = function(){
		
		$scope.toggleTipoNotificacion = !$scope.toggleTipoNotificacion;
	};
	
	$scope.callbackDispositivos = function(allowDevice){
		//Ajustar lista de dispositivos que estan seleccionados
		if($scope.notiVO.dispositivos.length > 0){
			if($scope.notificaVO.grupo != 0 && $scope.toggleTipoNotificacion && allowDevice == undefined){
				//$scope.toggleDispositivos = false;
				$scope.listaGrupos.valor = $scope.listaGrupos.lista.find(function(el){ return el.id == $scope.notificaVO.grupo});
				$scope.buscarDispositivoEnGrupo($scope.listaGrupos.valor.id, function(vr){$scope.callbackDispositivos(vr)});
				$('#select2-comboFiltroMonitoreo-container').text($scope.listaGrupos.valor.nombre);
				//$scope.listaDispositivos.list.push();
			}
			if(allowDevice || $scope.toggleTipoNotificacion == false){
				$scope.bandera.flag = true;
				$scope.listaNotificaDispositivos.list[1].items = $scope.listaNotificaDispositivos.list[0].items.filter(
						function (el){
							var found = $scope.notiVO.dispositivos.find(function(elm){
								return el.idItem == elm.idItem;
							}); 
							if(found != undefined)
								$scope.listaNotificaDispositivos.list[0].items = $scope.listaNotificaDispositivos.list[0].items.filter(
										function (el){
											return el.idItem != found.idItem; 
								});
						return found ? found : null;  
				});
				$scope.bandera.flag = true;
			}
		}
	}
	
	$scope.abrirModalDispositivo=function(force){
		$rootScope.$evalAsync(function() {
			$scope.showModalAsignacion = force != undefined ? force:true;
		});
		
		
		if($scope.prevToggle != $scope.toggleTipoNotificacion)
		{			
			$scope.bandera.flag=true;
			$scope.listaNotificaDispositivos.list = [];
			$scope.prevToggle = $scope.toggleTipoNotificacion;
			
			if(!$scope.toggleTipoNotificacion){
				//NotificacionService.buscarAgrupamiento(4).success(function(data){
				NotificacionService.buscarNotificacionDispositivos().success(function(data){
					$scope.bandera.flag=true;
					var tempLista = {
							titulo:$scope.translate.dispo,
							items:[]
					};
					
					tempLista.items = data.map(function(el, idx){
						return {
							idItem:el.id,
							srItem:el.serial,
							txtItem:el.nombre
						};
					});
					
					$scope.listaNotificaDispositivos.list.push({
						titulo: $scope.translate.dispo,
						items: tempLista.items
					});
					
					$scope.listaNotificaDispositivos.list.push({
						titulo: $scope.translate.actua,
						items:[]
					});
					
					if(force != undefined)
						$scope.callbackDispositivos();
				}).error(function(err){
					console.log("error");
				});
			}else{
				NotificacionService.buscarAgrupamiento(1).success(function(data){
					$scope.listaGrupos.lista = data;
					//$scope.listaGrupos.lista.unshift({id:0, nombre:"Seleccione"});
					$scope.listaGrupos.valor = {id:0, nombre:$scope.translate.seleccione}
					$('#select2-comboFiltroMonitoreo-container').text($scope.listaGrupos.valor.nombre);
					
					if(force != undefined)
						$scope.callbackDispositivos();
				}).error(function(err){
					console.log("error");
				});
			}
		}
	}
	
	$scope.buscarDispositivoEnGrupo=function(id, callback){
		
		$scope.bandera.flag=true;
		
		$scope.listaNotificaDispositivos.list = [];
		
		if(id != undefined){
			$scope.bandera.flag=true;
			
			NotificacionService.buscarDispositivoEnGrupoParaNotificacion(id).success(function(data){
				if(data.length > 0){
					var tempLista = {
							titulo:$scope.translate.dispo,
							items:[]
					};
					
					tempLista.items = data.map(function(el, idx){
						return {
							idItem:el.id,
							srItem:el.serial,
							txtItem:el.nombre
						};
					});
					
					$scope.bandera.flag=true;
					
					$scope.listaNotificaDispositivos.list.push({
						titulo: $scope.translate.dispo,
						items: tempLista.items
					});
					
					$scope.listaNotificaDispositivos.list.push({
						titulo: $scope.translate.actua,
						items:[]
					});
					
					if(callback)
						callback(true)
				}else{
					showAlert.error("No existen dispositivos asociados a este grupo");
				}
			}).error(function(err){
				
			});
		}
	}
	
	$scope.actualizaNotificacion=function(){
		if(isNewRegistry == true){
			if($scope.formActualizaNotifica.$invalid){
				showAlert.requiredFields($scope.formActualizaNotifica);
			}else{
				$scope.notificaVO.id = -1;
				//Acoplamos el metodo de envio como tipo en VO
				$scope.notificaVO.tipo = $scope.listaComboNotifica.valor.id;
				//Acoplamos la bandera en caso de que el envio sea inmediato
				$scope.notificaVO.isNow = $scope.toggleTipoEnvio ? true : false;
				//Acoplamos el grupo seleccionado, en caso contrario, se manda 0 en el VO
				$scope.notificaVO.grupo = $scope.listaGrupos.valor != undefined ? $scope.listaGrupos.valor.id : 0
				$scope.notificaVO.dispositivos = $scope.listaNotificaDispositivos.list.length >0 ? $scope.listaNotificaDispositivos.list[1].items.length > 0 ? [...$scope.listaNotificaDispositivos.list[1].items]: undefined:undefined;
				if($scope.notificaVO.dispositivos != undefined){
					if($scope.dateFechaEnvio != undefined){
						$scope.notificaVO.fecha = !$scope.toggleTipoEnvio ? $scope.dateFechaEnvio.format("DD/MM/YYYY HH:mm") : null
						//$scope.notificaVO.fecha = !$scope.toggleTipoEnvio ? $scope.dateFechaEnvio : null
					}else{
						if($scope.toggleTipoEnvio){
							$scope.notificaVO.fecha = null;
						}else{
							showAlert.error("Debe de seleccionar una fecha de envio o marcar la casilla de 'Envio Inmediato'.");
						}
					}
					
					if($scope.notificaVO.fecha != null || $scope.notificaVO.isNow){
						//Enviamos
						NotificacionService.guardarNotificacion($scope.notificaVO).success(function(data){
							if(data){
								if(!$scope.notificaVO.isNow){
									showAlert.aviso("Notificacion guardada con exito")
								}else{
									showAlert.aviso("Notificacion enviada con exito")
								}
								//Limpiar
								$scope.formAltaNotifica.$setPristine();
								$scope.notificaVO = {};
								$scope.listaComboNotifica.valor = $scope.listaComboNotifica.lista[0];
								$scope.prevToggle = undefined;
							}
						}).error(function(err){
							
						});
					}else{
						showAlert.error("Debe de seleccionar una fecha de envio o marcar la casilla de 'Envio Inmediato'.");
					}
				}else{
					showAlert.error("Debe de seleccionar al menos un dispositvo para enviar la notificación");
				}
			}
		}else{
			if($scope.formActualizaNotifica.$invalid){
				showAlert.requiredFields($scope.formActualizaNotifica);
			}else{
				//Acoplamos el metodo de envio como tipo en VO
				$scope.notificaVO.tipo = $scope.listaComboNotifica.valor.id;
				//Acoplamos la bandera en caso de que el envio sea inmediato
				$scope.notificaVO.isNow = $scope.toggleTipoEnvio ? true : false;
				//Acoplamos el grupo seleccionado, en caso contrario, se manda 0 en el VO
				$scope.notificaVO.grupo = $scope.listaGrupos.valor != undefined ? $scope.listaGrupos.valor.id : 0
				$scope.notificaVO.dispositivos = $scope.listaNotificaDispositivos.list.length >0 ? $scope.listaNotificaDispositivos.list[1].items.length > 0 ? [...$scope.listaNotificaDispositivos.list[1].items]: undefined:undefined;
				if($scope.notificaVO.dispositivos != undefined){
					if($scope.dateFechaEnvio !== "" && $scope.dateFechaEnvio != undefined){
						$scope.notificaVO.fecha = !$scope.toggleTipoEnvio ? $scope.dateFechaEnvio.format("DD/MM/YYYY HH:mm") : null
						//$scope.notificaVO.fecha = !$scope.toggleTipoEnvio ? $scope.dateFechaEnvio : null
					}else{
						if($scope.toggleTipoEnvio){
							$scope.notificaVO.fecha = null;
						}else{
							showAlert.error("Debe de seleccionar una fecha de envio o marcar la casilla de 'Envio Inmediato'.");
						}
					}
					
					if($scope.notificaVO.fecha != null || $scope.notificaVO.isNow){
						//Enviamos
						NotificacionService.actualizaNotificacion($scope.notificaVO).success(function(data){
							if(data){
								if(!$scope.notificaVO.isNow){
									showAlert.aviso("Notificacion actualizada con exito")
								}else{
									showAlert.aviso("Notificacion enviada con exito")
								}
							}
						}).error(function(err){
							
						});
					}else{
						showAlert.error("Debe de seleccionar una fecha de envio o marcar la casilla de 'Envio Inmediato'.");
					}
				}else{
					showAlert.error("Debe de seleccionar al menos un dispositvo para enviar la notificación");
				}
			}
		}
	};
	
	$scope.regresar = function(){
		$window.history.back();
	};
	
	  /*************************/
	 /** Oyentes / Listeners **/
	/*************************/
	
	$scope.$watch("$parent.currentLanguage", function(newValue, oldValue) {
		$scope.traducir(null);
	});
	
	  /********************/
	 /** Inicializacion **/
	/********************/
	$scope.traducir($scope.init);
});