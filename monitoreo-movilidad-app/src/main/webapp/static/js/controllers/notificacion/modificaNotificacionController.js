angular.module(appTeclo).controller("modificaNotificacionController", function($scope, $translate, CatalogoService, NotificacionService, showAlert) {
	$scope.busquedaVO = {
		fInicial:undefined,
		fFinal:undefined
	};
	
	$scope.tipoBusqueda = {};
	
	$scope.detalleNotifica= {
			usuarios:[],
			mensaje:{}
	}

	$scope.listaMedio = {
		lista:[]
	}

	$scope.viewDetail={
		
		searchSomething:""
	};
	$scope.viewNoti={};
	
	$scope.listaCatalogo = {
		lista:[]
	}
	
	$scope.showDetalleNotificacion = false;
	
	requiredFields = function(form){
		
		angular.forEach(form.$error, function (field) {
            	angular.forEach(field, function(errorField){
            	errorField.$setDirty();
            })
                     
            //$scope.showAviso("Es necesario completar el formulario");
		});
	}
	
	  /******************************/
	 /** Configuracion de idiomas **/
	/******************************/
	
	$scope.traducir = function(callback){
		$translate(['NOTIFICACIONES.Modifica.busquedaNotificacion.tipoBusqueda.medioBusqueda.todos']).then(function (headline) {
		    $scope.translate ={
		    		todos: headline['NOTIFICACIONES.Modifica.busquedaNotificacion.tipoBusqueda.medioBusqueda.todos']
		    }
		    if(callback)
		    	callback()
		}, function (translationId) {
		    $scope.headline = translationId;
		});
	}

	
	$scope.buscarCatalogo = function(){
		CatalogoService.catalogoBusqNotificaciones().success(function(data){
			$scope.listaCatalogo.lista = data;
			$scope.busquedaVO.tipoBusca = $scope.listaCatalogo.lista[0];
			$('#select2-comboBusquedaNotificacion-container').text($scope.busquedaVO.tipoBusca.nombre);
			
			if(NotificacionService.consultaVO.length > 0)
				$scope.notificaciones = NotificacionService.consultaVO;
			CatalogoService.buscaCatTipoNotifica().success(function(data){
				$scope.listaMedio.lista = data;
				$scope.listaMedio.valor =  {id:-1, nombre:$scope.translate.todos};
				$('#select2-comboMedioNotificacion-container').text($scope.listaMedio.valor.nombre);
			}).error(function(err){

			});
		}).error(function(err){
			console.log("error");
		});
	}
	$scope.modoBusqueda = function(){
		if($scope.busquedaVO.tipoBusca.id != 1){
			$scope.toggleTipo = true;
			NotificacionService.catalogoAgrupamiento($scope.busquedaVO.tipoBusca.id).success(function(data){
				$scope.tipoBusqueda.lista = data;
				//$scope.tipoBusqueda.valor = $scope.tipoBusqueda.lista[0];
				$scope.tipoBusqueda.valor = {id:-1, nombre:"Seleccione"};
				$('#select2-comboTipoBusquedaNotificacion-container').text($scope.tipoBusqueda.valor.nombre);
				$('#select2-comboTipoBusquedaNotificacion-container').attr('title',$scope.tipoBusqueda.valor.nombre);
			}).error(function(err){
				console.log("error");
			});
			
		}else{
			$scope.toggleTipo = false;
		}
		$scope.notificaciones = undefined;
	}
	
	$scope.consulta= function(tipo, VO){
		if($scope.formModificaNotificacion.$invalid){
			showAlert.requiredFields($scope.formModificaNotificacion);
		}else{
			var fIni = VO.fInicial != null ? VO.fInicial.format("YYYY-MM-DD") :"";
			var fFin = VO.fFinal != null ? VO.fFinal.format("YYYY-MM-DD") :"";
			NotificacionService.consultaNotificaciones(tipo, tipo != 1 ? $scope.tipoBusqueda.valor.id : VO.busca, $scope.listaMedio.valor !=undefined? $scope.listaMedio.valor.id : 0, fIni, fFin).success(function(data){
				if(data.length>0){
					NotificacionService.consultaVO = data 
				$scope.notificaciones = NotificacionService.consultaVO;
				}else{
					showAlert.error("No se encontraron resultados.");
					$scope.notificaciones = undefined;
				}
			}).error(function(err){
				showAlert.error(err.message);
				$scope.notificaciones = undefined;
			})
		}
	};
	
	$scope.consultaDetalle = function(id){
		$scope.detalleNotifica.usuarios = [];
		NotificacionService.consultaDetalle(id).success(function(data){
			var notificacion = $scope.notificaciones.find(function(el){ return el.id == id});
			
			$scope.detalleNotifica.usuarios = data;
			
			$scope.detalleNotifica.mensaje.nombre = notificacion.nombre;
			$scope.detalleNotifica.mensaje.fecha = notificacion.fecha;
			$scope.detalleNotifica.mensaje.activo = notificacion.activo;
			$scope.detalleNotifica.mensaje.mensaje = notificacion.mensaje;
			$scope.detalleNotifica.mensaje.tipo = notificacion.tipo == 1?'Push':notificacion.tipo == 2 ? 'Mail':notificacion.tipo == 3 ? 'Correo':'Desconocido';
			
			$scope.abrirModalDetalle();
		}).error(function(err){
			
		});
		
	}
	
	$scope.confirmaElim =function(notis){
		showAlert.confirmacion("¿Estas seguro que desea eliminar '"+notis.nombre+"'?", function(){
			NotificacionService.eliminaNotificacion(notis.id).success(function(data){
				if(data){
					showAlert.aviso("La notificación '"+notis.nombre+"' se canceló con éxito.");
					$scope.notificaciones = $scope.notificaciones.map(function(el){
						if(el.id == notis.id){
							el.activo= 0
						}
						return el ;
					})
				}
			}).error(function(err){
				showAlert.error(err);
			})
			},null, null, null);
	};
	
	$scope.abrirModalDetalle = function(){
		$scope.showDetalleNotificacion = true;
	};
	
	//Formato de fecha
	$scope.formatDate = function(date){
		return new Date(date);
	};
	
	$scope.$on('$locationChangeSuccess', function(event, current, previous) {
		var page = current.split('/')[5];
		if(page !== "actualizaNotificacion" && page !== "modificaNotificacion")
			NotificacionService.consultaVO = [];
    });
	
	//init
	$scope.traducir($scope.buscarCatalogo);
});