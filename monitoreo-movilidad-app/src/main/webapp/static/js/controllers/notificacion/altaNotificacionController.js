angular.module(appTeclo).controller("altaNotificacionController", function($scope, $translate, MonitoreoService, NotificacionService, CatalogoService, showAlert) {
	$scope.notificaVO = {}
	$scope.listaComboNotifica={}
	
	$scope.listaNotificaDispositivos={
			list:[]
	};	
	
	$scope.listaGrupos={};
	
	$scope.bandera = {
			flag:false
	}
	$scope.prevToggle=null;
	
	$scope.toggleTipoNotificacion = false;
	
	$scope.fechaProgramadaLimites = {
			format: 'DD/MM/YYYY HH:mm',
			minDate: moment()
		};
	
	//Obtener imagen a Base64
	/*var xhr = new XMLHttpRequest();
	  xhr.onload = function() {
	    var reader = new FileReader();
	    reader.onloadend = function() {
	      //callback(reader.result);
	    	$scope.notificaVO.base64 = reader.result.split('data:')[1];
	    }
	    reader.readAsDataURL(xhr.response);
	  };
	  xhr.open('GET', "./static/dist/img/iconTeclo.png");
	  xhr.responseType = 'blob';
	  xhr.send();*/
	
	  /******************************/
	 /** Configuracion de idiomas **/
	/******************************/
	
	$scope.traducir = function(callback){
		$translate(['NOTIFICACIONES.Nuevo.modalDispositivo.label', 
					'NOTIFICACIONES.Nuevo.modalDispositivo.labelDisp', 
					'NOTIFICACIONES.Nuevo.modalDispositivo.labelAsig',
					'MONITOREO.tiempoReal.nombreBox.filtracion.seleccionar']).then(function (headline) {
		    $scope.translate ={
		    		titulo: headline['NOTIFICACIONES.Nuevo.modalDispositivo.label'],
		    		dispo: headline['NOTIFICACIONES.Nuevo.modalDispositivo.labelDisp'],
		    		actua: headline['NOTIFICACIONES.Nuevo.modalDispositivo.labelAsig'],
		    		seleccione:headline['MONITOREO.tiempoReal.nombreBox.filtracion.seleccionar']
		    }
		    if(callback)
		    	callback()
		}, function (translationId) {
		    $scope.headline = translationId;
		});
	};
	
	  /***************/
	 /** Funciones **/
	/***************/
	
	$scope.buscarCatTipoNotifia = function(){
		CatalogoService.buscaCatTipoNotifica().success(function(data){
			$scope.listaComboNotifica.lista = data;
			$scope.listaComboNotifica.valor = $scope.listaComboNotifica.lista[0];
			$('#select2-comboTipoNotificacion-container').text($scope.listaComboNotifica.valor.nombre);
		}).error(function(err){
			
		});
	}
	
	$scope.toggleTipoAsignacion = function(){
		
		$scope.toggleTipoNotificacion = !$scope.toggleTipoNotificacion;
	};
	
	$scope.abrirModalDispositivo=function(){
		$scope.showModalAsignacion = true;
		
		if($scope.prevToggle != $scope.toggleTipoNotificacion)
		{
			$scope.bandera.flag=true;
			$scope.listaNotificaDispositivos.list = [];
			$scope.prevToggle = $scope.toggleTipoNotificacion;
			
			if(!$scope.toggleTipoNotificacion){
				//NotificacionService.buscarAgrupamiento(4).success(function(data){
				NotificacionService.buscarNotificacionDispositivos().success(function(data){
					$scope.bandera.flag=true;
					var tempLista = {
							titulo:$scope.translate.dispo,
							items:[]
					};
					
					tempLista.items = data.map(function(el, idx){
						return {
							idItem:el.id,
							srItem:el.serial,
							txtItem:el.nombre
						};
					});
					
					$scope.listaNotificaDispositivos.list.push({
						titulo: $scope.translate.dispo,
						items: tempLista.items
					});
					
					$scope.listaNotificaDispositivos.list.push({
						titulo: $scope.translate.actua,
						items:[]
					});
				}).error(function(err){
					console.log("error");
				});
			}else{
				NotificacionService.buscarAgrupamiento(1).success(function(data){
					$scope.listaGrupos.lista = data;
					//$scope.listaGrupos.lista.unshift({id:0, nombre:"Seleccione"});
					//$scope.listaGrupos.valor = $scope.listaGrupos.lista[0];
					$scope.listaGrupos.valor = {id:0, nombre:$scope.translate.seleccione};
					$('#select2-comboFiltroMonitoreo-container').text($scope.listaGrupos.valor.nombre);
				}).error(function(err){
					console.log("error");
				});
			}
		}
	}
	
	$scope.buscarDispositivoEnGrupo=function(id){
		
		$scope.bandera.flag=true;
		
		$scope.listaNotificaDispositivos.list = [];
		
		if(id != undefined){
			$scope.bandera.flag=true;
			
			NotificacionService.buscarDispositivoEnGrupoParaNotificacion(id).success(function(data){
				if(data.length > 0){
					var tempLista = {
							titulo:$scope.translate.dispo,
							items:[]
					};
					
					tempLista.items = data.map(function(el, idx){
						return {
							idItem:el.id,
							srItem:el.serial,
							txtItem:el.nombre
						};
					});
					
					$scope.bandera.flag=true;
					
					$scope.listaNotificaDispositivos.list.push({
						titulo: $scope.translate.dispo,
						items: tempLista.items
					});
					
					$scope.listaNotificaDispositivos.list.push({
						titulo: $scope.translate.actua,
						items:[]
					});
				}else{
					showAlert.error("No existen dispositivos asociados a este grupo");
				}				
			}).error(function(err){
				
			});
		}
	}
	
	$scope.guardarNotificacion=function(){
		if($scope.formAltaNotifica.$invalid){
			showAlert.requiredFields($scope.formAltaNotifica);
		}else{
			$scope.notificaVO.id = -1;
			//Acoplamos el metodo de envio como tipo en VO
			$scope.notificaVO.tipo = $scope.listaComboNotifica.valor.id;
			//Acoplamos la bandera en caso de que el envio sea inmediato
			$scope.notificaVO.isNow = $scope.toggleTipoEnvio ? true : false;
			//Acoplamos el grupo seleccionado, en caso contrario, se manda 0 en el VO
			$scope.notificaVO.grupo = $scope.listaGrupos.valor != undefined ? $scope.listaGrupos.valor.id : 0
			$scope.notificaVO.dispositivos = $scope.listaNotificaDispositivos.list.length >0 ? $scope.listaNotificaDispositivos.list[1].items.length > 0 ? [...$scope.listaNotificaDispositivos.list[1].items]: undefined:undefined;
			if($scope.notificaVO.dispositivos != undefined){
				//Acoplamos la fecha de envio en VO
				if($scope.dateFechaEnvio != undefined){
				//if($scope.dateFechaEnvio !== ""){
					$scope.notificaVO.fecha = !$scope.toggleTipoEnvio ? $scope.dateFechaEnvio.format("DD/MM/YYYY HH:mm") : null
					//$scope.notificaVO.fecha = !$scope.toggleTipoEnvio ? $scope.dateFechaEnvio : null
				}else{
					if($scope.toggleTipoEnvio){
						$scope.notificaVO.fecha = null;
					}else{
						showAlert.error("Debe de seleccionar una fecha de envio o marcar la casilla de 'Envio Inmediato'.");
					}
				}
				
				if($scope.notificaVO.fecha != null || $scope.notificaVO.isNow){
					//Enviamos
					NotificacionService.guardarNotificacion($scope.notificaVO).success(function(data){
						if(data){
							if(!$scope.notificaVO.isNow){
								showAlert.aviso("Notificación guardada con éxito")
							}else{
								showAlert.aviso("Notificación enviada con éxito")
							}
							//Limpiar
							$scope.formAltaNotifica.$setPristine();
							$scope.notificaVO = {};
							$scope.listaComboNotifica.valor = $scope.listaComboNotifica.lista[0];
							$('#select2-comboTipoNotificacion-container').text($scope.listaComboNotifica.valor.nombre);
							$scope.toggleTipoEnvio = false;
							$scope.prevToggle = undefined;
						}
					}).error(function(err){
						
					});
				}
			}else{
				showAlert.error("Debe de seleccionar al menos un dispositvo para enviar la notificación");
			}
			
		}
		
	}
	
	  /*************************/
	 /** Oyentes / Listeners **/
	/*************************/
	
	$scope.$watch("$parent.currentLanguage", function(newValue, oldValue) {
		$scope.traducir(null);
	});
	
	  /********************/
	 /** Inicializacion **/
	/********************/
	$scope.traducir();
	$scope.buscarCatTipoNotifia();
});