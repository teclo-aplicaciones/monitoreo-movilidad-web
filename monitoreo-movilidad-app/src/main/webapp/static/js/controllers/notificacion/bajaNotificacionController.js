angular.module(appTeclo).controller("bajaNotificacionController", function($scope, CatalogoService, NotificacionService, showAlert) {
	$scope.busquedaVO = {};
	
	$scope.listaCatalogo = {
		lista:[]
	}
	
	$scope.catalogosId = [3, 4];
	
	requiredFields = function(form){
		
	    
		angular.forEach(form.$error, function (field) {
            	angular.forEach(field, function(errorField){
            	errorField.$setDirty();
            })
                     
            //$scope.showAviso("Es necesario completar el formulario");
		});
	}
	
	$scope.buscarCatalogo = function(){
		CatalogoService.buscarCatalogosPorId($scope.catalogosId).success(function(data){
			$scope.listaCatalogo.lista = data;
			$scope.busquedaVO.tipoBusca = $scope.listaCatalogo.lista[0];
			$('#select2-comboBusquedaNotificacion-container').text($scope.busquedaVO.tipoBusca.nombre);
		}).error(function(err){
			console.log("error");
		});
	}
	
	$scope.consulta= function(tipo, valor){
		if($scope.formBajaNotificacion.$invalid){
			requiredFields($scope.formBajaNotificacion);
		}else{
			NotificacionService.consultaNotificaciones(tipo, valor).success(function(data){
				$scope.notificaciones = data;
			}).error(function(err){
				console.log("error");
			})
		}
	};
	
	$scope.confirmacion = function(){
		showAlert.confirmacion("¿Estas seguro de eliminar geocerca?",function(){
			showAlert.aviso("Eliminado");
		});
	}
	
	$scope.buscarCatalogo();
});