angular.module(appTeclo).controller("actualizaGrupoController", function($scope, $translate, $window, grupoVO, MonitoreoService, GrupoService, CatalogoService, showAlert){
	
	  /***************/
	 /** Variables **/
	/***************/
	
	$scope.gVO = angular.copy(grupoVO.data);
	
	$scope.grupoVO = {};
	
	$scope.listaTipoGrupos={};
	
	$scope.listaGrupoDispositivos={
			list:[]
	};	
	
	$scope.dispos={
			flag:false
	};
	
	
	
	  /******************************/
	 /** Configuracion de idiomas **/
	/******************************/
	
	$scope.traducir = function(callback){
		$translate(['GRUPO.Nuevo.listado.labelDisp', 
					'GRUPO.Nuevo.listado.labelAsig']).then(function (headline) {
		    $scope.translate ={
		    		dispo: headline['GRUPO.Nuevo.listado.labelDisp'],
		    		actua: headline['GRUPO.Nuevo.listado.labelAsig']
		    }
		    if(callback)
		    	callback()
		}, function (translationId) {
		    $scope.headline = translationId;
		});
	}
	
	  /***************/
	 /** Funciones **/
	/***************/
	
	$scope.buscarCatalogo=function(){
		CatalogoService.catalogoTipoGrupo().success(function(data){
			$scope.listaTipoGrupos.lista = data;
			$scope.listaTipoGrupos.valor = data[0];
			$("#select2-comboTipoGrupo-container").text($scope.listaTipoGrupos.valor.nombre);
			
			if($scope.gVO){
				$scope.grupoVO.id = $scope.gVO.id;
				$scope.grupoVO.nombre = $scope.gVO.nombre;
				$scope.grupoVO.descripcion = $scope.gVO.descripcion;
				
				$scope.listaTipoGrupos.valor = $scope.listaTipoGrupos.lista.find(function(el){ return el.id == $scope.gVO.tipo});
				
				$scope.cargarDispositivos();
			}
		}).error(function(err){
			console.log("error");
		});
	};
		
	
	$scope.cargarDispositivos = function(){
		$scope.listaGrupoDispositivos.list = [];
		
		GrupoService.buscarGrupoDispositivos().success(function(data){
			$scope.dispos.flag = true;
			var tempLista = {
					titulo:$scope.translate.dispo,
					items:[]
			};
			
			tempLista.items = data.map(function(el, idx){
				return {
					idItem:el.id,
					srItem:el.serial,
					txtItem:el.nombre
				};
			});
			
			$scope.listaGrupoDispositivos.list.push({
				titulo: $scope.translate.dispo,
				items: tempLista.items
			});
			
			$scope.listaGrupoDispositivos.list.push({
				titulo: $scope.translate.actua,
				items:[]
			});
			
			$scope.callbackDispositivos();
		}).error(function(err){
			console.log("error");
		});
	};
	
	$scope.callbackDispositivos = function(){
		//Ajustar lista de dispositivos que estan seleccionados
		if($scope.gVO.dispositivos.length > 0){
			$scope.dispos.flag = true;
			$scope.listaGrupoDispositivos.list[1].items = $scope.listaGrupoDispositivos.list[0].items.filter(function (el){
				var found = $scope.gVO.dispositivos.find(function(elm){
					return el.idItem == elm.idItem;
				}); 
				if(found != undefined)
					$scope.listaGrupoDispositivos.list[0].items = $scope.listaGrupoDispositivos.list[0].items.filter(function (el){
						return el.idItem != found.idItem; 
					});
				return found ? found : null;  
			});
			$scope.dispos.flag = true;
		}
	};
	
	$scope.actualizarGrupo = function(){
		if($scope.formActualizaGrupo.$invalid){
			showAlert.requiredFields($scope.formActualizaGrupo);
		}else{			
			//Acoplamos el tipo en VO
			$scope.grupoVO.tipo = $scope.listaTipoGrupos.valor.id;
			//Acoplamos el grupo seleccionado, en caso contrario, se manda 0 en el VO
			$scope.grupoVO.dispositivos = $scope.listaGrupoDispositivos.list.length >0 ? $scope.listaGrupoDispositivos.list[1].items.length > 0 ? [...$scope.listaGrupoDispositivos.list[1].items]: undefined:undefined;
			if($scope.grupoVO.dispositivos != undefined){
				//Enviamos
				GrupoService.actualizarGrupo($scope.grupoVO).success(function(data){
					if(data){
						showAlert.aviso("Grupo actualizado con éxito")
						
						//Limpiar
						$scope.formAltaGrupo.$setPristine();
						$scope.grupoVO = {};
						$scope.listaTipoGrupos.valor = $scope.listaTipoGrupos.lista[0];
						$('#select2-comboTipoGrupo-container').text($scope.listaTipoGrupos.valor.nombre);
						$scope.cargarDispositivos();
					}
				}).error(function(err){
					
				});
			}else{
				showAlert.error("Debe de seleccionar al menos un dispositvo para actualizar el grupo");
			}
		}
	};
	
	$scope.regresar = function(){
		$window.history.back();
	};
	
	$scope.traducir($scope.buscarCatalogo);
});