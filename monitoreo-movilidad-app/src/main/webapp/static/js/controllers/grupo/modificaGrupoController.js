angular.module(appTeclo).controller("modificaGrupoController", function($scope, CatalogoService, GrupoService, showAlert) {
	$scope.busquedaVO = {};
	
	$scope.listaCatalogo = {
		lista:[]
	}
	
	$scope.tipoBusqueda = {};

	$scope.init = function(){
		if(GrupoService.consultaVO.length >0){
			$scope.grupos = $scope.grupos == undefined ? GrupoService.consultaVO : undefined;
		}
		$scope.buscarCatalogo();
	}
	
	$scope.buscarCatalogo = function(){
		CatalogoService.catalogoBusqGrupo().success(function(data){
			$scope.listaCatalogo.lista = data;
			$scope.busquedaVO.tipoBusca = $scope.listaCatalogo.lista[0];
			$('#select2-comboBusquedaGrupo-container').text($scope.busquedaVO.tipoBusca.nombre);
		}).error(function(err){
			console.log("error");
		});
	}
	
	$scope.modoBusqueda = function(){	
		if($scope.busquedaVO.tipoBusca.id != 1){
			$scope.toggleTipo = true;
			GrupoService.catalogoGrupo($scope.busquedaVO.tipoBusca.id).success(function(data){
				$scope.tipoBusqueda.lista = data;
				$('#select2-comboTipoBusquedaGrupo-container').text($scope.tipoBusqueda.valor.nombre);
				$('#select2-comboTipoBusquedaGrupo-container').attr('title',$scope.tipoBusqueda.valor.nombre);
			}).error(function(err){
				console.log("error");
			});
		}else{
			$scope.toggleTipo = false;
		}
		$scope.grupos = undefined;
	}
	
	$scope.consulta= function(tipo, valor){
		if($scope.formModificaGrupo.$invalid){
			showAlert.requiredFields($scope.formModificaGrupo);
		}else{
			GrupoService.consultaGrupos(tipo.id, valor).success(function(data){
				if(data.length > 0){
					GrupoService.consultaVO = data;
					$scope.grupos = GrupoService.consultaVO;
				}else{
					showAlert.error("No se encontraron registros")
				}
			}).error(function(err){
				$scope.grupos = undefined;
				showAlert.error(err.message)
			});
		}
	};
	
	$scope.eliminarGrupo = function(grupo){
		showAlert.confirmacion("¿Estas seguro que desea eliminar '"+grupo.nombre+"'?", function(){
			GrupoService.eliminarGrupo(grupo.id).success(function(data){
				if(data){
					showAlert.aviso("El grupo '"+grupo.nombre+"' se elimino con exito.");
					$scope.grupos = $scope.grupos.filter(function(el){
						return el.id != grupo.id;
					})
				}
			}).error(function(err){
				showAlert.error(err);
			});	
		},null, null, null);
	};
	
	//Formato de fecha
	$scope.formatDate = function(date){
		return new Date(date);
	};
	
	$scope.$on('$locationChangeSuccess', function(event, current, previous) {
		var page = current.split('/')[5];
		if(page !== "modificaGrupo" && page !== "actualizaGrupo")
			GrupoService.consultaVO = []
    });
	
	$scope.init();
});