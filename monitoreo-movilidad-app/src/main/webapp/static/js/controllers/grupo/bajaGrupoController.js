angular.module(appTeclo).controller("bajaGrupoController", function($scope, CatalogoService, GrupoService) {
	$scope.busquedaVO = {};
	
	$scope.listaCatalogo = {
		lista:[]
	}
	
	$scope.catalogosId = [3, 4];
	
	requiredFields = function(form){
		
	    
		angular.forEach(form.$error, function (field) {
            	angular.forEach(field, function(errorField){
            	errorField.$setDirty();
            })
                     
            //$scope.showAviso("Es necesario completar el formulario");
		});
	}
	
	$scope.buscarCatalogo = function(){
		CatalogoService.buscarCatalogosPorId($scope.catalogosId).success(function(data){
			$scope.listaCatalogo.lista = data;
			$scope.busquedaVO.tipoBusca = $scope.listaCatalogo.lista[0];
			$('#select2-comboBusquedaGrupo-container').text($scope.busquedaVO.tipoBusca.nombre);
		}).error(function(err){
			console.log("error");
		});
	}
	
	$scope.consulta= function(tipo, valor){
		if($scope.formBajaGrupo.$invalid){
			requiredFields($scope.formBajaGrupo)
		}else{
			GrupoService.consultaGrupos(tipo, valor).success(function(data){
				$scope.grupos = data;
			}).error(function(err){
				console.log("error");
			})
		}
	};
	
	$scope.buscarCatalogo();
});