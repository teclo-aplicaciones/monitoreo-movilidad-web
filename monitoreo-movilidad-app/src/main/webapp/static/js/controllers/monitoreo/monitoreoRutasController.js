angular.module(appTeclo).controller("monitoreoRutasController", function($scope, $translate, CatalogoService, MonitoreoService, showAlert) {
	
	  /***************/
	 /** Variables **/
	/***************/
	$scope.listaComboFiltro = {
		lista:[],
		valor: null,
		buscar:""
	}
	$scope.filtroFechas={
			fechaInicio:"",
			fechaFin:""
	};
	
	$scope.toggle={
			etiqueta:'',
			valor:false
	};
	
		//limit picker
	$scope.limitStartDatePicker = {
			format: 'DD/MM/YYYY HH:mm:ss',
			minDate: moment(0),
			maxDate: $scope.filtroFechas.fechaFin ? $scope.filtroFechas.fechaFin : moment({h:23, m:59, s:59}),
			useCurrent:true
	};
	
	$scope.limitEndDatePicker = {
			format: 'DD/MM/YYYY HH:mm:ss',
			minDate: $scope.filtroFechas.fechaInicio ? $scope.filtroFechas.fechaInicio : moment(0),
			maxDate: moment({h:23, m:59, s:59}),
			useCurrent:true
	};
	
	$scope.enfocaEventoEnMapa;
	
	$scope.eventoEnMapa = function(id){
		$scope.viewMode = 0;
		$scope.enfocaEventoEnMapa(id)
	}
	
	//$scope.limitEndDatePicker.maxDate.day(1);
	//$scope.filtroFechas.fechaInicio.$setViewValue($scope.fechaActual.fecha.toLocaleDateString("en-US"));
	
	
	$scope.filtroEventos={
		evento:true,
		tiempo:true,
		info:true,
		advert:true,
		error:true
	};
	
	$scope.viewMode = 0;
	$scope.inputs = {
			comboFiltro:false,
			comboBusqueda:true,
			comboDispositivo:true,
			botonSeguimiento:true
	}
	
	$scope.listaAgrupamiento = {
			lista:[{id:"0", nombre:"Seleccione"}],
			valor: null,
	}
	$scope.listaDispositivo = {
			lista:[{id:"0", nombre:"Seleccione"}],
			valor: null,
	}
	
	$scope.toggleTipoBusquedaRuta=false;
	
	$scope.catalogosId = [1,3,4];
	
	$scope.datosRecibidos = false;
	
	$scope.copiaListaRutas = [];
	
	$scope.listaRutas ={
			info:{},
			lista:[],
			valor:[]
	}
	
	$scope.notInnerUpdate = true;
	
	/*$scope.listaTracks=
		[{
			info:{nombre:'dispositivo 1', tipo:'Movil'}, 
			events:[0, 0, 1, 0, 2, 3, 3],
			coords:[
				[19.40346583403076, -99.13655764094854],
				[19.402817573053433, -99.13106874263654],
				[19.40378996355063, -99.12738088908316],
				[19.40435718865631, -99.12360727149363],
				[19.404843380029714, -99.12120587848213],
				[19.405248538397405, -99.11837566528999],
				[19.40549163293361, -99.11365864330311]
			]
	}];*/
	
	$scope.defaultColor=/*"#2196F3";*/ "#FF0000";
	
	  /******************************/
	 /** Configuracion de idiomas **/
	/******************************/
	
	$scope.traducir = function(callback){
		$translate(['MONITOREO.rutas.nombreBox.filtrado.seleccionar',
					'MONITOREO.tiempoReal.nombreBox.seguimiento.botonH', 
					'MONITOREO.tiempoReal.nombreBox.seguimiento.botonD']).then(function (headline) {
		    $scope.translate ={
		    		seleccione: headline['MONITOREO.rutas.nombreBox.filtrado.seleccionar'],
		    		habilita: headline['MONITOREO.tiempoReal.nombreBox.seguimiento.botonH'],
		    		deshabilita: headline['MONITOREO.tiempoReal.nombreBox.seguimiento.botonD']
		    }
		    if(callback)
		    	callback()
		}, function (translationId) {
		    $scope.headline = translationId;
		});
	}
	
	  /***************/
	 /** Funciones **/
	/***************/
	
	$scope.buscarCatalogos = function(){
		CatalogoService.catalogoMonitoreo().success(function(data){
			$scope.listaComboFiltro.lista = data;
			//$scope.listaComboFiltro.lista.unshift({id:0, nombre:"Seleccione"});
			$scope.listaComboFiltro.valor = {id:-1, nombre:$scope.translate.seleccione};
			//$scope.listaAgrupamiento.valor = $scope.listaAgrupamiento.lista[0];
			//$scope.listaDispositivo.valor = $scope.listaDispositivo.lista[0];
		}).error(function(err){
			console.log("error");
		});
		
		$scope.listaAgrupamiento.valor = {id:0, nombre:$scope.translate.seleccione};
	}
	
	$scope.toggleSeguimiento=function(event){
		$scope.traducir();
		if(event != undefined){
			if($scope.toggle.valor && event != undefined){
				event.currentTarget.textContent=$scope.translate.habilita;
			}else{
				event.currentTarget.textContent=$scope.translate.deshabilita;
			}
			$scope.botonSeguimiento = event;	
			
			$scope.toggle.valor= !$scope.toggle.valor;
		}
		
	}
	
	$scope.cargaListaAgrupada = function(tipo){
		if(tipo !==undefined){
			MonitoreoService.buscarAgrupamiento(tipo.id).success(function(data){
				if(tipo.id != 3){
					$scope.listaAgrupamiento.lista = data;
					//$scope.listaAgrupamiento.lista.unshift({id:0, nombre:"Seleccione"});
					$scope.listaAgrupamiento.valor = {id:-1, nombre:$scope.translate.seleccione};
					$('select2-comboBusquedaRutas-container').text($scope.listaAgrupamiento.lista[0].nombre);
				}else{
					$scope.listaDispositivo.lista = data;
					//$scope.listaDispositivo.lista.unshift({id:0, nombre:"Seleccione"});
					$scope.listaDispositivo.valor = {id:-1, nombre:$scope.translate.seleccione};
				}
				
				var comboDispositivoRutas = $("#select2-comboDispositivoRutas-container");
				comboDispositivoRutas.attr('title','​Seleccione');
				comboDispositivoRutas.text($scope.translate.seleccione);
				
				$scope.listaAgrupamiento.valor = {id:-1, nombre:$scope.translate.seleccione};
				
				var comboBuscaComando = $("#select2-comboBusquedaRutas-container");
				comboBuscaComando.attr('title','​Seleccione');
				comboBuscaComando.text($scope.translate.seleccione);
				
				
			}).error(function(err){
				
			});
			$scope.inputs.comboBusqueda= tipo.id !=0 && tipo.id !=3 ? false : true;
			$scope.inputs.comboDispositivo= tipo.id == 3 ? false : true;
			$scope.listaDispositivo.valor = {id:-1, nombre:$scope.translate.seleccione};
		}else{
			$scope.listaAgrupamiento.valor = {id:-1, nombre:$scope.translate.seleccione};
			
			var comboBusquedaRutas = $("#select2-comboBusquedaRutas-container");
			comboBusquedaRutas.attr('title','​Seleccione');
			comboBusquedaRutas.text($scope.translate.seleccione);
			
			var comboDispositivoRutas = $("#select2-comboDispositivoRutas-container");
			comboDispositivoRutas.attr('title','​Seleccione');
			comboDispositivoRutas.text($scope.translate.seleccione);
			
			$scope.inputs.comboBusqueda= true;
			$scope.inputs.comboDispositivo= true;
		}	
	}
	
	$scope.cargaListaDispositivos = function(asignacion){
		if(asignacion){
			if($scope.listaComboFiltro.valor.id == 1){
				MonitoreoService.buscarDispositivosEnGrupo(asignacion.id).success(function(data){
					if(data.length >0){
						$scope.listaDispositivo.lista = data; 
						//$scope.listaDispositivo.lista.unshift({id:0, nombre:"Seleccione"});
						$scope.listaDispositivo.valor = {id:-1, nombre:$scope.translate.seleccione};
					}else{
						$scope.inputs.comboDispositivo = true;
						showAlert.error('No existen dispositivos asociados a este grupo');
					}
				}).error(function(err){
					
				});
			}else if($scope.listaComboFiltro.valor.id == 2){
				MonitoreoService.buscarDispositivosEnGeocerca(asignacion.id).success(function(data){
					if(data.length > 0){
						$scope.listaDispositivo.lista = data; 
						//$scope.listaDispositivo.lista.unshift({id:0, nombre:"Seleccione"});
						$scope.listaDispositivo.valor = {id:-1, nombre:$scope.translate.seleccione};
					}else{
						$scope.inputs.comboDispositivo = true;
						showAlert.error('No existen dispositivos asociados a esta geocerca');
					}
				}).error(function(err){
					
				});
			}else if($scope.listaComboFiltro.valor.id == 4){
				MonitoreoService.buscarDispositivosEnUsuario(asignacion.id).success(function(data){
					$scope.listaDispositivo.lista = data; 
					//$scope.listaDispositivo.lista.unshift({id:0, nombre:"Seleccione"});
					$scope.listaDispositivo.valor = {id:-1, nombre:$scope.translate.seleccione};
				}).error(function(err){
					
				});
			}
		}else{
			$scope.listaDispositivo.valor = {id:-1, nombre:$scope.translate.seleccione};
		}
		
		var comboDispositivoRutas = $("#select2-comboDispositivoRutas-container");
		comboDispositivoRutas.attr('title','​Seleccione');
		comboDispositivoRutas.text($scope.translate.seleccione);
		
		$scope.inputs.comboDispositivo= asignacion ? false : true;
	}
	
	$scope.buscarRutaDispositivo = function(){
		$scope.datosRecibidos = false;
		if($scope.listaDispositivo.valor && $scope.listaDispositivo.valor.id != -1 && $scope.filtroFechas.fechaInicio && $scope.filtroFechas.fechaFin){
			var fInicio = $scope.filtroFechas.fechaInicio ? $scope.toggleTipoBusquedaRuta ? $scope.filtroFechas.fechaInicio.format("D/MM/YYYY"):$scope.filtroFechas.fechaInicio.format("D/MM/YYYY HH:mm:ss") : "";
			var fFin = $scope.filtroFechas.fechaFin ? $scope.toggleTipoBusquedaRuta ? $scope.filtroFechas.fechaFin.format("D/MM/YYYY"): $scope.filtroFechas.fechaFin.format("D/MM/YYYY HH:mm:ss"): "";
			MonitoreoService.buscarRutaDispositivo($scope.listaDispositivo.valor.id, $scope.toggleTipoBusquedaRuta?1:0,fInicio, fFin).success(function(data){
				if(data.rutas.length >0){
					$scope.filtroEventos.evento = true;
					$scope.filtroEventos.tiempo = true;
					$scope.filtroEventos.info = true;
					$scope.filtroEventos.advert = true;
					$scope.filtroEventos.error = true;
					$scope.listaRutas.valor = [];
					$scope.copiaListaRutas = null;
					$scope.listaRutas.lista = data.rutas;
					$scope.listaRutas.info.nombre = data.nombre;
					$scope.listaRutas.valor.push($scope.listaRutas.lista[0]);
					$scope.copiaListaRutas = angular.copy($scope.listaRutas.valor);
					//COPIAMOS TODO, PERO LE AGREGAMOS SU IDENTIFICADOR UNICO, PARA EVITAR USAR EL $INDEX
					$scope.copiaListaRutas[0].events = $scope.copiaListaRutas[0].events.map(function(el,idx){
						var x = el;
						x.key = idx;
						return x;
					});
					$scope.datosRecibidos = fInicio != "" && fFin != "" ? true:false;
				}else{
					showAlert.aviso("No se encontro registros historicos");
				}
			}).error(function(err){
				
			});
		}
	};
	
	$scope.cargaPuntoPorDia = function(id){
		$scope.listaRutas.valor = [];
		$scope.copiaListaRutas = null;
		$scope.filtroEventos.evento = true;
		$scope.filtroEventos.tiempo = true;
		$scope.filtroEventos.info = true;
		$scope.filtroEventos.advert = true;
		$scope.filtroEventos.error = true;
		$scope.listaRutas.valor.push($scope.listaRutas.lista[id]);
	}
	
	$scope.buscarPorTipoRuta = function(){
		$scope.toggleTipoBusquedaRuta = !$scope.toggleTipoBusquedaRuta;
		$scope.copiaListaRutas = null;
		$scope.buscarRutaDispositivo();
	}
	
	$scope.generarExcel = function(){
		var fInicio = $scope.filtroFechas.fechaInicio ? $scope.toggleTipoBusquedaRuta ? $scope.filtroFechas.fechaInicio.format("D/MM/YYYY"):$scope.filtroFechas.fechaInicio.format("D/MM/YYYY HH:mm:ss") : "";
		var fFin = $scope.filtroFechas.fechaFin ? $scope.toggleTipoBusquedaRuta ? $scope.filtroFechas.fechaFin.format("D/MM/YYYY"): $scope.filtroFechas.fechaFin.format("D/MM/YYYY HH:mm:ss"): "";
		MonitoreoService.generarExcel($scope.listaDispositivo.valor.id, $scope.toggleTipoBusquedaRuta?1:0,fInicio, fFin).success(function(data, status, headers){
			var  filename  = headers('filename');
	     	var contentType = headers('content-type');
		 	var file = new Blob([data], {type: 'application/vnd.ms-excel;base64,'});
		 	MonitoreoService.descargarArchivo(file, filename);
		}).error(function(err){
			
		});
	}
	
	$scope.aplicaFiltroEvento = function(){
		if($scope.copiaListaRutas == null){
			$scope.copiaListaRutas = $scope.listaRutas.valor;
		}
		
		var ruta = angular.copy($scope.copiaListaRutas);
		var lista = ruta[0].events;
		
		var listaFiltrada = lista.map(function(el, idx){
			if($scope.filtroEventos.evento){
				if(idx != 0 && idx != lista.length-1)
					if($scope.filtroEventos.tiempo && el.id == 0 || idx == 0 || idx == lista.length-1)
						return el;
					if($scope.filtroEventos.info && el.id == 1 || idx == 0 || idx == lista.length-1)
						return el;
					if($scope.filtroEventos.advert && el.id == 2 || idx == 0 || idx == lista.length-1)
						return el;
					if($scope.filtroEventos.error && el.id == 3 || idx == 0 || idx == lista.length-1)
						return el;
			}else{
				return null;
			}
		});
		
		ruta[0].events = listaFiltrada;
		
		$scope.listaRutas.valor = ruta;
	}
	
	$scope.seguimiento = function(){
		if($scope.listaDispositivo.valor){
			if($scope.listaDispositivo.valor.id != -1){
				$scope.inputs.botonSeguimiento = false;
			}else{
				$scope.inputs.botonSeguimiento = true;
				if($scope.toggle.valor)
					$scope.toggleSeguimiento($scope.botonSeguimiento);
			}
		}
	};
	
	  /*************************/
	 /** Oyentes / Listeners **/
	/*************************/
	
	$scope.$watch("$parent.currentLanguage", function(newValue, oldValue) {
		$scope.traducir(function(){
			if($scope.toggle.valor){
				$scope.toggle.etiqueta= $scope.translate.deshabilita;
			}else{
				$scope.toggle.etiqueta= $scope.translate.habilita;
			}
		});
	});
	
	$scope.$watch('listaDispositivo.valor', function(newOne, oldOne){
		$scope.seguimiento();
	});
	
	$scope.$watch("filtroFechas.fechaInicio", function(newValue, oldValue){
		$scope.limitEndDatePicker = {
				format: 'DD/MM/YYYY HH:mm:ss',
				minDate: $scope.filtroFechas.fechaInicio ? $scope.filtroFechas.fechaInicio : moment(0),
				//maxDate: $scope.filtroFechas.fechaFin ? $scope.filtroFechas.fechaFin.set({'hour':0, 'minute':0,'second':0}) : moment({h:0, m:0, s:0}).add(1, 'day'),
				//maxDate: moment({h:0, m:0, s:0}).add(1, 'day'),
				maxDate: moment({h:23, m:59, s:59}),
				defaultDate: moment()
		};
		
	});
	
	$scope.$watch("filtroFechas.fechaFin", function(newValue, oldValue){
		if($scope.notInnerUpdate){
			if(oldValue && newValue){
				if(oldValue.hour() == newValue.hour() && 
						oldValue.minute() == newValue.minute() && 
							oldValue.second() == newValue.second() && 
								oldValue.day() != newValue.day()){
					/*var defDate = angular.copy($scope.filtroFechas.fechaFin);
					defDate = defDate.set({'hour':0, 'minute':0,'second':0});
					
					//defDate;*/
					
					$scope.limitEndDatePicker = {
							format: 'DD/MM/YYYY HH:mm:ss',
							minDate: $scope.filtroFechas.fechaInicio ? $scope.filtroFechas.fechaInicio : moment(0),
							//maxDate: $scope.filtroFechas.fechaFin ? $scope.filtroFechas.fechaFin.set({'hour':0, 'minute':0,'second':0}) : moment({h:0, m:0, s:0}).add(1, 'day'),
							//maxDate: moment({h:0, m:0, s:0}).add(1, 'day'),
							maxDate: moment({h:23, m:59, s:59}),
							defaultDate: moment()
					};
					
					//$scope.filtroFechas.fechaFin = $scope.filtroFechas.fechaFin.set({'hour':23, 'minute':59,'second':59});
					$scope.filtroFechas.fechaFin = moment({d:$scope.filtroFechas.fechaFin.date(),
														   M:$scope.filtroFechas.fechaFin.month(),
														   y:$scope.filtroFechas.fechaFin.year(),
														   h:23,
														   m:59,
														   s:59});
					$scope.notInnerUpdate = false;
				}
			}else{
				var ddate =  $scope.filtroFechas.fechaFin ? angular.copy($scope.filtroFechas.fechaFin):moment({h:0, m:0, s:0});
				var mdate = $scope.filtroFechas.fechaFin ? angular.copy($scope.filtroFechas.fechaFin) : moment();
				$scope.limitStartDatePicker = {
						format: 'DD/MM/YYYY HH:mm:ss',
						minDate: moment(0),
						defaultDate: $scope.filtroFechas.fechaFin ? ddate.toDate() : ddate.toDate(),
						maxDate: $scope.filtroFechas.fechaFin ? mdate.add(1, 'second').toDate() : mdate.toDate()
				};
			}
			
			//notInnerUpdate = !notInnerUpdate ? true: false;
		}else{ $scope.notInnerUpdate = true; }
	});
	
	  /********************/
	 /** Inicializacion **/
	/********************/
	$scope.traducir($scope.buscarCatalogos);
});