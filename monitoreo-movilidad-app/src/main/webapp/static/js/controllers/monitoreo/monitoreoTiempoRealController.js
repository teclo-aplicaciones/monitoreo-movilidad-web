angular.module(appTeclo).controller("monitoreoTiempoRealController", function($scope, $interval, $translate, MonitoreoService, CatalogoService, NotificacionService,ComandoService, showAlert, growl,constante) {
	  /***************/
	 /** Variables **/
	/***************/
	
	
	$scope.showModalTRNotifica = false;
	$scope.notificaVO = {};
	//$scope.botonSeguimiento;
	$scope.listaComboFiltro = {
		lista:[],
		valor: null
	}
	//$scope.MONITOREO={};
	$scope.timer;
	$scope.inputs = {
			comboFiltro:false,
			comboBusqueda:true,
			comboDispositivo:true,
			botonSeguimiento:true,
			botonFrequencia:true
	}
	
	$scope.listaComboNotifica = {};
	
	$scope.detalleDispositivo = {};
	$scope.loaderFreq = {
		enabled:false
	};

	$scope.listaTracks = [];
	
	/*$scope.listaTracks=
		[{
			nombre:'dispositivo 1', 
			tipo:'Movil', 
			coords:[
				[19.40346583403076, -99.13655764094854],
				[19.402817573053433, -99.13106874263654],
				[19.40378996355063, -99.12738088908316],
				[19.40435718865631, -99.12360727149363],
				[19.404843380029714, -99.12120587848213],
				[19.405248538397405, -99.11837566528999],
				[19.40549163293361, -99.11365864330311]
			]
	},{
		nombre:'12345678901234567890123456789012345678901234567890', 
		tipo:'Automovil', 
		coords:[
			[19.40346583403076, -99.13655764094854]
		]
	}];*/
	
	$scope.listaFrequencia={
		lista:[
			{id:0, nombre:"1 seg", tiempo:1000},
			{id:1, nombre:"5 seg", tiempo:5000},
			{id:2, nombre:"10 seg", tiempo:10000},
			{id:3, nombre:"30 seg", tiempo:30000},
			{id:3, nombre:"1 min", tiempo:60000},
			{id:3, nombre:"3 mins", tiempo:180000},
		],
		valor:{}
	};
	
	$scope.listaAgrupamiento = {
			lista:[{id:"0", nombre:"Seleccione"}],
			valor: null,
	}
	$scope.listaDispositivo = {
			lista:[{id:"0", nombre:"Seleccione"}],
			valor: null,
	}
	
	$scope.defaultColor = '#2196F3';
	
	$scope.toggle={
			etiqueta:'',
			valor:false
	};
	
	  /******************************/
	 /** Configuracion de idiomas **/
	/******************************/
	
	$scope.traducir = function(callback){
		$translate(['MONITOREO.tiempoReal.nombreBox.seguimiento.botonH', 
					'MONITOREO.tiempoReal.nombreBox.seguimiento.botonD',
					'MONITOREO.tiempoReal.nombreBox.detalle.titulo',
					'MONITOREO.tiempoReal.nombreBox.filtracion.seleccionar',
					'MONITOREO.tiempoReal.nombreBox.frequencia.seleccionar']).then(function (headline) {
		    $scope.translate ={
		    		seleccione: headline['MONITOREO.tiempoReal.nombreBox.filtracion.seleccionar'],
		    		frequenciaSelec: headline['MONITOREO.tiempoReal.nombreBox.frequencia.seleccionar'],
		    		titulo:headline['MONITOREO.tiempoReal.nombreBox.detalle.titulo'],
		    		habilita: headline['MONITOREO.tiempoReal.nombreBox.seguimiento.botonH'],
		    		deshabilita: headline['MONITOREO.tiempoReal.nombreBox.seguimiento.botonD']
		    }
		    if(callback)
		    	callback()
		}, function (translationId) {
		    $scope.headline = translationId;
		});
	}
	
	
	  /***************/
	 /** Funciones **/
	/***************/
	
	$scope.buscarCatalogos = function(){
		CatalogoService.catalogoMonitoreo().success(function(data){
			$scope.listaComboFiltro.lista = data;
			//$scope.listaComboFiltro.lista.unshift({id:0, nombre:"Seleccione"});
			$scope.listaComboFiltro.valor = {id:0, nombre:$scope.translate.seleccione};
			//$scope.listaAgrupamiento.valor = $scope.listaAgrupamiento.lista[0];
			//$scope.listaDispositivo.valor = $scope.listaDispositivo.lista[0];
			CatalogoService.buscaCatTipoNotifica().success(function(data){
				$scope.listaComboNotifica.lista = data;
				$scope.listaComboNotifica.valor = $scope.listaComboNotifica.lista[0];
				$('#select2-comboTipoNotificacion-container').text($scope.listaComboNotifica.valor.nombre);
			}).error(function(err){
				
			});
		}).error(function(err){
			console.log("error");
		});
	}
	
	$scope.cargaListaAgrupada = function(tipo){
		$scope.configuraTimer();
		if(tipo !==undefined){
			MonitoreoService.buscarAgrupamiento(tipo.id).success(function(data){
				if(tipo.id != 3){
					$scope.listaAgrupamiento.lista = data;
					$scope.listaAgrupamiento.lista.unshift({id:0, nombre:"Todos"});
					$scope.listaAgrupamiento.valor = {id:-1, nombre:$scope.translate.seleccione};
					$('select2-comboFiltroMonitoreo-container').text($scope.listaAgrupamiento.lista[0].nombre);
					//$scope.listaDispositivo.valor = {id:0, nombre:"Seleccione"};
				}else{
					$scope.listaDispositivo.lista = data;
					$scope.listaDispositivo.lista.unshift({id:0, nombre:"Todos"});
					$scope.listaDispositivo.valor = {id:-1, nombre:$scope.translate.seleccione};
				}
				var comboFiltroDispositivo = $("#select2-comboFiltroDispositivo-container");
				comboFiltroDispositivo.attr('title','​Seleccione');
				comboFiltroDispositivo.text($scope.translate.seleccione);
				
				$scope.listaAgrupamiento.valor = {id:-1, nombre:$scope.translate.seleccione};
				
				var comboFiltroAgrupamiento = $("#select2-comboFiltroAgrupamiento-container");
				comboFiltroAgrupamiento.attr('title','​Seleccione');
				comboFiltroAgrupamiento.text($scope.translate.seleccione);
			}).error(function(err){
				
			});
			$scope.inputs.comboBusqueda= tipo.id !=0 && tipo.id !=3 ? false : true;
			$scope.inputs.comboDispositivo= tipo.id == 3 ? false : true;
			$scope.listaDispositivo.valor = {id:-1, nombre:$scope.translate.seleccione};
		}else{
			
			$scope.listaAgrupamiento.valor = {id:-1, nombre:$scope.translate.seleccione};
			
			var comboFiltroAgrupamiento = $("#select2-comboFiltroAgrupamiento-container");
			comboFiltroAgrupamiento.attr('title','​Seleccione');
			comboFiltroAgrupamiento.text($scope.translate.seleccione);
			
			var comboFiltroDispositivo = $("#select2-comboFiltroDispositivo-container");
			comboFiltroDispositivo.attr('title','​Seleccione');
			comboFiltroDispositivo.text($scope.translate.seleccione);
			
			$scope.inputs.comboBusqueda= true;
			$scope.inputs.comboDispositivo= true;
		}
	}
	
	$scope.cargaListaDispositivos = function(asignacion){
		$scope.configuraTimer();
		if(asignacion){
			if($scope.listaComboFiltro.valor.id == 1){
				MonitoreoService.buscarDispositivosEnGrupo(asignacion.id).success(function(data){
					if(data.length > 0){
						$scope.listaDispositivo.lista = data; 
						$scope.listaDispositivo.lista.unshift({id:0, nombre:"Todos"});
						$scope.listaDispositivo.valor = {id:-1, nombre:$scope.translate.seleccione};
					}else{
						$scope.inputs.comboDispositivo = true;
						showAlert.error('No existen dispositivos asociados a este grupo');
					}
				}).error(function(err){
					
				});
			}else if($scope.listaComboFiltro.valor.id == 2){
				MonitoreoService.buscarDispositivosEnGeocerca(asignacion.id).success(function(data){
					if(data.length > 0){
						$scope.listaDispositivo.lista = data; 
						$scope.listaDispositivo.lista.unshift({id:0, nombre:"Todos"});
						$scope.listaDispositivo.valor = {id:-1, nombre:$scope.translate.seleccione};
					}else{
						$scope.inputs.comboDispositivo = true;
						showAlert.error('No existen dispositivos asociados a esta geocerca');
					}
				}).error(function(err){
					
				});
			}else if($scope.listaComboFiltro.valor.id == 4){
				MonitoreoService.buscarDispositivosEnUsuario(asignacion.id).success(function(data){
					$scope.listaDispositivo.lista = data; 
					$scope.listaDispositivo.lista.unshift({id:0, nombre:"Todos"});
					$scope.listaDispositivo.valor = {id:-1, nombre:$scope.translate.seleccione};
				}).error(function(err){
					
				});
			}
		}else{
			$scope.listaDispositivo.valor = {id:-1, nombre:$scope.translate.seleccione};
		}
		
		var comboFiltroDispositivo = $("#select2-comboFiltroDispositivo-container");
		comboFiltroDispositivo.attr('title','​Seleccione');
		comboFiltroDispositivo.text($scope.translate.seleccione);

		$scope.inputs.comboDispositivo = asignacion ? asignacion.id != -1 ? false : true: true;
	}
	
	$scope.toggleSeguimiento=function(event){
		$scope.traducir();
		if(event != undefined){
			if($scope.toggle.valor && event != undefined){
				event.currentTarget.textContent=$scope.translate.habilita;
			}else{
				event.currentTarget.textContent=$scope.translate.deshabilita;
			}
			$scope.botonSeguimiento = event;	
			
			$scope.toggle.valor= !$scope.toggle.valor;
		}
		
	}
	
	$scope.localizarDispositivo = function(){
		//$scope.toggleMultiSeguimiento();
	}
	
	$scope.toggleMultiSeguimiento = function(){
		if($scope.listaDispositivo.valor){
			if($scope.listaDispositivo.valor.id != -1){
				$scope.inputs.botonSeguimiento = false;
				$scope.inputs.botonFrequencia = false;
			}else{
				$scope.inputs.botonSeguimiento = true;
				$scope.inputs.botonFrequencia = true;
				if($scope.toggle.valor)
					$scope.toggleSeguimiento($scope.botonSeguimiento);
			}
		}
	};
	
	$scope.cargarDetalle = function(id){
		if($scope.listaDispositivo.valor){
			$scope.showModalDetalle = true;
			MonitoreoService.cargarDetalleDispositivo(id).success(function(data){
				$scope.dispDetalle = data;
				/*var mnt = moment($scope.dispDetalle.actividad, "DD-MM-YYYY");
				mnt.locale('es');
				$scope.dispDetalle.actividad = mnt.fromNow();*/
			}).error(function(error){
				showAlert.error(error)
			});
		}
		
	};
	
	$scope.cargarNotificacion = function(id){
		$scope.showModalTRNotifica = true;
		$scope.idDispToSend = id;
	};
	
	
	$scope.enviarNotificacionRapida = function(){
		$scope.notificaVO.id = -1;
		$scope.notificaVO.tipo = $scope.listaComboNotifica.valor.id;
		$scope.notificaVO.isNow = true;
		$scope.notificaVO.grupo = 0;
		$scope.notificaVO.dispositivos=[
			{idItem:$scope.idDispToSend, srItem:"", txtItem:""}
		];
		
		NotificacionService.guardarNotificacion($scope.notificaVO).success(function(data){
			if(data){
				showAlert.aviso("Notificación enviada con éxito")
				//Limpiar
				$scope.showModalTRNotifica = false;
				$scope.notificaVO = {};
				$scope.listaComboNotifica.valor = $scope.listaComboNotifica.lista[0];
				$('#select2-comboTipoNotificacion-container').text($scope.listaComboNotifica.valor.nombre);
			}
		}).error(function(err){
			
		});
	};

	
	$scope.configuraTimer=function(opcion){
		
		if($scope.timer){
			$interval.cancel($scope.timer);
			$scope.timer = null;
			$scope.inputs.botonFrequencia = false;
		}
		
		if(opcion){
			$scope.intervalTime = opcion.tiempo;
			$scope.timer = $interval($scope.localizar, 1000);
			$scope.inputs.botonFrequencia = true;
		}else{
			$scope.listaFrequencia.valor={id:0, nombre:$scope.translate.frequenciaSelec};
			var inputFrequencia = $("#select2-inputFrequencia-container");
			inputFrequencia.attr('title',$scope.translate.frequenciaSelec);
			inputFrequencia.text($scope.translate.frequenciaSelec);
		}
	}
	
	$scope.preparaLocalizar=function(disp){
		$scope.configuraTimer();
		$scope.localizar(disp);
	}
	
	$scope.localizar=function(disp){
		var dataToSend = [];
		
		if(disp){
			if(disp.id)
			{
				if(disp.id != 0){
					dataToSend.push(disp.id)
				}else{
					if(disp.id != -1)
						dataToSend = $scope.listaDispositivo.lista.map(function(el){ return el.id });
				}
			}else{
				disp = {id:-1};
				disp.id = $scope.listaDispositivo.valor.id;
				
				if(disp.id != 0){
					dataToSend.push(disp.id)
				}else{
					if(disp.id != -1)
						dataToSend = $scope.listaDispositivo.lista.map(function(el){ return el.id });
				}
			}
			
			if(disp.id != -1)
				if($scope.intervalTime-1000 != 0 && $scope.listaFrequencia.valor.id != 0){
				var duration = moment.duration($scope.intervalTime-1000,'milliseconds');
				var h = duration.hours().toString().length==1?"0"+duration.hours().toString():duration.hours();
				var m = duration.minutes().toString().length ==1?"0"+duration.minutes().toString():duration.minutes();
				var s = duration.seconds().toString().length ==1?"0"+duration.seconds().toString():duration.seconds();
				$('#select2-inputFrequencia-container').text(h + ":" + m + ":" + s);
				$scope.intervalTime = $scope.intervalTime-1000;
				}else{
					$scope.loaderFreq.enabled= true;
					MonitoreoService.localiza(dataToSend).success(function(data){
						if(data.length>1){
							$scope.listaTracks=data;
						}else if(data.length == 1){
							if(data[0].coords.length>0){
								$scope.listaTracks=data;
							}else{
								growl.error('No hay registros del dispositivo');
								$scope.inputs.botonSeguimiento = true;
								//$scope.inputs.botonSeguimiento = true;
							}
						}
						$scope.loaderFreq.enabled= false;
					}).error(function(err){
						console.log(err)
					});
					//Eliminar el bloqueado de la aplicacion al hacer request
					$scope.intervalTime = $scope.listaFrequencia.valor.tiempo ? $scope.listaFrequencia.valor.tiempo +1000 : undefined;
					/*var duration = moment.duration($scope.intervalTime,'milliseconds');
					var h = duration.hours().toString().length==1?"0"+duration.hours().toString():duration.hours();
					var m = duration.minutes().toString().length ==1?"0"+duration.minutes().toString():duration.minutes();
					var s = duration.seconds().toString().length ==1?"0"+duration.seconds().toString():duration.seconds();
					$('#select2-inputFrequencia-container').text(h + ":" + m + ":" + s);*/
				}
		}
	}

	
	  /*************************/
	 /** Oyentes / Listeners **/
	/*************************/
	
	$scope.$watch('listaDispositivo.valor', function(oldEl, newEl){
		$scope.toggleMultiSeguimiento();	
	});
	
	$scope.$watch("$parent.currentLanguage", function(newValue, oldValue) {
		$scope.traducir(function(){
			if($scope.toggle.valor){
				$scope.toggle.etiqueta= $scope.translate.deshabilita;
			}else{
				$scope.toggle.etiqueta= $scope.translate.habilita;
			}
		});
	});
	
	$scope.$on('$locationChangeSuccess', function(event, current, previous) {
		$interval.cancel($scope.timer);
		$scope.timer = null;
    });
	
	
	  /********************/
	 /** Inicializacion **/
	/********************/
	$scope.buscarCatalogos();
	$scope.traducir();
	
	/**
	 * Comandos
	 */
	
	$scope.viewMode = 0;
	$scope.logComando=[];
	$scope.showTabs=false;	
	$scope.currentId=0;
	$scope.urlApi = "//"+window.location.host+constante.urlWs+"comandoArchivo?idArchivo=";
	
	/*
	$scope.exec = function(idCommand){		
		ComandoService.execCommand(
				$scope.listaDispositivo.valor.id,
				idCommand			
			).success(function(data){
			growl.success($scope.mensajeModal('Se ejecuto el comando exitosamente'));
		}).error(function(err){
			growl.error($scope.mensajeModal('Error al ejecutar comando'));
		});
	}
	*/
	
	var lockExec = false;
	var timeExec = 10000;
	$scope.exec = function(idCommand){
		if(lockExec){
			growl.warning($scope.mensajeModal('Espere un momento, para ejecutar otro comando'));
		}else{
			ComandoService.execCommand(
					$scope.listaDispositivo.valor.id,
					idCommand			
				).success(function(data){
				growl.success($scope.mensajeModal('Se ejecuto el comando exitosamente'));
				
				lockExec=true;
				setTimeout(function(){
					lockExec=false
				},timeExec);
				
			}).error(function(err){
				growl.error($scope.mensajeModal('Error al ejecutar comando'));
			});
		}
	}
	
	$scope.logsCommand = function(){		
		ComandoService.logsCommand($scope.currentId).success(function(data){
			$scope.logComando=data;
		}).error(function(err){});
	}
		
	$scope.openModal = function(logComando){		
		angular.element("#showModalDetalle").modal('show');
		ComandoService.logsCommandDetalle(logComando.idBitacora).success(function(data){
			$scope.logComandoHeader=logComando;
			$scope.logComandoDetalle=data;
		}).error(function(err){});
	}
		
	$scope.openModalImage = function(idArchivo){
		angular.element("#showModalImagen").modal('show');
		$scope.image=$scope.urlApi+idArchivo+"&f="+Math.floor(Math.random() * 100) + 1;		
		angular.element('#imgLoading').show();
		angular.element('#imgModal').hide();
		angular.element('#imgModal').on('load', function(){
			angular.element('#imgLoading').hide();
			angular.element('#imgModal').show();
		});
	}
	
	$scope.defaultColor="#FF0000";
	$scope.openModalGpsActual = function(lattitude,longitude){		
		angular.element("#showModalGpsActual").modal('show');
		$scope.listaTracks2=[{
			coords:[
				[parseFloat(lattitude),parseFloat(longitude)]
			],			
			info:{				
				img:"\n<svg width=\"37.99999999999999\" height=\"37.99999999999999\" xmlns=\"http://www.w3.org/2000/svg\">\n <style type=\"text/css\">.st0{fill:#607D8B;}\n\t.st1{display:none;fill:none;stroke:#000000;stroke-miterlimit:10;}</style>\n\n <g>\n  <title>background</title>\n  <rect fill=\"none\" id=\"canvas_background\" height=\"40\" width=\"40\" y=\"-1\" x=\"-1\"/>\n </g>\n <g>\n  <title>Layer 1</title>\n  <rect id=\"svg_4\" height=\"27.625047\" width=\"16.125028\" y=\"1.56247\" x=\"11.062485\" stroke-width=\"1.5\" fill=\"#ffffff\"/>\n  <g id=\"svg_1\">\n   <path id=\"svg_2\" d=\"m28.302712,2.9c0,-0.4 -0.2,-1.2 -0.7,-1.7c-0.4,-0.4 -1,-0.7 -1.7,-0.7l-13.9,0c-0.7,0 -1.3,0.3 -1.7,0.7c-0.6,0.6 -0.7,1.3 -0.7,1.7c-0.4,4.2 0.3,26 0.3,26.4c0,0 0,0 0,0c0,0 0,0 0,0c0,0 0,0.2 0,0.2c0,0.9 1.7,2 1.7,2.1l0,0c1,0.8 1.7,1.4 2.2,1.8c1.1,1 1.3,1.1 1.6,1.3c0.2,0.2 0.3,0.3 0.4,0.3c0.5,0.5 1.1,0.8 1.8,1.5c0.9,0.8 0.9,1 1.3,1c0.4,0 0.7,-0.3 1.4,-0.9c0.6,-0.5 1.2,-0.9 1.7,-1.4c1.3,-1.1 1.9,-1.6 2.4,-2c1.4,-1.1 1.9,-1.5 1.9,-1.5c0.9,-0.7 1.8,-1.3 1.8,-2.1c0,-0.1 0,-0.2 0,-0.2c0.4,-2.2 0.6,-22.3 0.2,-26.5zm-12.8,-1l7,0c0.2,0 0.3,0.3 0.3,0.6c0,0.3 -0.1,0.6 -0.3,0.6l-7,0c-0.2,0 -0.3,-0.3 -0.3,-0.6c-0.1,-0.3 0.1,-0.6 0.3,-0.6zm3.5,30.6c-0.8,0 -1.5,-0.7 -1.5,-1.5c0,-0.8 0.7,-1.5 1.5,-1.5c0.8,0 1.5,0.7 1.5,1.5c0,0.8 -0.6,1.5 -1.5,1.5zm7.5,-3.8l-15.1,0l0,-24.2l15.1,0l0,24.2l0,0z\" class=\"st0\"/>\n  </g>\n  <rect id=\"svg_3\" height=\"38\" width=\"38\" class=\"st1\" y=\"225.3\" x=\"215.3\"/>\n </g>\n</svg>","ultimaFecha":"01/02/2019 19:18:11","imei":"351847090563056"
			}
		}]
	}

	$scope.cargarComando = function(id){	
		$scope.viewMode = 0;
		angular.element("#showModalComando").modal('show');
		$scope.currentId = id;
		
		ComandoService.catalogoComandos(id,1).success(function(data){			
			$scope.catalogoComandos=[];		
			for(i=0;i<data.length;i+=2){			 
				$scope.catalogoComandos.push({
					a:{
						show:true,
						data:data[i]
					},
					b:{
						show:true,
						data:data[i+1]
					}
				});			
			}
			
			for(var x of $scope.catalogoComandos){		
				x.a.show = false;			
				if(x.b.data && x.b.show){
					x.b.show = false;
				}	
			}		
			
			if($scope.listaDispositivo.valor && $scope.listaDispositivo.valor.id !== 0 ){
				ComandoService.obtenerComandosDispositivo($scope.listaDispositivo.valor.id).success(function(data){			
					for(var x of $scope.catalogoComandos){					
						
							for(var y of data){
								if(x.a.data.id === y.id){
									x.a.show = y.activo;
									x.a.model = y.activo;
								}
								if(x.b.data && x.b.data.id === y.id){
									x.b.show = y.activo;
									x.b.model = y.activo;
								}	
							}
						
					}			
				}).error(function(err){});
			}else{
				$scope.listaDispositivo.valor={id:0};			
			}	
			$scope.showTabs = true;			
		}).error(function(err){});		
		
	};	
	
	$scope.changeDeviceModel = function(self){
		var selfModel = true;
		if(self.model === 1){
			selfModel=false;
		}		
		ComandoService.activarDesactivarComandoPorUsuario($scope.listaDispositivo.valor.id,self.data.id,selfModel).success(function(data){
			for(var x of $scope.catalogoComandos){
				if(x.a.data.id === self.data.id){
					x.a.show = selfModel;
				}
				if(x.b.data && x.b.data.id === self.data.id){
					x.b.show = selfModel;
				}
			}
		}).error(function(err){});
	}
	
	
});