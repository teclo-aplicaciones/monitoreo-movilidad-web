angular.module(appTeclo).controller('perfilesController',function($scope,$translate,showAlert,growl,PerfilesService,ModalService) {
	$scope.modeEdit = false;
	
	$scope.formNuevoPerfil={};
	
	$scope.tipoBusqueda={
			lista:[],
			valor:{}
	};
	
	$scope.listaAccesosPerfil = {
			lista:[]
	};
	
	$scope.bandera = {
		isPerfilSelected:false,
		flag:false
	};
	
	$scope.isActive = false;
	
	  /******************************/
	 /** Configuracion de idiomas **/
	/******************************/
	
	$scope.traducir = function(callback){
		$translate(['ADMINISTRACION.Perfiles.menuBox.lista.activo', 
					'ADMINISTRACION.Perfiles.menuBox.lista.inactivo']).then(function (headline) {
		    $scope.translate ={
		    	lista: {
		    		activo:headline['ADMINISTRACION.Perfiles.menuBox.lista.activo'],
		    		inactivo: headline['ADMINISTRACION.Perfiles.menuBox.lista.inactivo']
		    	}
		    };
		    if(callback)
		    	callback()
		}, function (translationId) {
		    $scope.headline = translationId;
		});
	}
	
	$scope.cargarPerfiles = function(){
		PerfilesService.cargarPerfiles().success(function(data){
			$scope.tipoBusqueda.lista = data;
			//$scope.tipoBusqueda.valor = $scope.tipoBusqueda.lista[0];
			//$('select2-comboTipoBusqueda-container').text($scope.tipoBusqueda.valor);
		}).error(function(){
			
		});
	};
	
	$scope.seleccionPerfil = function(perfil){
		
		$scope.bandera.isPerfilSelected = false;
		if(perfil != undefined){
			//Consulta de todos los modulos que pueden acceder a ese modal
			PerfilesService.cargarAccesosPerfil(perfil.idPerfil).success(function(data){
				if(perfil != undefined){
					$scope.bandera.isPerfilSelected = true;
					$scope.listaAccesosPerfil.lista = [];
					//Cargar datos en Lista
					$scope.bandera.flag=true;
					$scope.isActive = data.stActivo == 1 ? true:false;
					$scope.listaAccesosPerfil.lista.push({titulo:$scope.translate.lista.inactivo, items:data.menuSinAcceso});
					$scope.listaAccesosPerfil.lista.push({titulo:$scope.translate.lista.activo, items:data.menuConAcceso});
				}else{
					$scope.bandera.isPerfilSelected = false;
				}
			}).error(function(){
				
			});
		}
	};
	
	$scope.actualizarAccesos = function(lista){
		//Extraemos el valor que fue seleccionado
		$scope.valorSeleccionado = lista.items.find(function(el){ return el.selected});
		
		if($scope.validarDNDMismaLista($scope.valorSeleccionado, lista.items)){
			//Validamos si agrego o elimino un modulo.
			if($scope.listaAccesosPerfil.lista[0].titulo === lista.listName){	
				//Si el origen proviene de aqui, SE AUTORIZO UN MODULO
				PerfilesService.asignarAccesosPerfil($scope.tipoBusqueda.valor.idPerfil, $scope.valorSeleccionado.label0, 1).success(function(data){
					if(data) 
						growl.success('Acceso a '+$scope.valorSeleccionado.label1+' autorizado',{title:"Control de Acceso"});
				}).error(function(err){
					
				});
			}
			if($scope.listaAccesosPerfil.lista[1].titulo === lista.listName){
				//Si el origen proviene de aqui, SE AUTORIZO UN MODULO
				PerfilesService.asignarAccesosPerfil($scope.tipoBusqueda.valor.idPerfil, $scope.valorSeleccionado.label0, 0).success(function(data){
					if(data)
						growl.success('Acceso a '+$scope.valorSeleccionado.label1+' revocado',{title:"Control de Acceso"});
				}).error(function(err){
					
				});
			}
		}
	};
	
	$scope.actualizaTodoAcceso = function(direccion){
		showAlert.confirmacion("¿Estas seguro que deseas realizar esta operación con todos los accesos?", function(){
			if(direccion === "ltr"){
				PerfilesService.asignarAccesosPerfil($scope.tipoBusqueda.valor.idPerfil, 0, 2).success(function(data){
					if(data) growl.success('Todo los accesos autorizado',{title:"Control de Acceso"});
				}).error(function(err){
					
				});
			}
			if(direccion === "rtl"){
				PerfilesService.asignarAccesosPerfil($scope.tipoBusqueda.valor.idPerfil, 0, 3).success(function(data){
					if(data) growl.success('Todo los accesos revocado',{title:"Control de Acceso"});
				}).error(function(err){
					
				});
			}
		}, $scope, function(){
			$scope.resetearLista();
			growl.info('Se canceló la operación',{title: 'Control de Acceso'});
		});
		
	};
	
	$scope.resetTodo = function(accesoPerfiles){
		console.log(accesoPerfiles);
		$scope.restablecer = {
				idPerfil:$scope.tipoBusqueda.valor.idPerfil,
				autorizados:accesoPerfiles[1].items,
				noAutorizados:accesoPerfiles[0].items,
		}
		
		PerfilesService.restablecerMenus($scope.restablecer).success(function(data){
			if(data) growl.success('Todo los accesos Reestablecidos',{title:"Control de Acceso"});
		}).error(function(err){
			
		});
	};
	
	$scope.validarDNDMismaLista = function(obj, lista){
		var encontrado = lista.find(function(el){
			if(el.label0 == obj.label0 && !el.selected)
				return el 
		});
		
		return encontrado != undefined ? false:true;
	};
	
	$scope.showModalNuevoPerfil = function(e){
		$scope.modeEdit=true; 
		$scope.bandera.isPerfilSelected=false;
		
		$scope.tipoBusqueda.valor = {idPerfil:0, nbPerfil:"Seleccione"};
		$('#select2-comboTipoBusqueda-container').text($scope.tipoBusqueda.valor.nbPerfil);
	};
	
	$scope.habilitaDeshabilitaPerfil= function(isActive){
		if(!isActive){
			showAlert.confirmacion("Este perfil puede estar asociado a un usuario, ¿Estas seguro que deseas realizar la operación?", function(){
				PerfilesService.habilitaDeshabilitaPerfil($scope.tipoBusqueda.valor.idPerfil,isActive).success(function(data){
					if(data)
						growl.success(isActive ? 'El perfil ha sido Habilitado':'El perfil ha sido Deshabilitado',{title:"Control de Acceso"});
				}).error(function(err){
					
				});
			}, $scope, function(){
				$scope.isActive = true;
				growl.info('Se canceló la operación',{title: 'Control de Acceso'});
			});
		}else{
			PerfilesService.habilitaDeshabilitaPerfil($scope.tipoBusqueda.valor.idPerfil,isActive).success(function(data){
				if(data)
					growl.success(isActive ? 'El perfil ha sido Habilitado' : 'El perfil ha sido Deshabilitado',{title:"Control de Acceso"});
			}).error(function(err){
				
			})
		}
	};
	
	$scope.crearPerfil=function(form){
		if(form.formNuevoPerfil.$invalid){
			showAlert.requiredFields(form.formNuevoPerfil, 'modal');
		}else{
			PerfilesService.crearPerfil(form.perfilVO).success(function(data){
				if(data)
					alert("creacion exitosa");
			}).error(function(){
				
			});
		}
	};
	
	  /*************************/
	 /** Oyentes / Listeners **/
	/*************************/
	
	$scope.$watch("$parent.currentLanguage", function(newValue, oldValue) {
		$scope.traducir(null);
	});
	
	
	$scope.traducir($scope.cargarPerfiles);
	
});
	