angular.module(appTeclo).controller('controlDispositivosHistoricoController', function($scope, $filter, $translate, MonitoreoService, consultaDispService, growl, showAlert) {

	$scope.listaDispActivos = [];
	$scope.listaDispDesactivados = [];
	$scope.listDispositivos = [];
	
	$scope.listaAnios = {};
	$scope.listaMeses = {};
	
	$scope.camposAnual = {
			grupo:{},
			dispo:{},
	}
	
	$scope.camposDispo = {};
	
	$scope.grafica = {
		grupo:{
			orderBy:false,
			vacio:[
				{tcNombre:"Grupo 1", tcConteo:"30"},
				{tcNombre:"Grupo 2", tcConteo:"50"},
				{tcNombre:"Grupo 3", tcConteo:"10"},
				{tcNombre:"Grupo 4", tcConteo:"80"},
				{tcNombre:"Grupo 5", tcConteo:"40"},
			]
		},
		dispo:{orderBy:false,
			tabla:{
				vacio:[
					{tcNombre:"Dispositivo 1", tcConteo:"30"},
					{tcNombre:"Dispositivo 2", tcConteo:"50"},
					{tcNombre:"Dispositivo 3", tcConteo:"10"},
					{tcNombre:"Dispositivo 4", tcConteo:"80"},
					{tcNombre:"Dispositivo 5", tcConteo:"40"},
				]
			},
			vacio:[
				{tcNombre:"Evento 1", tcConteo:"30"},
				{tcNombre:"Evento 2", tcConteo:"50"},
				{tcNombre:"Evento 3", tcConteo:"10"},
				{tcNombre:"Evento 4", tcConteo:"80"},
				{tcNombre:"Evento 5", tcConteo:"40"},
			]
		},
		anual:{orderBy:false,
			vacio:[
				{evento1:10, evento2:20, evento3:30},
				{evento1:9, evento2:24, evento3:32},
				{evento1:8, evento2:25, evento3:34},
				{evento1:7, evento2:25, evento3:34},
				{evento1:6, evento2:26, evento3:28},
				{evento1:6, evento2:18, evento3:26},
				{evento1:5, evento2:6, evento3:26},
				{evento1:7, evento2:2, evento3:29},
				{evento1:8, evento2:2, evento3:31},
				{evento1:10, evento2:1, evento3:32},
				{evento1:12, evento2:1, evento3:33},
				{evento1:25, evento2:1, evento3:33}
			]}
	}
	
	$scope.ctrls = 
		[
			{myValue:0, nbPanel: "Dispositivos Activos",idPanel:"dispActivo", myTarget:1, myDuration:1500, bgPanel:'bg-green', icon: "fa-mobile", myEffect:'swing'},
			{myValue:0, nbPanel: "Dispositivos Desactivados",idPanel:"dispInactivos", myTarget:2, myDuration:1500, bgPanel:'bg-aqua', icon: "fa-mobile", myEffect:'swing'},
			{myValue:0, nbPanel: "Dispositivos Totales",idPanel:"dispTotales", myTarget:3, myDuration:1500, bgPanel:'bg-red', icon: "fa-mobile", myEffect:'swing'}
		];
	
	$scope.viewMode={
			grupo:1,
			dispo:0
	};
	
	  /******************************/
	 /** Configuracion de idiomas **/
	/******************************/
	
	$scope.traducir = function(callback){
		//Traduccion de dias de la semana y meses
		$translate(['APP.Base.fechaDias.lunes', 'APP.Base.fechaDias.martes','APP.Base.fechaDias.miercoles',
					'APP.Base.fechaDias.jueves','APP.Base.fechaDias.viernes','APP.Base.fechaDias.sabado',
					'APP.Base.fechaDias.domingo','APP.Base.fechaMeses.enero','APP.Base.fechaMeses.febrero',
					'APP.Base.fechaMeses.marzo','APP.Base.fechaMeses.abril','APP.Base.fechaMeses.mayo',
					'APP.Base.fechaMeses.junio','APP.Base.fechaMeses.julio','APP.Base.fechaMeses.agosto',
					'APP.Base.fechaMeses.septiembre','APP.Base.fechaMeses.octubre','APP.Base.fechaMeses.noviembre',
					'APP.Base.fechaMeses.diciembre', 'DASHBOARD.Dispositivos.graficaAnual.campos.grupo.todos',
					'DASHBOARD.Dispositivos.graficaAnual.campos.dispo.todos', 'DASHBOARD.Dispositivos.tituloHistorico.activo.label',
					'DASHBOARD.Dispositivos.tituloHistorico.inactivo.label','DASHBOARD.Dispositivos.tituloHistorico.total.label']).then(function (headline) {
		    $scope.translate ={
		    		dias: [
		    			headline['APP.Base.fechaDias.domingo'],
		    			headline['APP.Base.fechaDias.lunes'],
		    			headline['APP.Base.fechaDias.martes'],
		    			headline['APP.Base.fechaDias.miercoles'],
		    			headline['APP.Base.fechaDias.jueves'],
		    			headline['APP.Base.fechaDias.viernes'],
		    			headline['APP.Base.fechaDias.sabado']
		    		],
		    		meses: [
		    			headline['APP.Base.fechaMeses.enero'],
		    			headline['APP.Base.fechaMeses.febrero'],
		    			headline['APP.Base.fechaMeses.marzo'],
		    			headline['APP.Base.fechaMeses.abril'],
		    			headline['APP.Base.fechaMeses.mayo'],
		    			headline['APP.Base.fechaMeses.junio'],
		    			headline['APP.Base.fechaMeses.julio'],
		    			headline['APP.Base.fechaMeses.agosto'],
		    			headline['APP.Base.fechaMeses.septiembre'],
		    			headline['APP.Base.fechaMeses.octubre'],
		    			headline['APP.Base.fechaMeses.noviembre'],
		    			headline['APP.Base.fechaMeses.diciembre']
		    		],
		    		campoGraficaAnual:{
		    			grupo:headline['DASHBOARD.Dispositivos.graficaAnual.campos.grupo.todos'],
		    			dispo:headline['DASHBOARD.Dispositivos.graficaAnual.campos.dispo.todos']
		    		},
		    		activo:headline['DASHBOARD.Dispositivos.tituloHistorico.activo.label'],
		    		inactivo:headline['DASHBOARD.Dispositivos.tituloHistorico.inactivo.label'],
		    		total:headline['DASHBOARD.Dispositivos.tituloHistorico.total.label']
		    		
		    }
		    if(callback)
		    	callback()
		}, function (translationId) {
		    $scope.headline = translationId;
		});
	}
	
	$scope.initController = function() {
		$scope.cargarCatalogos();				//Carga el catalogo de Años y Meses
		$scope.cargarCatalogosBack();
		$scope.cargarPanelesPrincipales();		//Carga los datos de los paneles principales
		$scope.cargarDatosPrincipales();		//Carga las graficas
	}
	
	$scope.cargarCatalogos = function(){
		$scope.listaAnios.lista = listaCatalogos();
		$scope.listaAnios.valor = $scope.listaAnios.lista[$scope.listaAnios.lista.length-1];
		$scope.listaMeses.lista = $scope.translate.meses.map(function(el, idx){ return {id:idx,nombre:el} });
		$scope.listaMeses.valor = $scope.listaMeses.lista.find(function(el){ return el.id == moment().month()});
		$scope.camposDispo = angular.copy($scope.listaMeses);
		
	}
	
	$scope.cargarCatalogosBack = function(){
		//Cargar catalogos desde Back
		MonitoreoService.buscarAgrupamiento(1).success(function(data){
			$scope.camposAnual.grupo.lista = data;
			//$scope.camposAnual.grupo.valor = {id:0, nombre:$scope.translate.campoGraficaAnual.grupo};
			$('#select2-comboGraficaGrupoAnual-container').text($scope.translate.campoGraficaAnual.grupo);
		}).error(function(err){
			console.log(err);
		});
		$scope.buscarDispositivos(0);
	}
	
	function listaCatalogos(){
		var minAnio = 2017;
		var maxAnio = parseInt(moment().format('YYYY'));
		var anios = [];
		while(minAnio <= maxAnio)
			anios.push({id:anios.length,nombre:minAnio++})
			
		return anios
	}
	
	$scope.cargarPanelesPrincipales = function(){
		consultaDispService.obtenerTodoDispositivoHistorico().success(function(data){
			$scope.listDispositivos = data;
			for(i = 0; i < $scope.listDispositivos.length ; i++){
				if($scope.listDispositivos[i].stActivo)
					$scope.listaDispActivos.push(Object.assign([], $scope.listDispositivos[i]));
				else
					$scope.listaDispDesactivados.push(Object.assign([], $scope.listDispositivos[i]));
			}
			$scope.ctrls[0].myValue = $scope.listaDispActivos.length;
			$scope.ctrls[1].myValue = $scope.listaDispDesactivados.length;
			$scope.ctrls[2].myValue = $scope.listaDispActivos.length + $scope.listaDispDesactivados.length;
		}).error(function(){
			console.log("error");
		});
	}
	
	$scope.buscarEventosPorGrupo = function(){
		$scope.grafica.grupo.data = undefined;
		consultaDispService.obtenerTotalGruposEvento($scope.listaMeses.valor.id).success(function(data){
			//Validar si todos los datos no son CERO, en caso verdadero setear como indefinido
			if($scope.validaEventosPorGrupo(data)){
				$scope.grafica.grupo.data = data;
			}
			console.log($scope.grafica.grupo.data);
			$scope.orderarEventosPorGrupo($scope.grafica.grupo.orderBy);
		}).error(function(){
			console.log("error");
		});
	}
	
	generarGraficaGrupoEventoHistorico = function(json, total){
		$scope.grafica.grupo.chart = AmCharts.makeChart( "eventosPorGrupo", {
			"type": "pie",
			  "theme": "light",
			  "addClassNames": true,
			  "dataProvider": json,
			  "titleField": "tcNombre",
			  "valueField": "tcConteo",
			  "labelRadius": 5,
			  "radius": "42%",
			  "innerRadius": "60%",
			  "labelText": "[[title]]",
			  "legend":{
				  "position":"right",
				  "autoMargins":false,
				  "valueFunction":function(item, content){return total != 0 ? ((parseInt(content)*100)/total).toFixed(2)+"%" : "0%"}
			  },
			  "export": {
			    "enabled": true
			  }
		});
	}
	
	$scope.orderarEventosPorGrupo = function(sortBy){
		var conteo = 0;
		if(sortBy == undefined){
			$scope.grafica.grupo.orderBy = !$scope.grafica.grupo.orderBy;
			sortBy = $scope.grafica.grupo.orderBy;
		}
		
		if($scope.grafica.grupo.data){
			//Contar
			$scope.grafica.grupo.data.map(function(el, idx){
				conteo = conteo + el.tcConteo;
			});
			
			$scope.grafica.grupo.data.sort( function( a, b ) {
				if(sortBy){
					if ( a.tcConteo < b.tcConteo ) {
						return -1;
					} else if ( a.tcConteo == b.tcConteo ) {
						return 0;
					}
					return 1;
				}else{
					if ( a.tcConteo > b.tcConteo ) {
						return -1;
					} else if ( a.tcConteo == b.tcConteo ) {
						return 0;
					}
					return 1;
				}
			});
		}

		generarGraficaGrupoEventoHistorico($scope.grafica.grupo.data != undefined ? $scope.grafica.grupo.data.filter(function(el, idx){return idx<=9}): $scope.grafica.grupo.vacio, conteo);
	}
	
	$scope.buscarEventosPorDispositivo = function(){
		$scope.grafica.dispo.tabla.data = undefined;
		$scope.grafica.dispo.data = undefined;
		consultaDispService.obtenerTotalEventosDispositivo($scope.camposDispo.valor.id).success(function(data){
			if($scope.validaEventosDispositivo(data)){
				$scope.grafica.dispo.tabla.data = data;
			}
			$scope.orderarEventosPorDispositivo($scope.grafica.grupo.orderBy);
		}).error(function(err){
			
		});
	}
	
	$scope.buscarGraficaEventosPorDispositivo = function(id){
		$scope.grafica.dispo.data = undefined;
		consultaDispService.obtenerTotalGraficaEventosDispositivo(id, $scope.camposDispo.valor.id).success(function(data){
			
			if($scope.validaEventosDispositivo(data)){
				$scope.grafica.dispo.data = data;
			}
			
			$scope.orderarEventosPorDispositivo($scope.grafica.grupo.orderBy);
			$scope.viewMode.dispo = 1
		}).error(function(err){
			
		});
	}
	
	generarGraficaEventosPorDispositivo = function(json, total){
		$scope.grafica.dispo.chart = AmCharts.makeChart("eventosDispositivoHist", {
			  "type": "pie",
			  "theme": "light",
			  "addClassNames": true,
			  "dataProvider": json,
			  "titleField": "tcNombre",
			  "valueField": "tcConteo",
			  "labelText":"[[percentage]]",
			  "radius": "42%",
			  "legend":{
				  "position":"right",
				  "autoMargins":false,
				  "valueFunction":function(item, content){return content !== "NaN" ? total != 0 ? ((parseInt(content)*100)/total).toFixed(2)+"%" : "0%":"0%"}
			  },
			  "export": {
				  "enabled": true,
				  "position":"top-left"
			  }
		});
	}
	
	$scope.orderarEventosPorDispositivo = function(sortBy){
		var conteo  = 0;
		if(sortBy == undefined){
			$scope.grafica.dispo.orderBy = !$scope.grafica.dispo.orderBy;
			sortBy = $scope.grafica.dispo.orderBy;
		}
		
		if($scope.grafica.dispo.data){
			
			//Contar
			$scope.grafica.dispo.data.map(function(el, idx){
				conteo = conteo + el.tcConteo;
			});
			
			$scope.grafica.dispo.data.sort(function(a, b) {
				if(sortBy){
					if ( a.tcConteo < b.tcConteo ) {
						return -1;
					} else if ( a.tcConteo == b.tcConteo ) {
						return 0;
					}
					return 1;
				}else{
					if ( a.tcConteo > b.tcConteo ) {
						return -1;
					} else if ( a.tcConteo == b.tcConteo ) {
						return 0;
					}
					return 1;
				}
			});
		}
		
		if($scope.grafica.dispo.tabla.data){
			$scope.grafica.dispo.tabla.data.sort(function(a, b) {
				if(sortBy){
					if ( a.tcConteo < b.tcConteo ) {
						return -1;
					} else if ( a.tcConteo == b.tcConteo ) {
						return 0;
					}
					return 1;
				}else{
					if ( a.tcConteo > b.tcConteo ) {
						return -1;
					} else if ( a.tcConteo == b.tcConteo ) {
						return 0;
					}
					return 1;
				}
			});
		}
		
		generarGraficaEventosPorDispositivo($scope.grafica.dispo.data != undefined ? $scope.grafica.dispo.data : $scope.grafica.dispo.vacio, conteo);
	}
	
	$scope.buscarEventoAnual = function(){
		if($scope.camposAnual.grupo.valor == undefined)
			{ $scope.camposAnual.grupo.valor={}; $scope.camposAnual.grupo.valor.id = 0; }
		if($scope.camposAnual.dispo.valor == undefined)
			{ $scope.camposAnual.dispo.valor={}; $scope.camposAnual.dispo.valor.id = 0; }
		
		$scope.grafica.anual.data = undefined;
		consultaDispService.obtenerTotalEventoAnual($scope.camposAnual.grupo.valor.id, $scope.camposAnual.dispo.valor.id, $scope.listaAnios.valor.nombre).success(function(data){
			//Validar si todos los datos no son CERO, en caso verdadero setear como indefinido
			if($scope.validaEventosAnual(data)){
				$scope.grafica.anual.data = data;
			}
				
			console.log($scope.grafica.anual.data);
			$scope.prepararGraficaEventoAnualHistorico();
		}).error(function(){
			console.log("error");
		});
	};
	
	generarGraficaEventosAnualHistorico = function(json, config){
		var chart = AmCharts.makeChart("eventoAnual", {
			  "type": "serial",
			  "theme": "light",
			  "legend": {
			    "equalWidths": true,
			    "periodValueText": "[[value.sum]]",
			    "position": "bottom",
			    "valueAlign": "left",
			    "valueWidth": 100
			  },
			  "dataProvider": json,
			  "valueAxes": [{
			    "stackType": "regular",
			    "gridAlpha": 0.07,
			    "position": "left",
			    "title": "Trafico de registros"
			  }],
			  "graphs": config,
			  "plotAreaBorderAlpha": 0,
			  "marginTop": 10,
			  "marginLeft": 0,
			  "marginBottom": 0,
			  "chartScrollbar": {},
			  "chartCursor": {
			    "cursorAlpha": 0
			  },
			  "categoryField": "tcMes",
			  "categoryAxis": {
			    "startOnAxis": true,
			    "axisColor": "#DADADA",
			    "gridAlpha": 0.07,
			    "title": "Año"
			  }
		});
	};
	
	$scope.prepararGraficaEventoAnualHistorico = function(){
		var listaPropiedades= [];
		
		if($scope.grafica.anual.data){
			$scope.grafica.anual.data = $scope.grafica.anual.data.map(function(el, idx){
				var listaKeys = Object.keys(el);
				
				if(listaKeys.length > 0)
					listaKeys.find(function(el){
						if(el !== "tcMes")
							listaPropiedades = buscarKeyEnLista(listaPropiedades, el);
					});
				
				var nuevoEl = el;
				
				nuevoEl.tcMes = $scope.translate.meses[idx];
				return nuevoEl;
			});
		}else{
			$scope.grafica.anual.vacio = $scope.grafica.anual.vacio.map(function(el, idx){
				var nuevoEl = el;
				
				nuevoEl.tcMes = $scope.translate.meses[idx];
				return nuevoEl;
			});
			
			listaPropiedades = ["evento1", "evento2", "evento3"];
		}
		
		//Crear las configuraciones para la grafica
		var labelsInput = listaPropiedades.map(function(el){
			return {
				"bullet":"round",
				"bulletSize": 5,
			    "lineAlpha": 0.8,
			    "title": el,
			    "valueField": el}
		});
		
		generarGraficaEventosAnualHistorico($scope.grafica.anual.data != undefined ? $scope.grafica.anual.data : $scope.grafica.anual.vacio, labelsInput);
	}
	
	buscarKeyEnLista = function(lista, key){
		var existe = false;
		
		lista.find(function(el){
			if(el === key){
				existe = true;
				return
			}
		});
		
		if(!existe){
			lista.push(key)
		}
		
		return lista;
	};
	
	$scope.validaEventosPorGrupo = function(data){
		var totalConteoEsCero = false;
		
		data.map(function(el, idx){
			if(el.tcConteo != 0){
				totalConteoEsCero = true;
			}
		});
		
		return totalConteoEsCero;
	}
	
	$scope.validaEventosDispositivo= function(data){
		var totalConteoEsCero = false;
		
		data.map(function(el, idx){
			if(el.tcConteo !== 0){
				totalConteoEsCero = true;
			}
		});
		return totalConteoEsCero;
	}
	
	$scope.validaEventosAnual = function(data){
		var totalConteoEsCero = false;
		
		data.map(function(el, idx){
			if(Object.keys(el).length !== 0){
				totalConteoEsCero = true;
			}
		});
		return totalConteoEsCero;
	}
	
	$scope.buscarDispositivos = function(id){
		if(id != undefined && id != 0){
			MonitoreoService.buscarDispositivosEnGrupo(id).success(function(data){
				$scope.camposAnual.dispo.lista = data;
			}).error(function(err){
				
			});
		}else{
			MonitoreoService.buscarAgrupamiento(3).success(function(data){
				$scope.camposAnual.dispo.lista = data;
			}).error(function(err){
				
			});
		}
		$scope.camposAnual.dispo.valor = {id:0, nombre:$scope.translate.campoGraficaAnual.dispo}
		$('#select2-comboGraficaDispoAnual-container').text($scope.translate.campoGraficaAnual.dispo);
	}
	
	
	$scope.cargarDatosPrincipales = function(){
		
		//Grafica 1
		$scope.buscarEventosPorGrupo();
		
		
		//Grafica 2
		$scope.buscarEventosPorDispositivo();
		
		//Grafica 3
		$scope.buscarEventoAnual();
		
		
	}
	
	  /*************************/
	 /** Oyentes / Listeners **/
	/*************************/
	
	$scope.$watch("$parent.currentLanguage", function(newValue, oldValue) {
		$scope.traducir(function(){
			$scope.cargarCatalogos();
			$scope.prepararGraficaEventoAnualHistorico();
			$scope.ctrls[0].nbPanel = $scope.translate.activo;
		    $scope.ctrls[1].nbPanel = $scope.translate.inactivo;
		    $scope.ctrls[2].nbPanel = $scope.translate.total;
		});
	});
	
	/*$scope.$on('$locationChangeSuccess', function(event, current, previous) {
		$interval.cancel($scope.actualiza);
		$scope.actualiza = null;
	});*/
	
	$scope.traducir($scope.initController);
});