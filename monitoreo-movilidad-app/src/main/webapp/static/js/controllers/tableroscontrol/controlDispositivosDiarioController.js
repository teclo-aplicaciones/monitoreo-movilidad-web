angular.module(appTeclo).controller('controlDispositivosDiarioController', function($scope, $filter, $translate, $compile, $interval, consultaDispService, growl, showAlert) {

    $scope.listaDispActivos = [];
	$scope.listaDispHaceRato = [];
	$scope.listaDispSinReportar = [];
	$scope.listDispositivos = [];
	
	$scope.carga = {
		resumen: false,
		grafica:{
			evento: false,
			dispos: false,
			dispo: false
		}
		
	}
	
	$scope.elementoIds= [];
    
    $scope.grafica = {
        evento:{
			orderBy:false,
			vacio:[
			{
				tcNombre:'Evento 1',
				tcConteo:2
			},{
				tcNombre:'Evento 2',
				tcConteo:1
			},{
				tcNombre:'Evento 3',
				tcConteo:0
			}]
		},
		dispo:{
			orderBy:false,
			vacio:[
			{
				tcNombre:'Evento 1',
				tcConteo:5
			},{
				tcNombre:'Evento 2',
				tcConteo:7
			},{
				tcNombre:'Evento 3',
				tcConteo:2
			},{
				tcNombre:'Evento 4',
				tcConteo:0
			},{
				tcNombre:'Evento 5',
				tcConteo:3
			},{
				tcNombre:'Evento 6',
				tcConteo:4
			}]
		},
		dispos:{
			orderBy:false,
			vacio:[
			{
				tcNombre:'Dispositivo 1',
				tcConteo:0
			},{
				tcNombre:'Dispositivo 2',
				tcConteo:2
			},{
				tcNombre:'Dispositivo 3',
				tcConteo:1
			}]
		}
    };
	
	$scope.panelDiario = 
		[
			{myValue:0, nbPanel: null,idPanel:"dispEnLinea", myTarget:1, bgPanel:'bg-green', fnClick:function(){ $scope.showModalPaneles = true; $scope.showDisposEnLinea=true; }},
			{myValue:0, nbPanel: null,idPanel:"dispHaceRato", myTarget:2, bgPanel:'bg-yellow', fnClick:function(){}},
			{myValue:0, nbPanel: null,idPanel:"dispFueraLinea", myTarget:3, bgPanel:'bg-red', fnClick:function(){}}
		];
	
    $scope.viewMode={
		evento:1,
		dispo:1,
		dispos:1,
	};
    

      /******************************/
	 /** Configuracion de idiomas **/
	/******************************/
	
	$scope.traducir = function(callback){
        $translate(['DASHBOARD.Diario.tituloActuales.activo.label',
                    'DASHBOARD.Diario.tituloActuales.activo.info', 
					'DASHBOARD.Diario.tituloActuales.espera.label',
                    'DASHBOARD.Diario.tituloActuales.espera.info', 
					'DASHBOARD.Diario.tituloActuales.total.label',
                    'DASHBOARD.Diario.tituloActuales.total.info']).then(function (headline) {
		    $scope.translate ={
		    		activo: headline['DASHBOARD.Diario.tituloActuales.activo.label'],
		    		activoInfo: headline['DASHBOARD.Diario.tituloActuales.activo.info'],
		    		espera: headline['DASHBOARD.Diario.tituloActuales.espera.label'],
		    		esperaInfo: headline['DASHBOARD.Diario.tituloActuales.espera.info'],
                    total: headline['DASHBOARD.Diario.tituloActuales.total.label'],
                    totalInfo: headline['DASHBOARD.Diario.tituloActuales.total.info']
		    }
		    if(callback)
		    	callback()
		}, function (translationId) {
		    $scope.headline = translationId;
		});
	}
	
	$scope.initController = function() {
		$scope.descargarDatosPrincipales();
		$scope.cargarDatosPrincipales();
	}

	$scope.descargarDatosPrincipales = function(){
		$scope.listDispositivos = [];
		$scope.listaDispActivos = [];
		$scope.listaDispHaceRato = [];
		$scope.listaDispSinReportar = [];
		$scope.carga.resumen = true;
		consultaDispService.obtenerTodoDispositivoDiario().success(function(data){
			$scope.listDispositivos = data;
			$scope.listaDispActivos = [];
			$scope.listaDispHaceRato = [];
			$scope.listaDispSinReportar = [];
			actualTime = moment().valueOf();
			for(i = 0; i < $scope.listDispositivos.length ; i++){
                regTime = moment($scope.listDispositivos[i].fhRegistro, "DD/MM/YYYY HH:mm").valueOf();
				if(actualTime <= regTime+300000) //Comparamos el tiempo actual en MS, si es menos de 5 minutos
					$scope.listaDispActivos.push(Object.assign([], $scope.listDispositivos[i]));
				else
					if(actualTime <= regTime+3600000)//Comparamos el tiempo actual en MS, si es menos de 1 hora
						$scope.listaDispHaceRato.push(Object.assign([], $scope.listDispositivos[i]));
					else
						$scope.listaDispSinReportar.push(Object.assign([], $scope.listDispositivos[i]));
			}
				$scope.panelDiario[0].myValue = $scope.listaDispActivos.length;
	            $scope.panelDiario[0].nbPanel = $scope.translate.activo;

	            $scope.panelDiario[1].myValue = $scope.listaDispHaceRato.length;
	            $scope.panelDiario[1].nbPanel = $scope.translate.espera;
	            
	            $scope.panelDiario[2].myValue = $scope.listaDispSinReportar.length;
	            $scope.panelDiario[2].nbPanel = $scope.translate.total;
            
            $scope.carga.resumen = false;
		}).error(function(err){
			console.log(err);
            $scope.panelDiario[0].nbPanel = $scope.translate.activo;
            $scope.panelDiario[1].nbPanel = $scope.translate.espera;
            $scope.panelDiario[2].nbPanel = $scope.translate.total;
			$scope.carga.resumen = false;
		});
	}

	$scope.fechaActivas = function(date){
		var tiempo = moment(date,"DD/MM/YYYY HH:mm");
		tiempo.locale('es');
		if(moment().diff(tiempo, 'days') != 0){
			return date;
		}else{
			return tiempo.fromNow();
		}
	}
	
	$scope.buscarTipoEventoDiario = function(){
		$scope.grafica.evento.data = undefined;
		$scope.carga.grafica.evento = true;
	    consultaDispService.obtenerTotalEventosDiario().success(function(data){
			$scope.grafica.evento.data = data.datos;
			$scope.ordenarEventosActuales($scope.grafica.evento.orderBy);
			$scope.carga.grafica.evento = false;
			$scope.configurarActualizacion();
		}).error(function(err){
			$scope.grafica.evento.errMensaje = err.message;
			$scope.ordenarEventosActuales($scope.grafica.evento.orderBy);
			$scope.carga.grafica.evento = false;
		});
	}

	generarGraficaEventoDiario = function(json, total){
        $scope.grafica.evento.chart = AmCharts.makeChart("EventosDiarios", {
        "type": "serial",
        "theme":"light",
        "dataProvider": json,
        "graphs": [{
			//"balloonText": "[[category]]: <b>[[value]]</b> ([[percentages]] %)",
        	"balloonFunction": function(item, content){return item.category+": <b>"+item.values.value+"</b> ("+((parseInt(item.values.value)*100).toFixed(2)/total)+"%)"},
			"fillColors": "#CC0000",
            "fillAlphas": 0.8,
            "lineAlpha": 0.2,
            "type": "column",
            "valueField": "tcConteo"
            }],
        "categoryField": "tcNombre",
        "chartCursor": {
            "fullWidth": true,
            "cursorAlpha": 0.1,
		},
		"export": {
			"enabled": true,
		}});
    };
    
    $scope.ordenarEventosActuales = function(sortBy){
    	var total = 0;
		if(sortBy == undefined){
			$scope.grafica.evento.orderBy = !$scope.grafica.evento.orderBy;
			sortBy = $scope.grafica.evento.orderBy;
		}
		if($scope.grafica.evento.data){
			//Contar totales
			$scope.grafica.evento.data.map(function(el){
				total = total + el.tcConteo;
			})
			
			$scope.grafica.evento.data.sort( function( a, b ) {
				if(sortBy){
					if ( a.tcConteo < b.tcConteo ) {
						return -1;
					} else if ( a.tcConteo == b.tcConteo ) {
						return 0;
					}
					return 1;
				}else{
					if ( a.tcConteo > b.tcConteo ) {
						return -1;
					} else if ( a.tcConteo == b.tcConteo ) {
						return 0;
					}
					return 1;
				}
			});
		}

		generarGraficaEventoDiario($scope.grafica.evento.data != undefined ? $scope.grafica.evento.data.filter(function(el, idx){ return idx<=9}): $scope.grafica.evento.vacio, total);
    };
	
	$scope.buscarDispositivosActuales = function(){
		$scope.grafica.dispos.data = undefined;
		$scope.carga.grafica.dispos = true;
		consultaDispService.obtenerTotalDispositivosDiario().success(function(data){
			$scope.grafica.dispos.data = data.datos;
			$scope.ordenarDispositivosActuales($scope.grafica.dispos.orderBy);
			$scope.carga.grafica.dispos = false;
			$scope.configurarActualizacion();
		}).error(function(err){
			$scope.grafica.dispos.errMensaje = err.message;
			$scope.ordenarDispositivosActuales($scope.grafica.dispos.orderBy);
			$scope.carga.grafica.dispos = false;
		});
	}

	generarGraficaDispositivosDiarios = function(json){
		$scope.grafica.dispos.chart = AmCharts.makeChart("DispositivosDiarios", {
			"type": "serial",
			"theme": "light",
			"dataProvider": json,
			"graphs": [{
				"bulletSize": 15,
				"bullet":"round",
				"bulletColor": "#CC0000",
				"lineThickness": 3,
				"lineAlpha": 0.2,
				"lineColor":"#CC0000",
				"valueField": "tcConteo"
			}],
			"smoothCustomBullets": {
				"borderRadius": "auto"
			},
			"categoryField": "tcNombre",
			"export":{
				"enabled": true
			}
		});
	}

	$scope.ordenarDispositivosActuales=function(sortBy){
		if(sortBy == undefined){
			$scope.grafica.dispos.orderBy = !$scope.grafica.dispos.orderBy;
			sortBy = $scope.grafica.dispos.orderBy;
		}
		if($scope.grafica.dispos.data)
			$scope.grafica.dispos.data.sort( function( a, b ) {
				if(sortBy){
					if ( a.tcConteo < b.tcConteo ) {
						return -1;
					} else if ( a.tcConteo == b.tcConteo ) {
						return 0;
					}
					return 1;
				}else{
					if ( a.tcConteo > b.tcConteo ) {
						return -1;
					} else if ( a.tcConteo == b.tcConteo ) {
						return 0;
					}
					return 1;
				}
			});

		generarGraficaDispositivosDiarios($scope.grafica.dispos.data != undefined ? $scope.grafica.dispos.data.filter(function(el, idx){ return idx<=9}): $scope.grafica.dispos.vacio);
	}

	$scope.buscarDispositivoActual = function(){
		if($scope.grafica.dispos.seleccion || $scope.grafica.dispo.chart == undefined)
			if($scope.grafica.dispos.seleccion){
				$scope.grafica.dispo.data = undefined;
				$scope.carga.grafica.dispo = true;
				consultaDispService.obtenerTotalEventosDispositivoDiario($scope.grafica.dispos.seleccion.tcIdDispo).success(function(data){
					$scope.grafica.dispo.data = data.datos;
					$scope.grafica.dispo.nombre = $scope.grafica.dispos.seleccion.tcNombre;
					$scope.ordenarDispositivoActual($scope.grafica.dispo.orderBy);
					$scope.carga.grafica.dispo = false;
				}).error(function(err){
					console.log(err);
					$scope.grafica.dispo.errMensaje = err.message;
					$scope.ordenarDispositivoActual($scope.grafica.dispo.orderBy);
					$scope.carga.grafica.dispo = false;
				});
			}else{
				$scope.grafica.dispo.errMensaje = "Seleccione un dispositivo de la grafica 'Dispositivos Actuales'";
				$scope.ordenarDispositivoActual($scope.grafica.dispo.orderBy);
			}
	};

	generarColorAleatorio= function(){
		var r = (Math.floor((Math.random() * 255) + 1)).toString(16);
		var g = (Math.floor((Math.random() * 255) + 1)).toString(16);
		var b = (Math.floor((Math.random() * 255) + 1)).toString(16);

		return "#"+r+g+b;
	}

	generarGraficaEventosDispositivoDiario = function(idElement, json){
		$scope.grafica.dispo.chart.push(AmCharts.makeChart(idElement, {
			"type": "pie",
			"marginBottom": 40,
			"allLabels": [{
				"x": "50%",
				"y": "35%",
				"text": ((parseInt(json[1].tcConteo)*100)/parseInt(json[1].tcConteo+json[0].tcConteo)).toFixed(2)+"%",
				"size": 28,
				"align": "middle",
				"color": "#555"
			}, {
				"x": "50%",
				"y": "85%",
				"text": json[1].tcNombre,
				"size": 14,
				"bold": true,
				"align": "middle",
				"color": "#555"
			}],
			"dataProvider": json,
			"colorField": "tcColor",
			"valueField": "tcConteo",
			"labelRadius": -130,
			"pullOutRadius": 0,
			"labelRadius": 0,
			"innerRadius": "70%",
			"labelText": "",
			"balloonText": ""
			}));
	}

	$scope.ordenarDispositivoActual = function(sortBy){
		if(sortBy == undefined){
			$scope.grafica.dispo.orderBy = !$scope.grafica.dispo.orderBy;
			sortBy = $scope.grafica.dispo.orderBy;
		}
		if($scope.grafica.dispo.data)
			$scope.grafica.dispo.data.sort( function( a, b ) {
				if(sortBy){
					if ( a.tcConteo < b.tcConteo ) {
						return -1;
					} else if ( a.tcConteo == b.tcConteo ) {
						return 0;
					}
					return 1;
				}else{
					if ( a.tcConteo > b.tcConteo ) {
						return -1;
					} else if ( a.tcConteo == b.tcConteo ) {
						return 0;
					}
					return 1;
				}
			});
		$scope.prepararGraficaDispositivoDiario();
	}

	$scope.prepararGraficaDispositivoDiario = function(){
		var idElemento = 'eventoPorDispositivo'; 							//identificador del DOM Generico
		var HTMLChart = '<div class="eventoChart" id="#"></div>'; 			//plantilla del DOM Generico
		var template; 														//Utilizaremos angular.element(HTMLChart) para crear el DOM Generico
		var target = document.getElementById('multiAmCharts'); 				//Buscamos donde vamos a insertar los DOM Genericos
		var conteoTotal = 0;

		//Especificar que sera un Array
		$scope.grafica.dispo.chart = [];

		//Limpiamos los elementos existentes
		$scope.elementoIds.map(function(el, idx){
			var element = document.getElementById(el);
			target.removeChild(element);
		});

		$scope.elementoIds = [];

		if($scope.grafica.dispo.data){
			//Recompilamos el total del conteo
			$scope.grafica.dispo.data.map(function(el, idx){
				conteoTotal = conteoTotal + el.tcConteo;
			});

			//Recorremos cada elemento y creamos su PieChart por cada Evento
			$scope.grafica.dispo.data.map(function(el, idx){
				var actualIdElemento = HTMLChart.replace('#',idElemento+(idx+1));

				template = angular.element(actualIdElemento);

				//Copiaremos el elemento actual y le agregaremos valores para su Chart
				var valor = {...el};
				var diferencia = {...el};

				//valor.tcColor = '#45b4e3';
				valor.tcColor = generarColorAleatorio();

				diferencia.tcConteo = conteoTotal - valor.tcConteo;
				diferencia.tcColor = '#bebdc3';

				angular.element(target).append(template);

				$compile(template)($scope, function (clonedElement) {
					$scope.elementoIds.push(clonedElement.context.id);
					generarGraficaEventosDispositivoDiario(clonedElement.context.id, [diferencia,valor]);
				});
			});
		}else{
			//Recompilamos el total del conteo
			$scope.grafica.dispo.vacio.map(function(el, idx){
				conteoTotal = conteoTotal + el.tcConteo;
			});

			//Recorremos cada elemento y creamos su PieChart por cada Evento
			$scope.grafica.dispo.vacio.map(function(el, idx){
				var actualIdElemento = HTMLChart.replace('#',idElemento+(idx+1));

				template = angular.element(actualIdElemento);

				//Copiaremos el elemento actual y le agregaremos valores para su Chart
				var valor = {...el};
				var diferencia = {...el};

				//valor.tcColor = '#45b4e3';
				valor.tcColor = generarColorAleatorio();

				diferencia.tcConteo = conteoTotal - valor.tcConteo;
				diferencia.tcColor = '#bebdc3';

				angular.element(target).append(template);

				$compile(template)($scope, function (clonedElement) {
					$scope.elementoIds.push(clonedElement.context.id);
					generarGraficaEventosDispositivoDiario(clonedElement.context.id, [diferencia,valor]);
				});
			});
		}
	}
	
	$scope.configurarActualizacion = function(){
		if(!$scope.actualiza){
			if($scope.grafica.evento.data != undefined &&
					$scope.grafica.dispos.data != undefined){
				$scope.actualiza = $interval($scope.initController, 20000);	
			}
		}else{
			/*if($scope.grafica.evento.data != undefined &&
					$scope.grafica.dispos.data != undefined){
				$interval.cancel($scope.actualiza);
				$scope.actualiza = null;
			}*/
		}
	}
	
	$scope.cargarDatosPrincipales = function(){
		
		//Grafica 1
        $scope.buscarTipoEventoDiario();
		
		//Grafica 2
		$scope.buscarDispositivosActuales();
		//$scope.buscarEventoActual(grupo.idGrupo, disp);
		
		//Grafica 3
		//ESTA GRAFICA UNICAMENTE SE CARGARA CUANDO SE SELECCIONE UN DISPOSITIVO
		$scope.buscarDispositivoActual(null);
		//$scope.buscarEventoDispositivo(1, anio.nbAnio);
    }
    
      /*************************/
	 /** Oyentes / Listeners **/
	/*************************/
	
	$scope.$watch("$parent.currentLanguage", function(newValue, oldValue) {
		$scope.traducir(function(){
			$scope.panelDiario[0].nbPanel = $scope.translate.activo;
		    $scope.panelDiario[1].nbPanel = $scope.translate.espera;
		    $scope.panelDiario[2].nbPanel = $scope.translate.total;
		});
	});
	
	$scope.$on('$locationChangeSuccess', function(event, current, previous) {
		$interval.cancel($scope.actualiza);
		$scope.actualiza = null;
    });
    
    $scope.traducir($scope.initController);
});