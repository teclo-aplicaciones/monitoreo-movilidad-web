angular.module(appTeclo).controller('nuevoMantenimientoController', function($scope, $filter, mantenimientoService, $rootScope, $timeout, $location, $localStorage, $document, configAppService, showAlert, ModalService){
	
	$scope.tipoMantenimiento=[];	
	$scope.mantenimientoVO={};

	// init
	mantenimientoService.ListaMantenimientos().success(function(data){
		$scope.tipoMantenimiento = data;
	});
	mantenimientoService.ListaDispositivos().success(function(data){
		$scope.dispositivos=data;
	});
	
	//save 
	$scope.save = function(){
		if($scope.altaMantenimiento.$invalid){
			angular.forEach($scope.altaMantenimiento.$error, 
				function(field){
					angular.forEach(field, function(errorField){
	              	  errorField.$setDirty();
	                })
              	});	
			showAlert.aviso("Formulario Incompleto");
		}
		else{		
			mantenimientoService.agregaMantenimiento({
				nombre:$scope.mantenimientoVO.nombre,
				tipo:$scope.mantenimientoVO.mantenimiento.id,
				fechaRecepcion:$scope.mantenimientoVO.fechaRecepcion,				
				fechaEntregaEstimada:$scope.mantenimientoVO.fechaEntregaEstimada,
				dispositivoId:$scope.mantenimientoVO.dispositivo.id,
				imei:$scope.mantenimientoVO.dispositivo.imei,
				descripcion:$scope.mantenimientoVO.descripcion
			}).success(function(data){			
//				console.info($scope.mantenimientoVO);
				$('#select2-resolucionesApp-container').text("Selccione");
				$('#select2-tipoMantenimient-container').text("Selccione");
				$scope.mantenimientoVO={}
				showAlert.aviso($scope.mensajeModal("Registro agregado correctamente"));
				$scope.altaMantenimiento={};				
			});
		}
		
	}

});