angular.module(appTeclo).controller('consultaMantenimientoController', function($scope, $filter, mantenimientoService, $rootScope, $timeout, $location, $localStorage, $document, configAppService, showAlert, ModalService){
	
	$scope.parametroBusqueda = {
		valor:""
	};	
	$scope.mantenimientos=[];
	$scope.mantenimientoVO={};
	
	mantenimientoService.ListaMantenimientos().success(function(data){
		$scope.tipoMantenimiento = data;		
	});	
	
	mantenimientoService.busqMantenimiento().success(function(data){
		$scope.tipoBusqueda=data;
		$scope.parametroBusqueda.tipoFiltro = $scope.tipoBusqueda[0];
		angular.element("#select2-tipoBusqueda-container").text(data[0].nombre);	
		$scope.buscarMantenimiento();
	});
	
	// variables para actualizar	
	$scope.mantenimientoVO={};
	mantenimientoService.ListaDispositivos().success(function(data){
		$scope.dispositivos=data;
	});
	
	/**
	 * Eventos
	 */	
	$scope.buscarMantenimiento = function(){
			// enviar solo el id de tipo de mantenimeinto
			if($scope.parametroBusqueda.tipoFiltro.id == 2){
				$scope.parametroBusqueda.valor = $scope.parametroBusqueda.valor.id;				
			}
		
			mantenimientoService.consultarMantenimiento($scope.parametroBusqueda.tipoFiltro.id, $scope.parametroBusqueda.valor).success(function(data){
				$scope.mantenimientos = data;
				
				if(data.length == 0 ){					
			      ModalService.showModal({
			        templateUrl: 'views/templatemodal/templateModalAviso.html',
			        controller: 'mensajeModalController',
			        inputs:{ message: $scope.mensajeModal("No se encontró información con los parámetros solicitados")}
			      }).then(function(modal) {
			        modal.element.modal();
			      });				      
				}
				
			}).error(function(error){
				alert("error");
			});
	}
	
	$scope.eliminar = function(idMantenimiento){
		setTimeout(function(){			
			ModalService.showModal({
		    	templateUrl: 'views/templatemodal/templateModalConfirmacion.html',
		        controller: 'mensajeModalController',
		        	inputs:{ message: "¿Esta seguro de eliminar?"}
		    }).then(function(modal) {
		        modal.element.modal();
		        modal.close.then(function(result) {
		        	if(result){
		        		mantenimientoService.eliminarMantenimiento(idMantenimiento).success(function(data){
		        			$scope.buscarMantenimiento();
		        		})
		        	}
		        }); 
		    });			
		})
	}
	
	$scope.finish = function(mantenimientoVO){
		setTimeout(function(){			
			ModalService.showModal({
		    	templateUrl: 'views/templatemodal/templateModalConfirmacion.html',
		        controller: 'mensajeModalController',
		        	inputs:{ message: "¿Esta seguro de finalizar el mantenimeinto?"}
		    }).then(function(modal) {
		        modal.element.modal();
		        modal.close.then(function(result) {
		        	if(result){
		        		mantenimientoService.finalizarMantenimiento(mantenimientoVO.id,mantenimientoVO.observation).success(function(data){
		        			$scope.showModalFinish = true;
		        			$scope.buscarMantenimiento();		        			
		        		});
		        	}
		        }); 
		    });			
		})
	}
	
	$scope.changeTipoBusqueda = function(){
		$scope.parametroBusqueda.valor = "";
	}
	 
	$scope.update = function(){
		mantenimientoService.actualizaMantenimiento({
			id:$scope.mantenimientoVO.id,
			nombre:$scope.mantenimientoVO.nombre,
			tipo:$scope.mantenimientoVO.mantenimiento.id,
			fechaRecepcion:$scope.mantenimientoVO.fechaRecepcion,			
			fechaEntregaEstimada:$scope.mantenimientoVO.fechaEntregaEstimada,
			dispositivoId:$scope.mantenimientoVO.dispositivo.id,
			imei:$scope.mantenimientoVO.dispositivo.imei,
			descripcion:$scope.mantenimientoVO.descripcion
		}).success(function(data){			
			console.info($scope.mantenimientoVO);
			$('#select2-resolucionesApp-container').text("Selccione");
			$('#select2-tipoMantenimient-container').text("Selccione");
			$scope.mantenimientoVO={}
			showAlert.aviso($scope.mensajeModal("Registro actualizado correctamente"));			
			$scope.buscarMantenimiento();		
			$scope.showModalUpdate = false;	
		});		
	}
	
	/**
	 * Modal
	 */
	
	$scope.openModalUpdate = function(idMantenimiento){
		$scope.showModalUpdate = true;				
		mantenimientoService.buscarMantenimientoPorId(idMantenimiento).success(function(data){
			$scope.mantenimientoVO=data;			
			// seleccionar mantenimeinto
			$scope.mantenimientoVO.mantenimiento = $scope.tipoMantenimiento.filter(function(x){if(x.id === data.tipo) return x})[0]; 
			angular.element("#select2-tipoMantenimient2-container").text($scope.mantenimientoVO.mantenimiento.nombre);
			// seleccionar dispositivo
			$scope.mantenimientoVO.dispositivo = $scope.dispositivos.filter(function(x){if(x.id === data.dispositivoId) return x})[0];
			angular.element("#select2-resolucionesApp-container").text($scope.mantenimientoVO.dispositivo.nombre);
		})
	}
	
	/**
	 * Tabs of timeline
	 */
	
	$scope.tab = 1;
	$scope.setTab = function(newTab){
		$scope.tab = newTab;
	};
    $scope.isSet = function(tabNum){
      return $scope.tab === tabNum;
    };
	
	$scope.openModalTimeline = function(idMantenimiento,idUsuarioOIMEI,tipoTimeline){		
		$scope.showModalTimeline = true;
		$scope.timeline =null;
		mantenimientoService.timelineMantenimiento(idMantenimiento,idUsuarioOIMEI,tipoTimeline).success(function(data){
			$scope.tipoTimeline=tipoTimeline;
			switch (tipoTimeline) {
				// busqueda por usuario por el idEmpleado
				case 1:
					$scope.timeline = data;					
				break;
				
				// busqueda por imei
				case 2:
					$scope.timeline = data[0];					
				break;
			}
			setTimeout(function(){
				angular.element('#tabTimeline0').click();	
			});			
			document.getElementById('scrollmodal').scrollTop =0;
		})		
	}
	
	$scope.openModalFinish = function(mantenimientoVO){
		$scope.mantenimientoVO=mantenimientoVO;
		$scope.showModalFinish = true;		
	}
});