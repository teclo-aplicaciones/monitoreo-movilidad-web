angular.module(appTeclo).controller("ejecutaComandoController", function(
		growl,$scope, $translate, CatalogoService, ComandoService,ModalService,consultaDispService,constante) {
	
	/**
	 * Var
	 */
	$scope.listaComboFiltro = {
			lista:[],
			valor: null
	};
	$scope.listaAgrupacion = {
			lista:[],
			valor: null,
	};
	$scope.listaDispositivo = {
			lista:[],
			valor: null,
	};
	$scope.listaMedioComunicacion ={
			lista:[],
			valor: null
	};	
	$scope.viewMode = 0;
	$scope.logComando=[];		
	$scope.disabledBusqueda = true;
	$scope.disabledDispositivo = true;
	$scope.showTabs=false;
		
	$scope.urlApi = "//"+window.location.host+constante.urlWs+"comandoArchivo?idArchivo=";
	
	/**
	 * Show/Hide web componentes
	 */	
	CatalogoService.catalogoComponenteWeb({
		activarComando:{},
		ejecutarComando:{}
	},function(data){
		$scope.webComponentes = data;		
	});
	
	/**
	 * Load
	 */
	
	CatalogoService.buscarCatalogosPorId([1, 3, 4]).success(function(data){
		$scope.listaComboFiltro.lista = data;
	}).error(function(err){
		console.log("error");
	});		
	
	CatalogoService.catalogoTipoMedio().success(function(data){
		$scope.listaMedioComunicacion.lista = data;
		$scope.listaMedioComunicacion.valor = data[0];
		angular.element("#select2-selectMedioComunicacion-container").text(data[0].nombre);		
	}).error(function(err){
		console.log("error");
	});

	/**
	 * Functions
	 */
	
	$scope.changeFiltro = function(tipo){
		$scope.disabledBusqueda = true;
		$scope.disabledDispositivo = true;
		$scope.showTabs =false;
		
		angular.element("#select2-comboDispositivoComando-container").text($scope.translate.seleccione);
		angular.element("#select2-comboBuscaComando-container").text($scope.translate.seleccione);
		
		if(tipo && tipo.id){
			switch (tipo.id) {
			// tipo de grupos
			case 1 : 
				$scope.disabledBusqueda = false;
				ComandoService.buscarAgrupamiento(1).success(function(data){
					$scope.listaAgrupacion.lista = data;
				});
				break;
				
			// tipo geocercas
			case 3 :
				$scope.disabledBusqueda = false;
				ComandoService.buscarAgrupamiento(2).success(function(data){
					$scope.listaAgrupacion.lista = data;
				});
				break;
				
			// dispositivos
			case 4 :
				$scope.disabledDispositivo = false;
				consultaDispService.consultarTodosLosDispositivos().success(function(data){
					$scope.listaDispositivo.lista = data;					
				});
				break;
			
			}
		}else{
			$scope.showTabs =false;
		}	
	}
	
	$scope.changeBusqueda = function(tipo){
		$scope.disabledDispositivo = true;
		$scope.showTabs =false;
		if(tipo){			
			$scope.disabledDispositivo = false;
			// tipo de grupos
			if($scope.listaComboFiltro.valor.id == 1){
				ComandoService.buscarDispositivosEnGrupo(tipo.id).success(function(data){
					$scope.listaDispositivo.lista = data;
					$scope.listaDispositivo.valor = {id:0, nombre:$scope.translate.seleccione};
				}).error(function(err){
					
				});
			}
			// tipo geocercas
			else if($scope.listaComboFiltro.valor.id == 3){
				ComandoService.buscarDispositivosEnGeocerca(tipo.id).success(function(data){
					$scope.listaDispositivo.lista = data;
					$scope.listaDispositivo.valor = {id:0, nombre:$scope.translate.seleccione};
				}).error(function(err){
					
				});
			}			
		}else{
			$scope.showTabs =false;
		}	
	}
	
	$scope.busqueda = function(){
		
		if($scope.listaDispositivo.valor && $scope.listaDispositivo.valor.id && $scope.listaMedioComunicacion.valor.id){
			$scope.showTabs =true;
//			$scope.logsCommand();
			ComandoService.catalogoComandos(
				$scope.listaDispositivo.valor.id,				
				$scope.listaMedioComunicacion.valor.id				
				).success(function(data){
					$scope.catalogoComandos=[];		
					for(i=0;i<data.length;i+=2){			 
						$scope.catalogoComandos.push({
							a:{
								show:true,
								data:data[i]
							},
							b:{
								show:true,
								data:data[i+1]
							}
						});			
					}
					
					for(var x of $scope.catalogoComandos){		
						x.a.show = false;			
						if(x.b.data && x.b.show){
							x.b.show = false;
						}	
					}		
					
					if($scope.listaDispositivo.valor && $scope.listaDispositivo.valor.id !== 0 ){
						ComandoService.obtenerComandosDispositivo($scope.listaDispositivo.valor.id).success(function(data){			
							for(var x of $scope.catalogoComandos){					
								
									for(var y of data){
										if(x.a.data.id === y.id){
											x.a.show = y.activo;
											x.a.model = y.activo;
										}
										if(x.b.data && x.b.data.id === y.id){
											x.b.show = y.activo;
											x.b.model = y.activo;
										}	
									}
								
							}			
						}).error(function(err){});
					}else{
						$scope.listaDispositivo.valor={id:0};			
					}	
					$scope.showTabs = true;
					
				}).error(function(err){});
		}else{
			$scope.showTabs =false;
		}
	}		
	
	
	$scope.changeDeviceModel = function(self){
		var selfModel = true;
		if(self.model === 1){
			selfModel=false;
		}		
		ComandoService.activarDesactivarComandoPorUsuario($scope.listaDispositivo.valor.id,self.data.id,selfModel).success(function(data){
			for(var x of $scope.catalogoComandos){
				if(x.a.data.id === self.data.id){
					x.a.show = selfModel;
				}
				if(x.b.data && x.b.data.id === self.data.id){
					x.b.show = selfModel;
				}
			}
		}).error(function(err){});
	}
	
	/*
	$scope.exec = function(idCommand){		
		ComandoService.execCommand(
				$scope.listaDispositivo.valor.id,
				idCommand			
			).success(function(data){
			growl.success($scope.mensajeModal('Se ejecuto el comando exitosamente'));
		}).error(function(err){
			growl.error($scope.mensajeModal('Error al ejecutar comando'));
		});
	}
	*/
	var lockExec = false;
	var timeExec = 10000;
	$scope.exec = function(idCommand){
		if(lockExec){
			growl.warning($scope.mensajeModal('Espere un momento, para ejecutar otro comando'));
		}else{
			ComandoService.execCommand(
					$scope.listaDispositivo.valor.id,
					idCommand			
				).success(function(data){
				growl.success($scope.mensajeModal('Se ejecuto el comando exitosamente'));
				
				lockExec=true;
				setTimeout(function(){
					lockExec=false
				},timeExec);
				
			}).error(function(err){
				growl.error($scope.mensajeModal('Error al ejecutar comando'));
			});
		}
	}
	
	$scope.logsCommand = function(){		
		ComandoService.logsCommand($scope.listaDispositivo.valor.id).success(function(data){
			$scope.logComando=data;
		}).error(function(err){});
	}
	
	$scope.openModal = function(logComando){
		$scope.showModal = true; 
		ComandoService.logsCommandDetalle(logComando.idBitacora).success(function(data){
			$scope.logComandoHeader=logComando;
			$scope.logComandoDetalle=data;
		}).error(function(err){});
	}
	
	
	$scope.openModalImage = function(idArchivo){	
		$scope.image=$scope.urlApi+idArchivo+"&f="+Math.floor(Math.random() * 100) + 1;
		$scope.showModalImage = true;			
		angular.element('#imgLoading').show();
		angular.element('#imgModal').hide();
		angular.element('#imgModal').on('load', function(){
			angular.element('#imgLoading').hide();
			angular.element('#imgModal').show();
		});
	}
	$scope.defaultColor="#FF0000";
	$scope.openModalGpsActual = function(lattitude,longitude){
		$scope.showModalGpsActual = true;
		$scope.listaTracks=[{
			coords:[
				[parseFloat(lattitude),parseFloat(longitude)]
			],			
			info:{				
				img:"\n<svg width=\"37.99999999999999\" height=\"37.99999999999999\" xmlns=\"http://www.w3.org/2000/svg\">\n <style type=\"text/css\">.st0{fill:#607D8B;}\n\t.st1{display:none;fill:none;stroke:#000000;stroke-miterlimit:10;}</style>\n\n <g>\n  <title>background</title>\n  <rect fill=\"none\" id=\"canvas_background\" height=\"40\" width=\"40\" y=\"-1\" x=\"-1\"/>\n </g>\n <g>\n  <title>Layer 1</title>\n  <rect id=\"svg_4\" height=\"27.625047\" width=\"16.125028\" y=\"1.56247\" x=\"11.062485\" stroke-width=\"1.5\" fill=\"#ffffff\"/>\n  <g id=\"svg_1\">\n   <path id=\"svg_2\" d=\"m28.302712,2.9c0,-0.4 -0.2,-1.2 -0.7,-1.7c-0.4,-0.4 -1,-0.7 -1.7,-0.7l-13.9,0c-0.7,0 -1.3,0.3 -1.7,0.7c-0.6,0.6 -0.7,1.3 -0.7,1.7c-0.4,4.2 0.3,26 0.3,26.4c0,0 0,0 0,0c0,0 0,0 0,0c0,0 0,0.2 0,0.2c0,0.9 1.7,2 1.7,2.1l0,0c1,0.8 1.7,1.4 2.2,1.8c1.1,1 1.3,1.1 1.6,1.3c0.2,0.2 0.3,0.3 0.4,0.3c0.5,0.5 1.1,0.8 1.8,1.5c0.9,0.8 0.9,1 1.3,1c0.4,0 0.7,-0.3 1.4,-0.9c0.6,-0.5 1.2,-0.9 1.7,-1.4c1.3,-1.1 1.9,-1.6 2.4,-2c1.4,-1.1 1.9,-1.5 1.9,-1.5c0.9,-0.7 1.8,-1.3 1.8,-2.1c0,-0.1 0,-0.2 0,-0.2c0.4,-2.2 0.6,-22.3 0.2,-26.5zm-12.8,-1l7,0c0.2,0 0.3,0.3 0.3,0.6c0,0.3 -0.1,0.6 -0.3,0.6l-7,0c-0.2,0 -0.3,-0.3 -0.3,-0.6c-0.1,-0.3 0.1,-0.6 0.3,-0.6zm3.5,30.6c-0.8,0 -1.5,-0.7 -1.5,-1.5c0,-0.8 0.7,-1.5 1.5,-1.5c0.8,0 1.5,0.7 1.5,1.5c0,0.8 -0.6,1.5 -1.5,1.5zm7.5,-3.8l-15.1,0l0,-24.2l15.1,0l0,24.2l0,0z\" class=\"st0\"/>\n  </g>\n  <rect id=\"svg_3\" height=\"38\" width=\"38\" class=\"st1\" y=\"225.3\" x=\"215.3\"/>\n </g>\n</svg>","ultimaFecha":"01/02/2019 19:18:11","imei":"351847090563056"
			}
		}]
	}
	
	
	/**
	 * util
	 */
	
	$scope.traducir = function(callback){
		$translate('COMANDOS.Nuevo.filtrado.seleccionar').then(function (headline) {
		    $scope.translate ={
		    		seleccione: headline
		    }
		    if(callback)
		    	callback()
		}, function (translationId) {
		    $scope.headline = translationId;
		});
	}
	$scope.traducir();
	
	$scope.$watch("$parent.currentLanguage", function(newValue, oldValue) {
		$scope.traducir(null);
	});	
	
});