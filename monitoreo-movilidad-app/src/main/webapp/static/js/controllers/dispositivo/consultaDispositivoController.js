angular.module(appTeclo).controller("consultaDispositivoController", function($q,$scope,growl, CatalogoService, consultaDispService, showAlert,ModalService,showAlert){
	
	/**
	 * Show/Hide web componentes
	 */	 
	CatalogoService.catalogoComponenteWeb({
		eliminarDispositivo:{},
		editarDispositivo:{},
		activarDispositivo:{}
	},function(data){
		$scope.webComponentes = data;
	});
	
	$scope.consultar = function(tipo, valor){		
		window.saveConsultar = {
				tipo:tipo,
				valor:valor,
				listaBusqueda:$scope.listaBusqueda.valor,
				myMarca: $scope.myMarca,
				myModel:$scope.myModelo,
				webComponentes:$scope.webComponentes
		}	
		
		// nombre o imei
		if(tipo.id == 1 || tipo.id == 2 ){
			valor = valor === undefined ? '' : valor;			
		}
		// marca
		else if (tipo.id == 3){
			if($scope.myMarca.nombre)
				valor = $scope.myMarca.nombre;
			else
				valor=''
		}
		// modelo
		else if (tipo.id == 4){
			if($scope.myModelo.nombre)
				valor = $scope.myModelo.nombre;
			else
				valor=''
		}
		
		if(($scope.listaBusqueda.valor.id === 1 || $scope.listaBusqueda.valor.id === 2 )&& $scope.formBusquedaDispositivo.$invalid){
			requiredFields($scope.formBusquedaDispositivo);
		}else{
			consultaDispService.consultarDispositivos(tipo.id, valor).success(function(data){
				$scope.dispositivos = data;
				if(data.length == 0 ){
					
			      ModalService.showModal({
			        templateUrl: 'views/templatemodal/templateModalAviso.html',
			        controller: 'mensajeModalController',
			        inputs:{ message: $scope.mensajeModal("No se encontró información con los parámetros solicitados")}
			      }).then(function(modal) {
			        modal.element.modal();
			      });
			      
				}
			})
		}
	}
	
	$scope.activarDesactivarDispositivo = function(activarDesactivarDispositivo){		
		consultaDispService.activarDesactivarDispositivo(activarDesactivarDispositivo.id,!activarDesactivarDispositivo.active)
			.success(function(data){
				// el compoente switch ya se encarga de hacer el cambio
				//activarDesactivarDispositivo.active = !activarDesactivarDispositivo.active;
			}).error(function(err){
				activarDesactivarDispositivo.active = activarDesactivarDispositivo.active;
			});
	}
	
	
	$scope.eliminar = function(id,$event){		
		setTimeout(function(){			
			ModalService.showModal({
		    	templateUrl: 'views/templatemodal/templateModalConfirmacion.html',
		        controller: 'mensajeModalController',
		        	inputs:{ message: "¿Esta seguro de eliminar el dispositivo?"}
		    }).then(function(modal) {
		        modal.element.modal();
		        modal.close.then(function(result) {
		        	if(result){
		        		consultaDispService.eliminarDispositivo(id).success(function(data){		        			
		        			$event.target.parentElement.parentElement.remove();
		        			growl.success($scope.mensajeModal('El dispositivo se elimnó correctamente'));
		        		}).error(function(err){
		        			console.error(err);
		        			growl.error($scope.mensajeModal('No se pudo eliminar el usuario'));
		        		});
		        	}
		        }); 
		    });			
		})		
	}
	
	
	$scope.changeBusqueda = function(){
		$scope.parametroBusqueda.valor='';		
		if($scope.listaBusqueda.valor.id === 5){
			angular.element('#tippBusqueda').addClass('col-sm-offset-3');
		} else{
			angular.element('#tippBusqueda').removeClass('col-sm-offset-3');
		}
	}
	
	$scope.listaBusqueda={}
	$scope.listaBusqueda.lista = [];
	CatalogoService.catBusuqedaDispositivo().success(function(data){
		
		$scope.listaBusqueda.lista = data;
		angular.element("#select2-tipoBusqueda-container").html(data[0].nombre);
		$scope.myMarca = {id:1}
		
		if(window.saveBack === null || window.saveBack === undefined){
			/**
			 * Marca
			 */
			CatalogoService.catalogoMarca().success(function(data){		
				$scope.marca=data;
				$scope.myMarca=data[0];	
				angular.element("#select2-smarca-container").html($scope.myMarca.nombre);
				$scope.loadModelo();
			}).error(function(){
				console.log("error");
			});
		}
		
		/**
		 * Modelo
		 */
		$scope.loadModelo = function(){
			CatalogoService.catalogoMarcaPorModelo($scope.myMarca.id).success(function(data){
				$scope.modelo=data;
				$scope.myModelo=data[0]
				angular.element("#select2-smodelo-container").html($scope.myModelo.nombre);
			}).error(function(error){
				console.log("error");
			})
		}
		
		requiredFields = function(form){    
			angular.forEach(form.$error, function (field) {
	            	angular.forEach(field, function(errorField){
	            	errorField.$setDirty();
	            })
			});
		}
		
		$scope.listaBusqueda.valor = $scope.listaBusqueda.lista[0];
		$scope.parametroBusqueda ={
				valor:''
		}
			
		if(window.saveBack && window.saveConsultar && window.saveConsultar.tipo){
				$scope.webComponentes= window.saveConsultar.webComponentes;
				var dd = window.saveConsultar;
				
				// sellecionar el id de select options
				$scope.listaBusqueda.valor = $scope.listaBusqueda.lista[dd.tipo.id-1];				
				
				// nombre o imei
				if(dd.tipo.id == 1 || dd.tipo.id == 2 ){				
					$scope.parametroBusqueda.valor=dd.valor;
					
					consultaDispService.consultarDispositivos(dd.tipo.id,window.saveConsultar.valor).success(function(data){
						$scope.dispositivos = data;					
					})
					
					/**
					 * Marca
					 */
					CatalogoService.catalogoMarca().success(function(data){		
						$scope.marca=data;
						$scope.myMarca=data[0];
						angular.element("#select2-smarca-container").html($scope.myMarca.nombre);
						/**
						 * Modelo
						 */				
						CatalogoService.catalogoMarcaPorModelo($scope.myMarca.id).success(function(data){
							$scope.modelo=data;
							$scope.myModelo=data[0]
						}).error(function(error){
							console.log("error");
						})	
						
					}).error(function(){
						console.log("error");
					});		
				}
				// marca
				else if (dd.tipo.id == 3){
					consultaDispService.consultarDispositivos(dd.tipo.id,window.saveConsultar.myMarca.nombre).success(function(data){
						$scope.dispositivos = data;					
					})
					/**
					 * Marca
					 */
					CatalogoService.catalogoMarca().success(function(data){					
						var tmp=data[i],i;
						for(i=0;i<data.length;i++){
							if(data[i].nombre === dd.myMarca.nombre){
								tmp = data[i];
							}
						}
						$scope.marca=data;
						$scope.myMarca=tmp;
						angular.element("#select2-smarca-container").html($scope.myMarca.nombre);
						
						/**
						 * Modelo
						 */
						CatalogoService.catalogoMarcaPorModelo($scope.myMarca.id).success(function(data){
							var tmp=data[i],i;
							for(i=0;i<data.length;i++){
								if(data[i].nombre === dd.myModel.nombre){
									tmp = data[i];
								}
							}
							$scope.modelo=data;
							$scope.myModelo=tmp;
							angular.element("#select2-smodelo-container").html($scope.myModelo.nombre);
						}).error(function(error){
							console.log("error");
						})
						
						
					}).error(function(){
						console.log("error");
					});
				}
				// modelo
				else if (dd.tipo.id == 4){
					
					consultaDispService.consultarDispositivos(dd.tipo.id,window.saveConsultar.myModel.nombre).success(function(data){
						$scope.dispositivos = data;					
					})
					
					/**
					 * Marca
					 */
					CatalogoService.catalogoMarca().success(function(data){					
						var tmp=data[i],i;
						for(i=0;i<data.length;i++){
							if(data[i].nombre === dd.myMarca.nombre){
								tmp = data[i];
							}
						}
						$scope.marca=data;
						$scope.myMarca=tmp;
						angular.element("#select2-smarca-container").html($scope.myMarca.nombre);
						
						/**
						 * Modelo
						 */
						CatalogoService.catalogoMarcaPorModelo($scope.myMarca.id).success(function(data){
							var tmp=data[i],i;
							for(i=0;i<data.length;i++){
								if(data[i].nombre === dd.myModel.nombre){
									tmp = data[i];
								}
							}
							$scope.modelo=data;
							$scope.myModelo=tmp;
							angular.element("#select2-smodelo-container").html($scope.myModelo.nombre);
						}).error(function(error){
							console.log("error");
						})
						
					}).error(function(){
						console.log("error");
					});
				}
				
				// todos
				else if(dd.tipo.id == 5){
					$scope.parametroBusqueda.valor=dd.valor;
					angular.element("#select2-tipoBusqueda-container").html(window.saveConsultar.listaBusqueda.nombre);
					consultaDispService.consultarDispositivos(dd.tipo.id,'').success(function(data){
						$scope.dispositivos = data;					
					})
					angular.element('#tippBusqueda').addClass('col-sm-offset-3');
					
					/**
					 * Marca
					 */
					CatalogoService.catalogoMarca().success(function(data){		
						$scope.marca=data;
						$scope.myMarca=data[0];	
						angular.element("#select2-smarca-container").html($scope.myMarca.nombre);
						$scope.loadModelo();
					}).error(function(){
						console.log("error");
					});
				}
			}
		
	}).error(function(){
		console.log("error");
	});
	
});