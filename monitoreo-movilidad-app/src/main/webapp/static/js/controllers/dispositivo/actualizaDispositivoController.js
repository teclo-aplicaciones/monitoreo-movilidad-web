angular.module(appTeclo).controller(
				"actualizaDispositivoController",
				function($scope, $filter, altaDispositivoService, $rootScope,
						$timeout, $location, $localStorage, $document,$routeParams,
						configAppService, growl, showAlert, ModalService,
						EmpleadoService, CatalogoService,
						altaDispositivoService,consultaDispService) {
									
					 consultaDispService.consultarDispositivo($routeParams.id).success(function(dataObj){						 
					
							$scope.tipoDispositivoVO = {
									idDispositivo:dataObj.idDispositivo,
									nombre:dataObj.nombre,
									//tipo:1,
									//tipoSubtipo:1,
									//marca:1,
									//tipModelo:1,
									idEmpleado:dataObj.idEmpleado,
									imei:dataObj.imei,
									nuIp:dataObj.nuIp,
									txDispositivo:dataObj.txDispositivo,
									nuCelularEmpresa:dataObj.nuCelularEmpresa,
									nuCelularPersonal:dataObj.nuCelularPersonal,
									serie:dataObj.serie,
									empleado:dataObj.nombreEmpleado
							};
							
							if($scope.tipoDispositivoVO.idEmpleado){
								$scope.showCloseEmpleado = true;	
							}else{
								$scope.showCloseEmpleado = false;
							}

							
							$scope.subtipo = true;
							$scope.marca = true;
							$scope.modelo = true;
							$scope.usuarioValida = false;
							$scope.listaTipoDisp = [];
							$scope.listaSubTipo = [];
							$scope.listaMarcas = [];
							$scope.listaMoelos = [];
							$scope.showModal = false;
							$scope.templateModalVO = {
									idEmpleado:	dataObj.idEmpleado									
							};
							$scope.selectListEmpleados = [];

							$scope.selectList = [ {
								"idOpcion" : 1,
								"nbOpcion" : 'Codigo Empleado',
								param : 'cdEmpleado'
							}, {
								"idOpcion" : 2,
								"nbOpcion" : 'Nombre',
								param : 'nombreEmpleado'
							}, {
								"idOpcion" : 3,
								"nbOpcion" : 'Apellido',
								param : 'apellidoEmpleado'
							}, {
								"idOpcion" : 4,
								"nbOpcion" : 'Email',
								param : 'emailEmpleado'
							}, {
								"idOpcion" : 5,
								"nbOpcion" : 'Telefono',
								param : 'telefonoEmpleado'
							} ];

							

							$scope.obtenerSubtipo = function() {
								if ($scope.tipoDispositivoVO.tipo) {
									CatalogoService.catalogoSubTipoDispositivo($scope.tipoDispositivoVO.tipo.id).success(
										function(data) {
											$scope.listaSubTipo = data
											
											if($scope.tipoDispositivoVO.tipo){
												$scope.subtipo =  false;
											}else{
												$scope.subtipo =  true;
											}
											
										}).error(function() {
											console.log("error");
										});
								}
									
								$scope.marca = true;
								$scope.modelo = true;
								
								$scope.tipoDispositivoVO.tipoSubtipo = "";							
								var select = $('#select2-subtipo-container');
								select.attr('title','​Seleccione');
								select.text('Seleccione');		
								
								$scope.tipoDispositivoVO.marca = "";
								var select = $('#select2-marca666-container');
								select.attr('title','​Seleccione');
								select.text('Seleccione');
								
								$scope.tipoDispositivoVO.tipModelo = "";
								var select = $('#select2-model333-container');
								select.attr('title','​Seleccione');
								select.text('Seleccione');
							}
							
						
							
							$scope.obtenerMarca = function() {
								if ($scope.tipoDispositivoVO.tipoSubtipo) {
									CatalogoService.catalogoMarcaDispositivo($scope.tipoDispositivoVO.tipoSubtipo.id).success(
										function(data) {
											$scope.marcasFiltradas = data;
											
											if($scope.tipoDispositivoVO.tipoSubtipo){
												$scope.marca = false;												
											}else{
												$scope.marca = true;
											}
											
											
										}).error(function() {
											console.log("error");
										});
								}
								$scope.marca = true;
								$scope.modelo = true;
								
								$scope.tipoDispositivoVO.marca = "";
								var select = $('#select2-marca666-container');
								select.attr('title','​Seleccione');
								select.text('Seleccione');
								
								$scope.tipoDispositivoVO.tipModelo = "";
								var select = $('#select2-model333-container');
								select.attr('title','​Seleccione');
								select.text('Seleccione');								
							}
							
							$scope.obtenerModelos = function() {
								if ($scope.tipoDispositivoVO.marca) {
									CatalogoService.catalogoModeloDispositivo(
													$scope.tipoDispositivoVO.tipo.id,
													$scope.tipoDispositivoVO.marca.id,
													$scope.tipoDispositivoVO.tipoSubtipo.id).success(
											function(data) {
												$scope.modelosFiltrados = data;
												
												if($scope.tipoDispositivoVO.marca){
													$scope.modelo = false;													
												}else{
													$scope.marca = true;
												}
												
												
												
											}).error(function(error) {
												console.log("error");
											})
								} 
								$scope.modelo = true;
								
								$scope.tipoDispositivoVO.tipModelo = "";
								var select = $('#select2-model333-container');
								select.attr('title', '​Seleccione');
								select.text('Seleccione');								
							}
							
							$scope.listaMarcas = altaDispositivoService.ListaMarcas();
							$scope.listaModelo = altaDispositivoService.listaModelo();
							
							/**
							 * The load values
							 */							
							
							CatalogoService.catalogoTipoDispositivo().success(function(data) {	
								$scope.listaTipoDisp = data;
								
								var tmpObj=[]
								angular.forEach(data, function(value, key) {
									if(value.id === dataObj.tipo){
										this.push(value)
									}												  
								}, tmpObj);											
								$scope.tipoDispositivoVO.tipo = tmpObj[0];	
								angular.element("#select2-tipDisp-container").html($scope.tipoDispositivoVO.tipo.nombre);
								
							})							
															
							CatalogoService.catalogoSubTipoDispositivo(dataObj.tipo).success(function(data) {
								$scope.listaSubTipo = data
								$scope.subtipo =  false;
									
								var tmpObj=[]
								angular.forEach(data, function(value, key) {
									if(value.id === dataObj.subtipo){
										this.push(value)
									}												  
								}, tmpObj);
								$scope.tipoDispositivoVO.tipoSubtipo = tmpObj[0];	
								angular.element("#select2-subtipo-container").html($scope.tipoDispositivoVO.tipoSubtipo.nombre);								
							}).error(function() {
								console.log("error");
							});					
						
							CatalogoService.catalogoMarcaDispositivo(dataObj.subtipo).success(function(data) {
								$scope.marcasFiltradas = data;
								$scope.marca = false;	
									
								var tmpObj=[]
								angular.forEach(data, function(value, key) {
									if(value.id === dataObj.marca){
										this.push(value)
									}												  
								}, tmpObj);
								$scope.tipoDispositivoVO.marca = tmpObj[0];	
								angular.element("#select2-marca666-container").html($scope.tipoDispositivoVO.marca.nombre);
							}).error(function() {
								console.log("error");
							});
															
							CatalogoService.catalogoModeloDispositivo(dataObj.tipo,dataObj.marca,dataObj.subtipo).success(function(data) {
								$scope.modelosFiltrados = data;
								$scope.modelo = false;	
									
								var tmpObj=[]
								angular.forEach(data, function(value, key) {
									if(value.id === dataObj.modelo){
										this.push(value)
									}												  
								}, tmpObj);
								$scope.tipoDispositivoVO.tipModelo = tmpObj[0];		
								angular.element("#select2-model333-container").html($scope.tipoDispositivoVO.tipModelo.nombre);								
																	
							}).error(function(error) {
								console.log("error");
							})
							
							
							/**
							 * Events
							 */
							
							$scope.saveModalInculde = function() {
								if ($scope.formTemplateModal.$invalid) {
									angular.forEach($scope.formTemplateModal.$error,
											function(field) {
												angular.forEach(field, function(
														errorField) {
													errorField.$setDirty();
												})
											});							
								} else {
									$scope.showCloseEmpleado = true;
									if ($scope.templateModalVO.empleadoSeleccionado.idEmpleado) {
										growl.success($scope.mensajeModal('Usuario Seleccionado'), {
											title : $scope.mensajeModal('Éxito')
										});
										$scope.tipoDispositivoVO.empleado = $scope.templateModalVO.empleadoSeleccionado.nbEmpleado
												+ " "
												+ $scope.templateModalVO.empleadoSeleccionado.nbPaterno
												+ " "
												+ $scope.templateModalVO.empleadoSeleccionado.nbMaterno;
										ModalService.closeModal();
									} else {
										growl.error($scope.mensajeModal('El usuario no ha sido seleccionado'));
										// showAlert.aviso("No ha seleccionado una opción");
									}
								}								

							}
							
							$scope.buscar = function() {
								var dato;
								var statusBuscar = true;
								if ($scope.formTemplateModal.$invalid) {
									showAlert.requiredFields($scope.formTemplateModal);
									growl.error($scope.mensajeModal('Formulario incompleto'));
									// showAlert.error("Formulario incompleto");
								} else {
									if ($scope.templateModalVO.busquedaTipo.param == "telefonoEmpleado") {
										if (isNaN(Number($scope.templateModalVO.txtBusqueda))) {
											statusBuscar = false;
											growl.error($scope.mensajeModal('Ingrese solo numeros'));
											// showAlert.aviso("Ingrese solo numeros");
										}
									}

									if (statusBuscar) {
										EmpleadoService.buscarUsuario($scope.templateModalVO.busquedaTipo.param,$scope.templateModalVO.txtBusqueda)
											.success(function(data) {
													$scope.templateModalVO.empleadoSeleccionado = {};
													$('#select2-empleadoSeleccionado-container').text("Seleccione");
													$scope.selectListEmpleados = data;
													if (data.length < 1)
														growl.error($scope.mensajeModal('No se encontraron coincidencias'));
													// showAlert.aviso("No se
													// encontraron
													// coincidencias");
	
												}).error(function(err) {
												console.log("error");
											});
									}
								}
							}
							
							$scope.showAviso = function(messageTo) {
								ModalService.showModal({
													templateUrl : 'views/templatemodal/templateModalAviso.html',
													controller : 'mensajeModalController',
													inputs : {
														message : messageTo
													}
												}).then(function(modal) {
											modal.element.modal();
										});
							};

							$scope.validaUsuario = function() {
								EmpleadoService.validarUsuario(5).success(function(data) {
										if (data)
											$scope.usuarioValida = !$scope.usuarioValida;
									}).error(function(error) {
								});
							}

							$scope.cambiaUsuario = function() {
								$scope.usuarioValida = false;
							}
							
							$scope.removerUsuario = function() {
								$scope.showCloseEmpleado = false;
								delete $scope.tipoDispositivoVO.empleado; 
								delete $scope.tipoDispositivoVO.idEmpleado;
							}

							$scope.newDispositivo = function(tipoDispositivoVO) {
								if ($scope.altaDispositivo.$invalid) {
									angular.forEach($scope.altaDispositivo.$error,
											function(field) {
												angular.forEach(field, function(
														errorField) {
													errorField.$setDirty();
												})
											});
									showAlert.aviso($scope.mensajeModal("Formulario Incompleto"));
								} else {
									
									if ($scope.templateModalVO.empleadoSeleccionado)
										$scope.tipoDispositivoVO.idEmpleado = $scope.templateModalVO.empleadoSeleccionado.idEmpleado;									
												
										ModalService.showModal({
									    	templateUrl: 'views/templatemodal/templateModalConfirmacion.html',
									        controller: 'mensajeModalController',
									        	inputs:{ message: $scope.mensajeModal("¿Esta seguro de actualizar el dispositivo?")}
									    }).then(function(modal) {
									        modal.element.modal();
									        modal.close.then(function(result) {
									        	if(result){
									        		altaDispositivoService.actualizarDispositivo($scope.tipoDispositivoVO)
										        		.success(function(data) {										
															console.log(data);
															showAlert.aviso($scope.mensajeModal("Dispositivo actualizado"));										
														}).error(function(err) {
															showAlert.error($scope.mensajeModal("Ocurrio un problema al guardar"));												
														});
									        	}
									        }); 
									    });											
								}
							}			
						}).error(function(err){
							//alert("error");
						});				 
					 
				});