angular.module(appTeclo).controller(
				'nuevoDispositivoController',
				function($scope, $filter, altaDispositivoService, $rootScope,
						$timeout, $location, $localStorage, $document,$translate,
						configAppService, growl, showAlert, ModalService, 
						EmpleadoService, CatalogoService, altaDispositivoService) {
					
					$scope.subtipo=true;
					$scope.marca =true;
					$scope.modelo = true;
					$scope.usuarioValida = false;
					$scope.listaTipoDisp = [];
					$scope.listaSubTipo = [];
					$scope.listaMarcas = [];
					$scope.listaMoelos = [];
					$scope.tipoDispositivoVO = {};
					
					$scope.selectList = [ {
						"idOpcion" : 1,
						"nbOpcion" : 'Codigo Empleado',
						param : 'cdEmpleado'
					}, {
						"idOpcion" : 2,
						"nbOpcion" : 'Nombre',
						param : 'nombreEmpleado'
					}, {
						"idOpcion" : 3,
						"nbOpcion" : 'Apellido',
						param : 'apellidoEmpleado'
					}, {
						"idOpcion" : 4,
						"nbOpcion" : 'Email',
						param : 'emailEmpleado'
					}, {
						"idOpcion" : 5,
						"nbOpcion" : 'Telefono',
						param : 'telefonoEmpleado'
					} ];


					$scope.traducir = function(callback){
						$translate(['DISPOSITIVOS.Nuevo.nombreBox.tipoDispositivo.seleccionar']).then(function (headline) {
						    $scope.translate ={
						    		seleccione: headline['DISPOSITIVOS.Nuevo.nombreBox.tipoDispositivo.seleccionar']    	 
						    		
						    }
						    if(callback)
						    	callback()
						}, function (translationId) {
						    $scope.headline = translationId;
						});
					}				
					
					$scope.showModal = false;
					$scope.templateModalVO = {};
					$scope.selectListEmpleados = [];
										
					$scope.saveModalInculde = function() {
						
						if ($scope.formTemplateModal.$invalid) {
							angular.forEach($scope.formTemplateModal.$error,
									function(field) {
										angular.forEach(field, function(
												errorField) {
											errorField.$setDirty();
										})
									});							
						} else {
							if($scope.templateModalVO.empleadoSeleccionado.idEmpleado){
								growl.success($scope.mensajeModal('Usuario Seleccionado'), {title : $scope.mensajeModal('Éxito')});
								$scope.tipoDispositivoVO.empleado = $scope.templateModalVO.empleadoSeleccionado.nbEmpleado + " " + 
								$scope.templateModalVO.empleadoSeleccionado.nbPaterno + " " +
								$scope.templateModalVO.empleadoSeleccionado.nbMaterno;
								ModalService.closeModal();
								$scope.showCloseEmpleado = true; 
							}else{
								growl.error($scope.mensajeModal('El usuario no ha sido seleccionado'));
							}
						}
					}
					
					
					$scope.buscar = function() {
						var dato;
						var statusBuscar = true;
						if($scope.formTemplateModal.$invalid) {
							showAlert.requiredFields($scope.formTemplateModal);
							growl.error($scope.mensajeModal('Formulario incompleto'));
						} 
						else {
							if($scope.templateModalVO.busquedaTipo.param == "telefonoEmpleado"){
								if(isNaN(Number($scope.templateModalVO.txtBusqueda))){
									statusBuscar = false;
									growl.error($scope.mensajeModal('Ingrese solo numeros'));
								}
							}
							
							if(statusBuscar){
								EmpleadoService.buscarUsuario($scope.templateModalVO.busquedaTipo.param, $scope.templateModalVO.txtBusqueda).success(function(data){
									$scope.templateModalVO.empleadoSeleccionado = {};
									$('#select2-empleadoSeleccionado-container').text("Seleccione");
									$scope.selectListEmpleados = data;
									
								}).error(function(err){
									console.log("error");
								});
							}
						}
					}
					
					
					$scope.obtenerSubtipo = function(){
						if($scope.tipoDispositivoVO.tipo){
							CatalogoService.catalogoSubTipoDispositivo($scope.tipoDispositivoVO.tipo.id).success(function(data){
								$scope.listaSubTipo = data
								$scope.subtipo = $scope.tipoDispositivoVO.tipo ? false : true;
								$('#select2-subtipo-container').text("Seleccione");
								$('#select2-marca666-container').text("Seleccione");
								$('#select2-model333-container').text("Seleccione");
							}).error(function(){
								console.log("error");
							});
						}else{
							$scope.subtipo = true;
							$scope.marca = true;
							$scope.modelo = true;
							
							$scope.tipoDispositivoVO.tipoSubtipo = "";							
							var select = $('#select2-subtipo-container');
							select.attr('title','​Seleccione');
							select.text('Seleccione');		
							
							$scope.tipoDispositivoVO.marca = "";
							var select = $('#select2-marca666-container');
							select.attr('title','​Seleccione');
							select.text('Seleccione');
							
							$scope.tipoDispositivoVO.tipModelo = "";
							var select = $('#select2-model333-container');
							select.attr('title','​Seleccione');
							select.text('Seleccione');
						}
					}

					$scope.obtenerMarca = function() {
						if($scope.tipoDispositivoVO.tipoSubtipo){
							CatalogoService.catalogoMarcaDispositivo($scope.tipoDispositivoVO.tipoSubtipo.id).success(function(data){
								$scope.marcasFiltradas = data;
								$scope.marca = $scope.tipoDispositivoVO.tipoSubtipo ? false : true;
								
								$scope.listaTipoDisp.tipo = $scope.listaTipoDisp[0];
								
								$('#select2-marca666-container').text("Seleccione");
								$('#select2-model333-container').text("Seleccione");
								
						}).error(function(){
								console.log("error");
								$('#select2-marca666-container').text("Seleccione");
								$('#select2-model333-container').text("Seleccione");
							});
						}else{
							$scope.marca = true;
							$scope.modelo = true;
							
							$scope.tipoDispositivoVO.marca = "";
							var select = $('#select2-marca666-container');
							select.attr('title','​Seleccione');
							select.text('Seleccione');
							
							$scope.tipoDispositivoVO.tipModelo = "";
							var select = $('#select2-model333-container');
							select.attr('title','​Seleccione');
							select.text('Seleccione');
						}
					}
					$scope.obtenerModelos = function() {
						if($scope.tipoDispositivoVO.marca){
							CatalogoService.catalogoModeloDispositivo($scope.tipoDispositivoVO.tipo.id,
																	  $scope.tipoDispositivoVO.marca.id,
																	  $scope.tipoDispositivoVO.tipoSubtipo.id
							).success(function(data){
								$scope.modelosFiltrados = data;
								$scope.modelo = $scope.tipoDispositivoVO.marca ? false : true;
								
								$scope.listaTipoDisp.tipo = $scope.listaTipoDisp[0];
								$('#select2-model333-container').text("Seleccione");
							}).error(function(error){
								console.log("error");
								$('#select2-model333-container').text("Seleccione");
							})
						}else{
							$scope.modelo = true;
							
							$scope.tipoDispositivoVO.tipModelo = "";
							var select = $('#select2-model333-container');
							select.attr('title','​Seleccione');
							select.text('Seleccione');
						}
					}
					
					$scope.showAviso = function(messageTo) {
						ModalService.showModal({
											templateUrl : 'views/templatemodal/templateModalAviso.html',
											controller : 'mensajeModalController',
											inputs : {
												message : messageTo
											}
										}).then(function(modal) {
									modal.element.modal();
								});
					};

					listTipDisp = function() {
						CatalogoService.catalogoTipoDispositivo().success(function(data){
							$scope.listaTipoDisp = data;
							$scope.listaTipoDisp.tipo = $scope.listaTipoDisp[0];
							
 						}).error(function(err){
							console.log("error");
						});
						
					}

					listTipMarca = function() {
						
						$scope.listaMarcas = altaDispositivoService
								.ListaMarcas();
					}

					listTipModelo = function() {
						$scope.listaModelo = altaDispositivoService
								.listaModelo();
					}
					
					$scope.validaUsuario = function(){
						EmpleadoService.validarUsuario(5).success(function(data){
							if(data)
								$scope.usuarioValida = !$scope.usuarioValida;
						}).error(function(error){
							
						});
					}
					
					$scope.cambiaUsuario = function(){						
						$scope.usuarioValida = false;
					}
					
					$scope.removerUsuario = function() {
						$scope.showCloseEmpleado = false;
						delete $scope.tipoDispositivoVO.empleado; 
						delete $scope.tipoDispositivoVO.idEmpleado;
					}

					$scope.newDispositivo = function(tipoDispositivoVO) {
						if ($scope.altaDispositivo.$invalid) {
							angular.forEach($scope.altaDispositivo.$error,
									function(field) {
										angular.forEach(field, function(
												errorField) {
											errorField.$setDirty();
										})
									});
							showAlert.aviso($scope.mensajeModal($scope.mensajeModal("Formulario Incompleto")));
						} else {
							if($scope.templateModalVO.empleadoSeleccionado)
								$scope.tipoDispositivoVO.idEmpleado = $scope.templateModalVO.empleadoSeleccionado.idEmpleado;
							else
								$scope.tipoDispositivoVO.idEmpleado = -1; // no se le asigna a un empleado
							altaDispositivoService.guardarDispositivo($scope.tipoDispositivoVO).success(function(data){
								console.log(data);
								if(data.idDispositivo > 0){
									showAlert.aviso($scope.mensajeModal("Registro agregado correctamente"));
									$scope.tipoDispositivoVO={};
									$scope.altaDispositivo.$setPristine();
									$('#select2-tipDisp-container').text("Seleccione");
									$('#select2-subtipo-container').text("Seleccione");
									$('#select2-marca666-container').text("Seleccione");
									$('#select2-model333-container').text("Seleccione");
									$('#select2-model333-container').text("Seleccione");
									$scope.subtipo = true;
									$scope.marca=true;
									$scope.modelo=true;
									$scope.templateModalVO = {};
									$scope.formTemplateModal.$setPristine();
									$scope.selectListEmpleados.length = 0;
									$('#select2-empleadoSeleccionado-container').text("Seleccione");
									$('#select2-busquedaTipo-container').text("Seleccione");
								}else{
									if(data.idDispositivo == 0)
										showAlert.error($scope.mensajeModal("Ya existe registro con el IMEI"));
									else
										showAlert.error($scope.mensajeModal("Ocurrio un problema al guardar"));
								}
							}).error(function(err){
								showAlert.error($scope.mensajeModal("Ocurrio un problema al guardar"));
								console.log("error");
							});
							
						
						}

					}

					listTipDisp();
					listTipMarca();
					listTipModelo();
					$scope.traducir();

				});