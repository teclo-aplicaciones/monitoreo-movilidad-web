/*
 * Author: Sail Ponce
 * Directive: mapa
 * Versión: 1.0.0
 */

angular.module(appTeclo).directive('mapForDevice', function ($rootScope) {
	return{
			restrict: 'E',
			template:'<div>&nbsp;</div>',
			replace: true,
			link:function(scope, element, attr) {
				var cdmx = {lat: 19.4125882, lng: -99.1403691};
		        var myLatLng = new google.maps.LatLng(cdmx.lat, cdmx.lng);
		        
		        var listPoints = [];
		        
		        var marker = {
		          center: myLatLng,
		          zoom: 12,
		          mapTypeId: google.maps.MapTypeId.ROADMAP,
		          disableDoubleClickZoom: false,
		          streetViewControl: false,
		          styles: [
		              {elementType: 'geometry', stylers: [{color: '#efeaea'}]},
		              {elementType: 'labels.text.stroke', stylers: [{color: '#efeaea'}]},
		              {elementType: 'labels.text.fill', stylers: [{color: '#896969'}]},
		              {
		                featureType: 'administrative.locality',
		                elementType: 'labels.text.fill',
		                stylers: [{color: '#896969'}]
		              },
		              {
		            	  featureType: 'poi',
			              elementType: 'labels.text.fill',
			              stylers: [{color: '#4074b4'}]
			          },
		              {
		                featureType: 'poi.park',
		                elementType: 'geometry',
		                stylers: [{color: '#c0ecae'}]
		              },
		              {
		                featureType: 'poi.park',
		                elementType: 'labels.text.fill',
		                stylers: [{color: '#50984e'}]
		              },
		              {
		                featureType: 'road',
		                elementType: 'geometry',
		                stylers: [{color: '#ffffff'}]
		              },
		              {
		                featureType: 'road',
		                elementType: 'geometry.stroke',
		                //stylers: [{color: '#ffc1c1'}]
		                stylers: [{color: '#ffe2e2'}]
		              },
		              {
		                featureType: 'road',
		                elementType: 'labels.text.fill',
		                stylers: [{color: '#896969'}]
		              },
		              {
		                featureType: 'road.highway',
		                elementType: 'geometry',
		                //stylers: [{color: '#fea7a7'}]
		                stylers: [{color: '#ffc0c0'}]
		              },
		              {
		                featureType: 'road.highway',
		                elementType: 'geometry.stroke',
		                //stylers: [{color: '#f66464'}]#f67f7f
		                stylers: [{color: '#f67f7f'}]
		              },
		              {
		                featureType: 'road.highway',
		                elementType: 'labels.text.fill',
		                stylers: [{color: '#770303'}]
		              },
		              {
		                featureType: 'transit',
		                elementType: 'geometry',
		                stylers: [{color: '#e5dede'}]
		              },
		              {
		                featureType: 'transit.station',
		                elementType: 'labels.text.fill',
		                stylers: [{color: '#854848'}]
		              },
		              {
		                featureType: 'water',
		                elementType: 'geometry',
		                stylers: [{color: '#a8c9ff'}]
		              },
		              {
		                featureType: 'water',
		                elementType: 'labels.text.fill',
		                stylers: [{color: '#515c6d'}]
		              },
		              {
		                featureType: 'water',
		                elementType: 'labels.text.stroke',
		                stylers: [{color: '#17263c'}]
		              }
		            ]
		        };
		        
		        var map = new google.maps.Map(document.getElementById(attr.id), marker);
		        
		        function showDataInMap(data){
		        	listPoints.push({lat:data.lat, lng:data.lng});
		        	
		        	var ruta = new google.maps.Polyline({
		                path: listPoints,
		                geodesic: true,
		                strokeColor: '#FF0000',
		                strokeOpacity: 1.0,
		                strokeWeight: 2
		              });
		        	
		        	var latLng = new google.maps.LatLng(data.lat,data.lng);
		        	var marker = new google.maps.Marker({
		                position: latLng,
		                map: map
		              });
		        	
		        	ruta.setMap(map);
		        }
		        
		        if(attr.enablepointers === "true"){
			        //Listener del mapa, detectara los clicks del usuario
			        map.addListener('click', function(e) {
			          var data = {lat:0, lng:0};
			          data.lat = e.latLng.lat();
			          data.lng = e.latLng.lng();
			          showDataInMap(data);
			        });
		        }
			}
		}
	});