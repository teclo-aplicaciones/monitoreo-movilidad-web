/*
 * Author: Sail Ponce
 * Directive: mapa
 * Versión: 1.0.0
 */

angular.module(appTeclo).directive('mapForDevice', function ($rootScope, $interval) {
	return{
			restrict: 'E',
			template:'<div>&nbsp;</div>',
			scope:{
				color:'=',
				type:'=',
				model:'=',
				shape:'@',
				enabletracking:'=',
				enablerealtime:'=',
				allowevents:'=',
				out:'='
			},
			replace: true,
			link:function(scope, element, attr) {
				var cdmx = {lat: 19.410, lng: -99.133};
		        
				var lockModalByAutoUpdate =false;
				var shape = 'round';
				
				var tracking;
				
				var points = 0;
				var flagCircleLock = false;
		        var listTracks = [];
		        var listTrackMarkers = [];
		        var listTrackGhost = [];
		        
		        
		        //Creacion del mapa(Configuraciones)
		        var maps = L.map(element[0], {
		            zoomControl: false,
		            //... other options
		        }).setView([cdmx.lat, cdmx.lng],12);
		        //L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
		        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/cjig9ri4i3vhe2sqpq67sah0y/tiles/256/{z}/{x}/{y}?access_token={accessToken}', {
		        	attribution: 'Monitoring powered by <a href="http://teclomexicana.mx/">Teclo Mexicana</a> &copy;',
		        	maxZoom: 20,
		        	//id: 'mapbox.streets',
		        	id: 'apescape16',
		        	accessToken: 'pk.eyJ1IjoiYXBlc2NhcGUxNiIsImEiOiJjamlnOGh2b3owcGM4M3dxcHR4dHgyMDhjIn0.LBU5k6cj4O0_d4wlTdbLRg'
		        }).addTo(maps);
		        
		        //Cambia la posicion del control del Zoom
		        L.control.zoom({
		             position:'bottomright'
		        }).addTo(maps);
		        
		        maps.on('unload', function(){
		        	map.remove();
		        });
  
		        
		        function showDataInMap(data, isNotDragging){
		        	
		        	var icon = L.divIcon({
		        		  iconSize: [16, 16],
		        		  iconAnchor: [8, 8],
		        		  popupAnchor: [1, 0],
		        		  shadowSize: [0, 0],
		        		  className: 'mapMarker',
		        		  html: ''
		        		});
		        	
		        	var iconGH = L.divIcon({
		        		  iconSize: [16, 16],
		        		  iconAnchor: [8, 8],
		        		  popupAnchor: [1, 0],
		        		  shadowSize: [0, 0],
		        		  className: 'ghostMarker',
		        		  html: ''
		        		});
		        	
		        	var iconRT = L.divIcon({
		        		  iconSize: [16, 16],
		        		  iconAnchor: [8, 8],
		        		  popupAnchor: [1, 0],
		        		  shadowSize: [0, 0],
		        		  className: 'mapMarkerRT',
		        		  html: ''
		        		});
		        	
		        	if(data){
			        	if(listTracks[0]){
			        		if(flagCircleLock){
			        			var distance = 0;
			        			var point1 = listTracks[0].coords[0];
			        			var point2 = [data.lat, data.lng];
			        			var lat = Math.abs((point2[0] * (Math.PI/180)) - (point1[0] * (Math.PI/180)));
			        			var lng = Math.abs((point2[1] * (Math.PI/180)) - (point1[1] * (Math.PI/180)));
			        			var a = (Math.sin(lat/2) * Math.sin(lat/2)) + (Math.cos(point1[0])) * (Math.cos(point2[0])) * (Math.sin(lng/2) * Math.sin(lng/2))  
			        			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
			        			
			        			distance = 6378000 * c; 
			        			
			        			listTracks[0].coords.push(distance); //Convert Km to meters
			        		}else{
			        			listTracks[0].coords.push([data.lat, data.lng]);
			        		}
			        		
			        		
			        	}else{
			        		listTracks[0]= { id:1,
			        						 coords:[[data.lat, data.lng]],
			        						 route:{}
				        	};
			        		if(scope.type == 1) flagCircleLock = !flagCircleLock;
			        	}
		        	
			        	if(attr.enablepointers === "true"){
			        		if (flagCircleLock && listTracks[0].coords.length>1){
			        			flagCircleLock = !flagCircleLock
			        		}else{
					        	var ll = L.latLng(data.lat,data.lng);
				        		var marker = L.marker(ll, {
				        			icon:icon,
				        			title: points,
				        			draggable:scope.type != 1 ? attr.allowdraggable === "true" ? true:false:false
				        		});
				        		
				        		var ghost;
				        		var resultado;
				        		
				        		if(listTrackMarkers[0])
				        			if(listTrackMarkers[0].markers.length >0){
				        				var newCoord = recalculateGhostPos(listTracks[0].coords[listTracks[0].coords.length-1], listTracks[0].coords[listTracks[0].coords.length-2]);
				        				resultado=L.latLng(newCoord[0], newCoord[1]);
				        				ghost = L.marker(resultado, {
						        			icon:iconGH,
						        			title: points-1,
						        			draggable:scope.type != 1 ? attr.allowdraggable === "true" ? true:false:false
						        		});	
				        			}
				        		
				        		if(scope.type != 1){
				        			if(ghost){
				        				ghost.on('mousedown', function(e){
				        					var firstTempList = listaTracks[0].coords.filter(function(el, idx){ 
				        						return idx <= e.target.options.title
				        					});
				        					
				        					var lastTempList = listaTracks[0].coords.filter(function(el, idx){ 
				        						return idx > e.target.options.title
				        					});
				        					
				        					var newPoint =[e.target._latlng.lat,e.target._latlng.lng];
				        					 listTracks[0].coords = [];
				        					 
				        					 //Concatenamos la primera lista
				        					 listTracks[0].coords = listTracks[0].coords.concat(firstTempList);
				        					 
				        					//Interseccion del nuevo punto
				        					 listTracks[0].coords.push(newPoint);
				        					 
				        					//Concatenamos ultima lista
				        					 listTracks[0].coords = listTracks[0].coords.concat(lastTempList);
				        					 
				        					 showDataInMap(null, false);
				        				});
				        				
				        				ghost.on('mouseup', function(e){
				        					//AQUI CONVERTIMOS EL MARCADOR FANTASMA A UNO REAL...
				        					var newPoint =[e.target._latlng.lat,e.target._latlng.lng];
				        					var resultado=L.latLng(newPoint[0], newPoint[1]);
					        				var marker = L.marker(resultado, {
							        			icon:icon,
							        			title: listTrackMarkers[0].markers.length,
							        			draggable:scope.type != 1 ? attr.allowdraggable === "true" ? true:false:false
							        		});
					        				
					        				marker.on
					        				
				        					var firstTempList = listTrackMarkers[0].markers.filter(function(el, idx){ 
				        						return el.id <= e.target.options.title
				        					});
				        					
				        					var lastTempList = listTrackMarkers[0].markers.filter(function(el, idx){ 
				        						return el.id > e.target.options.title
				        					});
				        					
				        					
				        					listTrackMarkers[0].markers = [];
				        					
				        					//Concatenamos la primera lista
				        					listTrackMarkers[0].markers = listTrackMarkers[0].markers.concat(firstTempList);
				        					
				        					//Interseccion del nuevo punto
				        					listTrackMarkers[0].markers.push({id:listTrackMarkers[0].markers.length,coord:newPoint, point:marker});
				        					
				        					//Concatenamos ultima lista
				        					listTrackMarkers[0].markers = listTrackMarkers[0].markers.concat(lastTempList.map(function(el){
				        						el.id = el.id+1;
				        						return el;
				        					}));
				        					
				        					listTrackGhost[0].markers = listTrackGhost[0].markers.filter(function(el){return el.id != e.target.options.title});
				        					
				        					showDataInMap(null, true);
				        				});
				        				 
				        				ghost.on('drag', function(e){
					        				var idx;
						        			listTrackGhost[0].markers.map(function(el, index){
						        				
						        				if (el.id == e.target.options.title)
						        				{
						        					idx = index+1;
						        					return;
						        				}
						        			})
											
						        			//listTrackMarkers[0].markers[idx].coord=[e.latlng.lat, e.latlng.lng];
						        			listTracks[0].coords[idx] = [e.latlng.lat, e.latlng.lng];
						        			
						        			showDataInMap(null, false);
					        			});
				        			}
				        			marker.on('drag', function(e){
					        			var idx;
					        			listTrackMarkers[0].markers.map(function(el, index){
					        				
					        				if (el.id == e.target.options.title)
					        				{
					        					idx = index;
					        					return;
					        				}
					        			})
										
					        			//listTrackMarkers[0].markers[idx].coord=[e.latlng.lat, e.latlng.lng];
					        			listTracks[0].coords[idx] = [e.latlng.lat, e.latlng.lng];
					        			if(listTrackGhost[0]){
					        				var newNextPos;
					        				var newPrevPos;
					        				
					        				if(idx==0){
					        					if(scope.type !=2){
					        						newPrevPos = recalculateGhostPos(listTracks[0].coords[listTracks[0].coords.length-1], listTracks[0].coords[idx]);
					        						newNextPos = recalculateGhostPos(listTracks[0].coords[idx], listTracks[0].coords[idx+1]);
					        					}else{
					        						newPrevPos = null;
					        						newNextPos = recalculateGhostPos(listTracks[0].coords[idx], listTracks[0].coords[idx+1]);
					        					}
					        				}else if(idx == listTrackMarkers[0].markers.length-1){
					        					if(scope.type !=2){
					        						newPrevPos = recalculateGhostPos(listTracks[0].coords[idx-1], listTracks[0].coords[idx]);
					        						newNextPos = recalculateGhostPos(listTracks[0].coords[idx], listTracks[0].coords[0]);
					        					}else{
					        						newPrevPos = recalculateGhostPos(listTracks[0].coords[idx-1], listTracks[0].coords[idx]);
					        						newNextPos = null;
					        					}
					        				}else{
					        					newPrevPos = recalculateGhostPos(listTracks[0].coords[idx-1], listTracks[0].coords[idx]);
					        					newNextPos = recalculateGhostPos(listTracks[0].coords[idx], listTracks[0].coords[idx+1]);
					        				}

						        			if(newPrevPos){
						        				listTrackGhost[0].markers[idx == 0 ? listTrackGhost[0].markers.length-1: idx-1].coord = newPrevPos;
						        				listTrackGhost[0].markers[idx == 0 ? listTrackGhost[0].markers.length-1: idx-1].point.setLatLng(newPrevPos);
						        			}
						        			
						        			if(newNextPos){
						        				listTrackGhost[0].markers[idx].coord = newNextPos;
							        			listTrackGhost[0].markers[idx].point.setLatLng(newNextPos);
						        			}
					        			}
					        			
						        		showDataInMap(null, false);
				        			});
				        		}
				        		
				        		if(listTrackMarkers[0]){
				        			listTrackMarkers[0].markers.push({id:points, coord:[data.lat, data.lng], point:marker});
				        			if(listTrackGhost[0]){
				        				if(listTrackMarkers[0].markers.length >3 && scope.type !=2)
					        			{
				        					var lastGhost = L.marker([], {
							        			icon:iconGH,
							        			title: points,
							        			draggable:scope.type != 1 ? attr.allowdraggable === "true" ? true:false:false
							        		});	
				        					
				        					lastGhost.on('mousedown', function(e){
					        					var firstTempList = listaTracks[0].coords.filter(function(el, idx){ 
					        						return idx <= e.target.options.title
					        					});
					        					
					        					var lastTempList = listaTracks[0].coords.filter(function(el, idx){ 
					        						return idx > e.target.options.title
					        					});
					        					console.log(firstTempList);
					        				});
				        					
					        				//Agregar,//VIGILAR ESTA PARTE, INCONSISTENCIAS MUY POCO LATENTE...
					        				
					        				listTrackGhost[0].markers.pop();
					        				var newLastPos = recalculateGhostPos(listTrackMarkers[0].markers[0].coord,listTrackMarkers[0].markers[listTrackMarkers[0].markers.length-1].coord);
					        				lastGhost._latlng = L.latLng(newLastPos[0], newLastPos[1]); 
					        				listTrackGhost[0].markers.push({id:points-1, coord:[resultado.lat, resultado.lng], point:ghost});
					        				
					        				listTrackGhost[0].markers.push({id:points, coord:newLastPos, point:lastGhost});
					        				
					        			}else{
					        				listTrackGhost[0].markers.push({id:points-1, coord:[resultado.lat, resultado.lng], point:ghost});
					        			}
					        		}else{
					        			listTrackGhost[0]= {id:1, 
					        								   markers:[{id:points-1, coord:[resultado.lat, resultado.lng], point:ghost}]};
					        		}
				        		}else{
				        			listTrackMarkers[0]= {id:1, 
				        								   markers:[{id:points, coord:[data.lat, data.lng], point:marker}]};
				        		}
				        		
				        		if(scope.type != 1 && scope.type != 2)
				        		{
		        					
		        					var lastGhost = L.marker([], {
					        			icon:iconGH,
					        			title: points-1,
					        			draggable:scope.type != 1 ? attr.allowdraggable === "true" ? true:false:false
					        		});	
		        					
		        					if(listTrackMarkers[0].markers.length == 3)
				        			{
				        				//agregar
				        				var newLastPos = recalculateGhostPos(listTrackMarkers[0].markers[0].coord,listTrackMarkers[0].markers[listTrackMarkers[0].markers.length-1].coord);
				        				lastGhost._latlng = L.latLng(newLastPos[0], newLastPos[1]); 
				        				listTrackGhost[0].markers.push({id:points, coord:newLastPos, point:lastGhost})
				        			}
				        		}
				        		points++
				        	};
			        	}
		        	}
		        	
		        	clearLayer(scope.enablerealtime ? true:scope.allowevents ? true:isNotDragging);
		        	
		        	//Recorre cada dispositivo y lo genera en el mapa
		        	listaTracks = listTracks.map(function(element, index){
		        		
			        	if(scope.type){
				        	//Circular
				        	if(scope.type == 1){
				        		if(index == 0){
				        			element.route = element.coords.length >1 ? L.circle(element.coords[0], {color: scope.color,
																				radius: element.coords[1]}) : null;
				        			if(element.route)
				        				element.route.addTo(maps);
				        			
				        			return element;
				        		}
				        	}
				        	//PolyGonal
				        	else if(scope.type == 3){
				        		element.route = L.polygon(element.coords, {color: scope.color,
																		   lineCap:'triangle',
																		   lineJoin:'triangle',
																		   dashArray: '20,20'});
			        			element.route.addTo(maps);
			        			return element;
				        	}
				        	//PolyLineal
				        	else {
				        		element.route = L.polyline(element.coords, {color: scope.color});
			        			element.route.addTo(maps);
			        			return element;
				        	}
			        	}else{
			        		element.route = L.polyline(element.coords, {color: scope.color});
			        		element.route.addTo(maps);
			        		return element;
			        	}
		        	});
		        	
		        	//Recorre cada marcador y lo asigna al mapa
		        	for(i in listTrackMarkers)
		        	{
		        		listTrackMarkers[i].markers = listTrackMarkers[i].markers.map(function(element){
		        			/*if(element.point._latlng){
		        				element.point.setLatLng([element.coord[0],element.coord[1]]);
		        			}*/
		        			element.point.addTo(maps);
		        			return element;
		        		});
		        	}
		        	
		        	//Recorre cada marcador fantasma y lo asigna al mapa
		        	for(i in listTrackGhost)
		        	{
		        		listTrackGhost[i].markers = listTrackGhost[i].markers.map(function(element){
		        			/*if(element.point._latlng){
		        				element.point.setLatLng([element.coord[0],element.coord[1]]);
		        			}*/
		        			element.point.addTo(maps);
		        			return element;
		        		});
		        	}
		        	
		        }
		        
		        function clearLayer(deleteAll){
		        	for(i in maps._layers) {
		                if(scope.type == 1 && maps._layers[i]._icon != undefined 
		                	|| maps._layers[i]._path != undefined 
		                	|| deleteAll && maps._layers[i]._icon != undefined) {
		                    try {
		                    	maps.removeLayer(maps._layers[i]);
		                    }
		                    catch(e) {
		                        console.log("problem with " + e + maps._layers[i]);
		                    }
		                }
		            }
		        }
		        
		        function recalculateGhostPos(pos1, pos2){
		        	if(!pos1) return null;
		        	if(!pos2) return null;
		        	return [(pos1[0]+pos2[0])/2,(pos1[1]+pos2[1])/2];
		        }
		        
		        //Habilita la opcion de crear puntos de acuerdo al tipo.
		        if(attr.enablepointers === "true"){
			        //Listener del mapa, detectara los clicks del usuario
			        maps.on('click', function(e) {
			        	//alert(e.latlng);
			        	var data = {lat:e.latlng.lat, lng:e.latlng.lng};
			        	if(scope.type == 1)
			        		if(listTracks[0])
			        			if(listTracks[0].coords.length >1){
			        				listTracks = [];
			        				listTrackMarkers = [];
			        			}
			        				
			        	showDataInMap(data, true);
		        		if(attr.enablepointers === "true"){
		        			scope.out = listTracks; //PRUEBAS
		        		}
			        });
		        }
		        
		      //se ejecuta si la opcion de habilitar rastreo esta activa
		        function enableTrack(){
		        	if(scope.enabletracking){
		        		maps.panTo([listTrackMarkers[0].markers[0].point._latlng.lat,listTrackMarkers[0].markers[0].point._latlng.lng], {duration:0.5});
		        	}
		        }
		        
		        function extractData(isRealTime)
		        {
		        	if(isRealTime){
		        		
		        		var iconRT = L.divIcon({
			        		  iconSize: [16, 16],
			        		  iconAnchor: [8, 8],
			        		  popupAnchor: [1, 0],
			        		  shadowSize: [0, 0],
			        		  className: 'mapMarkerRT',
			        		  html: ''
			        		});
		        		
		        		return !scope.model ? null: scope.model.map(function(element, index){
		        			var ll = L.latLng(element.coords[element.coords.length -1][0],element.coords[element.coords.length -1][1]);
			        		var marker = L.marker(ll, {
			        			icon:iconRT,
			        			title: points,
			        			draggable:false
			        		});
			        		
		        			return {
			        			id:index, 
			        			markers:[{id:1, point:marker}]
			        		};
		        		});
		        	}else{
		        		if(scope.allowevents){
		        			scope.model.map(function(element, index){
		        				
			        			listTrackMarkers[0]={
					        			id:index, 
					        			markers:element.events.map(function(el, idx){
					        				
					        				var iconEvent = L.divIcon({
							        			iconSize: [16, 16],
							        			iconAnchor: [8, 8],
							        			popupAnchor: [1, 0],
							        			shadowSize: [0, 0],
							        			className: el == 0 ? ' ':el == 1 ? 'mapEvent1':el == 2 ? 'mapEvent2':el == 3 ? 'mapEvent3':'mapMarkerRT',
							        			html: ''
					        				});
					        				var midLat = idx == 0 ? element.coords[idx][0] : (element.coords[idx][0] + element.coords[idx-1][0] )/2 ;
					        				var midLng = idx == 0 ? element.coords[idx][1] : (element.coords[idx][1] + element.coords[idx-1][1] )/2 ;
					        				var ll = L.latLng(midLat,midLng);
							        		var marker = L.marker(ll, {
							        			icon:iconEvent,
							        			title: "Mensaje",
							        			draggable:false
							        		});
							        		
					        				return {
					        					id: idx,
					        					point: marker 
					        				}
					        			})
					        	}
		        			});
			        	}
		        		return scope.model.map(function(element, index){
		        			if(attr.enablepointers === "true"){
		        				for(x in element.coords){
		        					showDataInMap({lat:element.coords[x][0], lng:element.coords[x][1]}, true);
		        				}
		        				/*element.coords.map(function (el,idx){
		        					showDataInMap({lat:el[0], lng:el[1]});
		        				})*/
		        			}
		        			return { id:index,
					        	coords:element.coords,
					        	route:{}};
				        });
		        	}
		        }
		        
		        
		        /**Funciones que monitorean las variables, si se detecta 
		         * un cambio, se realiza el recalculo de los servicios
		         */
		        
		        scope.$watch('color', function(newValue, oldValue) {
		        	showDataInMap(null, false);
	            }, true);
		        
		        scope.$watch('type', function(newValue, oldValue) {
		        	points = 0;
					flagCircleLock = false;
			        listTracks = [];
			        listTrackMarkers = [];
			        listTrackGhost = [];
			        clearLayer(true);
			        //Clean Markers
	            }, true);
		        scope.$watch('shape', function(newValue, oldValue) {
		        	shape = newValue;
		        	showDataInMap(null, false);
	            }, true);
		        scope.$watch('enablerealtime', function(newValue, oldValue) {
		        	if(newValue){
			        	listTrackMarkers = extractData(true);
		        		showDataInMap(null, false);
		        	}
	            }, true);
		        scope.$watch('model', function(newValue, oldValue) {
		        	if(newValue){
		        		if(!scope.enablerealtime){
		        			if(attr.enablepointers !=="true"){
		        				listTracks = extractData(false);
		        				showDataInMap(null, false);
		        			}else{
		        				if(!lockModalByAutoUpdate){
		        					listTracks = extractData(false);
		        					lockModalByAutoUpdate = true;
		        				}
		        				scope.out = listTracks; //PRUEBAS
		        			}
		        		}
		        		if(scope.enablerealtime){
		        			listTrackMarkers = extractData(true);
		        			showDataInMap(null, false);
		        		}
		        		
		        	}
	            }, true);
		        scope.$watch('enabletracking', function(newValue, oldValue) {
		        	
		        	if(newValue){
		        		if(!tracking){
		        			maps.flyTo([listTrackMarkers[0].markers[0].point._latlng.lat,listTrackMarkers[0].markers[0].point._latlng.lng], 17);
		        			tracking = $interval(enableTrack, 4000);
		        		}
		        	}else{
		        		if(tracking){
		        			$interval.cancel(tracking);
		        			tracking = undefined;
		        		}
		        	}
		        	
	            }, true);
		        
			}
		}
	});