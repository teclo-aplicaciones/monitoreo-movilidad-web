/*
 * Author: Sail Ponce
 * Directive: mapa
 * Versión: 1.0.0
 */

angular.module(appTeclo).directive('mapForDevice', function ($window, $rootScope, $interval, $timeout, $compile, showAlert) {
	return{
			restrict: 'E',
			template:'<div class="map"></div>',
			scope:{
				color:'=',
				type:'=',
				model:'=?',
				allowedit:'=',
				shape:'@',
				enabletracking:'=',
				enablerealtime:'=',
				getwkt:'=?',
				allowevents:'=',
				enfoqueevento:'=?',
				callback:'=?',
				callback2:'=?',
				callback3:'=?',
				focusoncoord:'=?'
			},
			replace: true,
			link:function(scope, element, attr) {
				var DOM = element[0];
				var cdmx = {lat: 19.410, lng: -99.133};
				var innerColor, fill;
				var convertLngLat = function(lon, lat){
					return ol.proj.fromLonLat([lon, lat]);
				};
				var convertXY = function(lat, lng){
					return ol.proj.transform([lat, lng], 'EPSG:3857', 'EPSG:4326');
				}
				
				var noInnerUpdate = false;
				
				var isPopoverEnabled = false;
				
				var tracking;
				
				var seleccionClick = null;
				
				var seleccion = new ol.interaction.Select({wrapX:false});
				
				var modificacion = new ol.interaction.Modify({features: seleccion.getFeatures()});
				
				var mapLayer = new ol.layer.Tile({
					source: new ol.source.OSM()
				      /*source: new ol.source.XYZ({
				    	  url:'https://api.mapbox.com/styles/v1/apescape16/cjig9ri4i3vhe2sqpq67sah0y/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiYXBlc2NhcGUxNiIsImEiOiJjamlnOGh2b3owcGM4M3dxcHR4dHgyMDhjIn0.LBU5k6cj4O0_d4wlTdbLRg'
				    	  //url:'https://api.mapbox.com/styles/v1/apescape16/cjig9ri4i3vhe2sqpq67sah0y/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiYXBlc2NhcGUxNiIsImEiOiJjamlnOGh2b3owcGM4M3dxcHR4dHgyMDhjIn0.LBU5k6cj4O0_d4wlTdbLRg'
						  //url:'https://api.mapbox.com/styles/v1/mapbox/streets-v10/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiYXBlc2NhcGUxNiIsImEiOiJjamlnOGh2b3owcGM4M3dxcHR4dHgyMDhjIn0.LBU5k6cj4O0_d4wlTdbLRg'
						})*/
					});
				
				var container = document.getElementById('popup');
				
				//var maxExtent = [80.05844110726194,26.34796712822462,88.2015218371264,30.44742963310623];
				
				//Ciudad de Mexico
				//var maxExtent = [-99.38430152702331,19.208040351929455,-98.88236366081237,19.61280061968756];
				
				var contenido ="";
				
				var popup = new ol.Overlay({
					element: container,
					autoPan: true,
					autoPanAnimation: {
						duration: 250
					}
				});
				
				var source = new ol.source.Vector({wrapX: false});
				
				var vector = new ol.layer.Vector({
					  source: source,
					  //renderBuffer: scope.enablerealtime ? 600:100,
					  interactions: scope.allowedit ? ol.interaction.defaults().extend([seleccion, modificacion]) : ol.interaction.defaults(),
					  style: new ol.style.Style({
					    fill: new ol.style.Fill({
					      color: fill
					    }),
					    stroke: new ol.style.Stroke({
					      color: innerColor,
					      width: 2
					    }),
					    image: new ol.style.Circle({
					      radius: 7,
					      fill: new ol.style.Fill({
					        color: innerColor
					      })
					    })
					  })
					});
  
				var maps = new ol.Map({
					target: element[0],
					layers: [mapLayer, vector],
					controls: ol.control.defaults().extend([
				          new ol.control.FullScreen()
				    ]),
					view: new ol.View({
						//extent: ol.proj.transformExtent(maxExtent, 'EPSG:4326', 'EPSG:900913'),
						center: ol.proj.fromLonLat([cdmx.lng,cdmx.lat]),
						//minZoom:11,
						zoom:11
					})
				});
				
				/*var target = maps.getTarget();
                var jTarget = typeof target === "string" ? $("#" + target) : $(target);
                // change mouse cursor when over marker
                $(maps.getViewport()).on('mousemove', function (e) {
                    var pixel = maps.getEventPixel(e.originalEvent);
                    var hit = maps.forEachFeatureAtPixel(pixel, function (feature, layer) {
                        return true;
                    });
                    if (hit) {
                        jTarget.css("cursor", "pointer");
                    } else {
                        jTarget.css("cursor", "");
                    }
                });*/
				
				var draw = null;
				
				//maps.addOverlay(popup);
				
				var listTracks = [];
				var listTrackMarkers = [];
				
				maps.on('click', function(){
					$(container).popover('destroy');
					isPopoverEnabled = false;
				});
				
				scope.enfoqueevento = function(pos){
					var coord = convertLngLat(scope.model[0].coords[pos][1], scope.model[0].coords[pos][0]);
					
					flyTo(coord, 17, function(){});
					
					$timeout(function(){
						timerEnfoqueEvento(pos);
					    },1000)
				}
				
				function timerEnfoqueEvento(pos){
					if(scope.allowevents){
						
						if(isPopoverEnabled){
   							
   							$(container).on('hidden.bs.popover', function () {
   								$timeout(function(){
   									var contnt=$compile(contenido)(scope);
   		       						
   		       						$(container).popover({
   		       							placement: 'top',
   		       							html: true,
   		       							content: contnt
   		       						});
   		       						
   		       						popup.setPosition(coord);
   									
   				   					$(container).popover('show');
   				   					isPopoverEnabled = true;
   								},true);
   							})
   						}
						
		   				$(container).attr('title',"<span style='white-space: nowrap'>Evento</span>");
		   				
		   				var coord = convertLngLat(scope.model[0].coords[pos][1], scope.model[0].coords[pos][0]);
		   				
		   				//coord = convertXY(coord[0], coord[1]);
		   				contenido ="<div class='row'><div class='col-md-12 contentDisp'><div class='row'>" +
		   						   "<div class='col-md-12'><label>Tipo de evento:</label><p>"+scope.model[0].events[pos].tipo+"</p></div></div>" +
		   						   "<div class='row'>"+
		   						   "<div class='col-md-6'><label>Latitud:</label><p>"+scope.model[0].events[pos].lat+"</p></div>" +
		   						   "<div class='col-md-6'><label>Longitud:</label><p>"+scope.model[0].events[pos].lng+"</p></div></div>"+
		   						   "<div class='row'>"+
		   						   "<div class='col-md-6'><label>Fecha:</label><p>"+scope.model[0].events[pos].fecha+"</p></div>" +
		   						   "<div class='col-md-6'><label>Dirección:</label><p>"+scope.model[0].events[pos].direccion+"</p></div></div>"+
		   						   "<div class='row'>"+
		   						   "<div class='col-md-12'><label>Descripción:</label><p>"+scope.model[0].events[pos].descripcion+"</p></div></div>"+
		   						   "</div></div>";
		   				
		   				var contnt=$compile(contenido)(scope);
		   					
		   				$(container).popover({
		   					placement: 'top',
		   					html: true,
		   					content: contnt
		   				});
		   				
		   				if(!isPopoverEnabled){
		   					popup.setPosition(coord);
   							$(container).popover('show');
   							isPopoverEnabled = true;
   						}else{
   							$(container).popover('destroy');
   							isPopoverEnabled = false;
   						}
		   				
		   				
					}
				}
				
				function showDataInMap(data, isNotDragging){
		        	
		        	if(data){
			        	if(listTracks[0]){
			        		listTracks[0].coords.push([data.lat, data.lng]);
			        	}else{
			        		listTracks[0]= { id:1,
			        						 coords:[[data.lat, data.lng]],
			        						 route:{}
				        	};
			        	}
		        	
			        	if(attr.enablecreate === "true"){
					        var ll = convertLngLat(data.lng, data.lat);
				        	var marker = L.marker(ll, {
				        		icon:icon,
				        		title: points,
				        		draggable:scope.type != 1 ? attr.allowdraggable === "true" ? true:false:false
				        	});
				        }
				        	
				       	if(listTrackMarkers[0]){
				       		listTrackMarkers[0].markers.push({id:points, coord:[data.lat, data.lng], point:marker});
				       	
				        }else{
				       		listTrackMarkers[0]= {id:1, 
				        						   markers:[{id:points, coord:[data.lat, data.lng], point:marker}]};
				        }
				       	points++
		        	}

		        	clearLayer(scope.enablerealtime ? true:scope.allowevents ? true:isNotDragging);
		        	
			       	//Recorre cada dispositivo y lo genera en el mapa
			        listaTracks = listTracks.map(function(element, index){
			        	if(scope.allowedit){
			        		if(scope.type != 1){
				        		seleccion.getFeatures().push(element.route[0]);
				        		maps.removeInteraction(modificacion);
				        		
				        		modificacion = new ol.interaction.Modify({features: seleccion.getFeatures()});
				        		
				        		
			        		}else{
			        			seleccion.getFeatures().push(element.route);
			        			maps.removeInteraction(modificacion);
			        			
				        		modificacion = new ol.interaction.Modify({features: seleccion.getFeatures()});
				        		
				        		//vector.getSource().addFeature(element.route);
			        		}
			        		maps.addInteraction(modificacion);
			        	}
			        	
			        	if(element.route.length){
			        		source.addFeatures(element.route);
			        	}else{
			        		source.addFeature(element.route);
			        	}
			        	
			        	return element;
			       	});
		       	
			       	//Recorre cada marcador y lo asigna al mapa
			        
			        if(scope.enablerealtime){
			       		if(seleccion)
			       			maps.removeInteraction(seleccion);
			       		
			       		seleccion = new ol.interaction.Select({
	       					condition: ol.events.condition.click
	       				});
	       				
			       		maps.addInteraction(seleccion);
			       		
			       		seleccion.on('select', function(e){
	       					var X = e.selected[0] ? e.selected[0].get("identifier") : null;
	       					
	       					if(X != null){
	       						
	       						if(isPopoverEnabled){
	       							
	       							$(container).on('hidden.bs.popover', function () {
	       								$timeout(function(){
	       									var contnt=$compile(contenido)(scope);
	       		       						
	       		       						$(container).popover({
	       		       							placement: 'top',
	       		       							html: true,
	       		       							content: contnt
	       		       						});
	       		       						
	       		       						popup.setPosition(coord);
	       									
	       				   					$(container).popover('show');
	       				   					isPopoverEnabled = true;
	       								},true);
	       							})
	       						}
	       						
	       						var pos = e.selected[0].get("idRT");
	       						//container.innerHTML=;
	       						$(container).attr('title',"<span style='white-space: nowrap'>Detalle del dispositivo</span>");
	       						var coord = e.selected[0].getGeometry().getCoordinates();
	       						
	       						//coord = convertXY(coord[0], coord[1]);
	       						var mnt = moment(scope.model[pos].info.ultimaFecha, "DD-MM-YYYY HH:mm:ss");
	       						mnt.locale('es');
	       						/*contenido =  "<div class='row'><div class='col-md-12'><b class='inline-block'>Nombre: </b><p class='inline-block' style='white-space: normal; word-break:break-all;padding-left:5px;'>"+scope.model[pos].info.nombre+"</p></div>"+
	       									 "<div class='col-md-12'><b class='inline-block'>Usuario: </b><p class='inline-block' style='white-space: normal; word-break:break-all;padding-left:5px;'>"+scope.model[pos].info.usuario+"</p></div>"+
	       									"<div class='col-md-12'><b class='inline-block'>IMEI: </b><p class='inline-block' style='white-space: normal; word-break:break-all;padding-left:5px;'>"+scope.model[pos].info.imei+"</p></div>"+
	       									"<div class='col-md-12'><b class='inline-block'>Dirección: </b><p class='inline-block' style='white-space: normal; word-break:break-all;padding-left:5px;'>"+scope.model[pos].info.direccion+"</p></div>"+
	       									"<div class='col-md-12'><b class='inline-block'>Última Actividad: </b><p class='inline-block' style='white-space: normal; word-break:break-all;padding-left:5px;'>"+mnt.fromNow()+"</p></div>"+
	       									"<div class='col-md-12'><a  ng-click='callback("+X+")'><b class='inline-block'>Mas detalles...</b></a></div>"+
	       									 "</div>";*/
	       						contenido ="<div class='row'><div class='col-md-12 contentDisp'><div class='row'>" +
	       								   "<div class='col-md-6'><label>Nombre:</label><p>"+scope.model[pos].info.nombre+"</p></div>" +
	       								   "<div class='col-md-6'><label>Usuario:</label><p>"+scope.model[pos].info.usuario+"</p></div></div>" +
	       								   "<div class='row'>"+
	       								   "<div class='col-md-6'><label>IMEI:</label><p>"+scope.model[pos].info.imei+"</p></div>"+ 
	       								   "<div class='col-md-6'><label>Última Actividad:</label><p>"+mnt.fromNow()+"</p></div></div>"+
	       								   "<div class='row'>"+
	       								   "<div class='col-md-12'><label>Dirección:</label><p>"+scope.model[pos].info.direccion+"</p></div></div>"+
	       								   "<div class='row'>"+
	       								   "<div class='col-md-4'><a ng-click='callback("+X+")'><b class='inline-block'>Más detalles...</b></a></div>"+
	       								   "<div class='col-md-4'><a ng-click='callback3("+X+")'><b class='inline-block'>Comandos</b></a></div>"+
	       								   "<div class='col-md-4'><a ng-click='callback2("+X+")'><b class='inline-block pull-right'>Mensaje</b></a></div></div>"+
	       								   "</div></div>";
	       						
	       						var contnt=$compile(contenido)(scope);
	       						
	       						$(container).popover({
	       							placement: 'top',
	       							html: true,
	       							content: contnt
	       						});
	       						
	       						if(!isPopoverEnabled){
	       							popup.setPosition(coord);
	       							$(container).popover('show');
	       							isPopoverEnabled = true;
	       						}else{
	       							$(container).popover('destroy');
	       							isPopoverEnabled = false;
	       						}

	       					}else{
	       						$(container).popover('destroy');
	   							isPopoverEnabled = false;
	       					}
	       				});
			       		maps.addOverlay(popup);
	       			}
			        
			        //Si los eventos estan activos, mostrar detalle de eventos
			        if(scope.allowevents){
			       		if(seleccion)
			       			maps.removeInteraction(seleccion);
			       		
			       		seleccion = new ol.interaction.Select({
	       					condition: ol.events.condition.click
	       				});
	       				
			       		maps.addInteraction(seleccion);
			       		
			       		seleccion.on('select', function(e){
	       					var X = e.selected[0] ? e.selected[0].get("identifier") : null;
	       					
	       					if(X != null){
	       						
	       						if(isPopoverEnabled){
	       							
	       							$(container).on('hidden.bs.popover', function () {
	       								$timeout(function(){
	       									var contnt=$compile(contenido)(scope);
	       		       						
	       		       						$(container).popover({
	       		       							placement: 'top',
	       		       							html: true,
	       		       							content: contnt
	       		       						});
	       		       						
	       		       						popup.setPosition(coord);
	       									
	       				   					$(container).popover('show');
	       				   					isPopoverEnabled = true;
	       								},true);
	       							})
	       						}
	       						
	       						var pos = e.selected[0].get("idEV");
	       						//container.innerHTML=;
	       						$(container).attr('title',"<span style='white-space: nowrap'>Evento</span>");
	       						var coord = e.selected[0].getGeometry().getCoordinates();
	       						//popup.setPosition(coord);
	       						//coord = convertXY(coord[0], coord[1]);
	       						//var mnt = moment(scope.model[pos].info.ultimaFecha, "DD-MM-YYYY HH:mm:ss");
	       						//mnt.locale('es');
	       						/*contenido =  "<div class='row'><div class='col-md-12'><b class='inline-block'>Nombre: </b><p class='inline-block' style='white-space: normal; word-break:break-all;padding-left:5px;'>"+scope.model[pos].info.nombre+"</p></div>"+
	       									 "<div class='col-md-12'><b class='inline-block'>Usuario: </b><p class='inline-block' style='white-space: normal; word-break:break-all;padding-left:5px;'>"+scope.model[pos].info.usuario+"</p></div>"+
	       									"<div class='col-md-12'><b class='inline-block'>IMEI: </b><p class='inline-block' style='white-space: normal; word-break:break-all;padding-left:5px;'>"+scope.model[pos].info.imei+"</p></div>"+
	       									"<div class='col-md-12'><b class='inline-block'>Dirección: </b><p class='inline-block' style='white-space: normal; word-break:break-all;padding-left:5px;'>"+scope.model[pos].info.direccion+"</p></div>"+
	       									"<div class='col-md-12'><b class='inline-block'>Última Actividad: </b><p class='inline-block' style='white-space: normal; word-break:break-all;padding-left:5px;'>"+mnt.fromNow()+"</p></div>"+
	       									"<div class='col-md-12'><a  ng-click='callback("+X+")'><b class='inline-block'>Mas detalles...</b></a></div>"+
	       									 "</div>";*/
	       						contenido ="<div class='row'><div class='col-md-12 contentDisp'><div class='row'>" +
	       								   "<div class='col-md-12'><label>Tipo de evento:</label><p>"+scope.model[0].events[pos].tipo+"</p></div></div>" +
	       								   "<div class='row'>"+
	       								   "<div class='col-md-6'><label>Latitud:</label><p>"+scope.model[0].events[pos].lat+"</p></div>" +
	       								   "<div class='col-md-6'><label>Longitud:</label><p>"+scope.model[0].events[pos].lng+"</p></div></div>"+
	       								   "<div class='row'>"+
	       								   "<div class='col-md-6'><label>Fecha:</label><p>"+scope.model[0].events[pos].fecha+"</p></div>" +
	       								   "<div class='col-md-6'><label>Descripción:</label><p>"+scope.model[0].events[pos].descripcion+"</p></div></div>"+
	       								   "<div class='row'>"+
	       								   "<div class='col-md-12'><label>Dirección:</label><p>"+scope.model[0].events[pos].direccion+"</p></div></div>"+
	       								   "</div></div>";
	       						
	       						var contnt=$compile(contenido)(scope);
	       						$(container).popover({
	       							placement: 'top',
	       							html: true,
	       							content: function(){
	       								return contnt;
	       							}
	       						});
	       						
	       						if(!isPopoverEnabled){
	       							popup.setPosition(coord);
	       							$(container).popover('show');
	       							isPopoverEnabled = true;
	       						}else{
	       							$(container).popover('destroy');
	       							isPopoverEnabled = false;
	       						}
	       						
	       						//$(container).popover('show');

	       					}else{
	       						$(container).popover('destroy');
	       					}
	       					//$(container).popover('destroy');
	       				});
			       		maps.addOverlay(popup);
	       			}
			        
			       	for(i in listTrackMarkers)
			       	{
			       		listTrackMarkers[i].markers = listTrackMarkers[i].markers.map(function(element){
			       			/*if(scope.enablerealtime)
			       				seleccionClick.getFeatures().push(element.point);*/
			       			
			       			if(element.point)
			       				source.addFeature(element.point);
			       			return element;
			       		});
			       	}
			       	
			       	
				}
		        
				function clearLayer(deleteAll){
					source.clear();
					$(container).popover('destroy');
					maps.removeOverlay(popup);
				}
			
				function convertLonLatToXY(list){
					
					if(scope.type==3)
						list = list[0];
					
					var XY = new Array();
					if(scope.type !=1){
						for(x in list){
							XY.push([list[x][1], list[x][0]]);
						}
					}else{
						var unit = maps.getView().getProjection().getUnits();
						XY.push([list[0][1], list[0][0]]);
						XY.push([list[1] * 100000, 0]);
					}
					
					return XY;
				}
				
				function validGeometry(geometry){
				    var wktReader = new jsts.io.WKTReader();
				    var olWKTParser = new ol.format.WKT();
		        	var wkt = null;
		        	
		        	var feature = new ol.Feature({
        				type:'click',
        				geometry: geometry
        			});
		        	
		        	feature.getGeometry().transform('EPSG:3857', 'EPSG:4326');
		        	
		        	wkt = olWKTParser.writeFeature(feature);
				    
				    var jstsGeometry = wktReader.read(wkt);
				    var isValidOp = new jsts.operation.valid.IsValidOp(jstsGeometry);
				    var valid = isValidOp.isValid();
				    return valid;
				};
			
				function calculateDistance(list){
					var distance = 0;
	    			var point1 = list[0];
	    			var point2 = list[1];
	    			var lat = Math.abs((point2[0] * (Math.PI/180)) - (point1[0] * (Math.PI/180)));
	    			var lng = Math.abs((point2[1] * (Math.PI/180)) - (point1[1] * (Math.PI/180)));
	    			var a = (Math.sin(lat/2) * Math.sin(lat/2)) + (Math.cos(point1[0])) * (Math.cos(point2[0])) * (Math.sin(lng/2) * Math.sin(lng/2))  
	    			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	    			
	    			distance = 6378000 * c;
	    			return distance;
				}
		        
				function creacionPuntos(isEditing){
					if(isEditing != undefined){
						maps.removeInteraction(seleccion);
						maps.removeInteraction(modificacion);
						scope.allowedit = false;
					}
			        //Habilita la opcion de crear puntos de acuerdo al tipo.
					if(attr.enablecreate === "true" && !scope.allowedit){
						
					    //Listener del mapa, detectara los clicks del usuario
						if(scope.type == 1){
							draw = new ol.interaction.Draw({
				        		source: source,
				        		type: "Circle",
				        		geometryFunction: null,
				        		maxPoints: 2
				        	});
							
							saveCoords(false);
				        }else if(scope.type == 2){
				        	draw = new ol.interaction.Draw({
				        		source: source,
				        		type: "LineString",
				        		geometryFunction: null
				        	});
				        	
				        	saveCoords(true);
				        }else if(scope.type == 3){
				        	draw = new ol.interaction.Draw({
				        		source: source,
				        		type: "Polygon",
				        		geometryFunction: null
				        	});
				        	
				        	saveCoords(false);
				        }
					}
				}
		        
		        
				//Se ejecuta si la opcion de habilitar rastreo esta activa
		        function enableTrack(){
		        	if(scope.enabletracking){
		        		maps.getView().animate({
		        			center: convertLngLat(listTrackMarkers[0].markers[0].coord[1],listTrackMarkers[0].markers[0].coord[0]),
		        			duration: 500
		        		});
		        	}
		        }
		        
		        function flyTo(location, zoomIn, done) {
		        	if(scope.model != undefined && scope.model.length >1){
		        		//Si son mas de 1 dispositivo
		        		maps.getView().fit(source.getExtent(), {
		        			duration:500
		        		});
		        	}else{
		        		//Si solo es 1 dispositivo
			            var duration = 1000;
			            var zoom = maps.getView().getZoom();
			            var maxZoom = zoomIn;
			            var parts = 2;
			            var called = false;
			            function callback(complete) {
			              --parts;
			              if (called) {
			                return;
			              }
			              if (parts === 0 || !complete) {
			                called = true;
			                done(complete);
			              }
			            }
			            maps.getView().animate({
			              center: location,
			              duration: duration
			            }, callback);
			            maps.getView().animate({
			              zoom: zoom - 1,
			              duration: duration / 2
			            }, {
			              zoom: maxZoom,
			              duration: duration / 2
			            }, callback);
			        	}
		          }
		        
		        function saveCoords(isPolyLine){
		        	noInnerUpdate = true;
		        	if(isPolyLine){
		        		draw.on('drawend', function(e){
		        			noInnerUpdate = true;
				       		var jssts = new jsts.io.OL3Parser();
				       		var geom = jssts.read(e.feature.getGeometry());
				       		
				       		//showAlert.confirmacion("Confirmacion", function(){}, null,function(){});
				       		showAlert.prompt("Ingrese el buffer(metros):",function(buffer){
				       			var theGeom = geom.buffer(buffer);
					       		
					       		var newCoord = jssts.write(theGeom);
					       		
					       		var ex = e.feature.getGeometry().clone();
					       		
					       		//if(validGeometry(newCoord.clone())){
						       		
						       		var exy = newCoord.clone();
						       		
						       		var coordsWithPossibleInners = [...exy.transform('EPSG:3857', 'EPSG:4326').getCoordinates()];
						       		
						       		var coordConcatByInnerAndOuter = [...coordsWithPossibleInners[0]];
						       		//CASO EXTREMO, SI LA FORMA TIENE FORMAS INTERNAS, ITERARLAS TODAS
						       		for(x in coordsWithPossibleInners){
						       			if(x !== "0"){
						       				coordConcatByInnerAndOuter.push([-1, -1]);
								       		
								       		coordConcatByInnerAndOuter = coordConcatByInnerAndOuter.concat(coordsWithPossibleInners[x]);
						       			}
						       		}
						       		
									listTracks[0] = {id:1,
											 //coords: exy.transform('EPSG:3857', 'EPSG:4326').getCoordinates()[0],
											 coords: coordConcatByInnerAndOuter,
											 //route:e.feature.getGeometry()
											 route:ex.clone()
									};
									
									//Guardar en model
						        	scope.model = listTracks[0] ? [{coords:listTracks[0].coords}] : scope.model;
						       		
						       		e.feature.setGeometry(newCoord);
					       		/*}else{
					       			showAlert.error("La forma creada no es valida, porfavor trace otra forma.")
						       		e.feature.setGeometry(ex.clone());
					       		}*/
					       		
					       		
				       		}, null, function(){
				       			var ex = e.feature.getGeometry().clone();
				       			listTracks[0] = {id:1,
										 coords: ex.transform('EPSG:3857', 'EPSG:4326').getCoordinates(),
										 route:e.feature.getGeometry()
								};
								
								//Guardar en model
					        	scope.model = listTracks[0] ? [{coords:listTracks[0].coords}] : scope.model;
				       		});
				       		
				       		/*var theGeom = geom.buffer(40);
				       		
				       		var newCoord = jssts.write(theGeom);
				       		
				       		var ex = e.feature.getGeometry().clone();

							listTracks[0] = {id:1,
									 coords: convertLonLatToXY(ex.transform('EPSG:3857', 'EPSG:4326').getCoordinates()),
									 route:e.feature.getGeometry()
							};
							
							//Guardar en model
				        	scope.model = listTracks[0] ? listTracks[0].coords : scope.model;
				       		
				       		e.feature.setGeometry(newCoord);*/
				       	});
		        	}else{
		        		draw.on('drawend', function(e){
		        			noInnerUpdate = true;
		        			if(scope.type != 1){
				        		var jssts = new jsts.io.OL3Parser();
				        			
						       	var geom = jssts.read(e.feature.getGeometry());
						       	
						       	var theGeom = geom.buffer(0);
						       		
						       	var newCoord = jssts.write(theGeom);
						       	
						       	var exy = newCoord.clone();
						       	
						       	//if(validGeometry()){
						       		var coordsWithPossibleInners = [...exy.transform('EPSG:3857', 'EPSG:4326').getCoordinates()];
						       		
							       	var coordConcatByInnerAndOuter = [...coordsWithPossibleInners[0]];
			        			
							       	//CASO EXTREMO, SI LA FORMA TIENE FORMAS INTERNAS, ITERARLAS TODAS
							       	for(x in coordsWithPossibleInners){
							       		if(x !== "0"){
							       			coordConcatByInnerAndOuter.push([-1, -1]);
									      		
							       			coordConcatByInnerAndOuter = coordConcatByInnerAndOuter.concat(coordsWithPossibleInners[x]);
							       		}
							       	}
						       	/*}else{
						       		showAlert.error("La forma creada no es valida, porfavor trace otra forma.")
						       	}*/
		        			}
		        			
		        			var ex = e.feature.getGeometry().clone();
				       		
							listTracks[0] = {id:1,
									 coords: scope.type != 1 ? coordConcatByInnerAndOuter: [ex.transform('EPSG:3857', 'EPSG:4326').getCenter(), [0, ex.transform('EPSG:4326', 'EPSG:3857').getRadius()]],
									//coords: scope.type != 1 ? convertLonLatToXY(ex.transform('EPSG:3857', 'EPSG:4326').getCoordinates()): [ex.transform('EPSG:3857', 'EPSG:4326').getCenter(), [ex.getRadius(), 0]],
									 route:e.feature.getGeometry()
							};
							
							//Guardar en model
				        	scope.model = listTracks[0] ? [{coords:listTracks[0].coords}] : scope.model;
				        	
				        	e.feature.setGeometry(ex.clone());
				       	});
		        	}
		        	
		        	draw.on('drawstart', function(e){
		        		
		        		source.clear();
			       		
		        		e.feature.setStyle(new ol.style.Style({
						    fill: new ol.style.Fill({
							      color: fill
							    }),
							stroke: new ol.style.Stroke({
							      color: innerColor,
							      width: 2
							    }),
							image: new ol.style.Circle({
							      radius: 7,
							      fill: new ol.style.Fill({
							    	  color: innerColor
							      })
							})
		        		}));
		        	});
		        	
		        	maps.addInteraction(draw);
		        }
		        
		        function extractData(isRealTime)
		        {
		        	if(isRealTime){
		        		return !scope.model ? null: scope.model.map(function(elem, index){
		        			if(elem.coords.length >0){
			        			var ll = convertLngLat(elem.coords[elem.coords.length -1][1], elem.coords[elem.coords.length -1][0]);
			        			var feature = new ol.Feature({
			        				type:'click',
			        				geometry: new ol.geom.Point(ll)
			        			});
			        			
			        			//Modifica el radio del Point
			        			
			        			feature.set("idRT", index);
			        			feature.set("identifier", elem.id);
			        			
			        			var iconRT = new ol.style.Style({
			        				image: new ol.style.Icon(({
			        					scale: 1,
			        					rotateWithView:false,
			        					anchor: [0.5, 1],
			        					opacity:1,
			        					src: 'data:image/svg+xml;charset=utf-8,'+encodeURIComponent(elem.info.img)
			        					//src:'./static/dist/img/position.png'
			        				})),
			        				zIndex: 5
				        		});
			        			
			        			feature.setStyle(iconRT)
				        		
			        			return {
				        			id:index, 
				        			markers:[{id:1, 
				        					  coord: [elem.coords[elem.coords.length -1][0], elem.coords[elem.coords.length -1][1]],
				        					  point:feature}]
				        		};
		        			}else{
		        				return {
				        			id:index, 
				        			markers:[{id:1, 
				        					  coord: [0,0],
				        					  point:null}]
				        		};
		        			}
		        		});
		        	}else{
		        		if(scope.allowevents){
		        			if(scope.model.length >0){
			        			scope.model.map(function(element, index){
				        			listTrackMarkers[0]={
						        			id:index, 
						        			markers:element.events.map(function(el, idx){
						        				if(el != null){
							        				//var midLat = idx == 0 ? element.coords[idx][0] : (element.coords[idx][0] + element.coords[idx-1][0] )/2 ;
							        				//var midLng = idx == 0 ? element.coords[idx][1] : (element.coords[idx][1] + element.coords[idx-1][1] )/2 ;
							        				//var ll = convertLngLat(midLng, midLat);
						        					var ll = convertLngLat(element.coords[idx][1], element.coords[idx][0]);
							        				var feature = new ol.Feature({
								        				type:'click',
								        				geometry: new ol.geom.Point(ll)
								        			});
								        			
							        				feature.set("idEV", idx);
													feature.set("identifier", el.id);

													if(idx == 0 || idx == element.events.length-1){
														var iconStartEnd = new ol.style.Style({
															image: new ol.style.Icon(({
																scale: 1,
																rotateWithView:false,
																anchor: [idx == 0 ? 0.5: 0.1, 0.9],
																opacity:1,
																src: idx == 0 ? './static/dist/img/start.svg':idx == element.events.length-1 ? './static/dist/img/end.svg': ' '
															})),
															zIndex: 5
														});
													
														feature.setStyle(iconStartEnd);
													}else{
														var iconEvent = new ol.style.Style({
															image: new ol.style.Icon(({
																scale: 1,
																rotateWithView:false,
																anchor: [0.1, 0.9],
																opacity:1,
																src: 'data:image/svg+xml;charset=utf-8,'+encodeURIComponent(el.image)
																//src: el == 0 ? ' ':el == 1 ? './static/dist/img/position.png':el == 2 ? './static/dist/img/event_2.png':el == 3 ? './static/dist/img/event_3.png':'./static/dist/img/position.png'
															})),
															zIndex: 5
														});
														
														feature.setStyle(iconEvent);
													}
						        				}
						        				return {
						        					id: idx,
						        					coord:element.coords[idx],
						        					point: feature ? feature : null
						        				}
						        			})
						        	}
			        			});
		        			}else{
		        				listTrackMarkers[0]={
					        			id:1, 
					        			markers:[]
		        				};
		        			}
			        	}
		        		return scope.model.map(function(element, index){
		        				var crd = [];
		        				var geoJSON;
		        				var ll;
		        				if(element.tipo != 1){
			        				geoJSON={
			        						type:"FeatureCollection",
			        						features:[
			        							{
			        								type:"Feature",
			        								geometry:{
			        									//type:element.tipo ? element.tipo == 1 ? "Circle": element.tipo == 2 ? "LineString" : element.tipo == 3 ? "Polygon":"LineString":"LineString",
			        									type:element.tipo ? element.tipo == 1 ? "Circle": element.tipo == 2 ? "Polygon" : element.tipo == 3 ? "Polygon":"LineString":"LineString",
			        									coordinates:null
			        								}
				        						}
			        						]
			        				};
		        				
	    						    if(scope.allowedit){
	    						    	var lastPoint = false;
		    						    var innerCoords = [];
		    						    var innerOfInners = [];
		    						    var pivotCoord = [[element.coords[0][0], element.coords[0][1]]];
		    						    var pivotPoint = "0";
		    						    //CHECA SI TIENE FORMAS INTERNAS
		    						    if(element.coords[0][0] != element.coords[element.coords.length-1][0] &&
		    						    		element.coords[0][1] != element.coords[element.coords.length-1][1]){
		    						    	for(x in element.coords){
			    						    	//Cargar las coordenadas en el mapa
						        				ll = convertLngLat(element.coords[x][0], element.coords[x][1]);
						        				innerOfInners.push([ll[0], ll[1]]);
		    						    		
		    						    		if (x !== pivotPoint){
		    						    			if(element.coords[x][0] == pivotCoord[0][0] &&
		    						    					element.coords[x][1] == pivotCoord[0][1]){
		    						    				innerCoords.push(innerOfInners);
		    						    				innerOfInners = [];
		    						    				if(element.coords.length -1 != parseInt(x, 10)){
		    						    					pivotCoord = [[element.coords[parseInt(x,10)+1][0], element.coords[parseInt(x,10)+1][1]]];
			    						    				pivotPoint = (parseInt(x,10)+1).toString();
		    						    				}
		    						    			}
		    						    		}
					        				}
		    						    }
		    						    if(innerCoords.length > 1){
		    						    	geoJSON.features[0].geometry.type = "MultiPolygon";
		    						    	crd = innerCoords;
		    						    }else{
		    						    	for(x in element.coords){
						        				//Cargar las coordenadas en el mapa
						        				ll = convertLngLat(element.coords[x][0], element.coords[x][1]);
						        				crd.push([ll[0], ll[1]]);
						        				//crd.push([element.coords[x][0], element.coords[x][1]]);
						        			}
		    						    }
	    						    }else{
	    						    	for(x in element.coords){
					        				//Cargar las coordenadas en el mapa
					        				ll = convertLngLat(element.coords[x][1], element.coords[x][0]);
					        				crd.push([ll[0], ll[1]]);
					        				//crd.push([element.coords[x][0], element.coords[x][1]]);
					        			}
			        				}
	    						    
		        				}else{
		        					ll = convertLngLat(element.coords[0][0], element.coords[0][1]);
		        					crd.push([ll[0], ll[1]]);
		        				}
		        				
		        				if(element.tipo)
		        				{
		        					if(element.tipo > 1){
		        						if(scope.allowedit){
		        							geoJSON.features[0].geometry.coordinates= new Array();
		        							geoJSON.features[0].geometry.coordinates.push(crd);
		        						}else{
		        							geoJSON.features[0].geometry.coordinates= new Array();
		        							geoJSON.features[0].geometry.coordinates.push(crd);
		        						}
		        					}
		        					/*if(element.tipo == 2)
				        				geoJSON.features[0].geometry.coordinates= crd;
		        					
		        					if(element.tipo == 3){
		        						geoJSON.features[0].geometry.coordinates= new Array();
		        						geoJSON.features[0].geometry.coordinates.push(crd);
		        					}*/
		        				}else{
		        					geoJSON.features[0].geometry.coordinates= crd;
		        				}
		        				
		        				var vex =  element.tipo != undefined ? element.tipo != 1 ? new ol.format.GeoJSON().readFeatures(geoJSON, { dataProjection: 'EPSG:3857', featureProjection: 'EPSG:3857'}) :
		        					new ol.Feature(new ol.geom.Circle(
		        							  crd[0],
		        							  element.coords[1][1]/* / 100000*/)): 
		        								  new ol.format.GeoJSON().readFeatures(geoJSON, { dataProjection: 'EPSG:3857', featureProjection: 'EPSG:3857'});
		        					/*new ol.Feature(new ol.geom.Polygon.circular(
		        							  ll,
		        							  element.coords[1][1] / 100000
		        							  ,360))*/
		        					
		        				var style2 = null;
		        				if(element.color){
		        					style2 = new ol.style.Style({
		        					    fill: new ol.style.Fill({
		        					      color: fill
		        					    }),
		        					    stroke: new ol.style.Stroke({
		        					      color: innerColor,
		        					      width: 2
		        					    }),
		        					    image: new ol.style.Circle({
		        					      radius: 7,
		        					      fill: new ol.style.Fill({
		        					        color: innerColor
		        					      })
		        					    })
		        		        	});
		        				}else{
		        					style2 = new ol.style.Style({
		        					    fill: new ol.style.Fill({
		        					      color: innerColor
		        					    }),
		        					    stroke: new ol.style.Stroke({
		        					      color: innerColor,
		        					      width: 2
		        					    })
		        		        	});
		        				}
		        				
		        				if(element.tipo == 1){
		        					vex.setStyle(style2);
		        				}
		        					
		        				vector.setStyle(style2);
		        				if(scope.allowedit){
		        					if(scope.type != 1){
			        					vex[0].on('change',function(e){
			        						noInnerUpdate = true;
			    					       	var exy = e.target.getGeometry().clone();
			    		        			
				    						//Es necesario convertir para encontrar formas internas...
			    					       	var jssts = new jsts.io.OL3Parser();
						        			
									       	var geom = jssts.read(exy.clone());
									       	
									       	var theGeom = geom.buffer(0);
									       		
									       	var newCoord = jssts.write(theGeom);
			    						    
								       		var exz = newCoord.clone();
								       		
			    						    var coordsWithPossibleInners = [...exz.transform('EPSG:3857', 'EPSG:4326').getCoordinates()];
			    						       		
			    						    var coordConcatByInnerAndOuter = [...coordsWithPossibleInners[0]];
			    					       	
			    						    var lastPoint = false;
			    						    var innerCoords = [];
			    						    //CHECA SI TIENE FORMAS INTERNAS
			    						    if(coordConcatByInnerAndOuter[0][0] != coordConcatByInnerAndOuter[coordConcatByInnerAndOuter.length-1][0] &&
			    						    		coordConcatByInnerAndOuter[0][1] != coordConcatByInnerAndOuter[coordConcatByInnerAndOuter.length-1][1]){
			    						    	for(x in coordConcatByInnerAndOuter){
			    						    		if (x !== "0"){
			    						    			if(lastPoint){
			    						    				innerCoords = coordConcatByInnerAndOuter.slice(x,coordConcatByInnerAndOuter.length);
			    						    				break;
			    						    			}
			    						    			if(coordConcatByInnerAndOuter[x][0] == coordConcatByInnerAndOuter[0][0] &&
						    						    		coordConcatByInnerAndOuter[x][1] == coordConcatByInnerAndOuter[0][1]){
			    						    				lastPoint = true;
			    						    			}
			    						    		}
			    						    	}
			    						    	coordsWithPossibleInners.push(innerCoords);
			    						    }
			    						    
			    						    //CASO EXTREMO, SI LA FORMA TIENE FORMAS INTERNAS, ITERARLAS TODAS
			    						    for(x in coordsWithPossibleInners){
			    						    	if(x !== "0"){
			    						       		coordConcatByInnerAndOuter.push([-1, -1]);
			    								      		
			    						       		coordConcatByInnerAndOuter = coordConcatByInnerAndOuter.concat(coordsWithPossibleInners[x]);
			    						       	}
			    						    }
			    		        			
			    		        			var ex = e.target.getGeometry().clone();
			    				       		
			    							listTracks[0] = {id:1,
			    									 coords: scope.type != 1 ? coordConcatByInnerAndOuter: [ex.transform('EPSG:3857', 'EPSG:4326').getCenter(), [0, ex.transform('EPSG:4326', 'EPSG:3857').getRadius()]],
			    									//coords: scope.type != 1 ? convertLonLatToXY(ex.transform('EPSG:3857', 'EPSG:4326').getCoordinates()): [ex.transform('EPSG:3857', 'EPSG:4326').getCenter(), [ex.getRadius(), 0]],
			    									 route:e.target.getGeometry()
			    							};
			    							
			    							//Guardar en model
			    				        	scope.model = listTracks[0] ? [{coords:listTracks[0].coords}] : scope.model;
			        					});
		        					}else{
		        						vex.on('change',function(e){
			        						noInnerUpdate = true;
			    		        			
			    		        			
			    		        			var ex = e.target.getGeometry().clone();
			    				       		
			    							listTracks[0] = {id:1,
			    									 coords: [ex.transform('EPSG:3857', 'EPSG:4326').getCenter(), [0, ex.transform('EPSG:4326', 'EPSG:3857').getRadius()]],
			    									//coords: scope.type != 1 ? convertLonLatToXY(ex.transform('EPSG:3857', 'EPSG:4326').getCoordinates()): [ex.transform('EPSG:3857', 'EPSG:4326').getCenter(), [ex.getRadius(), 0]],
			    									 route:e.target.getGeometry()
			    							};
			    							
			    							//Guardar en model
			    				        	scope.model = listTracks[0] ? [{coords:listTracks[0].coords}] : scope.model;
			        					});
		        					}
		        				}
		        				
		        				if(scope.allowevents){
			        				vex[0].getGeometry().forEachSegment(function(start, end) {
				        					
				        				var isAny = -1;
				        				
				        				var dx = end[0] - start[0];
				        		        var dy = end[1] - start[1];
				        					
				        				//Si es la primera posicion, marcarlo como inicial
				        				/*if(start[0] == vex[0].getGeometry().getCoordinates()[0][0] &&
					        					start[1] == vex[0].getGeometry().getCoordinates()[0][1]){
					        					isAny = 0
					        			}*/
				        					
				        				//Si es la ultima posicion, marcarlo como final
				        				/*if(end[0] == vex[0].getGeometry().getCoordinates()[vex[0].getGeometry().getCoordinates().length-1][0] &&
				        						end[1] == vex[0].getGeometry().getCoordinates()[vex[0].getGeometry().getCoordinates().length-1][1]){
					        					isAny = 1;
					        			}*/
				        				
				        				/*if (isAny != -1){
				        					
				        					var feature = new ol.Feature({
							       				type:'click',
							       				geometry: new ol.geom.Point(isAny == 0 ? start : end)
							       			});
				        				
					        				var iconStartEnd = new ol.style.Style({
					        					image: new ol.style.Icon(({
							        				scale: 1,
							        				rotateWithView:false,
							       					anchor: [0.5, 1],
							       					opacity:1,
							       					src: isAny == 0 ? './static/dist/img/start.svg':isAny == 1 ? './static/dist/img/end.png': ' '
							       				})),
							       				zIndex: 5
					        				});
					        				
					        				feature.set("idEV", isAny == 0 ? 0:scope.model[0].coords.length-1);
						        			feature.set("identifier", isAny);
					        				
					        				feature.setStyle(iconStartEnd);
					        				
					        				vex.push(feature);
				        				}else{*/
				        					var midLat = (end[0] + start[0])/2;
					        				var midLng = (end[1] + start[1])/2;
				        					var rotation = Math.atan2(dy, dx);
				        					
				        					var feature = new ol.Feature({
							       				type:'click',
							       				geometry: new ol.geom.Point([midLat, midLng])
							       				//geometry: new ol.geom.Point(end)
							       			});
				        					var iconArrow = new ol.style.Style({
					        					image: new ol.style.Icon(({
					        						src: './static/dist/img/arrow.svg',
					        						anchor: [0.75, 0.5],
					        						rotateWithView: true,
					        						rotation: -rotation
							       				})),
							       				zIndex: 5
					        				});
				        					
				        					feature.setStyle(iconArrow);
				        					vex.push(feature);
				        				//}
				       				});
			        			}
		       			return { id:index,
					        	coords:element.coords,
					        	route:vex};
				        });
		        	}
		        }
		        
		        
		        /**Funciones que monitorean las variables, si se detecta 
		         * un cambio, se realiza el recalculo de los servicios
		         */
		        
		        scope.$watch('color', function(newValue, oldValue) {
		        	
		        	if(newValue.indexOf("#")  != -1)
		        	{
		        		innerColor = newValue;
		        		fill = "rgba("+parseInt(newValue.substring(1,3),16)+","+parseInt(newValue.substring(3,5),16)+","+parseInt(newValue.substring(5,7),16)+",0.35)";
		        	}else{
		        		innerColor= newValue.split(",")
		        		innerColor[3]="0)";
		        		innerColor = innerColor.join(",");
		        		fill = newValue;
		        	}
		        	//showDataInMap(null, false);
	            }, true);
		        
		        scope.$watch('type', function(newValue, oldValue) {
		        	maps.removeInteraction(draw);
		        	source.clear();
		        	creacionPuntos(newValue != oldValue? oldValue != undefined ? true: undefined:undefined);
		        	//=================
		        	points = 0;
					flagCircleLock = false;
			        listTracks = [];
			        listTrackMarkers = [];
			        listTrackGhost = [];
			        clearLayer(true);
			        //Clean Markers
	            }, true);
		        scope.$watch('shape', function(newValue, oldValue) {
		        	shape = newValue;
		        	showDataInMap(null, false);
	            }, true);
		        scope.$watch('enablerealtime', function(newValue, oldValue) {
		        	if(newValue){
			        	listTrackMarkers = extractData(true);
		        		showDataInMap(null, false);
		        	}
	            }, true);
		        scope.$watch('model', function(newValue, oldValue) {
		        	if(newValue){
		        		if(!scope.enablerealtime){
		        			if(attr.enablecreate !=="true"){
		        				listTracks = extractData(false);
		        				showDataInMap(null, false);
		        			}else{
		        				if(!noInnerUpdate){
		        					listTracks = extractData(false);
		        					showDataInMap(null, false);
		        				}else{
		        					noInnerUpdate = false;
		        				}
		        			}
		        		}
		        		if(scope.enablerealtime){
		        			listTrackMarkers = extractData(true);
		        			showDataInMap(null, false);
		        		}
		        		
		        	}
	            }, true);
		        scope.$watch('enabletracking', function(newValue, oldValue) {
		        	
		        	if(newValue){
		        		if(!tracking){
		        			flyTo(convertLngLat(listTrackMarkers[0].markers[0].coord[1],listTrackMarkers[0].markers[0].coord[0]), 17, function(){});
		        			if(scope.model.length == 1){
		        				tracking = $interval(enableTrack, 4000);
		        			}else if(scope.model.length >1){
		        				tracking = $interval(flyTo, 4000);
		        			}
		        		}
		        	}else{
		        		if(tracking){
		        			$interval.cancel(tracking);
		        			tracking = undefined;
		        		}
		        	}
		        	
	            }, true);
		        
		        scope.getwkt = attr.enablecreate ? function(){
		        	if(scope.type != 1){
			        	var ftrsWKT = [];
			        	var ftrs = source.getFeatures();
			        	var olWKTParser = new ol.format.WKT();
			        	var wkt = null;
			        	var item = null;
			        	for (var i = 0; i < ftrs.length; i++) {
			        		wkt = null;
			        		var ftr = ftrs[i].clone();
			        		ftr.getGeometry().transform('EPSG:3857', 'EPSG:4326');
			        		wkt = olWKTParser.writeFeature(ftr);
			        		//item = new GeoCerca(ftrs[i].get('name'), wkt);
			        		ftrsWKT.push(wkt);
			        	}
		        	}else{
		        		ftrsWKT = null;
		        	}
		        	return ftrsWKT;
		        } : null;
		        
		        scope.focusoncoord = attr.enablecreate ? function(lng, lat){
		        	flyTo(convertLngLat(lng, lat),15, function(){});
		        }:null;
		        
		        //Funcion que escanea cada vez que se reajusta la pantalla del mapa
		        function cacheElementSize(scope, element) {
	                scope.cachedElementWidth = element.offsetWidth;
	                scope.cachedElementHeight = element.offsetHeight;
	            }

		        function onWindowResize() {
	                var isSizeChanged = scope.cachedElementWidth != element.offsetWidth || scope.cachedElementHeight != element.offsetHeight;
	                if (isSizeChanged) {
	                	maps.updateSize();
	                }
	            };
	            
	            element[0].onresize = onWindowResize;
	            window.onresize = onWindowResize;
	            
	            //cacheElementSize(scope, DOM);
	            $window.addEventListener('onresize', onWindowResize);
	            
	            /*$timeout(function(){
                    scope.sizeWidth = DOM.clientWidth;
                    scope.sizeHeight = DOM.clientHeight;
*/
                    var mapSize = $interval(function(){
                        let sizeWtmp = DOM.clientWidth;
                        let sizeHtmp = DOM.clientHeight;
                        
                        if(sizeWtmp !== scope.sizeWidth || sizeHtmp !== scope.sizeHeight) {
                            scope.sizeWidth = DOM.clientWidth;
                            scope.sizeHeight = DOM.clientHeight;
                            cacheElementSize(scope, DOM);
                            onWindowResize();
                        }
                    }, 100);
               // }, 100);

                scope.$on('$destroy', function () {
                    $interval.cancel(mapSize);
                });
			}
		}
	});