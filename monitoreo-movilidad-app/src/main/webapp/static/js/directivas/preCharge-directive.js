/*
 * Author: César Gómez
 * Directive: modalBootstrap-directive
 * Versión: 1.0.0
 */

var app = angular.module(appTeclo);

app.directive('preCharge', function() {
        return {
            template: '<div style="width:{{width}};height:{{height}};'
                      + 'background: url(static/dist/img/charge.gif);'
                        +'background-size: 100%;">'
                        +'</div>',
            restrict: 'AE',
            replace: true,
            scope: {
                width: '@',
                height: '@'
            },
            link:function postLink($s, $e, $a){
                if(angular.isUndefined($s.width)) {
                    $s.width = '100%';
                } else if(angular.isUndefined($s.height)) {
                    $s.height = '1rem';
                }
            }
        };
    }
);