/*
 * Author: Sail Jesus Ponce Isturiz
 * Directive: Slide-Bar History
 * Versión: 1.0.0
 */

angular.module(appTeclo).directive('slideHistory', function($translate,$timeout) {
	return {
		restrict: 'E',
		templateUrl: 'views/templatedirectivas/slideBarHistory.html',
		replace: true,
		scope: {
			model:'=',
			info:'=',
			func:'='
		},
		link: function($s, $e, $a) {
			
			$s.listaDias = [];
			
			$s.translate = [];

			$s.selected = 0;
			
			$s.rangoFecha = "";

			$s.validaSeleccion = function(id){
				return id == $s.selected;
			}
			
			function traducir(callback){
				//Traduccion de dias de la semana y meses
				$translate(['APP.Base.fechaDias.lunes', 'APP.Base.fechaDias.martes','APP.Base.fechaDias.miercoles',
							'APP.Base.fechaDias.jueves','APP.Base.fechaDias.viernes','APP.Base.fechaDias.sabado',
							'APP.Base.fechaDias.domingo','APP.Base.fechaMeses.enero','APP.Base.fechaMeses.febrero',
							'APP.Base.fechaMeses.marzo','APP.Base.fechaMeses.abril','APP.Base.fechaMeses.mayo',
							'APP.Base.fechaMeses.junio','APP.Base.fechaMeses.julio','APP.Base.fechaMeses.agosto',
							'APP.Base.fechaMeses.septiembre','APP.Base.fechaMeses.octubre','APP.Base.fechaMeses.noviembre',
							'APP.Base.fechaMeses.diciembre']).then(function (headline) {
				    $s.translate ={
				    		dias: [
				    			headline['APP.Base.fechaDias.domingo'],
				    			headline['APP.Base.fechaDias.lunes'],
				    			headline['APP.Base.fechaDias.martes'],
				    			headline['APP.Base.fechaDias.miercoles'],
				    			headline['APP.Base.fechaDias.jueves'],
				    			headline['APP.Base.fechaDias.viernes'],
				    			headline['APP.Base.fechaDias.sabado']
				    		],
				    		meses: [
				    			headline['APP.Base.fechaMeses.enero'],
				    			headline['APP.Base.fechaMeses.febrero'],
				    			headline['APP.Base.fechaMeses.marzo'],
				    			headline['APP.Base.fechaMeses.abril'],
				    			headline['APP.Base.fechaMeses.mayo'],
				    			headline['APP.Base.fechaMeses.junio'],
				    			headline['APP.Base.fechaMeses.julio'],
				    			headline['APP.Base.fechaMeses.agosto'],
				    			headline['APP.Base.fechaMeses.septiembre'],
				    			headline['APP.Base.fechaMeses.octubre'],
				    			headline['APP.Base.fechaMeses.noviembre'],
				    			headline['APP.Base.fechaMeses.diciembre']
				    		] 
				    }
				    if(callback)
				    	callback()
				}, function (translationId) {
				    $scope.headline = translationId;
				});
			}
			
			function recalcular(){
				$s.listaDias = $s.model.map(function(el, idx){
					var specialMonth = parseInt(el.fecha.split('/')[1])-1;
					//specialMonth = specialMonth.toString().length == 1 ? "0"+specialMonth.toString(): specialMonth.toString();
					var date = new Date(el.fecha.split('/')[2], specialMonth, el.fecha.split('/')[0], 0,0,0,0);
					var mnt = moment(el.fecha,"DD-MM-YYYY");
					mnt.locale('es');
					$s.rangoFecha = mnt.format('YYYY');
					return {
						id:idx,
						numeroDia:parseInt(el.fecha.split('/')[0]),
						dia: $s.translate.dias[date.getDay()],
						mes: $s.translate.meses[date.getMonth()]
					}
				});
				redimensionar(true);
			}
			
			function redimensionar(forced){
				var slides = $('#contenidoRutas #rutaHistorica').children().length;
			    var elemSlide = $(".diaRutas").width() +55; // +40 es el padding aplicado en cada elemento y +15 de Margin a la izquierda
			    var slideWidth = $('#contenidoRutas').width();
			    var min = 0;
			    var max = ((slideWidth+1) - (slides*elemSlide));
			    
			    //reiniciar posicionamiento
			    if(forced)
			    	if(max >0)
			    		$("#contenidoRutas #rutaHistorica").css("left", max);
			    	else
			    		$("#contenidoRutas #rutaHistorica").css("left", "0");
			    
			    //$("#contenidoRutas #rutaHistorica").width(slides*slideWidth).draggable({
			    $("#contenidoRutas #rutaHistorica").width(slides*elemSlide).draggable({
			        axis: 'x',
			        drag: function (event, ui) {
			            if (ui.position.left > min) ui.position.left = min;
			            if (ui.position.left < max) ui.position.left = max;
			        }
			    });
			}
			
			$s.$watch( function() {
                $s.__width = $e.width();
            });
			
			$s.$watch( '__width', function( newHeight, oldHeight ) {
				redimensionar(true);
            });
			
			$s.$watch('model', function(newModel,oldModel){
				if(newModel){
					if(newModel.length>0){
						$s.listaDias = [];
						traducir(recalcular);
					}
				}
			});
			
			//$s.$watch('callback', function(newModel,oldModel){
			$s.$watch('func', function(newModel,oldModel){
				$s.callback = function(param){$s.selected = param; $s.func(param); console.log("selected: "+$s.selected)};
			});
			$s.$watch("$parent.currentLanguage", function(newValue, oldValue) {
				traducir(recalcular);
			});
		},
		controller: function ($scope, $rootScope){
			$scope.testCall = function(){
				console.log("ejemplo");
			}
		}
	}
});