/*
 * Author: César Gómez
 * Directive: dragNdrop-directive
 * Versión: 1.0.0
 */

angular.module(appTeclo).directive('dndLists', function($rootScope, $timeout) {
	return {
		restrict: 'E',
		templateUrl: 'views/templatedirectivas/dragNdrop-lists.html',
		replace: true,
		scope: {
			col: '@',
			maxHeight:'@',
			model: '=?',
			nbList: '@',
			nbItem: '@',
			nbTexts:'@',
			nbLabels: '@',
			locker: '=',
			onReset: '=?',
			onMove: '=?',
			onAll: '=?',
			resetModel:'=?',
			modelAux: '=',
			requiredList: '=',
			imgCursor: '='
		},
		link: function(scope, element, attr) {
			
			var listNames = 'listName',
				items	 = 'items',
				labels	 = 'label';
			
			//setea el valor de permitir eliminado manual
			scope.allowDelete = attr.allowdelete === "true"? true:false;
			
			// Limpiar los arregos para quitar espacios en blanco
			cleanArray = function(str) {
				
				var arr = str.split(',');
				
				for(let pos in arr) {
					arr[pos] = arr[pos].trim();
				}
				
				return arr;
			}
			
			// Escuchar cambios en el modelo principal
			scope.$watch('model', function(newModel, oldModel) {
				
				// Validar si la bandera que se cambia al tener estatus success está en true
				if(scope.locker) {
					
					scope.listModel = angular.copy(scope.model);
					scope.txs = cleanArray(scope.nbTexts);
					scope.lbs = cleanArray(scope.nbLabels);
					
					// Modificar los nombres de las propiedades del model principal para usarlos en la directiva
					scope.listModel = scope.listModel.map( item => {
						if(item !== null) {
							
							var _listName = item[scope.nbList],
							_items	 = item[scope.nbItem];
						
							delete item[scope.nbList];
							delete item[scope.nbItem];
							
							item[listNames] = _listName;
							item[items] = _items;
							
							item[items] = item[items].map(label => {
								
								for(var i in scope.lbs) {
									
									var _labels = label[scope.lbs[i]];
									
									delete label[scope.lbs[i]];
									
									label[labels+i] = _labels;
								}
								
								return label;
							});
						} else {
							return;
						}
						
						return item;
					});
					
					scope.originalListModel = angular.copy(scope.listModel);
					scope.locker = false;
					scope.btnReset = true;
				}
				
			}, true);
			    
			scope.getSelectedItemsIncluding = function(list, item) {
				item.selected = true;
				return list.items.filter(function(item) { return item.selected; });
			};
			
			scope.onDragstart = function(list, event) {
				console.log("On Drag Start");
				list.dragging = true;
				
				if (event.dataTransfer.setDragImage && scope.imgCursor) {

					var img = new Image()

					img.src = scope.imgCursor;
					event.dataTransfer.setDragImage(img, 0, 0);
		    	}
				
				scope.saveChanges();
		    };
		    
		    scope.onDrop = function(list, items, index) {
		    	console.log("On Drop");
		  		list.originalList = false;
			  	
			   	angular.forEach(items, function(item) {
			    	item.selected = false;
		    	});
			  	
			   	list.items = list.items.slice(0, index).concat(items).concat(list.items.slice(index));
			    	
			    scope.saveChanges();
			    	
			   	return true;
		    	
		    }
		    	
		    
		    scope.onMoved = function(list) {
		    	console.log("On Moving");
		    	if(scope.onMove != undefined)
		    		scope.onMove(list);
		    	list.originalList = false;
		    	
		    	list.items = list.items.filter(function(item) {
		    		return !item.selected;
		    	});
		    	
		    	scope.saveChanges();
		    };
		    
		    // Transferir los objetos de una lista a otra (Únicamente para dos listas)
		    scope.transferItems = function(dir) {

		    	var li  = $('.liRepeat');
		    	if(scope.onAll != undefined)
		    		scope.onAll(dir);
		    		
		    	if(dir === 'ltr') {
		    		
		    		if(scope.listModel[0].items.length > 0){
		    			
		    			scope.listModel[1].items = scope.listModel[1].items.concat(scope.listModel[0].items);
		    			delete scope.listModel[0].items;
		    			scope.listModel[0].items = [];
		    			
		    			let size = scope.listModel[1].items.length;
		    			
		    			li.hide();
		    			
		    			showElement(size, li);
		    		}
		    		
		    	} else if (dir === 'rtl') {
		    		
		    		
		    		if(scope.listModel[1].items.length > 0){
		    			
		    			scope.listModel[0].items = scope.listModel[0].items.concat(scope.listModel[1].items);
			    		delete scope.listModel[1].items;
			    		scope.listModel[1].items = [];
			    		
			    		let size = scope.listModel[0].items.length;
			    		
			    		li.hide();

			    		showElement(size, li);
		    		}
		    	}
		    	
		    	scope.listModel[0].originalList = false;
		    	scope.listModel[1].originalList = false;
		    	
		    	scope.saveChanges();
		    }
		    
		    // Reestablecer las listas a sus valores por defecto
		    scope.resetModel = function() {
		    	scope.listModel = scope.originalListModel;
		    	scope.originalListModel = angular.copy(scope.listModel);
		    	
		    	scope.saveChanges();
		    	
		    	if(scope.onReset != undefined)
		    		scope.onReset(scope.model);
		    }
		    
		    // Ver los elementos de las listas a determinado tiempo de acuerdo al número de registros
		    showElement = function(s, l) {
		    	switch(true) {
	    			case (s < 100):
	    				$timeout(function(){l.show();}, 800);
	    				break;
	    			case (s < 300):
	    				$timeout(function(){l.show();}, 1000);
	    				break;
	    			case (s < 500):
	    				$timeout(function(){l.show();}, 1500);
	    				break;
	    			case (s < 800):
	    				$timeout(function(){l.show();}, 2000);
	    				break;
	    			case (s > 800):
	    				$timeout(function(){l.show();}, 3500);
	    				break;
	    		}
		    }
		    
		    // Regresar los nombres de las propiedades del model principal a su nombre original
		    scope.saveChanges = function() {
		    	
		    	scope.model = [];
		    	scope.model = angular.copy(scope.listModel);
		    	
		    	scope.model = scope.model.map( item => {

					var _listName = item[listNames],
						_items	 = item[items];
					
					delete item[listNames];
					delete item[items];
					
					item[scope.nbList] = _listName;
					item[scope.nbItem] = _items;
					
					item[scope.nbItem] = item[scope.nbItem].map(label => {
						
						let sz = scope.lbs.length;
						
						switch(sz) {
							case 1:
								var _label0 = label['label0'];
								
								delete label['label0'];
								
								label[scope.lbs[0].trim()] = _label0;
								break;
							case 2:
								var _label0 = label['label0'],
									_label1 = label['label1'];
								
								delete label['label0'];
								delete label['label1'];
								
								label[scope.lbs[0].trim()] = _label0;
								label[scope.lbs[1].trim()] = _label1;
								break;
							case 3:
								var _label0 = label['label0'],
									_label1 = label['label1'],
									_label2 = label['label2'];
							
								delete label['label0'];
								delete label['label1'];
								delete label['label2'];
								
								label[scope.lbs[0].trim()] = _label0;
								label[scope.lbs[1].trim()] = _label1;
								label[scope.lbs[2].trim()] = _label2;
								break;
							default:
								break;
						}
												
						return label;
					});
					
					return item;
				});	
		    }
		    
		    if(scope.modelAux !== undefined) {
		    	angular.extend(scope.modelAux, {
		    		reset: function(){
		    			scope.resetModel();
		    		},
		    		cleanModel: function() {
		    			scope.model = {};
		    			scope.locker = false;
		    		}
		    	});
		    }
			
		}
	};
});