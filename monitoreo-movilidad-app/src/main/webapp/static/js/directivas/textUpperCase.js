angular.module(appTeclo).directive('capitalize', function() {
	  return {
		    require: 'ngModel',
		    link: function(scope, element, attrs, modelCtrl) {
		      var capitalize = function(inputValue) {
		        if (inputValue == undefined) inputValue = '';
		        var capitalized = inputValue.toUpperCase();
		        if (capitalized !== inputValue) {
		          modelCtrl.$setViewValue(capitalized);
		          modelCtrl.$render();
		        }
		        return capitalized;
		      }
		      modelCtrl.$parsers.push(capitalize);
		      capitalize(scope[attrs.ngModel]); // capitalize initial value
		    }
		  };
});

angular.module(appTeclo).directive('onlyalphabets', function() {
	return {
		require: 'ngModel',
		link: function(scope, element, attr, ngModelCtrl) {

			const fromUser = function(text) {

				let transformedInput = (text.toLowerCase()).replace(/[^0-9a-zA-Z\-\s]/g, '');

				if (transformedInput !== text) {
					ngModelCtrl.$setViewValue(transformedInput);
					ngModelCtrl.$render();
				}

				return transformedInput;
			}
			
			ngModelCtrl.$parsers.push(fromUser);
		}
	};
});

 

 

 


 

 

 

 

 

 

 
 