angular.module(appTeclo).factory('msjModalFactory',[function($translate){
	
	var listaMensajes = [];
	
//	MENSAJES GENÉRICOS
	listaMensajes.push({mensajeEs: 'La función de actualizar o recargar la página está deshabilitada en el modo pantalla completa',  mensajeEn: 'The function of updating or reloading the page is disabled in full screen mode'});
	listaMensajes.push({mensajeEs: 'Registro guardado correctamente',  mensajeEn: 'Registration saved correctly'});
	listaMensajes.push({mensajeEs: 'Formulario incompleto',  mensajeEn: 'Incomplete application form'});
	listaMensajes.push({mensajeEs: 'La información se registró correctamente',  mensajeEn: 'The information was registered correctly'});
	listaMensajes.push({mensajeEs: 'El registro se guardó correctamente',  mensajeEn: 'The record was saved correctly'});
	listaMensajes.push({mensajeEs: 'Ocurrio un error al registrar',  mensajeEn: 'There was an error when registering'});
	listaMensajes.push({mensajeEs: 'No se encontraron registros',  mensajeEn: 'No records found'});
	listaMensajes.push({mensajeEs: '¡ATENCIÓN!',  mensajeEn: 'ATTENTION!'});
	listaMensajes.push({mensajeEs: 'La función de actualizar o recargar la página está deshabilitada en el modo pantalla completa',  mensajeEn: 'The function of updating or reloading the page is disabled in full screen mode'});
	listaMensajes.push({mensajeEs: 'El archivo seleccionado excede el tamaño permitido de',  mensajeEn: 'The selected file exceeds the allowed size of'});
	listaMensajes.push({mensajeEs: 'El archivo seleccionado no cumple con ninguno de los formatos',  mensajeEn: 'The selected file does not comply with any of the formats'});
	listaMensajes.push({mensajeEs: 'No se pudo establecer la conexión para obtener la configuración del sistema',  mensajeEn: 'The connection could not be established to obtain the system configuration'});
	listaMensajes.push({mensajeEs: 'No existe conexión',  mensajeEn: 'There is no connection'});
	
//	MENSAJES DEL NEGOCIO
	
//	Administración
	// Usuarios
	listaMensajes.push({mensajeEs: '¿Desea reemplazar la imagen cargada por el avatar seleccionado?',  mensajeEn: 'Do you want to replace the image loaded by the selected avatar?'});
	
	//Componentes Web
	listaMensajes.push({mensajeEs: 'Título del detalle de información (Opción 1)',  mensajeEn: 'Title of popover'});
	listaMensajes.push({mensajeEs: 'Título del detalle de información (Opción 2)',  mensajeEn: 'Title of webui-popover'});
	listaMensajes.push({mensajeEs: 'Contenido del detalle de información de bootstrap',  mensajeEn: 'Content popover bootstrap'});
	listaMensajes.push({mensajeEs: '¡Éxito!',  mensajeEn: 'Success!'});
	listaMensajes.push({mensajeEs: 'Notificación de éxito',  mensajeEn: 'Success notification'});
	listaMensajes.push({mensajeEs: 'Notificación de advertencia',  mensajeEn: 'Warning notification'});
	listaMensajes.push({mensajeEs: 'Notificación de error',  mensajeEn: 'Error notification'});
	listaMensajes.push({mensajeEs: 'Notificación de información',  mensajeEn: 'Information notification'});
	listaMensajes.push({mensajeEs: 'INFORMACIÓN',  mensajeEn: 'INFORMATION'});
	listaMensajes.push({mensajeEs: 'Notificación con 2 segundos de retraso',  mensajeEn: 'Notification with 2 seconds delay'});
	
	//Configurar Aplicación
	listaMensajes.push({mensajeEs: 'La imagen seleccionada no tiene formato PNG',  mensajeEn: 'The selected image does not have PNG format'});
	listaMensajes.push({mensajeEs: 'La imagen seleccionada no tiene formato PNG o JPG|JPEG',  mensajeEn: 'The selected image does not have PNG o JPG|JPEG format'});
	listaMensajes.push({mensajeEs: 'Configuración actualizada correctamente',  mensajeEn: 'Configuration updated correctly'});
	listaMensajes.push({mensajeEs: 'Ocurrió un error al actualizar la configuración',  mensajeEn: 'There was an error updating the configuration'});
	listaMensajes.push({mensajeEs: 'Recomendaciones',  mensajeEn: 'Recomendations'});
	
	// Dispositivo
	listaMensajes.push({mensajeEs: 'Formulario Incompleto',  mensajeEn: 'Incomplete Form'});
	listaMensajes.push({mensajeEs: 'Usuario Seleccionado',  mensajeEn: 'Selected user'});	
	listaMensajes.push({mensajeEs: 'No se ha seleccionado una opción',  mensajeEn: 'An option has not been selected'});
	listaMensajes.push({mensajeEs: 'El usuario no ha sido seleccionado',  mensajeEn: 'The user has not been selected'});
	listaMensajes.push({mensajeEs: 'Éxito',  mensajeEn: 'Success'});
	listaMensajes.push({mensajeEs: 'Ingrese solo numeros',  mensajeEn: 'Enter only numbers'});
	listaMensajes.push({mensajeEs: 'No se encontraron coincidencias',  mensajeEn: 'No matches were found'});
	listaMensajes.push({mensajeEs: 'Registro agregado correctamente',  mensajeEn: 'Record added correctly'});
	listaMensajes.push({mensajeEs: 'Ya existe registro con el IMEI',  mensajeEn: 'Registration with the IMEI already exists'});
	listaMensajes.push({mensajeEs: 'Ocurrio un problema al guardar',  mensajeEn: 'There was a problem saving'});

	// Catalogo
	listaMensajes.push({mensajeEs: 'No se encontró información con los parámetros solicitados',  mensajeEn: 'No information was found with the requested parameters'});
	listaMensajes.push({mensajeEs: '¿Esta seguro de eliminar el dispositivo?',  mensajeEn: 'Are you sure you want to delete the device?'});
	listaMensajes.push({mensajeEs: 'El dispositivo se elimnó correctamente',  mensajeEn: 'The device was successfully deleted'});
	listaMensajes.push({mensajeEs: 'No se pudo eliminar el usuario',  mensajeEn: 'The user could not be deleted'});
	listaMensajes.push({mensajeEs: 'No se ha seleccionado una opción',  mensajeEn: 'An option has not been selected'});
	listaMensajes.push({mensajeEs: 'No se encontraron coincidencias',  mensajeEn: 'No matches were found'});
	listaMensajes.push({mensajeEs: '¿Esta seguro de actualizar el dispositivo?',  mensajeEn: 'Are you sure to update the device?'});
	listaMensajes.push({mensajeEs: 'Dispositivo actualizado',  mensajeEn: 'Updated device'});
	listaMensajes.push({mensajeEs: '¿Esta seguro de actualizar el catálogo?',  mensajeEn: 'Are you sure to update the catalog?'});
	listaMensajes.push({mensajeEs: 'Registro actualizado correctamente',  mensajeEn: 'Registration updated correctly'});
	listaMensajes.push({mensajeEs: 'Catálogo actualizado',  mensajeEn: 'Updated catalog'});
	
	// Comando
	listaMensajes.push({mensajeEs: 'Se ejecuto el comando exitosamente',  mensajeEn: 'The command was executed successfully'});
	listaMensajes.push({mensajeEs: 'Error al ejecutar comando',  mensajeEn: 'Error executing command'});
	listaMensajes.push({mensajeEs: 'Espere un momento, para ejecutar otro comando',  mensajeEn: 'Wait one moment, for will execute new command'});
	
	var mensaje = {

        getMensaje: function(msj, lang) {
        	
        	var item = "";
        	
        	angular.forEach(listaMensajes, function(mensajes, key) {
        		
        		if(msj == mensajes.mensajeEs && lang == "es_ES") {
        			item = mensajes.mensajeEs;
        		} else if (msj == mensajes.mensajeEs && lang == "en_US") {
        			item = mensajes.mensajeEn;
        		}
        	});
        	
        	return item;
        }
    }
    return mensaje;
}]);