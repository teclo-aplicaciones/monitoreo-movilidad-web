angular.module(appTeclo).config(function($routeProvider, $locationProvider, blockUIConfig) {
	
	$routeProvider.when("/", {
		templateUrl : "login.html",
		controller: "loginController"
	});
	
	$routeProvider.when("/login", {
		templateUrl : "login.html",
		controller: "loginController"
	});
	
	$routeProvider.when("/error", {
		templateUrl : "views/error.html",
	});
	
	$routeProvider.when("/index",{
		templateUrl : "views/index.html",
	});
	
	$routeProvider.when("/accesoNegado", {
		templateUrl : "views/accesoNegado.html",
	});
	
	$routeProvider.otherwise({redirectTo: "/index"});
	
	/*___________________________________________________________
	________** INICIO -> MONITOREO CONTROLLER ** ________*/
	$routeProvider.when("/ubicacionTiempoReal",{
		templateUrl : "views/monitoreo/ubicacionTiempoReal.html",
		controller : "monitoreoTiempoRealController"
	});
	
	$routeProvider.when("/ubicacionRuta",{
		templateUrl : "views/monitoreo/ubicacionRutas.html",
		controller : "monitoreoRutasController"
	});
	/*___________________________________________________________
	________** FIN -> MONITOREO CONTROLLER ** ___________*/
	
	/*___________________________________________________________
	________** INICIO -> GEOCERCAS CONTROLLER ** ________*/
	$routeProvider.when("/altaGeocerca",{
		templateUrl : "views/geocerca/altaGeocerca.html",
		controller : "altaGeocercaController"
	});
	
	$routeProvider.when("/modificaGeocerca",{
		templateUrl : "views/geocerca/modificaGeocerca.html",
		controller : "modificaGeocercaController",
		resolve:{
			geocercaVO:function(){return null}
		}
	});
	
	$routeProvider.when("/modificaAreaGeocerca/:id",{
		templateUrl : "views/geocerca/altaGeocerca.html",
		controller : "modificaGeocercaController",
		resolve:{
			geocercaVO:function(GeocercaService, $route){
				return GeocercaService.buscarGeocercaPorId($route.current.params.id);
			}
		}
	});
	
	$routeProvider.when("/bajaGeocerca",{
		templateUrl : "views/geocerca/bajaGeocerca.html",
		controller : "bajaGeocercaController"
	});
	/*___________________________________________________________
	________** FIN -> GEOCERCAS CONTROLLER ** ___________*/
	
	/*___________________________________________________________
	________** INICIO -> COMANDOS CONTROLLER ** ________*/
	
	$routeProvider.when("/ejecutaComando",{
		templateUrl : "views/comando/ejecutaComando.html",
		controller : "ejecutaComandoController"
	});
	/*___________________________________________________________
	________** FIN -> COMANDOS CONTROLLER ** ___________*/
	
	/*___________________________________________________________
	________** INICIO -> CATALOGO CONTROLLER ** ________*/
	$routeProvider.when("/nuevoCatalogo",{
		templateUrl : "views/catalogo/nuevoCatalogo.html",
		controller : "nuevoCatalogoController"
	});
	
	$routeProvider.when("/consultaCatalogo",{
		templateUrl : "views/catalogo/consultaCatalogo.html",
		controller : "consultaCatalogoController"
	});
	

	$routeProvider.when("/actualizaCatalogo/:id",{
		templateUrl : "views/catalogo/actualizaCatalogo.html",
		controller : "actualizaCatalogoController"		
	});
	/*___________________________________________________________
	________** FIN -> CATALOGO CONTROLLER ** ___________*/
	
	/*___________________________________________________________
	________** INICIO -> MANTENIMIENTO CONTROLLER ** ________*/
	
	$routeProvider.when("/nuevoMantenimiento",{
		templateUrl : "views/mantenimiento/nuevoMantenimiento.html",
		controller : "nuevoMantenimientoController"
	});
	
	$routeProvider.when("/consultaMantenimiento", {
		templateUrl : "views/mantenimiento/consultaMantenimiento.html",
		controller : "consultaMantenimientoController"
		
	});
	
	/*___________________________________________________________
	________** FIN -> MANTENIMIENTO CONTROLLER ** ___________*/
	
	
	/*___________________________________________________________
	________** INICIO -> NOTIFICACIONES CONTROLLER ** ________*/
	$routeProvider.when("/altaNotificacion",{
		templateUrl : "views/notificacion/altaNotificacion.html",
		controller : "altaNotificacionController"
	});
	
	$routeProvider.when("/actualizaNotificacion/:id/:isNew",{
		templateUrl : "views/notificacion/actualizaNotificacion.html",
		controller : "actualizaNotificacionController",
		resolve : {
			notificaVO:function(NotificacionService, $route){
				return NotificacionService.buscarNotificacionParaActualizar($route.current.params.id)
			},
			isNewRegistry:function($route){
				return $route.current.params.isNew === "true" ? true:false;
			}
		}
	});
	
	$routeProvider.when("/modificaNotificacion",{
		templateUrl : "views/notificacion/modificaNotificacion.html",
		controller : "modificaNotificacionController"
	});
	
	$routeProvider.when("/bajaNotificacion",{
		templateUrl : "views/notificacion/bajaNotificacion.html",
		controller : "bajaNotificacionController"
	});
	/*___________________________________________________________
	________** FIN -> NOTIFICACIONES CONTROLLER ** ___________*/
	
	/*___________________________________________________________
	________** INICIO -> GRUPOS CONTROLLER ** ________*/
	$routeProvider.when("/altaGrupo",{
		templateUrl : "views/grupo/altaGrupo.html",
		controller : "altaGrupoController"
	});
	
	$routeProvider.when("/actualizaGrupo/:id",{
		templateUrl : "views/grupo/actualizaGrupo.html",
		controller : "actualizaGrupoController",
		resolve:{
			grupoVO:function(GrupoService, $route){
				return GrupoService.buscarGrupoPorId($route.current.params.id);
			}
		}
	});
	
	$routeProvider.when("/modificaGrupo",{
		templateUrl : "views/grupo/modificaGrupo.html",
		controller : "modificaGrupoController"
	});
	
	$routeProvider.when("/bajaGrupo",{
		templateUrl : "views/grupo/bajaGrupo.html",
		controller : "bajaGrupoController"
	});
	/*___________________________________________________________
	________** FIN -> GRUPOS CONTROLLER ** ___________*/
	
	
	
	/*___________________________________________________________
	________** INICIO -> DISPOSITIVO CONTROLLER ** ________*/
	
	$routeProvider.when("/nuevoDispositivo",{
		templateUrl : "views/dispositivo/nuevoDispositivo.html",
		controller : "nuevoDispositivoController"
	});
	
	$routeProvider.when("/consultaDispositivo", {
		templateUrl : "views/dispositivo/consultaDispositivo.html",
		controller : "consultaDispositivoController"
		
	});
	
	$routeProvider.when("/actualizaDispositivo/:id",{
		templateUrl : "views/dispositivo/actualizaDispositivo.html",
		controller : "actualizaDispositivoController"		
	});
	
	/*___________________________________________________________
	________** FIN -> MANTENIMIENTO CONTROLLER ** ___________*/
	
	
	/*___________________________________________________________
	________** INICIO TABLEROS CONTROL ** ________*/
	
	$routeProvider.when("/controlDispoDiario",{
		templateUrl : "views/tableroscontrol/controlDispositivosDiario.html",
		controller : "controlDispositivosDiarioController"
	});

	$routeProvider.when("/controlDispositivos",{
		templateUrl : "views/tableroscontrol/controlDispositivosHistorico.html",
		controller : "controlDispositivosHistoricoController"
	});
	
	
	/*___________________________________________________________
	________** FIN -> TABLEROS CONTROL ** ___________*/
	
	
	
	
	
	
	/*___________________________________________________________
	________** INICIO -> ADMINISTRACIÓN CONTROLLERS ** ________*/
	$routeProvider.when("/cambioContrasena",{
		templateUrl : "views/administracion/administracionModificaClave.html",
		controller : "administracionCambioContraseniaController"
	});
	
	
    /***** LOGS *****/	

	$routeProvider.when("/administracionLogs",{
		templateUrl : "views/logs/administracionLogs.html",
		controller : "adminLogsController"
	});
	
	$routeProvider.when("/administracionLogs/administracionModificaLogs/:id",{
		templateUrl : "views/logs/administracionModificaLogs.html",
		controller : "adminModificaLogsController"
	});
	
	$routeProvider.when("/consultaLogs",{
		templateUrl : "views/logs/consultaLogs.html",
		controller : "consultaLogsController"
	});
	
	//	Usuarios
	$routeProvider.when("/users", {
		templateUrl : "views/administracion/users.html",
		controller: "usersController",
		resolve: {
			predata: async function(userService){
				
				let response = new Object();

				try {
					let typeSearch = await userService.getTypeSearch();
					let searchForAll = await userService.getTypeSearchForAll();
					let profiles = await userService.getProfiles();

					response.typeSearch = typeSearch.data;
					response.searchForAll = searchForAll.data;
					response.profiles = profiles.data;

					return response;
				} catch (e) {
					return console.log("Ocurrió un error");
				}
			}
		}
	});
	
//	Configurar Aplicación
	$routeProvider.when("/configuracion", {
		templateUrl : "views/administracion/configuracionApp.html",
		controller: "configuracionAppController"
    });
	
// Perfiles
	$routeProvider.when("/perfiles", {
		templateUrl : "views/administracion/perfiles.html",
		controller: "perfilesController"
    });
	
//	Componentes Web
	$routeProvider.when("/componentesWeb",{
		templateUrl : "views/administracion/resources/pluginsWeb.html",
		controller : "pluginsWebController"
	});
	
	/*___________________________________________________________
	________** FIN -> ADMINISTRACIÓN CONTROLLERS ** ___________*/
	
	blockUIConfig.requestFilter = function(config) {
		// var re = new
		// RegExp('\/pagos\/infracciones\/deposito$');
		if (config.url
		.match(/(\/localizar)|(\/infoDispositivosDiarios)|(\/evento\/total\/dia)|(\/evento\/dispositivos\/dia)|(\/evento\/dispositivo\/dia)$/)) {
		return false;
		}
	};

});