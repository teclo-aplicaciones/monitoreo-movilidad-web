angular.module(appTeclo).service("logsService", function ($http, config) {	
		
		
	this.consultaActivos = function () {
	   return $http.get(config.baseUrl + "/logs/consultaActivos");
	};
	
	this.consultaPorId = function (id) {
		   return $http.get(config.baseUrl + "/logs/consultaPorId",
				   {params:{"id": id}});
		   
	};
	
	this.getPerfilesActivosPorLog = function (id) {
		   return $http.get(config.baseUrl + "/logs/consultaPerfilesActivos",
				   {params:{"id": id}});
		   
	};
	this.getPerfilesNoActivosPorLog = function (id) {
		   return $http.get(config.baseUrl + "/logs/consultaPerfilesNoAsignados",
				   {params:{"id": id}});
		   
	};
	
	this.agregarPerfil = function (logVO) {
		   return $http.post(config.baseUrl + "/logs/agregarPerfil",logVO);
		   
	};
	this.eliminarPerfil = function (logVO) {
		   return $http.post(config.baseUrl + "/logs/eliminarPerfil",logVO);
		   
	};
	this.crear = function (logVO) {
		   return $http.post(config.baseUrl + "/logs/crear",logVO);
		   
	};
	this.actualizar = function (logVO) {
		   return $http.post(config.baseUrl + "/logs/actualizar",logVO);
		   
	};	
	this.cambiarEstatus = function (id,accion) {
		   return $http.get(config.baseUrl + "/logs/cambiarEstatus",
				   {params:{"id": id, "accion": accion}});
		   
	};
	this.consultaPorPerfil = function () {
		   return $http.get(config.baseUrl + "/logs/consultaPorPerfil");
		   
	};
	this.consultaArchivosPorLog = function (id) {
		   return $http.get(config.baseUrl + "/logs/consultaArchivosPorLog",
				   {params:{"id": id}});
		   
	};
	this.descargaArchivo = function (id,nombre) {
		   return $http.get(config.baseUrl + "/logs/descargaArchivo",
				   {params:{"id": id, "nombre": nombre}});
		   
		   
		   return $http({
		        method: 'GET',
		        url: config.baseUrl + "/logs/descargaArchivo",
		        params:{"id": id, "nombre": nombre},
		        dataType: "json",
		        header :{ "Content-type": "application/json"},
		        responseType: 'arraybuffer'
		    });
		   
	};
	
	

});
