angular.module(appTeclo).service('CatalogoService', function($http, $localStorage, $q, $rootScope, $route, config) {
	
	this.catalogoComponenteWeb = function(wc,callback) {		
		var menu = $route.current.originalPath;
		var menuId = null;
			
		$http.get(config.baseUrl + "/login/menus").success(function(data){
			for (var d of data){
				if(menu === d.menuUri){
					menuId = d.id;		
					break;
				}				
			}
			if(menuId !== null){
				$http.get(config.baseUrl + "/catalogoComponenteWeb",{params:{"menuId":menuId}}).success(function(data){
					
					var dataWC={};
					for(var d of data){						
						for (var w in wc ){
							if(d.nombre === w){
								switch (d.accion) {
								case 'hide':
									dataWC[w] = {
										show:false,
										disabled:false
									}
									break;									
								case 'show':
									dataWC[w] = {
										show:true,
										disabled:false
									}
									break;																
								case 'disabled':
									dataWC[w] = {
										show:true,
										disabled:true
									}
									break;
								}							
							}
						}
					}
					
					callback(dataWC);
				}).error(function(err){});				
			}else{
				callback([]);
			}
		}).error(function(err){});
	};
	
	this.buscarCatalogo = function(id) {
		return $http.get(config.baseUrl + "/buscarCatalogo");
	};
	
	this.catBusuqedaDispositivo = function() {
		return $http.get(config.baseUrl + "/catBusuqedaDispositivo");
	};
	
	this.buscarCatalogosPorId = function(id) {
		return $http.get(config.baseUrl + "/buscarCatalogosPorId", {params:{"id":id}});
	};
	
	//Catalogo para Monitoreo
	this.catalogoMonitoreo = function() {
		return $http.get(config.baseUrl + "/catalogoMonitoreo");
	}
	
	//Catalogo para Dispositivos
	this.catalogoTipoDispositivo = function() {
		return $http.get(config.baseUrl + "/catTipoDispositivo");
	}
	
	this.catalogoTipoMedio = function() {
		return $http.get(config.baseUrl + "/catalogoTipoMedio");
	}
	
	//Catalogo para Geocercas
	this.catalogoBusqGeocerca = function(){
		return $http.get(config.baseUrl + "/catBusquedaGeocerca");
	}
	
	//Catalogo para Notificaciones
	this.catalogoBusqNotificaciones = function(){
		return $http.get(config.baseUrl + "/catBusquedaNotificacion");
	}
	
	//Catalogo para Grupo
	this.catalogoBusqGrupo = function(){
		return $http.get(config.baseUrl + "/catBusquedaGrupo");
	}
	
	//Catalogo para Tipo Geocerca
	this.catalogoTipoGeocerca = function(){
		return $http.get(config.baseUrl + "/catTipoGeocerca");
	}

	this.catalogoTipoGrupo = function(){
		return $http.get(config.baseUrl + "/catTipoGrupo");
	}
	
	this.catalogoTipoEvento = function(){
		return $http.get(config.baseUrl + "/catTipoEvento");
	}
	
	this.catalogoTipoComando = function(){
		return $http.get(config.baseUrl + "/catTipoComando");
	}
	
	this.catalogoSubTipoDispositivo = function(idTipo) {
		return $http.get(config.baseUrl + "/catSubTipoDispositivo", {params:{"idTipo": idTipo}});
	}
	
	this.catalogoSubTipoDispositivoTodos = function() {
		return $http.get(config.baseUrl + "/catSubTipoDispositivoTodos");
	}
	
	this.catalogoMarcaDispositivo = function(idTipo) {
		return $http.get(config.baseUrl + "/catMarcaDispositivo", {params:{"idTipo": idTipo}});
	}
	
	this.catalogoMarca = function() {
		return $http.get(config.baseUrl + "/catMarca");
	}
	
	this.catalogoMarcaPorModelo = function(idMarca) {
		return $http.get(config.baseUrl + "/catMarcaPorModelo",{params:{"idMarca":idMarca}});
	}
	
	this.catalogoModeloDispositivo = function(idTipo, idMarca, idSubTipo) {
		return $http.get(config.baseUrl + "/catModeloDispositivo", {params:{"idTipo": idTipo, "idMarca":idMarca, "idSubTipo":idSubTipo}});
	}
	
	this.buscaCatTipoNotifica = function(){
		return $http.get(config.baseUrl + "/catTipoNotificacion");
	}

	this.buscaCatTipoNotiGeocerca = function(){
		return $http.put(config.baseUrl + "/catTipoNotiGeocerca");
	}

	this.buscaCatConfigNotiGeocerca = function(){
		return $http.put(config.baseUrl + "/catConfigNotiGeocerca");
	}

	this.catalogoCatalogo = function(){
		return $http.get(config.baseUrl + "/catCatalogo");
	}
	
	this.catalogoCatalogos = function(idCatalog){
		return $http.get(config.baseUrl + "/catCatalogos", {params:{"idCatalog": idCatalog}});
	}
	
	this.agregaCatalogo = function(itemCatalogo){
		return $http.post(config.baseUrl + "/agregaCatalogo",itemCatalogo);
	}
	
	this.actualizaCatalogo = function(itemCatalogo){
		return $http.post(config.baseUrl + "/actualizaCatalogo", itemCatalogo);
	}

	this.catalogoCatalogoId = function(id,idCatalog){
		return $http.get(config.baseUrl + "/catCatalogoId", {params:{"id": id,"idCatalog": idCatalog}});
	}

	this.activarDesactivarDispositivo = function(id,idCatalogo,activar){
		return $http.get(config.baseUrl +"/activarDesactivarCatalogo",{params: {"id":id,"idCatalogo": idCatalogo, "activar":activar}})
	}
		
});
