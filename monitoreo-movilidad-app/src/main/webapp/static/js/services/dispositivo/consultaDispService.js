angular.module(appTeclo).service('consultaDispService', function($http, jwtService, loginService, config){
	
	this.ListaBusqueda = function(){
		return listTipBus =[{
			id : 1,
			nb : 'Tipo de Dispositivo',
			st : 1,
		},
		{
			id : 2,
			nb : 'IMEI',
			st : 1
		},
		{
			id : 3,
			nb : 'Marca',
			st : 1
		},
		{
			id : 4,
			nb : 'Nombre del Disposistivo',
			st : 1
		}];
		
	}
	
	this.consultarDispositivos = function(tipo, valor){
		return $http.get(config.baseUrl +"/consultarDispositivos", {params:{"tipo":tipo, "valor":valor}})
	}
	
	this.consultarDispositivo = function(id){
		return $http.get(config.baseUrl +"/consultarDispositivo", {params:{"id":id}})
	}
	
	this.consultarTodosLosDispositivos = function(){
		return $http.get(config.baseUrl +"/consultarTodosLosDispositivos")
	}

	this.obtenerTodoDispositivoHistorico = function(){
		return $http.get(config.baseUrl +"/infoDispositivos")
	}

	this.obtenerTodoDispositivoDiario = function(){
		return $http.get(config.baseUrl +"/infoDispositivosDiarios")
	}

	this.obtenerTotalGruposEvento = function(idMes){
		return $http.get(config.baseUrl +"/evento/total/hist", {params:{"idMes":idMes}})
	}

	this.obtenerTotalEventosDispositivo = function(idMes){
		return $http.get(config.baseUrl +"/evento/dispo/hist", {params:{"idMes":idMes}})
	}
	
	this.obtenerTotalGraficaEventosDispositivo = function(idDispo, idMes){
		return $http.get(config.baseUrl +"/evento/dispo/graph", {params:{"idDispo":idDispo, "idMes":idMes}})
	}
	
	this.obtenerTotalEventoAnual = function(idGrupo, idDispo, idAnio){
		return $http.get(config.baseUrl +"/evento/anual/hist", {params:{"idGrupo":idGrupo,"idDispo":idDispo,"idAnio":idAnio}})
	}

	this.obtenerTotalEventosDiario = function(){
		return $http.get(config.baseUrl +"/evento/total/dia")
	}

	this.obtenerTotalDispositivosDiario = function(){
		return $http.get(config.baseUrl+"/evento/dispositivos/dia")
	}

	this.obtenerTotalEventosDispositivoDiario = function(idDispo){
		return $http.get(config.baseUrl+"/evento/dispositivo/dia",{params:{"idDispo":idDispo}})
	}
	
	this.obtenerEventosDispositivo = function(idDispositivo,fecha){
		return $http.get(config.baseUrl +"/evento/dispositivo", {params:{"idDispositivo":idDispositivo, "fecha":fecha}})
	}
	
	this.activarDesactivarDispositivo = function(id, activar){
		return $http.put(config.baseUrl +"/activarDesactivarDispositivo", {"id":id, "activar":activar})
	}
	
	this.eliminarDispositivo = function(id){
		return $http.put(config.baseUrl +"/eliminarDispositivo", {"id":id})
	}
	
	this.obtenerEventosDispositivoHoy = function(idGrupo, idDispositivo){
		return $http.get(config.baseUrl +"/evento/hoy", {params:{"idDispositivo":idDispositivo, "idGrupo":idGrupo}})
	}
	
	this.obtenerEventosActualesPorDispo = function(){
		return $http.get(config.baseUrl +"/evento/actual")
	}
	
});