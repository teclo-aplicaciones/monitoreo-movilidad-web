angular.module(appTeclo).service('altaDispositivoService', function($http, jwtService, loginService, config){
	
	this.guardarDispositivo = function(dispoVO){
		dispositivoGuardarVO = {
				nombre: dispoVO.nombre,
				tipo: dispoVO.tipo.id,
				subtipo: dispoVO.tipoSubtipo.id,
				marca : dispoVO.marca.id,
				modelo : dispoVO.tipModelo.id,
				imei : dispoVO.imei,
				serie : dispoVO.serie,
				idEmpleado: dispoVO.idEmpleado,
				txDispositivo: dispoVO.txDispositivo,
				nuIp: dispoVO.nuIp,
				nuCelularEmpresa: dispoVO.nuCelularEmpresa,
				nuCelularPersonal: dispoVO.nuCelularPersonal
		}
		console.log(dispositivoGuardarVO)
		return $http.put(config.baseUrl + "/guardarDispositivo", dispositivoGuardarVO);
	};
	
	this.actualizarDispositivo = function(dispoVO){
		dispositivoGuardarVO = {
				nombre: dispoVO.nombre,
				tipo: dispoVO.tipo.id,
				subtipo: dispoVO.tipoSubtipo.id,
				marca : dispoVO.marca.id,
				modelo : dispoVO.tipModelo.id,
				imei : dispoVO.imei,
				serie : dispoVO.serie,
				idEmpleado: dispoVO.idEmpleado,
				idDispositivo: dispoVO.idDispositivo,
				txDispositivo: dispoVO.txDispositivo,
				nuIp: dispoVO.nuIp,
				nuCelularEmpresa: dispoVO.nuCelularEmpresa,
				nuCelularPersonal: dispoVO.nuCelularPersonal
		}
		console.log(dispositivoGuardarVO)
		return $http.put(config.baseUrl + "/actualizarDispositivo", dispositivoGuardarVO);
	};
		
	this.ListatipoDispositivo = function(){
		return tipDisp =[
		{
			idTD : 1,
			cdDisp: 0,
			cd : 'MV',
			nb : 'Movil',
			tx : 'Aplica a todos los dispositivos moviles'
		},
		{
			idTD : 2,
			idSup: 0,
			cd : 'AM',
			nb : 'Automovil',
			tx : 'Aplica para todo los automoviles'
		},
		{
			idTD : 3,
			idSup: 0,
			cb : 'HH',
			nb : 'HandHell',
			tx : 'Solo para dispositivos HandHell'
		}];
		
	}
	
	this.ListaMarcas = function(){
		return tipMarcas =[{
			idM: 1,
			cd : 1,
			nb : 'Motorola' 
			
		},
		{
			idM : 2,
			cd : 2,
			nb : 'Nissan'
		},
		{
			idM : 3,
			cd : 3,
			nb : 'HH1'
		},
		{
			idM : 4,
			cd : 1,
			nb : 'Samsung'
		},
		{
			idM : 5,
			cd : 2,
			nb : 'Ford'
		},
		{
			idM : 6,
			cd : 3,
			nb : 'HH2'
		},
		{
			idM : 7,
			cd : 1,
			nb : 'LG'
		},
		{
			idM : 8,
			cd : 2,
			nb : 'Chevrolet'
		},
		{
			idM : 9,
			cd : 3,
			nb : 'HH3'
		}];
	}
	
	this.listaModelo = function(){
		return tipModelos = [{
			idMo : 1,
			cd: 4,
			nb : 'Samsung Grand'
		},
		{
			idMo : 2,
			cd: 4,
			nb : 'Samsung Grand Prime'
		},
		{
			idMo : 3,
			cd : 1,
			nb : 'Motorola XT1764',
		},
		{
			idMo : 4,
			cd :1,
			nb : 'Motorola Moto E 4 Plus'
		},
		{
			idMo : 5,
			cd : 7,
			nb : 'LG K10 Smartphone',
		},
		{
			idMo: 6,
			cd : 7,
			nb : 'LG K4 2017'
		},
		{
			idMo : 7,
			cd : 2,
			nb : 'Nissan Murano'
		},
		{
			idMo : 8,
			cd :2, 
			nb : 'Nissan X-Trail'
		},
		{
			idMo : 9,
			cd: 5,
			nb : 'Ford-Figo'
		},
		{
			idMo : 10, 
			cd :5,
			nb : 'Ford-Focus'
		},
		{
			idMo: 11, 
			cd :8,
			nb: 'Chevrolet Aveo'
		},
		{
			idMo : 12,
			cd:8,
			nb : 'Chevrolet Malibu'
		},
		{
			idMo : 13,
			cd : 3,
			nb : 'HH WinPad'
		},
		,
		{
			idMo : 14,
			cd :3,
			nb : 'HH BOBCAT'
		},
		{
			idMo : 15,
			cd : 6,
			nb : 'HH XSLATE'
		},
		{
			idMo : 16,
			cd :6,
			nb : 'HH HB'
		},
		{
			idMo : 17,
			cd :9,
			nb : 'HH SCANDPAD'
		},
		{
			idMo : 18,
			cd :9,
			nb : 'HH XSLATE D10'
		}];
	}
	
});