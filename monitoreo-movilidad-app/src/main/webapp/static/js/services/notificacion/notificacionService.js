angular.module(appTeclo).service('NotificacionService', function($http, $localStorage, $q, $rootScope, $timeout, $route, config) {
	
	this.consultaVO = [];
	
	/**
	 * Modulo de Alta de Notificaciones
	 */
	this.buscarAgrupamiento = function(tipo) {
		return $http.get(config.baseUrl + "/buscarAgrupamiento", {params:{"tipo":tipo}});
	};
	
	this.buscarNotificacionDispositivos = function(){
		return  $http.put(config.baseUrl + "/buscarNotificacionDispositivos");
	}
	
	this.buscarDispositivoEnGrupoParaNotificacion = function(id){
		return $http.get(config.baseUrl + "/buscarDispositivosEnGrupoParaNotificacion", {params:{"id":id}});
	}
	
	this.guardarNotificacion = function(notificacionVO){
		return $http.put(config.baseUrl + "/guardarNotificacion",notificacionVO);
	}
	
	/**
	 * Modulo de Actualiza Notificacion
	 */
	this.buscarNotificacionParaActualizar = function(id) {
		return $http.get(config.baseUrl + "/buscarNotificacionParaActualizar", {params:{"id":id}});
	};
	this.actualizaNotificacion = function(notificacionVO){
		return $http.put(config.baseUrl + "/actualizaNotificacion",notificacionVO);
	}
	/**
	 * Modulo de Modificacion de Notificaciones
	 */
	this.consultaNotificaciones = function(tipo, valor, medio, fInicial, fFinal) {
		return $http.get(config.baseUrl + "/consultaNotificaciones", {params:{"tipo":tipo, "valor":valor, "medio":medio, "fechaInicial":fInicial, "fechaFinal":fFinal}});
	};
	
	this.consultaDetalle = function(id){
		return $http.get(config.baseUrl + "/consultaDetalleNotificaciones", {params:{"id":id}});
	}
	
	this.catalogoAgrupamiento = function(id){
		return $http.get(config.baseUrl + "/buscarNotificacionAgrupamiento", {params:{"id":id}});
	}
	
	this.eliminaNotificacion = function(id){
		return $http.get(config.baseUrl + "/eliminaNotificacion", {params:{"id":id}})
	}
	
});