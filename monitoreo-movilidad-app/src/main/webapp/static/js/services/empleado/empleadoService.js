angular.module(appTeclo).service('EmpleadoService', function($http, $localStorage, $q, $rootScope, $route, config) {
	
	/**
	 * Consumo de validacion de Usuario
	 */
	this.validarUsuario = function(id) {
		return $http.get(config.baseUrl + "/validarUsuario", {params:{"id":id}});
	};
	
	/*this.buscarGeocercaPorId = function(id){
		return $http.get(config.baseUrl + "/buscarGeocercaPorId", {params:{"id":id}});
	};*/
	
	this.buscarUsuario = function(parametro, valor) {
		return $http.get(config.baseUrl + "/buscarUsuario", {params:{"param":parametro,"value":valor}});
	};
});