angular.module(appTeclo).service('MonitoreoService', function($http, $localStorage, $q, $rootScope, $route, config) {
	
	/**
	 * Modulo de Monitoreo Tiempo Real
	 */
	this.buscarAgrupamiento = function(tipo) {
		return $http.get(config.baseUrl + "/buscarAgrupamiento", {params:{"tipo":tipo}});
	};
	
	this.buscarDispositivosEnGrupo = function(id) {
		return $http.get(config.baseUrl + "/buscarDispositivosEnGrupo", {params:{"id":id}});
	};
	
	this.buscarDispositivosEnGeocerca = function(id) {
		return $http.get(config.baseUrl + "/buscarDispositivosEnGeocerca", {params:{"id":id}});
	};
	
	this.buscarDispositivosEnUsuario = function(id){
		return $http.get(config.baseUrl + "/buscarDispositivosEnUsuario",{params:{"id":id}});
	}
	
	this.buscarDispositivos = function(id) {
		return $http.get(config.baseUrl + "/buscarDispositivoPorId", {params:{"id":id}});
	};
	
	this.localiza = function(dispositivo) {
		return $http.get(config.baseUrl + "/localizar",{params:{"id":dispositivo}})
	};
	
	this.cargarDetalleDispositivo = function(id){
		return $http.get(config.baseUrl + "/cargarDetalleDispositivo",{params:{"id":id}});
	}
	
	/**
	 * Modulo de Monitoreo Rutas
	 */
	
	this.buscarRutaDispositivo = function(id, tipoBusq, fechaInicio, fechaFin){
		return $http.get(config.baseUrl + "/buscarRutaDispositivo", {params:{"id":id,"tipoBusq":tipoBusq , "fechaInicio":fechaInicio, "fechaFin":fechaFin}})
	};
	
	this.generarExcel = function(id, tipoBusq, fechaInicio, fechaFin){
		return $http({
		       method: 'GET',
		       url: config.baseUrl + "/generarExcel",
		       params: {"id":id,"tipoBusq":tipoBusq , "fechaInicio":fechaInicio, "fechaFin":fechaFin},
		       dataType: "json",
		       header :{ "Content-type": "application/json",
		       	"Accept"    : "vnd.openxmlformats-officedocument.spreadsheetml.sheet"
		       },
		       responseType: 'arraybuffer'
		   });
	};
	
	/**
	 *  Utilidades
	 */
	this.descargarArchivo = function(file, fileName){
		var url = window.URL || window.webkitURL;
		var blobUrl = url.createObjectURL(file);
		var a = document.createElement('a');
		a.href = blobUrl;
		a.target = '_blank';
		a.download = fileName;
		document.body.appendChild(a);
		a.click();
	};
});