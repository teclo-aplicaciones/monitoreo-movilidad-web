angular.module(appTeclo).service('GeocodingService', function($http, $localStorage, $q, $rootScope, $route, config, blockUI) {
	$rootScope.geoCoder = ($rootScope.geoCoder == null ? new google.maps.Geocoder() : $rootScope.geoCoder) ;
	this.getLatLngFromAddress = function(address, callback){
		//blockUI.start();
		$rootScope.geoCoder.geocode({'address':address}, callback);
	}
});