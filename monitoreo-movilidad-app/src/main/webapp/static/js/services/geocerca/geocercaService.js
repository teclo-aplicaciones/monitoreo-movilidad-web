angular.module(appTeclo).service('GeocercaService', function($http, $localStorage, $q, $rootScope, $route, config) {
	
	this.consultaVO = [];
	
	/**
	 * Modulo de Alta de Geocerca
	 */
	
	this.buscarGeocercaDispositivos = function(){
		return  $http.put(config.baseUrl + "/buscarGeocercaDispositivos");
	}
	
	this.buscarDispositivoEnGrupoParaGeocerca = function(id){
		return $http.get(config.baseUrl + "/buscarDispositivosEnGrupoParaGeocerca", {params:{"id":id}});
	}
	
	this.guardarGeocerca = function(geocercaVO){
		return $http.put(config.baseUrl + "/guardarGeocerca",geocercaVO);
	}
	
	/**
	 * Modulo de Modificacion Geocerca
	 */
	
	this.catalogoGeocerca = function(id){
		return $http.get(config.baseUrl + "/buscarGeocercaAgrupamiento", {params:{"id":id}});
	}
	this.consultaGeocercas = function(tipo, valor) {
		return $http.get(config.baseUrl + "/buscarGeocerca", {params:{"tipo":tipo, "valor":valor}});
	};
	
	this.buscarGeocercaPorId = function(id){
		return $http.get(config.baseUrl + "/buscarGeocercaPorId", {params:{"id":id}});
	};
	this.cambiarEstadoGeocerca = function(id){
		return $http.get(config.baseUrl + "/cambiarEstadoGeocerca", {params:{"id":id}});
	}
	
	this.buscarDetalleGeocerca = function(id){
		return $http.get(config.baseUrl + "/buscarDetalleGeocerca", {params:{"id":id}});
	}
	
	this.eliminaGeocercaPerma = function(id){
		return $http.get(config.baseUrl + "/eliminaGeocercaPerma", {params:{"id":id}});
	}
	
	this.actualizaGeocerca = function(geocercaVO){
		return $http.put(config.baseUrl + "/actualizarGeocerca",geocercaVO);
	}
	
});