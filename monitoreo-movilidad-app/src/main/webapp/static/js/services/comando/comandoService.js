angular.module(appTeclo).service('ComandoService', function($http, $localStorage, $q, $rootScope, $route, config) {
	
	/**
	 * Modulo de Alta de Comandos
	 */
	this.buscarAgrupamiento = function(tipo) {
		return $http.get(config.baseUrl + "/buscarAgrupamientoConIdSubtipoDispositivo", {params:{"tipo":tipo}});
	};
	
	this.buscarDispositivosEnGrupo = function(id) {
		return $http.get(config.baseUrl + "/buscarDispositivosEnGrupo", {params:{"id":id}});
	};
	
	this.buscarDispositivosEnGeocerca = function(id) {
		return $http.get(config.baseUrl + "/buscarDispositivosEnGeocerca", {params:{"id":id}});
	};
	
	/**
	 * Modulo de Modificación de Comandos
	 */
	this.consultaComandos = function(tipo, valor) {
		return $http.get(config.baseUrl + "/consultaComandos", {params:{"tipo":tipo, "valor":valor}});
	};
	
	/**
	 * Modulo de Actualizar comandos
	 */
	this.buscarComandoPorId = function(id) {
		return $http.get(config.baseUrl + "/buscarComandoPorId", {params:{"id":id}});
	};
	
	this.catalogoComandos = function(idDispositivo,idTipoMedioComunicacion) {
		return $http.get(config.baseUrl + "/catalogoComandos", {params:{"idDispositivo":idDispositivo,"idTipoMedioComunicacion":idTipoMedioComunicacion}});
	};
	
	this.obtenerComandosDispositivo = function(idDispositivo) {
		return $http.get(config.baseUrl + "/obtenerComandosDispositivo", {params:{"idDispositivo":idDispositivo}});
	};
	
	this.activarDesactivarComandoPorUsuario = function(idDispositivo,idComando,activo){
		return $http.get(config.baseUrl +"/activarDesactivarComandoPorUsuario",{params: {"idDispositivo":idDispositivo,"idComando": idComando, "activo":activo}})
	}

	this.execCommand = function(idDispositivo,idComando){
		return $http.get(config.baseUrl +"/execCommand",{params: {"idDispositivo":idDispositivo,"idComando": idComando}})
	}

	this.logsCommand = function(idDispositivo){
		return $http.get(config.baseUrl +"/logsCommand",{params: {"idDispositivo":idDispositivo}});
	}
	
	this.logsCommandDetalle = function(idBitacora){
		return $http.get(config.baseUrl +"/logsCommandDetalle",{params: {"idBitacora":idBitacora}});
	}
	
	this.comandoArchivo = function(idArchivo){
		return $http.get(config.baseUrl +"/comandoArchivo",{params: {"idArchivo":idArchivo}});
	}
	
});
