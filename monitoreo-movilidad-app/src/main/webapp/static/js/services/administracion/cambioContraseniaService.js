angular.module(appTeclo).service('cambioContraseniaService', function($http, config) {

	var myPath = "/usuarios";
	   
	this.cambioContrasenia = function ( oldPassword, newPassword) {
        var cambioContrasenia = {"password": newPassword, "oldPassword": oldPassword};
		return $http.put(config.baseUrl + myPath + "/password",cambioContrasenia);
    }
	
});