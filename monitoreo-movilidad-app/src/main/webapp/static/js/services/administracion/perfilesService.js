angular.module(appTeclo).service("PerfilesService", function($http, config) {
	
	
	this.cargarPerfiles = function (id){
		return $http.get(config.baseUrl + "/consultaPerfiles");
	};
	
	this.cargarAccesosPerfil = function (id){
		return $http.get(config.baseUrl + "/consultaMenusPorPerfil", {params:{"profile":id}});
	};
	
	this.asignarAccesosPerfil = function (perfil, menu, codigo){
		return $http.get(config.baseUrl + "/asignaAccesoPerfil", {params:{"profile":perfil, "menu":menu, "codigo":codigo}});
	};
	
	this.habilitaDeshabilitaPerfil = function(idPerfil,activo){
		return $http.get(config.baseUrl + "/habilitaDeshabilitaPerfil", {params:{"profile":idPerfil, "activo":activo}});
	}
	
	this.restablecerMenus = function(accesosVO){
		return $http.put(config.baseUrl + "/restablecerAccesoPerfil", accesosVO);
	}
	
	this.crearPerfil = function (perfilVO){
		return $http.put(config.baseUrl + "/crearPerfil", perfilVO);
	};
});