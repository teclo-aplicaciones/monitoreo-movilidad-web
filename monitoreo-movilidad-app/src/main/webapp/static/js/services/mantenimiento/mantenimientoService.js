angular.module(appTeclo).service('mantenimientoService', function($http, jwtService, loginService, config) {
	
	this.ListaMantenimientos = function(){
		return $http.get(config.baseUrl + "/tipoMantenimiento");
	}
	
	this.ListaDispositivos = function(){
		return $http.get(config.baseUrl + "/listaDispositivos");		
	}
	
	this.busqMantenimiento = function(){
		return $http.get(config.baseUrl + "/busqMantenimiento");		
	}
	
	this.consultarMantenimiento = function(tipo, valor){
		return $http.get(config.baseUrl + "/consultaMantenimientoBusqueda",  {params:{"tipo":tipo, "valor":valor}});
	}
	
	this.buscarMantenimientoPorId = function(id){
		return $http.get(config.baseUrl + "/buscarMantenimientoPorId", {params:{"id":id}});
	}
	
	this.agregaMantenimiento = function(mantenimiento){
		return $http.post(config.baseUrl + "/agregaMantenimiento",mantenimiento);
	}
	
	this.actualizaMantenimiento = function(mantenimiento){
		return $http.post(config.baseUrl + "/actualizaMantenimiento",mantenimiento);
	}
	
	this.eliminarMantenimiento = function(idMantenimiento){
		return $http.get(config.baseUrl + "/eliminarMantenimiento", {params:{"idMantenimiento":idMantenimiento}});
	}
	
	this.finalizarMantenimiento = function(idMantenimiento,observaciones){
		return $http.get(config.baseUrl + "/finalizarMantenimiento", {params:{"idMantenimiento":idMantenimiento,"observaciones":observaciones}});
	}
	
	this.timelineMantenimiento = function(idMantenimiento,idUsuarioOIMEI,tipoTimeline){
		return $http.get(config.baseUrl + "/timelineMantenimiento", {params:{"idMantenimiento":idMantenimiento,"idUsuarioOIMEI":idUsuarioOIMEI,"tipoTimeline":tipoTimeline}});
	}
	
});