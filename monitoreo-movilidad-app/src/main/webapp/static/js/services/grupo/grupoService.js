angular.module(appTeclo).service('GrupoService', function($http, $localStorage, $q, $rootScope, $route, config) {
	
	this.consultaVO = [];
	
	/**
	 * Modulo de Consulta Grupos
	 */
	this.consultaGrupos = function(tipo, valor) {
		return $http.get(config.baseUrl + "/consultarGrupos", {params:{"idTipoBusq":tipo, "valor":valor}});
	};
	
	/**
	 * Consulta criterios de valor por tipo busqueda
	 */
	this.catalogoGrupo = function(id) {
		return $http.get(config.baseUrl + "/buscarGrupoAgrupamiento", {params:{"id":id}});
	};
	
	/**
	 * Modulo de Actualiza Grupo
	 */
	this.buscarGrupoPorId = function(id) {
		return $http.get(config.baseUrl + "/buscarGrupoPorId", {params:{"id":id}});
	};
	
	/**
	 * Buscar dispositivos para agregar en grupo
	 */
	this.buscarGrupoDispositivos = function(){
		return $http.get(config.baseUrl + "/buscarGrupoDispositivos");
	}
	
	/**
	 * Guardar grupo
	 */
	this.guardarGrupo = function(grupoVO){
		return $http.put(config.baseUrl + "/guardarGrupo", grupoVO);
	}
	
	
	/**
	 * Actualizar grupo
	 */
	this.actualizarGrupo = function(grupoVO){
		return $http.put(config.baseUrl + "/actualizarGrupo", grupoVO);
	}
	
	/**
	 * Eliminar grupo
	 */
	this.eliminarGrupo = function(idGrupo){
		return $http.get(config.baseUrl + "/eliminarGrupo", {params:{"id":idGrupo}});
	}
});

