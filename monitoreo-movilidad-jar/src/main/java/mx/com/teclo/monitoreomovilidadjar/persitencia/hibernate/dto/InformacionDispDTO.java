package mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TMM053D_DP_INFORMACION_DISP")
public class InformacionDispDTO implements Serializable {

	private static final long serialVersionUID = -4971965127267248468L;

	@Id
	@Column(name = "ID_INFORMACION_DISP")
	private Long idInformacionDisp;
	
	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_DISPOSITIVO")
	private DispositivoDTO idDispositivo;
	
	@Column(name = "NU_IMEI") 
	private Long nuImei;
	
	@Column(name = "NU_SERIE")  
	private String nuSerie;
	
	@Column(name = "NU_IP")  
	private String nuIp;
	
	@Column(name = "NU_CELULAR_EMPRESA")  
	private String nuCelularEmpresa;
	
	@Column(name = "NU_CELULAR_PERSONAL")  
	private String nuCelularPersonal;
	
	@Column(name = "ST_ACTIVO")
	private Boolean stActivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;
	public Long getIdInformacionDisp() {
		return idInformacionDisp;
	}
	public void setIdInformacionDisp(Long idInformacionDisp) {
		this.idInformacionDisp = idInformacionDisp;
	}
	public DispositivoDTO getIdDispositivo() {
		return idDispositivo;
	}
	public void setIdDispositivo(DispositivoDTO idDispositivo) {
		this.idDispositivo = idDispositivo;
	}
	public Long getNuImei() {
		return nuImei;
	}
	public void setNuImei(Long nuImei) {
		this.nuImei = nuImei;
	}
	public String getNuSerie() {
		return nuSerie;
	}
	public void setNuSerie(String nuSerie) {
		this.nuSerie = nuSerie;
	}
	public String getNuIp() {
		return nuIp;
	}
	public void setNuIp(String nuIp) {
		this.nuIp = nuIp;
	}
	public String getNuCelularEmpresa() {
		return nuCelularEmpresa;
	}
	public void setNuCelularEmpresa(String nuCelularEmpresa) {
		this.nuCelularEmpresa = nuCelularEmpresa;
	}
	public String getNuCelularPersonal() {
		return nuCelularPersonal;
	}
	public void setNuCelularPersonal(String nuCelularPersonal) {
		this.nuCelularPersonal = nuCelularPersonal;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
	
	
}
