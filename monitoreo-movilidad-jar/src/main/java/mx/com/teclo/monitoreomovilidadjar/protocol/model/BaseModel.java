package mx.com.teclo.monitoreomovilidadjar.protocol.model;

public class BaseModel {

    private long id;

    public final long getId() {
        return id;
    }

    public final void setId(long id) {
        this.id = id;
    }

}
