package mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.dao;

import java.util.List;
import mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.comun.BaseDAO;
import mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.dto.LogGPSDTO;

public interface LogGPSDAO extends BaseDAO<LogGPSDTO>{
	public List<LogGPSDTO> getLogGPSs();
}
