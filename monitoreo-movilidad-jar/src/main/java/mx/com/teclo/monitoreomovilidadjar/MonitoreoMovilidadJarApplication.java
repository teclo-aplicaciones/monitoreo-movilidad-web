package mx.com.teclo.monitoreomovilidadjar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;

import mx.com.teclo.monitoreomovilidadjar.service.socket.SocketServer;

@SpringBootApplication
public class MonitoreoMovilidadJarApplication implements CommandLineRunner {

	@Autowired
	private SocketServer socketServer;

	public static void main(String[] args) throws Exception {
		new SpringApplication(MonitoreoMovilidadJarApplication.class).run(args);
	}

	@Override
	public void run(String... args) throws Exception {
		socketServer.start();
	}

	@Bean
	public HibernateJpaSessionFactoryBean sessionFactory() {
		return new HibernateJpaSessionFactoryBean();
	}
}