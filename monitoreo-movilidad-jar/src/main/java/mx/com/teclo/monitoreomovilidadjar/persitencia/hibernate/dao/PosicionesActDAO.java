package mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.dao;

import mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.comun.BaseDAO;
import mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.dto.PosicionesActDTO;

public interface PosicionesActDAO extends BaseDAO<PosicionesActDTO> {

	public PosicionesActDTO findByIdDispositivoAndImei(Long idDispositivo, Long nuImei);
	public PosicionesActDTO guardarPosicionActualCompleto(PosicionesActDTO posicionesActDTO);
	public PosicionesActDTO actualizarPosicionActualCompleto(PosicionesActDTO posicionesActDTO);
}
