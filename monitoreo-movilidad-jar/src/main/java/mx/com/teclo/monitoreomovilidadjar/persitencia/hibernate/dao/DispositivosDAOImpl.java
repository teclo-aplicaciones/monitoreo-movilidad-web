package mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.dto.DispositivoDTO;

@Repository
public class DispositivosDAOImpl extends BaseDAOImpl<DispositivoDTO> implements DispositivosDAO{

	@Override
	public DispositivoDTO findByImei(Long nuImei) {
		Criteria query = getCurrentSession().createCriteria(DispositivoDTO.class);
		query.createAlias("idInformacionDisp", "idInformacionDisp");
		query.add(Restrictions.eq("idInformacionDisp.nuImei", nuImei));
		query.add(Restrictions.eq("idInformacionDisp.stActivo", Boolean.TRUE));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));

		List<DispositivoDTO> list = (List<DispositivoDTO>) query.list(); 
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

}
