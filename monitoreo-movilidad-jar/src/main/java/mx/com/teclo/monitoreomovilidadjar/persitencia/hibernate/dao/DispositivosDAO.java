package mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.dao;


import mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.comun.BaseDAO;
import mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.dto.DispositivoDTO;

public interface DispositivosDAO extends BaseDAO<DispositivoDTO>{

	public DispositivoDTO findByImei(Long nuImei);

}
