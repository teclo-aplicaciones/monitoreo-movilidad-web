package mx.com.teclo.monitoreomovilidadjar.service.coordenadas;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.dao.DispositivosDAO;
import mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.dao.PosicionesActDAO;
import mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.dto.DispositivoDTO;
import mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.dto.PosicionesActDTO;
import mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.vo.PosicionesActVO;
import mx.com.teclo.monitoreomovilidadjar.persitencia.mybatis.PosicionesActMapper;
import mx.com.teclo.monitoreomovilidadjar.service.google.ServicioGoogle;

@Service
public class CoordenadasServiceImpl implements CoordenadaService {

	@Autowired
	private PosicionesActDAO posicionesActDAO;
	@Autowired
	private DispositivosDAO dispositivosDAO;
	@Autowired
	private PosicionesActMapper posicionesActMapper;

	@Autowired
	private ServicioGoogle servicioGoogle;

	@Transactional
	@Override
	public PosicionesActVO agregarCoordenada(PosicionesActVO posicionesActVO) {
		DispositivoDTO disp;
		PosicionesActDTO pos, posConsulta;

		/*
		 * Buscar los datos del dispositivo que tiene asignado el numero imei que
		 * reporta las coordenadas
		 */
		disp = dispositivosDAO.findByImei(posicionesActVO.getNuImei());
		if (disp != null) {

			/* Obtener la direccion y la altitud a travez de los servicios de google */
			String direccion = servicioGoogle.buscarDireccion(posicionesActVO.getNuLatitud(),
					posicionesActVO.getNuLongitud());
			Double altitud = servicioGoogle.buscarAltitud(posicionesActVO.getNuLatitud(),
					posicionesActVO.getNuLongitud());

			/*
			 * Buscar si existe registro de posiciones actuales del dispositivo encontrado
			 * con el numero imei
			 */
			posConsulta = posicionesActDAO.findByIdDispositivoAndImei(disp.getIdDispositivo(),
					posicionesActVO.getNuImei());

			pos = new PosicionesActDTO();
			pos.setIdDispositivo(disp);
			pos.setNuLatitud(posicionesActVO.getNuLatitud());
			pos.setNuLongitud(posicionesActVO.getNuLongitud());
			pos.setNuAccuracy(posicionesActVO.getNuAccuracy());
			pos.setNuAltitud(altitud);
			pos.setNuImei(posicionesActVO.getNuImei());
			pos.setNuIp(posicionesActVO.getNuIp());
			pos.setTxDireccion(direccion);
			pos.setFhRegistro(posicionesActVO.getFhRegistro());
			pos.setStActivo(true);
			pos.setIdUsrCreacion(1L);
			pos.setFhCreacion(new Date());
			pos.setIdUsrModifica(1L);
			pos.setFhModificacion(new Date());
			pos.setIdEvento(posicionesActVO.getIdEvento());

			if (posConsulta == null) {
				posicionesActMapper.insertarPosicionesActuales(pos);
			} else {
				pos.setIdPosicionesAct(posConsulta.getIdPosicionesAct());
				posicionesActMapper.actualizarPosicionesActuales(pos);
			}

			return posicionesActVO;
		}
		// si no existe el dispositivo registrado no se agregan los datos en la tabla de
		// posiciones actuales ...
		else
			return null;
	}

}
