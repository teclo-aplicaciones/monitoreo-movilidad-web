package mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.vo;

import java.io.Serializable;
import java.util.Date;

public class PosicionesActVO implements Serializable{

	private static final long serialVersionUID = 6987108582570490458L;

	private Long idPosicionesAct;
	private Long idDispositivo;
	private Long idEvento;
	private Double nuLatitud;
	private Double nuLongitud;
	private Double nuAltitud;
	private Long nuImei;
	private String nuIp;
	private Date fhRegistro;
	private Double nuAccuracy;

	public Double getNuAccuracy() {
		return nuAccuracy;
	}
	public void setNuAccuracy(Double nuAccuracy) {
		this.nuAccuracy = nuAccuracy;
	}
	public Long getIdPosicionesAct() {
		return idPosicionesAct;
	}
	public void setIdPosicionesAct(Long idPosicionesAct) {
		this.idPosicionesAct = idPosicionesAct;
	}
	public Long getIdEvento() {
		return idEvento;
	}
	public void setIdEvento(Long idEvento) {
		this.idEvento = idEvento;
	}
	public Long getIdDispositivo() {
		return idDispositivo;
	}
	public void setIdDispositivo(Long idDispositivo) {
		this.idDispositivo = idDispositivo;
	}
	public Double getNuLatitud() {
		return nuLatitud;
	}
	public void setNuLatitud(Double nuLatitud) {
		this.nuLatitud = nuLatitud;
	}
	public Double getNuLongitud() {
		return nuLongitud;
	}
	public void setNuLongitud(Double nuLongitud) {
		this.nuLongitud = nuLongitud;
	}
	public Double getNuAltitud() {
		return nuAltitud;
	}
	public void setNuAltitud(Double nuAltitud) {
		this.nuAltitud = nuAltitud;
	}
	public Long getNuImei() {
		return nuImei;
	}
	public void setNuImei(Long nuImei) {
		this.nuImei = nuImei;
	}
	public String getNuIp() {
		return nuIp;
	}
	public void setNuIp(String nuIp) {
		this.nuIp = nuIp;
	}

	public Date getFhRegistro() {
		return fhRegistro;
	}
	public void setFhRegistro(Date fhRegistro) {
		this.fhRegistro = fhRegistro;
	}
	@Override
	public String toString() {
		return "PosicionesActVO [idPosicionesAct=" + idPosicionesAct + ", idDispositivo=" + idDispositivo
				+ ", idEvento=" + idEvento + ", nuLatitud=" + nuLatitud + ", nuLongitud=" + nuLongitud + ", nuAltitud="
				+ nuAltitud + ", nuImei=" + nuImei + ", nuIp=" + nuIp + ", fhRegistro=" + fhRegistro + ", nuAccuracy="
				+ nuAccuracy + "]";
	}
}
