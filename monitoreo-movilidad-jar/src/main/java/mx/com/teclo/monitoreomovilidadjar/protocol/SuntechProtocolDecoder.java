package mx.com.teclo.monitoreomovilidadjar.protocol;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import mx.com.teclo.monitoreomovilidadjar.protocol.helper.UnitsConverter;
import mx.com.teclo.monitoreomovilidadjar.protocol.model.Position;

public class SuntechProtocolDecoder {

	public SuntechProtocolDecoder() {
	}

	private Position decode9(String[] values) throws ParseException {
		int index = 1;		
		String type = values[index++];
		
		if (!type.equals("Location") && !type.equals("Emergency") && !type.equals("Alert")) {
			return null;
		}
		
		Position position = new Position();
		position.setId(Long.parseLong(values[index++]));

		if (type.equals("Emergency") || type.equals("Alert")) {
			position.set(Position.KEY_ALARM, Position.ALARM_GENERAL);
		}

		if (!type.equals("Alert")) {
			position.set(Position.KEY_VERSION_FW, values[index++]);
		}

		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		position.setTime(dateFormat.parse(values[index++] + values[index++]));

		index++;
		
		position.setLatitude(Double.parseDouble(values[index++]));
		position.setLongitude(Double.parseDouble(values[index++]));
		position.setSpeed(UnitsConverter.knotsFromKph(Double.parseDouble(values[index++])));
		position.setCourse(Double.parseDouble(values[index++]));

		position.setValid(values[index++].equals("1"));
		
		return position;
	}

	public Object decode(String msg) throws Exception {

		String[] values = msg.split(";");
		Object object = null;

		switch (values[0]) {
		case "910":
			object = decode9(values);
			break;

		default:
			break;
		}

		return object;

	}
}
