package mx.com.teclo.monitoreomovilidadjar.service.logs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.dao.LogGPSDAO;
import mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.dto.LogGPSDTO;


@Service
@Transactional
public class LogServiceImpl implements LogService{

	@Autowired
	private LogGPSDAO logGpsdao;
	
	public List<LogGPSDTO> getLogGPSs(){
		return logGpsdao.getLogGPSs();
	}
	
	@Transactional
	public void saveLog(String tx) {
		LogGPSDTO logGPSDTO = new LogGPSDTO();
		logGPSDTO.setIdLogGPS(1L);
		logGPSDTO.setTxLogGPS(tx);
		logGpsdao.save(logGPSDTO);
	}

}
