package mx.com.teclo.monitoreomovilidadjar.service.dispositivo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.dao.DispositivosDAO;
import mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.dto.DispositivoDTO;
import mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.vo.DispositivoVO;

@Service
public class DispositivoServiceImpl implements DispositivoService{

	@Autowired
	DispositivosDAO dispositivosDAO;
	
	@Transactional(readOnly = true)
	@Override
	public DispositivoVO searchDispositivobyIMEI(Long imei) {
		DispositivoDTO dispositivoDTO = dispositivosDAO.findByImei(imei);
		DispositivoVO dispositivoVO= new DispositivoVO();
		
		dispositivoVO.setId(dispositivoDTO.getIdDispositivo());
		
		return dispositivoVO;
	}

}
