package mx.com.teclo.monitoreomovilidadjar.service.google;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.maps.ElevationApi;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.ElevationResult;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;

import mx.com.teclo.monitoreomovilidadjar.service.ConfigService;

@Service
public class ServicioGoogleImpl implements ServicioGoogle {

	@Autowired
	ConfigService configService;

	@Override
	public String buscarDireccion(Double lat, Double lon) {
		final GeoApiContext context = new GeoApiContext.Builder().apiKey(configService.getGoogleApiKey()).build();
		String direccion = "S/I";
		GeocodingResult[] results;
		try {
			LatLng latlng = new LatLng(lat, lon);
			results = GeocodingApi.newRequest(context).latlng(latlng).await();
			if (results.length > 0)
				direccion = results[0].formattedAddress;
		} catch (final Exception e) {
		}
		return direccion;
	}

	@Override
	public Double buscarAltitud(Double lat, Double lon) {
		final GeoApiContext context = new GeoApiContext.Builder().apiKey(configService.getGoogleApiKey()).build();
		Double altitud = 0.0;
		ElevationResult results;
		try {
			LatLng latlng = new LatLng(lat, lon);
			results = ElevationApi.getByPoint(context, latlng).await();
			altitud = results.elevation;
		} catch (final Exception e) {
		}
		return altitud;
	}
}
