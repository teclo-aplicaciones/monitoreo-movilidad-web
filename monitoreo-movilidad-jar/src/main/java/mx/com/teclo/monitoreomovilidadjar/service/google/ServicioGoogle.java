package mx.com.teclo.monitoreomovilidadjar.service.google;

public interface ServicioGoogle {
	public String buscarDireccion(Double lat, Double lon);
	public Double buscarAltitud(Double lat, Double lon);

}
