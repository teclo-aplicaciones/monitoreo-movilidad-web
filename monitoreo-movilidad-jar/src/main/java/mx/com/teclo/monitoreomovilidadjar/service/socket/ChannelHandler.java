package mx.com.teclo.monitoreomovilidadjar.service.socket;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;
import mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.vo.DispositivoVO;
import mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.vo.PosicionesActVO;
import mx.com.teclo.monitoreomovilidadjar.protocol.SuntechProtocolDecoder;
import mx.com.teclo.monitoreomovilidadjar.protocol.model.Position;
import mx.com.teclo.monitoreomovilidadjar.service.coordenadas.CoordenadaService;
import mx.com.teclo.monitoreomovilidadjar.service.dispositivo.DispositivoService;
import mx.com.teclo.monitoreomovilidadjar.service.logs.LogService;

@Service
public class ChannelHandler extends ChannelInboundHandlerAdapter {

	private Logger log = Logger.getLogger(getClass().getName());

	@Autowired
	private LogService logService;
	@Autowired
	private CoordenadaService coordenadaService;
	@Autowired
	private DispositivoService dispositivoService;

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		ByteBuf in = (ByteBuf) msg;
		in.readShort();
		try {
			String message = in.toString(io.netty.util.CharsetUtil.US_ASCII);
			logService.saveLog(message);
			initProtocol(message);
		} finally {
			System.out.flush();
			ReferenceCountUtil.release(msg);
		}
	}

	private void initProtocol(String message) {
		SuntechProtocolDecoder decoder = new SuntechProtocolDecoder();
		Position position = null;
		try {
			position = (Position) decoder.decode(message);
			PosicionesActVO posicionesActVO = new PosicionesActVO();
			DispositivoVO dispositivoVO = dispositivoService.searchDispositivobyIMEI(position.getId());

			posicionesActVO.setNuImei(position.getId());
			posicionesActVO.setIdDispositivo(dispositivoVO.getId());
			posicionesActVO.setIdEvento(2L);
			posicionesActVO.setFhRegistro(position.getDeviceTime());
			posicionesActVO.setNuIp("1.1.1.1");
			posicionesActVO.setNuAccuracy(position.getAccuracy());
			posicionesActVO.setNuLatitud(position.getLatitude());
			posicionesActVO.setNuLongitud(position.getLongitude());
			posicionesActVO.setNuAltitud(position.getAltitude());

			coordenadaService.agregarCoordenada(posicionesActVO);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		// ctx.writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
		ctx.close();
	}

}
