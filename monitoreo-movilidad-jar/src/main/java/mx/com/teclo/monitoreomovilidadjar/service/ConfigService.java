package mx.com.teclo.monitoreomovilidadjar.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author sinuhe
 *
 */
@Service
public class ConfigService {

	@Value("${gps.port}")
	private int gpsPort;

	@Value("${google.apikey}")
	private String googleApiKey;

	public int getGpsPort() {
		return gpsPort;
	}

	public String getGoogleApiKey() {
		return googleApiKey;
	}

}
