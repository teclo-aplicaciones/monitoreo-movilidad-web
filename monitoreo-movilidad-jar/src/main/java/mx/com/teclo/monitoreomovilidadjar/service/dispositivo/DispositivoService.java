package mx.com.teclo.monitoreomovilidadjar.service.dispositivo;

import mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.vo.DispositivoVO;

public interface DispositivoService {
	DispositivoVO searchDispositivobyIMEI(Long imei);
}
