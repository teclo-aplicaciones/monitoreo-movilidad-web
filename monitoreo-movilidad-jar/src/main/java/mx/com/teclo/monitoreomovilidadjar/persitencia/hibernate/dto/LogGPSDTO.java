package mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LOG_GPS")
public class LogGPSDTO implements Serializable {

	private static final long serialVersionUID = -4406901921013624600L;

	@Id
	@Column(name = "ID_LOG_GPS")
	private Long idLogGPS;
	@Column(name = "TX_LOG_GPS")
	private String txLogGPS;

	public Long getIdLogGPS() {
		return idLogGPS;
	}

	public void setIdLogGPS(Long idLogGPS) {
		this.idLogGPS = idLogGPS;
	}

	public String getTxLogGPS() {
		return txLogGPS;
	}

	public void setTxLogGPS(String txLogGPS) {
		this.txLogGPS = txLogGPS;
	}

}
