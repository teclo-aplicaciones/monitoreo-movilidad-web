package mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.dto.LogGPSDTO;

@SuppressWarnings("unchecked")
@Repository("LogGPSDAO")
public class LogGPSDAOImpl extends BaseDAOImpl<LogGPSDTO> implements LogGPSDAO{

	@Override
	@Transactional(readOnly = true)
	public List<LogGPSDTO> getLogGPSs(){		
		Criteria query = getCurrentSession().createCriteria(LogGPSDTO.class);		
		return (List<LogGPSDTO>)query.list();
	}	
}
