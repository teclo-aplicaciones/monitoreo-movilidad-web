package mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TMM054D_GL_POSICIONES_ACT")
public class PosicionesActDTO implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID_POSICIONES_ACT")
	@SequenceGenerator(name="TMM054D", sequenceName="SQTMM054D_GL_POSI", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TMM054D")
	private Long idPosicionesAct;
	@ManyToOne
    @JoinColumn(name="ID_DISPOSITIVO", nullable=false)
	private DispositivoDTO idDispositivo;
    @Column(name="ID_EVENTO")
	private Long idEvento;
	@Column(name = "NU_LATITUD")
	private Double nuLatitud;
	@Column(name = "NU_LONGITUD")
	private Double nuLongitud;
	@Column(name = "NU_ALTITUD")
	private Double nuAltitud;
	@Column(name = "NU_ACCURACY")
	private Double nuAccuracy;
//	@Column(name="THE_GEOM")
//	@Type(type="jts_geometry")
//	private Point theGeom;
	@Column(name = "NU_IMEI")
	private Long nuImei;
	@Column(name = "NU_IP")
	private String nuIp;
	@Column(name = "TX_DIRECCION")
	private String txDireccion;
	@Column(name = "FH_REGISTRO")
	private Date fhRegistro;
	@Column(name = "ST_ACTIVO")
	private Boolean stActivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;



	public Double getNuAccuracy() {
		return nuAccuracy;
	}
	public void setNuAccuracy(Double nuAccuracy) {
		this.nuAccuracy = nuAccuracy;
	}
	public Long getIdPosicionesAct() {
		return idPosicionesAct;
	}
	public void setIdPosicionesAct(Long idPosicionesAct) {
		this.idPosicionesAct = idPosicionesAct;
	}
	public DispositivoDTO getIdDispositivo() {
		return idDispositivo;
	}
	public void setIdDispositivo(DispositivoDTO idDispositivo) {
		this.idDispositivo = idDispositivo;
	}

	public Long getIdEvento() {
		return idEvento;
	}
	public void setIdEvento(Long idEvento) {
		this.idEvento = idEvento;
	}
	public Double getNuLatitud() {
		return nuLatitud;
	}
	public void setNuLatitud(Double nuLatitud) {
		this.nuLatitud = nuLatitud;
	}
	public Double getNuLongitud() {
		return nuLongitud;
	}
	public void setNuLongitud(Double nuLongitud) {
		this.nuLongitud = nuLongitud;
	}
	public Double getNuAltitud() {
		return nuAltitud;
	}
	public void setNuAltitud(Double nuAltitud) {
		this.nuAltitud = nuAltitud;
	}
//	public Point getTheGeom() {
//		return theGeom;
//	}
//	public void setTheGeom(Point theGeom) {
//		this.theGeom = theGeom;
//	}
	public Long getNuImei() {
		return nuImei;
	}
	public void setNuImei(Long nuImei) {
		this.nuImei = nuImei;
	}
	public String getNuIp() {
		return nuIp;
	}
	public void setNuIp(String nuIp) {
		this.nuIp = nuIp;
	}
	public String getTxDireccion() {
		return txDireccion;
	}
	public void setTxDireccion(String txDireccion) {
		this.txDireccion = txDireccion;
	}
	public Date getFhRegistro() {
		return fhRegistro;
	}
	public void setFhRegistro(Date fhRegistro) {
		this.fhRegistro = fhRegistro;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
	@Override
	public String toString() {
		return "PosicionesActDTO [idPosicionesAct=" + idPosicionesAct + ", idDispositivo=" + idDispositivo
				+ ", nuLatitud=" + nuLatitud + ", nuLongitud=" + nuLongitud + ", nuAltitud=" + nuAltitud + ", nuIp="
				+ nuIp + ", txDireccion=" + txDireccion + ", stActivo=" + stActivo + ", fhCreacion=" + fhCreacion
				+ ", idUsrCreacion=" + idUsrCreacion + ", fhModificacion=" + fhModificacion + ", idUsrModifica="
				+ idUsrModifica + "]";
	}
}
