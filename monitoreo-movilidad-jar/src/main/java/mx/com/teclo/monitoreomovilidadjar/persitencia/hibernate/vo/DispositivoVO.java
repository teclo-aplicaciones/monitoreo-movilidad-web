package mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.vo;

import java.util.List;

public class DispositivoVO {

	private Long id;
	private List<Double[]> coords;
	private InfoEmpleadoVO info;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<Double[]> getCoords() {
		return coords;
	}
	public void setCoords(List<Double[]> coords) {
		this.coords = coords;
	}
	public InfoEmpleadoVO getInfo() {
		return info;
	}
	public void setInfo(InfoEmpleadoVO info) {
		this.info = info;
	}
}
