package mx.com.teclo.monitoreomovilidadjar.service.logs;

import java.util.List;

import mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.dto.LogGPSDTO;

public interface LogService {
	public List<LogGPSDTO> getLogGPSs();
	public void saveLog(String tx);
}
