package mx.com.teclo.monitoreomovilidadjar.service.coordenadas;

import mx.com.teclo.monitoreomovilidadjar.persitencia.hibernate.vo.PosicionesActVO;

public interface CoordenadaService {

	public PosicionesActVO agregarCoordenada(PosicionesActVO posicionesActVO);

}
