package mx.com.teclo.monitoreomovilidadjar.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;


/* Test

910;Location;907510186;552;20190529;02:05:12;07d2232e44;+19.321760;-099.242544;000.306;000.00;1;28462;100;0;0;1400;02;334;03;-204;9006;255;10;100

*/

@SpringBootTest
public class TCPClientTest {

	private final static String ADDRESS = null;
	private final static int PORT = 5011;

	@Test
	public void TCPTest() {
		Socket socket = null;
		DataInputStream input = null;
		DataOutputStream out = null;

		// establish a connection
		try {
			socket = new Socket(ADDRESS,PORT);
			System.out.println("Connected");

			// takes input from terminal
			input = new DataInputStream(System.in);

			// sends output to the socket
			out = new DataOutputStream(socket.getOutputStream());
		} catch (UnknownHostException u) {
			System.out.println(u);

		} catch (IOException i) {
			System.out.println(i);
		}

		// string to read message from input
		String line = "";

		// keep reading until "Over" is input
		while (!line.equals("Over")) {
			try {
				line = input.readLine();
				out.writeUTF(line);
			} catch (IOException i) {
				System.out.println(i);
			}
		}

		// close the connection
		try {

			input.close();
			out.close();
			socket.close();
		} catch (IOException i) {
			System.out.println(i);
		}
	}
}
