package mx.com.teclo.monitoreomovilidadjar.protocol;

import java.util.logging.Logger;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import mx.com.teclo.monitoreomovilidadjar.protocol.model.Position;

@SpringBootTest
public class SuntechProtocolDecoderTest {
	Logger log = Logger.getLogger(getClass().getName());

	@Test
	public void positionTest() throws Exception {
		SuntechProtocolDecoder decoder = new SuntechProtocolDecoder();
		String msg = "910;Location;907510186;552;20190529;02:05:12;07d2232e44;+19.321760;-099.242544;000.306;000.00;1;28462;100;0;0;1400;02;334;03;-204;9006;255;10;100";		
		Position position = (Position)decoder.decode(msg);
		
		log.info("Latitude: "+position.getLatitude());
		log.info("Longitud: "+position.getLongitude());
	}

}
