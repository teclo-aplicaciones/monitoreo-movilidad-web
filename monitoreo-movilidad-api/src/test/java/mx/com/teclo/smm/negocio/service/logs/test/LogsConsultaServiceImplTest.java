package mx.com.teclo.smm.negocio.service.logs.test;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import mx.com.teclo.MonitoreoMovilidadApiApplication;
import mx.com.teclo.smm.negocio.service.logs.LogsConsultaService;
import mx.com.teclo.smm.persistencia.vo.logs.ComboVO;
import mx.com.teclo.smm.persistencia.vo.logs.LogsBusquedaPorIdVO;
import mx.com.teclo.smm.persistencia.vo.logs.LogsBusquedaVO;
import mx.com.teclo.smm.persistencia.vo.logs.LogsConsultaComboVO;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { MonitoreoMovilidadApiApplication.class })
public class LogsConsultaServiceImplTest {
	@Autowired
	private LogsConsultaService logsConsultaService;

	@Ignore
	// @Test
	public void findLogById() {
		LogsBusquedaPorIdVO logVO = null;
		logVO = logsConsultaService.busquedaLogPorId(1L);
		System.out.println(logVO);
	}

//	@Test
	@Ignore
	public void busquedaLogsTodos() {
		List<LogsBusquedaVO> logsBusquedaVO = null;
		logsBusquedaVO = logsConsultaService.busquedaLogsTodos();
		System.out.println(logsBusquedaVO);

	}

	// @Test
	@Ignore
	public void busquedaLogsPorPerfil() {
		List<LogsConsultaComboVO> logsConsultaComboVO = null;
		logsConsultaComboVO = logsConsultaService.busquedaLogsPorPerfil(9001L);

		System.out.println(logsConsultaComboVO);

	}

//	@Test
	@Ignore
	public void perfilesAsignadosPorLog() {
		List<ComboVO> listacomboVO = null;
		listacomboVO = logsConsultaService.perfilesAsignadosPorLog(1L);

		System.out.println(listacomboVO);

	}

	@Test
	// @Ignore
	public void perfilesNoAsignadosPorLog() {
		List<ComboVO> listacomboVO = null;
		listacomboVO = logsConsultaService.perfilesNoAsignadosPorLog(1L);

		System.out.println(listacomboVO);

	}
}
