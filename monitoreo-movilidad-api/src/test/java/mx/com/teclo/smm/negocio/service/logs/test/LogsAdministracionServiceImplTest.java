package mx.com.teclo.smm.negocio.service.logs.test;

import java.text.ParseException;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import mx.com.teclo.MonitoreoMovilidadApiApplication;
import mx.com.teclo.smm.negocio.service.logs.LogsAdministracionService;
import mx.com.teclo.smm.persistencia.vo.logs.LogsVO;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { MonitoreoMovilidadApiApplication.class })
public class LogsAdministracionServiceImplTest {
	@Autowired
	private LogsAdministracionService logsAdministracionService;

	@Ignore
	// @Test
	public void cambioDeEstatus() {
		logsAdministracionService.cambioDeEstatus(1L, "A");
	}

	@Ignore
//	@Test
	public void agregarPerfilALog() {
		logsAdministracionService.agregarPerfilALog(1L, 9004L);
	}
	@Ignore
//	@Test
	public void eliminarPerfilALog() {
		logsAdministracionService.eliminarPerfilALog(1L, 9004L);
	}
	@Test
 	public void crudLog() {
		LogsVO log = new LogsVO();
		log.setPerfilId(9001L);
		log.setLogId(1L);
		log.setLogId(3L);
		log.setLogNombre("LOG GENERADO WAasdasdasdadS 90");
		log.setLogDescripcion("CONTIENE LOS ARCHIVOS LOGS DEL WAS90 2");
		log.setLogTipoArchivos("log|txt");
		log.setRutaArchivo("/opt/IBM/WebSphere/AppServer/profiles/AppSrv01/logs/server1/");
	  
		String operacion ="EP";
		
		try {
			logsAdministracionService.crudLog( log, operacion);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	} 

}
