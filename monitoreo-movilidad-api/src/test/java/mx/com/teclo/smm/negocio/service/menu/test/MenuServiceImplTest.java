package mx.com.teclo.smm.negocio.service.menu.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import mx.com.teclo.MonitoreoMovilidadApiApplication;
import mx.com.teclo.smm.negocio.service.menu.MenuService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { MonitoreoMovilidadApiApplication.class })
public class MenuServiceImplTest {

	@Autowired
	MenuService menuService;

	@Test
	// @Ignore
	public void findMenusByProfile() {
//		menuService.findMenusByProfile("ADMIN", "MM");
	}

}
