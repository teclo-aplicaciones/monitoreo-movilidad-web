package mx.com.teclo.smm.persistencia.hibernate.dto.mantenimiento;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispositivoDTO;

@Entity
@Table(name = "TMM019D_CT_MANTENIMIENTOS")
public class MantenimeintoDTO  implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator="id_mantenimeinto_generator",strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="id_mantenimeinto_generator", sequenceName = "SQTMM019D_CT_MANTENIMI",allocationSize=1)
	@Column(name="ID_MANTENIMIENTO")
	private Long idMantenimiento;
	
	@Column(name="ID_EMPRESA")
	private long idEmpresa;
	
	@Column(name="NB_MANTENIMIENTO")
	private String nbMantenimiento;
	
	@ManyToOne
	@JoinColumn(name="ID_TIPO_MANTENIMIENTO")
	private TipoMantenimeintoDTO tipoMantenimeinto;
	
	@Column(name="FH_INGRESO")
	private Date fhIngreso;
	@Column(name="FH_ENTREGA")
	private Date fhEntrega;
	@Column(name="FH_ENTREGA_ESTIMADA")
	private Date fhEntregaEstimada;
	
	
	@ManyToOne
	@JoinColumn(name="ID_ST_MANTENIMIENTO")
	private StatusMantenimeintoDTO statusMantenimeinto;	
	
	@ManyToOne
	@JoinColumn(name="ID_DISPOSITIVO")
	private DispositivoDTO idDispositivo;
	
	@Column(name="TX_MANTENIMIENTO")
	private String txMantenimiento;
	
	@Column(name="TX_OBSERVACIONES")
	private String txObservaciones;
	
	@Column(name="ST_ACTIVO")
	private Boolean stActivo;
	@Column(name="FH_CREACION")
	private Date fhCreacion;
	@Column(name="ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name="FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name="ID_USR_MODIFICA")
	private Long idUsrModifica;
	
	
	
	public StatusMantenimeintoDTO getStatusMantenimeinto() {
		return statusMantenimeinto;
	}
	public void setStatusMantenimeinto(StatusMantenimeintoDTO statusMantenimeinto) {
		this.statusMantenimeinto = statusMantenimeinto;
	}
	public Date getFhEntregaEstimada() {
		return fhEntregaEstimada;
	}
	public void setFhEntregaEstimada(Date fhEntregaEstimada) {
		this.fhEntregaEstimada = fhEntregaEstimada;
	}
	public String getTxObservaciones() {
		return txObservaciones;
	}
	public void setTxObservaciones(String txObservaciones) {
		this.txObservaciones = txObservaciones;
	}
	public Long getIdMantenimiento() {
		return idMantenimiento;
	}
	public void setIdMantenimiento(Long idMantenimiento) {
		this.idMantenimiento = idMantenimiento;
	}
	public String getNbMantenimiento() {
		return nbMantenimiento;
	}
	public void setNbMantenimiento(String nbMantenimiento) {
		this.nbMantenimiento = nbMantenimiento;
	}
	public TipoMantenimeintoDTO getTipoMantenimeinto() {
		return tipoMantenimeinto;
	}
	public void setTipoMantenimeinto(TipoMantenimeintoDTO tipoMantenimeinto) {
		this.tipoMantenimeinto = tipoMantenimeinto;
	}
	public Date getFhIngreso() {
		return fhIngreso;
	}
	public void setFhIngreso(Date fhIngreso) {
		this.fhIngreso = fhIngreso;
	}
	public Date getFhEntrega() {
		return fhEntrega;
	}
	public void setFhEntrega(Date fhEntrega) {
		this.fhEntrega = fhEntrega;
	}
	public DispositivoDTO getIdDispositivo() {
		return idDispositivo;
	}
	public void setIdDispositivo(DispositivoDTO idDispositivo) {
		this.idDispositivo = idDispositivo;
	}

	public String getTxMantenimiento() {
		return txMantenimiento;
	}
	public void setTxMantenimiento(String txMantenimiento) {
		this.txMantenimiento = txMantenimiento;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
	public long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
}