package mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispositivoDTO;

@SuppressWarnings("unchecked")
@Repository("dispositivoDAO")
public class DispositivoDAOImpl extends BaseDAOImpl<DispositivoDTO> implements DispositivoDAO{

	@Override
	public void guardarDispositivo(DispositivoDTO dispDTO)
	{
		save(dispDTO);
	}

	@Override
	public List<DispositivoDTO> buscarDispositivos()
	{
		Criteria query = getCurrentSession().createCriteria(DispositivoDTO.class);
		//idEempresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		return query.list();
	}

	@Override
	public DispositivoDTO buscarDispositivosPorId(Long id)
	{
		Criteria query = getCurrentSession().createCriteria(DispositivoDTO.class);
		query.add(Restrictions.eq("idDispositivo", id));
		//idEempresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		return (DispositivoDTO) query.uniqueResult();
	}

	@Override
	public List<DispositivoDTO> buscarDispositivosPorId(Long[] id)
	{
		Criteria query = getCurrentSession().createCriteria(DispositivoDTO.class);
		Disjunction multi = Restrictions.disjunction();
		if(id != null){
			for(int i = 0;i<id.length;i++){
				multi.add(Restrictions.eq("idDispositivo", Long.parseLong(id[i].toString())));
			}
			//idEempresa
			//multi.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
			query.add(multi);
		}
		return query.list();
	}

	@Override
	public Boolean crearTablasDispositivo(Long numId) {

		String numCeros = numId < 10 ? "00" + numId : numId < 100 ? "0" + numId : "" + numId ;

		String posicion = "CREATE TABLE TMM_DP_DISP_#_POSICION(" +
				"    ID_DISP_POSICION NUMBER(11,0) NOT NULL ENABLE," +
				"	ID_DISPOSITIVO NUMBER(11,0) NOT NULL ENABLE," +
				"	NU_LONGITUD NUMBER(11,6) NOT NULL ENABLE," +
				"	NU_LATITUD NUMBER(11,6) NOT NULL ENABLE," +
				"	NU_ALTITUD NUMBER(11,6) NOT NULL ENABLE," +
				"	NU_ACCURACY NUMBER(11,6) NOT NULL ENABLE," +
				"	THE_GEOM SDO_GEOMETRY," +
				"	NU_IMEI NUMBER(20,0) NOT NULL ENABLE," +
				"	NU_IP VARCHAR2(15 BYTE) NOT NULL ENABLE," +
				"	TX_DIRECCION VARCHAR2(300 BYTE) NOT NULL ENABLE," +
				"	ID_EVENTO NUMBER(11,0) NOT NULL ENABLE," +
				"	FH_REGISTRO DATE NOT NULL ENABLE," +
				"	ST_ACTIVO NUMBER(1,0) NOT NULL ENABLE," +
				"	FH_CREACION DATE NOT NULL ENABLE," +
				"	ID_USR_CREACION NUMBER(11,0) NOT NULL ENABLE," +
				"	FH_MODIFICACION DATE NOT NULL ENABLE," +
				"	ID_USR_MODIFICA NUMBER(11,0) NOT NULL ENABLE," +
				"	CONSTRAINT PKMM$POS_DP_DISP PRIMARY KEY (ID_DISP_POSICION)," +
				"	CONSTRAINT RMM01200# FOREIGN KEY (ID_EVENTO)" +
				"	REFERENCES TMM012C_CT_EVENTOS (ID_EVENTO)ENABLE" +
				"   )";
		SQLQuery createPosicion = getSession().createSQLQuery(posicion.replace("$", numCeros).replace("#", numId.toString()));
		int resPosicion = createPosicion.executeUpdate();


//		String evento = "CREATE TABLE TMM_DP_DISP_#_EVENTO(" +
//				"	ID_DISP_EVENTO NUMBER(11,0) NOT NULL ENABLE," +
//				"	ID_DISPOSITIVO NUMBER(11,0) NOT NULL ENABLE," +
//				"	ID_EVENTO NUMBER(11,0) NOT NULL ENABLE," +
//				"	ST_ACTIVO NUMBER(11,0) NOT NULL ENABLE," +
//				"	FH_CREACION DATE NOT NULL ENABLE," +
//				"	ID_USR_CREACION NUMBER(11,0) NOT NULL ENABLE," +
//				"	FH_MODIFICACION DATE NOT NULL ENABLE," +
//				"	ID_USR_MODIFICA NUMBER(11,0) NOT NULL ENABLE," +
//				"	CONSTRAINT PKMM$EVEN_DP_DISP PRIMARY KEY (ID_DISP_EVENTO)," +
//				"   CONSTRAINT RMM012$ FOREIGN KEY (ID_EVENTO)" +
//				"	REFERENCES TMM012C_CT_EVENTOS (ID_EVENTO) ENABLE" +
//				"   )";
//		SQLQuery createEvento = getSession().createSQLQuery(evento.replace("$", numCeros).replace("#", numId.toString()));
//		int resEvento = createEvento.executeUpdate();

//		String comando = "CREATE TABLE TMM_DP_DISP_#_COMANDO(" +
//				"    ID_DISP_COMPANDO NUMBER(11,0) NOT NULL ENABLE," +
//				"	ID_DISPOSITIVO NUMBER(11,0) NOT NULL ENABLE," +
//				"	ID_COMANDO NUMBER(11,0)," +
//				"	ST_ACTIVO NUMBER(1,0) NOT NULL ENABLE," +
//				"	FH_CREACION DATE NOT NULL ENABLE," +
//				"	ID_USR_CREACION NUMBER(11,0) NOT NULL ENABLE," +
//				"	FH_MODIFICACION DATE NOT NULL ENABLE," +
//				"	ID_USR_MODIFCA NUMBER(11,0) NOT NULL ENABLE," +
//				"	CONSTRAINT PKMM$COM_DP_DISP PRIMARY KEY (ID_DISP_COMPANDO), " +
//				"	CONSTRAINT RMM025$ FOREIGN KEY (ID_COMANDO)" +
//				"	REFERENCES TMM025D_CT_COMANDOS (ID_COMANDO) ENABLE" +
//				"   )";
//		SQLQuery createComando = getSession().createSQLQuery(comando.replace("$", numCeros).replace("#", numId.toString()));
//		int resComando = createComando.executeUpdate();

		if(resPosicion != -1)
			return true;
		return false;

	}

	@Override
	public List<DispositivoDTO> buscarDispositivosPorTipo(String tipo, String valor) {

		Query query = null;

		if(valor == null || valor.isEmpty()) {
			query = getCurrentSession().createQuery("from DispositivoDTO where idEmpresa = :idEmpresa");
			query.setParameter("idEmpresa",getIdEmpresa());
		}else {
			switch (tipo) {
			// Nombre
			case "1":
					query = getCurrentSession().createQuery("from DispositivoDTO"
							+ " where lower(nbDispositivo) like lower(:valor) and idEmpresa = :idEmpresa");
					query.setParameter("valor", "%"+valor+"%");
					query.setParameter("idEmpresa",getIdEmpresa());
				break;

			// IMEI
			case "2":
					query = getCurrentSession().createQuery("from DispositivoDTO"
							+ " where lower(cdDispositivo) like lower(:valor) and idEmpresa = :idEmpresa");
					query.setParameter("valor", "%"+valor+"%");
					query.setParameter("idEmpresa",getIdEmpresa());
				break;

			// Marca
			case "3":
					query = getCurrentSession().createQuery("from DispositivoDTO"
							+ " where lower(idModelo.idMarca.nbMarca) like lower(:valor) and idEmpresa = :idEmpresa");
					query.setParameter("valor", "%"+valor+"%");
					query.setParameter("idEmpresa",getIdEmpresa());
				break;

			// Modelo
			case "4":
					query = getCurrentSession().createQuery("from DispositivoDTO"
							+ " where lower(idModelo.nbModelo) like lower(:valor) and idEmpresa = :idEmpresa");
					query.setParameter("valor", "%"+valor+"%");
					query.setParameter("idEmpresa",getIdEmpresa());
				break;
			}
		}

		List<DispositivoDTO> dispositivoDTOs=query.list();
		return dispositivoDTOs;
	}

	@Override
	public List<DispositivoDTO> buscarDispositivoPorNombre(String nombre){
		Criteria query = getCurrentSession().createCriteria(DispositivoDTO.class);
		nombre = nombre.replaceAll("%", "");
		query.add(Restrictions.eq("nbDispositivo", nombre).ignoreCase());
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		//idEmpresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));

		return query.list();
	}

	@Override
	public void dropTableNbTableDispPosici(String table) {
		/**
		  CREATE OR REPLACE PROCEDURE ICD.droptable (tablename IN VARCHAR2) IS
			BEGIN
			 	BEGIN
				   EXECUTE IMMEDIATE 'DROP TABLE ' || tablename;
				EXCEPTION
				   WHEN OTHERS THEN
				      IF SQLCODE != -942 THEN
				         RAISE;
				      END IF;
				END;
		  END;
		 */
		getSession().createSQLQuery("CALL ICD.DROPTABLE('"+table+"')");
		/*
		try {
			getSession().createSQLQuery("select * from " + table ).executeUpdate();
			getSession().createSQLQuery("drop table " + table ).executeUpdate();
		}catch (SQLGrammarException e) {
			e.printStackTrace();
        }*/
	}

	@Override
	public void dropTableNbTableDispEvento(String table) {
		getSession().createSQLQuery("CALL ICD.DROPTABLE('"+table+"')");
		/*
		try {
			getSession().createSQLQuery("select * from " + table ).executeUpdate();
			getSession().createSQLQuery("drop table " + table ).executeUpdate();
		}catch (SQLGrammarException e) {
			e.printStackTrace();
        }*/
	}

	@Override
	public void dropTableNbTableDispComand(String table) {
		getSession().createSQLQuery("CALL ICD.DROPTABLE('"+table+"')");
		/*
		try {
			getSession().createSQLQuery("select * from " + table ).executeUpdate();
			getSession().createSQLQuery("drop table " + table ).executeUpdate();
		}catch (SQLGrammarException e) {
			e.printStackTrace();
        }*/
	}
}
