package mx.com.teclo.smm.persistencia.hibernate.dao.comandos;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.MedioSubtipoCmdDTO;

public interface MedioSubtipoCmdDAO extends BaseDAO<MedioSubtipoCmdDTO>{
	public MedioSubtipoCmdDTO obtenerMedioSubtipoCmd(Long id);
	public MedioSubtipoCmdDTO obtenerMedioSubtipoCmdPorIdMedioIdSubtipoDispositivo(Long idMedio, Long idSubtipoDispositivo);	
}
