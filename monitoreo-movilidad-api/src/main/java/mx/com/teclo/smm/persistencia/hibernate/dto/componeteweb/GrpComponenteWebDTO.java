package mx.com.teclo.smm.persistencia.hibernate.dto.componeteweb;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TAQ054C_SE_GRP_COMPONENTE_WEB")
public class GrpComponenteWebDTO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 4722758760270195303L;


	@Id
	@GeneratedValue(generator="id_grpcomponenteweb_generator",strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="id_grpcomponenteweb_generator", sequenceName = "SQTAQ054C_SE_GRPCOMPWEB",allocationSize=1)
	@Column(name = "ID_GRP_COMPONENTE_WEB")
	private Long idGrpComponenteWeb;
	@Column(name = "NB_GRP_COMPONENTE_WEB")
	private String nbGrpComponenteWeb;
	@Column(name = "ST_ACTIVO")
	private Boolean stActivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;

	public Long getIdGrpComponenteWeb() {
		return idGrpComponenteWeb;
	}
	public void setIdGrpComponenteWeb(Long idGrpComponenteWeb) {
		this.idGrpComponenteWeb = idGrpComponenteWeb;
	}
	public String getNbGrpComponenteWeb() {
		return nbGrpComponenteWeb;
	}
	public void setNbGrpComponenteWeb(String nbGrpComponenteWeb) {
		this.nbGrpComponenteWeb = nbGrpComponenteWeb;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
}
