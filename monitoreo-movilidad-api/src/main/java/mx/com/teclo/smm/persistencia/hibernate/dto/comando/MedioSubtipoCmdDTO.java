package mx.com.teclo.smm.persistencia.hibernate.dto.comando;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.SubTipoDispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.MedioComunicacionDTO;

@Entity
@Table(name = "TMM061C_CT_MEDIO_SUBTIPO_CMD")
public class MedioSubtipoCmdDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5420785184526540266L;


	@Id
	@GeneratedValue(generator="id_mediosubtipocmd_generator",strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="id_mediosubtipocmd_generator", sequenceName = "SQTMM061C_CT_MED_SUB_CMD",allocationSize=1)
	@Column(name = "ID_MEDIO_SUBTIPO_COMANDO")
	private Long idMedioSubtipoComando;
	@Column(name="ID_EMPRESA")
	private long idEmpresa;
	@ManyToOne
	@JoinColumn(name = "ID_MEDIO")
	private MedioComunicacionDTO idMedioComunicacionDTO;
	@ManyToOne
	@JoinColumn(name = "ID_SUBTIPO_DISPOSITIVO")
	private SubTipoDispositivoDTO idSubTipoDispositivoDTO;
	@Column(name = "ST_ACTIVO")
	private Boolean stActivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;
	public Long getIdMedioSubtipoComando() {
		return idMedioSubtipoComando;
	}
	public void setIdMedioSubtipoComando(Long idMedioSubtipoComando) {
		this.idMedioSubtipoComando = idMedioSubtipoComando;
	}
	
	public MedioComunicacionDTO getIdMedioComunicacionDTO() {
		return idMedioComunicacionDTO;
	}
	public void setIdMedioComunicacionDTO(MedioComunicacionDTO idMedioComunicacionDTO) {
		this.idMedioComunicacionDTO = idMedioComunicacionDTO;
	}
	public SubTipoDispositivoDTO getIdSubTipoDispositivoDTO() {
		return idSubTipoDispositivoDTO;
	}
	public void setIdSubTipoDispositivoDTO(SubTipoDispositivoDTO idSubTipoDispositivoDTO) {
		this.idSubTipoDispositivoDTO = idSubTipoDispositivoDTO;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
	public long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	
	
}
