package mx.com.teclo.smm.persistencia.hibernate.dao.comandos;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.BitacoraComandoDetalleArchivoDTO;


public interface BitacoraComandoDetalleArchivoDAO extends BaseDAO<BitacoraComandoDetalleArchivoDTO>{

	public BitacoraComandoDetalleArchivoDTO obtenerArchivo(Long idBitacora);
	public BitacoraComandoDetalleArchivoDTO obtenerArchivoPorIdArchivo(Long idArchivo);
}
