package mx.com.teclo.smm.util.enumerados;

public enum TipoBusquedaRuta {
	RUTAS(0L), DIAS(1L);
	
	private Long tipoBusquedaRuta;
	
	private TipoBusquedaRuta(Long tipoBusquedaRuta)
	{
		this.tipoBusquedaRuta=tipoBusquedaRuta;
	}

	public Long getTipoBusquedaRuta() {
		return tipoBusquedaRuta;
	}

	public void setTipoBusquedaRuta(Long tipoBusquedaRuta) {
		this.tipoBusquedaRuta = tipoBusquedaRuta;
	}
}
