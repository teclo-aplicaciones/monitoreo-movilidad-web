package mx.com.teclo.smm.negocio.service.mantenimiento;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.dto.mantenimiento.BusqMantenimeintoDTO;
import mx.com.teclo.smm.persistencia.vo.mantenimiento.MantenimientoVO;

public interface MantenimientoService {
	public List<MantenimientoVO> consultarMantenimiento(String tipo, String valor);
	public MantenimientoVO buscarMantenimientoPorId(Long id);
	public List<MantenimientoVO> listaDispositivos();
	public List<BusqMantenimeintoDTO> busqMantenimiento();
	public List<MantenimientoVO> tipoMantenimiento();
	public boolean agregaMantenimiento(MantenimientoVO mantenimientoVO);
	public boolean actualizaMantenimiento(MantenimientoVO mantenimientoVO);
	public boolean eliminarMantenimiento(Long idMantenimientoVO);
	public boolean finalizarMantenimiento(Long idMantenimiento,String Observaciones);
	public List<List<MantenimientoVO>> timelineMantenimiento(Long idMantenimientoVO,String idUsuarioOIMEI,int tipoTimeline);
}
