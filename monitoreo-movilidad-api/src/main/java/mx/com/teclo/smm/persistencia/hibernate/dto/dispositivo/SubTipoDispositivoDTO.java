package mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TMM058D_DP_SUBTIPO_DIPOSITIVOS")
public class SubTipoDispositivoDTO implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 907335068136992748L;

	@Id
	@Column(name = "ID_SUBTIPO_DISPOSITIVO", nullable = false, unique = true)
	private Long idSubTipoDispositivo;
	@Column(name="ID_EMPRESA")
	private long idEmpresa;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_TIPO_DISPOSITIVO")
	private TipoDispositivoDTO idTipoDispositivo;
	@Column(name = "CD_SUBTIPO_DISPOSITIVO")
	private String cdSubTipoDispositivo;
	@Column(name = "NB_SUBTIPO_DISPOSITIVO")
	private String nbSubTipoDispositivo;
	@Column(name = "TX_SUBTIPO_DISPOSITIVO")
	private String txSubTipoDispositivo;
	@Column(name = "ST_ACTIVO")
	private Boolean stActivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;


	public Long getIdSubTipoDispositivo() {
		return idSubTipoDispositivo;
	}
	public void setIdSubTipoDispositivo(Long idSubTipoDispositivo) {
		this.idSubTipoDispositivo = idSubTipoDispositivo;
	}
	public TipoDispositivoDTO getIdTipoDispositivo() {
		return idTipoDispositivo;
	}
	public void setIdTipoDispositivo(TipoDispositivoDTO idTipoDispositivo) {
		this.idTipoDispositivo = idTipoDispositivo;
	}
	public String getCdSubTipoDispositivo() {
		return cdSubTipoDispositivo;
	}
	public void setCdSubTipoDispositivo(String cdSubTipoDispositivo) {
		this.cdSubTipoDispositivo = cdSubTipoDispositivo;
	}
	public String getNbSubTipoDispositivo() {
		return nbSubTipoDispositivo;
	}
	public void setNbSubTipoDispositivo(String nbSubTipoDispositivo) {
		this.nbSubTipoDispositivo = nbSubTipoDispositivo;
	}
	public String getTxSubTipoDispositivo() {
		return txSubTipoDispositivo;
	}
	public void setTxSubTipoDispositivo(String txSubTipoDispositivo) {
		this.txSubTipoDispositivo = txSubTipoDispositivo;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
	public long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
}
