package mx.com.teclo.smm.persistencia.hibernate.dao.mantenimiento;

import java.util.List;

import javax.persistence.OrderBy;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.mantenimiento.MantenimeintoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.mantenimiento.TipoMantenimeintoDTO;

@SuppressWarnings("unchecked")
@Repository("MantenimientoDAO")
public class MantenimientoDAOImpl extends BaseDAOImpl<MantenimeintoDTO> implements MantenimientoDAO{

	@Override
	@Transactional(readOnly = true)
	public List<MantenimeintoDTO> obtenerMantenimeinto(){	
		Criteria query = getCurrentSession().createCriteria(MantenimeintoDTO.class);
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		query.addOrder(Order.desc("statusMantenimeinto.idStatusMantenimiento"));
		
		//idEempresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		
		return (List<MantenimeintoDTO>)query.list();
	}
	
	
	@Override
	@Transactional(readOnly = true)
	public List<MantenimeintoDTO> obtenerMantenimeintoPorTipoMantenimiento(TipoMantenimeintoDTO idTipoMantenimiento){	
		Criteria query = getCurrentSession().createCriteria(MantenimeintoDTO.class);
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		query.add(Restrictions.eq("tipoMantenimeinto", idTipoMantenimiento));
		
		//idEempresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		
		return (List<MantenimeintoDTO>)query.list();
	}
	
	@Override	
	@Transactional(readOnly = true)
	@OrderBy("statusMantenimeinto.idStatusMantenimiento DESC")
	public List<MantenimeintoDTO> obtenerMantenimeintoLikeNombre(String valor){		
		Query query = null;
		query = getCurrentSession().createQuery("from MantenimeintoDTO"							
				+ " where lower(nbMantenimiento) like lower(:valor) and stActivo = 1 and idEmpresa = :idEmpresa");
		query.setParameter("valor", "%"+valor+"%");			
		
		//idEempresa
		query.setParameter("idEmpresa", getIdEmpresa());		
		
		return (List<MantenimeintoDTO>)query.list();
	}
	
	@Override
	@Transactional
	public MantenimeintoDTO obtenerMantenimeintoPorId(Long idMantenimientoVO){
		Criteria query = getCurrentSession().createCriteria(MantenimeintoDTO.class);
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		query.add(Restrictions.eq("idMantenimiento",idMantenimientoVO));
		return (MantenimeintoDTO)query.uniqueResult();
	}
	

	@Override
	@Transactional
	public List<MantenimeintoDTO> obtenerMantenimeintoPorIdDispositivo(DispositivoDTO dispositivoDTO){
		Criteria query = getCurrentSession().createCriteria(MantenimeintoDTO.class);
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		query.add(Restrictions.eq("idDispositivo", dispositivoDTO));
		
		//idEempresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));

		
		query.addOrder(Order.desc("statusMantenimeinto.idStatusMantenimiento"));
		return (List<MantenimeintoDTO>)query.list();
	}
	
}
