package mx.com.teclo.smm.persistencia.vo.evento;

public class TCDispositivosVO {
	
	private Long tcIdDispo;
	private String tcNombre;
	private Long tcConteo;
	
	
	public Long getTcIdDispo() {
		return tcIdDispo;
	}
	public void setTcIdDispo(Long tcIdDispo) {
		this.tcIdDispo = tcIdDispo;
	}
	public String getTcNombre() {
		return tcNombre;
	}
	public void setTcNombre(String tcNombre) {
		this.tcNombre = tcNombre;
	}
	public Long getTcConteo() {
		return tcConteo;
	}
	public void setTcConteo(Long tcConteo) {
		this.tcConteo = tcConteo;
	}
}
