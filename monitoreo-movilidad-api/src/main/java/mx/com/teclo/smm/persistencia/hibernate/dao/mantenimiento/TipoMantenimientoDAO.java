package mx.com.teclo.smm.persistencia.hibernate.dao.mantenimiento;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.catalogo.CatalogoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.TipoEventoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.mantenimiento.TipoMantenimeintoDTO;


public interface TipoMantenimientoDAO extends BaseDAO<TipoMantenimeintoDTO>{
	public List<TipoMantenimeintoDTO> obtenerTipoMantenimeinto();	
	public TipoMantenimeintoDTO obtenerTipoMantenimeintoPorId(Long id);
}
