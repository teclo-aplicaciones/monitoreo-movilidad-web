package mx.com.teclo.smm.persistencia.vo.notificacion;

public class DetalleNotificacionVO {
	private String usuario;
	private String dispositivo;
	private Boolean estatus;
	private Long fechaEnvio;
	private Long fechaRecepcion;
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getDispositivo() {
		return dispositivo;
	}
	public void setDispositivo(String dispositivo) {
		this.dispositivo = dispositivo;
	}
	public Boolean getEstatus() {
		return estatus;
	}
	public void setEstatus(Boolean estatus) {
		this.estatus = estatus;
	}
	public Long getFechaEnvio() {
		return fechaEnvio;
	}
	public void setFechaEnvio(Long fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}
	public Long getFechaRecepcion() {
		return fechaRecepcion;
	}
	public void setFechaRecepcion(Long fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}
}
