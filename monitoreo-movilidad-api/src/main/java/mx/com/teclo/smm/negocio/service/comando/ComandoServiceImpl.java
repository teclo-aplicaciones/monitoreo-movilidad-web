package mx.com.teclo.smm.negocio.service.comando;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.notificacionespush.cliente.firebase.ClienteFirebaseService;
import mx.com.teclo.notificacionespush.message.firebase.Client;
import mx.com.teclo.notificacionespush.message.firebase.Push;
import mx.com.teclo.notificacionespush.message.firebase.ResponseFirebaseMessage;
import mx.com.teclo.smm.negocio.service.empresa.EmpresaService;
import mx.com.teclo.smm.persistencia.hibernate.dao.comandos.BitacoraComandoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.comandos.BitacoraComandoDetalleArchivoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.comandos.BitacoraComandoDetalleDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.comandos.BitacoraComandoStatusDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.comandos.ComandoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.comandos.DispositivoComandoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.comandos.TipoComandoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo.DispositivoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.notificacion.MedioComunicacionDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.BitacoraComandoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.BitacoraComandoDetalleArchivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.BitacoraComandoDetalleDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.BitacoraStatusDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.ComandoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.DispositivoComandoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.vo.comando.ComandoVO;
import mx.com.teclo.smm.persistencia.vo.comando.LogComandoVO;
import mx.com.teclo.smm.util.enumerados.ComandoBitacoraStatus;
import mx.com.teclo.smm.util.enumerados.ComandoEnum;
import mx.com.teclo.smm.util.enumerados.TemplatePushEnum;

@Service
public class ComandoServiceImpl implements ComandoService{

	@Autowired
	ComandoDAO comandoDAO;
	@Autowired
	DispositivoComandoDAO dispositivoComandoDAO;
	@Autowired
	DispositivoDAO dispositivoDAO;
	@Autowired
	BitacoraComandoDAO bitacoraComandoDAO;
	@Autowired
	BitacoraComandoDetalleDAO BitacoraComandoDetalleDAO;
	@Autowired
	TipoComandoDAO tipoComandoDAO;
	@Autowired
	MedioComunicacionDAO medioComunicacionDAO;
	@Autowired
	BitacoraComandoStatusDAO bitacoraComandoStatusDAO;
	@Autowired
	BitacoraComandoDetalleArchivoDAO bitacoraComandoDetalleArchivoDAO;
	@Autowired
	EmpresaService empresaService;

	@Value("${firebase.apikey}")
	private String API_KEY;

	@Autowired
	ClienteFirebaseService clienteFirebaseService;

	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");

	@Override
	public List<ComandoVO> obtenerComandosDispositivo(Long idDispositivo){
		List<ComandoVO> comandoVO = new ArrayList<>();
		for(DispositivoComandoDTO d : dispositivoComandoDAO.obtenerComandoPorDispositivo(idDispositivo)) {
			ComandoVO c = new ComandoVO();
			c.setId(d.getIdComando().getIdComando());
			c.setActivo(d.getStActivo());
			comandoVO.add(c);
		}

		return comandoVO;
	}

	@Override
	@Transactional(readOnly=true)
	public List<ComandoVO> obtenerCatalogoComandos(Long idDispositivo,Long idTipoMedioComunicacion) {
		List<ComandoVO> comandoVO = new ArrayList<>();
		DispositivoDTO dispositivoDTO = dispositivoDAO.buscarDispositivosPorId(idDispositivo);

		for(ComandoDTO c : comandoDAO.obtenerComandosPorTipo(dispositivoDTO.getIdModelo().getIdSubtipoDispositivo().getIdSubTipoDispositivo(),idTipoMedioComunicacion)) {
			ComandoVO e = new ComandoVO();
			e.setId(c.getIdComando());
			e.setNombre(c.getCdComando());
			comandoVO.add(e);
		}

		return comandoVO;
	}

	@Override
	@Transactional
	public Boolean insertarOactualizarComando(Long idDispositivo,Long idComando,Boolean stActivo) {

		DispositivoComandoDTO dispositivoComandoDTO = dispositivoComandoDAO.obtenerComandoPorDispositivoYComando(idDispositivo, idComando);

		if(dispositivoComandoDTO == null) {
			//crear
			DispositivoComandoDTO d = new DispositivoComandoDTO();
			d.setIdDispositivo(dispositivoDAO.buscarDispositivosPorId(idDispositivo));
			d.setIdComando(comandoDAO.obtenerComando(idComando));
			d.setStActivo(stActivo);
			d.setFhCreacion(new Date());
			d.setFhModificacion(new Date());
			d.setIdUsrCreacion(1L);
			d.setIdUsrModifica(1L);
			dispositivoComandoDAO.save(d);
		}else {
			// actualizar
			dispositivoComandoDTO.setStActivo(stActivo);
			dispositivoComandoDTO.setFhModificacion(new Date());
			dispositivoComandoDAO.save(dispositivoComandoDTO);
		}

		return true;
	}


	@Override
	public List<LogComandoVO> obtenerLogComando(Long idDispositivo){
		List<LogComandoVO> logComandoVOs = new ArrayList<>();


		for(BitacoraComandoDTO l : bitacoraComandoDAO.obtenerLogComando(idDispositivo)) {


			LogComandoVO logComandoVO = new LogComandoVO();
			logComandoVO.setIdBitacora(l.getIdBitacora());
			logComandoVO.setDispositivo(l.getIdDispositivoDTO().getNbDispositivo());
			logComandoVO.setComando(l.getIdComandoDTO().getCdComando());
			logComandoVO.setTipoComando(l.getIdTipoComando().getTxTipoComando());
			logComandoVO.setTipoComunicacion(l.getIdComandoDTO().getIdMedioSubtipoCmdDTO().getIdMedioComunicacionDTO().getNbMedio());
			logComandoVO.setFecha(" "+sdf.format(l.getFhCreacion()));

			// Guardar archivo si existe
			if(
				l.getIdComandoDTO().getCdComando().equals(ComandoEnum.CAPTURA_PANTALLA.getNombre())
				|| l.getIdComandoDTO().getCdComando().equals(ComandoEnum.TOMAR_FOTO.getNombre())
				|| l.getIdComandoDTO().getCdComando().equals(ComandoEnum.TOMAR_FOTO_DELANTERA.getNombre())
				|| l.getIdComandoDTO().getCdComando().equals(ComandoEnum.GRABAR_AUDIO.getNombre())
			) {
				BitacoraComandoDetalleArchivoDTO bitacoraComandoDetalleArchivoDTO = bitacoraComandoDetalleArchivoDAO.obtenerArchivo(l.getIdBitacora());
				if(bitacoraComandoDetalleArchivoDTO != null) {
					logComandoVO.setIdArchivo(bitacoraComandoDetalleArchivoDTO.getIdArchivo());
					logComandoVO.setArchivo(bitacoraComandoDetalleArchivoDTO.getTxFormatoArchivo());
				}
			}

			// Guardar GPS_ACTUAL
			if(l.getIdComandoDTO().getCdComando().equals(ComandoEnum.GPS_ACTUAL.getNombre())) {
				for (BitacoraComandoDetalleDTO b : BitacoraComandoDetalleDAO.obtenerBitacoraComandoDetalle(l.getIdBitacora())){
					if(b.getIdBitacoraStatus() == 3) {
						logComandoVO.setGpsActual(b.getTxBitacora());
					}
				}
			}

			logComandoVOs.add(logComandoVO);
		};
		return logComandoVOs;
	}

	@Override
	public List<LogComandoVO> obtenerLogComandoDetalle(Long idBitacora){
		List<LogComandoVO> logComandoVOs = new ArrayList<>();
		for (BitacoraComandoDetalleDTO b : BitacoraComandoDetalleDAO.obtenerBitacoraComandoDetalle(idBitacora)){
			BitacoraStatusDTO bitacoraStatusDTO = bitacoraComandoStatusDAO.obtenerBitacoraStatus(b.getIdBitacoraStatus());

			LogComandoVO l = new LogComandoVO();
			l.setLogs(b.getTxBitacora());
			l.setFecha(" "+sdf.format(b.getFhCreacion()));
			l.setStatus(bitacoraStatusDTO.getNbStatus());
			l.setColor(bitacoraStatusDTO.getTxColor());
			logComandoVOs.add(l);
		}

		return logComandoVOs;

	}

	@SuppressWarnings("finally")
	@Override
	@Transactional
	public BitacoraComandoDTO executeComando(Long idDispositivo,Long idComando) {

		BitacoraComandoDTO l = new BitacoraComandoDTO();
		ComandoDTO comandoDTO = comandoDAO.obtenerComando(idComando);
		l.setIdDispositivoDTO(dispositivoDAO.buscarDispositivosPorId(idDispositivo));
		l.setIdComandoDTO(comandoDTO);
		l.setIdTipoComando(comandoDTO.getIdTipoComando());
		l.setIdMedioComunicacionDTO(comandoDTO.getIdMedioSubtipoCmdDTO().getIdMedioComunicacionDTO());
		l.setFhCreacion(new Date());
		l.setFhModificacion(new Date());
		l.setIdUsrCreacion(1L);
		l.setIdUsrModifica(1L);
		l.setIdEmpresa(empresaService.getIdEmpresa());

		bitacoraComandoDAO.save(l);

		return l;
	}

	@SuppressWarnings("finally")
	@Override
	@Transactional
	public Boolean executeComandoDetalle(BitacoraComandoDTO bitacoraComandoDTO,Long idDispositivo,Long idComando) {


		String cdCommando = comandoDAO.obtenerComando(idComando).getCdComando();
		String fbDispositivo  =  dispositivoDAO.buscarDispositivosPorId(idDispositivo).getFbDispositivo();
		Client cliente = new Client(fbDispositivo);
		Push push = TemplatePushEnum.MENSAJE.getPush();
		//Agregamos los Clientes(Como solo hay 1 no es necesario la lista)
		push.setListDevice(null);
		//Agregamos la API KEY
		push.setApiKey(API_KEY);


		insertarBitacoraComandoDetalle(bitacoraComandoDTO,"Envio de comando "+ cdCommando,ComandoBitacoraStatus.ENVIO);

		/**
		 * ALERTA
		 */
		if(cdCommando.equals(ComandoEnum.ALERTA_ON.getNombre())) {
			push.getData().setAction(ComandoEnum.ALERTA_ON.getAction());
			push.getData().setBody(""+bitacoraComandoDTO.getIdBitacora());
		}
		else if(cdCommando.equals(ComandoEnum.ALERTA_OFF.getNombre())) {
			push.getData().setAction(ComandoEnum.ALERTA_OFF.getAction());
			push.getData().setBody(""+bitacoraComandoDTO.getIdBitacora());
		}
		/**
		 * WIFI
		 */
		else if(cdCommando.equals(ComandoEnum.WIFI_ON.getNombre())) {
			push.getData().setAction(ComandoEnum.WIFI_ON.getAction());
			push.getData().setBody(""+bitacoraComandoDTO.getIdBitacora());

		}else if(cdCommando.equals( ComandoEnum.WIFI_OFF.getNombre())) {
			push.getData().setAction(ComandoEnum.WIFI_OFF.getAction());
			push.getData().setBody(""+bitacoraComandoDTO.getIdBitacora());
		}
		/**
		 * GPS
		 */
		else if(cdCommando.equals( ComandoEnum.GPS_ACTUAL.getNombre())) {
			// data
			push.getData().setAction(ComandoEnum.GPS_ACTUAL.getAction());
			push.getData().setBody(""+bitacoraComandoDTO.getIdBitacora());
		}
		/**
		 * VIBRAR
		 */
		else if(cdCommando.equals( ComandoEnum.VIBRAR.getNombre())) {
			// data
			push.getData().setAction(ComandoEnum.VIBRAR.getAction());
			push.getData().setBody(""+bitacoraComandoDTO.getIdBitacora());
		}
		/**
		 * CAPTURA_PANTALLA
		 */
		else if(cdCommando.equals( ComandoEnum.CAPTURA_PANTALLA.getNombre())) {
			// data
			push.getData().setAction(ComandoEnum.CAPTURA_PANTALLA.getAction());
			push.getData().setBody(""+bitacoraComandoDTO.getIdBitacora());
		}
		/**
		 * TOMAR_FOTO_DELANTERA
		 */
		else if(cdCommando.equals( ComandoEnum.TOMAR_FOTO_DELANTERA.getNombre())) {
			// data
			push.getData().setAction(ComandoEnum.TOMAR_FOTO_DELANTERA.getAction());
			push.getData().setBody(""+bitacoraComandoDTO.getIdBitacora());
		}
		/**
		 * TOMAR_FOTO
		 */
		else if(cdCommando.equals( ComandoEnum.TOMAR_FOTO.getNombre())) {
			// data
			push.getData().setAction(ComandoEnum.TOMAR_FOTO.getAction());
			push.getData().setBody(""+bitacoraComandoDTO.getIdBitacora());
		}
		/**
		 * TOMAR_VIDEO
		 */
		else if(cdCommando.equals( ComandoEnum.TOMAR_VIDEO.getNombre())) {
			// data
			push.getData().setAction(ComandoEnum.TOMAR_VIDEO.getAction());
			push.getData().setBody(""+bitacoraComandoDTO.getIdBitacora());
		}

		/**
		 * TOMAR_VIDEO_DELANTERA
		 */
		else if(cdCommando.equals( ComandoEnum.TOMAR_VIDEO_DELANTERA.getNombre())) {
			// data
			push.getData().setAction(ComandoEnum.TOMAR_VIDEO_DELANTERA.getAction());
			push.getData().setBody(""+bitacoraComandoDTO.getIdBitacora());
		}

		/**
		 * GRABAR_AUDIO
		 */
		else if(cdCommando.equals( ComandoEnum.GRABAR_AUDIO.getNombre())) {
			// data
			push.getData().setAction(ComandoEnum.GRABAR_AUDIO.getAction());
			push.getData().setBody(""+bitacoraComandoDTO.getIdBitacora());
		}


		ResponseFirebaseMessage rfbm = null;
		try{
			rfbm = clienteFirebaseService.FBMNotificationClient(cliente, push, push.getApiKey());
			insertarBitacoraComandoDetalle(bitacoraComandoDTO,"Pendiente por enviar comando "+ cdCommando ,ComandoBitacoraStatus.PENDIENTE);
		}catch(Exception e){
			e.printStackTrace();
			insertarBitacoraComandoDetalle(bitacoraComandoDTO,"Error de comando"+ cdCommando+ " " +e.getMessage(),ComandoBitacoraStatus.ERROR);
		}finally{
			if(rfbm != null ) {
				if(rfbm.getSuccess().equals("1") ) {
					insertarBitacoraComandoDetalle(bitacoraComandoDTO,"Proceso de comando "+ cdCommando+" en Firebase",ComandoBitacoraStatus.PROCESO);
					return Boolean.TRUE;
				}
			}
			return Boolean.FALSE;
		}
	}

	public void insertarBitacoraComandoDetalle(BitacoraComandoDTO bitacoraComandoDTO,String txDetalle,ComandoBitacoraStatus comandoBitacoraStatus) {
		BitacoraComandoDetalleDTO b = new BitacoraComandoDetalleDTO();
		b.setIdBitacora(bitacoraComandoDTO.getIdBitacora());
		b.setTxBitacora(txDetalle);
		b.setIdBitacoraStatus(comandoBitacoraStatus.getIdStatus());
		b.setFhCreacion(new Date());
		b.setFhModificacion(new Date());
		b.setIdUsrCreacion(1L);
		b.setIdUsrModifica(1L);
		BitacoraComandoDetalleDAO.save(b);
	}

	@Override
	public BitacoraComandoDetalleArchivoDTO obtenerArchivo(Long idArchivo) {
		return bitacoraComandoDetalleArchivoDAO.obtenerArchivoPorIdArchivo(idArchivo);
	}
}
