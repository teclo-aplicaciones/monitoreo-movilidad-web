package mx.com.teclo.smm.negocio.service.notificacion;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import mx.com.teclo.arquitectura.ortogonales.exception.NotFoundException;
import mx.com.teclo.arquitectura.ortogonales.service.comun.UsuarioFirmadoService;
import mx.com.teclo.smm.negocio.service.empresa.EmpresaService;
import mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo.DispoEmpleadoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo.DispositivoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo.InformacionDispDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.empleado.EmpleadoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.grupo.GrupoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.notificacion.MedioComunicacionDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.notificacion.MedioNotificacionDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.notificacion.NotificaDispositivoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.notificacion.NotificacionDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.notificacion.TipoNotificacionDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispoEmpleadoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.empleado.EmpleadoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.geocerca.GeocercaDispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.grupo.GrupoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.MedioComunicacionDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.MedioNotificacionDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.NotificaDispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.NotificacionDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.TipoNotificacionDTO;
import mx.com.teclo.smm.persistencia.vo.JsonObjectVO;
import mx.com.teclo.smm.persistencia.vo.geocerca.DispoGeocercaVO;
import mx.com.teclo.smm.persistencia.vo.notificacion.ConsultaNotificacionVO;
import mx.com.teclo.smm.persistencia.vo.notificacion.DetalleNotificacionVO;
import mx.com.teclo.smm.persistencia.vo.notificacion.NotificacionVO;
import mx.com.teclo.smm.util.enumerados.CatalogoNotificacionEnum;
import mx.com.teclo.smm.util.enumerados.TipoNotificacionEnum;

@Service
public class NotificacionServiceImpl implements NotificacionService{
	
	@Autowired
	GrupoDAO grupoDAO;
	
	@Autowired
	EmpleadoDAO empleadoDAO;
	
	@Autowired
	DispositivoDAO dispositivoDAO;
	
	@Autowired
	DispoEmpleadoDAO dispoEmpleadoDAO;
	
	@Autowired
	NotificacionDAO notificacionDAO;
	
	@Autowired
	NotificaDispositivoDAO notificaDispositivoDAO;
	
	@Autowired
	TipoNotificacionDAO tipoNotificacionDAO;
	
	@Autowired
	NotificacionPushService notificacionPushService;
	
	@Autowired
	NotificacionCorreoService notificacionCorreoService;
	
	@Autowired
	NotificacionSMSService notificacionSMSService;
	
	@Autowired
	InformacionDispDAO informacionDispDAO;
	
	@Autowired
	UsuarioFirmadoService usuarioFirmadoService;
	
	@Autowired
	MedioNotificacionDAO medioNotificacionDAO;
	
	@Autowired
	MedioComunicacionDAO medioComunicacionDAO; 
	
	@Autowired
	EmpresaService empresaService;
	
	@Override
	@Transactional
	public List<ConsultaNotificacionVO>consultaNotificaciones(Long tipoBusq, String valor,  Long medio, String fInicial, String fFinal)throws NotFoundException, ParseException{
		
		List<ConsultaNotificacionVO> listaNotificacion = new ArrayList<ConsultaNotificacionVO>();
		List<NotificacionDTO> listaNDTO = new ArrayList<NotificacionDTO>();
		DispositivoDTO dDTO = null;
		EmpleadoDTO eDTO = null;
		NotificacionDTO nDTO = null;
		
		MedioNotificacionDTO mnDTO = medio != -1 ? medioNotificacionDAO.buscarMedioPorId(medio) : null;
		
		TipoNotificacionDTO tnDTO = tipoNotificacionDAO.buscarTipoNotificacionPorId(1L);
		
		List<NotificaDispositivoDTO> listaNdDTO = null;
		List<DispoEmpleadoDTO> listaDeDTO = null;
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		if(tipoBusq == CatalogoNotificacionEnum.NOMBRE_NOTIFICACION.getIdCat()){
			listaNDTO = notificacionDAO.buscarNotificacionPorNombre(valor, tnDTO, mnDTO);
			
			if(listaNDTO.isEmpty())
				throw new NotFoundException("No se encontraron resultados.");
		}else if (tipoBusq == CatalogoNotificacionEnum.DISPOSITIVO.getIdCat()){
			dDTO = dispositivoDAO.buscarDispositivosPorId(Long.parseLong(valor));
			listaNdDTO = notificaDispositivoDAO.buscarTodoDispositivosNotificacion(dDTO, tnDTO,mnDTO);
			
			if(listaNdDTO.isEmpty())
				throw new NotFoundException("No existen notificaciones relacionadas con el dispositivo.");
			
			for(NotificaDispositivoDTO ndDTO: listaNdDTO){
				nDTO = ndDTO.getIdNotificacion();
				listaNDTO.add(nDTO);
			}
		}else if (tipoBusq == CatalogoNotificacionEnum.USUARIOS.getIdCat()){
			eDTO = empleadoDAO.buscarEmpleado(Long.parseLong(valor));
			listaDeDTO = dispoEmpleadoDAO.buscarRelacionDispoEmpleadoPorEmpleado(eDTO);
			if(listaDeDTO.isEmpty())
				throw new NotFoundException("No existe dispositivo(s) relacionados con el usuario.");
		
			for(DispoEmpleadoDTO edDTO : listaDeDTO){
				dDTO = edDTO.getIdDispositivo();
				listaNdDTO = notificaDispositivoDAO.buscarTodoDispositivosNotificacion(dDTO, tnDTO, mnDTO);
				for(NotificaDispositivoDTO ndDTO : listaNdDTO){
					nDTO = ndDTO.getIdNotificacion();
					listaNDTO.add(nDTO);
				}
			}
			if(listaNDTO.isEmpty())
				throw new NotFoundException("No existen notificaciones relacionadas con el usuario.");
		}
		
		Date dInicial = !fInicial.trim().equals("") ? sdf.parse(fInicial) : null;
		Date dFinal = !fFinal.trim().equals("") ? sdf.parse(fFinal) : null;
		
		for(NotificacionDTO nIterableDTO : listaNDTO){
			
			ConsultaNotificacionVO nVO = new ConsultaNotificacionVO();
			nVO.setId(nIterableDTO.getIdNotificacion());
			nVO.setNombre(nIterableDTO.getNbNotificacion());
			nVO.setActivo(nIterableDTO.getStActivo());
			nVO.setEnviado(nIterableDTO.getStEnviado());
			nVO.setTipo(nIterableDTO.getIdMedioTipoNotifi().getIdMedioTipoNotifi());
			nVO.setFecha(nIterableDTO.getFhEnvio().getTime());
			nVO.setFechaCreacion(nIterableDTO.getFhCreacion().getTime());
			nVO.setMensaje(nIterableDTO.getTxMensaje());
			if(!fInicial.trim().equals("")){
				if(!fFinal.trim().equals("")){
					if(nIterableDTO.getFhEnvio().after(dInicial) && nIterableDTO.getFhEnvio().before(dFinal))
						listaNotificacion.add(nVO);
				}else{
					if(nIterableDTO.getFhEnvio().after(dInicial) && nIterableDTO.getFhEnvio().before(new Date()))
						listaNotificacion.add(nVO);
				}
			}else{
				if(!fFinal.trim().equals("")){
					Calendar c = Calendar.getInstance();
					c.add(Calendar.DATE, 365);
					if(nIterableDTO.getFhEnvio().after(c.getTime()) && nIterableDTO.getFhEnvio().before(dFinal))
						listaNotificacion.add(nVO);
				}else{
					listaNotificacion.add(nVO);
				}
			}
			
		}
		
		/*ObjectMapper objectMapper = new ObjectMapper();
		List<NotificacionVO> notificaciones = new ArrayList<NotificacionVO>();
		
		try {
			listaObject = objectMapper.readValue(JSON, new TypeReference<List<JsonObjectVO>>(){});
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(JsonObjectVO object: listaObject)
		{
			NotificacionVO notiVo = new NotificacionVO();
			notiVo.setId(Long.parseLong(object.getId()));
			notiVo.setNombre(object.getNombre());
			notiVo.setFecha("23/11/2018");
			notiVo.setGrupo(notiVo.getId()%2==0?null:1L);
			notiVo.setTipo(notiVo.getId()%2==0?1L:2L);
			notiVo.setMensaje("Ejemplo de Mensaje");
			notificaciones.add(notiVo);
		}
		
		return notificaciones;*/
		
		return listaNotificacion;
	}
	
	@Override
	@Transactional(readOnly=true)
	public List<DetalleNotificacionVO>consultaDetalleNotifica(Long id){
		DetalleNotificacionVO nDVO = null;
		List<DetalleNotificacionVO> listaNDVO = new ArrayList<DetalleNotificacionVO>();
		NotificacionDTO nDTO = notificacionDAO.buscarNotificacionPorId(id);
		
		List<NotificaDispositivoDTO> listaNDDTO = notificaDispositivoDAO.buscarTodoDispositivosEnNotificacion(nDTO);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy H:mm");
		for(NotificaDispositivoDTO nDDTO: listaNDDTO){
			nDVO = new DetalleNotificacionVO();
			DispoEmpleadoDTO dEDTO = dispoEmpleadoDAO.buscarRelacionDispoEmpleadoPorDispositivo(nDDTO.getIdDispositivo()); 
			nDVO.setUsuario(dEDTO != null ? dEDTO.getIdEmpleado().getNbEmpleado()+" "+dEDTO.getIdEmpleado().getNbPaterno()+" "+dEDTO.getIdEmpleado().getNbMaterno():"Sin asignar");
			nDVO.setDispositivo(nDDTO.getIdDispositivo().getNbDispositivo());
			nDVO.setEstatus(nDDTO.getStActivo());
			nDVO.setFechaEnvio(nDDTO.getFhEnvio() != null ? nDDTO.getFhEnvio().getTime():0L);
			nDVO.setFechaRecepcion(nDDTO.getFhRecepcion() != null ? nDDTO.getFhRecepcion().getTime():0L);
			listaNDVO.add(nDVO);
		}
		
		return listaNDVO;
	}
	
	@Override
	@Transactional
	public NotificacionVO buscarNotificacionPorActualizar(Long id)throws NotFoundException{
		
		NotificacionVO nVO = null;
		NotificacionDTO nDTO = notificacionDAO.buscarNotificacionPorId(id);
		
		List<DispoGeocercaVO> listaDGVO = new ArrayList<DispoGeocercaVO>();
		
		List<NotificaDispositivoDTO> listaNDDTO = notificaDispositivoDAO.buscarDispositivosEnNotificacion(nDTO);
		
		if(nDTO != null){
			nVO = new NotificacionVO();
			nVO.setId(nDTO.getIdNotificacion());
			nVO.setNombre(nDTO.getNbNotificacion());
			nVO.setMensaje(nDTO.getTxMensaje());
			nVO.setIsNow(Boolean.FALSE);
			nVO.setTipo(nDTO.getIdMedioTipoNotifi().getIdMedioTipoNotifi());
			nVO.setFecha(new SimpleDateFormat("dd/MM/YYYY H:mm").format(nDTO.getFhEnvio()));
			nVO.setGrupo(validaNotificacionGrupoRelacion(nDTO));
			
			for(NotificaDispositivoDTO nDDTO: listaNDDTO)
			{
				DispoGeocercaVO dgVO = new DispoGeocercaVO();
				dgVO.setIdItem(nDDTO.getIdDispositivo().getIdDispositivo());
				dgVO.setSrItem(informacionDispDAO.buscarRelacionPorDispositivo(nDDTO.getIdDispositivo()).getNuSerie());
				dgVO.setTxtItem(nDDTO.getIdDispositivo().getNbDispositivo());
				listaDGVO.add(dgVO);
			}
			
			nVO.setDispositivos(listaDGVO);
		}else{
			throw new NotFoundException("No se encontro la notificación.");
		}
		/*List<NotificacionVO>notificaciones = consultaNotificaciones(null, null);
		NotificacionVO notif = null;
		
		for(NotificacionVO n : notificaciones){
			for(int i :ids){
				if(n.getId() == i){
					notif = n;
				}
			}
		}
		return notif;*/
		return nVO;
	}
	
	@Override
	@Transactional
	public NotificacionDTO guardarNotificacion(NotificacionVO notificacionVO) throws ParseException{
		NotificacionDTO nDTO = new NotificacionDTO();
		NotificaDispositivoDTO ndDTO = null;
		
		//Buscar id Siguiente
		Long idNotificacion = notificacionDAO.buscarSiguienteIdentificador();
		//Id de Usuario
		Long idUsuario = usuarioFirmadoService.getUsuarioFirmadoVO().getId();
		//Fecha Creacion
		Date fechaCreacion = new Date();
		
		//valida si dispositivos asignados pertenecen a un grupo
		
		GrupoDTO grupo = notificacionVO.getGrupo() != 0 ? grupoDAO.buscarGrupoPorId(notificacionVO.getGrupo()) : null;
		nDTO.setIdNotificacion(idNotificacion);
		//nDTO.setIdTipoNotificacion(tipoNotificacionDAO.buscarTipoNotificacionPorId(notificacionVO.getTipo()));
		nDTO.setIdMedioTipoNotifi(medioNotificacionDAO.buscarMedioPorId(notificacionVO.getTipo()));
		nDTO.setNbNotificacion(notificacionVO.getNombre());
		nDTO.setTxMensaje(notificacionVO.getMensaje());
		nDTO.setFhEnvio(notificacionVO.getFecha() != null ? new SimpleDateFormat("dd/MM/yyyy H:mm").parse(notificacionVO.getFecha()):new Date());
		nDTO.setStActivo(Boolean.TRUE);
		nDTO.setStEnviado(Boolean.FALSE);
		nDTO.setFhCreacion(fechaCreacion);
		nDTO.setIdUsrCreacion(idUsuario);
		nDTO.setFhModificacion(fechaCreacion);
		nDTO.setIdUsrModifica(idUsuario);
		nDTO.setIdEmpresa(empresaService.getIdEmpresa());
		
		notificacionDAO.save(nDTO);
		
		//Se agregan los dispositivos asociados
		for(DispoGeocercaVO dgVO: notificacionVO.getDispositivos()){
			ndDTO = new NotificaDispositivoDTO();
			
			ndDTO.setIdDispNotif(notificaDispositivoDAO.buscarSiguienteIdentificador());//Consultar para siguiente valor
			ndDTO.setIdDispositivo(dispositivoDAO.buscarDispositivosPorId(dgVO.getIdItem()));
			ndDTO.setIdGrupo(grupo != null ? grupo : null);
			ndDTO.setIdNotificacion(notificacionDAO.buscarNotificacionPorId(idNotificacion));
			nDTO.setStActivo(Boolean.TRUE);
			ndDTO.setFhCreacion(fechaCreacion);
			ndDTO.setIdUsrCreacion(idUsuario);
			
			if(notificacionVO.getIsNow()){
				ndDTO.setFhEnvio(new Date());
				if(TipoNotificacionEnum.PUSH.getIdCat() == notificacionVO.getTipo()){
					//Funcion para mandar push
					ndDTO.setStActivo(notificacionPushService.enviaNotificacionPushInmediata(ndDTO.getIdDispositivo(), nDTO, notificacionVO.getBase64()));
				}else if(TipoNotificacionEnum.MAIL.getIdCat() == notificacionVO.getTipo()){
					//Funcion para mandar correo
					ndDTO.setStActivo(notificacionCorreoService.enviaNotificacionMailVelocityInmediata(dispoEmpleadoDAO.buscarRelacionDispoEmpleadoPorDispositivo(ndDTO.getIdDispositivo()).getIdEmpleado().getNbMail() , nDTO));
					//ndDTO.setStActivo(notificacionCorreoService.enviaNotificacionMailInmediata(dispoEmpleadoDAO.buscarRelacionDispoEmpleadoPorDispositivo(ndDTO.getIdDispositivo()).getIdEmpleado().getNbMail() , nDTO));
				}else if(TipoNotificacionEnum.SMS.getIdCat() == notificacionVO.getTipo()){
					//Funcion para mandar mensaje de Texto
					ndDTO.setStActivo(notificacionSMSService.enviaNotificacionSMSInmediata(informacionDispDAO.buscarRelacionPorDispositivo(ndDTO.getIdDispositivo()).getNuCelularEmpresa() , nDTO));
				}
				//ndDTO.setFhEnvio(ndDTO.getStActivo() ? null : new Date());
				ndDTO.setFhRecepcion(ndDTO.getStActivo() ? null : new Date());
			}else{
				ndDTO.setFhEnvio(null);
				ndDTO.setFhRecepcion(null);
				ndDTO.setStActivo(Boolean.TRUE);
			}
			
			ndDTO.setFhModificacion(fechaCreacion);
			ndDTO.setIdUsrModifica(idUsuario);
			
			notificaDispositivoDAO.save(ndDTO);
		}
		
		//Validamos si todos los mensajes se enviaron, de no ser asi, se modifica la notificacion...
		if(notificacionVO.getIsNow()){
			Boolean isPending = notificaDispositivoDAO.validaPushEnviados(nDTO);
			nDTO.setStEnviado(isPending);
			notificacionDAO.update(nDTO);
		}
		
		
		return notificacionDAO.buscarNotificacionPorId(idNotificacion);
	}
	
	@Override
	@Transactional
	public Boolean actualizaNotificacion(NotificacionVO notificacionVO) throws ParseException{
		NotificacionDTO nDTO = notificacionDAO.buscarNotificacionPorId(notificacionVO.getId());
		
		NotificaDispositivoDTO ndDTO = null;
		
		//Id de Usuario
		Long idUsuario = usuarioFirmadoService.getUsuarioFirmadoVO().getId();
		//Fecha Creacion
		Date fechaModificacion = new Date();
		
		//valida si dispositivos asignados pertenecen a un grupo
		GrupoDTO grupo = notificacionVO.getGrupo() != 0 ? grupoDAO.buscarGrupoPorId(notificacionVO.getGrupo()) : null;
		
		//nDTO.setIdTipoNotificacion(tipoNotificacionDAO.buscarTipoNotificacionPorId(notificacionVO.getTipo()));
		nDTO.setIdMedioTipoNotifi(medioNotificacionDAO.buscarMedioPorId(notificacionVO.getTipo()));
		nDTO.setNbNotificacion(notificacionVO.getNombre());
		nDTO.setTxMensaje(notificacionVO.getMensaje());
		nDTO.setFhEnvio(notificacionVO.getFecha() != null ? new SimpleDateFormat("dd/MM/yyyy H:mm").parse(notificacionVO.getFecha()):new Date());
		//nDTO.setStActivo(Boolean.TRUE);
		//nDTO.setFhCreacion(fechaCreacion);
		//nDTO.setIdUsrCreacion(idUsuario);
		nDTO.setFhModificacion(fechaModificacion);
		nDTO.setIdUsrModifica(idUsuario);
		
		notificacionDAO.save(nDTO);
		
		//Limpiamos relaciones
		List<NotificaDispositivoDTO> listaRelacion = notificaDispositivoDAO.buscarDispositivosEnNotificacion(nDTO);
		
		for(NotificaDispositivoDTO ndDTOIterable: listaRelacion){
			notificaDispositivoDAO.delete(ndDTOIterable);
		}
		
		//Se modifican los dispositivos asociados
		for(DispoGeocercaVO dgVO: notificacionVO.getDispositivos()){
			ndDTO = new NotificaDispositivoDTO();
			
			ndDTO.setIdDispNotif(notificaDispositivoDAO.buscarSiguienteIdentificador());//Consultar para siguiente valor
			ndDTO.setIdDispositivo(dispositivoDAO.buscarDispositivosPorId(dgVO.getIdItem()));
			ndDTO.setIdGrupo(grupo != null ? grupo : null);
			//ndDTO.setIdNotificacion(nDTO);
			ndDTO.setIdNotificacion(notificacionDAO.buscarNotificacionPorId(notificacionVO.getId()));
			ndDTO.setFhCreacion(fechaModificacion);
			ndDTO.setIdUsrCreacion(idUsuario);
			ndDTO.setFhModificacion(fechaModificacion);
			ndDTO.setIdUsrModifica(idUsuario);
			
			if(notificacionVO.getIsNow()){
				if(TipoNotificacionEnum.PUSH.getIdCat() == notificacionVO.getTipo()){
					ndDTO.setStActivo(notificacionPushService.enviaNotificacionPushInmediata(ndDTO.getIdDispositivo(), nDTO, notificacionVO.getBase64()));
				}else if(TipoNotificacionEnum.MAIL.getIdCat() == notificacionVO.getTipo()){
					//Funcion para mandar correo
					ndDTO.setStActivo(Boolean.FALSE);
				}else if(TipoNotificacionEnum.SMS.getIdCat() == notificacionVO.getTipo()){
					//Funcion para mandar mensaje de Texto
					ndDTO.setStActivo(Boolean.FALSE);
				}
				ndDTO.setFhEnvio(ndDTO.getStActivo() ? null : new Date());
				ndDTO.setFhRecepcion(ndDTO.getStActivo() ? null : new Date());
			}else{
				ndDTO.setFhEnvio(null);
				ndDTO.setFhRecepcion(null);
				ndDTO.setStActivo(Boolean.TRUE);
			}
			
			notificaDispositivoDAO.save(ndDTO);
		}
		
		//Validamos si todos los mensajes se enviaron, de no ser asi, se modifica la notificacion...
		if(notificacionVO.getIsNow()){
			Boolean isPending = notificaDispositivoDAO.validaPushEnviados(nDTO);
			nDTO.setStEnviado(isPending);
			notificacionDAO.update(nDTO);
		}
		
		
		return Boolean.TRUE;
	}
	
	@Override
	@Transactional
	public Boolean eliminaNotificacion(Long id){
		NotificacionDTO nDTO = notificacionDAO.buscarNotificacionPorId(id);
		
		nDTO.setStActivo(Boolean.FALSE);
		
		return !nDTO.getStActivo();
	}
	
	private Long validaNotificacionGrupoRelacion(NotificacionDTO nDTO){
		List<NotificaDispositivoDTO> listaNdDTO = notificaDispositivoDAO.buscarDispositivosEnNotificacion(nDTO);
		if(!listaNdDTO.isEmpty()){
			for(NotificaDispositivoDTO ndDTO:listaNdDTO){
				if(ndDTO.getIdGrupo() != null)
					return ndDTO.getIdGrupo().getIdGrupo();
			}
		}
		return 0L;
	}
}
