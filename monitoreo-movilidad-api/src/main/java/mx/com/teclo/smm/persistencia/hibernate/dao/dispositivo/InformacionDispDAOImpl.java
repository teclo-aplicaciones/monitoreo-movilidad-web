package mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.arquitectura.persistencia.comun.dao.BaseDaoHibernate;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.InformacionDispDTO;

@Repository
public class InformacionDispDAOImpl extends BaseDaoHibernate<InformacionDispDTO> implements InformacionDispDAO {
	
	@Override
	public InformacionDispDTO buscarRelacionPorDispositivo(DispositivoDTO dispDTO){
		Criteria query = getCurrentSession().createCriteria(InformacionDispDTO.class);
		query.add(Restrictions.eq("idDispositivo", dispDTO));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		
		return (InformacionDispDTO) query.uniqueResult();
	} 
}
