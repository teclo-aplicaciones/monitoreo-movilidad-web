package mx.com.teclo.smm.negocio.service.notificacion;

import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.NotificacionDTO;

public interface NotificacionSMSService {
	public Boolean enviaNotificacionSMSInmediata(String nTelefonico, NotificacionDTO notificacion);
}
