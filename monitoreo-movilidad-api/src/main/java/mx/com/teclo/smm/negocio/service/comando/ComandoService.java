package mx.com.teclo.smm.negocio.service.comando;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.dto.comando.BitacoraComandoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.BitacoraComandoDetalleArchivoDTO;
import mx.com.teclo.smm.persistencia.vo.comando.ComandoVO;
import mx.com.teclo.smm.persistencia.vo.comando.LogComandoVO;

public interface ComandoService {
	public List<ComandoVO> obtenerComandosDispositivo(Long idDispositivo);	
	public Boolean insertarOactualizarComando(Long idDispositivo,Long idComando,Boolean stActivo);
	public List<ComandoVO> obtenerCatalogoComandos(Long idDispositivo,Long idTipoMedioComunicacion);
	
	public BitacoraComandoDTO executeComando(Long idDispositivo,Long idComando);
	public Boolean executeComandoDetalle(BitacoraComandoDTO bitacoraComandoDTO,Long idDispositivo,Long idComando);
	
	public List<LogComandoVO> obtenerLogComando(Long idDispositivo);	
	public List<LogComandoVO> obtenerLogComandoDetalle(Long idBitacora);
	
	public BitacoraComandoDetalleArchivoDTO obtenerArchivo(Long idArchivo);
}
