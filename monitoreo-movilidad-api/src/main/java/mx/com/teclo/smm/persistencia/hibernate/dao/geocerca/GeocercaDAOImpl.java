package mx.com.teclo.smm.persistencia.hibernate.dao.geocerca;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.geocerca.GeocercaDTO;

@SuppressWarnings("unchecked")
@Repository("geocercaDAO")
public class GeocercaDAOImpl extends BaseDAOImpl<GeocercaDTO> implements GeocercaDAO{
	
	@Override
	public List<GeocercaDTO> buscarGeocercas()
	{
		Criteria query = getCurrentSession().createCriteria(GeocercaDTO.class);
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		
		//idEempresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		
		query.addOrder(Order.asc("idTipoGeocercas"));
		
		return (List<GeocercaDTO>) query.list();
	}
	
	@Override
	public GeocercaDTO buscarGrupoPorId(Long id){
		Criteria query = getCurrentSession().createCriteria(GeocercaDTO.class);
		query.add(Restrictions.eq("idGeocerca", id));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		
		//idEempresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		
		return (GeocercaDTO) query.uniqueResult();
	}
	
	@Override
	public GeocercaDTO buscarGeocercaPorId(Long id){
		Criteria query = getCurrentSession().createCriteria(GeocercaDTO.class);
		query.add(Restrictions.eq("idGeocerca", id));
		
		//idEempresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
	
		//query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		
		return (GeocercaDTO) query.uniqueResult();
	}
	
	@Override
	public List<GeocercaDTO> buscarGeocercasPorNombre(String nombre){
		Criteria query = getCurrentSession().createCriteria(GeocercaDTO.class);
		nombre = nombre.replaceAll("%", "");
		query.add(Restrictions.like("nbGeocerca", nombre, MatchMode.ANYWHERE).ignoreCase());
		
		//idEempresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));

		
		//query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		
		return (List<GeocercaDTO>) query.list();
	}
}
