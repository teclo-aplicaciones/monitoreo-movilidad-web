package mx.com.teclo.smm.persistencia.hibernate.dao.catalogo;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.catalogo.CatalogoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.TipoEventoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.grupo.TipoGrupoDTO;


public interface TipoGruposDAO extends BaseDAO<TipoGrupoDTO>{
	public List<TipoGrupoDTO> obtenerTipoGrupos();
	public TipoGrupoDTO activarDesactivarObtenerTipoGrupos(Long id);
	public TipoGrupoDTO obtenerTipoGrupos(Long id);
	public List<TipoGrupoDTO> activarDesactivarObtenerTipoGrupos() ;
}
