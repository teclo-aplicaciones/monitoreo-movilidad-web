package mx.com.teclo.smm.persistencia.hibernate.dao.catalogo;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.catalogo.CatalogoDTO;

@SuppressWarnings("unchecked")
@Repository("catalogoDAO")
public class CatalogoDAOImpl extends BaseDAOImpl<CatalogoDTO> implements CatalogoDAO{

	@Override
	@Transactional(readOnly = true)
	public List<CatalogoDTO> obtenerCatalogos() {
		Criteria query = getCurrentSession().createCriteria(CatalogoDTO.class);
		query.addOrder(Order.asc("idCatalogo"));
		return query.list();
	}

	@Override
	public CatalogoDTO activarDesactivarobtenerCatalogo(long id){
		Criteria query = getCurrentSession().createCriteria(CatalogoDTO.class);
		query.add(Restrictions.eq("idCatalogo", id));
		query.addOrder(Order.desc("idCatalogo"));
		return (CatalogoDTO)query.uniqueResult();
	}

}
