package mx.com.teclo.smm.negocio.service.logs;

 
import java.io.File;
import java.util.List;

import mx.com.teclo.smm.persistencia.vo.logs.ComboVO;
import mx.com.teclo.smm.persistencia.vo.logs.LogsBusquedaPorIdVO;
import mx.com.teclo.smm.persistencia.vo.logs.LogsBusquedaVO;
import mx.com.teclo.smm.persistencia.vo.logs.LogsConsultaComboVO;
import mx.com.teclo.smm.persistencia.vo.logs.LogsVO;
 

/**
 *  Copyright (c) 2018, Teclo Mexicana. 
 * 
 *  Descripcion					: LogsConsultaService
 *  Historial de Modificaciones	: 
 *  Descripcion del Cambio 		: Modificación
 *  
 *  @author 					: fjmb
 *  @version 					: 1.0 
 *  Fecha 						: 01/Abril/2019
 */
public interface LogsConsultaService {

	/**
     * Consulta los <b>Logs</b> que se encuntran registrados en la base de datos según los criterios de búsqueda establecidos.
     * 
     * @return List<LogsBusquedaVO> Regresa una lista de objetos de de tipo <b>List<LogsBusquedaVO></b>.
     * 
     * @author 				        javier07, fmartinez 
     * @version 			        2.0
     * Fecha				        01/Abril/2019
     */
	public List<LogsBusquedaVO> busquedaLogsTodos();
	
	/**
     * Consulta los <b>Logs</b> por perfil.
     * 
     * @param perfilId 				Id de Perfil.
     * @return List<LogsConsultaComboVO> Regresa una lista de objetos de tipo <b>List<LogsConsultaComboVO></b>.
     * 
     * @author 				        javier07, fmartinez 
     * @version 			        2.0
     * Fecha				        01/Abril/2019
     */
	public List<LogsConsultaComboVO> busquedaLogsPorPerfil(Long perfilId);
	
	/**
     * Consulta los <b>Logs</b> por Identificador de log.
     * 
     * @param idLog 				Id de Log.
     * @return LogsBusquedaPorIdVO  Regresa un objeto de tipo <b>LogsBusquedaPorIdVO</b>.
     * 
     * @author 				        javier07, fmartinez 
     * @version 			        2.0
     * Fecha				        01/Abril/2019
     */
	public LogsBusquedaPorIdVO busquedaLogPorId(Long idLog);

	/**
     * Consulta los <b>Perfiles Asignados al Log</b> que se busca.
     * 
     * @param idLog 				Id de Log.
     * @return List<ComboVO>        Regresa una lista de objetos de tipo <b>List<ComboVO></b>.
     * 
     * @author 				        javier07, fmartinez 
     * @version 			        2.0
     * Fecha				        01/Abril/2019
     */	 
	public List<ComboVO> perfilesAsignadosPorLog(Long idLog);

	/**
     * Consulta los <b>Perfiles Asignados al Log</b> que se busca.
     * 
     * @param idLog 				Id de Log.
     * @return List<ComboVO> 		Regresa un objeto de tipo <b>List<ComboVO></b>.
     * 
     * @author 				        javier07, fmartinez 
     * @version 			        2.0
     * Fecha				        01/Abril/2019
     */
	public List<ComboVO> perfilesNoAsignadosPorLog(Long idLog);
	
	/**
     * Consulta la lista de archivos 
     * 
     * @param file 					Archivo log.
     * @param logInfo 				Informacion de Log.
     * @param logId 				Id de Log.
     * @return List<LogsVO> 		Regresa un objeto de tipo <b>List<LogsVO></b>.
     * 
     * @author 				        javier07, fmartinez 
     * @version 			        2.0
     * Fecha				        01/Abril/2019
     */
 	List<LogsVO> obtenerListaArchivosLogs(File file,LogsBusquedaPorIdVO logInfo,Long logId);

}
