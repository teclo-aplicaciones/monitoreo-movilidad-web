package mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.EventoDTO;
import mx.com.teclo.smm.persistencia.vo.catalogo.CatalogoVO;
import mx.com.teclo.smm.persistencia.vo.evento.EventoAnualVO;
import mx.com.teclo.smm.persistencia.vo.evento.EventosTablasHistoricoVO;

public interface EventoDAO extends BaseDAO<EventoDTO>{
	public List<EventoDTO> buscarCatalogoEventos();
	public List<EventosTablasHistoricoVO> consultarTiposEventos(Long idGrupo, String fecha, String dia);
	public List<EventosTablasHistoricoVO> consultarTiposEventosDispositivo(Long idDispositivo, String fecha);
	public List<EventosTablasHistoricoVO> consultarTiposEventosHoy(Long idGrupo, Long idDispositivo);
	public List<CatalogoVO>consultarTipoEventosDispoActuales();
	public List<EventoAnualVO> consultarEventosPorMesHistorico(String tabla, String anio);
	public Long consultarEventosDiarios(String tabla, Long idEvento);
	public Long consultarDispositivoDiario(String tabla);
	public Long consultarEventosDispositivoHistorico(String tabla, String criterio);
	public EventoDTO buscarCatalogoEventosId(long id);
	public EventoDTO activarDesactivarBuscarCatalogoEventosId(long id);
	public List<EventoDTO> buscarCatalogoEventosPorSubtipo(Long id);
	public Long consultarEventosHistorico(String tabla, Long idEvento, String criterio);
	public List<EventoDTO> activarDesactivarBuscarCatalogoEventos();
}
