package mx.com.teclo.smm.persistencia.hibernate.dao.geocerca;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dao.notificacion.NotificaDispositivoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.notificacion.NotificacionDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.geocerca.GeocercaDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.geocerca.GeocercaNotificacionDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.NotificaDispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.NotificacionDTO;

@SuppressWarnings("unchecked")
@Repository("geocercaNotificacionDAO")
public class GeocercaNotificacionDAOImpl extends BaseDAOImpl<GeocercaNotificacionDTO> implements GeocercaNotificacionDAO{

	@Autowired
	NotificacionDAO notificacionDAO;
	
	@Autowired
	NotificaDispositivoDAO notificaDispositivoDAO;
	
	@Override
	public Long buscarSiguienteIdentificador(){
		Criteria query = getCurrentSession().createCriteria(GeocercaNotificacionDTO.class);
		query.setProjection(Projections.max("idGeoNotifica"));
		Long res = (Long)query.uniqueResult();
		
		return res != null? res+1:1;
	}
	
	@Override
	public List<GeocercaNotificacionDTO> buscaRelacionGeocercaNotificacionPorGeocerca(GeocercaDTO gDTO){
		Criteria query = getCurrentSession().createCriteria(GeocercaNotificacionDTO.class);
		query.add(Restrictions.eq("idGeocerca", gDTO));
		
		return (List<GeocercaNotificacionDTO>) query.list();
	}
	
	@Override
	public Boolean eliminaRelacionGeocercaNotificacionPorGeocerca(GeocercaDTO gDTO){
		Criteria query = getCurrentSession().createCriteria(GeocercaNotificacionDTO.class);
		query.add(Restrictions.eq("idGeocerca", gDTO));
		
		List<GeocercaNotificacionDTO> listaGNDTO = (List<GeocercaNotificacionDTO>) query.list(); 
		
		for(GeocercaNotificacionDTO gNDTO :listaGNDTO)
		{
			NotificacionDTO nDTO = gNDTO.getIdNotificacion();
			this.delete(gNDTO);
			if(nDTO != null){
				//Limpiamos relaciones
				List<NotificaDispositivoDTO> listaRelacion = notificaDispositivoDAO.buscarDispositivosEnNotificacion(nDTO);
					
				for(NotificaDispositivoDTO ndDTOIterable: listaRelacion){
					notificaDispositivoDAO.delete(ndDTOIterable);
				}
				notificacionDAO.delete(nDTO);
			}
		}
		
		return Boolean.TRUE;
	}
}
