package mx.com.teclo.smm.persistencia.hibernate.dao.usuario;

import java.util.List;
import java.util.Map;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.empresa.EmpresaUsuarioDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.usuario.UsuarioDTO;
import mx.com.teclo.smm.persistencia.vo.usuario.UsuarioVO;

public interface EmpresaUsuarioDAO extends BaseDAO<EmpresaUsuarioDTO>{
	public UsuarioDTO findUserById(Long id, String cdApplication);
	public UsuarioDTO findUserByUserName(String username, String cdApplication, boolean isByApplication, boolean isActivo);
	public List<UsuarioDTO> findUsersByParams(String parameter, String value, String cdApplication, boolean isByApplication);
	public List<UsuarioDTO> findUsersByProfile(String profile, String cdApplication, boolean isByApplication);
	public List<UsuarioDTO> findExistentUser(Map<String, Object> parameters);
	public boolean isUserExist(UsuarioVO usuarioVO, String action);
	public Long findEmpresaByUsuario(Long idUsuario);
}
