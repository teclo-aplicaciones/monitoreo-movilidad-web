package mx.com.teclo.smm.negocio.service.empleado;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo.DispoEmpleadoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.empleado.EmpleadoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispoEmpleadoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.empleado.EmpleadoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.geocerca.GeocercaDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.geocerca.GeocercaDispositivoDTO;
import mx.com.teclo.smm.persistencia.vo.catalogo.CatalogoConSubtipoVO;
import mx.com.teclo.smm.persistencia.vo.catalogo.CatalogoVO;
import mx.com.teclo.smm.persistencia.vo.empleado.EmpleadoVO;

@Service
public class EmpleadoServiceImpl implements EmpleadoService {

	@Autowired
	private EmpleadoDAO empleadoDAO;
	
	@Autowired
	private DispoEmpleadoDAO dispoEmpleadoDAO;


	
	@Override
	@Transactional(readOnly=true)
	public List<CatalogoVO> buscarEmpleado(){
		List<CatalogoVO> listCatVO = new ArrayList<CatalogoVO>();
		List<EmpleadoDTO> listaDispoEmp = dispoEmpleadoDAO.buscarRelacionDispoEmpleadoUnicos();
		
		for(EmpleadoDTO eDTO : listaDispoEmp){
			CatalogoVO catVO = new CatalogoVO();
			catVO.setId(eDTO.getIdEmpleado());
			catVO.setNombre(eDTO.getNbEmpleado()+" "+eDTO.getNbPaterno()+" "+eDTO.getNbMaterno());
			listCatVO.add(catVO);
		}
		return listCatVO;
	}

	
	@Override
	@Transactional(readOnly=true)
	public List<CatalogoConSubtipoVO> buscarEmpleadoConIdSubtipoDispositivo(){
		List<CatalogoConSubtipoVO> listCatVO = new ArrayList<CatalogoConSubtipoVO>();
		List<EmpleadoDTO> listaDispoEmp = dispoEmpleadoDAO.buscarRelacionDispoEmpleadoUnicos();
		
		for(EmpleadoDTO eDTO : listaDispoEmp){
			CatalogoConSubtipoVO catVO = new CatalogoConSubtipoVO();
			catVO.setId(eDTO.getIdEmpleado());
			catVO.setNombre(eDTO.getNbEmpleado()+" "+eDTO.getNbPaterno()+" "+eDTO.getNbMaterno());			
			catVO.setSubTipoDispositivo(dispoEmpleadoDAO.buscarRelacionIdDispoEmpleadoPorEmpleado(eDTO.getIdEmpleado()).getIdDispositivo().getIdModelo().getIdSubtipoDispositivo().getIdSubTipoDispositivo()); 
			
			listCatVO.add(catVO);
		}
		return listCatVO;
	}
	
	
	@Transactional
	@Override
	public List<EmpleadoVO> buscarEmpleado(String param, String value) {
		List<EmpleadoDTO> lista = empleadoDAO.buscarEmpleados(param, value);
		List<EmpleadoVO> listaRet = new ArrayList<>();
		
		if(lista.size() > 0) {
			for(EmpleadoDTO empl : lista) {
				EmpleadoVO em = new EmpleadoVO();
				em.setIdEmpleado(empl.getIdEmpleado());
				em.setCdEmpleado(empl.getCdEmpleado());
				em.setNbEmpleado(empl.getNbEmpleado());
				em.setNbPaterno(empl.getNbPaterno());
				em.setNbMaterno(empl.getNbMaterno());
				em.setNuTelefono(empl.getNuTelefono());
				em.setNbMail(empl.getNbMail());
				em.setFhIngreso(empl.getFhIngreso().toString());
				listaRet.add(em);
			}
			
		}
			
		return listaRet;	
	}
	
	
	@Override
	@Transactional(readOnly = true)
	public EmpleadoDTO buscarEmpleado(Long id) {
		return empleadoDAO.buscarEmpleado(id);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Long[] obtenerDispositivosEnUsuario(Long id){
		
		List<DispoEmpleadoDTO> listaDispos;
		List<EmpleadoDTO> listaDispos2;
		EmpleadoDTO usuario = buscarEmpleado(id);
		Long[] ids = null;
		if(usuario != null){
			listaDispos = dispoEmpleadoDAO.buscarRelacionDispoEmpleadoPorEmpleado(usuario);
			ids = new Long[listaDispos.size()];
			for (int i = 0; i < listaDispos.size(); i++) {
				ids[i] = listaDispos.get(i).getIdDispositivo().getIdDispositivo();
			}
		}else{
			listaDispos = dispoEmpleadoDAO.buscarRelacionDispoEmpleado();
			ids = new Long[listaDispos.size()];
			for (int i = 0; i < listaDispos.size(); i++) {
				ids[i] = listaDispos.get(i).getIdDispositivo().getIdDispositivo();
			}
		}
		return ids;
	}
}
