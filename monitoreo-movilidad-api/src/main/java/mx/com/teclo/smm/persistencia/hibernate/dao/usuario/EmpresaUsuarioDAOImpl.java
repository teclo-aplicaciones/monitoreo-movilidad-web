package mx.com.teclo.smm.persistencia.hibernate.dao.usuario;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.empresa.EmpresaUsuarioDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.usuario.UsuarioDTO;
import mx.com.teclo.smm.persistencia.vo.usuario.UsuarioVO;
import mx.com.teclo.smm.util.enumerados.PerfilesEnum;

@SuppressWarnings("unchecked")
@Repository("empresaUsuarioDAO")
public class EmpresaUsuarioDAOImpl  extends BaseDAOImpl<EmpresaUsuarioDTO> implements EmpresaUsuarioDAO{

	@Override
	public List<UsuarioDTO> findUsersByParams(String parameter, String value, String cdApplication,boolean isByApplication) {

		ArrayList<UsuarioDTO> users = new ArrayList<UsuarioDTO>();
		ArrayList<EmpresaUsuarioDTO> empresaUsuarioDTO = null;
		Criteria query = getCurrentSession().createCriteria(EmpresaUsuarioDTO.class);

		query.createAlias("usuario", "usuario");
		query.createAlias("usuario.usuarioAplicacion", "usuarioAplicacion");
		query.createAlias("usuarioAplicacion.aplicacion", "aplicacion");

		query.add(Restrictions.eq("stActivo", new Integer(1)));
		query.add(Restrictions.eq("idEmpresa",getIdEmpresa()));

		if (parameter.equals("USERNAME")) {
			query.add(Restrictions.like("usuario.cdUsuario", "%" + value + "%").ignoreCase());
		}
		else if (parameter.equals("APELLIDO")) {
			query.add(Restrictions.like("usuario.nbApaterno", "%" + value + "%").ignoreCase());
		}
		else if (parameter.equals("NOMBRE")) {
			query.add(Restrictions.like("usuario.nbUsuario", "%" + value + "%").ignoreCase());
		}
		else if (parameter.equals("TODOS") && value.equals("ACTIVES")) {
			query.add(Restrictions.like("usuario.stActivo", new Integer(1)));
		}
		else if (parameter.equals("TODOS") && value.equals("INACTIVES")) {
			query.add(Restrictions.like("usuario.stActivo", new Integer(0)));
		}

		if(isByApplication){
			query.add(Restrictions.eq("aplicacion.cdAplicacion", cdApplication));
		}


		query.addOrder(Order.asc("usuario.nbUsuario"));

		empresaUsuarioDTO = (ArrayList<EmpresaUsuarioDTO>) query.list();

		for(EmpresaUsuarioDTO eu : empresaUsuarioDTO) {
			users.add(eu.getUsuario());
		}

		return users;
	}

	@Override
	public UsuarioDTO findUserById(Long id, String cdApplication) {

		UsuarioDTO usuario = null;
		EmpresaUsuarioDTO empresaUsuarioDTO = null;

		Criteria query = getCurrentSession().createCriteria(EmpresaUsuarioDTO.class);
		query.createAlias("usuario", "usuario");
		query.createAlias("usuario.perfilUsuario", "perfilUsuario");
		query.createAlias("perfilUsuario.perfil", "perfil");
		query.createAlias("perfil.aplicacion", "perfilAplicacion");
		query.createAlias("usuario.usuarioAplicacion", "usuarioAplicacion");
		query.createAlias("usuarioAplicacion.aplicacion", "aplicacion");

		query.add(Restrictions.eq("stActivo", new Integer(1)));
		query.add(Restrictions.eq("idEmpresa",getIdEmpresa()));

		query.add(Restrictions.eq("perfilAplicacion.cdAplicacion", cdApplication));
		query.add(Restrictions.eq("aplicacion.cdAplicacion", cdApplication));
		query.add(Restrictions.eq("usuario.idUsuario", id));


		empresaUsuarioDTO = (EmpresaUsuarioDTO) query.uniqueResult();
		usuario = empresaUsuarioDTO.getUsuario();

		return usuario;
	}

	@Override
	public List<UsuarioDTO> findExistentUser(Map<String, Object> parameters) {

		ArrayList<UsuarioDTO> users = new ArrayList<UsuarioDTO>();
		ArrayList<EmpresaUsuarioDTO> empresaUsuarioDTO = null;

		Criteria query = getCurrentSession().createCriteria(EmpresaUsuarioDTO.class);
		query.createAlias("usuario", "usuario");

		query.add(Restrictions.eq("stActivo", new Integer(1)));
		query.add(Restrictions.eq("idEmpresa",getIdEmpresa()));

		Iterator<String> iterator = parameters.keySet().iterator();
		while (iterator.hasNext()) {
			String key = iterator.next();
			query.add(Restrictions.eq(key, parameters.get(key)));
		}


		empresaUsuarioDTO = (ArrayList<EmpresaUsuarioDTO>) query.list();

		for(EmpresaUsuarioDTO eu : empresaUsuarioDTO) {
			users.add(eu.getUsuario());
		}

		return users;
	}

	@Override
	public boolean isUserExist(UsuarioVO usuarioVO, String action) {

		boolean isExist = false;
		ArrayList<UsuarioDTO> users = new ArrayList<UsuarioDTO>();
		ArrayList<EmpresaUsuarioDTO> empresaUsuarioDTO = null;

		Criteria query = getCurrentSession().createCriteria(EmpresaUsuarioDTO.class);
		query.createAlias("usuario", "usuario");

		query.add(Restrictions.eq("stActivo", new Integer(1)));
		query.add(Restrictions.eq("idEmpresa",getIdEmpresa()));

		Criterion nameUser = Restrictions.and(
			Restrictions.eq("usuario.nbUsuario", usuarioVO.getNbUsuario()),
			Restrictions.eq("usuario.nbApaterno", usuarioVO.getNbApaterno()),
			Restrictions.eq("usuario.nbAmaterno", usuarioVO.getNbAmaterno())
		);

		if(action.equals("new")) {
			query.add(Restrictions.or(nameUser, Restrictions.eq("usuario.cdUsuario", usuarioVO.getCdUsuario())));
		} else {
			query.add(nameUser);
		}


		empresaUsuarioDTO = (ArrayList<EmpresaUsuarioDTO>) query.list();

		for(EmpresaUsuarioDTO eu : empresaUsuarioDTO) {
			users.add(eu.getUsuario());
		}

		isExist = users.size() != 0 ? true : false;

		return isExist;
	}

	@Override
	public UsuarioDTO findUserByUserName(String username, String cdApplication, boolean isByApplication, boolean isActivo) {

		UsuarioDTO usuario = null;
		EmpresaUsuarioDTO empresaUsuarioDTO = null;
		Criteria query = getCurrentSession().createCriteria(EmpresaUsuarioDTO.class);
		query.createAlias("usuario", "usuario");
		query.createAlias("usuario.perfilUsuario", "perfilUsuario");
		query.createAlias("perfilUsuario.perfil", "perfil");
		query.createAlias("perfil.aplicacion", "perfilAplicacion");

		// Alias para alcanzar la aplicacion del usuario
		query.createAlias("usuario.usuarioAplicacion","usuarioAplicacion" );
		query.createAlias("usuarioAplicacion.aplicacion", "aplicacion");

		query.add(Restrictions.eq("stActivo", new Integer(1)));
		query.add(Restrictions.eq("idEmpresa",getIdEmpresa()));

		query.add(Restrictions.like("usuario.cdUsuario", "%" + username + "%").ignoreCase());
		if(isByApplication){
			query.add(Restrictions.eq("perfilAplicacion.cdAplicacion", cdApplication));
			query.add(Restrictions.eq("aplicacion.cdAplicacion", cdApplication));
		}
		if(isActivo)
			query.add(Restrictions.eq("usuario.stActivo", new Integer(1)));

		empresaUsuarioDTO = (EmpresaUsuarioDTO) query.uniqueResult();
		usuario = empresaUsuarioDTO.getUsuario();

		return usuario;
	}

	@Override
	public List<UsuarioDTO> findUsersByProfile(String cdProfile, String cdApplication, boolean isByApplication) {

		ArrayList<UsuarioDTO> users = new ArrayList<UsuarioDTO>();
		ArrayList<EmpresaUsuarioDTO> empresaUsuarioDTO = null;

		Criteria query = getCurrentSession().createCriteria(EmpresaUsuarioDTO.class);
		query.createAlias("usuario", "usuario");
		query.createAlias("usuario.perfilUsuario", "perfilUsuario");
		query.createAlias("perfilUsuario.perfil", "perfil");
		query.createAlias("perfil.aplicacion", "perfilAplicacion");
		query.createAlias("usuario.usuarioAplicacion", "usuarioAplicacion");
		query.createAlias("usuarioAplicacion.aplicacion", "aplicacion");



		query.add(Restrictions.eq("stActivo", new Integer(1)));
		query.add(Restrictions.eq("idEmpresa",getIdEmpresa()));

		if(!cdProfile.equals(PerfilesEnum.SIN_PERFIL.getCdPerfil())) {
			query.add(Restrictions.eq("perfil.cdPerfil", cdProfile));
			query.add(Restrictions.eq("perfilUsuario.stActivo", new Integer(1)));
		}

		if(isByApplication){
			query.add(Restrictions.eq("aplicacion.cdAplicacion", cdApplication));
		}

		query.addOrder(Order.asc("usuario.nbUsuario"));


		empresaUsuarioDTO = (ArrayList<EmpresaUsuarioDTO>) query.list();


		Map<Long, UsuarioDTO> muser = new HashMap<Long, UsuarioDTO>();

		for(EmpresaUsuarioDTO eu : empresaUsuarioDTO) {
			muser.put(eu.getUsuario().getIdUsuario(), eu.getUsuario());
		}

		for(Entry<Long, UsuarioDTO> entry : muser.entrySet()) {
			users.add(entry.getValue());
		}


		return users;
 	}

	@Override
	public Long findEmpresaByUsuario(Long idUsuario) {
		Criteria query = getCurrentSession().createCriteria(EmpresaUsuarioDTO.class);
		query.createAlias("usuario", "usuario");
		query.add(Restrictions.eq("usuario.idUsuario",idUsuario));

		EmpresaUsuarioDTO empresaUsuarioDTO = (EmpresaUsuarioDTO)query.uniqueResult();

		return empresaUsuarioDTO.getIdEmpresa();
	}

}
