package mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.MarcaDispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.TipoDispositivoDTO;

@SuppressWarnings("unchecked")
@Repository("marcaDispositivoDAO")
public class MarcaDispositivoDAOImpl extends BaseDAOImpl<MarcaDispositivoDTO> implements MarcaDispositivoDAO{

	
	@Override
	public List<MarcaDispositivoDTO> buscarCatalogo(Long[] id){
		Criteria query = getCurrentSession().createCriteria(MarcaDispositivoDTO.class);
		Disjunction multi = Restrictions.disjunction();
		if(id != null){
			for(int i = 0;i<id.length;i++){
				multi.add(Restrictions.eq("idMarca", id[i])); 
			}
			query.add(multi);
		}
		multi.add(Restrictions.eq("stActivo", Boolean.TRUE));
		//idEempresa
		multi.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		
		return (List<MarcaDispositivoDTO>) query.list();
	}
	
	@Override
	public MarcaDispositivoDTO obtenerMarca(Long id){
		Criteria query = getCurrentSession().createCriteria(MarcaDispositivoDTO.class);
		query.add(Restrictions.eq("idMarca", id));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		//idEempresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		return (MarcaDispositivoDTO) query.uniqueResult();
	}
	
	@Override
	public MarcaDispositivoDTO activarDesactivarObtenerMarca(Long id){
		Criteria query = getCurrentSession().createCriteria(MarcaDispositivoDTO.class);
		query.add(Restrictions.eq("idMarca", id));		
		return (MarcaDispositivoDTO) query.uniqueResult();
	}
	
	@Override
	public List<MarcaDispositivoDTO> buscarCatalogoPorTipo(Long[] id, TipoDispositivoDTO tdDTO){
		Criteria query = getCurrentSession().createCriteria(MarcaDispositivoDTO.class);
		Disjunction multi = Restrictions.disjunction();
		if(id != null){
			for(int i = 0;i<id.length;i++){
				multi.add(Restrictions.eq("idMarca", id[i])); 
			}
			query.add(multi);
		}
		query.createAlias("idTipoDispositivo", "tipoDispositivo");
		
		query.add(Restrictions.eq("tipoDispositivo.idTipoDispositivo", tdDTO.getIdTipoDispositivo()));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		//idEempresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		return (List<MarcaDispositivoDTO>) query.list();
	}
	
	@Override
	public List<MarcaDispositivoDTO> catalogoMarca(){
		Criteria query = getCurrentSession().createCriteria(MarcaDispositivoDTO.class);
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return (List<MarcaDispositivoDTO>) query.list();
	}
	
	@Override
	public List<MarcaDispositivoDTO> activarDesactivarCatalogoMarca(){
		Criteria query = getCurrentSession().createCriteria(MarcaDispositivoDTO.class);
		//idEempresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		query.addOrder(Order.asc("idMarca"));
		return (List<MarcaDispositivoDTO>) query.list();
	}
	
	
	@Override
	public List<MarcaDispositivoDTO> activarDesactivarBuscarCatalogo(Long[] id){
		Criteria query = getCurrentSession().createCriteria(MarcaDispositivoDTO.class);
		Disjunction multi = Restrictions.disjunction();
		if(id != null){
			for(int i = 0;i<id.length;i++){
				multi.add(Restrictions.eq("idMarca", id[i])); 
			}
			query.add(multi);
			//idEempresa
			query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		}
		
		return (List<MarcaDispositivoDTO>) query.list();
	}
	

}
