package mx.com.teclo.smm.persistencia.hibernate.dao.logs;

import java.util.List;

import mx.com.teclo.arquitectura.persistencia.comun.dao.BaseDao;
import mx.com.teclo.smm.persistencia.hibernate.dto.logs.PerfilLogsDTO;
import mx.com.teclo.smm.persistencia.vo.logs.ComboVO;

/**
 *  Copyright (c) 2018, Teclo Mexicana. 
 * 
 *  Descripcion					: PerfilLogsDAO
 *  Historial de Modificaciones	: 
 *  Descripcion del Cambio 		: Modificación
 *  
 *  @author 					: fjmb
 *  @version 					: 1.0 
 *  Fecha 						: 02/Abril/2019
 */
public interface PerfilLogsDAO extends BaseDao<PerfilLogsDTO> {

	/**
     * Consulta los <b>Logs</b> identificador de perfil.
     * 
     * @param perfilId 				Id del perfil.
     * @return List<PerfilLogsDTO> 	Regresa una lista de objetos de tipo <b>List<PerfilLogsDTO></b>.
     * 
     * @author 				   	 	javier07, fmartinez 
     * @version 			    	2.0
     * Fecha				    	01/Abril/2019
     */
	List<PerfilLogsDTO> busquedaPorPerfil(Long perfilId);
	
	/**
     * Consulta los <b>Perfiles</b> No asignados a logs
     * 
     * @param idLog 				Identificador del Log.
     * @param cdAplicacion 			Código de la aplicación.
     * @return List<ComboVO> 		Regresa una lista de objetos de tipo <b>List<ComboVO></b>.
     * 
     * @author 				   	 	javier07, fmartinez 
     * @version 			    	2.0
     * Fecha				    	01/Abril/2019
     */
	List<ComboVO> consultaPerfilesNoAsignados( Long idLog, String cdAplicacion);
	
	/**
     * Consulta los <b>Perfiles</b> asignados a un log especifico.
     * 
     * @param idLog 				Identificador del log.
     * @param cdAplicacion 			Código dela aplicación.

     * @return List<ComboVO> 		Regresa una lista de objetos de tipo <b>List<ComboVO></b>.
     * 
     * @author 				   	 	javier07, fmartinez 
     * @version 			    	2.0
     * Fecha				    	01/Abril/2019
     */
	List<PerfilLogsDTO> perfilesAsignadosPorLog( Long idLog, String cdAplicacion);
	
	/**
     * Consulta los <b>Perfiles</b> asignados a un log especifico por perfil.
     * 
     * @param perfilId 				Identificador del perfil.
     * @param idLog 				Identificador del Log.

     * @return PerfilLogsDTO 		Regresa uno objeto de tipo PerfilLogsDTO.
     * 
     * @author 				   	 	javier07, fmartinez 
     * @version 			    	2.0
     * Fecha				    	01/Abril/2019
     */
	PerfilLogsDTO busquedaPorPerfil(Long perfilId, Long idLog);

}
