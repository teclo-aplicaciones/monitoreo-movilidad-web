package mx.com.teclo.smm.persistencia.hibernate.dto.componeteweb;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TAQ055D_SE_CMP_GRP_COMP_WEB")
public class CmpGrpCompWebDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1576425801559752056L;
	

	@Id
	@GeneratedValue(generator="id_cmpgrpcompweb_generator",strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="id_cmpgrpcompweb_generator", sequenceName = "SQTAQ055D_SE_CMPGRPCOMWEB",allocationSize=1)
	@Column(name = "ID_CMP_GRP_COMP_WEB")
	private Long idCmpGrpCompWeb;		
	@ManyToOne
	@JoinColumn(name = "ID_GRP_COMPONENTE_WEB")
	private GrpComponenteWebDTO idGrpComponenteWebDTO;
	@ManyToOne
	@JoinColumn(name = "ID_COMPONENTE_WEB")
	private ComponenteWebDTO idComponenteWebDTO;
	@ManyToOne
	@JoinColumn(name = "ID_COMPWEB_ACCION")
	private ComponenteWebAccionDTO idComponenteWebAccionDTO;
	
	
	public ComponenteWebAccionDTO getIdComponenteWebAccionDTO() {
		return idComponenteWebAccionDTO;
	}
	public void setIdComponenteWebAccionDTO(ComponenteWebAccionDTO idComponenteWebAccionDTO) {
		this.idComponenteWebAccionDTO = idComponenteWebAccionDTO;
	}
	public Long getIdCmpGrpCompWeb() {
		return idCmpGrpCompWeb;
	}
	public void setIdCmpGrpCompWeb(Long idCmpGrpCompWeb) {
		this.idCmpGrpCompWeb = idCmpGrpCompWeb;
	}
	public GrpComponenteWebDTO getIdGrpComponenteWebDTO() {
		return idGrpComponenteWebDTO;
	}
	public void setIdGrpComponenteWebDTO(GrpComponenteWebDTO idGrpComponenteWebDTO) {
		this.idGrpComponenteWebDTO = idGrpComponenteWebDTO;
	}
	public ComponenteWebDTO getIdComponenteWebDTO() {
		return idComponenteWebDTO;
	}
	public void setIdComponenteWebDTO(ComponenteWebDTO idComponenteWebDTO) {
		this.idComponenteWebDTO = idComponenteWebDTO;
	}
	
	

}
