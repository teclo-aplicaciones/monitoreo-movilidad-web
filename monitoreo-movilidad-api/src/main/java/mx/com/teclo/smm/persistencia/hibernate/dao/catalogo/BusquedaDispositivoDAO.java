package mx.com.teclo.smm.persistencia.hibernate.dao.catalogo;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.catalogo.BusquedaDispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.catalogo.CatalogoDTO;


public interface BusquedaDispositivoDAO extends BaseDAO<BusquedaDispositivoDTO>{
	public List<BusquedaDispositivoDTO> obtenerCatalogos();	
}
