package mx.com.teclo.smm.persistencia.hibernate.dao.componenteweb;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.componeteweb.CmpGrpCompWebDTO;

public interface ComponenteWebGrupoDAO extends BaseDAO<CmpGrpCompWebDTO> {
	List<CmpGrpCompWebDTO> obtenerComponenteGrupoWebPorId(Long idGrupoComponente);	
}
