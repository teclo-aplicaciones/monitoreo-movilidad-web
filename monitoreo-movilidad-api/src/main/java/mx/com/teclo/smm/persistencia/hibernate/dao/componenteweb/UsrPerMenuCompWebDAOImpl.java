package mx.com.teclo.smm.persistencia.hibernate.dao.componenteweb;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.componeteweb.UsrPerMenuCompWebDTO;

@SuppressWarnings("unchecked")
@Repository("UsrPerMenuCompWebDAO")
public class UsrPerMenuCompWebDAOImpl extends BaseDAOImpl<UsrPerMenuCompWebDTO> implements UsrPerMenuCompWebDAO{

	@Override
	public List<UsrPerMenuCompWebDTO> obtenerUsrPerMenuCompWebPorUsuario(Long idUsuario,Long idMenu) {
		Criteria query = getCurrentSession().createCriteria(UsrPerMenuCompWebDTO.class);
		query.createAlias("idUsuarioDTO", "idUsuarioDTO");
		query.createAlias("idMenuDTO", "idMenuDTO");
		query.add(Restrictions.eq("idUsuarioDTO.idUsuario", idUsuario));
		query.add(Restrictions.eq("idMenuDTO.idMenu",idMenu));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return query.list();
	}

	@Override
	public List<UsrPerMenuCompWebDTO> obtenerUsrPerMenuCompWebPorPerfilMenu(Long idPerfilMenu){
		Criteria query = getCurrentSession().createCriteria(UsrPerMenuCompWebDTO.class);
		query.createAlias("idPerfilMenuDTO", "idPerfilMenuDTO");
		query.add(Restrictions.eq("idPerfilMenuDTO.idPerfilMenu", idPerfilMenu));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		List<UsrPerMenuCompWebDTO> res = query.list();
		return res;
	}

}
