package mx.com.teclo.smm.persistencia.vo.dispositivo;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class DispositivoHistoricoVO {
	private Long id;
	private String nombre;
	private List<RutaVO> rutas;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public List<RutaVO> getRutas() {
		return rutas;
	}
	public void setRutas(List<RutaVO> rutas) {
		this.rutas = rutas;
	}
}
