package mx.com.teclo.smm.persistencia.hibernate.dao.notificacion;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.TipoNotificacionDTO;

@Repository("tipoNotificacionDAO")
public class TipoNotificacionDAOImpl extends BaseDAOImpl<TipoNotificacionDTO> implements TipoNotificacionDAO{

	@Override
	public TipoNotificacionDTO buscarTipoNotificacionPorId(Long id){
		Criteria query = getCurrentSession().createCriteria(TipoNotificacionDTO.class);
		query.add(Restrictions.eq("idTipoNotificacion", id));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return (TipoNotificacionDTO) query.uniqueResult();
	}
}
