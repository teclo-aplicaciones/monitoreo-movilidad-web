package mx.com.teclo.smm.persistencia.hibernate.dao.grupo;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.grupo.TipoGrupoDTO;

public interface TipoGrupoDAO extends BaseDAO<TipoGrupoDTO>{
	public TipoGrupoDTO buscarTipoGrupoPorId(Long idTipoGrupo);
}
