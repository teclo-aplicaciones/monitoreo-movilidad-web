package mx.com.teclo.smm.persistencia.hibernate.dao.grupo;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.grupo.GrupoDTO;

public interface GrupoDAO extends BaseDAO<GrupoDTO> {
	public Long buscarSiguienteValor();
	public List<GrupoDTO> buscarGrupos();
	public List<GrupoDTO> buscarGruposPorNombre(String nombre);
	public GrupoDTO buscarGrupoPorId(Long id);
	public GrupoDTO activarDesactivarBuscarGrupoPorId(Long id);
	public List<GrupoDTO> activarDesactivarBuscarGrupos();
}
