package mx.com.teclo.smm.negocio.service.logs;

import java.text.ParseException;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import mx.com.teclo.arquitectura.ortogonales.persistencia.hibernate.dao.configuracion.AplicacionDAO;
import mx.com.teclo.arquitectura.ortogonales.seguridad.vo.UsuarioFirmadoVO;
import mx.com.teclo.arquitectura.ortogonales.service.comun.UsuarioFirmadoService;
import mx.com.teclo.smm.persistencia.hibernate.dao.logs.PerfilLogsDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.logs.WebLogsDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.logs.PerfilLogsDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.logs.WebLogsDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.perfil.PerfilDTO;
import mx.com.teclo.smm.persistencia.vo.logs.LogsVO;
import mx.com.teclo.smm.util.enumerados.LogsEnum;

@Service
public class LogsAdministracionServiceImpl implements LogsAdministracionService {

	@Autowired
	private PerfilLogsDAO perfilLogsDAO;
	@Autowired
	private WebLogsDAO webLogsDAO;
	@Autowired
	private AplicacionDAO aplicacionDAO;
	@Autowired
	private UsuarioFirmadoService usuarioFirmadoService;
	@Value("${app.config.codigo}")
	private String cdApplication;
	
	@Override
	@Transactional
	public LogsVO agregarPerfilALog(Long logId, Long perfilId) {
		PerfilLogsDTO perfilLogDTO = null;
		LogsVO logsVo = new LogsVO();

		perfilLogDTO = perfilLogsDAO.busquedaPorPerfil(perfilId, logId);

		if (perfilLogDTO == null) {
			UsuarioFirmadoVO usuarioFirmadoVO = usuarioFirmadoService.getUsuarioFirmadoVO();
			Long usuarioId = usuarioFirmadoVO.getId();
			
			PerfilDTO perfilDTO = new PerfilDTO();
			perfilDTO.setIdPerfil(perfilId);
			WebLogsDTO webLogDTO = new WebLogsDTO();
			webLogDTO.setIdLogWeb(logId);

			perfilLogDTO = new PerfilLogsDTO();
			perfilLogDTO.setPerfilDTO(perfilDTO);
			perfilLogDTO.setWebLogsDTO(webLogDTO);
			perfilLogDTO.setStActivo(1);
			perfilLogDTO.setFhCreacion(new Date());
			perfilLogDTO.setFhModificacion(new Date());

			perfilLogDTO.setIdUsrCreacion(usuarioId);
			perfilLogDTO.setIdUsrModifica(usuarioId);
			perfilLogsDAO.save(perfilLogDTO);
 			logsVo.setMensaje(LogsEnum.CREACION_REGISTRO.getAction());

		} else {
			logsVo.setMensaje(LogsEnum.ERROR_REGISTRO.getAction());
 		}

		return logsVo;
	}

	@Override
	@Transactional
	public void eliminarPerfilALog(Long logId, Long perfilId) {

		PerfilDTO perfilDTO = new PerfilDTO();
		perfilDTO.setIdPerfil(perfilId);
		WebLogsDTO webLogDTO = new WebLogsDTO();
		webLogDTO.setIdLogWeb(logId);

		PerfilLogsDTO perfilLogDTO = new PerfilLogsDTO();
		perfilLogDTO.setPerfilDTO(perfilDTO);
		perfilLogDTO.setWebLogsDTO(webLogDTO);
		perfilLogsDAO.delete(perfilLogDTO);

	}

	@Override
	@Transactional
	public void cambioDeEstatus(Long logId, String accion) {
		UsuarioFirmadoVO usuarioFirmadoVO = usuarioFirmadoService.getUsuarioFirmadoVO();
		Long usuarioId = usuarioFirmadoVO.getId();

		int estatus = accion.equals("1") ? 0 : 1;
		WebLogsDTO webLogsDTO = webLogsDAO.findOne(logId);
		webLogsDTO.setStActivo(estatus);
		webLogsDTO.setIdUsrModifica(usuarioId);
		webLogsDTO.setFhModificacion(new Date());
		webLogsDAO.update(webLogsDTO);

	}

	@Override
	@Transactional
	public LogsVO crudLog(LogsVO log, String operacion) throws ParseException {
		LogsVO logsVo = null;
		WebLogsDTO webLogsDTO = null;
		UsuarioFirmadoVO usuarioFirmadoVO = usuarioFirmadoService.getUsuarioFirmadoVO();
		Long usuarioId = usuarioFirmadoVO.getId();

		switch (operacion) {
		case "A":
			webLogsDTO = new WebLogsDTO();
			webLogsDTO.setNbLog(log.getLogNombre());
			webLogsDTO.setAplicacion(aplicacionDAO.getAplicacionByCode(cdApplication));
			webLogsDTO.setTxLog(log.getLogDescripcion());
			webLogsDTO.setNbExtension(log.getLogTipoArchivos());
			webLogsDTO.setNbRutaArchivo(log.getRutaArchivo());
			webLogsDTO.setStActivo(1);
			webLogsDTO.setIdUsrCreacion(usuarioId);
			webLogsDTO.setIdUsrModifica(usuarioId);
			webLogsDTO.setFhCreacion(new Date());
			webLogsDTO.setFhModificacion(new Date());
			webLogsDAO.saveOrUpdate(webLogsDTO);
			logsVo = new LogsVO();
			logsVo = copiarPropiedades(webLogsDTO,LogsEnum.CREACION_REGISTRO.getAction());

			break;
		case "M":
			webLogsDTO = webLogsDAO.busquedaLogPorId(log.getLogId());
			if (webLogsDTO != null) {
				webLogsDTO.setNbLog(log.getLogNombre());
				webLogsDTO.setTxLog(log.getLogDescripcion());
				webLogsDTO.setNbExtension(log.getLogTipoArchivos());
				webLogsDTO.setNbRutaArchivo(log.getRutaArchivo());
				webLogsDTO.setStActivo(1);
				webLogsDTO.setIdUsrModifica(usuarioId);
				webLogsDTO.setFhModificacion(new Date());
				webLogsDAO.saveOrUpdate(webLogsDTO);
				logsVo = new LogsVO();
				logsVo = copiarPropiedades(webLogsDTO,LogsEnum.ACTUALIZACION_REGISTRO.getAction());

			}
			
			break;
		case "AP":
			if (log.getLogId() != null && log.getPerfilId() != null) {
 				logsVo = agregarPerfilALog(log.getLogId(), log.getPerfilId());
 			}
			break;
		case "EP":
			PerfilLogsDTO perfilLogDTO = perfilLogsDAO.busquedaPorPerfil(log.getPerfilId(), log.getLogId());
			if (perfilLogDTO != null) {
				perfilLogsDAO.delete(perfilLogDTO);
				logsVo = new LogsVO();

				logsVo = copiarPropiedades(perfilLogDTO.getWebLogsDTO(),LogsEnum.ELIMINACION_REGISTRO.getAction());
			}
			break;
		default:
			break;
		}

		return logsVo;
	}
	
	private LogsVO copiarPropiedades( WebLogsDTO webLogsDTO, String menssage) {
		
		LogsVO logsVo = new LogsVO();
		logsVo.setLogId(webLogsDTO.getIdLogWeb());
		logsVo.setLogNombre( webLogsDTO.getNbLog());
		logsVo.setLogDescripcion(webLogsDTO.getTxLog());
		logsVo.setRutaArchivo(webLogsDTO.getNbRutaArchivo());
		logsVo.setLogEstatus(webLogsDTO.getStActivo() == 1 ? true: false);
		logsVo.setLogTipoArchivos(webLogsDTO.getNbExtension());
		logsVo.setMensaje(menssage);
		return logsVo;
		
	}
	
}
