package mx.com.teclo.smm.util.enumerados;

public enum ConfigNotiGeocercaEnum {
	DENTRO(1L),FUERA(2L),METROS100(3L);
	
	private Long id;
	
	private ConfigNotiGeocercaEnum(Long id){
		this.id = id;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
}
