package mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.PosicionActualDTO;

@SuppressWarnings("unchecked")
@Repository("posicionActualDAO")
public class PosicionActualDAOImpl extends BaseDAOImpl<PosicionActualDTO> implements PosicionActualDAO{

	@Override
	public List<PosicionActualDTO> obtenerCoordenadaActual(DispositivoDTO dispoDTO)
	{
		Criteria query = getCurrentSession().createCriteria(PosicionActualDTO.class);
		query.createAlias("idDispositivo", "idDispositivo");
		query.add(Restrictions.eq("idDispositivo", dispoDTO));
		//idEmpresa
		query.add(Restrictions.eq("idDispositivo.idEmpresa", getIdEmpresa()));
		return (List<PosicionActualDTO>) query.list();
	}
	
	@Override
	public List<PosicionActualDTO> obtenerRegistrosHoy(){
		Criteria query = getCurrentSession().createCriteria(PosicionActualDTO.class);
		query.add(Restrictions.sqlRestriction("FH_REGISTRO BETWEEN TRUNC(SYSDATE) AND TRUNC(SYSDATE+1)"));
		
		return (List<PosicionActualDTO>)query.list();
	}
	
	@Override
	public PosicionActualDTO obtenerRegistroHoyPorDispositivo(Long idDispo){
		Criteria query = getCurrentSession().createCriteria(PosicionActualDTO.class);
		query.createAlias("idDispositivo", "idDispositivo");
		query.add(Restrictions.eq("idDispositivo.idDispositivo",idDispo));
		//idEmpresa
		query.add(Restrictions.eq("idDispositivo.idEmpresa", getIdEmpresa()));
		query.add(Restrictions.sqlRestriction("FH_REGISTRO BETWEEN TRUNC(SYSDATE) AND TRUNC(SYSDATE+1)"));
		
		return (PosicionActualDTO)query.uniqueResult();
	}
}
