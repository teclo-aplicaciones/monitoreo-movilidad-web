package mx.com.teclo.smm.persistencia.hibernate.dao.comandos;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.ComandoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.DispositivoComandoDTO;


public interface DispositivoComandoDAO extends BaseDAO<DispositivoComandoDTO>{
	public List<DispositivoComandoDTO> obtenerComandoPorDispositivo(Long idDispositivo);		
	public DispositivoComandoDTO obtenerComandoPorDispositivoYComando(Long idDispositivo,Long idComando);
}
