package mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.TipoDispositivoDTO;

public interface TipoDispositivoDAO extends BaseDAO<TipoDispositivoDTO>{

	public List<TipoDispositivoDTO> buscarCatalogo();
	public TipoDispositivoDTO buscarCatalogoPorTipo(Long id);
	public TipoDispositivoDTO activarDesactivarBuscarCatalogoPorTipo(Long id);
	public List<TipoDispositivoDTO> activarDesactivarBuscarCatalogo();
}
