package mx.com.teclo.smm.persistencia.hibernate.dao.notificacion;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.MedioComunicacionDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.MedioNotificacionDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.NotificaDispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.NotificacionDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.TipoNotificacionDTO;

public interface NotificaDispositivoDAO extends BaseDAO<NotificaDispositivoDTO>{
	public Long buscarSiguienteIdentificador();
	public Boolean validaPushEnviados(NotificacionDTO nDTO);
	public List<NotificaDispositivoDTO> buscarDispositivosGeocerca(DispositivoDTO dDTO);
	public List<NotificaDispositivoDTO> buscarDispositivosNotificacion(DispositivoDTO dDTO);
	public List<NotificaDispositivoDTO> buscarTodoDispositivosNotificacion(DispositivoDTO dDTO, TipoNotificacionDTO tNDTO, MedioNotificacionDTO mCDTO);
	public List<NotificaDispositivoDTO> buscarDispositivosEnNotificacion(NotificacionDTO nDTO);
	public List<NotificaDispositivoDTO> buscarTodoDispositivosEnNotificacion(NotificacionDTO nDTO);
}
