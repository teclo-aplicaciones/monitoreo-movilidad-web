package mx.com.teclo.smm.negocio.service.evento;

import java.util.List;
import java.util.Map;

import mx.com.teclo.arquitectura.ortogonales.exception.NotFoundException;
import mx.com.teclo.smm.persistencia.vo.catalogo.CatalogoVO;
import mx.com.teclo.smm.persistencia.vo.evento.EventosTablasHistoricoVO;
import mx.com.teclo.smm.persistencia.vo.evento.TCDispositivoDiarioVO;
import mx.com.teclo.smm.persistencia.vo.evento.TCDispositivosDiariosVO;
import mx.com.teclo.smm.persistencia.vo.evento.TCDispositivosVO;
import mx.com.teclo.smm.persistencia.vo.evento.TCEventoVO;
import mx.com.teclo.smm.persistencia.vo.evento.TCEventosDiariosVO;
import mx.com.teclo.smm.persistencia.vo.evento.TCGrupoVO;

public interface EventoService {

	public List<EventosTablasHistoricoVO> consultarTiposEventosDispositivo(Long idDispositivo, String fecha);
	public List<mx.com.teclo.smm.persistencia.vo.evento.EventoVO> consultarEventos();
	public List<EventosTablasHistoricoVO> consultarTiposEventos(Long idGrupo, String fecha, String dia);
	public List<EventosTablasHistoricoVO> consultarTiposEventosHoy(Long idGrupo, Long idDispositivo);
	public List<CatalogoVO> consultarTiposEventosDispoActuales();
	public TCEventosDiariosVO consultarTiposEventosDiarios() throws NotFoundException;
	public TCDispositivosDiariosVO consultarDispositivosDiarios() throws NotFoundException;
	public TCDispositivoDiarioVO consultarEventosDispositivoDiarios(Long idDispositivo) throws NotFoundException;
	public List<TCGrupoVO> consultarTiposEventosHistoricos(Long idMes);
	public List<TCEventoVO> consultarGraficaEventosDispositivoHistorico(Long idDispo, Long idMes);
	public List<TCDispositivosVO> consultarEventosDispositivoHistorico(Long idMes);
	public List<Map<String, Long>> consultarEventosAnualesHistoricos(Long idGrupo, Long idDispo, Long idAnio);
}
