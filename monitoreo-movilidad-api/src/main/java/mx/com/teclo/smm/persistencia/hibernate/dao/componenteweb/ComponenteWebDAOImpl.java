package mx.com.teclo.smm.persistencia.hibernate.dao.componenteweb;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.componeteweb.ComponenteWebDTO;

@SuppressWarnings("unchecked")
@Repository("ComponenteWebDAO")
public class ComponenteWebDAOImpl extends BaseDAOImpl<ComponenteWebDTO> implements ComponenteWebDAO{

	@Override
	public ComponenteWebDTO obtenerComponenteWebPorId(Long idComponenteWeb) {
		 Criteria query = getCurrentSession().createCriteria(ComponenteWebDTO.class);
		 query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		 query.add(Restrictions.eq("idComponenteWeb", idComponenteWeb));

		 return (ComponenteWebDTO)query.uniqueResult();
	}
}
