package mx.com.teclo.smm.negocio.service.usuario;

import java.util.List;
import java.util.Map;

import mx.com.teclo.arquitectura.ortogonales.exception.NotFoundException;
import mx.com.teclo.smm.persistencia.vo.usuario.UsuarioVO;
import mx.com.teclo.smm.util.comun.FilterValuesVO;
import mx.com.teclo.smm.util.comun.ResponseVO;


public interface EmpresaUsuarioService {

	public UsuarioVO findUserById(Long id, String cdApplication);
	public List<UsuarioVO> findUsersByParams(String parameter, String value);
	public List<UsuarioVO> findUsersByProfile(String profile);
	public boolean saveOrUpdateUser(UsuarioVO usuarioVO, String action) throws NotFoundException;
	public boolean changeStatusUser(UsuarioVO usuarioVO);
	public boolean findExistentUser(Map<String, Object> parameters);
	public ResponseVO updatePassword(String username, String password, String oldPassword);
	public boolean resetPassword(String username);
	public List<FilterValuesVO> getQueryParamsUser();
	public List<FilterValuesVO> getQueryParamsAll();
	public String toggleEncryption(String password, String action);

}
