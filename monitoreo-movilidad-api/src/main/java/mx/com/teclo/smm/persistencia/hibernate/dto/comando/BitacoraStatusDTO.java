package mx.com.teclo.smm.persistencia.hibernate.dto.comando;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TMM067C_CM_BITACORA_STATUS")
public class BitacoraStatusDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator="id_bstatus_generator",strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="id_bstatus_generator", sequenceName = "SQTMM067C_CM_BIT_STATUS",allocationSize=1)
	@Column(name = "ID_BITACORA_STATUS")
	private Long idBitacoraStatus;

	@Column(name = "NB_STATUS")
	private String nbStatus;
	@Column(name = "ST_ACTIVO")
	private Boolean stActivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;
	@Column(name = "TX_COLOR")
	private String txColor;



	public String getTxColor() {
		return txColor;
	}
	public void setTxColor(String txColor) {
		this.txColor = txColor;
	}
	public Long getIdBitacoraStatus() {
		return idBitacoraStatus;
	}
	public void setIdBitacoraStatus(Long idBitacoraStatus) {
		this.idBitacoraStatus = idBitacoraStatus;
	}
	public String getNbStatus() {
		return nbStatus;
	}
	public void setNbStatus(String nbStatus) {
		this.nbStatus = nbStatus;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}



}

