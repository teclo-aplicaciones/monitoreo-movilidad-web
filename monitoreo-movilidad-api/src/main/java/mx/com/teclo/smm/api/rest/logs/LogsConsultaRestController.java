package mx.com.teclo.smm.api.rest.logs;

 

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.com.teclo.arquitectura.ortogonales.exception.NotFoundException;
import mx.com.teclo.arquitectura.ortogonales.seguridad.vo.UsuarioFirmadoVO;
import mx.com.teclo.arquitectura.ortogonales.service.comun.UsuarioFirmadoService;
import mx.com.teclo.smm.negocio.service.logs.LogsConsultaService;
import mx.com.teclo.smm.negocio.service.usuario.UsuarioService;
import mx.com.teclo.smm.persistencia.vo.logs.ComboVO;
import mx.com.teclo.smm.persistencia.vo.logs.LogsBusquedaPorIdVO;
import mx.com.teclo.smm.persistencia.vo.logs.LogsBusquedaVO;
import mx.com.teclo.smm.persistencia.vo.logs.LogsConsultaComboVO;
import mx.com.teclo.smm.persistencia.vo.logs.LogsVO;

@RestController
@RequestMapping("/logs")
public class LogsConsultaRestController {

	@Autowired
    private LogsConsultaService logsConsultaService;
	@Autowired
	private UsuarioFirmadoService usuarioFirmadoService;
	@Autowired
	UsuarioService usuarioService; 
    
    
    @RequestMapping(value = "/descargaArchivo", method = RequestMethod.GET)
    @PreAuthorize("hasAnyAuthority('LOGS_DESCARGA_ARCHIVO')")
    public ResponseEntity<InputStreamResource>  descargarArchivo(@RequestParam(value = "id")Long logId, @RequestParam(value = "nombre")String nombreArchivo,
    		HttpServletResponse response) throws IOException  {
 
        LogsBusquedaPorIdVO logVO = null;
        logVO = logsConsultaService.busquedaLogPorId( logId );
        logVO.setLogNombre(nombreArchivo);

    	File file = new File(logVO.getRutaArchivo() + "/"+ logVO.getLogNombre());
    	
		HttpHeaders respHeaders = new HttpHeaders();
		respHeaders.setContentType(new MediaType("application","force-download"));
		respHeaders.setContentLength(file.length());
		response.setHeader("Content-disposition", "attachment; filename=\"" + logVO.getLogNombre() + ".txt\"");
		response.setHeader("filename", logVO.getLogNombre());
		
		InputStreamResource isr = new InputStreamResource(new FileInputStream(file));
		return new ResponseEntity<InputStreamResource>(isr, respHeaders, HttpStatus.OK);
	}
    
    /**
     * Lista los <b>Archivos que contiene un log</> dependiendo del log
     * especificado.
     *
     * @param ruta Ruta donde se encuntran los archivos a listar.
     * @return Regresa un objeto de tipo <b>List<LogsVO><b/>
     */
    @RequestMapping(value = "/consultaArchivosPorLog", method = RequestMethod.GET)
    @PreAuthorize("hasAnyAuthority('LOGS_CONSULTA_ARCHIVOS')")
    public ResponseEntity<List<LogsVO>> getArchivosPorLog(@RequestParam(value = "id")Long logId) throws NotFoundException  {

    	LogsBusquedaPorIdVO logInfo = logsConsultaService.busquedaLogPorId(logId);  
        File file = new File(logInfo.getRutaArchivo());      
        List<LogsVO> listaArchivos = logsConsultaService.obtenerListaArchivosLogs(file, logInfo, logId);
  
        if (listaArchivos.isEmpty()) {
			throw new NotFoundException("No se encontraron registros");
		}	
        return new ResponseEntity<List<LogsVO>>(listaArchivos, HttpStatus.OK);
    }

  
    /**
     * Obtiene los <b>Todos los Logs</> existentes en la base de datos.
     *
     * @return Regresa un objeto de tipo <b>LogsVO<b/>.
     */
    @RequestMapping(value = "/consultaPorId", method = RequestMethod.GET)
    @PreAuthorize("hasAnyAuthority('LOGS_CONSULTA_ID')")
    public LogsBusquedaPorIdVO getLogPorId(@RequestParam(value = "id") Long id) {
        return logsConsultaService.busquedaLogPorId(id);
    }

    /**
     * Obtiene los <b>Logs Activos</> existentes en la base de datos.
     *
     * @return Regresa un objeto de tipo <b>List<LogsVO><b/>.
     */
    @RequestMapping(value = "/consultaActivos", method = RequestMethod.GET)
    @PreAuthorize("hasAnyAuthority('LOGS_CONSULTA_ACTIVOS')")
    public List<LogsBusquedaVO> getLogsActivos() {
        return logsConsultaService.busquedaLogsTodos();
    }

    /**
     * Obtiene los <b>Logs</> pertenecientes a un perfil web.
     *
     * @return Regresa un objeto de tipo <b>List<LogsVO><b/>.
     */
    @RequestMapping(value = "/consultaPorPerfil", method = RequestMethod.GET)
    @PreAuthorize("hasAnyAuthority('LOGS_CONSULTA_LOGS_PORPERFIL')")
    public List<LogsConsultaComboVO> getLogsPorPerfil() {
 	
    	UsuarioFirmadoVO usuario = usuarioFirmadoService.getUsuarioFirmadoVO();
        return logsConsultaService.busquedaLogsPorPerfil(usuario.getPerfilId());
    }

    /**
     * Obtiene los <b>Perfiles Asociados</> a un log.
     *
     * @return Regresa un objeto de tipo <b>List<ComboVO><b/>.
     */
    @RequestMapping(value = "/consultaPerfilesActivos", method = RequestMethod.GET)
    @PreAuthorize("hasAnyAuthority('LOGS_CONSULTA_PERFILES_ACTIVOS')")
    public List<ComboVO> getPerfilesActivosPorLog(@RequestParam(value = "id")Long logId) {
        return logsConsultaService.perfilesAsignadosPorLog(logId);
    }

    /**
     * Obtiene los <b>Perfiles no Asociados</> a un log.
     *
     * @return Regresa un objeto de tipo <b>List<ComboVO><b/>.
     */
    @RequestMapping(value = "/consultaPerfilesNoAsignados", method = RequestMethod.GET)
    @PreAuthorize("hasAnyAuthority('LOGS_CONSULTA_PERFILES_NOACTIVOS')")
    public List<ComboVO> getPerfilesNoActivosPorLog(@RequestParam(value = "id")Long logId) {
        return logsConsultaService.perfilesNoAsignadosPorLog(logId);
    }

    
}
