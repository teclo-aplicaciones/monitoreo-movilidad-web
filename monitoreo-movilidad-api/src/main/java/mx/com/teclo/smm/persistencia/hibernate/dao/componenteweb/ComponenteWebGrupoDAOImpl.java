package mx.com.teclo.smm.persistencia.hibernate.dao.componenteweb;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.componeteweb.CmpGrpCompWebDTO;



@SuppressWarnings("unchecked")
@Repository("ComponenteWebGrupoDAO")
public class ComponenteWebGrupoDAOImpl extends BaseDAOImpl<CmpGrpCompWebDTO> implements ComponenteWebGrupoDAO{

	@Override
	public	List<CmpGrpCompWebDTO> obtenerComponenteGrupoWebPorId(Long idGrupoComponente){
		 Criteria query = getCurrentSession().createCriteria(CmpGrpCompWebDTO.class);		 
		 query.add(Restrictions.eq("idGrpComponenteWebDTO.idGrpComponenteWeb", idGrupoComponente));
		 return (List<CmpGrpCompWebDTO>)query.list();		
	}
}
	