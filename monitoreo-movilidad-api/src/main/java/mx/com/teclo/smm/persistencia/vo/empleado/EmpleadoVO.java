package mx.com.teclo.smm.persistencia.vo.empleado;

public class EmpleadoVO{
	
	private Long idEmpleado;
	private String cdEmpleado;
	private String nbEmpleado;
	private String nbPaterno;
	private String nbMaterno;
	private String nbMail;
	private Long nuTelefono;
	private String fhIngreso;
	private String fhBaja;

	public EmpleadoVO() {}
	
	public EmpleadoVO(Long idEmpleado, String cdEmpleado, String nbEmpleado, String nbPaterno, String nbMaterno,
			String nbMail, Long nuTelefono, String fhIngreso, String fhBaja) {
		this.idEmpleado = idEmpleado;
		this.cdEmpleado = cdEmpleado;
		this.nbEmpleado = nbEmpleado;
		this.nbPaterno = nbPaterno;
		this.nbMaterno = nbMaterno;
		this.nbMail = nbMail;
		this.nuTelefono = nuTelefono;
		this.fhIngreso = fhIngreso;
		this.fhBaja = fhBaja;
	}
	
	
	public Long getIdEmpleado() {
		return idEmpleado;
	}
	public void setIdEmpleado(Long idEmpleado) {
		this.idEmpleado = idEmpleado;
	}
	public String getCdEmpleado() {
		return cdEmpleado;
	}
	public void setCdEmpleado(String cdEmpleado) {
		this.cdEmpleado = cdEmpleado;
	}
	public String getNbEmpleado() {
		return nbEmpleado;
	}
	public void setNbEmpleado(String nbEmpleado) {
		this.nbEmpleado = nbEmpleado;
	}
	public String getNbPaterno() {
		return nbPaterno;
	}
	public void setNbPaterno(String nbPaterno) {
		this.nbPaterno = nbPaterno;
	}
	public String getNbMaterno() {
		return nbMaterno;
	}
	public void setNbMaterno(String nbMaterno) {
		this.nbMaterno = nbMaterno;
	}
	public String getNbMail() {
		return nbMail;
	}
	public void setNbMail(String nbMail) {
		this.nbMail = nbMail;
	}
	public Long getNuTelefono() {
		return nuTelefono;
	}
	public void setNuTelefono(Long nuTelefono) {
		this.nuTelefono = nuTelefono;
	}
	public String getFhIngreso() {
		return fhIngreso;
	}
	public void setFhIngreso(String fhIngreso) {
		this.fhIngreso = fhIngreso;
	}
	public String getFhBaja() {
		return fhBaja;
	}
	public void setFhBaja(String fhBaja) {
		this.fhBaja = fhBaja;
	}
	

}
