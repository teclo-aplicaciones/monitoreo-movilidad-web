package mx.com.teclo.smm.persistencia.hibernate.dto.catalogo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TMM059C_CT_CATALOGO")
public class CatalogoDTO  implements Serializable{

	private static final long serialVersionUID = 4478682893395748764L;

	@Id
	@Column(name="ID_CATALOGO")
	private Long idCatalogo;

	@Column(name="TX_CATALOGO")
	private String txCatalogo;
	@Column(name="TX_CATALOGO_TABLA")
	private String txCatalogoTabla;
	@Column(name="ST_ACTIVO")
	private Boolean stActivo;
	@Column(name="FH_CREACION")
	private Date fhCreacion;
	@Column(name="ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name="FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name="ID_USR_MODIFICA")
	private Long idUsrModifica;

	public Long getIdCatalogo() {
		return idCatalogo;
	}
	public void setIdCatalogo(Long idCatalogo) {
		this.idCatalogo = idCatalogo;
	}
	public String getTxCatalogo() {
		return txCatalogo;
	}
	public void setTxCatalogo(String txCatalogo) {
		this.txCatalogo = txCatalogo;
	}
	public String getTxCatalogoTabla() {
		return txCatalogoTabla;
	}
	public void setTxCatalogoTabla(String txCatalogoTabla) {
		this.txCatalogoTabla = txCatalogoTabla;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}