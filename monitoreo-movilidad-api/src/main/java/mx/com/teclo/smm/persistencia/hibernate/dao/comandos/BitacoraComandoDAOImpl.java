package mx.com.teclo.smm.persistencia.hibernate.dao.comandos;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.hql.internal.ast.tree.OrderByClause;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.BitacoraComandoDTO;

@SuppressWarnings("unchecked")
@Repository("BitacoraComandoDAO")
public class BitacoraComandoDAOImpl extends BaseDAOImpl<BitacoraComandoDTO> implements BitacoraComandoDAO{

	@Override
	@Transactional(readOnly = true)
	public List<BitacoraComandoDTO> obtenerLogComando(Long idDispositivo){
		Criteria query = getCurrentSession().createCriteria(BitacoraComandoDTO.class);
		query.add(Restrictions.eq("idDispositivoDTO.idDispositivo", idDispositivo));
		
		//idEmpresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));

		
		query.addOrder(Order.desc("idBitacora"));
		return (List<BitacoraComandoDTO>)query.list();
	}
}
