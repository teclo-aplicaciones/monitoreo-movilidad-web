package mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.TipoDispositivoDTO;

@SuppressWarnings("unchecked")
@Repository("tipoDispositivoDAO")
public class TipoDispositivoDAOImpl extends BaseDAOImpl<TipoDispositivoDTO> implements TipoDispositivoDAO{

	
	@Override
	public List<TipoDispositivoDTO> buscarCatalogo()
	{
		Criteria query = getCurrentSession().createCriteria(TipoDispositivoDTO.class);
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));

		//idEempresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		
		return (List<TipoDispositivoDTO>)query.list();
	}
	
	@Override
	public TipoDispositivoDTO buscarCatalogoPorTipo(Long id)
	{
		Criteria query = getCurrentSession().createCriteria(TipoDispositivoDTO.class);
		query.add(Restrictions.eq("idTipoDispositivo", id));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		
		//idEempresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		
		return (TipoDispositivoDTO)query.uniqueResult();
	}
	
	@Override
	public TipoDispositivoDTO activarDesactivarBuscarCatalogoPorTipo(Long id)
	{
		Criteria query = getCurrentSession().createCriteria(TipoDispositivoDTO.class);
		query.add(Restrictions.eq("idTipoDispositivo", id));

		//idEempresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		
		return (TipoDispositivoDTO)query.uniqueResult();
	}
	
	@Override
	public List<TipoDispositivoDTO> activarDesactivarBuscarCatalogo()
	{
		Criteria query = getCurrentSession().createCriteria(TipoDispositivoDTO.class);
		

		//idEempresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		
		query.addOrder(Order.asc("idTipoDispositivo"));
		return (List<TipoDispositivoDTO>)query.list();
	}
}
