package mx.com.teclo.smm.negocio.service.notificacion;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.NotificacionDTO;

public interface NotificacionPushService {
	public Boolean enviaNotificacionPushInmediata(DispositivoDTO dispo, NotificacionDTO notificacion, String base64);
	public Boolean enviaNotificacionPushBatchInmediata(List<DispositivoDTO> listaDispos, NotificacionDTO notificacion);
}
