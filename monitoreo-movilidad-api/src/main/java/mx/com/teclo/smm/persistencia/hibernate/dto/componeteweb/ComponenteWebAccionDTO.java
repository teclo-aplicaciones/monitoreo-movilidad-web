package mx.com.teclo.smm.persistencia.hibernate.dto.componeteweb;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author sinuhe
 *
 */
@Entity
@Table(name = "TAQ052C_SE_COMPWEB_ACCION")
public class ComponenteWebAccionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6777902704704770685L;

	
	@Id
	@GeneratedValue(generator="id_compwebaccion_generator",strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="id_compwebaccion_generator", sequenceName = "SQTAQ052C_SE_COMPWEB_ACC",allocationSize=1)
	@Column(name = "ID_COMPWEB_ACCION")
	private Long idComponenteWebAccion;		
	@Column(name = "NB_COMPWEB_ACCION")
	private String nbComponenteWebAccion;
	@Column(name = "CD_COMPWEB_ACCION")
	private String cdComponenteWebAccion;	
	@Column(name = "ST_ACTIVO")
	private Boolean stActivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;
	
	public Long getIdComponenteWebAccion() {
		return idComponenteWebAccion;
	}
	public void setIdComponenteWebAccion(Long idComponenteWebAccion) {
		this.idComponenteWebAccion = idComponenteWebAccion;
	}
	public String getNbComponenteWebAccion() {
		return nbComponenteWebAccion;
	}
	public void setNbComponenteWebAccion(String nbComponenteWebAccion) {
		this.nbComponenteWebAccion = nbComponenteWebAccion;
	}
	public String getCdComponenteWebAccion() {
		return cdComponenteWebAccion;
	}
	public void setCdComponenteWebAccion(String cdComponenteWebAccion) {
		this.cdComponenteWebAccion = cdComponenteWebAccion;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
	
	
	

}
