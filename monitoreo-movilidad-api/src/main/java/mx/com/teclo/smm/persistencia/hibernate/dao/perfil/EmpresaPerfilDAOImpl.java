package mx.com.teclo.smm.persistencia.hibernate.dao.perfil;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.empresa.EmpresaPerfilDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.perfil.PerfilDTO;

@SuppressWarnings("unchecked")
@Repository
public class EmpresaPerfilDAOImpl  extends BaseDAOImpl<EmpresaPerfilDTO> implements EmpresaPerfilDAO{
	@Override
	public List<PerfilDTO> findProfilesByApp(String cdApplication) {
		ArrayList<PerfilDTO> perfils = new ArrayList<PerfilDTO>();

		Criteria query = getCurrentSession().createCriteria(EmpresaPerfilDTO.class);
		query.createAlias("idPerfil", "idPerfil");
		query.createAlias("idPerfil.aplicacion", "aplicacion");
		query.add(Restrictions.eq("idPerfil.stActivo", new Integer(1)));
		query.add(Restrictions.eq("aplicacion.cdAplicacion",cdApplication));
		query.add(Restrictions.eq("stActivo", new Integer(1)));
		query.add(Restrictions.eq("idEmpresa",getIdEmpresa()));

		ArrayList<EmpresaPerfilDTO> profiles = null;
		profiles = (ArrayList<EmpresaPerfilDTO>) query.list();

		for(EmpresaPerfilDTO ep : profiles) {
			perfils.add(ep.getIdPerfil());
		}

		return perfils;
	}

	@Override
	public List<PerfilDTO> findAllProfilesByApp(String cdApplication) {
		ArrayList<PerfilDTO> perfils = new ArrayList<PerfilDTO>();

		Criteria query = getCurrentSession().createCriteria(EmpresaPerfilDTO.class);
		query.createAlias("idPerfil", "idPerfil");
		query.createAlias("idPerfil.aplicacion", "aplicacion");
		query.add(Restrictions.eq("idPerfil.stActivo", new Integer(1)));
		query.add(Restrictions.eq("aplicacion.cdAplicacion",cdApplication));
		query.add(Restrictions.eq("stActivo", new Integer(1)));
		query.add(Restrictions.eq("idEmpresa",getIdEmpresa()));

		ArrayList<EmpresaPerfilDTO> profiles = null;
		profiles = (ArrayList<EmpresaPerfilDTO>) query.list();

		for(EmpresaPerfilDTO ep : profiles) {
			perfils.add(ep.getIdPerfil());
		}

		return perfils;
	}

	@Override
	public PerfilDTO findProfileById(Long id, String cdApplication, boolean isByApplication) {

		EmpresaPerfilDTO empresaPerfil = null;
		PerfilDTO perfilDTO = null;

		Criteria query = getCurrentSession().createCriteria(EmpresaPerfilDTO.class);
		query.createAlias("idPerfil", "idPerfil");
		query.createAlias("idPerfil.aplicacion", "aplicacion");

		query.add(Restrictions.eq("idPerfil.stActivo", new Integer(1)));
 		query.add(Restrictions.eq("idPerfil.idPerfil", id));
 		query.add(Restrictions.eq("stActivo", new Integer(1)));
 		query.add(Restrictions.eq("idEmpresa",getIdEmpresa()));

		if(isByApplication) {
			query.add(Restrictions.eq("aplicacion.cdAplicacion", cdApplication));
		}

		empresaPerfil = (EmpresaPerfilDTO) query.uniqueResult();
		perfilDTO = empresaPerfil.getIdPerfil();

		return perfilDTO;
	}

	@Override
	public PerfilDTO findUniqueProfileById(Long id, String cdApplication, boolean isByApplication) {

		EmpresaPerfilDTO empresaPerfil = null;
		PerfilDTO perfilDTO = null;

		Criteria query = getCurrentSession().createCriteria(EmpresaPerfilDTO.class);
		query.createAlias("idPerfil", "idPerfil");
		query.createAlias("idPerfil.aplicacion", "aplicacion");

 		query.add(Restrictions.eq("idPerfil.idPerfil", id));
 		query.add(Restrictions.eq("idEmpresa",getIdEmpresa()));

		if(isByApplication) {
			query.add(Restrictions.eq("aplicacion.cdAplicacion", cdApplication));
		}

		empresaPerfil = (EmpresaPerfilDTO) query.uniqueResult();
		perfilDTO = empresaPerfil.getIdPerfil();

		return perfilDTO;
	}
}
