package mx.com.teclo.smm.persistencia.hibernate.dao.geocerca;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.geocerca.GeocercaDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.geocerca.GeocercaNotificacionDTO;

public interface GeocercaNotificacionDAO extends BaseDAO<GeocercaNotificacionDTO>{
	public Long buscarSiguienteIdentificador();
	public List<GeocercaNotificacionDTO> buscaRelacionGeocercaNotificacionPorGeocerca(GeocercaDTO gDTO);
	public Boolean eliminaRelacionGeocercaNotificacionPorGeocerca(GeocercaDTO gDTO);
}
