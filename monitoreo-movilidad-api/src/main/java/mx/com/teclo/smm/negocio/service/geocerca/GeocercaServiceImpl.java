package mx.com.teclo.smm.negocio.service.geocerca;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.arquitectura.ortogonales.exception.NotFoundException;
import mx.com.teclo.arquitectura.ortogonales.service.comun.UsuarioFirmadoService;
import mx.com.teclo.smm.negocio.service.empresa.EmpresaService;
import mx.com.teclo.smm.negocio.service.notificacion.NotificacionService;
import mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo.DispoEmpleadoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo.DispositivoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo.InformacionDispDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.empleado.EmpleadoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.geocerca.ConfigNotificaGeocercaDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.geocerca.GeocercaCoordenadaDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.geocerca.GeocercaDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.geocerca.GeocercaDispositivoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.geocerca.GeocercaNotificacionDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.geocerca.TipoGeocercaDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.grupo.GrupoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.notificacion.MedioNotificacionDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.notificacion.NotificaDispositivoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.notificacion.TipoNotificacionDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispoEmpleadoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.empleado.EmpleadoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.geocerca.ConfigNotificaGeocercaDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.geocerca.GeocercaCoordenadaDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.geocerca.GeocercaDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.geocerca.GeocercaDispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.geocerca.GeocercaNotificacionDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.grupo.GrupoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.NotificacionDTO;
import mx.com.teclo.smm.persistencia.mybatis.dao.geocerca.GeocercaMyBatisDAO;
import mx.com.teclo.smm.persistencia.vo.JsonObjectVO;
import mx.com.teclo.smm.persistencia.vo.catalogo.CatalogoConSubtipoVO;
import mx.com.teclo.smm.persistencia.vo.catalogo.CatalogoVO;
import mx.com.teclo.smm.persistencia.vo.geocerca.ConsultaGeocercaVO;
import mx.com.teclo.smm.persistencia.vo.geocerca.CoordGeoVO;
import mx.com.teclo.smm.persistencia.vo.geocerca.DetalleGeocercaVO;
import mx.com.teclo.smm.persistencia.vo.geocerca.DispoGeocercaVO;
import mx.com.teclo.smm.persistencia.vo.geocerca.GeoNotiOpcionVO;
import mx.com.teclo.smm.persistencia.vo.geocerca.GeoNotificacionVO;
import mx.com.teclo.smm.persistencia.vo.geocerca.GeocercaVO;
import mx.com.teclo.smm.persistencia.vo.notificacion.NotificacionVO;
import mx.com.teclo.smm.util.enumerados.CatalogoGeocercaEnum;
import mx.com.teclo.smm.util.enumerados.TipoGeocercaEnum;

@Service
public class GeocercaServiceImpl implements GeocercaService{
	
	@Autowired
	GeocercaDAO geocercaDAO;
	
	@Autowired
	GrupoDAO grupoDAO;
	
	@Autowired
	EmpleadoDAO empleadoDAO;
	
	@Autowired
	DispositivoDAO dispositivoDAO;
	
	@Autowired
	UsuarioFirmadoService usuarioFirmadoService;
	
	@Autowired
	GeocercaDispositivoDAO geocercaDispositivoDAO;
	
	@Autowired
	GeocercaCoordenadaDAO geocercaCoordenadaDAO;
	
	@Autowired
	TipoGeocercaDAO tipoGeocercaDAO;
	
	@Autowired
	DispoEmpleadoDAO dispoEmpleadoDAO;
	
	@Autowired
	GeocercaMyBatisDAO geocercaMyBatisDAO;
	
	@Autowired
	InformacionDispDAO informacionDispDAO;
	
	@Autowired
	NotificacionService notificacionService;
	
	@Autowired
	ConfigNotificaGeocercaDAO configNotificaGeocercaDAO;
	
	@Autowired
	GeocercaNotificacionDAO geocercaNotificacionDAO;
	
	@Autowired
	MedioNotificacionDAO medioNotificacionDAO;
	
	@Autowired
	TipoNotificacionDAO tipoNotificacionDAO;
	
	@Autowired
	NotificaDispositivoDAO notificaDispositivoDAO;
	
	@Autowired
	EmpresaService empresaService;

	private static String JSON = "[{\"id\":\"400\",\"nombre\":\"GeoCerca Polanco\"},"
							   + " {\"id\":\"500\",\"nombre\":\"GeoCerca Coyoacan\"},"
							   + " {\"id\":\"600\",\"nombre\":\"GeoCerca Benito Juarez\"}]";

	List<JsonObjectVO> listaObject;
	
	@Override
	@Transactional(readOnly=true)
	public List<CatalogoVO> buscarGeocercas() {
		//ObjectMapper objectMapper = new ObjectMapper();
		List<CatalogoVO> listaGeocercas = new ArrayList<CatalogoVO>();
		List<GeocercaDTO>lista = geocercaDAO.buscarGeocercas();
		CatalogoVO catVO = null;
		
		for(GeocercaDTO geocerca:lista){
			catVO = new CatalogoVO();
			catVO.setId(geocerca.getIdGeocerca());
			catVO.setNombre(geocerca.getNbGeocerca());
			listaGeocercas.add(catVO);
		}
		
		/*try {
			listaObject = objectMapper.readValue(JSON, new TypeReference<List<JsonObjectVO>>(){});
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(JsonObjectVO object: listaObject)
		{
			CatalogoVO catVo = new CatalogoVO();
			catVo.setId(Long.parseLong(object.getId()));
			catVo.setNombre(object.getNombre());
			listaGeocercas.add(catVo);
		}*/
		
		return listaGeocercas;
	}
	
	@Override
	@Transactional(readOnly=true)
	public List<CatalogoConSubtipoVO> buscarGeocercasConIdSubtipoDispositivo(){
		//ObjectMapper objectMapper = new ObjectMapper();
		List<CatalogoConSubtipoVO> listaGeocercas = new ArrayList<CatalogoConSubtipoVO>();
		List<GeocercaDTO>lista = geocercaDAO.buscarGeocercas();
		CatalogoConSubtipoVO catVO = null;
		
		for(GeocercaDTO geocerca:lista){
			catVO = new CatalogoConSubtipoVO();
			catVO.setId(geocerca.getIdGeocerca());
			catVO.setNombre(geocerca.getNbGeocerca());
			listaGeocercas.add(catVO);
		}
		
		/*try {
			listaObject = objectMapper.readValue(JSON, new TypeReference<List<JsonObjectVO>>(){});
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(JsonObjectVO object: listaObject)
		{
			CatalogoVO catVo = new CatalogoVO();
			catVo.setId(Long.parseLong(object.getId()));
			catVo.setNombre(object.getNombre());
			listaGeocercas.add(catVo);
		}*/
		
		return listaGeocercas;
	}
	
	@Override
	@Transactional
	public List<ConsultaGeocercaVO> consultarGeocercas(Long tipoBusq, String valor) throws NotFoundException{
		
		List<ConsultaGeocercaVO> listaGeocercas = new ArrayList<ConsultaGeocercaVO>();
		List<GeocercaDTO> listaGDTO = new ArrayList<GeocercaDTO>();
		DispositivoDTO dDTO = null;
		EmpleadoDTO eDTO = null;
		GeocercaDTO gDTO = null;
		List<GeocercaDispositivoDTO> listaGdDTO = null;
		List<DispoEmpleadoDTO> listaDeDTO = null;
		
		if(tipoBusq == CatalogoGeocercaEnum.NOMBRE_GEOCERCA.getIdCat()){
			listaGDTO = geocercaDAO.buscarGeocercasPorNombre(valor);
			
			if(listaGDTO.isEmpty())
				throw new NotFoundException("No se encontraron resultados.");
			
		}else if (tipoBusq == CatalogoGeocercaEnum.DISPOSITIVO.getIdCat()){
			dDTO = dispositivoDAO.buscarDispositivosPorId(Long.parseLong(valor));
			listaGdDTO = geocercaDispositivoDAO.buscarDispositivosGeocerca(dDTO);
			
			if(listaGdDTO.isEmpty())
				throw new NotFoundException("No existen geocercas relacionadas con el dispositivo.");
			
			for(GeocercaDispositivoDTO gdDTO : listaGdDTO){
				gDTO = gdDTO.getIdGeocerca();
				listaGDTO.add(gDTO);
			}
		}else if (tipoBusq == CatalogoGeocercaEnum.USUARIOS.getIdCat()){
			eDTO = empleadoDAO.buscarEmpleado(Long.parseLong(valor));
			listaDeDTO = dispoEmpleadoDAO.buscarRelacionDispoEmpleadoPorEmpleado(eDTO);
			if(listaDeDTO.isEmpty())
				throw new NotFoundException("No existe dispositivo(s) relacionados con el usuario.");
			
			for(DispoEmpleadoDTO edDTO : listaDeDTO){
				dDTO = edDTO.getIdDispositivo();
				listaGdDTO = geocercaDispositivoDAO.buscarDispositivosGeocerca(dDTO);
				for(GeocercaDispositivoDTO gdDTO : listaGdDTO){
					gDTO = gdDTO.getIdGeocerca();
					listaGDTO.add(gDTO);
				}
			}
			if(listaGDTO.isEmpty())
				throw new NotFoundException("No existen geocercas relacionadas con el usuario.");
		}
		
		for(GeocercaDTO gIterableDTO : listaGDTO){
			ConsultaGeocercaVO gVO = new ConsultaGeocercaVO();
			gVO.setId(gIterableDTO.getIdGeocerca());
			gVO.setNombre(gIterableDTO.getNbGeocerca());
			gVO.setTipo(gIterableDTO.getIdTipoGeocercas().getNbTipoGeocerca());
			gVO.setColor(gIterableDTO.getCdColor());
			gVO.setGrupo(validaGeocercaGrupoRelacion(gIterableDTO).toString());
			gVO.setActivo(gIterableDTO.getStActivo());
			listaGeocercas.add(gVO);
		}
		
		/*ObjectMapper objectMapper = new ObjectMapper();
		List<GeocercaVO> listaGeocercas = new ArrayList<GeocercaVO>();
		
		try {
			listaObject = objectMapper.readValue(JSON, new TypeReference<List<JsonObjectVO>>(){});
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(JsonObjectVO object: listaObject)
		{
			double num = Math.random();
			Random x = new Random();
			GeocercaVO geoVo = new GeocercaVO();
			
			geoVo.setId(Long.parseLong(object.getId()));
			geoVo.setNombre(object.getNombre());
			geoVo.setTipo(num <= 0 ? 1L : num % 2 == 0 ? 2L:3L);
			geoVo.setColor("#".concat(Integer.toHexString(x.nextInt(16)))
							  .concat(Integer.toHexString(x.nextInt(16)))
							  .concat(Integer.toHexString(x.nextInt(16))));
			geoVo.setGrupo(num<=0?1L:0L);
			listaGeocercas.add(geoVo);
		}*/
		
		return listaGeocercas;
		
	}
	
	@Override
	public GeocercaDTO buscarGeocercaPorId(Long id){
		/*ObjectMapper objectMapper = new ObjectMapper();
		GeocercaVO geocerca = null;
		
		List<Double[]> coordenadas = new ArrayList<Double[]>();
		
		Double[] coordenada1 = {new Double(19.412240160937305), new Double(-99.16279971394947)};
		coordenadas.add(coordenada1);
		
		Double[] coordenada2 = {new Double(19.37465577893284), new Double(-99.15628164720393)};
		coordenadas.add(coordenada2);
		
		Double[] coordenada3 = {new Double(19.38791478859063), new Double(-99.12689208984376)};
		coordenadas.add(coordenada3);
		
		Double[] coordenada4 = {new Double(19.42617009660326), new Double(-99.12300520118735)};
		coordenadas.add(coordenada4);
		
		Double[] coordenada5 = {new Double(19.412240160937305), new Double(-99.16279971394947)};
		coordenadas.add(coordenada5);
		
		try {
			listaObject = objectMapper.readValue(JSON, new TypeReference<List<JsonObjectVO>>(){});
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(JsonObjectVO object: listaObject)
		{
			if(Long.parseLong(object.getId()) == id){
				double num = Math.random();
				Random x = new Random();
				geocerca = new GeocercaVO();
				geocerca.setId(Long.parseLong(object.getId()));
				geocerca.setNombre(object.getNombre());
				geocerca.setTipo(num <= 0 ? "1" : num % 2 == 0 ? "2":"3");
				geocerca.setDescripcion("Ejemplo de Descripcion");
				geocerca.setCoords(coordenadas);
				geocerca.setColor("#".concat(Integer.toHexString(x.nextInt(16)))
								  .concat(Integer.toHexString(x.nextInt(16)))
								  .concat(Integer.toHexString(x.nextInt(16)))
								  .concat(Integer.toHexString(x.nextInt(16)))
								  .concat(Integer.toHexString(x.nextInt(16)))
								  .concat(Integer.toHexString(x.nextInt(16))));
				geocerca.setGrupo(num<=0?true:false);
			}
		}*/
		
		return geocercaDAO.buscarGrupoPorId(id);
	}
	
	@Override
	@Transactional
	public GeocercaVO buscarGeocercaPorModificar(Long id){
		GeocercaVO gVO = geocercaMyBatisDAO.buscarGeocercaPorModificar(id);
		List<CoordGeoVO> listaCoordGeo = geocercaMyBatisDAO.buscaGeocercaPorModificarCoord(id);
		List<Double[]> listaCoords = new ArrayList<Double[]>();
		List<GeocercaDispositivoDTO> listaDispoGeo = geocercaDispositivoDAO.buscarDispositivosEnGeocerca(geocercaDAO.buscarGeocercaPorId(id));
		List<DispoGeocercaVO> listaDispoGeoVO = new ArrayList<DispoGeocercaVO>();
		gVO.setGrupo(validaGeocercaGrupoRelacion(geocercaDAO.buscarGeocercaPorId(id)));
		for(CoordGeoVO cgVO:listaCoordGeo){
			listaCoords.add(cgVO.getCoord());
		}
		if(gVO.getTipo() == TipoGeocercaEnum.CIRCULAR.getTipoGeocerca())
			listaCoords.add(new Double[]{0D,listaCoordGeo.get(0).getDistance()});
		
		for(GeocercaDispositivoDTO gdDTO: listaDispoGeo)
		{
			DispoGeocercaVO dgVO = new DispoGeocercaVO();
			dgVO.setIdItem(gdDTO.getIdDispositivo().getIdDispositivo());
			dgVO.setSrItem(informacionDispDAO.buscarRelacionPorDispositivo(gdDTO.getIdDispositivo()).getNuSerie());
			dgVO.setTxtItem(gdDTO.getIdDispositivo().getNbDispositivo());
			listaDispoGeoVO.add(dgVO);
		}
		
		gVO.setCoords(listaCoords);
		gVO.setDispositivos(listaDispoGeoVO);
		
		List<GeocercaNotificacionDTO> listaGNDTO = geocercaNotificacionDAO.buscaRelacionGeocercaNotificacionPorGeocerca(geocercaDAO.buscarGeocercaPorId(id));
		boolean isTypeAlreadySelected = false;
		GeoNotificacionVO gNVO = new GeoNotificacionVO();
		GeoNotiOpcionVO gNOVO = null;
		List<GeoNotiOpcionVO> listaGNOVO = new ArrayList<GeoNotiOpcionVO>();
		
		for(GeocercaNotificacionDTO gNDTO:listaGNDTO){
			if(!isTypeAlreadySelected)
				if(gNDTO.getIdNotificacion() != null){
					gNVO.setTipoEnvio(gNDTO.getIdNotificacion().getIdMedioTipoNotifi().getIdMedioTipoNotifi());
					isTypeAlreadySelected=true;
				}
			gNOVO = new GeoNotiOpcionVO();
			gNOVO.setId(gNDTO.getIdConfigGeoNoti().getIdConfigGeoNoti());
			gNOVO.setNombre(gNDTO.getIdConfigGeoNoti().getTxConfigGeoNoti());
			gNOVO.setValor(gNDTO.getStActivo());
			listaGNOVO.add(gNOVO);
			
		}
		
		if(!isTypeAlreadySelected)
			//Buscar catalogo de medios por tipo de notificacion Geocerca
			gNVO.setTipoEnvio(medioNotificacionDAO.buscarCatalogo(tipoNotificacionDAO.buscarTipoNotificacionPorId(2L)).get(0).getIdMedioTipoNotifi());
			
		gNVO.setOpciones(listaGNOVO);
		
		gVO.setNotificacion(gNVO);
		
		return gVO;
	}
	
	@Override
	@Transactional(readOnly = true)
	public Long[] obtenerDispositivosEnGeocerca(Long idGeocerca) {
		List<GeocercaDispositivoDTO> listaDispos;
		GeocercaDTO geocerca = buscarGeocercaPorId(idGeocerca);

		if(geocerca != null){
			listaDispos = geocercaDispositivoDAO.buscarDispositivosEnGeocerca(geocerca);
		}else{
			listaDispos = geocercaDispositivoDAO.buscarTodoDispositivosEnGeocercas();
		}

		Long[] ids = new Long[listaDispos.size()];
		for (int i = 0; i < listaDispos.size(); i++) {
			ids[i] = listaDispos.get(i).getIdDispositivo().getIdDispositivo();
		}
		return ids;
	}
	
	@Override
	@Transactional
	public Boolean cambiarEstadoGeocerca(Long id){
		GeocercaDTO gDTO = geocercaDAO.buscarGeocercaPorId(id);
		Boolean anterior = gDTO.getStActivo();
		gDTO.setStActivo(!gDTO.getStActivo());
		
		return anterior != gDTO.getStActivo() ? Boolean.TRUE:Boolean.FALSE;
	}
	
	@Override
	@Transactional
	public List<DetalleGeocercaVO> buscaDetalleGeocerca(Long id){
		List<DetalleGeocercaVO> res = new ArrayList<DetalleGeocercaVO>();
		
		GeocercaDTO gDTO = geocercaDAO.buscarGeocercaPorId(id);
		List<GeocercaDispositivoDTO> listaGDDTO = geocercaDispositivoDAO.buscarDispositivosEnGeocerca(gDTO);
		
		for(GeocercaDispositivoDTO gDDTO: listaGDDTO){
			DetalleGeocercaVO dGVO = new DetalleGeocercaVO();
			dGVO.setNombre(gDDTO.getIdDispositivo().getNbDispositivo());
			dGVO.setTipo(gDDTO.getIdDispositivo().getIdModelo().getIdSubtipoDispositivo().getIdTipoDispositivo().getNbTipoDispositivo());
			dGVO.setActivo(gDDTO.getIdDispositivo().getStActivo());
			dGVO.setMarca(gDDTO.getIdDispositivo().getIdModelo().getIdMarca().getNbMarca());
			dGVO.setModelo(gDDTO.getIdDispositivo().getIdModelo().getNbModelo());
			res.add(dGVO);
		}
		
		return res;
	}
	
	
	
	
	@Override
	@SuppressWarnings("unchecked")
	@Transactional
	public Boolean guardarGeocerca(GeocercaVO geocercaVO) throws ParseException{
		
		GrupoDTO gDTO = null;
		GeocercaDispositivoDTO GeoDispDTO;
		GeocercaCoordenadaDTO GeoCoordDTO;
		GeocercaNotificacionDTO GeoNotiDTO;
		
		
		Long idUsuario = usuarioFirmadoService.getUsuarioFirmadoVO().getId();
		Long idGeocerca = geocercaMyBatisDAO.buscarSiguienteIdGeocerca();
		Map res = configurarInformacionSpatial(geocercaVO.getCoords());
		
		Date fhCreacion = new Date();
		
		List<Double[]> coords = (List<Double[]>)res.get("coords");
		String config = (String) res.get("config");
		
		String listCoords = convertirCoordsParaTheGeom(geocercaVO.getTipo(), coords);
		if(geocercaVO.getTipo() != TipoGeocercaEnum.CIRCULAR.getTipoGeocerca()){
			geocercaMyBatisDAO.insertarGeocercaForma(idGeocerca, empresaService.getIdEmpresa(),geocercaVO.getTipo(), geocercaVO.getNombre(), geocercaVO.getDescripcion(), geocercaVO.getColor(), idUsuario, geocercaVO.getWkt());
			//geocercaMyBatisDAO.insertarGeocercaForma(idGeocerca, geocercaVO.getTipo(), geocercaVO.getNombre(), config, listCoords, geocercaVO.getDescripcion(), geocercaVO.getColor(), idUsuario);
			geocercaMyBatisDAO.insertarGeom(idGeocerca);
			//geocercaMyBatisDAO.updateSRID(idGeocerca);
		}else{
			geocercaMyBatisDAO.insertarGeocercaCirculo(idGeocerca, empresaService.getIdEmpresa(), geocercaVO.getTipo(), geocercaVO.getNombre(), listCoords, geocercaVO.getDescripcion(), geocercaVO.getColor(), idUsuario);
			geocercaMyBatisDAO.proyeccionGeom(idGeocerca);
		}
			
		//Agregamos la relación a los dispositivos en la tabla intermedia
		
		if(geocercaVO.getGrupo() != 0L)
			gDTO = grupoDAO.buscarGrupoPorId(geocercaVO.getGrupo());
		
		if(geocercaVO.getDispositivos() != null){
			for(DispoGeocercaVO dgVO:geocercaVO.getDispositivos()){
				fhCreacion = new Date();
				GeoDispDTO = new GeocercaDispositivoDTO();
				GeoDispDTO.setIdDispGeo(geocercaDispositivoDAO.buscarSiguenteValor());
				GeoDispDTO.setIdDispositivo(dispositivoDAO.buscarDispositivosPorId(dgVO.getIdItem()));
				GeoDispDTO.setIdGeocerca(geocercaDAO.buscarGrupoPorId(idGeocerca));
				GeoDispDTO.setIdGrupo(gDTO != null ? gDTO: null);
				GeoDispDTO.setStActivo(Boolean.TRUE);
				GeoDispDTO.setFhCreacion(fhCreacion);
				GeoDispDTO.setIdUsrCreacion(idUsuario);
				GeoDispDTO.setFhModificacion(fhCreacion);
				GeoDispDTO.setIdUsrModifica(idUsuario);
				
				geocercaDispositivoDAO.save(GeoDispDTO);
			}
		}
		
		DecimalFormat df = new DecimalFormat("#.######");
		df.setRoundingMode(RoundingMode.CEILING);
		for(Double[] coord : coords){
			fhCreacion = new Date();
			GeoCoordDTO = new GeocercaCoordenadaDTO();
			GeoCoordDTO.setIdCordenadasGeo(geocercaCoordenadaDAO.buscarSiguenteValor());
			GeoCoordDTO.setIdGeocercas(geocercaDAO.buscarGrupoPorId(idGeocerca));
			GeoCoordDTO.setNuLatitud(Double.parseDouble(df.format(coord[1])));
			GeoCoordDTO.setNuLongitud(Double.parseDouble(df.format(coord[0])));
			GeoCoordDTO.setNuDistancia(0D);
			GeoCoordDTO.setStActivo(Boolean.TRUE);
			GeoCoordDTO.setFhCreacion(fhCreacion);
			GeoCoordDTO.setIdUsrCreacion(idUsuario);
			GeoCoordDTO.setFhModificacion(fhCreacion);
			GeoCoordDTO.setIdUsrModifica(idUsuario);
			if(geocercaVO.getTipo() == TipoGeocercaEnum.CIRCULAR.getTipoGeocerca()){
				if(coord[0] != 0L){
					GeoCoordDTO.setNuDistancia(Double.parseDouble(df.format(coords.get(1)[1])));
				}else{
					continue;
				}
			}
			geocercaCoordenadaDAO.save(GeoCoordDTO);
			
			// dejar de guardar geo por coordenada
			/*
			GeocercaDTO tempGeo = geocercaDAO.buscarGeocercaPorId(idGeocerca);			
			for(GeoNotiOpcionVO gNOVO:geocercaVO.getNotificacion().getOpciones()){
				NotificacionDTO nDTO = null;
				if(gNOVO.getValor()){
					//Llamar a funcion que creara la notificacion de acuerdo al id, el mensaje y el titulo
					nDTO = generaNotificacionParaGeocerca(gNOVO.getId(),geocercaVO.getNotificacion().getTipoEnvio(), geocercaVO, tempGeo);
				}
				GeoNotiDTO = new GeocercaNotificacionDTO();
				
				GeoNotiDTO.setIdGeoNotifica(geocercaNotificacionDAO.buscarSiguienteIdentificador());
				GeoNotiDTO.setIdGeocerca(tempGeo);
				GeoNotiDTO.setIdConfigGeoNoti(configNotificaGeocercaDAO.buscaCatalogoPorId(gNOVO.getId()));
				GeoNotiDTO.setIdNotificacion(nDTO);
				GeoNotiDTO.setStActivo(nDTO != null ? Boolean.TRUE: Boolean.FALSE);
				GeoNotiDTO.setFhCreacion(new Date());
				GeoNotiDTO.setIdUsrCreacion(idUsuario);
				GeoNotiDTO.setFhModificacion(new Date());
				GeoNotiDTO.setIdUsrModifica(idUsuario);
				
				geocercaNotificacionDAO.save(GeoNotiDTO);
			}
			*/
		}		
		
		// guardar geo por notificaciones
		GeocercaDTO tempGeo = geocercaDAO.buscarGeocercaPorId(idGeocerca);			
		for(GeoNotiOpcionVO gNOVO:geocercaVO.getNotificacion().getOpciones()){
			NotificacionDTO nDTO = null;
			if(gNOVO.getValor()){
				//Llamar a funcion que creara la notificacion de acuerdo al id, el mensaje y el titulo
				nDTO = generaNotificacionParaGeocerca(gNOVO.getId(),geocercaVO.getNotificacion().getTipoEnvio(), geocercaVO, tempGeo);
			}
			GeoNotiDTO = new GeocercaNotificacionDTO();
			
			GeoNotiDTO.setIdGeoNotifica(geocercaNotificacionDAO.buscarSiguienteIdentificador());
			GeoNotiDTO.setIdGeocerca(tempGeo);
			GeoNotiDTO.setIdConfigGeoNoti(configNotificaGeocercaDAO.buscaCatalogoPorId(gNOVO.getId()));
			GeoNotiDTO.setIdNotificacion(nDTO);
			GeoNotiDTO.setStActivo(nDTO != null ? Boolean.TRUE: Boolean.FALSE);
			GeoNotiDTO.setFhCreacion(new Date());
			GeoNotiDTO.setIdUsrCreacion(idUsuario);
			GeoNotiDTO.setFhModificacion(new Date());
			GeoNotiDTO.setIdUsrModifica(idUsuario);
			
			geocercaNotificacionDAO.save(GeoNotiDTO);
		}		
		
		return Boolean.TRUE;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	@Transactional
	public Boolean actualizarGeocerca(GeocercaVO geocercaVO) throws ParseException{
		
		GrupoDTO gDTO = null;
		GeocercaDTO geoDTO = geocercaDAO.buscarGeocercaPorId(geocercaVO.getId());
		GeocercaDispositivoDTO GeoDispDTO = null;
		GeocercaCoordenadaDTO GeoCoordDTO;
		GeocercaNotificacionDTO GeoNotiDTO;
		
		Long idUsuario = usuarioFirmadoService.getUsuarioFirmadoVO().getId();
		
		Map res = configurarInformacionSpatial(geocercaVO.getCoords());
		
		Date fhModificacion = new Date();
		
		geoDTO.setNbGeocerca(geocercaVO.getNombre());
		geoDTO.setCdColor(geocercaVO.getColor());
		geoDTO.setIdTipoGeocercas(tipoGeocercaDAO.buscarTipoGeocercaPorId(geocercaVO.getTipo()));
		geoDTO.setTxGeocerca(geocercaVO.getDescripcion());
		geoDTO.setIdUsrModifica(idUsuario);
		geoDTO.setFhModificacion(fhModificacion);
		
		List<Double[]> coords = (List<Double[]>)res.get("coords");
		String config = (String) res.get("config");
		
		String listCoords = convertirCoordsParaTheGeom(geocercaVO.getTipo(), coords);
		
		//RE-ESTRUCTURAMOS LA THE_GEOM
		if(geocercaVO.getTipo() != TipoGeocercaEnum.CIRCULAR.getTipoGeocerca()){
			geocercaMyBatisDAO.actualizaGeocercaForma(geoDTO.getIdGeocerca(), geocercaVO.getWkt());
			//geocercaMyBatisDAO.insertarGeocercaForma(idGeocerca, geocercaVO.getTipo(), geocercaVO.getNombre(), config, listCoords, geocercaVO.getDescripcion(), geocercaVO.getColor(), idUsuario);
			geocercaMyBatisDAO.insertarGeom(geoDTO.getIdGeocerca());
			//geocercaMyBatisDAO.updateSRID(idGeocerca);
		}else{
			geocercaMyBatisDAO.actualizarGeocercaCirculo(geoDTO.getIdGeocerca(), listCoords);
			geocercaMyBatisDAO.proyeccionGeom(geoDTO.getIdGeocerca());
		}
			
		//ELIMINA DISPOSITIVOS
		geocercaDispositivoDAO.eliminarRelacionGeocercaDispositivo(geoDTO);
		
		if(geocercaVO.getGrupo() != 0L)
			gDTO = grupoDAO.buscarGrupoPorId(geocercaVO.getGrupo());
		
		if(geocercaVO.getDispositivos() != null){
			for(DispoGeocercaVO dgVO:geocercaVO.getDispositivos()){
				fhModificacion = new Date();
				GeoDispDTO = new GeocercaDispositivoDTO();
				GeoDispDTO.setIdDispGeo(geocercaDispositivoDAO.buscarSiguenteValor());
				GeoDispDTO.setIdDispositivo(dispositivoDAO.buscarDispositivosPorId(dgVO.getIdItem()));
				GeoDispDTO.setIdGeocerca(geoDTO);
				GeoDispDTO.setIdGrupo(gDTO != null ? gDTO: null);
				GeoDispDTO.setStActivo(Boolean.TRUE);
				GeoDispDTO.setFhCreacion(fhModificacion);
				GeoDispDTO.setIdUsrCreacion(idUsuario);
				GeoDispDTO.setFhModificacion(fhModificacion);
				GeoDispDTO.setIdUsrModifica(idUsuario);
				
				geocercaDispositivoDAO.save(GeoDispDTO);
			}
		}
		
		geocercaCoordenadaDAO.eliminarRelacionGeocercaCoordenada(geoDTO);
		DecimalFormat df = new DecimalFormat("#.######");
		df.setRoundingMode(RoundingMode.CEILING);
		for(Double[] coord : coords)
		{
			fhModificacion = new Date();
			GeoCoordDTO = new GeocercaCoordenadaDTO();
			GeoCoordDTO.setIdCordenadasGeo(geocercaCoordenadaDAO.buscarSiguenteValor());
			GeoCoordDTO.setIdGeocercas(geoDTO);
			GeoCoordDTO.setNuLatitud(Double.parseDouble(df.format(coord[1])));
			GeoCoordDTO.setNuLongitud(Double.parseDouble(df.format(coord[0])));
			GeoCoordDTO.setNuDistancia(0D);
			GeoCoordDTO.setStActivo(Boolean.TRUE);
			GeoCoordDTO.setFhCreacion(fhModificacion);
			GeoCoordDTO.setIdUsrCreacion(idUsuario);
			GeoCoordDTO.setFhModificacion(fhModificacion);
			GeoCoordDTO.setIdUsrModifica(idUsuario);
			if(geocercaVO.getTipo() == TipoGeocercaEnum.CIRCULAR.getTipoGeocerca()){
				if(coord[0] != 0L){
					GeoCoordDTO.setNuDistancia(Double.parseDouble(df.format(coords.get(1)[1])));
				}else{
					continue;
				}
			}
			geocercaCoordenadaDAO.save(GeoCoordDTO);
		}		
		
		//Eliminar relacion de notificaciones en caso de que existan
		geocercaNotificacionDAO.eliminaRelacionGeocercaNotificacionPorGeocerca(geoDTO);
		
		for(GeoNotiOpcionVO gNOVO:geocercaVO.getNotificacion().getOpciones()){
			NotificacionDTO nDTO = null;
			if(gNOVO.getValor()){
				//Llamar a funcion que creara la notificacion de acuerdo al id, el mensaje y el titulo
				nDTO = generaNotificacionParaGeocerca(gNOVO.getId(),geocercaVO.getNotificacion().getTipoEnvio(), geocercaVO, geoDTO);
			}
			GeoNotiDTO = new GeocercaNotificacionDTO();
			
			GeoNotiDTO.setIdGeoNotifica(geocercaNotificacionDAO.buscarSiguienteIdentificador());
			GeoNotiDTO.setIdGeocerca(geoDTO);
			GeoNotiDTO.setIdConfigGeoNoti(configNotificaGeocercaDAO.buscaCatalogoPorId(gNOVO.getId()));
			GeoNotiDTO.setIdNotificacion(nDTO);
			GeoNotiDTO.setStActivo(nDTO != null ? Boolean.TRUE: Boolean.FALSE);
			GeoNotiDTO.setFhCreacion(new Date());
			GeoNotiDTO.setIdUsrCreacion(idUsuario);
			GeoNotiDTO.setFhModificacion(new Date());
			GeoNotiDTO.setIdUsrModifica(idUsuario);
			
			geocercaNotificacionDAO.save(GeoNotiDTO);
		}
		
		return Boolean.TRUE;
	}
	
	@Override
	@Transactional
	public Boolean eliminaGeocercaPerma(Long id) throws NotFoundException{
		GeocercaDTO gDTO = geocercaDAO.buscarGeocercaPorId(id);
		boolean hasCoordsDeleted = false;
		boolean hasDevicesDeleted = false;
		boolean hasMessagesDeleted  = false;
		
		//Relacion Coordenadas
		List<GeocercaCoordenadaDTO> gCDTO = geocercaCoordenadaDAO.buscarRelacionGeocercaCoordenadas(gDTO);
		
		//Relacion Dispositivos
		List<GeocercaDispositivoDTO> gDDTO = geocercaDispositivoDAO.buscarDispositivosEnGeocerca(gDTO);
		
		
		//PRIMERO ELIMINAMOS LAS RELACIONES
		for(GeocercaCoordenadaDTO x : gCDTO)
		{
			geocercaCoordenadaDAO.delete(x);
		}
		
		gCDTO = geocercaCoordenadaDAO.buscarRelacionGeocercaCoordenadas(gDTO);
		if(gCDTO.isEmpty())
			hasCoordsDeleted = true;
		
		for(GeocercaDispositivoDTO y : gDDTO)
		{
			geocercaDispositivoDAO.delete(y);
		}
		
		gDDTO = geocercaDispositivoDAO.buscarDispositivosEnGeocerca(gDTO);
		if(gDDTO.isEmpty())
			hasDevicesDeleted = true;
		
		hasMessagesDeleted = geocercaNotificacionDAO.eliminaRelacionGeocercaNotificacionPorGeocerca(gDTO);
		
		if(gDTO != null && hasCoordsDeleted && hasDevicesDeleted && hasMessagesDeleted){
			geocercaDAO.delete(gDTO);
			
			gDTO = geocercaDAO.buscarGeocercaPorId(id);
			
			return gDTO == null ? Boolean.TRUE : Boolean.FALSE;
		}else{
			throw new NotFoundException("No se encontro la geocerca a eliminar");
		}
	}
	
	
	private String convertirCoordsParaTheGeom(Long tipo, List<Double[]> lista){
		String resultado = "";
		if(tipo != 1){
			for(int i = 0; i < lista.size();i++){
				resultado += lista.get(i)[0]+","+lista.get(i)[1];
				
				if(lista.size() != i+1)
					resultado +=",";
			}
		}else{
			int radius = (int) Math.round(lista.get(1)[1]);
			resultado = (lista.get(0)[0])+","+lista.get(0)[1]+","+(radius)+",0.005";
		}
		
		return resultado;
	}
	
	private Map configurarInformacionSpatial(List<Double[]> listCoords){
		String headers[] = {"1,1003,1", "1,1005,"};
		String config = "";
		int elementCount = 0;
		boolean hasInnerForms = false;
		Map<String, Object> res = new HashMap<String,Object>();
		List<Double[]> cleanListCoords = new ArrayList<Double[]>();
		
		for(int i=0;i<listCoords.size();i++){
			if(listCoords.get(i)[0] == -1 && listCoords.get(i)[1] == -1){
				//config += ","+String.valueOf((cleanListCoords.size()*2)-1)+ ",2003,1";
				config += ","+String.valueOf((cleanListCoords.size()*2)-1)+ ",2,1";
				elementCount++;
				hasInnerForms = true;
			}else{
				cleanListCoords.add(listCoords.get(i));
			}
		}
		
		if(hasInnerForms){
			config = headers[1]+String.valueOf(elementCount+1) + ",1,2,1" +config;
		}else{
			config = headers[0];
		}
		
		res.put("config", config);
		res.put("coords", cleanListCoords);
		
		return res;
	}
	
	private Long validaGeocercaGrupoRelacion(GeocercaDTO gDTO){
		List<GeocercaDispositivoDTO> listaGdDTO = geocercaDispositivoDAO.buscarDispositivosEnGeocerca(gDTO);
		if(!listaGdDTO.isEmpty()){
			for(GeocercaDispositivoDTO gdDTO:listaGdDTO){
				if(gdDTO.getIdGrupo() != null)
					return gdDTO.getIdGrupo().getIdGrupo();
			}
		}
		return 0L;
	}
	
	private NotificacionDTO generaNotificacionParaGeocerca(Long idConfig, Long idTipoEnvio, GeocercaVO gVO, GeocercaDTO idGeocerca) throws ParseException{
		NotificacionVO nVO = new NotificacionVO();
		
		ConfigNotificaGeocercaDTO cNGDTO = configNotificaGeocercaDAO.buscaCatalogoPorId(idConfig);
		
		nVO.setDispositivos(gVO.getDispositivos() != null ? gVO.getDispositivos() : new ArrayList<DispoGeocercaVO>());
		nVO.setFecha(new SimpleDateFormat("DD/MM/YYYY hh:mm").format(new Date()));
		nVO.setGrupo(gVO.getGrupo());
		nVO.setId(-1L);
		nVO.setIsNow(Boolean.FALSE);
		
		nVO.setTipo(gVO.getNotificacion().getTipoEnvio());
		
		nVO.setNombre(cNGDTO.getNbTitulo());
		nVO.setMensaje(cNGDTO.getTxMensaje());
		
		return notificacionService.guardarNotificacion(nVO);
	}
}
