package mx.com.teclo.smm.persistencia.vo.evento;

import java.util.List;

public class TCDispositivoDiarioVO {
	private List<TCEventoVO> datos;

	public List<TCEventoVO> getDatos() {
		return datos;
	}

	public void setDatos(List<TCEventoVO> datos) {
		this.datos = datos;
	}
}
