package mx.com.teclo.smm.util.enumerados;

public enum TipoNotificacionEnum {
	PUSH(1L, "Push"), SMS(2L, "Mensaje de Texto"), MAIL(3L, "Correo");
	
	private Long idCat;
	private String nombreCat;
	
	private TipoNotificacionEnum(Long idCat, String nombreCat){
		this.idCat = idCat;
		this.nombreCat = nombreCat;
	}
	
	public Long getIdCat(){
		return this.idCat;
	}
	public void setIdCar(Long idCat){
		this.idCat = idCat;
	}
	public String getNombreCat(){
		return this.nombreCat;
	}
	public void setNombreCat(String nombreCat){
		this.nombreCat = nombreCat;
	}
	
	public static TipoNotificacionEnum getTipoNotificacionEnumPorId(Long id){
		TipoNotificacionEnum[] listaEnum = TipoNotificacionEnum.values();
		for(TipoNotificacionEnum tnEnum: listaEnum){
			if(tnEnum.getIdCat() == id)  
				return tnEnum; 
		}
		return null;
	}
}
