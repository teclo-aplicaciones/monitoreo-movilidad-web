package mx.com.teclo.smm.persistencia.hibernate.dao.grupo;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.grupo.GrupoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.grupo.GrupoDispositivoDTO;

public interface GrupoDispositivoDAO extends BaseDAO<GrupoDispositivoDTO> {
	public Long buscarSiguienteValor();
	public List<GrupoDispositivoDTO> buscarTodoDispositivosEnGrupos();
	public List<GrupoDispositivoDTO> buscarDispositivosEnGrupo(GrupoDTO grupo);
	public List<DispositivoDTO> buscarDispositivosPorGrupoId(Long idGrupo);
	public GrupoDispositivoDTO buscarGrupoDeDispositivo(DispositivoDTO idDispositivo);
	public List<GrupoDispositivoDTO> buscarGruposPorDispositivo(Long idDispositivo);
	public List<DispositivoDTO> buscarDispositivosSinGrupo();
	public Boolean eliminarRelacionGrupo(Long idGrupo);
}
