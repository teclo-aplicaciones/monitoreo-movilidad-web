package mx.com.teclo.smm.api.rest.evento;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.com.teclo.arquitectura.ortogonales.exception.NotFoundException;
import mx.com.teclo.arquitectura.ortogonales.responsehttp.ConflictHttpResponse;
import mx.com.teclo.arquitectura.ortogonales.responsehttp.OKHttpResponse;
import mx.com.teclo.smm.negocio.service.evento.EventoService;
import mx.com.teclo.smm.persistencia.vo.catalogo.CatalogoVO;
import mx.com.teclo.smm.persistencia.vo.dispositivo.DispositivoVO;
import mx.com.teclo.smm.persistencia.vo.evento.EventoVO;
import mx.com.teclo.smm.persistencia.vo.evento.EventosTablasHistoricoVO;
import mx.com.teclo.smm.persistencia.vo.evento.TCDispositivoDiarioVO;
import mx.com.teclo.smm.persistencia.vo.evento.TCDispositivosDiariosVO;
import mx.com.teclo.smm.persistencia.vo.evento.TCDispositivosVO;
import mx.com.teclo.smm.persistencia.vo.evento.TCEventoVO;
import mx.com.teclo.smm.persistencia.vo.evento.TCEventosDiariosVO;
import mx.com.teclo.smm.persistencia.vo.evento.TCGrupoVO;

@RestController
public class EventoRestController {

	@Autowired 
	private EventoService eventoService;
	
	@RequestMapping(value="/eventos", method=RequestMethod.GET)
	public ResponseEntity<List<EventoVO>> consultaEventos(){
		
		List<EventoVO> eventoVO = eventoService.consultarEventos();
		return new ResponseEntity<List<EventoVO>>(eventoVO, HttpStatus.OK);
	}
	
	@RequestMapping(value="/evento/total/hist", method=RequestMethod.GET)
	@PreAuthorize("hasAnyAuthority('EVENTO_TIPOS')")
	public ResponseEntity<List<TCGrupoVO>> consultarTiposEventos(@RequestParam(name="idMes") Long idMes){
		
		List<TCGrupoVO> list = eventoService.consultarTiposEventosHistoricos(idMes);
		return new ResponseEntity<List<TCGrupoVO>>(list, HttpStatus.OK);
		
	}
	
	@RequestMapping(value="/evento/dispo/hist", method=RequestMethod.GET)
	//@PreAuthorize("hasAnyAuthority('EVENTO_TIPOS')")
	public ResponseEntity<List<TCDispositivosVO>> consultarEventosPorDispositivo(@RequestParam(name="idMes") Long idMes){
		
		List<TCDispositivosVO> list = eventoService.consultarEventosDispositivoHistorico(idMes);
		return new ResponseEntity<List<TCDispositivosVO>>(list, HttpStatus.OK);
		
	}
	
	@RequestMapping(value="/evento/dispo/graph", method=RequestMethod.GET)
	//@PreAuthorize("hasAnyAuthority('EVENTO_TIPOS')")
	public ResponseEntity<List<TCEventoVO>> consultarGraficaEventosPorDispositivo(@RequestParam(name="idDispo") Long idDispo, @RequestParam(name="idMes") Long idMes){
		
		List<TCEventoVO> list = eventoService.consultarGraficaEventosDispositivoHistorico(idDispo, idMes);
		return new ResponseEntity<List<TCEventoVO>>(list, HttpStatus.OK);
		
	}
	
	
	
	@RequestMapping(value="/evento/anual/hist", method=RequestMethod.GET)
	//@PreAuthorize("hasAnyAuthority('EVENTO_TIPOS')")
	public ResponseEntity<List<Map<String, Long>>> consultarEventosAnual(@RequestParam(name="idGrupo") Long idGrupo, @RequestParam(name="idDispo") Long idDispo, @RequestParam(name="idAnio") Long idAnio){
		
		List<Map<String, Long>> list = eventoService.consultarEventosAnualesHistoricos(idGrupo, idDispo, idAnio);
		return new ResponseEntity<List<Map<String, Long>>>(list, HttpStatus.OK);
		
	}
	
	@RequestMapping(value="/evento/total/dia", method=RequestMethod.GET)
	//@PreAuthorize("hasAnyAuthority('EVENTO_TIPOS')")
	public ResponseEntity<TCEventosDiariosVO> consultarTiposEventosDiario() throws OKHttpResponse, ConflictHttpResponse, NotFoundException{
		 
		TCEventosDiariosVO list = eventoService.consultarTiposEventosDiarios();
		return new ResponseEntity<TCEventosDiariosVO>(list, HttpStatus.OK);
		
	}
	
	@RequestMapping(value="/evento/dispositivo/dia", method=RequestMethod.GET)
	//@PreAuthorize("hasAnyAuthority('EVENTO_TIPOS')")
	public ResponseEntity<TCDispositivoDiarioVO> consultarEventosDispositivoDiario(@RequestParam(name="idDispo")Long idDispositivo) throws OKHttpResponse, ConflictHttpResponse, NotFoundException{
		 
		TCDispositivoDiarioVO list = eventoService.consultarEventosDispositivoDiarios(idDispositivo);
		return new ResponseEntity<TCDispositivoDiarioVO>(list, HttpStatus.OK);
	}
	
	
	@RequestMapping(value="/evento/dispositivos/dia", method=RequestMethod.GET)
	//@PreAuthorize("hasAnyAuthority('EVENTO_TIPOS')")
	public ResponseEntity<TCDispositivosDiariosVO> consultarDispositivosDiarios() throws OKHttpResponse, ConflictHttpResponse, NotFoundException{
		 
		TCDispositivosDiariosVO list = eventoService.consultarDispositivosDiarios();
		return new ResponseEntity<TCDispositivosDiariosVO>(list, HttpStatus.OK);
	}
	
	
	@RequestMapping(value="/evento/hoy", method=RequestMethod.GET)
	@PreAuthorize("hasAnyAuthority('EVENTO_TIPOS_HOY')")
	public ResponseEntity<List<EventosTablasHistoricoVO>> consultarTiposEventosHoy(@RequestParam(name="idDispositivo") Long idDispositivo, @RequestParam(name="idGrupo") Long idGrupo) throws OKHttpResponse, ConflictHttpResponse{

		List<EventosTablasHistoricoVO> list = eventoService.consultarTiposEventosHoy(idGrupo,idDispositivo);
		return new ResponseEntity<List<EventosTablasHistoricoVO>>(list, HttpStatus.OK);
		
	}
	
	@RequestMapping(value="/evento/actual", method=RequestMethod.GET)
	@PreAuthorize("hasAnyAuthority('EVENTO_TIPOS_HOY_DISP')")
	public ResponseEntity<List<CatalogoVO>> consultarDispositivosEventosHoy(){
		List<CatalogoVO> list = eventoService.consultarTiposEventosDispoActuales();
		return new ResponseEntity<List<CatalogoVO>>(list, HttpStatus.OK);
	}

	@RequestMapping(value="/evento/dispositivo", method=RequestMethod.GET)
	@PreAuthorize("hasAnyAuthority('EVENTO_TIPOS_DISP')")
	public ResponseEntity<List<EventosTablasHistoricoVO>> consultarTiposEventosDispositivo(@RequestParam(name="idDispositivo") Long idDispositivo, @RequestParam(name="fecha") String fecha) throws OKHttpResponse, ConflictHttpResponse{
		
		List<EventosTablasHistoricoVO> list = eventoService.consultarTiposEventosDispositivo(idDispositivo, fecha);
		return new ResponseEntity<List<EventosTablasHistoricoVO>>(list, HttpStatus.OK);
		
	}

}