package mx.com.teclo.smm.api.rest.componenteweb;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.com.teclo.arquitectura.ortogonales.exception.NotFoundException;
import mx.com.teclo.smm.negocio.service.componenteweb.ComponenteWebService;
import mx.com.teclo.smm.persistencia.vo.componenteweb.ComponenteWebVO;

@RestController
public class ComponenteWebRestController {

	@Autowired
	private ComponenteWebService componenteWebService;

	@RequestMapping("/catalogoComponenteWeb")
	public ResponseEntity<List<ComponenteWebVO>> catalogoComponenteWeb(@RequestParam("menuId") Long menuId) throws NotFoundException {
		List<ComponenteWebVO> x = new ArrayList<ComponenteWebVO>();
		x = componenteWebService.obtenerComponenteWeb(menuId);
		return new ResponseEntity<List<ComponenteWebVO>>(x, HttpStatus.OK);
	}

}
