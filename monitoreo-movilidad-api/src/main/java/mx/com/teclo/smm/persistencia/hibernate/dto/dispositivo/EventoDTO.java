package mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo;

import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@Table(name="TMM012C_CT_EVENTOS")
public class EventoDTO implements Serializable{

	private static final long serialVersionUID = 5769932718175270625L;
	
	@Id
	@GeneratedValue(generator="id_evento_generator",strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="id_evento_generator", sequenceName = "SQTMM012C_CT_EVENTOS",allocationSize=1)
	@Column(name="ID_EVENTO")
	private Long idEvento;
	@Column(name="ID_EMPRESA")
	private long idEmpresa;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_TIPO_EVENTO")
	private TipoEventoDTO idTipoEvento;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_SUBTIPO_DISPOSITIVO")
	private SubTipoDispositivoDTO idSubtipoDispositivo;
	@Column(name="CD_EVENTO")
	private String cdEvento;
	@Column(name="NB_EVENTO")
	private String nbEvento;
	@Column(name="TX_EVENTO")
	private String txEvento;
	@Column(name="ST_ACTIVO")
	private Boolean stActivo;
	@Column(name="FH_CREACION")
	private Date fhCreacion;
	@Column(name="ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name="FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name="ID_USR_MODIFICA")
	private Long idUsrModifica;
	@Column(name="TX_PARAMETRO")
	private String txParametro;
	
	
	
	public String getTxParametro() {
		return txParametro;
	}
	public void setTxParametro(String txParametro) {
		this.txParametro = txParametro;
	}
	public Long getIdEvento() {
		return idEvento;
	}
	public void setIdEvento(Long idEvento) {
		this.idEvento = idEvento;
	}
	public TipoEventoDTO getIdTipoEvento() {
		return idTipoEvento;
	}
	public void setIdTipoEvento(TipoEventoDTO idTipoEvento) {
		this.idTipoEvento = idTipoEvento;
	}
	public SubTipoDispositivoDTO getIdSubtipoDispositivo() {
		return idSubtipoDispositivo;
	}
	public void setIdSubtipoDispositivo(SubTipoDispositivoDTO idSubtipoDispositivo) {
		this.idSubtipoDispositivo = idSubtipoDispositivo;
	}
	public String getCdEvento() {
		return cdEvento;
	}
	public void setCdEvento(String cdEvento) {
		this.cdEvento = cdEvento;
	}
	public String getNbEvento() {
		return nbEvento;
	}
	public void setNbEvento(String nbEvento) {
		this.nbEvento = nbEvento;
	}
	public String getTxEvento() {
		return txEvento;
	}
	public void setTxEvento(String txEvento) {
		this.txEvento = txEvento;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
	public long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
}
