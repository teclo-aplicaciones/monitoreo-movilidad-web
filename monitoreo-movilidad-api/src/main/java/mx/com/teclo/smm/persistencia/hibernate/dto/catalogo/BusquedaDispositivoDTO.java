package mx.com.teclo.smm.persistencia.hibernate.dto.catalogo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TMM064C_CT_BUS_DISPOSITIVO")
public class BusquedaDispositivoDTO  implements Serializable{


	/**
	 *
	 */
	private static final long serialVersionUID = 3649445260108579494L;


	@Id
	@Column(name="ID_BUSQUEDA_DISPOSITIVO")
	private Long idBusquedaDispositivo;

	@Column(name="NB_BUSQUEDA_DISPOSITIVO")
	private String nbBusquedaDispositivo;
	@Column(name="TX_BUSQUEDA_DISPOSITIVO")
	private String txBusquedaDispositivo;
	@Column(name="ST_ACTIVO")
	private Boolean stActivo;
	@Column(name="FH_CREACION")
	private Date fhCreacion;
	@Column(name="ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name="FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name="ID_USR_MODIFICA")
	private Long idUsrModifica;

	public Long getIdBusquedaDispositivo() {
		return idBusquedaDispositivo;
	}
	public void setIdBusquedaDispositivo(Long idBusquedaDispositivo) {
		this.idBusquedaDispositivo = idBusquedaDispositivo;
	}
	public String getNbBusquedaDispositivo() {
		return nbBusquedaDispositivo;
	}
	public void setNbBusquedaDispositivo(String nbBusquedaDispositivo) {
		this.nbBusquedaDispositivo = nbBusquedaDispositivo;
	}
	public String getTxBusquedaDispositivo() {
		return txBusquedaDispositivo;
	}
	public void setTxBusquedaDispositivo(String txBusquedaDispositivo) {
		this.txBusquedaDispositivo = txBusquedaDispositivo;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}


}