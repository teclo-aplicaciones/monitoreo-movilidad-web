package mx.com.teclo.smm.persistencia.hibernate.dao.catalogo;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.catalogo.BusquedaDispositivoDTO;

@SuppressWarnings("unchecked")
@Repository("busquedaDAO")
public class BusquedaDisposoitivoDAOImpl extends BaseDAOImpl<BusquedaDispositivoDTO> implements BusquedaDispositivoDAO{

	@Override
	@Transactional(readOnly = true)
	public List<BusquedaDispositivoDTO> obtenerCatalogos() {
		Criteria query = getCurrentSession().createCriteria(BusquedaDispositivoDTO.class);
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));

		return query.list();
	}

}
