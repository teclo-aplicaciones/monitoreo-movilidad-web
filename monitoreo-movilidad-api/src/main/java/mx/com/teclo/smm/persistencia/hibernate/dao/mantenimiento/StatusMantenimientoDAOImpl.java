package mx.com.teclo.smm.persistencia.hibernate.dao.mantenimiento;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.mantenimiento.StatusMantenimeintoDTO;

@SuppressWarnings("unchecked")
@Repository("StatusMantenimientoDAO")
public class StatusMantenimientoDAOImpl extends BaseDAOImpl<StatusMantenimeintoDTO> implements StatusMantenimientoDAO{


	@Override
	@Transactional(readOnly = true)
	public StatusMantenimeintoDTO obtenerStatusMantenimientoPorId(Long id) {
		Criteria query = getCurrentSession().createCriteria(StatusMantenimeintoDTO.class);
		query.add(Restrictions.eq("idStatusMantenimiento", id));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return (StatusMantenimeintoDTO)query.uniqueResult();
	}
}
