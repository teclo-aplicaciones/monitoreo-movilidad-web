package mx.com.teclo.smm.persistencia.hibernate.dao.grupo;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.grupo.TipoGrupoDTO;

@Repository("tipoGrupoDAO")
public class TipoGrupoDAOImpl extends BaseDAOImpl<TipoGrupoDTO> implements TipoGrupoDAO{

	@Override
	public TipoGrupoDTO buscarTipoGrupoPorId(Long idTipoGrupo){
		Criteria query = getCurrentSession().createCriteria(TipoGrupoDTO.class);
		query.add(Restrictions.eq("idTipoGrupo", idTipoGrupo));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		
		//idEmpresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));		
		
		return (TipoGrupoDTO) query.uniqueResult();
	}
}
