package mx.com.teclo.smm.persistencia.hibernate.dao.mantenimiento;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.mantenimiento.TipoMantenimeintoDTO;

@SuppressWarnings("unchecked")
@Repository("TipoMantenimientoDAO")
public class TipoMantenimientoDAOImpl extends BaseDAOImpl<TipoMantenimeintoDTO> implements TipoMantenimientoDAO{

	@Override
	@Transactional(readOnly = true)
	public List<TipoMantenimeintoDTO> obtenerTipoMantenimeinto(){
		Criteria query = getCurrentSession().createCriteria(TipoMantenimeintoDTO.class);
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return query.list();
	}

	@Override
	@Transactional(readOnly = true)
	public TipoMantenimeintoDTO obtenerTipoMantenimeintoPorId(Long id) {
		Criteria query = getCurrentSession().createCriteria(TipoMantenimeintoDTO.class);
		query.add(Restrictions.eq("idTipoMantenimiento", id));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return (TipoMantenimeintoDTO)query.uniqueResult();
	}
}
