package mx.com.teclo.smm.persistencia.vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

public class JsonObjectVO {
	
	private String id;
	private String nombre;
	private Map<String, String> param = new HashMap<String, String>();
	private List<Map<String, String>> datos = new ArrayList<Map<String, String>>();
	
	@JsonGetter("id")
	public String getId()
	{ return id; }
	
	@JsonSetter("id")
	public void setId(String id)
	{ this.id = id; }
	
	@JsonGetter("nombre")
	public String getNombre()
	{ return nombre; }
	
	@JsonSetter("nombre")
	public void setNombre(String nombre)
	{ this.nombre = nombre; }
	
	@JsonGetter("param")
	public Map<String, String> getParam()
	{ return param; }
	
	@JsonSetter("param")
	public void setParam(String parame, String value)
	{ 
		//Map<String, String> newReg = new HashMap<String, String>();
		param.put(parame, value);
		//this.param.add(newReg);
	}
	
	@JsonGetter("datos")
	public List<Map<String, String>> getDatos()
	{ return datos; }
	
	@JsonSetter("datos")
	public void setDatos(String param, String value)
	{ 
		Map<String, String> newReg = new HashMap<String, String>();
		newReg.put(param, value);
		this.datos.add(newReg); 
	}
}
