package mx.com.teclo.smm.persistencia.hibernate.dto.geocerca;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table(name = "TMM013D_GL_GEOCERCAS")
public class GeocercaDTO implements Serializable{

	/**/
	private static final long serialVersionUID = 3684549851520048062L;

	@Id
	@Column(name="ID_GEOCERCA")
	private Long idGeocerca;
	@Column(name="ID_EMPRESA")
	private long idEmpresa;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_TIPO_GEOCERCAS")
	private TipoGeocercaDTO idTipoGeocercas;
	@Column(name="CD_GEOCERCAS")
	private String cdGeocercas;
	@Column(name="NB_GEOCERCA")
	private String nbGeocerca;
	/*@Column(name="THE_GEOM")
	private Polygon theGeom;*/
	@Column(name="TX_GEOCERCA")
	private String txGeocerca;
	@Column(name="CD_COLOR")
	private String cdColor;
	@Column(name="ST_ACTIVO")
	private Boolean stActivo;
	@Column(name="FH_CREACION")
	private Date fhCreacion;
	@Column(name="ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name="FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name="ID_USR_MODIFICA")
	private Long idUsrModifica;
	
	public Long getIdGeocerca() {
		return idGeocerca;
	}
	public void setIdGeocerca(Long idGeocerca) {
		this.idGeocerca = idGeocerca;
	}
	public TipoGeocercaDTO getIdTipoGeocercas() {
		return idTipoGeocercas;
	}
	public void setIdTipoGeocercas(TipoGeocercaDTO idTipoGeocercas) {
		this.idTipoGeocercas = idTipoGeocercas;
	}
	public String getCdGeocercas() {
		return cdGeocercas;
	}
	public void setCdGeocercas(String cdGeocercas) {
		this.cdGeocercas = cdGeocercas;
	}
	public String getNbGeocerca() {
		return nbGeocerca;
	}
	public void setNbGeocerca(String nbGeocerca) {
		this.nbGeocerca = nbGeocerca;
	}
	public String getTxGeocerca() {
		return txGeocerca;
	}
	public void setTxGeocerca(String txGeocerca) {
		this.txGeocerca = txGeocerca;
	}
	public String getCdColor() {
		return cdColor;
	}
	public void setCdColor(String cdColor) {
		this.cdColor = cdColor;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
	public long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
}
