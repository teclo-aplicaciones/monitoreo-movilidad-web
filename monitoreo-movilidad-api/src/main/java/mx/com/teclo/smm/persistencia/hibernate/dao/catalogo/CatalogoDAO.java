package mx.com.teclo.smm.persistencia.hibernate.dao.catalogo;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.catalogo.CatalogoDTO;


public interface CatalogoDAO extends BaseDAO<CatalogoDTO>{
	public List<CatalogoDTO> obtenerCatalogos();
	public CatalogoDTO activarDesactivarobtenerCatalogo(long id);
}
