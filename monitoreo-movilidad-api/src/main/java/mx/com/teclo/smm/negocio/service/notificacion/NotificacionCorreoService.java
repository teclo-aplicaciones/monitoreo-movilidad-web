package mx.com.teclo.smm.negocio.service.notificacion;

import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.NotificacionDTO;

public interface NotificacionCorreoService {
	public Boolean enviaNotificacionMailInmediata(String nTelefonico, NotificacionDTO notificacion);
	public Boolean enviaNotificacionMailVelocityInmediata(String nCorreo, NotificacionDTO notificacion);
}
