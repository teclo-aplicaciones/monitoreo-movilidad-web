package mx.com.teclo.smm.persistencia.hibernate.dao.perfil;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.perfil.PerfilDTO;

/**
 *  Copyright (c) 2018, Teclo Mexicana.
 *
 *  Descripcion					: UsuarioService
 *  Historial de Modificaciones	:
 *  Descripcion del Cambio 		: Creacion
 *  @author 					: fjmb
 *  @version 					: 1.0
 *  Fecha 						: 05/Diciembre/2018
 */

@SuppressWarnings("unchecked")
@Repository
public class PerfilDAOImpl extends BaseDAOImpl<PerfilDTO> implements PerfilDAO {

	@Override
	public PerfilDTO findProfileByPerfil(PerfilDTO perfilDTO) {
		PerfilDTO perfilDTORes = null;
		Criteria query = getCurrentSession().createCriteria(PerfilDTO.class);
 		query.createAlias("aplicacion", "aplicacion");

		query.add(Restrictions.eq("aplicacion.idAplicacion", perfilDTO.getAplicacion().getIdAplicacion()));
 		query.add(Restrictions.eq("cdPerfil", perfilDTO.getCdPerfil()));
 		query.add(Restrictions.eq("nbPerfil", perfilDTO.getNbPerfil()));
 		query.add(Restrictions.eq("txPerfil", perfilDTO.getTxPerfil()));
 		query.add(Restrictions.eq("stActivo", new Integer(1)));
 		query.add(Restrictions.eq("idUsrCreacion",perfilDTO.getIdUsrCreacion()));
 		query.add(Restrictions.eq("fhCreacion",perfilDTO.getFhCreacion()));
 		query.add(Restrictions.eq("idUsrModifica",perfilDTO.getIdUsrModifica()));
 		query.add(Restrictions.eq("fhModificacion",perfilDTO.getFhModificacion()));

		perfilDTORes = (PerfilDTO) query.uniqueResult();

		return perfilDTORes;
	}

	/*
	@Override
	public Long findNextValue(){
		Criteria query = getCurrentSession().createCriteria(PerfilDTO.class);
		query.setProjection(Projections.max("idPerfil"));
		Long id = (Long)query.uniqueResult();

		return id!= null ? id+1L:0L;
	}

	@Override
	public List<PerfilDTO> findProfilesByApp(String cdApplication) {
		ArrayList<PerfilDTO> profiles = null;
		Criteria query = getCurrentSession().createCriteria(PerfilDTO.class);
		query.createAlias("aplicacion", "aplicacion");
		query.add(Restrictions.eq("stActivo", new Integer(1)));
		query.add(Restrictions.eq("aplicacion.cdAplicacion",cdApplication));

		profiles = (ArrayList<PerfilDTO>) query.list();

		return profiles;
	}

	@Override
	public List<PerfilDTO> findAllProfilesByApp(String cdApplication) {
		ArrayList<PerfilDTO> profiles = null;
		Criteria query = getCurrentSession().createCriteria(PerfilDTO.class);
		query.createAlias("aplicacion", "aplicacion");
		query.add(Restrictions.eq("aplicacion.cdAplicacion",cdApplication));

		// idEmpresa
		//query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		//query.add(Restrictions.eq("aplicacion.idEmpresa", getIdEmpresa()));

		profiles = (ArrayList<PerfilDTO>) query.list();

		return profiles;
	}

	@Override
	public PerfilDTO findProfileById(Long id, String cdApplication, boolean isByApplication) {

		PerfilDTO perfilDTO = null;
		Criteria query = getCurrentSession().createCriteria(PerfilDTO.class);

		// Alias para alcanzar la aplicacion al que pertenece el perfil
 		query.createAlias("aplicacion", "aplicacion");

		query.add(Restrictions.eq("stActivo", new Integer(1)));
 		query.add(Restrictions.eq("idPerfil", id));

		if(isByApplication) {
			query.add(Restrictions.eq("aplicacion.cdAplicacion", cdApplication));
		}


		// idEmpresa
		//query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		//query.add(Restrictions.eq("aplicacion.idEmpresa", getIdEmpresa()));

		perfilDTO = (PerfilDTO) query.uniqueResult();

		return perfilDTO;
	}

	@Override
	public PerfilDTO findUniqueProfileById(Long id, String cdApplication, boolean isByApplication) {

		PerfilDTO perfilDTO = null;
		Criteria query = getCurrentSession().createCriteria(PerfilDTO.class);

		// Alias para alcanzar la aplicacion al que pertenece el perfil
 		query.createAlias("aplicacion", "aplicacion");

 		query.add(Restrictions.eq("idPerfil", id));

		if(isByApplication) {
			query.add(Restrictions.eq("aplicacion.cdAplicacion", cdApplication));
		}


		// idEmpresa
		//query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		//query.add(Restrictions.eq("aplicacion.idEmpresa", getIdEmpresa()));

		perfilDTO = (PerfilDTO) query.uniqueResult();

		return perfilDTO;
	}

*/

}