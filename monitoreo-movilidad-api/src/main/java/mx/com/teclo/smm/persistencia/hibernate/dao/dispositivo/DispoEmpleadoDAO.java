package mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispoEmpleadoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.empleado.EmpleadoDTO;

public interface DispoEmpleadoDAO extends BaseDAO<DispoEmpleadoDTO>{
	public List<DispoEmpleadoDTO> buscarRelacionDispoEmpleado();
	public List<EmpleadoDTO> buscarRelacionDispoEmpleadoUnicos();
	public DispoEmpleadoDTO buscarRelacionDispoEmpleadoPorDispositivo(DispositivoDTO dispoDTO);
	public List<DispoEmpleadoDTO> buscarRelacionDispoEmpleadoPorEmpleado(EmpleadoDTO empleadoDTO);
	public DispoEmpleadoDTO buscarRelacionIdDispoEmpleadoPorEmpleado(Long idEmpleadoDTO);
	public List<DispoEmpleadoDTO> buscarRelacionIdDisposEmpleadoPorEmpleado(Long idEmpleadoDTO);
}
