package mx.com.teclo.smm.persistencia.vo.dispositivo;

import java.io.Serializable;

public class DispositivoGuardarVO implements Serializable{

	private static final long serialVersionUID = -4136795425537363631L;
	
	private Long idDispositivo;
	private String nombre;
	private Long tipo;
	private Long subtipo;
	private Long marca;
	private Long modelo;
	private String imei;
	private String serie;
	private Long idEmpleado;
	private String txDispositivo;
	private String nuIp;
	private String nuCelularEmpresa;
	private String nuCelularPersonal;
	// extra
	private String nombreEmpleado;
	
	
	public String getNombreEmpleado() {
		return nombreEmpleado;
	}
	public void setNombreEmpleado(String nombreEmpleado) {
		this.nombreEmpleado = nombreEmpleado;
	}
	public Long getIdDispositivo() {
		return idDispositivo;
	}
	public void setIdDispositivo(Long idDispositivo) {
		this.idDispositivo = idDispositivo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Long getTipo() {
		return tipo;
	}
	public void setTipo(Long tipo) {
		this.tipo = tipo;
	}
	public Long getSubtipo() {
		return subtipo;
	}
	public void setSubtipo(Long subtipo) {
		this.subtipo = subtipo;
	}
	public Long getMarca() {
		return marca;
	}
	public void setMarca(Long marca) {
		this.marca = marca;
	}
	public Long getModelo() {
		return modelo;
	}
	public void setModelo(Long modelo) {
		this.modelo = modelo;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public Long getIdEmpleado() {
		return idEmpleado;
	}
	public void setIdEmpleado(Long idEmpleado) {
		this.idEmpleado = idEmpleado;
	}
	public String getTxDispositivo() {
		return txDispositivo;
	}
	public void setTxDispositivo(String txDispositivo) {
		this.txDispositivo = txDispositivo;
	}
	public String getNuIp() {
		return nuIp;
	}
	public void setNuIp(String nuIp) {
		this.nuIp = nuIp;
	}
	public String getNuCelularEmpresa() {
		return nuCelularEmpresa;
	}
	public void setNuCelularEmpresa(String nuCelularEmpresa) {
		this.nuCelularEmpresa = nuCelularEmpresa;
	}
	public String getNuCelularPersonal() {
		return nuCelularPersonal;
	}
	public void setNuCelularPersonal(String nuCelularPersonal) {
		this.nuCelularPersonal = nuCelularPersonal;
	}
	
	
}
