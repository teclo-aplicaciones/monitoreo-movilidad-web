package mx.com.teclo.smm.persistencia.hibernate.dao.logs;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.com.teclo.arquitectura.persistencia.comun.dao.BaseDaoHibernate;
import mx.com.teclo.smm.negocio.service.empresa.EmpresaService;
import mx.com.teclo.smm.persistencia.hibernate.dto.logs.PerfilLogsDTO;
import mx.com.teclo.smm.persistencia.vo.logs.ComboVO;

@SuppressWarnings("unchecked")
@Repository
public class PerfilLogsDAOImpl extends BaseDaoHibernate<PerfilLogsDTO> implements PerfilLogsDAO {


	@Autowired
	private EmpresaService empresaService;


	@Override
	public List<PerfilLogsDTO> busquedaPorPerfil(Long perfilId) {
		List<PerfilLogsDTO> lista = null;
		Criteria criteria = getCurrentSession().createCriteria(PerfilLogsDTO.class);
		criteria.createAlias("perfilDTO", "perfilDTO");
		criteria.createAlias("webLogsDTO", "webLogsDTO");
		criteria.add(Restrictions.eq("perfilDTO.idPerfil", perfilId));
		criteria.add(Restrictions.eq("webLogsDTO.stActivo", 1));

		//idEmpresa
		//criteria.add(Restrictions.eq("perfilDTO.idEmpresa", empresaService.getIdEmpresa()));

		lista = criteria.list();
		return lista;
	}

	@Override
	public List<ComboVO> consultaPerfilesNoAsignados(Long idLog, String cdAplicacion) {

		SQLQuery query;
		StringBuilder sql = new StringBuilder();

		sql.append("select p.id_perfil as comboValor, p.cd_perfil as comboEtiqueta from taq026c_se_perfiles p ");
		sql.append("inner join taq028c_se_aplicaciones app on app.id_aplicacion =p.id_aplicacion ");
		sql.append("where p.id_perfil not in (");
		sql.append("select p.id_perfil from taq057d_lg_perfil_logs pl ");
		sql.append("inner join taq026c_se_perfiles p on pl.id_perfil = p.id_perfil ");
		sql.append("inner join taq056c_lg_web_logs wl on wl.id_web_log = pl.id_web_log ");
		sql.append("inner join taq028c_se_aplicaciones app on app.id_aplicacion =p.id_aplicacion ");
		sql.append("where app.cd_aplicacion=:cdAplicacion and wl.id_web_log= :idLog) ");
		sql.append("and app.cd_aplicacion=:cdAplicacion");

		query = getSession().createSQLQuery(sql.toString());
		query.setLong("idLog", idLog);
		query.setString("cdAplicacion", cdAplicacion);

		query.addScalar("comboValor", StringType.INSTANCE).addScalar("comboEtiqueta", StringType.INSTANCE);
		return query.setResultTransformer(Transformers.aliasToBean(ComboVO.class)).list();

	}

	@Override
	public List<PerfilLogsDTO> perfilesAsignadosPorLog(Long idLog, String cdAplicacion) {

		List<PerfilLogsDTO> lista = null;
		Criteria criteria = getCurrentSession().createCriteria(PerfilLogsDTO.class);

		criteria.createAlias("perfilDTO", "perfilDTO");
		criteria.createAlias("perfilDTO.aplicacion", "aplicacionDTO");
		criteria.createAlias("webLogsDTO", "webLogsDTO");
		criteria.add(Restrictions.eq("webLogsDTO.idLogWeb", idLog));
		criteria.add(Restrictions.eq("aplicacionDTO.cdAplicacion", cdAplicacion));
		//criteria.add(Restrictions.eq("webLogsDTO.stActivo", 1));

		//idEmpresa
		//criteria.add(Restrictions.eq("perfilDTO.idEmpresa", empresaService.getIdEmpresa()));

		lista = criteria.list();
		return lista;

	}

	@Override
	public PerfilLogsDTO busquedaPorPerfil(Long perfilId, Long idLog) {
		PerfilLogsDTO perfilLogsDTO = null;
		Criteria criteria = getCurrentSession().createCriteria(PerfilLogsDTO.class);
		criteria.createAlias("perfilDTO", "perfilDTO");
		criteria.createAlias("perfilDTO.aplicacion", "aplicacionDTO");
		criteria.createAlias("webLogsDTO", "webLogsDTO");
		criteria.add(Restrictions.eq("perfilDTO.idPerfil", perfilId));
		criteria.add(Restrictions.eq("webLogsDTO.idLogWeb", idLog));
		//criteria.add(Restrictions.eq("webLogsDTO.stActivo", 1));

		//idEmpresa
		//criteria.add(Restrictions.eq("perfilDTO.idEmpresa", empresaService.getIdEmpresa()));

		perfilLogsDTO = (PerfilLogsDTO) criteria.uniqueResult();
		return perfilLogsDTO;
	}
}
