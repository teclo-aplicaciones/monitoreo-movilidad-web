package mx.com.teclo.smm.persistencia.hibernate.dto.componeteweb;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author sinuhe
 *
 */
@Entity
@Table(name = "TAQ053D_SE_COMPWEB_POLITICA")
public class CompWebPoliticaDTO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -8792816551203367147L;




	@Id
	@GeneratedValue(generator="id_compwebpolitica_generator",strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="id_compwebpolitica_generator", sequenceName = "SQTAQ053D_SE_COMP_POL",allocationSize=1)
	@Column(name = "ID_COMPWEB_POLITICA")
	private Long idCompWebPolitica;

	@ManyToOne
	@JoinColumn(name = "ID_USRPERMENU")
	private UsrPerMenuCompWebDTO idUsrPerMenuCompWebDTO;

	@ManyToOne
	@JoinColumn(name = "ID_COMPONENTE_WEB")
	private ComponenteWebDTO idComponenteWebDTO;

	@ManyToOne
	@JoinColumn(name = "ID_COMPWEB_ACCION")
	private ComponenteWebAccionDTO idComponenteWebAccionDTO;

	@ManyToOne
	@JoinColumn(name = "ID_GRP_COMPONENTE_WEB")
	private GrpComponenteWebDTO idGrpComponenteWebDTO;

	@Column(name = "ST_ACTIVO")
	private Boolean stActivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;




	public GrpComponenteWebDTO getIdGrpComponenteWebDTO() {
		return idGrpComponenteWebDTO;
	}
	public void setIdGrpComponenteWebDTO(GrpComponenteWebDTO idGrpComponenteWebDTO) {
		this.idGrpComponenteWebDTO = idGrpComponenteWebDTO;
	}
	public Long getIdCompWebPolitica() {
		return idCompWebPolitica;
	}
	public void setIdCompWebPolitica(Long idCompWebPolitica) {
		this.idCompWebPolitica = idCompWebPolitica;
	}
	public UsrPerMenuCompWebDTO getIdUsrPerMenuCompWebDTO() {
		return idUsrPerMenuCompWebDTO;
	}
	public void setIdUsrPerMenuCompWebDTO(UsrPerMenuCompWebDTO idUsrPerMenuCompWebDTO) {
		this.idUsrPerMenuCompWebDTO = idUsrPerMenuCompWebDTO;
	}
	public ComponenteWebDTO getIdComponenteWebDTO() {
		return idComponenteWebDTO;
	}
	public void setIdComponenteWebDTO(ComponenteWebDTO idComponenteWebDTO) {
		this.idComponenteWebDTO = idComponenteWebDTO;
	}
	public ComponenteWebAccionDTO getIdComponenteWebAccionDTO() {
		return idComponenteWebAccionDTO;
	}
	public void setIdComponenteWebAccionDTO(ComponenteWebAccionDTO idComponenteWebAccionDTO) {
		this.idComponenteWebAccionDTO = idComponenteWebAccionDTO;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
}
