package mx.com.teclo.smm.persistencia.hibernate.dao.mantenimiento;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.mantenimiento.BusqMantenimeintoDTO;

@SuppressWarnings("unchecked")
@Repository("BusqMantenimientoDAO")
public class BusqMantenimientoDAOImpl extends BaseDAOImpl<BusqMantenimeintoDTO> implements BusqMantenimientoDAO{

	@Override
	@Transactional(readOnly = true)
	public List<BusqMantenimeintoDTO> obtenerBusqMantenimeinto(){
		Criteria query = getCurrentSession().createCriteria(BusqMantenimeintoDTO.class);
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		query.addOrder(Order.asc("idBusqMantenimiento"));
		return query.list();
	}


}
