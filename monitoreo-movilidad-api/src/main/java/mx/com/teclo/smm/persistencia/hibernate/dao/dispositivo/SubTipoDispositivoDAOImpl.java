package mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.SubTipoDispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.TipoDispositivoDTO;

@SuppressWarnings("unchecked")
@Repository("subTipoDispositivoDAO")
public class SubTipoDispositivoDAOImpl extends BaseDAOImpl<SubTipoDispositivoDTO> implements SubTipoDispositivoDAO{

	@Override
	public List<SubTipoDispositivoDTO> buscarCatalogo(Long[] id){
		Criteria query = getCurrentSession().createCriteria(SubTipoDispositivoDTO.class);
		Disjunction multi = Restrictions.disjunction();
		if(id != null){
			for(int i = 0;i<id.length;i++){
				multi.add(Restrictions.eq("idSubTipoDispositivo", id[i])); 
			}
			query.add(multi);
		}
		multi.add(Restrictions.eq("stActivo", Boolean.TRUE));

		//idEempresa
		multi.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		
		return (List<SubTipoDispositivoDTO>) query.list();
	}
	
	@Override
	public List<SubTipoDispositivoDTO> buscarCatalogoPorTipo(TipoDispositivoDTO tdDTO){
		Criteria query = getCurrentSession().createCriteria(SubTipoDispositivoDTO.class);
		query.add(Restrictions.eq("idTipoDispositivo", tdDTO));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));

		//idEempresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		
		return (List<SubTipoDispositivoDTO>) query.list();
	}
	
	@Override
	public SubTipoDispositivoDTO buscarCatalogoPorTipoAndIdSubtipo(TipoDispositivoDTO tdDTO,Long idSubTipo){
		Criteria query = getCurrentSession().createCriteria(SubTipoDispositivoDTO.class);
		query.add(Restrictions.eq("idTipoDispositivo", tdDTO));
		query.add(Restrictions.eq("idSubTipoDispositivo",idSubTipo));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));

		//idEempresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		
		return (SubTipoDispositivoDTO) query.uniqueResult();
	}
	
	@Override
	public List<SubTipoDispositivoDTO> buscarCatalogoTodos(){
		Criteria query = getCurrentSession().createCriteria(SubTipoDispositivoDTO.class);		
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));

		//idEempresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		
		return (List<SubTipoDispositivoDTO>) query.list();
	}
}
