package mx.com.teclo.smm.persistencia.hibernate.dto.notificacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.grupo.GrupoDTO;

@Entity
@Table(name = "TMM024D_DP_DISP_NOTIF")
public class NotificaDispositivoDTO implements Serializable{
	
	private static final long serialVersionUID = 8479358336650625266L;
	
	@Id
	@Column(name="ID_DISP_NOTIF")
	private Long idDispNotif;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_DISPOSITIVO")
	private DispositivoDTO idDispositivo;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_NOTIFICACION")
	private NotificacionDTO idNotificacion;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_GRUPO")
	private GrupoDTO idGrupo;
	@Column(name="FH_ENVIO")
	private Date fhEnvio;
	@Column(name="FH_RECEPCION")
	private Date fhRecepcion;
	@Column(name="ST_ACTIVO")
	private Boolean stActivo;
	@Column(name="FH_CREACION")
	private Date fhCreacion;
	@Column(name="ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name="FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name="ID_USR_MODIFICA")
	private Long idUsrModifica;
	
	public Long getIdDispNotif() {
		return idDispNotif;
	}
	public void setIdDispNotif(Long idDispNotif) {
		this.idDispNotif = idDispNotif;
	}
	public DispositivoDTO getIdDispositivo() {
		return idDispositivo;
	}
	public void setIdDispositivo(DispositivoDTO idDispositivo) {
		this.idDispositivo = idDispositivo;
	}
	public NotificacionDTO getIdNotificacion() {
		return idNotificacion;
	}
	public void setIdNotificacion(NotificacionDTO idNotificacion) {
		this.idNotificacion = idNotificacion;
	}
	public GrupoDTO getIdGrupo() {
		return idGrupo;
	}
	public void setIdGrupo(GrupoDTO idGrupo) {
		this.idGrupo = idGrupo;
	}
	public Date getFhEnvio() {
		return fhEnvio;
	}
	public void setFhEnvio(Date fhEnvio) {
		this.fhEnvio = fhEnvio;
	}
	public Date getFhRecepcion() {
		return fhRecepcion;
	}
	public void setFhRecepcion(Date fhRecepcion) {
		this.fhRecepcion = fhRecepcion;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
}
