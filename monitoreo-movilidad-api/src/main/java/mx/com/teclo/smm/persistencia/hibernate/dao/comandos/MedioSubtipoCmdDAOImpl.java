package mx.com.teclo.smm.persistencia.hibernate.dao.comandos;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.MedioSubtipoCmdDTO;

@SuppressWarnings("unchecked")
@Repository("medioSubtipoCmdDAO")
public class MedioSubtipoCmdDAOImpl extends BaseDAOImpl<MedioSubtipoCmdDTO> implements MedioSubtipoCmdDAO{

	@Override
	@Transactional(readOnly = true)
	public MedioSubtipoCmdDTO obtenerMedioSubtipoCmd(Long id) {
		Criteria query = getCurrentSession().createCriteria(MedioSubtipoCmdDTO.class);	
		query.add(Restrictions.eq("idMedioSubtipoComando", id));	
		
		//idEmpresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));

		
		return (MedioSubtipoCmdDTO)query.uniqueResult();		
	}

	
	@Override
	@Transactional(readOnly = true)
	public MedioSubtipoCmdDTO obtenerMedioSubtipoCmdPorIdMedioIdSubtipoDispositivo(Long idMedio, Long idSubtipoDispositivo) {
		Criteria query = getCurrentSession().createCriteria(MedioSubtipoCmdDTO.class);	
		query.add(Restrictions.eq("idMedioComunicacionDTO.idMedio", idMedio));				
		query.add(Restrictions.eq("idSubTipoDispositivoDTO.idSubTipoDispositivo", idSubtipoDispositivo));
		
		//idEmpresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));

		
		return (MedioSubtipoCmdDTO)query.uniqueResult();
	}
}
