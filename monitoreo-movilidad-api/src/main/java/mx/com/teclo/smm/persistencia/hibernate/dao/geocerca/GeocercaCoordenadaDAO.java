package mx.com.teclo.smm.persistencia.hibernate.dao.geocerca;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.geocerca.GeocercaCoordenadaDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.geocerca.GeocercaDTO;

public interface GeocercaCoordenadaDAO extends BaseDAO<GeocercaCoordenadaDTO>{
	public Long buscarSiguenteValor();
	public List<GeocercaCoordenadaDTO> buscarRelacionGeocercaCoordenadas(GeocercaDTO gDTO);
	public Boolean eliminarRelacionGeocercaCoordenada(GeocercaDTO gDTO);
}
