 package mx.com.teclo.smm.persistencia.hibernate.dao.geocerca;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.geocerca.GeocercaDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.geocerca.GeocercaDispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.grupo.GrupoDTO;

public interface GeocercaDispositivoDAO extends BaseDAO<GeocercaDispositivoDTO>{
	public Long buscarSiguenteValor();
	public List<GeocercaDispositivoDTO> buscarTodoDispositivosEnGeocercas();
	public List<GeocercaDispositivoDTO> buscarDispositivosEnGeocerca(GeocercaDTO geocerca);
	public List<GeocercaDispositivoDTO> buscarDispositivosEnGeocerca(DispositivoDTO dispositivoDTO);
	public List<GeocercaDispositivoDTO> buscarDispositivosGeocerca(DispositivoDTO dispositivoDTO);
	public Boolean eliminarRelacionGeocercaDispositivo(GeocercaDTO geocerca);
}
 