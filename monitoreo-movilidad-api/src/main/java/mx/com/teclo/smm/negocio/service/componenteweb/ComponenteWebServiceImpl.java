package mx.com.teclo.smm.negocio.service.componenteweb;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.arquitectura.ortogonales.seguridad.vo.UsuarioFirmadoVO;
import mx.com.teclo.arquitectura.ortogonales.service.comun.UsuarioFirmadoService;
import mx.com.teclo.smm.persistencia.hibernate.dao.componenteweb.CompWebPoliticaDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.componenteweb.ComponenteWebDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.componenteweb.ComponenteWebGrupoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.componenteweb.UsrPerMenuCompWebDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.menu.MenuDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.perfil.PerfilMenuDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.componeteweb.CmpGrpCompWebDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.componeteweb.CompWebPoliticaDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.componeteweb.ComponenteWebDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.componeteweb.UsrPerMenuCompWebDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.menu.MenuDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.perfil.PerfilMenuDTO;
import mx.com.teclo.smm.persistencia.vo.componenteweb.ComponenteWebVO;

@Service
public class ComponenteWebServiceImpl implements ComponenteWebService {

	@Autowired
	ComponenteWebDAO componenteWebDAO;
	@Autowired
	UsrPerMenuCompWebDAO usrPerMenuCompWebDAO;
	@Autowired
	PerfilMenuDAO perfilMenuDAO;
	@Autowired
	ComponenteWebGrupoDAO componenteWebGrupoDAO;
	@Autowired
	MenuDAO menuDAO;
	@Autowired
	CompWebPoliticaDAO compWebPoliticaDAO;
	
	@Autowired
	private UsuarioFirmadoService usuarioFirmadoService;
	@Value("${app.config.codigo}")
	private String codeApplication;

	@Override
	@Transactional(readOnly = true)
	public List<ComponenteWebVO> obtenerComponenteWeb(Long menuId) {
		List<ComponenteWebVO> componenteWebVOs = new ArrayList<ComponenteWebVO>();		
		UsuarioFirmadoVO usuarioFirmadoVO = usuarioFirmadoService.getUsuarioFirmadoVO();
		MenuDTO menuDTO = menuDAO.findMenusById(menuId, codeApplication);

		/**
		 * Obtener componentes por PerfilMenu
		 */
		List<PerfilMenuDTO> perfilMenuDTOs = perfilMenuDAO.findProfilesByIdPerfil(usuarioFirmadoVO.getPerfilId());
		for (PerfilMenuDTO perfilMenuDTO : perfilMenuDTOs) {
			if (perfilMenuDTO.getMenu().getIdMenu() == menuDTO.getIdMenu()) {
				for (UsrPerMenuCompWebDTO usrPerMenuCompWebDTO : usrPerMenuCompWebDAO.obtenerUsrPerMenuCompWebPorPerfilMenu(perfilMenuDTO.getIdPerfilMenu())) {					
					List<CompWebPoliticaDTO> compWebPoliticaDTOs =  compWebPoliticaDAO.obtenerCompWebPoliticaPorUsrPerMenu(usrPerMenuCompWebDTO.getIdUsrPerMenu());				
					for(CompWebPoliticaDTO compWebPoliticaDTO : compWebPoliticaDTOs) {					
						for (CmpGrpCompWebDTO c : componenteWebGrupoDAO.obtenerComponenteGrupoWebPorId(compWebPoliticaDTO.getIdGrpComponenteWebDTO().getIdGrpComponenteWeb())) {
							ComponenteWebVO componenteWebVO = new ComponenteWebVO();
							componenteWebVO.setId(c.getIdComponenteWebDTO().getIdComponenteWeb());
							componenteWebVO.setNombre(c.getIdComponenteWebDTO().getNbComponenteWeb());
							componenteWebVO.setAccion(c.getIdComponenteWebAccionDTO().getCdComponenteWebAccion());
							componenteWebVOs.add(componenteWebVO);
						}
					}
					
				}
			}
		}
		
		/**
		 * Obtener componentes por Usuario
		 */
		for (UsrPerMenuCompWebDTO usrPerMenuCompWebDTO : usrPerMenuCompWebDAO.obtenerUsrPerMenuCompWebPorUsuario(usuarioFirmadoVO.getId(), menuDTO.getIdMenu())) {
			List<CompWebPoliticaDTO> compWebPoliticaDTOs = compWebPoliticaDAO.obtenerCompWebPoliticaPorUsrPerMenu(usrPerMenuCompWebDTO.getIdUsrPerMenu());
			for(CompWebPoliticaDTO compWebPoliticaDTO : compWebPoliticaDTOs) {
				ComponenteWebDTO componenteWebDTO = componenteWebDAO.obtenerComponenteWebPorId(compWebPoliticaDTO.getIdComponenteWebDTO().getIdComponenteWeb());
				
				/*
				 * Sobreescribir politicas por usuario
				 * Se busca el componente web y se reeplaza su accion
				 */
				boolean addComponentWeb = true;
				for(ComponenteWebVO c : componenteWebVOs) {
					if(c.getNombre().equals(componenteWebDTO.getNbComponenteWeb())) {
						c.setId(componenteWebDTO.getIdComponenteWeb());
						c.setNombre(componenteWebDTO.getNbComponenteWeb());
						c.setAccion(compWebPoliticaDTO.getIdComponenteWebAccionDTO().getCdComponenteWebAccion());
						addComponentWeb =false;
						break;
					}
				}
				
				// se agrega nuevo compoenete
				if(addComponentWeb) {
					ComponenteWebVO componenteWebVO = new ComponenteWebVO();
					componenteWebVO.setId(componenteWebDTO.getIdComponenteWeb());
					componenteWebVO.setNombre(componenteWebDTO.getNbComponenteWeb());
					componenteWebVO.setAccion(compWebPoliticaDTO.getIdComponenteWebAccionDTO().getCdComponenteWebAccion());
					componenteWebVOs.add(componenteWebVO);
				}	
			}
		}

		return new ArrayList<ComponenteWebVO>(new HashSet<ComponenteWebVO>(componenteWebVOs));
	}
	
}
