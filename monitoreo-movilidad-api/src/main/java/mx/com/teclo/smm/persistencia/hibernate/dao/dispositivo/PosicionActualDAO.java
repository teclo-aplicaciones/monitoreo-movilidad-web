package mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.PosicionActualDTO;

public interface PosicionActualDAO extends BaseDAO<PosicionActualDTO>{
	public List<PosicionActualDTO> obtenerCoordenadaActual(DispositivoDTO dispoDTO);
	public List<PosicionActualDTO> obtenerRegistrosHoy();
	public PosicionActualDTO obtenerRegistroHoyPorDispositivo(Long idDispo);
}
