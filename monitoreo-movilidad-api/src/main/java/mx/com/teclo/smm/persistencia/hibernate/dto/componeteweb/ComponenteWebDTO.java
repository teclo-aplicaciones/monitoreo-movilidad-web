package mx.com.teclo.smm.persistencia.hibernate.dto.componeteweb;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author sinuhe
 *
 */
@Entity
@Table(name = "TAQ051C_SE_COMPONENTE_WEB")
public class ComponenteWebDTO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -5082442332530262322L;

	@Id
	@GeneratedValue(generator="id_componenteweb_generator",strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="id_componenteweb_generator", sequenceName = "SQTAQ051C_SE_COMP_WEB",allocationSize=1)
	@Column(name = "ID_COMPONENTE_WEB")
	private Long idComponenteWeb;
	@Column(name = "NB_COMPONENTE_WEB")
	private String nbComponenteWeb;
	@Column(name = "NU_COMPONENTE_WEB_SUPERIOR")
	private Long nuComponenteWebSuperior;
	@Column(name = "ST_ACTIVO")
	private Boolean stActivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;

	public Long getIdComponenteWeb() {
		return idComponenteWeb;
	}
	public void setIdComponenteWeb(Long idComponenteWeb) {
		this.idComponenteWeb = idComponenteWeb;
	}
	public String getNbComponenteWeb() {
		return nbComponenteWeb;
	}
	public void setNbComponenteWeb(String nbComponenteWeb) {
		this.nbComponenteWeb = nbComponenteWeb;
	}

	public Long getNuComponenteWebSuperior() {
		return nuComponenteWebSuperior;
	}
	public void setNuComponenteWebSuperior(Long nuComponenteWebSuperior) {
		this.nuComponenteWebSuperior = nuComponenteWebSuperior;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
}
