package mx.com.teclo.smm.negocio.service.grupo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.arquitectura.ortogonales.exception.NotFoundException;
import mx.com.teclo.arquitectura.ortogonales.service.comun.UsuarioFirmadoService;
import mx.com.teclo.smm.negocio.service.empresa.EmpresaService;
import mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo.DispoEmpleadoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo.DispositivoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo.InformacionDispDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.empleado.EmpleadoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.grupo.GrupoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.grupo.GrupoDispositivoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.grupo.TipoGrupoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispoEmpleadoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.empleado.EmpleadoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.grupo.GrupoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.grupo.GrupoDispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.grupo.TipoGrupoDTO;
import mx.com.teclo.smm.persistencia.vo.JsonObjectVO;
import mx.com.teclo.smm.persistencia.vo.catalogo.CatalogoConSubtipoVO;
import mx.com.teclo.smm.persistencia.vo.catalogo.CatalogoVO;
import mx.com.teclo.smm.persistencia.vo.geocerca.DispoGeocercaVO;
import mx.com.teclo.smm.persistencia.vo.grupo.ConsultaGrupoVO;
import mx.com.teclo.smm.persistencia.vo.grupo.GrupoVO;
import mx.com.teclo.smm.util.enumerados.CatalogoGrupoEnum;

@Service
public class GrupoServiceImpl implements GrupoService{

	@Autowired
	GrupoDAO grupoDAO;

	@Autowired
	EmpleadoDAO empleadoDAO;

	@Autowired
	DispoEmpleadoDAO dispoEmpleadoDAO;

	@Autowired
	GrupoDispositivoDAO grupoDispositivoDAO;

	@Autowired
	DispositivoDAO dispositivoDAO;

	@Autowired
	TipoGrupoDAO tipoGrupoDAO;

	@Autowired
	InformacionDispDAO informacionDispDAO;

	@Autowired
	UsuarioFirmadoService usuarioFirmadoService;

	@Autowired
	EmpresaService empresaService;

	private static String JSON = "[{\"id\":\"100\",\"nombre\":\"Grupo Concesionaria\"},"
							   + " {\"id\":\"200\",\"nombre\":\"Grupo SSP\"},"
							   + " {\"id\":\"300\",\"nombre\":\"Grupo TECLO\"}]";

	List<JsonObjectVO> listaObject;

	@Override
	@Transactional(readOnly = true)
	public List<CatalogoVO> buscarGrupos()
	{
		List<CatalogoVO> listaGrupos = new ArrayList<CatalogoVO>();
		List<GrupoDTO>lista = grupoDAO.buscarGrupos();
		CatalogoVO catVO = null;

		for(GrupoDTO grupo:lista){
			catVO = new CatalogoVO();
			catVO.setId(grupo.getIdGrupo());
			catVO.setNombre(grupo.getNbGrupo());
			listaGrupos.add(catVO);
		}
		/*ObjectMapper objectMapper = new ObjectMapper();
		List<CatalogoVO> listaGrupos = new ArrayList<CatalogoVO>();

		try {
			listaObject = objectMapper.readValue(JSON, new TypeReference<List<JsonObjectVO>>(){});
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for(JsonObjectVO object: listaObject)
		{
			CatalogoVO catVo = new CatalogoVO();
			catVo.setId(Long.parseLong(object.getId()));
			catVo.setNombre(object.getNombre());
			listaGrupos.add(catVo);
		}*/

		return listaGrupos;
	}

	@Override
	@Transactional(readOnly = true)
	public List<CatalogoConSubtipoVO> buscarGruposConIdSubtipoDispositivo(){
		List<CatalogoConSubtipoVO> listaGrupos = new ArrayList<CatalogoConSubtipoVO>();
		List<GrupoDTO>lista = grupoDAO.buscarGrupos();
		CatalogoConSubtipoVO catVO = null;

		for(GrupoDTO grupo:lista){
			catVO = new CatalogoConSubtipoVO();
			catVO.setId(grupo.getIdGrupo());
			catVO.setNombre(grupo.getNbGrupo());
			listaGrupos.add(catVO);
		}
		/*ObjectMapper objectMapper = new ObjectMapper();
		List<CatalogoVO> listaGrupos = new ArrayList<CatalogoVO>();

		try {
			listaObject = objectMapper.readValue(JSON, new TypeReference<List<JsonObjectVO>>(){});
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for(JsonObjectVO object: listaObject)
		{
			CatalogoVO catVo = new CatalogoVO();
			catVo.setId(Long.parseLong(object.getId()));
			catVo.setNombre(object.getNombre());
			listaGrupos.add(catVo);
		}*/

		return listaGrupos;
	}

	@Override
	@Transactional
	public List<ConsultaGrupoVO> consultaGrupos(Long tipo, String valor) throws NotFoundException
	{
		EmpleadoDTO eDTO;
		DispositivoDTO dDTO;
		List<ConsultaGrupoVO> grupos = new ArrayList<ConsultaGrupoVO>();

		List<DispoEmpleadoDTO> listaDeDTO;
		List<GrupoDTO> listaGDTO = new ArrayList<GrupoDTO>();
		List<GrupoDispositivoDTO> listaGDDTO;

		if(CatalogoGrupoEnum.NOMBRE_GRUPO.getIdCat() == tipo){
			listaGDTO = grupoDAO.buscarGruposPorNombre(valor);
		}else if(CatalogoGrupoEnum.DISPOSITIVO.getIdCat() == tipo){
			listaGDDTO = grupoDispositivoDAO.buscarGruposPorDispositivo(Long.parseLong(valor));

			if(!listaGDDTO.isEmpty()){
				for(GrupoDispositivoDTO gDDTO : listaGDDTO){
					listaGDTO.add(gDDTO.getIdGrupo());
				}
			}else{
				throw new NotFoundException("No se encontro ningun grupo asociado al dispositivo");
			}
		}else if(CatalogoGrupoEnum.USUARIOS.getIdCat() == tipo){
			eDTO = empleadoDAO.buscarEmpleado(Long.parseLong(valor));
			listaDeDTO = dispoEmpleadoDAO.buscarRelacionDispoEmpleadoPorEmpleado(eDTO);
			if(listaDeDTO.isEmpty())
				throw new NotFoundException("No existe dispositivo(s) relacionados con el usuario.");

			for(DispoEmpleadoDTO edDTO : listaDeDTO){
				dDTO = edDTO.getIdDispositivo();
				listaGDDTO = grupoDispositivoDAO.buscarGruposPorDispositivo(dDTO.getIdDispositivo());
				for(GrupoDispositivoDTO gDDTO : listaGDDTO){
					listaGDTO.add(gDDTO.getIdGrupo());
				}
			}
			if(listaGDTO.isEmpty())
				throw new NotFoundException("No se encontro ningun grupo asociado al dispositivo");
		}


		for(GrupoDTO gDTO: listaGDTO)
		{
			ConsultaGrupoVO cgVO = new ConsultaGrupoVO();
			cgVO.setId(gDTO.getIdGrupo());
			cgVO.setNombre(gDTO.getNbGrupo());
			cgVO.setDescripcion(gDTO.getTxGrupo());
			cgVO.setTipo(gDTO.getIdTipoGrupo().getNbTipoGrupo());
			cgVO.setFechaCreacion(gDTO.getFhCreacion().getTime());
			grupos.add(cgVO);
		}

		return grupos;
	}

	@Override
	@Transactional(readOnly = true)
	public GrupoDTO buscarGrupoPorId(Long id)
	{
		/*List<GrupoVO> grupos = consultaGrupos(null, null);

		for(GrupoVO gVo:grupos){
			if(gVo.getId() == Long.parseLong(id))
			{
				return gVo;
			}
		}
		return null;*/
		return grupoDAO.buscarGrupoPorId(id);
	}

	@Override
	@Transactional(readOnly = true)
	public GrupoVO buscarGrupoParaActualizar(Long id)
	{
		GrupoDTO grupo = grupoDAO.buscarGrupoPorId(id);

		List<GrupoDispositivoDTO> listaGDDTO = grupoDispositivoDAO.buscarDispositivosEnGrupo(grupo);

		GrupoVO gVO = new GrupoVO();

		List<DispoGeocercaVO> listaDGVO = new ArrayList<DispoGeocercaVO>();

		gVO.setId(grupo.getIdGrupo());
		gVO.setNombre(grupo.getNbGrupo());
		gVO.setDescripcion(grupo.getTxGrupo());
		gVO.setTipo(grupo.getIdTipoGrupo().getIdTipoGrupo());

		for(GrupoDispositivoDTO gDDTO: listaGDDTO){
			DispoGeocercaVO dGVO = new DispoGeocercaVO();
			dGVO.setIdItem(gDDTO.getIdDispositivo().getIdDispositivo());
			dGVO.setTxtItem(gDDTO.getIdDispositivo().getNbDispositivo());
			dGVO.setSrItem(informacionDispDAO.buscarRelacionPorDispositivo(gDDTO.getIdDispositivo()).getNuSerie());
			listaDGVO.add(dGVO);
		}

		gVO.setDispositivos(listaDGVO);

		return gVO;
	}

	@Override
	@Transactional(readOnly = true)
	public Long[] obtenerDispositivosEnGrupo(Long idGrupo) {
		List<GrupoDispositivoDTO> listaDispos;
		GrupoDTO grupo = buscarGrupoPorId(idGrupo);

		if(grupo != null){
			listaDispos = grupoDispositivoDAO.buscarDispositivosEnGrupo(grupo);
		}else{
			listaDispos = grupoDispositivoDAO.buscarTodoDispositivosEnGrupos();
		}

		Long[] ids = new Long[listaDispos.size()];
		for (int i = 0; i < listaDispos.size(); i++) {
			ids[i] = listaDispos.get(i).getIdDispositivo().getIdDispositivo();
		}
		return ids;
	}

	@Override
	@Transactional
	public GrupoDTO guardarGrupo(GrupoVO grupoVO){

		GrupoDTO gDTO = new GrupoDTO();

		//Buscar id siguiente
		Long idGrupo = grupoDAO.buscarSiguienteValor();
		//Buscar el tipo y asignarlo
		TipoGrupoDTO idTipo = tipoGrupoDAO.buscarTipoGrupoPorId(grupoVO.getTipo());
		//Id de Usuario
		Long idUsuario = usuarioFirmadoService.getUsuarioFirmadoVO().getId();
		//Fecha de creacion
		Date creacion = new Date();

		gDTO.setIdGrupo(idGrupo);
		gDTO.setIdTipoGrupo(idTipo);
		gDTO.setNbGrupo(grupoVO.getNombre());
		gDTO.setTxGrupo(grupoVO.getDescripcion());
		gDTO.setStActivo(Boolean.TRUE);
		gDTO.setFhCreacion(creacion);
		gDTO.setIdUsrCreacion(idUsuario);
		gDTO.setFhModificacion(creacion);
		gDTO.setIdUsrModifica(idUsuario);
		gDTO.setIdEmpresa(empresaService.getIdEmpresa());
		grupoDAO.save(gDTO);

		return gDTO;
	}

	@Override
	@Transactional
	public Boolean guardarGrupoAsociado(GrupoDTO grupoDTO,GrupoVO grupoVO) {

		Long idGrupo = grupoDTO.getIdGrupo()+1;
		//Se agregan los dispositivos asociados
		for(DispoGeocercaVO dgVO: grupoVO.getDispositivos()){
			grupoDispositivoDAO.save(asignarDispositivosGrupo(dgVO,idGrupo,grupoDTO.getIdUsrCreacion()));
		}

		return Boolean.TRUE;
	}

	@Override
	@Transactional
	public Boolean actualizarGrupo(GrupoVO grupoVO){

		GrupoDTO gDTO = grupoDAO.buscarGrupoPorId(grupoVO.getId());

		//Buscar el tipo y asignarlo
		TipoGrupoDTO idTipo = tipoGrupoDAO.buscarTipoGrupoPorId(grupoVO.getTipo());
		//Id de Usuario
		Long idUsuario = usuarioFirmadoService.getUsuarioFirmadoVO().getId();
		//Fecha de modificacion
		Date modificacion = new Date();

		gDTO.setIdTipoGrupo(idTipo);
		gDTO.setNbGrupo(grupoVO.getNombre());
		gDTO.setTxGrupo(grupoVO.getDescripcion());
		gDTO.setFhModificacion(modificacion);
		gDTO.setIdUsrModifica(idUsuario);

		grupoDAO.saveOrUpdate(gDTO);

		//Eliminar las relaciones anteriores
		grupoDispositivoDAO.eliminarRelacionGrupo(gDTO.getIdGrupo());

		//Se agregan los dispositivos asociados
		for(DispoGeocercaVO dgVO: grupoVO.getDispositivos()){
			grupoDispositivoDAO.save(asignarDispositivosGrupo(dgVO, gDTO.getIdGrupo(), idUsuario));
		}

		return Boolean.TRUE;
	}

	@Override
	@Transactional
	public Boolean eliminarGrupo(Long idGrupo){

		GrupoDTO gDTO = grupoDAO.buscarGrupoPorId(idGrupo);

		Boolean before = gDTO.getStActivo();

		gDTO.setStActivo(Boolean.FALSE);

		grupoDAO.saveOrUpdate(gDTO);

		return before != gDTO.getStActivo() ? Boolean.TRUE : Boolean.FALSE;
	}


	private GrupoDispositivoDTO asignarDispositivosGrupo(DispoGeocercaVO dgVO, Long idGrupo, Long idUsuario){
		GrupoDispositivoDTO gDDTO = new GrupoDispositivoDTO();
		gDDTO.setIdDispGrupo(grupoDispositivoDAO.buscarSiguienteValor());
		gDDTO.setIdDispositivo(dispositivoDAO.buscarDispositivosPorId(dgVO.getIdItem()));
		gDDTO.setIdGrupo(grupoDAO.buscarGrupoPorId(idGrupo));
		gDDTO.setStActivo(Boolean.TRUE);
		gDDTO.setFhCreacion(new Date());
		gDDTO.setIdUsrCreacion(idUsuario);
		gDDTO.setFhModificacion(new Date());
		gDDTO.setIdUsrModifica(idUsuario);

		return gDDTO;
	}
}
