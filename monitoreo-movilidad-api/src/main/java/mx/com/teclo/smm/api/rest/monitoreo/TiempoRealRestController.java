package mx.com.teclo.smm.api.rest.monitoreo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.com.teclo.arquitectura.ortogonales.exception.NotFoundException;
import mx.com.teclo.smm.negocio.service.dispositivo.DispositivoService;
import mx.com.teclo.smm.negocio.service.empleado.EmpleadoService;
import mx.com.teclo.smm.negocio.service.geocerca.GeocercaService;
import mx.com.teclo.smm.negocio.service.grupo.GrupoService;
import mx.com.teclo.smm.persistencia.vo.catalogo.CatalogoConSubtipoVO;
import mx.com.teclo.smm.persistencia.vo.catalogo.CatalogoVO;
import mx.com.teclo.smm.persistencia.vo.dispositivo.DispositivoVO;
import mx.com.teclo.smm.persistencia.vo.monitoreo.DispositivoRTDetalleVO;
import mx.com.teclo.smm.util.enumerados.CatalogoMonitoreoEnum;

@RestController
public class TiempoRealRestController {

	@Autowired
	GrupoService grupoService;
	
	@Autowired
	GeocercaService geocercaService;
	
	@Autowired
	DispositivoService dispositivoService;
	
	@Autowired
	EmpleadoService empleadoService;
	
	private static final Log log = LogFactory.getLog(TiempoRealRestController.class);
	
	@RequestMapping("/buscarAgrupamiento")
	@PreAuthorize("hasAnyAuthority('BUSCA_CATALOGO_TIEMPO_REAL')")
	public ResponseEntity<List<CatalogoVO>> buscarCatalogo(@RequestParam("tipo")String tipo){
		List<CatalogoVO> x = new ArrayList<CatalogoVO>();
		
		if(Long.parseLong(tipo) == CatalogoMonitoreoEnum.GRUPOS.getIdCat()){
			x = grupoService.buscarGrupos();
		}else if(Long.parseLong(tipo) == CatalogoMonitoreoEnum.GEOCERCAS.getIdCat()){
			x = geocercaService.buscarGeocercas();
		}else if(Long.parseLong(tipo) == CatalogoMonitoreoEnum.DISPOSITIVOS.getIdCat()){
			x = dispositivoService.buscarDispositivo(null);
		}else if(Long.parseLong(tipo) == CatalogoMonitoreoEnum.USUARIOS.getIdCat()){
			x = empleadoService.buscarEmpleado();
		}
		
		return new ResponseEntity<List<CatalogoVO>>(x, HttpStatus.OK);
	}
	
	@RequestMapping("/buscarAgrupamientoConIdSubtipoDispositivo")
	@PreAuthorize("hasAnyAuthority('BUSCA_CATALOGO_TIEMPO_REAL')")
	public ResponseEntity<List<CatalogoConSubtipoVO>> buscarAgrupamiento(@RequestParam("tipo")String tipo){
		List<CatalogoConSubtipoVO> x = new ArrayList<CatalogoConSubtipoVO>();
		
		if(Long.parseLong(tipo) == CatalogoMonitoreoEnum.GRUPOS.getIdCat()){
			x = grupoService.buscarGruposConIdSubtipoDispositivo();
		}else if(Long.parseLong(tipo) == CatalogoMonitoreoEnum.GEOCERCAS.getIdCat()){
			x = geocercaService.buscarGeocercasConIdSubtipoDispositivo();
		}else if(Long.parseLong(tipo) == CatalogoMonitoreoEnum.DISPOSITIVOS.getIdCat()){
			x = dispositivoService.buscarDispositivoConIdSubtipoDispositivo(null);
		}else if(Long.parseLong(tipo) == CatalogoMonitoreoEnum.USUARIOS.getIdCat()){
			x = empleadoService.buscarEmpleadoConIdSubtipoDispositivo();
		}
		
		return new ResponseEntity<List<CatalogoConSubtipoVO>>(x, HttpStatus.OK);
	}
	
	@RequestMapping("/cargarDetalleDispositivo")
	@PreAuthorize("hasAnyAuthority('CARGA_DETALE_DISPO')")
	public ResponseEntity<DispositivoRTDetalleVO> cargarDetalleDispositivo(@RequestParam("id")Long id){
		DispositivoRTDetalleVO drtdVO = dispositivoService.cargarDetalleDispositivo(id);
		return new ResponseEntity<DispositivoRTDetalleVO>(drtdVO, HttpStatus.OK);
	}
	
	@RequestMapping("/localizar")
	@PreAuthorize("hasAnyAuthority('LOCALIZA_DISPO')")
	public ResponseEntity<List<DispositivoVO>> localizarTiempoReal(@RequestParam("id")Long[] ids)throws NotFoundException
	{
		/*int[] numbers = new int[ids.length];
		
		for(int i=0;i<ids.length;i++){
			numbers[i]=(int)ids[i];
		}*/
		log.info("Localiza");
		List<DispositivoVO> listaDispositivos = dispositivoService.buscarDispositivos(ids);
		if(listaDispositivos != null){
			return new ResponseEntity<List<DispositivoVO>>(listaDispositivos, HttpStatus.OK);	
		}else{
			throw new NotFoundException("No se encontro registros.");
		}
		
	}
}
