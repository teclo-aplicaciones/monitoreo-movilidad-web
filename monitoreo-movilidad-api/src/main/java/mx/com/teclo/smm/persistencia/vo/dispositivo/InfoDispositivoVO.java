package mx.com.teclo.smm.persistencia.vo.dispositivo;

public class InfoDispositivoVO {

	private Long idDispositivo;
	private Long idGrupo;	
	private String nbGrupo;	
	private String nbDispositivo;
	private String nbEmplead;
	private String nbUbicacion;
	private String fhRegistro;
	private boolean stActivo;
	
	public InfoDispositivoVO(Long idDispositivo, Long idGrupo, String nbGrupo, String nbDispositivo, String nbEmplead,
			String nbUbicacion, String fhRegistro, boolean stActivo) {
		super();
		this.idDispositivo = idDispositivo;
		this.idGrupo = idGrupo;
		this.nbGrupo = nbGrupo;
		this.nbDispositivo = nbDispositivo;
		this.nbEmplead = nbEmplead;
		this.nbUbicacion = nbUbicacion;
		this.fhRegistro = fhRegistro;
		this.stActivo = stActivo;
	}
	
	
	
	public InfoDispositivoVO() {
		// TODO Auto-generated constructor stub
	}
	public Long getIdDispositivo() {
		return idDispositivo;
	}
	public void setIdDispositivo(Long idDispositivo) {
		this.idDispositivo = idDispositivo;
	}
	public Long getIdGrupo() {
		return idGrupo;
	}
	public void setIdGrupo(Long idGrupo) {
		this.idGrupo = idGrupo;
	}
	public String getNbGrupo() {
		return nbGrupo;
	}
	public void setNbGrupo(String nbGrupo) {
		this.nbGrupo = nbGrupo;
	}
	public String getNbDispositivo() {
		return nbDispositivo;
	}
	public void setNbDispositivo(String nbDispositivo) {
		this.nbDispositivo = nbDispositivo;
	}
	public String getNbEmplead() {
		return nbEmplead;
	}
	public void setNbEmplead(String nbEmplead) {
		this.nbEmplead = nbEmplead;
	}
	public String getNbUbicacion() {
		return nbUbicacion;
	}
	public void setNbUbicacion(String nbUbicacion) {
		this.nbUbicacion = nbUbicacion;
	}
	public String getFhRegistro() {
		return fhRegistro;
	}
	public void setFhRegistro(String fhRegistro) {
		this.fhRegistro = fhRegistro;
	}
	public boolean isStActivo() {
		return stActivo;
	}
	public void setStActivo(boolean stActivo) {
		this.stActivo = stActivo;
	}
	
}
