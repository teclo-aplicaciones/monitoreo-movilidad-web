package mx.com.teclo.smm.persistencia.hibernate.dao.mantenimiento;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.mantenimiento.MantenimeintoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.mantenimiento.TipoMantenimeintoDTO;


public interface MantenimientoDAO extends BaseDAO<MantenimeintoDTO>{
	public List<MantenimeintoDTO> obtenerMantenimeinto();
	public MantenimeintoDTO obtenerMantenimeintoPorId(Long idMantenimientoVO);
	public List<MantenimeintoDTO> obtenerMantenimeintoPorIdDispositivo(DispositivoDTO dispositivoDTO);
	public List<MantenimeintoDTO> obtenerMantenimeintoPorTipoMantenimiento(TipoMantenimeintoDTO idTipoMantenimiento);
	public List<MantenimeintoDTO> obtenerMantenimeintoLikeNombre(String nombre);
	
}
