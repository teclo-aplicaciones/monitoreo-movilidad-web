package mx.com.teclo.smm.persistencia.hibernate.dto.logs;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.teclo.smm.persistencia.hibernate.dto.perfil.PerfilDTO;


@Entity
@Table(name="TAQ057D_LG_PERFIL_LOGS")
public class PerfilLogsDTO implements Serializable{

	private static final long serialVersionUID = 8712894809770168539L;
	
	@Id
	@SequenceGenerator(name = "aq057dSeq", sequenceName="SQAQ057D_LG_PERFIL_LOGS", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "aq057dSeq")
    @Column(name = "ID_PERFIL_LOG")
	private Long idPerfilLog;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PERFIL")
	private PerfilDTO perfilDTO;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_WEB_LOG")
	private WebLogsDTO webLogsDTO;

	@Column(name = "ST_ACTIVO", nullable = false)
	private Integer stActivo;
	
	@Column(name = "ID_USR_CREACION", nullable = false)
	private Long idUsrCreacion;

	@Column(name = "FH_CREACION")
	private Date fhCreacion;

	@Column(name = "ID_USR_MODIFICA", nullable = false)
	private Long idUsrModifica;

	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;

	public Long getIdPerfilLog() {
		return idPerfilLog;
	}

	public void setIdPerfilLog(Long idPerfilLog) {
		this.idPerfilLog = idPerfilLog;
	}

	public PerfilDTO getPerfilDTO() {
		return perfilDTO;
	}

	public void setPerfilDTO(PerfilDTO perfilDTO) {
		this.perfilDTO = perfilDTO;
	}

	public WebLogsDTO getWebLogsDTO() {
		return webLogsDTO;
	}

	public void setWebLogsDTO(WebLogsDTO webLogsDTO) {
		this.webLogsDTO = webLogsDTO;
	}

	public Integer getStActivo() {
		return stActivo;
	}

	public void setStActivo(Integer stActivo) {
		this.stActivo = stActivo;
	}

	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}

	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}

	public Date getFhCreacion() {
		return fhCreacion;
	}

	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}

	public Long getIdUsrModifica() {
		return idUsrModifica;
	}

	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}

	public Date getFhModificacion() {
		return fhModificacion;
	}

	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	} 
		

}
