package mx.com.teclo.smm.persistencia.vo.evento;

public class EventoAnualVO {
	private Long mes;
	private String cdEvento;
	private Long conteo;
	
	public EventoAnualVO(Long mes, String cdEvento, Long conteo){
		this.mes = mes;
		this.cdEvento = cdEvento;
		this.conteo = conteo;
	}
	
	public Long getMes() {
		return mes;
	}
	public void setMes(Long mes) {
		this.mes = mes;
	}
	public String getCdEvento() {
		return cdEvento;
	}
	public void setCdEvento(String cdEvento) {
		this.cdEvento = cdEvento;
	}
	public Long getConteo() {
		return conteo;
	}
	public void setConteo(Long conteo) {
		this.conteo = conteo;
	}
}
