package mx.com.teclo.smm.api.rest.catalogo;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.com.teclo.smm.negocio.service.catalogo.CatalogoService;
import mx.com.teclo.smm.persistencia.vo.JsonObjectVO;
import mx.com.teclo.smm.persistencia.vo.catalogo.CatalogoVO;
import mx.com.teclo.smm.persistencia.vo.catalogo.ItemCatalogoVO;
import mx.com.teclo.smm.persistencia.vo.geocerca.GeoNotiOpcionVO;

@RestController
public class CatalogoRestController {
	@Autowired
	CatalogoService catalogoService;

	@RequestMapping("/catalogoMonitoreo")
	public ResponseEntity<List<CatalogoVO>> catalogoMonitoreo() {
		List<CatalogoVO> catalogo = this.catalogoService.catalogoMonitoreo();

		return new ResponseEntity<List<CatalogoVO>>(catalogo, HttpStatus.OK);
	}

	@RequestMapping("/buscarCatalogo")
	public ResponseEntity<List<JsonObjectVO>> buscarCatalogo() {
		List<JsonObjectVO> x = new ArrayList<JsonObjectVO>();
		x = this.catalogoService.buscarCatalogo();
		return new ResponseEntity<List<JsonObjectVO>>(x, HttpStatus.OK);
	}

	@RequestMapping("/buscarCatalogosPorId") /* No se utiliza por Geocerca */
	public ResponseEntity<List<CatalogoVO>> buscarCatalogosPorId(@RequestParam("id") Integer[] id) {
		List<CatalogoVO> x = new ArrayList<CatalogoVO>();
		x = this.catalogoService.buscarCatalogosPorId(id);
		return new ResponseEntity<List<CatalogoVO>>(x, HttpStatus.OK);
	}

	@RequestMapping("/catBusquedaGeocerca")
	public ResponseEntity<List<CatalogoVO>> catalogoBusquedaGeocerca() {
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		listaCat = this.catalogoService.catalogoBusquedaGeocerca();
		return new ResponseEntity<List<CatalogoVO>>(listaCat, HttpStatus.OK);
	}
	
	@RequestMapping("/catBusquedaNotificacion")
	public ResponseEntity<List<CatalogoVO>> catalogoBusquedaNotificacion() {
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		listaCat = this.catalogoService.catalogoBusquedaNotificacion();
		return new ResponseEntity<List<CatalogoVO>>(listaCat, HttpStatus.OK);
	}
	
	@RequestMapping("/catBusquedaGrupo")
	public ResponseEntity<List<CatalogoVO>> catalogoBusquedaGrupo() {
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		listaCat = this.catalogoService.catalogoBusquedaGrupo();
		return new ResponseEntity<List<CatalogoVO>>(listaCat, HttpStatus.OK);
	}

	@RequestMapping("/catTipoDispositivo")
	public ResponseEntity<List<CatalogoVO>> catalogoTipoDispositivo() {
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		listaCat = this.catalogoService.catalogoTipoDispositivo();
		return new ResponseEntity<List<CatalogoVO>>(listaCat, HttpStatus.OK);
	}
	
	@RequestMapping("/catalogoTipoMedio")
	public ResponseEntity<List<CatalogoVO>> catalogoTipoMedio() {
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		listaCat = this.catalogoService.catalogoTipoMedio();
		return new ResponseEntity<List<CatalogoVO>>(listaCat, HttpStatus.OK);
	}

	@RequestMapping("/catSubTipoDispositivo")
	public ResponseEntity<List<CatalogoVO>> catalogoSubTipoDispositivo(@RequestParam("idTipo") Long idTipo) {
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		listaCat = this.catalogoService.catalogoSubTipoDispositivo(idTipo);
		return new ResponseEntity<List<CatalogoVO>>(listaCat, HttpStatus.OK);
	}

	@RequestMapping("/catSubTipoDispositivoTodos")
	public ResponseEntity<List<CatalogoVO>> catalogoSubTipoDispositivoTodos() {
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		listaCat = this.catalogoService.catalogoSubTipoDispositivoTodos();
		return new ResponseEntity<List<CatalogoVO>>(listaCat, HttpStatus.OK);
	}

	@RequestMapping("/catTipoGeocerca")
	public ResponseEntity<List<CatalogoVO>> catalogoTipoGeocerca() {
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		listaCat = this.catalogoService.catalogoTipoGeocerca();
		return new ResponseEntity<List<CatalogoVO>>(listaCat, HttpStatus.OK);
	}

	@RequestMapping("/catMarcaDispositivo")
	public ResponseEntity<List<CatalogoVO>> catalogoMarcaDispositivo(@RequestParam("idTipo") Long idTipo) {
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		listaCat = this.catalogoService.catalogoMarcaDispositivo(idTipo);
		return new ResponseEntity<List<CatalogoVO>>(listaCat, HttpStatus.OK);
	}

	@RequestMapping("/catMarca")
	public ResponseEntity<List<CatalogoVO>> catalogoMarca() {
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		listaCat = this.catalogoService.catalogoMarca();
		return new ResponseEntity<List<CatalogoVO>>(listaCat, HttpStatus.OK);
	}

	@RequestMapping("/catMarcaPorModelo")
	public ResponseEntity<List<CatalogoVO>> catalogoMarcaPorModelo(@RequestParam("idMarca") Long idMarca) {
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		listaCat = this.catalogoService.catalogoModeloPorMarca(idMarca);
		return new ResponseEntity<List<CatalogoVO>>(listaCat, HttpStatus.OK);
	}

	@RequestMapping("/catModeloDispositivo")
	public ResponseEntity<List<CatalogoVO>> catalogoModeloDispositivo(@RequestParam("idTipo") Long idTipo,
			@RequestParam("idMarca") Long idMarca, @RequestParam("idSubTipo") Long idSubTipo) {
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		listaCat = this.catalogoService.catalogoModeloDispositivo(idTipo, idMarca, idSubTipo);
		return new ResponseEntity<List<CatalogoVO>>(listaCat, HttpStatus.OK);
	}
	
	@RequestMapping("/catTipoNotificacion")
	public ResponseEntity<List<CatalogoVO>> catalogoTipoNotificacion() {
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		listaCat = this.catalogoService.catalogoTipoNotificacion();
		return new ResponseEntity<List<CatalogoVO>>(listaCat, HttpStatus.OK);
	}
	
	@RequestMapping("/catTipoNotiGeocerca")
	public ResponseEntity<List<CatalogoVO>> catalogoTipoNotificaGeocerca() {
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		listaCat = this.catalogoService.catalogoTipoNotificaGeocerca();
		return new ResponseEntity<List<CatalogoVO>>(listaCat, HttpStatus.OK);
	}
	
	@RequestMapping("/catConfigNotiGeocerca")
	public ResponseEntity<List<GeoNotiOpcionVO>> catalogoConfiguraNotificaGeocerca() {
		List<GeoNotiOpcionVO> listaCat = new ArrayList<GeoNotiOpcionVO>();
		listaCat = this.catalogoService.catalogoConfigNotiGeocerca();
		return new ResponseEntity<List<GeoNotiOpcionVO>>(listaCat, HttpStatus.OK);
	}
	
	@RequestMapping("/catTipoEvento")
	public ResponseEntity<List<CatalogoVO>> catTipoEvento() {
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		listaCat = this.catalogoService.catalogoTipoEvento();
		return new ResponseEntity<List<CatalogoVO>>(listaCat, HttpStatus.OK);
	}
	
	@RequestMapping("/catTipoComando")
	public ResponseEntity<List<CatalogoVO>> catTipoComando() {
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		listaCat = this.catalogoService.catalogoTipoComando();
		return new ResponseEntity<List<CatalogoVO>>(listaCat, HttpStatus.OK);
	}

	@RequestMapping("/catTipoGrupo")
	public ResponseEntity<List<CatalogoVO>> catTipoGrupo() {
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		listaCat = this.catalogoService.catalogoTipoGrupo();
		return new ResponseEntity<List<CatalogoVO>>(listaCat, HttpStatus.OK);
	}

	@RequestMapping("/catCatalogo")
	public ResponseEntity<List<Map<String, Object>>> catCatalogo() throws ParseException {
		List<Map<String, Object>> list = catalogoService.catalogos(1);
		return new ResponseEntity<List<Map<String, Object>>>(list, HttpStatus.OK);
	}

	@RequestMapping("/catCatalogos")
	public ResponseEntity<List<Map<String, Object>>> catCatalogos(@RequestParam("idCatalog") int idCatalog) throws ParseException {
		List<Map<String, Object>> list = catalogoService.catalogos(idCatalog);
		return new ResponseEntity<List<Map<String, Object>>>(list, HttpStatus.OK);
	}

	@PostMapping("/agregaCatalogo")
	public ResponseEntity<String> agregaCatalogo(@RequestBody ItemCatalogoVO itemCatalogoVO) {
		System.out.println(itemCatalogoVO.getId());
		if (catalogoService.agregaCatalogos(itemCatalogoVO)) {
			return new ResponseEntity<String>("", HttpStatus.OK);
		}
		return new ResponseEntity<String>("", HttpStatus.BAD_REQUEST);
	}

	@PostMapping("/actualizaCatalogo")
	public ResponseEntity<String> actualizaCatalogo(@RequestBody ItemCatalogoVO itemCatalogoVO) {
		if (catalogoService.actualizaCatalogos(itemCatalogoVO)) {
			return new ResponseEntity<String>("", HttpStatus.OK);
		}
		return new ResponseEntity<String>("", HttpStatus.BAD_REQUEST);
	}

	
	@RequestMapping("/catCatalogoId")
	public ResponseEntity<ItemCatalogoVO> catCatalogoId(@RequestParam("id") long id,@RequestParam("idCatalog") int idCatalog) {
		
		ItemCatalogoVO itemCatalogoVO =catalogoService.catalogoId(id,idCatalog);
		
		return new ResponseEntity<ItemCatalogoVO>(itemCatalogoVO, HttpStatus.OK);
	}
	
	@RequestMapping("/activarDesactivarCatalogo")
	public ResponseEntity<String> activarDesactivarCatalogo(@RequestParam("id") long id,@RequestParam("idCatalogo") long idCatalogo,@RequestParam("activar") boolean activar) {
		if (catalogoService.activarDesactivarCatalogo(id,idCatalogo,activar)) {
			return new ResponseEntity<String>("", HttpStatus.OK);
		}
		return new ResponseEntity<String>("", HttpStatus.BAD_REQUEST);
	}
	
	@RequestMapping("/catBusuqedaDispositivo")
	public ResponseEntity<List<CatalogoVO>> catBusuqedaDispositivo() {
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		listaCat = catalogoService.catalogoBusquedaDispositivo();
		return new ResponseEntity<List<CatalogoVO>>(listaCat, HttpStatus.OK);
	}
}
