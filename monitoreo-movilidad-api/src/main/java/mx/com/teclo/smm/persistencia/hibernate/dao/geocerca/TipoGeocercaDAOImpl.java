package mx.com.teclo.smm.persistencia.hibernate.dao.geocerca;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.catalogo.CatalogoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.geocerca.TipoGeocercaDTO;

@Repository("tipoGeocercaDAO")
public class TipoGeocercaDAOImpl extends BaseDAOImpl<TipoGeocercaDTO> implements TipoGeocercaDAO{
	
	@Override
	public TipoGeocercaDTO buscarTipoGeocercaPorId(Long id){
		Criteria query = getCurrentSession().createCriteria(TipoGeocercaDTO.class);
		query.add(Restrictions.eq("idTipoGeocerca", id));
		query.add(Restrictions.eq("stActivo",Boolean.TRUE));
		return (TipoGeocercaDTO) query.uniqueResult();
	}
	
	@Override
	public List<TipoGeocercaDTO> buscarTipoGeocerca(){
		Criteria query = getCurrentSession().createCriteria(TipoGeocercaDTO.class);
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return (List<TipoGeocercaDTO>)query.list();
	}
	
	@Override
	public List<TipoGeocercaDTO> activarDesactivarBuscarTipoGeocerca(){
		Criteria query = getCurrentSession().createCriteria(TipoGeocercaDTO.class);	
		query.addOrder(Order.asc("idTipoGeocerca"));
		return (List<TipoGeocercaDTO>)query.list();
	}
	
	@Override
	public TipoGeocercaDTO activarDesactivarBuscarTipoGeocercaPorId(Long id){
		Criteria query = getCurrentSession().createCriteria(TipoGeocercaDTO.class);
		query.add(Restrictions.eq("idTipoGeocerca", id));
		return (TipoGeocercaDTO) query.uniqueResult();
	}

}
