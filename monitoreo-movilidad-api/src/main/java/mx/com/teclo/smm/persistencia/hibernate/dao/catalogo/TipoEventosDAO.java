package mx.com.teclo.smm.persistencia.hibernate.dao.catalogo;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.catalogo.CatalogoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.TipoEventoDTO;


public interface TipoEventosDAO extends BaseDAO<TipoEventoDTO>{
	public List<TipoEventoDTO> obtenerTipoEventos();
	public TipoEventoDTO obtenerTipoEventos(Long id);
	public TipoEventoDTO activarDesactivarObtenerTipoEventos(Long id);
	public List<TipoEventoDTO> activarDesactivarObtenerTipoEventos();
}
