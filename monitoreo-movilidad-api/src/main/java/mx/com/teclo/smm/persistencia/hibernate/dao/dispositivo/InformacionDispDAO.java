package mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo;

import mx.com.teclo.arquitectura.persistencia.comun.dao.BaseDao;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.InformacionDispDTO;

public interface InformacionDispDAO extends BaseDao<InformacionDispDTO>{
	public InformacionDispDTO buscarRelacionPorDispositivo(DispositivoDTO dispDTO);
}
