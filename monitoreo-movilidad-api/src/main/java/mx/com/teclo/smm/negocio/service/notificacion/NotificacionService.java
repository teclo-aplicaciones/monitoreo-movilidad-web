package mx.com.teclo.smm.negocio.service.notificacion;

import java.text.ParseException;
import java.util.List;

import mx.com.teclo.arquitectura.ortogonales.exception.NotFoundException;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.NotificacionDTO;
import mx.com.teclo.smm.persistencia.vo.notificacion.ConsultaNotificacionVO;
import mx.com.teclo.smm.persistencia.vo.notificacion.DetalleNotificacionVO;
import mx.com.teclo.smm.persistencia.vo.notificacion.NotificacionVO;

public interface NotificacionService {
	public List<ConsultaNotificacionVO>consultaNotificaciones(Long tipoBusq, String valor,  Long medio, String fInicial, String fFinal)throws NotFoundException, ParseException;
	public List<DetalleNotificacionVO>consultaDetalleNotifica(Long id);
	public NotificacionVO buscarNotificacionPorActualizar(Long id)throws NotFoundException;
	public NotificacionDTO guardarNotificacion(NotificacionVO notificacionVO) throws ParseException;
	public Boolean actualizaNotificacion(NotificacionVO notificacionVO) throws ParseException;
	public Boolean eliminaNotificacion(Long id);
}
