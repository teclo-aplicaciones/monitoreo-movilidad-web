package mx.com.teclo.smm.persistencia.hibernate.dao.componenteweb;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.componeteweb.UsrPerMenuCompWebDTO;

public interface UsrPerMenuCompWebDAO extends BaseDAO<UsrPerMenuCompWebDTO> {
	List<UsrPerMenuCompWebDTO> obtenerUsrPerMenuCompWebPorUsuario(Long idUsuario,Long idMenu);	
	List<UsrPerMenuCompWebDTO> obtenerUsrPerMenuCompWebPorPerfilMenu(Long idPerfilMenu);
}
