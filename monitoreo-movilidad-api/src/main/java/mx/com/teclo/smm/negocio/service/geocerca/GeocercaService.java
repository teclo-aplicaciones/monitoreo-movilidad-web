package mx.com.teclo.smm.negocio.service.geocerca;

import java.text.ParseException;
import java.util.List;

import mx.com.teclo.arquitectura.ortogonales.exception.NotFoundException;
import mx.com.teclo.smm.persistencia.hibernate.dto.geocerca.GeocercaDTO;
import mx.com.teclo.smm.persistencia.vo.catalogo.CatalogoConSubtipoVO;
import mx.com.teclo.smm.persistencia.vo.catalogo.CatalogoVO;
import mx.com.teclo.smm.persistencia.vo.geocerca.ConsultaGeocercaVO;
import mx.com.teclo.smm.persistencia.vo.geocerca.DetalleGeocercaVO;
import mx.com.teclo.smm.persistencia.vo.geocerca.GeocercaVO;

public interface GeocercaService {

	public List<CatalogoVO> buscarGeocercas();
	public List<CatalogoConSubtipoVO> buscarGeocercasConIdSubtipoDispositivo();
	
	public List<ConsultaGeocercaVO> consultarGeocercas(Long tipoBusq, String valor) throws NotFoundException;
	
	public GeocercaVO buscarGeocercaPorModificar(Long id);
	
	public GeocercaDTO buscarGeocercaPorId(Long id);
	
	public Long[] obtenerDispositivosEnGeocerca(Long idGeocerca);
	
	public Boolean guardarGeocerca(GeocercaVO geocercaVO)throws ParseException;
	
	public Boolean actualizarGeocerca(GeocercaVO geocercaVO) throws ParseException;
	
	public Boolean cambiarEstadoGeocerca(Long id);
	
	public List<DetalleGeocercaVO> buscaDetalleGeocerca(Long id);
	
	public Boolean eliminaGeocercaPerma(Long id) throws NotFoundException;
}
