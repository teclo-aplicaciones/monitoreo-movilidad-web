package mx.com.teclo.smm.persistencia.hibernate.dao.notificacion;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.TipoNotificacionDTO;

public interface TipoNotificacionDAO extends BaseDAO<TipoNotificacionDTO> {
	public TipoNotificacionDTO buscarTipoNotificacionPorId(Long id);
}
