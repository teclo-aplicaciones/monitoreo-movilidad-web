package mx.com.teclo.smm.persistencia.hibernate.dao.geocerca;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.geocerca.GeocercaDTO;

public interface GeocercaDAO extends BaseDAO<GeocercaDTO>{
	public List<GeocercaDTO> buscarGeocercas();
	public GeocercaDTO buscarGrupoPorId(Long id);
	public GeocercaDTO buscarGeocercaPorId(Long id);
	public List<GeocercaDTO> buscarGeocercasPorNombre(String nombre);
}
