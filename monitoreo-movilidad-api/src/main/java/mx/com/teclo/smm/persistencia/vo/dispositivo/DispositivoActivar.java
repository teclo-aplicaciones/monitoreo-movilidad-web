package mx.com.teclo.smm.persistencia.vo.dispositivo;

public class DispositivoActivar {
	private Long id;
	private Boolean activar;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Boolean getActivar() {
		return activar;
	}
	public void setActivar(Boolean activar) {
		this.activar = activar;
	}
	
}
