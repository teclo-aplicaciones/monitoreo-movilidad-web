package mx.com.teclo.smm.negocio.service.perfil;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.dto.perfil.PerfilDTO;
import mx.com.teclo.smm.persistencia.vo.perfil.ConsultaPerfilVO;
import mx.com.teclo.smm.persistencia.vo.perfil.PerfilVO;


public interface EmpresaPerfilService {

	public List<PerfilVO> findProfilesByApp();
	public List<ConsultaPerfilVO> consultaPerfiles();
	public boolean habilitaDeshabilitaPerfil(Long idPerfil, Boolean activo);
	public PerfilDTO crearPerfil(PerfilVO pVO);
	public boolean crearEmpresaPerfil(PerfilDTO perfilDTO);
}
