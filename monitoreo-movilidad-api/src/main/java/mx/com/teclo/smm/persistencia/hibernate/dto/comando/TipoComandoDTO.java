package mx.com.teclo.smm.persistencia.hibernate.dto.comando;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TMM003C_CT_TIPO_COMANDOS")
public class TipoComandoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6044011365049195993L;

	@Id
	@GeneratedValue(generator="id_tcomando_generator",strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="id_tcomando_generator", sequenceName = "SQTMM003C_CT_TIPO_CMD",allocationSize=1)
	@Column(name = "ID_TIPO_COMANDO")
	private Long idTipoComando;
	@Column(name="ID_EMPRESA")
	private long idEmpresa;
	@Column(name = "CD_TIPO_COMANDO")
	private String cdTipoComando;
	@Column(name = "NB_TIPO_COMANDO")
	private String nbTipoComando;
	@Column(name = "TX_TIPO_COMANDO")
	private String txTipoComando;
	@Column(name = "ST_ACTIVO")
	private Boolean stActivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;
	
	public Long getIdTipoComando() {
		return idTipoComando;
	}
	public void setIdTipoComando(Long idTipoComando) {
		this.idTipoComando = idTipoComando;
	}
	public String getCdTipoComando() {
		return cdTipoComando;
	}
	public void setCdTipoComando(String cdTipoComando) {
		this.cdTipoComando = cdTipoComando;
	}
	public String getNbTipoComando() {
		return nbTipoComando;
	}
	public void setNbTipoComando(String nbTipoComando) {
		this.nbTipoComando = nbTipoComando;
	}
	public String getTxTipoComando() {
		return txTipoComando;
	}
	public void setTxTipoComando(String txTipoComando) {
		this.txTipoComando = txTipoComando;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
	public long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
}
