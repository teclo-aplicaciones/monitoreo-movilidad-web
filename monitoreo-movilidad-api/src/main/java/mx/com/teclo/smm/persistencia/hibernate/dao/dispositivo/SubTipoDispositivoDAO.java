package mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.SubTipoDispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.TipoDispositivoDTO;

public interface SubTipoDispositivoDAO extends BaseDAO<SubTipoDispositivoDTO>{

	public List<SubTipoDispositivoDTO> buscarCatalogo(Long[] id);
	public List<SubTipoDispositivoDTO> buscarCatalogoTodos();
	public List<SubTipoDispositivoDTO> buscarCatalogoPorTipo(TipoDispositivoDTO tdDTO);
	public SubTipoDispositivoDTO buscarCatalogoPorTipoAndIdSubtipo(TipoDispositivoDTO tdDTO,Long idSubTipo);	
}
