package mx.com.teclo.smm.util.enumerados;

public enum TipoDispositivoEnum {
	MOVIL(1L), VEHICULO(2L);
	
	private Long  tipoDispositivo;
	
	private TipoDispositivoEnum(Long tipoDispositivo){
		this.tipoDispositivo = tipoDispositivo;
	}
	
	public Long getTipoDispositivo()
	{
		return tipoDispositivo;
	}
	
	public void setTipoDispositivo(Long tipoDispositivo){
		this.tipoDispositivo = tipoDispositivo;
	}
}
