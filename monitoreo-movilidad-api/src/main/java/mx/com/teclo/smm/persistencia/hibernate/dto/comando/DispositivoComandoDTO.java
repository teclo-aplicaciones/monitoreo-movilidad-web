package mx.com.teclo.smm.persistencia.hibernate.dto.comando;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.SubTipoDispositivoDTO;

@Entity
@Table(name = "TMM026D_DP_DISP_COMANDO")
public class DispositivoComandoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4630351890686134951L;
	
	@Id
	@GeneratedValue(generator="id_dispcmd_generator",strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="id_dispcmd_generator", sequenceName = "SQTMM026D_DP_DISP_CMD",allocationSize=1)
	@Column(name = "ID_DISP_COMANDO")
	private Long idDispositivoComando;
	@ManyToOne
	@JoinColumn(name = "ID_DISPOSTIVO")
	private DispositivoDTO idDispositivo;
	@ManyToOne
	@JoinColumn(name = "ID_COMANDO")
	private ComandoDTO idComando;
	@Column(name = "ST_ACTIVO")
	private Boolean stActivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;
	public Long getIdDispositivoComando() {
		return idDispositivoComando;
	}
	public void setIdDispositivoComando(Long idDispositivoComando) {
		this.idDispositivoComando = idDispositivoComando;
	}
	
	public DispositivoDTO getIdDispositivo() {
		return idDispositivo;
	}
	public void setIdDispositivo(DispositivoDTO idDispositivo) {
		this.idDispositivo = idDispositivo;
	}
	public ComandoDTO getIdComando() {
		return idComando;
	}
	public void setIdComando(ComandoDTO idComando) {
		this.idComando = idComando;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
	
	
	
		
}
