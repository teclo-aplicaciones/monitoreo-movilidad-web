package mx.com.teclo.smm.persistencia.hibernate.dto.grupo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.Column;

@Entity
@Table(name = "TMM020D_CT_GRUPOS")
public class GrupoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2758333202814022787L;


	@Id
	@GeneratedValue(generator="id_grupos_generator",strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="id_grupos_generator", sequenceName = "SQTMM020D_CT_GRUPOS",allocationSize=1)
	@Column(name = "ID_GRUPO")
	private Long idGrupo;
	@Column(name="ID_EMPRESA")
	private long idEmpresa;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_TIPO_GRUPO")
	private TipoGrupoDTO idTipoGrupo;
	@Column(name = "NB_GRUPO")
	private String nbGrupo;
	@Column(name = "TX_GRUPO")
	private String txGrupo;
	@Column(name = "ST_ACTIVO")
	private Boolean stActivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;

	public Long getIdGrupo() {
		return idGrupo;
	}

	public void setIdGrupo(Long idGrupo) {
		this.idGrupo = idGrupo;
	}

	public TipoGrupoDTO getIdTipoGrupo() {
		return idTipoGrupo;
	}

	public void setIdTipoGrupo(TipoGrupoDTO idTipoGrupo) {
		this.idTipoGrupo = idTipoGrupo;
	}

	public String getNbGrupo() {
		return nbGrupo;
	}

	public void setNbGrupo(String nbGrupo) {
		this.nbGrupo = nbGrupo;
	}

	public String getTxGrupo() {
		return txGrupo;
	}

	public void setTxGrupo(String txGrupo) {
		this.txGrupo = txGrupo;
	}

	public Boolean getStActivo() {
		return stActivo;
	}

	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}

	public Date getFhCreacion() {
		return fhCreacion;
	}

	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}

	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}

	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}

	public Date getFhModificacion() {
		return fhModificacion;
	}

	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}

	public Long getIdUsrModifica() {
		return idUsrModifica;
	}

	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
	public long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
}
