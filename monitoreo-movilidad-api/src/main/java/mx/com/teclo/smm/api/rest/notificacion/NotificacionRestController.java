package mx.com.teclo.smm.api.rest.notificacion;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.com.teclo.arquitectura.ortogonales.exception.NotFoundException;
import mx.com.teclo.smm.negocio.service.dispositivo.DispositivoService;
import mx.com.teclo.smm.negocio.service.empleado.EmpleadoService;
import mx.com.teclo.smm.negocio.service.notificacion.NotificacionService;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.NotificacionDTO;
import mx.com.teclo.smm.persistencia.vo.catalogo.CatalogoVO;
import mx.com.teclo.smm.persistencia.vo.geocerca.DispositivoEnGeocercaVO;
import mx.com.teclo.smm.persistencia.vo.notificacion.ConsultaNotificacionVO;
import mx.com.teclo.smm.persistencia.vo.notificacion.DetalleNotificacionVO;
import mx.com.teclo.smm.persistencia.vo.notificacion.NotificacionVO;
import mx.com.teclo.smm.util.enumerados.CatalogoGeocercaEnum;

@RestController
public class NotificacionRestController {
	
	@Autowired
	DispositivoService dispositivoService;
	
	@Autowired
	NotificacionService notificacionService;
	
	@Autowired
	EmpleadoService empleadoService;

	@RequestMapping("/consultaNotificaciones")
	@PreAuthorize("hasAnyAuthority('BUSCA_DISPO_EN_NOTI')")
	public ResponseEntity<List<ConsultaNotificacionVO>> consultaNotificaciones(@RequestParam("tipo")Long tipo, @RequestParam("valor")String valor, @RequestParam("medio")Long medio, @RequestParam("fechaInicial")String fInicial, @RequestParam("fechaFinal")String fFinal)throws NotFoundException, ParseException{
		List<ConsultaNotificacionVO> x = new ArrayList<ConsultaNotificacionVO>();
		
		x = notificacionService.consultaNotificaciones(tipo, valor, medio,fInicial, fFinal);
		
		return new ResponseEntity<List<ConsultaNotificacionVO>>(x, HttpStatus.OK);
	}
	
	
	
	@RequestMapping("/consultaDetalleNotificaciones")
	//@PreAuthorize("hasAnyAuthority('BUSCA_DISPO_EN_NOTI')")
	public ResponseEntity<List<DetalleNotificacionVO>> buscarDispositivoEnNotificacion(@RequestParam("id")Long id){
		List<DetalleNotificacionVO> x = new ArrayList<DetalleNotificacionVO>();
		
		x = notificacionService.consultaDetalleNotifica(id);
		
		return new ResponseEntity<List<DetalleNotificacionVO>>(x, HttpStatus.OK);
	}
	
	@RequestMapping("/buscarNotificacionParaActualizar")
	public ResponseEntity<NotificacionVO> buscarNotificacionPorId(@RequestParam("id")Long id)throws NotFoundException{
		
		NotificacionVO nVO = notificacionService.buscarNotificacionPorActualizar(id);
		
		return new ResponseEntity<NotificacionVO>(nVO, HttpStatus.OK);
	}
	
	@RequestMapping("/buscarNotificacionDispositivos")
	@PreAuthorize("hasAnyAuthority('BUSCA_DISPO_PARA_NOTI')")
	public ResponseEntity<List<DispositivoEnGeocercaVO>> buscarDispositivoParaNotificaciones(){
		
		List<DispositivoEnGeocercaVO> lista = dispositivoService.buscarDispositivoParaGeocerca(null);
		
		return new ResponseEntity<List<DispositivoEnGeocercaVO>>(lista, HttpStatus.OK);
	}
	
	@RequestMapping("/buscarNotificacionAgrupamiento")
	@PreAuthorize("hasAnyAuthority('BUSCA_NOTI_AGRUPA')")
	public ResponseEntity<List<CatalogoVO>> buscarCatalogoAgrupamiento(@RequestParam("id")String tipo){
		List<CatalogoVO> x = new ArrayList<CatalogoVO>();
		
		if(Long.parseLong(tipo) == CatalogoGeocercaEnum.DISPOSITIVO.getIdCat()){
			x = dispositivoService.buscarDispositivo(null);
		}else if(Long.parseLong(tipo) == CatalogoGeocercaEnum.USUARIOS.getIdCat()){
			x = empleadoService.buscarEmpleado();
		}
		
		return new ResponseEntity<List<CatalogoVO>>(x, HttpStatus.OK);
	}
	
	@RequestMapping(value="/guardarNotificacion", method=RequestMethod.PUT)
	@PreAuthorize("hasAnyAuthority('GUARDA_NOTI')")
	public ResponseEntity<Boolean> guardarNotificacion(@RequestBody NotificacionVO notificacionVO) throws ParseException{
		
		NotificacionDTO res = notificacionService.guardarNotificacion(notificacionVO);
		
		return new ResponseEntity<Boolean>(res != null ?  Boolean.TRUE : Boolean.FALSE, HttpStatus.OK);
	}
	
	@RequestMapping(value="/actualizaNotificacion", method=RequestMethod.PUT)
	@PreAuthorize("hasAnyAuthority('ACTUALIZA_NOTI')")
	public ResponseEntity<Boolean> actualizaNotificacion(@RequestBody NotificacionVO notificacionVO) throws ParseException{
		
		Boolean res = notificacionService.actualizaNotificacion(notificacionVO);
		
		return new ResponseEntity<Boolean>(res, HttpStatus.OK);
	}
	
	@RequestMapping(value="/eliminaNotificacion", method=RequestMethod.GET)
	@PreAuthorize("hasAnyAuthority('ELIMINA_NOTI')")
	public ResponseEntity<Boolean> eliminaNotificacion(@RequestParam Long id){
		
		Boolean res = notificacionService.eliminaNotificacion(id);
		
		return new ResponseEntity<Boolean>(res, HttpStatus.OK);
	}
}
