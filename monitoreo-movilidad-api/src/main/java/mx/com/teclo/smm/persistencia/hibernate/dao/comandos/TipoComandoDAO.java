package mx.com.teclo.smm.persistencia.hibernate.dao.comandos;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.TipoComandoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.grupo.TipoGrupoDTO;


public interface TipoComandoDAO extends BaseDAO<TipoComandoDTO>{
	public List<TipoComandoDTO> obtenerTipoComando();
	public TipoComandoDTO activarDesactivarObtenerTipoComando(Long id);
	public TipoComandoDTO obtenerTipoComando(Long id);
	public List<TipoComandoDTO> activarDesactivarObtenerTipoComando() ;
}
	