package mx.com.teclo.smm.persistencia.hibernate.dao.comandos;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.catalogo.CatalogoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.ComandoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.TipoEventoDTO;


public interface ComandoDAO extends BaseDAO<ComandoDTO>{
	public List<ComandoDTO> obtenerComando();
	public ComandoDTO obtenerComando(Long id);
	public ComandoDTO activarDesactivarObtenerComando(Long id);
	public List<ComandoDTO> activarDesactivarObtenerComando();
	public List<ComandoDTO> obtenerComandosPorTipo(Long idSubTipoDispositivo,Long idTipoMedioComunicacion);
}
