package mx.com.teclo.smm.util.comun;

public class ResponseVO {

	private Integer code;
	private String message;
	
	public ResponseVO(Integer code, String message) {
		this.code = code;
		this.message = message;
	}
	
	public ResponseVO(Integer code) {
		this(code, "");
	}

	public ResponseVO() {
		this(0);
	}

	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
