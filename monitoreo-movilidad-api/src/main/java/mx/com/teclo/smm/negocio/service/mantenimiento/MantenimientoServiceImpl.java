package mx.com.teclo.smm.negocio.service.mantenimiento;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.smm.negocio.service.empresa.EmpresaService;
import mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo.DispoEmpleadoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo.DispositivoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.mantenimiento.BusqMantenimientoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.mantenimiento.MantenimientoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.mantenimiento.StatusMantenimientoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.mantenimiento.TipoMantenimientoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispoEmpleadoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.mantenimiento.BusqMantenimeintoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.mantenimiento.MantenimeintoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.mantenimiento.TipoMantenimeintoDTO;
import mx.com.teclo.smm.persistencia.vo.JsonObjectVO;
import mx.com.teclo.smm.persistencia.vo.mantenimiento.MantenimientoVO;

@Service
public class MantenimientoServiceImpl implements MantenimientoService {

	List<JsonObjectVO> listaObject;

	@Autowired
	DispositivoDAO dispositivoDAO;
	@Autowired
	TipoMantenimientoDAO tipoMantenimientoDAO;
	@Autowired
	MantenimientoDAO mantenimientoDAO;
	@Autowired
	BusqMantenimientoDAO busqMantenimeintoDAO;
	@Autowired
	StatusMantenimientoDAO statusMantenimientoDAO;
	@Autowired
	DispoEmpleadoDAO dispoEmpleadoDAO;
	
	@Autowired
	private EmpresaService empresaService; 

	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	@Override
	@Transactional
	public List<MantenimientoVO> consultarMantenimiento(String tipo, String valor) {
		List<MantenimeintoDTO> list = null;

		switch (tipo) {
		// TODOS
		case "1":
			list = mantenimientoDAO.obtenerMantenimeinto();
			break;
		// tipo de mantenimiento
		case "2":
			TipoMantenimeintoDTO tipoMantenimeintoDTO = tipoMantenimientoDAO.obtenerTipoMantenimeintoPorId(new Long(valor));
			list = mantenimientoDAO.obtenerMantenimeintoPorTipoMantenimiento(tipoMantenimeintoDTO);
			break;
		// Nombre de mantenimiento
		case "3":
			list = mantenimientoDAO.obtenerMantenimeintoLikeNombre(valor);
			break;
		// Nombre de IMEI
		case "4":
			list = new ArrayList<>();
			List<DispositivoDTO> dispositivoDTOs = dispositivoDAO.buscarDispositivosPorTipo("2", valor);
			for (DispositivoDTO d : dispositivoDTOs) {
				for (MantenimeintoDTO m : mantenimientoDAO.obtenerMantenimeintoPorIdDispositivo(d)) {
					list.add(m);
				}
			}
			break;
		// Nombre de usuario
		case "5":
			list = new ArrayList<>();
			List<DispositivoDTO> dispositivoDTO2s = dispositivoDAO.buscarDispositivosPorTipo("1", valor);
			for (DispositivoDTO d : dispositivoDTO2s) {
				for (MantenimeintoDTO m : mantenimientoDAO.obtenerMantenimeintoPorIdDispositivo(d)) {
					list.add(m);
				}
			}
			break;
		}

		List<MantenimientoVO> listaMantenimiento = new ArrayList<MantenimientoVO>();

		for (MantenimeintoDTO m : list) {

			// solo para status En Curso
			if (m.getStatusMantenimeinto().getIdStatusMantenimiento() == 2) {
				// actualizar status de rechazado
				if (m.getFhEntregaEstimada().getTime() < m.getFhIngreso().getTime()) {
					m.setStatusMantenimeinto(statusMantenimientoDAO.obtenerStatusMantenimientoPorId(3L));
					mantenimientoDAO.save(m);
				}
			}

			MantenimientoVO manteVO = new MantenimientoVO();
			manteVO.setId(m.getIdMantenimiento());
			manteVO.setNombre(m.getNbMantenimiento());

			// obtener nombre de usuario
			DispoEmpleadoDTO dispoEmpleadoDTO = dispoEmpleadoDAO.buscarRelacionDispoEmpleadoPorDispositivo(m.getIdDispositivo());
			if (dispoEmpleadoDTO != null && dispoEmpleadoDTO.getIdEmpleado() != null) {
				manteVO.setUsuario(dispoEmpleadoDTO.getIdEmpleado().getNbEmpleado());
				manteVO.setIdUsuario(dispoEmpleadoDTO.getIdEmpleado().getIdEmpleado());
			}

			manteVO.setImei(m.getIdDispositivo().getCdDispositivo());
			manteVO.setTipoNombre(m.getTipoMantenimeinto().getNbTipoMantenimiento());
			manteVO.setDescripcion(m.getTxMantenimiento());
			manteVO.setDispositivo(m.getIdDispositivo().getNbDispositivo());
			manteVO.setFechaRecepcion("" + sdf.format(m.getFhIngreso()));
			if (m.getFhEntrega() != null) {
				manteVO.setFechaEntrega("" + sdf.format(m.getFhEntrega()));
			} else {
				manteVO.setFechaEntrega("--/--/--");
			}
			manteVO.setFechaEntregaEstimada("" + sdf.format(m.getFhEntregaEstimada()));
			manteVO.setIdStatus(m.getStatusMantenimeinto().getIdStatusMantenimiento());
			manteVO.setStatus(m.getStatusMantenimeinto().getNbStatusMantenimiento());
			manteVO.setStatusColor(m.getStatusMantenimeinto().getTxColor());
			manteVO.setObservaciones(m.getTxObservaciones());

			listaMantenimiento.add(manteVO);
		}
		return listaMantenimiento;
	}

	@Override
	@Transactional(readOnly = true)
	public MantenimientoVO buscarMantenimientoPorId(Long id) {
		MantenimeintoDTO m = mantenimientoDAO.obtenerMantenimeintoPorId(id);

		MantenimientoVO mantenimientoVO = new MantenimientoVO();

		mantenimientoVO.setId(m.getIdMantenimiento());
		mantenimientoVO.setTipo(m.getTipoMantenimeinto().getIdTipoMantenimiento());
		mantenimientoVO.setTipoNombre(m.getTipoMantenimeinto().getNbTipoMantenimiento());
		mantenimientoVO.setNombre(m.getNbMantenimiento());
		mantenimientoVO.setDispositivo(m.getIdDispositivo().getNbDispositivo());
		mantenimientoVO.setDispositivoId(m.getIdDispositivo().getIdDispositivo());
		mantenimientoVO.setDescripcion(m.getTxMantenimiento());
		mantenimientoVO.setImei(m.getIdDispositivo().getCdDispositivo());
		mantenimientoVO.setFechaRecepcion("" + sdf.format(m.getFhIngreso()));
		if (m.getFhEntrega() != null) {
			mantenimientoVO.setFechaEntrega("" + sdf.format(m.getFhEntrega()));
		} else {
			mantenimientoVO.setFechaEntrega("--/--/--");
		}
		mantenimientoVO.setFechaEntregaEstimada("" + sdf.format(m.getFhEntregaEstimada()));
		mantenimientoVO.setIdStatus(m.getStatusMantenimeinto().getIdStatusMantenimiento());
		mantenimientoVO.setStatus(m.getStatusMantenimeinto().getNbStatusMantenimiento());
		mantenimientoVO.setStatusColor(m.getStatusMantenimeinto().getTxColor());
		mantenimientoVO.setObservaciones(m.getTxObservaciones());
		return mantenimientoVO;
	}

	@Override
	@Transactional(readOnly = true)
	public List<MantenimientoVO> listaDispositivos() {
		List<MantenimientoVO> mantenimientoVOs = new ArrayList<MantenimientoVO>();
		List<DispositivoDTO> dispositivoDTOs = dispositivoDAO.buscarDispositivos();

		for (DispositivoDTO d : dispositivoDTOs) {
			MantenimientoVO m = new MantenimientoVO();
			m.setId(d.getIdDispositivo());
			m.setNombre(d.getNbDispositivo());
			m.setImei(d.getCdDispositivo());
			mantenimientoVOs.add(m);
		}

		return mantenimientoVOs;
	}

	@Override
	@Transactional(readOnly = true)
	public List<MantenimientoVO> tipoMantenimiento() {
		List<MantenimientoVO> mantenimientoVOs = new ArrayList<MantenimientoVO>();

		for (TipoMantenimeintoDTO t : tipoMantenimientoDAO.obtenerTipoMantenimeinto()) {
			MantenimientoVO m = new MantenimientoVO();
			m.setNombre(t.getNbTipoMantenimiento());
			m.setId(t.getIdTipoMantenimiento());
			mantenimientoVOs.add(m);
		}

		return mantenimientoVOs;
	}

	@Override
	@Transactional(readOnly = true)
	public List<BusqMantenimeintoDTO> busqMantenimiento() {
		return busqMantenimeintoDAO.obtenerBusqMantenimeinto();
	}

	@Override
	@Transactional
	public boolean agregaMantenimiento(MantenimientoVO mantenimientoVO) {
		Date dateFechaEntregaEstimada = null;
		Date dateFechaRecepcion = null;
		try {
			dateFechaEntregaEstimada = new SimpleDateFormat("dd/MM/yyyy").parse(mantenimientoVO.getFechaEntregaEstimada());
			dateFechaRecepcion = new SimpleDateFormat("dd/MM/yyyy").parse(mantenimientoVO.getFechaRecepcion());
		} catch (ParseException e) {
			e.printStackTrace();
		}

		MantenimeintoDTO mantenimeintoDTO = new MantenimeintoDTO();
		mantenimeintoDTO.setNbMantenimiento(mantenimientoVO.getNombre());
		mantenimeintoDTO.setTipoMantenimeinto(tipoMantenimientoDAO.obtenerTipoMantenimeintoPorId(mantenimientoVO.getTipo()));
		mantenimeintoDTO.setFhIngreso(dateFechaRecepcion);
		mantenimeintoDTO.setFhEntregaEstimada(dateFechaEntregaEstimada);
		mantenimeintoDTO.setIdDispositivo(dispositivoDAO.buscarDispositivosPorId(mantenimientoVO.getDispositivoId()));
		mantenimeintoDTO.setTxMantenimiento(mantenimientoVO.getDescripcion());
		mantenimeintoDTO.setStatusMantenimeinto(statusMantenimientoDAO.obtenerStatusMantenimientoPorId(2L));
		mantenimeintoDTO.setStActivo(true);
		mantenimeintoDTO.setIdUsrCreacion(1L);
		mantenimeintoDTO.setIdUsrModifica(1L);
		mantenimeintoDTO.setFhModificacion(new Date());
		mantenimeintoDTO.setFhCreacion(new Date());

		mantenimeintoDTO.setIdEmpresa(empresaService.getIdEmpresa());
		
		mantenimientoDAO.save(mantenimeintoDTO);
		return true;
	}

	@Override
	@Transactional
	public boolean actualizaMantenimiento(MantenimientoVO mantenimientoVO) {
		MantenimeintoDTO mantenimeintoDTO = mantenimientoDAO.obtenerMantenimeintoPorId(mantenimientoVO.getId());

		Date dateFechaRecepcion = null;
		Date dateFechaEntregaEstimada = null;
		try {
			dateFechaEntregaEstimada = new SimpleDateFormat("dd/MM/yyyy").parse(mantenimientoVO.getFechaEntregaEstimada());
			dateFechaRecepcion = new SimpleDateFormat("dd/MM/yyyy").parse(mantenimientoVO.getFechaRecepcion());
		} catch (ParseException e) {
			e.printStackTrace();
		}

		mantenimeintoDTO.setNbMantenimiento(mantenimientoVO.getNombre());
		mantenimeintoDTO.setTipoMantenimeinto(tipoMantenimientoDAO.obtenerTipoMantenimeintoPorId(mantenimientoVO.getTipo()));
		mantenimeintoDTO.setFhIngreso(dateFechaRecepcion);
		mantenimeintoDTO.setFhEntregaEstimada(dateFechaEntregaEstimada);
		mantenimeintoDTO.setIdDispositivo(dispositivoDAO.buscarDispositivosPorId(mantenimientoVO.getDispositivoId()));
		mantenimeintoDTO.setTxMantenimiento(mantenimientoVO.getDescripcion());

		mantenimeintoDTO.setStActivo(true);
		mantenimeintoDTO.setIdUsrCreacion(1L);
		mantenimeintoDTO.setIdUsrModifica(1L);
		mantenimeintoDTO.setFhModificacion(new Date());
		mantenimeintoDTO.setFhCreacion(new Date());

		mantenimientoDAO.save(mantenimeintoDTO);

		return true;
	}

	@Override
	@Transactional
	public boolean eliminarMantenimiento(Long idMantenimientoVO) {
		MantenimeintoDTO mantenimeintoDTO = mantenimientoDAO.obtenerMantenimeintoPorId(idMantenimientoVO);
		mantenimientoDAO.delete(mantenimeintoDTO);
		return true;
	}

	@Override
	@Transactional
	public boolean finalizarMantenimiento(Long idMantenimiento, String Observaciones) {
		MantenimeintoDTO mantenimeintoDTO = mantenimientoDAO.obtenerMantenimeintoPorId(idMantenimiento);
		mantenimeintoDTO.setStatusMantenimeinto(statusMantenimientoDAO.obtenerStatusMantenimientoPorId(1L));
		mantenimeintoDTO.setTxObservaciones(Observaciones);
		mantenimeintoDTO.setFhEntrega(new Date());
		mantenimientoDAO.save(mantenimeintoDTO);
		return true;
	}

	@Override
	@Transactional
	public List<List<MantenimientoVO>> timelineMantenimiento(Long idMantenimientoVO, String idUsuarioOIMEI, int tipoTimeline) {

		List<List<MantenimientoVO>> listMantenimientoVOs = new ArrayList<List<MantenimientoVO>>();
		List<MantenimientoVO> mantenimientoVOs;

		switch (tipoTimeline) {
		// busqueda por usuario por el idEmpleado
		case 1:
			Long idEmpleado = Long.parseLong(idUsuarioOIMEI);
			List<DispoEmpleadoDTO> dispoEmpleadoDTOs = dispoEmpleadoDAO.buscarRelacionIdDisposEmpleadoPorEmpleado(idEmpleado);
			
			for (DispoEmpleadoDTO d : dispoEmpleadoDTOs) {
				List<MantenimeintoDTO> mantenimeintoDTOs = mantenimientoDAO.obtenerMantenimeintoPorIdDispositivo(d.getIdDispositivo());				
				mantenimientoVOs = new ArrayList<MantenimientoVO>();	
				for (MantenimeintoDTO m : mantenimeintoDTOs) {
					MantenimientoVO manteVO = new MantenimientoVO();
					manteVO.setId(m.getIdMantenimiento());
					manteVO.setNombre(m.getNbMantenimiento());
					manteVO.setDescripcion(m.getTxMantenimiento());					
					manteVO.setUsuario(d.getIdEmpleado().getNbEmpleado());					
					manteVO.setImei(m.getIdDispositivo().getCdDispositivo());
					manteVO.setTipoNombre(m.getTipoMantenimeinto().getNbTipoMantenimiento());
					manteVO.setFechaRecepcion("" + sdf.format(m.getFhIngreso()));
					if (m.getFhEntrega() != null) {
						manteVO.setFechaEntrega("" + sdf.format(m.getFhEntrega()));
					} else {
						manteVO.setFechaEntrega("--/--/--");
					}
					manteVO.setFechaEntregaEstimada("" + sdf.format(m.getFhEntregaEstimada()));
					manteVO.setStatus(m.getStatusMantenimeinto().getNbStatusMantenimiento());
					manteVO.setStatusColor(m.getStatusMantenimeinto().getTxColor());
					manteVO.setIdStatus(m.getStatusMantenimeinto().getIdStatusMantenimiento());
					manteVO.setObservaciones(m.getTxObservaciones());
					mantenimientoVOs.add(manteVO);
				}
				listMantenimientoVOs.add(mantenimientoVOs);
			}

			break;
		// busqueda por imei
		case 2:
			String imei = idUsuarioOIMEI;
			MantenimeintoDTO mantenimeintoDTO = mantenimientoDAO.obtenerMantenimeintoPorId(idMantenimientoVO);
			List<MantenimeintoDTO> mantenimeintoDTOs = mantenimientoDAO.obtenerMantenimeintoPorIdDispositivo(mantenimeintoDTO.getIdDispositivo());
			mantenimientoVOs = new ArrayList<MantenimientoVO>();
			for (MantenimeintoDTO m : mantenimeintoDTOs) {
				MantenimientoVO manteVO = new MantenimientoVO();
				manteVO.setId(m.getIdMantenimiento());
				manteVO.setNombre(m.getNbMantenimiento());
				manteVO.setDescripcion(m.getTxMantenimiento());
				// obtener nombre de usuario
				DispoEmpleadoDTO dispoEmpleadoDTO = dispoEmpleadoDAO.buscarRelacionDispoEmpleadoPorDispositivo(m.getIdDispositivo());
				if (dispoEmpleadoDTO != null && dispoEmpleadoDTO.getIdEmpleado() != null) {
					manteVO.setUsuario(dispoEmpleadoDTO.getIdEmpleado().getNbEmpleado());
				}
				manteVO.setImei(m.getIdDispositivo().getCdDispositivo());
				manteVO.setTipoNombre(m.getTipoMantenimeinto().getNbTipoMantenimiento());
				manteVO.setFechaRecepcion("" + sdf.format(m.getFhIngreso()));
				if (m.getFhEntrega() != null) {
					manteVO.setFechaEntrega("" + sdf.format(m.getFhEntrega()));
				} else {
					manteVO.setFechaEntrega("--/--/--");
				}
				manteVO.setFechaEntregaEstimada("" + sdf.format(m.getFhEntregaEstimada()));
				manteVO.setStatus(m.getStatusMantenimeinto().getNbStatusMantenimiento());
				manteVO.setStatusColor(m.getStatusMantenimeinto().getTxColor());
				manteVO.setIdStatus(m.getStatusMantenimeinto().getIdStatusMantenimiento());
				manteVO.setObservaciones(m.getTxObservaciones());
				mantenimientoVOs.add(manteVO);
			}
			listMantenimientoVOs.add(mantenimientoVOs);
			break;
		}

		return listMantenimientoVOs;
	}

}
