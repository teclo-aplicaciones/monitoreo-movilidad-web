package mx.com.teclo.smm.negocio.service.componenteweb;

import java.util.List;

import mx.com.teclo.smm.persistencia.vo.componenteweb.ComponenteWebVO;

public interface ComponenteWebService {

	List<ComponenteWebVO> obtenerComponenteWeb (Long menuId);	
	
}
