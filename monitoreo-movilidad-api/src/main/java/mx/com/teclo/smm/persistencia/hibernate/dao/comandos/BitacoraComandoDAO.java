package mx.com.teclo.smm.persistencia.hibernate.dao.comandos;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.BitacoraComandoDTO;


public interface BitacoraComandoDAO extends BaseDAO<BitacoraComandoDTO>{
	public List<BitacoraComandoDTO> obtenerLogComando(Long idDispositivo);	
}
