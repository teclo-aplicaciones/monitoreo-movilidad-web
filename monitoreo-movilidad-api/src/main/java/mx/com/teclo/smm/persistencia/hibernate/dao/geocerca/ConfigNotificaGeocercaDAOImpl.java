package mx.com.teclo.smm.persistencia.hibernate.dao.geocerca;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.geocerca.ConfigNotificaGeocercaDTO;

@SuppressWarnings("unchecked")
@Repository("configNotificaGeocercaDAO")
public class ConfigNotificaGeocercaDAOImpl extends BaseDAOImpl<ConfigNotificaGeocercaDTO> implements ConfigNotificaGeocercaDAO{

	@Override
	public List<ConfigNotificaGeocercaDTO> buscarCatalogo(){
		Criteria query = getCurrentSession().createCriteria(ConfigNotificaGeocercaDTO.class);
		query.add(Restrictions.eq("stActivo",Boolean.TRUE));
		
		return (List<ConfigNotificaGeocercaDTO>) query.list();
	}
	
	@Override
	public ConfigNotificaGeocercaDTO buscaCatalogoPorId(Long id){
		Criteria query = getCurrentSession().createCriteria(ConfigNotificaGeocercaDTO.class);
		query.add(Restrictions.eq("idConfigGeoNoti",id));
		query.add(Restrictions.eq("stActivo",Boolean.TRUE));
		
		return (ConfigNotificaGeocercaDTO) query.uniqueResult();
	}
}
