package mx.com.teclo.smm.persistencia.vo.evento;

public class TCGrupoVO {
	private String tcNombre;
	private Long tcConteo;
	
	public String getTcNombre() {
		return tcNombre;
	}
	public void setTcNombre(String tcNombre) {
		this.tcNombre = tcNombre;
	}
	public Long getTcConteo() {
		return tcConteo;
	}
	public void setTcConteo(Long tcConteo) {
		this.tcConteo = tcConteo;
	}
}
