package mx.com.teclo.smm.negocio.service.logs;

 
import java.text.ParseException;

import mx.com.teclo.smm.persistencia.vo.logs.LogsVO;
 
/**
 *  Copyright (c) 2018, Teclo Mexicana. 
 * 
 *  Descripcion					: LogsAdministracionService
 *  Historial de Modificaciones	: 
 *  Descripcion del Cambio 		: Modificación
 *  
 *  @author 					: fjmb
 *  @version 					: 1.0 
 *  Fecha 						: 01/Abril/2019
 */
public interface LogsAdministracionService {
	
    

	/**
	 * Habilita o deshabilita <b>Consultas a Logs</b> dependiendo del parámetro especificado.
	 * 
	 * @param logId 		Log al que se le cambiar  el estatus.
     * @param accion 		A:Activo D:Desactivado.
     * 
     * @author 				javier07, fmartinez 
     * @version 		    2.0
     * Fecha				01/Abril/2019
     */
      void cambioDeEstatus(Long logId, String accion); 
  	
    /**
  	 *  Agrega el permiso de acceso a un <b>Perfil sobre un Log</b>.
  	 * 
  	 * @param logId 	    Log al que se le asociara un perfil
     * @param perfilId 		Perfil a asignar
     * 
     * @author 				javier07, fmartinez 
     * @version 			2.0
     * Fecha				01/Abril/2019
    */
     LogsVO agregarPerfilALog(Long logId, Long perfilId);

	 /**
	  *  Elimina el permiso de acceso de un <b>Perfil sobre un Log</b>.
	  * 
	  * @param logId 		log al que se le asociara un perfil
	  * @param perfilId 	Perfil a liminar
	  * 
	  * @author 			javier07, fmartinez 
	  * @version 			2.0
	  * Fecha				01/Abril/2019
	 */
    void eliminarPerfilALog(Long logId, Long perfilId);

    /**
	  *  Crea o modifica un <b>Registro de Log</b>.
	  * 
	  * @param log 			Bean con los parametros a modificar o crear.
      * @param operacion 	A:Alta M:Modificar
	  * 
	  * @author 			javier07, fmartinez 
	  * @version 			2.0
	  * Fecha				01/Abril/2019
	 */
    LogsVO crudLog(LogsVO log, String operacion) throws ParseException;
}
