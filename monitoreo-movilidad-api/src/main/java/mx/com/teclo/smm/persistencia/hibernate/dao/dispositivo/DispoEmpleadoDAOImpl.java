package mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispoEmpleadoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.empleado.EmpleadoDTO;

@SuppressWarnings("unchecked")
@Repository("DispoEmpleadoDAO")
public class DispoEmpleadoDAOImpl extends BaseDAOImpl<DispoEmpleadoDTO> implements DispoEmpleadoDAO{

	@Override
	public List<DispoEmpleadoDTO> buscarRelacionDispoEmpleado()
	{
		Criteria query = getCurrentSession().createCriteria(DispoEmpleadoDTO.class);
		query.createAlias("idDispositivo", "idDispositivo");
		query.createAlias("idEmpleado", "idEmpleado");
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		
		
		//idEmpresa
		query.add(Restrictions.eq("idDispositivo.idEmpresa", getIdEmpresa()));
		query.add(Restrictions.eq("idEmpleado.idEmpresa", getIdEmpresa()));
		
		return (List<DispoEmpleadoDTO>)query.list();
	}
	
	@Override
	public List<EmpleadoDTO> buscarRelacionDispoEmpleadoUnicos()
	{
		Criteria query = getCurrentSession().createCriteria(DispoEmpleadoDTO.class);
		query.createAlias("idDispositivo", "idDispositivo");
		query.createAlias("idEmpleado", "idEmpleado");
		query.setProjection(Projections.distinct(Projections.property("idEmpleado")));		
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		
		//idEmpresa
		query.add(Restrictions.eq("idDispositivo.idEmpresa", getIdEmpresa()));
		query.add(Restrictions.eq("idEmpleado.idEmpresa", getIdEmpresa()));
		
		return (List<EmpleadoDTO>)query.list();
	}
	
	
	
	@Override
	public DispoEmpleadoDTO buscarRelacionDispoEmpleadoPorDispositivo(DispositivoDTO dispoDTO)
	{
		Criteria query = getCurrentSession().createCriteria(DispoEmpleadoDTO.class);
		query.createAlias("idDispositivo", "idDispositivo");
		query.createAlias("idEmpleado", "idEmpleado");
		query.add(Restrictions.eq("idDispositivo", dispoDTO));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		
		//idEmpresa
		query.add(Restrictions.eq("idDispositivo.idEmpresa", getIdEmpresa()));
		query.add(Restrictions.eq("idEmpleado.idEmpresa", getIdEmpresa()));

		
		List<DispoEmpleadoDTO> lista = (List<DispoEmpleadoDTO>)query.list();
		if(lista.size()>0)
			return lista.get(0);
		return null;
	}
	
	@Override
	public List<DispoEmpleadoDTO> buscarRelacionDispoEmpleadoPorEmpleado(EmpleadoDTO empleadoDTO)
	{
		Criteria query = getCurrentSession().createCriteria(DispoEmpleadoDTO.class);
		query.createAlias("idDispositivo", "idDispositivo");
		query.createAlias("idEmpleado", "idEmpleado");
		query.add(Restrictions.eq("idEmpleado", empleadoDTO));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		
		//idEmpresa
		query.add(Restrictions.eq("idDispositivo.idEmpresa", getIdEmpresa()));
		query.add(Restrictions.eq("idEmpleado.idEmpresa", getIdEmpresa()));

		
		/*List<DispoEmpleadoDTO> lista = (List<DispoEmpleadoDTO>)query.list();
		if(lista.size()>0)
			return lista.get(0);*/
		return (List<DispoEmpleadoDTO>)query.list();
	}
	
	
	@Override
	public DispoEmpleadoDTO buscarRelacionIdDispoEmpleadoPorEmpleado(Long idEmpleadoDTO)
	{
		Criteria query = getCurrentSession().createCriteria(DispoEmpleadoDTO.class);
		query.createAlias("idDispositivo", "idDispositivo");
		query.createAlias("idEmpleado", "idEmpleado");
		query.add(Restrictions.eq("idEmpleado.idEmpleado", idEmpleadoDTO));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		
		//idEmpresa
		query.add(Restrictions.eq("idDispositivo.idEmpresa", getIdEmpresa()));
		query.add(Restrictions.eq("idEmpleado.idEmpresa", getIdEmpresa()));

		
		List<DispoEmpleadoDTO> lista = (List<DispoEmpleadoDTO>)query.list();		
		return (DispoEmpleadoDTO)lista.get(0);		
	}
	
	public List<DispoEmpleadoDTO> buscarRelacionIdDisposEmpleadoPorEmpleado(Long idEmpleadoDTO){
		Criteria query = getCurrentSession().createCriteria(DispoEmpleadoDTO.class);
		query.createAlias("idDispositivo", "idDispositivo");
		query.createAlias("idEmpleado", "idEmpleado");
		query.add(Restrictions.eq("idEmpleado.idEmpleado", idEmpleadoDTO));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));		
		
		//idEmpresa
		query.add(Restrictions.eq("idDispositivo.idEmpresa", getIdEmpresa()));
		query.add(Restrictions.eq("idEmpleado.idEmpresa", getIdEmpresa()));

		return (List<DispoEmpleadoDTO>)query.list();
	}
}
