package mx.com.teclo.smm.persistencia.mybatis.dao.dispositivo;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import mx.com.teclo.smm.persistencia.vo.monitoreo.CoordVO;
import mx.com.teclo.smm.persistencia.vo.monitoreo.EventoVO;

@Mapper
public interface DispositivoMyBatisDAO {

	String SIGUIENTE_ID_DISPOSITIVO = "SELECT NVL(MAX(ID_DISPOSITIVO), 0)+1 FROM TMM011D_DP_DISPOSITIVOS";

	String SIGUIENTE_ID_DISPOSITIVO_INFORMACION = "SELECT NVL(MAX(ID_INFORMACION_DISP), 0)+1 FROM TMM053D_DP_INFORMACION_DISP";

	String SIGUIENTE_ID_DISPOSITIVO_EMPLEADO = "SELECT NVL(MAX(ID_DISP_EMP), 0)+1 FROM TMM016D_DP_DISP_EMPLEADOS";
	
	//String CONSULTA_HISTORICO_DISPOSITIVO = "SELECT NU_LATITUD as lat, NU_LONGITUD as lon FROM TMM_DP_DISP_1_POSICION";
	
	String CONSULTA_HISTORICO_DISPOSITIVO = "SELECT NU_LATITUD as lat, NU_LONGITUD as lon FROM ${tablaDispo}";
	
	String C_H_POR_FECHA_F = CONSULTA_HISTORICO_DISPOSITIVO + " WHERE  NU_LATITUD <> 0 AND NU_LONGITUD <> 0 AND FH_REGISTRO BETWEEN TO_DATE(#{fechaInicio},'dd/MM/yyyy HH24:MI:SS') AND TO_DATE(#{fechaFin},'dd/MM/yyyy HH24:MI:SS') ORDER BY FH_REGISTRO ASC";
	
	String C_H_POR_DIA_F = CONSULTA_HISTORICO_DISPOSITIVO + " WHERE NU_LATITUD <> 0 AND NU_LONGITUD <> 0 AND FH_REGISTRO BETWEEN TO_DATE('${fecha} 00:00:00','dd/MM/yyyy HH24:MI:SS') AND TO_DATE('${fecha} 23:59:59','dd/MM/yyyy HH24:MI:SS') ORDER BY FH_REGISTRO ASC";
	
	String C_H_POR_FECHA = CONSULTA_HISTORICO_DISPOSITIVO + " WHERE FH_REGISTRO BETWEEN TO_DATE(#{fechaInicio},'dd/MM/yyyy HH24:MI:SS') AND TO_DATE(#{fechaFin},'dd/MM/yyyy HH24:MI:SS') ORDER BY FH_REGISTRO ASC";
	
	String C_H_POR_DIA = CONSULTA_HISTORICO_DISPOSITIVO + " WHERE FH_REGISTRO BETWEEN TO_DATE('${fecha} 00:00:00','dd/MM/yyyy HH24:MI:SS') AND TO_DATE('${fecha} 23:59:59','dd/MM/yyyy HH24:MI:SS') ORDER BY FH_REGISTRO ASC";
	
	String CONSULTA_EVENTO_HISTORICO_DISPOSITIVO = "SELECT tmmTiEv.ID_TIPO_EVENTO as id,  tmmTiEv.IMG_TIPO_EVENTO as image, tmmEv.NB_EVENTO as tipo, TO_CHAR(tmmDisp.FH_REGISTRO,'dd/MM/yyyy HH24:MI:SS') as fecha, tmmEv.TX_EVENTO as descripcion, tmmDisp.TX_DIRECCION as direccion, tmmDisp.NU_LATITUD lat, tmmDisp.NU_LONGITUD as lng FROM ${tablaDispo} tmmDisp INNER JOIN TMM012C_CT_EVENTOS tmmEv ON (tmmDisp.ID_EVENTO = tmmEv.ID_EVENTO) JOIN TMM006C_CT_TIPO_EVENTOS tmmTiEv ON (tmmEv.ID_TIPO_EVENTO = tmmTiEv.ID_TIPO_EVENTO)";
	
	String C_EV_H_POR_FECHA_F = CONSULTA_EVENTO_HISTORICO_DISPOSITIVO + " WHERE tmmDisp.NU_LATITUD <> 0 AND tmmDisp.NU_LONGITUD <> 0 AND tmmDisp.FH_REGISTRO BETWEEN TO_DATE(#{fechaInicio},'dd/MM/yyyy HH24:MI:SS') AND TO_DATE(#{fechaFin},'dd/MM/yyyy HH24:MI:SS') ORDER BY tmmDisp.FH_REGISTRO ASC";
	
	String C_EV_H_POR_DIA_F = CONSULTA_EVENTO_HISTORICO_DISPOSITIVO + " WHERE tmmDisp.NU_LATITUD <> 0 AND tmmDisp.NU_LONGITUD <> 0 AND tmmDisp.FH_REGISTRO BETWEEN TO_DATE('${fecha} 00:00:00','dd/MM/yyyy HH24:MI:SS') AND TO_DATE('${fecha} 23:59:59','dd/MM/yyyy HH24:MI:SS') ORDER BY tmmDisp.FH_REGISTRO ASC";
	
	String C_EV_H_POR_FECHA = CONSULTA_EVENTO_HISTORICO_DISPOSITIVO + " WHERE tmmDisp.FH_REGISTRO BETWEEN TO_DATE(#{fechaInicio},'dd/MM/yyyy HH24:MI:SS') AND TO_DATE(#{fechaFin},'dd/MM/yyyy HH24:MI:SS') ORDER BY tmmDisp.FH_REGISTRO ASC";
	
	String C_EV_H_POR_DIA = CONSULTA_EVENTO_HISTORICO_DISPOSITIVO + " WHERE tmmDisp.FH_REGISTRO BETWEEN TO_DATE('${fecha} 00:00:00','dd/MM/yyyy HH24:MI:SS') AND TO_DATE('${fecha} 23:59:59','dd/MM/yyyy HH24:MI:SS') ORDER BY tmmDisp.FH_REGISTRO ASC";

	String CONSULTA_EXISTE_IMEI = "SELECT COUNT(ID_DISPOSITIVO) FROM TMM053D_DP_INFORMACION_DISP WHERE NU_IMEI = #{imei}";
	
	@Select(SIGUIENTE_ID_DISPOSITIVO)
	public Long generarSiguienteId();

	@Select(SIGUIENTE_ID_DISPOSITIVO_INFORMACION)
	public Long generarSiguienteIdDispInfo();

	@Select(SIGUIENTE_ID_DISPOSITIVO_EMPLEADO)
	public Long generarSiguienteIdDispEmpl();
	
	@Select(CONSULTA_HISTORICO_DISPOSITIVO)
	public List<CoordVO> consultaRutaHistorico();
	//public CoordVO consultaRutaHistorico(@Param("tablaDispo") String tablaDispo);
	
	@Select(C_H_POR_FECHA)
	public List<CoordVO> consultaRutaHistoricoPorFecha(@Param("tablaDispo") String tablaDispo, @Param("fechaInicio") String fechaInicio, @Param("fechaFin") String fechaFin);
	
	@Select(C_H_POR_DIA)
	public List<CoordVO> consultaRutaHistoricoPorDia(@Param("tablaDispo") String tablaDispo, @Param("fecha") String fecha);
	
	@Select(C_H_POR_FECHA_F)
	public List<CoordVO> consultaRutaHistoricoPorFechaFiltrada(@Param("tablaDispo") String tablaDispo, @Param("fechaInicio") String fechaInicio, @Param("fechaFin") String fechaFin);
	
	@Select(C_H_POR_DIA_F)
	public List<CoordVO> consultaRutaHistoricoPorDiaFiltrada(@Param("tablaDispo") String tablaDispo, @Param("fecha") String fecha);
	
	@Select(C_EV_H_POR_FECHA)
	public List<EventoVO> consultaEventoRutaHistoricoPorFecha(@Param("tablaDispo") String tablaDispo, @Param("fechaInicio") String fechaInicio, @Param("fechaFin") String fechaFin);
	
	@Select(C_EV_H_POR_DIA)
	public List<EventoVO> consultaEventoRutaHistoricoPorDia(@Param("tablaDispo") String tablaDispo, @Param("fecha") String fecha);
	
	@Select(C_EV_H_POR_FECHA_F)
	public List<EventoVO> consultaEventoRutaHistoricoPorFechaFiltrada(@Param("tablaDispo") String tablaDispo, @Param("fechaInicio") String fechaInicio, @Param("fechaFin") String fechaFin);
	
	@Select(C_EV_H_POR_DIA_F)
	public List<EventoVO> consultaEventoRutaHistoricoPorDiaFiltrada(@Param("tablaDispo") String tablaDispo, @Param("fecha") String fecha);

	@Select(CONSULTA_EXISTE_IMEI)
	public Long existeIMEI(@Param("imei") String imei);
}
