package mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="TMM006C_CT_TIPO_EVENTOS")
public class TipoEventoDTO implements Serializable{

	private static final long serialVersionUID = 2570676161458817680L;


	@Id
	@GeneratedValue(generator="id_tevento_generator",strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="id_tevento_generator", sequenceName = "SQTMM006C_CT_TIPO_EVT",allocationSize=1)
	@Column(name="ID_TIPO_EVENTO")
	private Long idTipoEvento;
	@Column(name="CD_TIPO_EVENTO")
	private String cdTipoEvento;
	@Column(name="NB_TIPO_EVENTO")
	private String nbTipoEvento;
	@Column(name="TX_TIPO_EVENTO")
	private String txTipoEvento;
	@Column(name="ST_ACTIVO")
	private Boolean stActivo;
	@Column(name="FH_CREACION")
	private Date fhCreacion;
	@Column(name="ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name="FH_MODIFICA")
	private Date fhModifica;
	@Column(name="ID_USR_MODIFICA")
	private Long idUsrModifica;
	@Column(name="IMG_TIPO_EVENTO")
	private String imgTipoEvento;


	public String getImgTipoEvento() {
		return imgTipoEvento;
	}
	public void setImgTipoEvento(String imgTipoEvento) {
		this.imgTipoEvento = imgTipoEvento;
	}
	public Long getIdTipoEvento() {
		return idTipoEvento;
	}
	public void setIdTipoEvento(Long idTipoEvento) {
		this.idTipoEvento = idTipoEvento;
	}
	public String getCdTipoEvento() {
		return cdTipoEvento;
	}
	public void setCdTipoEvento(String cdTipoEvento) {
		this.cdTipoEvento = cdTipoEvento;
	}
	public String getNbTipoEvento() {
		return nbTipoEvento;
	}
	public void setNbTipoEvento(String nbTipoEvento) {
		this.nbTipoEvento = nbTipoEvento;
	}
	public String getTxTipoEvento() {
		return txTipoEvento;
	}
	public void setTxTipoEvento(String txTipoEvento) {
		this.txTipoEvento = txTipoEvento;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModifica() {
		return fhModifica;
	}
	public void setFhModifica(Date fhModifica) {
		this.fhModifica = fhModifica;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}

}
