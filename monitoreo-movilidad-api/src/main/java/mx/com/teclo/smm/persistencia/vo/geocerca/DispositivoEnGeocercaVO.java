package mx.com.teclo.smm.persistencia.vo.geocerca;

public class DispositivoEnGeocercaVO {
	private Long id;
	private String nombre;
	private String serial;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
}
