package mx.com.teclo.smm.persistencia.hibernate.dto.empresa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.teclo.smm.persistencia.hibernate.dto.perfil.PerfilDTO;

@Entity
@Table(name = "TMM046D_EM_EMPRESA_PERFIL")
public class EmpresaPerfilDTO implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 4189398691600018392L;

	@Id
	@GeneratedValue(generator="id_empresa_perfil",strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="id_empresa_perfil", sequenceName = "SQMM075B_EMPRESA_PERFIL",allocationSize=1)
	@Column(name = "ID_EM_PERFIL")
	private Long idEmPerfil;

	@Column(name = "ID_EMPRESA")
	private Long idEmpresa;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PERFIL")
	private PerfilDTO idPerfil;

	@Column(name = "ST_ACTIVO")
	private Integer stActivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;
	public Long getIdEmPerfil() {
		return idEmPerfil;
	}
	public void setIdEmPerfil(Long idEmPerfil) {
		this.idEmPerfil = idEmPerfil;
	}
	public Long getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	public PerfilDTO getIdPerfil() {
		return idPerfil;
	}
	public void setIdPerfil(PerfilDTO idPerfil) {
		this.idPerfil = idPerfil;
	}
	public Integer getStActivo() {
		return stActivo;
	}
	public void setStActivo(Integer stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}




}
