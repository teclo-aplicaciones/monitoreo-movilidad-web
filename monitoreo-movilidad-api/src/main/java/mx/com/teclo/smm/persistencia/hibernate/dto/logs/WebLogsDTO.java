package mx.com.teclo.smm.persistencia.hibernate.dto.logs;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mx.com.teclo.arquitectura.ortogonales.persistencia.hibernate.dto.seguridad.AplicacionesDTO;

@Entity
@Table(name="TAQ056C_LG_WEB_LOGS")
public class WebLogsDTO implements Serializable{

	private static final long serialVersionUID = -6977734763697688343L;

	@Id
	@SequenceGenerator(name = "aq056dSeq", sequenceName="SQAQ056C_LG_WEB_LOGS", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "aq056dSeq")
	@Column(name = "ID_WEB_LOG", unique = true, nullable = false)
	private Long idLogWeb;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_APLICACION", nullable = false)
	private AplicacionesDTO aplicacion;

	@Column(name="NB_LOG", nullable = false)
	private String nbLog;

	@Column(name="TX_LOG", nullable = false)
	private String txLog;

	@Column(name="NB_RUTA_ARCHIVO", nullable = false)
	private String nbRutaArchivo;

	@Column(name="NB_EXTENSION", nullable = false)
	private String nbExtension;

	@Column(name = "ST_ACTIVO", nullable = false)
	private Integer stActivo;

	@Column(name = "ID_USR_CREACION", nullable = false)
	private Long idUsrCreacion;

	@Column(name = "FH_CREACION")
	private Date fhCreacion;

	@Column(name = "ID_USR_MODIFICA", nullable = false)
	private Long idUsrModifica;

	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;

	public Long getIdLogWeb() {
		return idLogWeb;
	}

	public void setIdLogWeb(Long idLogWeb) {
		this.idLogWeb = idLogWeb;
	}

	public AplicacionesDTO getAplicacion() {
		return aplicacion;
	}

	public void setAplicacion(AplicacionesDTO aplicacion) {
		this.aplicacion = aplicacion;
	}

	public String getNbLog() {
		return nbLog;
	}

	public void setNbLog(String nbLog) {
		this.nbLog = nbLog;
	}

	public String getTxLog() {
		return txLog;
	}

	public void setTxLog(String txLog) {
		this.txLog = txLog;
	}

	public String getNbRutaArchivo() {
		return nbRutaArchivo;
	}

	public void setNbRutaArchivo(String nbRutaArchivo) {
		this.nbRutaArchivo = nbRutaArchivo;
	}

	public String getNbExtension() {
		return nbExtension;
	}

	public void setNbExtension(String nbExtension) {
		this.nbExtension = nbExtension;
	}

	public Integer getStActivo() {
		return stActivo;
	}

	public void setStActivo(Integer stActivo) {
		this.stActivo = stActivo;
	}

	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}

	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}

	public Date getFhCreacion() {
		return fhCreacion;
	}

	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}

	public Long getIdUsrModifica() {
		return idUsrModifica;
	}

	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}

	public Date getFhModificacion() {
		return fhModificacion;
	}

	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
}
