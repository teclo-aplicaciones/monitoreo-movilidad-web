package mx.com.teclo.smm.persistencia.hibernate.dao.componenteweb;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.componeteweb.ComponenteWebDTO;

public interface ComponenteWebDAO extends BaseDAO<ComponenteWebDTO> {
	ComponenteWebDTO obtenerComponenteWebPorId(Long idComponenteWeb);	
}
