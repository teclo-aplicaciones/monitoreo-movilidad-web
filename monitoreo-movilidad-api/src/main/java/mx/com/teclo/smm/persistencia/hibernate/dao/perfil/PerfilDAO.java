package mx.com.teclo.smm.persistencia.hibernate.dao.perfil;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.perfil.PerfilDTO;

/**
 *  Copyright (c) 2018, Teclo Mexicana.
 *
 *  Descripcion					: UsuarioDAO
 *  Historial de Modificaciones	:
 *  Descripcion del Cambio 		: Creacion
 *  @author 					: fjmb
 *  @version 					: 1.0
 *  Fecha 						: 05/Diciembre/2018
 */

public interface PerfilDAO extends BaseDAO<PerfilDTO> {

	public PerfilDTO findProfileByPerfil(PerfilDTO perfilDTO);

	/*
	public List<PerfilDTO> findProfilesByApp(String cdApplication);
	public PerfilDTO findProfileById(Long id, String cdApplication, boolean isByApplication);
	public List<PerfilDTO> findAllProfilesByApp(String cdApplication);
	public PerfilDTO findUniqueProfileById(Long id, String cdApplication, boolean isByApplication);
	public Long findNextValue();
	*/
}
