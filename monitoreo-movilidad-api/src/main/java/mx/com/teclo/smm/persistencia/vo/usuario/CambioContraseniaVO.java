package mx.com.teclo.smm.persistencia.vo.usuario;

public class CambioContraseniaVO {
	private String password;
	private String oldPassword;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	@Override
	public String toString() {
		return "CambioContraseniaVO [password=" + password + ", oldPassword=" + oldPassword + ", getPassword()="
				+ getPassword() + ", getOldPassword()=" + getOldPassword() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}

}
