package mx.com.teclo.smm.persistencia.hibernate.dao.comandos;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.hql.internal.ast.tree.OrderByClause;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.BitacoraComandoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.BitacoraComandoDetalleDTO;

@SuppressWarnings("unchecked")
@Repository("BitacoraComandoDetalleDAO")
public class BitacoraComandoDetalleDAOImpl extends BaseDAOImpl<BitacoraComandoDetalleDTO> implements BitacoraComandoDetalleDAO{

	
	@Override
	@Transactional(readOnly = true)
	public List<BitacoraComandoDetalleDTO> obtenerBitacoraComandoDetalle(Long idBitacora){
		Criteria query = getCurrentSession().createCriteria(BitacoraComandoDetalleDTO.class);
		query.add(Restrictions.eq("idBitacora", idBitacora));	
		query.addOrder(Order.asc("idBitacoraDetalle"));
		return (List<BitacoraComandoDetalleDTO>)query.list();
	}
	
}
