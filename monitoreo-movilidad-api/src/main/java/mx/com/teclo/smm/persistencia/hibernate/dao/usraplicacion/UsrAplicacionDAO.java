package mx.com.teclo.smm.persistencia.hibernate.dao.usraplicacion;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.usuario.AplicacionDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.usuario.UsrAplicacionDTO;

/**
 *  Copyright (c) 2018, Teclo Mexicana. 
 * 
 *  Descripcion					: UsuarioDAO
 *  Historial de Modificaciones	: 
 *  Descripcion del Cambio 		: Creacion
 *  @author 					: fjmb
 *  @version 					: 1.0 
 *  Fecha 						: 05/Diciembre/2018
 */

public interface UsrAplicacionDAO extends BaseDAO<UsrAplicacionDTO> {

	public UsrAplicacionDTO findUserAplication(String cdApplication, Long idUsuario);
	
	public AplicacionDTO getApplicationByCode(String cdApplication);

}
