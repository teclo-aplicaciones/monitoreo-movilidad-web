package mx.com.teclo.smm.persistencia.hibernate.dao.geocerca;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.geocerca.TipoGeocercaDTO;

public interface TipoGeocercaDAO extends BaseDAO<TipoGeocercaDTO>{
	public TipoGeocercaDTO buscarTipoGeocercaPorId(Long id);
	public List<TipoGeocercaDTO> buscarTipoGeocerca();
	public TipoGeocercaDTO activarDesactivarBuscarTipoGeocercaPorId(Long id);
	public List<TipoGeocercaDTO> activarDesactivarBuscarTipoGeocerca();
}
