package mx.com.teclo.smm.util.enumerados;

public enum ComandoBitacoraStatus {
	ENVIO(1L),PROCESO(2L),RECIBIDO(3L),ERROR(4L),PENDIENTE(5L);
	
	private Long idStatus;
	
	private ComandoBitacoraStatus(Long idStatus) {
		this.idStatus = idStatus;
	}

	public Long getIdStatus() {
		return idStatus;
	}

	public void setIdStatus(Long idStatus) {
		this.idStatus = idStatus;
	}
	
	
}

