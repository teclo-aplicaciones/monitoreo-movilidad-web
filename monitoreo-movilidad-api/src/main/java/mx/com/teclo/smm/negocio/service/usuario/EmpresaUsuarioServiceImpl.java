package mx.com.teclo.smm.negocio.service.usuario;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.arquitectura.ortogonales.exception.NotFoundException;
import mx.com.teclo.arquitectura.ortogonales.seguridad.util.TokenUtils;
import mx.com.teclo.arquitectura.ortogonales.service.comun.UsuarioFirmadoService;
import mx.com.teclo.arquitectura.ortogonales.util.ResponseConverter;
import mx.com.teclo.smm.negocio.service.empresa.EmpresaService;
import mx.com.teclo.smm.persistencia.hibernate.dao.perfil.PerfilUsuarioDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.usraplicacion.UsrAplicacionDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.usuario.EmpresaUsuarioDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.usuario.UsuarioDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.empresa.EmpresaUsuarioDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.perfil.PerfilDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.perfil.PerfilUsuarioDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.usuario.AplicacionDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.usuario.UsrAplicacionDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.usuario.UsuarioDTO;
import mx.com.teclo.smm.persistencia.vo.perfil.PerfilVO;
import mx.com.teclo.smm.persistencia.vo.usuario.UsuarioVO;
import mx.com.teclo.smm.util.comun.FilterValuesVO;
import mx.com.teclo.smm.util.comun.ResponseVO;
import mx.com.teclo.smm.util.enumerados.PerfilesEnum;

@Service
public class EmpresaUsuarioServiceImpl implements EmpresaUsuarioService {

	@Autowired
	private EmpresaUsuarioDAO empresaUsuarioDAO;

	@Autowired
	@Qualifier("usuariosDAO")
	private UsuarioDAO usuarioDAO;

	@Autowired
	private UsuarioFirmadoService usuarioFirmadoService;

	@Autowired
	private PerfilUsuarioDAO perfilUsuarioDAO;

	@Autowired
	private UsrAplicacionDAO usrAplicacionDAO;

	@Value("${app.config.codigo}")
	private String codeApplication;

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private EmpresaService empresaService;


	@Override
	@Transactional(readOnly = true)
	public UsuarioVO findUserById(Long id, String cdApplication) {
		UsuarioVO usuarioVO = null;
		UsuarioDTO usuarioDTO = null;
		usuarioDTO = empresaUsuarioDAO.findUserById(id, cdApplication);
	    usuarioVO = ResponseConverter.copiarPropiedadesFull(usuarioDTO, UsuarioVO.class);

	    return usuarioVO;
	}

	@Override
	@Transactional(readOnly = true)
	public List<UsuarioVO> findUsersByParams(String parameter, String value) {

		List<UsuarioDTO> usuariosDTO = null;
		List<UsuarioVO> usuariosVO = new ArrayList<UsuarioVO>();
		PerfilVO withoutProfile = new PerfilVO();
		PerfilVO profile = new PerfilVO();

		withoutProfile.setIdPerfil(new Long(0));
		withoutProfile.setAplicacion(null);
		withoutProfile.setCdPerfil(PerfilesEnum.SIN_PERFIL.getCdPerfil());
		withoutProfile.setNbPerfil(PerfilesEnum.SIN_PERFIL.getNbPerfil());
		withoutProfile.setTxPerfil(PerfilesEnum.SIN_PERFIL.getTxPerfil());
		withoutProfile.setStActivo(1);

		usuariosDTO = empresaUsuarioDAO.findUsersByParams(parameter, value, codeApplication, true);

		for (UsuarioDTO usuarioDTO : usuariosDTO) {

			UsuarioVO usuarioVO = ResponseConverter.copiarPropiedadesFull(usuarioDTO, UsuarioVO.class);

			if(usuarioDTO.getPerfilUsuario().isEmpty()
				|| usuarioDTO.getPerfilUsuario().get(0).getStActivo().equals(0)) {

				profile = withoutProfile;

			} else {
				profile = ResponseConverter.copiarPropiedadesFull(usuarioDTO.getPerfilUsuario().get(0).getPerfil(), PerfilVO.class);
			}

			usuarioVO.setPerfil(profile);

			usuariosVO.add(usuarioVO);
		}

		return usuariosVO;
	}

	@Override
	@Transactional
	public boolean saveOrUpdateUser(UsuarioVO userVO, String action) throws NotFoundException {

		if(action.equals("new")) {

			boolean isExistUser = false;
			ResponseVO response = new ResponseVO();

			isExistUser = empresaUsuarioDAO.isUserExist(userVO, action);

			if(isExistUser) {
				response.setMessage("El usuario ya existe");
				throw new NotFoundException(response.getMessage());
			}
		}

		boolean isSaveUser = false;

		UsuarioDTO userDTO = null;
		PerfilUsuarioDTO profileUserDTO = null;
		UsrAplicacionDTO userAplicationDTO = null;
		PerfilDTO profileDTO = new PerfilDTO();
		AplicacionDTO applicationDTO = new AplicacionDTO();

		List<PerfilUsuarioDTO> profileUserList = new ArrayList<PerfilUsuarioDTO>();
		List<UsrAplicacionDTO> userAplicationList = new ArrayList<UsrAplicacionDTO>();

		userAplicationDTO = usrAplicacionDAO.findUserAplication(codeApplication, userVO.getIdUsuario());

		if(userAplicationDTO != null) {

			if(!userAplicationDTO.getUsuario().getPerfilUsuario().isEmpty()) {
				Long idProfileOld = userAplicationDTO.getUsuario().getPerfilUsuario().get(0).getPerfil().getIdPerfil();
				profileUserDTO = perfilUsuarioDAO.getPerfilUsuario(userVO.getIdUsuario(), idProfileOld);
			} else {
				profileUserDTO = new PerfilUsuarioDTO();
			}

		}

		if(!userVO.getPerfil().getNbPerfil().equals(PerfilesEnum.SIN_PERFIL.getNbPerfil())) {
			ResponseConverter.copiarPropriedades(profileDTO, userVO.getPerfil());
			ResponseConverter.copiarPropriedades(applicationDTO, userVO.getPerfil().getAplicacion());
		} else {
			applicationDTO = usrAplicacionDAO.getApplicationByCode(codeApplication);
			profileDTO = null;

			if(action.equals("edit") ) {
				profileUserDTO.setStActivo(0);
			}
		}

		if(profileUserDTO == null && userAplicationDTO == null) {

			// NEW
			profileUserDTO = new PerfilUsuarioDTO();
			userAplicationDTO = new UsrAplicacionDTO();

			userDTO = ResponseConverter.copiarPropiedadesFull(userVO, UsuarioDTO.class);

			if(profileDTO != null) {
				profileUserDTO.setPerfil(profileDTO);
			}

			profileUserDTO.setUsuario(userDTO);
			profileUserDTO.setIdUsrCreacion(usuarioFirmadoService.getUsuarioFirmadoVO().getId());
			profileUserDTO.setIdUsrModifica(usuarioFirmadoService.getUsuarioFirmadoVO().getId());
			profileUserDTO.setFhCreacion(new Date());
			profileUserDTO.setFhModificacion(new Date());
	 		profileUserDTO.setStActivo(1);

	 		userAplicationDTO.setAplicacion(applicationDTO);
	 		userAplicationDTO.setUsuario(userDTO);
			userAplicationDTO.setIdUsrCreacion(usuarioFirmadoService.getUsuarioFirmadoVO().getId());
			userAplicationDTO.setIdUsrModifica(usuarioFirmadoService.getUsuarioFirmadoVO().getId());
			userAplicationDTO.setStActivo(true);
			userAplicationDTO.setFhCreacion(new Date());
			userAplicationDTO.setFhModificacion(new Date());

			userDTO.setIdUsrCreacion(usuarioFirmadoService.getUsuarioFirmadoVO().getId());
			userDTO.setIdUsrModifica(usuarioFirmadoService.getUsuarioFirmadoVO().getId());
			userDTO.setFhCreacion(new Date());
			userDTO.setFhModificacion(new Date());
			userDTO.setNbContrasenia(empresaUsuarioDAO.encriptarCampo(userVO.getNbContrasenia()));
			userDTO.setFhModifContrasenia(new Date());
			userDTO.setStContrasenia(1);

			//idEmpresa
			EmpresaUsuarioDTO empresaUsuario = new EmpresaUsuarioDTO();
			empresaUsuario.setUsuario(userDTO);
			empresaUsuario.setStActivo(1);
			empresaUsuario.setIdEmpresa(empresaService.getIdEmpresa());
			empresaUsuario.setIdUsrCreacion(usuarioFirmadoService.getUsuarioFirmadoVO().getId());
			empresaUsuario.setIdUsrModifica(usuarioFirmadoService.getUsuarioFirmadoVO().getId());
			empresaUsuario.setFhModificacion(new Date());
			empresaUsuario.setFhCreacion(new Date());
			userDTO.setEmpresaUsuario(empresaUsuario);


		} else {

			ResponseVO response = new ResponseVO();

			boolean isExistUser = false;
			boolean isDifferentUser = (userAplicationDTO.getUsuario().getNbUsuario().equals(userVO.getNbUsuario())
					&& userAplicationDTO.getUsuario().getNbApaterno().equals(userVO.getNbApaterno())
					&& userAplicationDTO.getUsuario().getNbAmaterno().equals(userVO.getNbAmaterno()))
					? false
					: true;

			if(isDifferentUser) {

				isExistUser = empresaUsuarioDAO.isUserExist(userVO, action);

				if(isExistUser) {
					response.setMessage("El usuario ya existe");
					throw new NotFoundException(response.getMessage());
				}
			}

			// EDIT
			userDTO = userAplicationDTO.getUsuario();

			profileUserDTO.setIdUsrModifica(usuarioFirmadoService.getUsuarioFirmadoVO().getId());
			profileUserDTO.setFhModificacion(new Date());

			if(profileUserDTO.getUsuario() == null) {
				profileUserDTO.setUsuario(userDTO);
				profileUserDTO.setIdUsrCreacion(usuarioFirmadoService.getUsuarioFirmadoVO().getId());
				profileUserDTO.setIdUsrModifica(usuarioFirmadoService.getUsuarioFirmadoVO().getId());
				profileUserDTO.setFhCreacion(new Date());
				profileUserDTO.setFhModificacion(new Date());
		 		profileUserDTO.setStActivo(1);
			}

			if(profileDTO != null) {
				profileUserDTO.setPerfil(profileDTO);
				profileUserDTO.setStActivo(1);
			}

			userAplicationDTO.setIdUsrModifica(usuarioFirmadoService.getUsuarioFirmadoVO().getId());
			userAplicationDTO.setFhModificacion(new Date());

			userDTO.setFhModificacion(new Date());
			userDTO.setIdUsrModifica(usuarioFirmadoService.getUsuarioFirmadoVO().getId());
			userDTO.setLbFoto(userVO.getLbFoto());
			userDTO.setNbUsuario(userVO.getNbUsuario());
			userDTO.setNbAmaterno(userVO.getNbAmaterno());
			userDTO.setNbApaterno(userVO.getNbApaterno());
			userDTO.setNbContrasenia(userVO.getNbContrasenia());
			userDTO.setNbEmail(userVO.getNbEmail());
			userDTO.setNuTelefono(userVO.getNuTelefono());
			userDTO.setStActivo(userVO.getStActivo());

		}

		if(profileDTO != null) {
			profileUserList.add(profileUserDTO);
			userDTO.setPerfilUsuario(profileUserList);
		}

		userAplicationList.add(userAplicationDTO);

		userDTO.setUsuarioAplicacion(userAplicationList);


		usuarioDAO.saveOrUpdate(userDTO);

		if (userDTO.getIdUsuario() != null) {
			isSaveUser = true;
		}

		return isSaveUser;
	}

	@Override
	public List<FilterValuesVO> getQueryParamsUser() {

		List<FilterValuesVO> filterValues = new ArrayList<FilterValuesVO>();

		filterValues.add(new FilterValuesVO(1, "TODOS", "TODOS"));
		filterValues.add(new FilterValuesVO(2, "USERNAME", "USUARIO"));
		filterValues.add(new FilterValuesVO(3, "NOMBRE", "NOMBRE(S)"));
		filterValues.add(new FilterValuesVO(4, "APELLIDO", "APELLIDO(S)"));
		filterValues.add(new FilterValuesVO(5, "PERFIL", "PERFIL"));

		return filterValues;
	}

	@Override
	public List<FilterValuesVO> getQueryParamsAll() {

		List<FilterValuesVO> filterValuesForAll = new ArrayList<FilterValuesVO>();

		filterValuesForAll.add(new FilterValuesVO(1, "ACTIVES", "ACTIVOS"));
		filterValuesForAll.add(new FilterValuesVO(2, "INACTIVES", "INACTIVOS"));
		filterValuesForAll.add(new FilterValuesVO(3, "ALLSTATUS", "ACTIVOS/INACTIVOS"));

		return filterValuesForAll;
	}

	@Override
	@Transactional
	public boolean changeStatusUser(UsuarioVO usuarioVO ) {
		boolean isSaveUser = false;
		UsuarioDTO usuarioDTO = null;
		usuarioDTO = empresaUsuarioDAO.findUserByUserName(usuarioVO.getCdUsuario(), codeApplication, true, false);

		int status = usuarioDTO.getStActivo() != 0 ? 0 : 1;

		usuarioDTO.setStActivo(status);
		usuarioDTO.setIdUsrModifica(usuarioFirmadoService.getUsuarioFirmadoVO().getId());
		usuarioDTO.setFhModificacion(new Date());

		if (usuarioDTO != null) {
			isSaveUser = true;
			usuarioDAO.saveOrUpdate(usuarioDTO);
			usuarioVO.setFhModificacion(usuarioDTO.getFhModificacion());
		}
		return isSaveUser;
	}

	@Override
	@Transactional(readOnly = true)
	public boolean findExistentUser(Map<String, Object> parameters) {

		boolean isExistUser = false;
		List<UsuarioDTO> usuarios = null;
		usuarios = empresaUsuarioDAO.findExistentUser(parameters);
		if (usuarios != null)
			isExistUser = true;

		return isExistUser;
	}

	@Override
	@Transactional
	public ResponseVO updatePassword(String username, String password, String oldPassword) {
		ResponseVO responseVO = new ResponseVO();
 		UsuarioDTO usuarioDTO = null;
		usuarioDTO = empresaUsuarioDAO.findUserByUserName(username, codeApplication, true,true);
 		Pattern patron = Pattern.compile(
				"(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[a-zA-Z0-9$@$!%*?&](?=.*[!#$%&()*+,-./:;<=>?@"
						+ "\"[" + "\\" + "\"]" + "_|]){8,}");
		Matcher muestra = patron.matcher(password);

		if (usuarioDTO != null){
			String oldPasswordEncrypted = empresaUsuarioDAO.encriptarCampo(oldPassword);
			String passwordEncrypted = empresaUsuarioDAO.encriptarCampo(password);

			if(!muestra.find() || muestra.group(0) == null) {
				responseVO.setCode(409);
				responseVO.setMessage("La contraseña introducida no cumple con los requisitos de seguridad mínimos.");
				return responseVO;
 			}
			if(!oldPasswordEncrypted.equals(usuarioDTO.getNbContrasenia())) {
				responseVO.setCode(409);
				responseVO.setMessage("La contraseña actual que introdujo para el usuario " + usuarioDTO.getCdUsuario()+" es incorrecta.");
				return responseVO;
 			}
			if(passwordEncrypted.equals(usuarioDTO.getNbContrasenia())) {
				responseVO.setCode(409);
				responseVO.setMessage("La contraseña introducida no puede ser iguales a la actual.");
				return responseVO;

			}
			usuarioDTO.setNbContrasenia(passwordEncrypted);
			usuarioDTO.setFhModifContrasenia(new Date());
	 		usuarioDAO.saveOrUpdate(usuarioDTO);
			responseVO.setCode(200);
			responseVO.setMessage("La contraseña se cambió correctamente.");

		}else
		{
			responseVO.setCode(404);
			responseVO.setMessage("No existe el usuario que desea cambiar la contraseña.");
			return responseVO;
		}

		return responseVO;
	}

	@Override
	@Transactional()
	public boolean resetPassword(String username) {
		boolean isExistUser = false;
		UsuarioDTO usuarioDTO = null;
		usuarioDTO = empresaUsuarioDAO.findUserByUserName(username, codeApplication, true, true);
		usuarioDTO.setNbContrasenia(empresaUsuarioDAO.encriptarCampo(usuarioDTO.getCdUsuario()));
 		usuarioDAO.saveOrUpdate(usuarioDTO);
 		if (usuarioDTO.getIdUsuario() != null)
 			isExistUser = true;

		return isExistUser;
	}

	@Override
	@Transactional
	public List<UsuarioVO> findUsersByProfile(String cdProfile) {

		List<UsuarioDTO> usuariosDTO = null;
		List<UsuarioVO> usuariosVO = new ArrayList<UsuarioVO>();
		PerfilVO withoutProfile = new PerfilVO();
		PerfilVO profile = new PerfilVO();

		withoutProfile.setIdPerfil(new Long(0));
		withoutProfile.setAplicacion(null);
		withoutProfile.setCdPerfil(PerfilesEnum.SIN_PERFIL.getCdPerfil());
		withoutProfile.setNbPerfil(PerfilesEnum.SIN_PERFIL.getNbPerfil());
		withoutProfile.setTxPerfil(PerfilesEnum.SIN_PERFIL.getTxPerfil());
		withoutProfile.setStActivo(1);

		usuariosDTO = empresaUsuarioDAO.findUsersByProfile(cdProfile, codeApplication, true);

		for (UsuarioDTO usuarioDTO : usuariosDTO) {

			UsuarioVO usuarioVO = ResponseConverter.copiarPropiedadesFull(usuarioDTO, UsuarioVO.class);

			if(cdProfile.endsWith(PerfilesEnum.SIN_PERFIL.getCdPerfil())) {
				if(usuarioDTO.getPerfilUsuario().isEmpty()
					|| usuarioDTO.getPerfilUsuario().get(0).getStActivo().equals(0)) {

					usuarioVO.setPerfil(withoutProfile);
					usuariosVO.add(usuarioVO);

				}
			} else {
				profile = ResponseConverter.copiarPropiedadesFull(usuarioDTO.getPerfilUsuario().get(0).getPerfil(), PerfilVO.class);

				usuarioVO.setPerfil(profile);
				usuariosVO.add(usuarioVO);
			}
		}

		return usuariosVO;
	}

	@Override
	@Transactional
	public String toggleEncryption(String password, String action) {

		switch(action) {
			case "encrypt":
				password = empresaUsuarioDAO.encriptarCampo(password);
				break;
			case "decrypt":
				password = empresaUsuarioDAO.desencriptarCampo(password);
				break;
			default:
				break;
		}

		return password;
	}
}
