package mx.com.teclo.smm.persistencia.vo.geocerca;

import java.util.List;

public class GeoNotificacionVO {
	private Long tipoEnvio;
	private List<GeoNotiOpcionVO> opciones;
	
	public Long getTipoEnvio() {
		return tipoEnvio;
	}
	public void setTipoEnvio(Long tipoEnvio) {
		this.tipoEnvio = tipoEnvio;
	}
	public List<GeoNotiOpcionVO> getOpciones() {
		return opciones;
	}
	public void setOpciones(List<GeoNotiOpcionVO> opciones) {
		this.opciones = opciones;
	}
}
