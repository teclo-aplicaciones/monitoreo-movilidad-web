package mx.com.teclo.smm.persistencia.vo.evento;

import java.util.Map;

public class TCAnualVO {
	private String tcMes;
	private Map<String, Long> tcMapeo;
	
	public String getTcMes() {
		return tcMes;
	}
	public void setTcMes(String tcMes) {
		this.tcMes = tcMes;
	}
	public Map<String, Long> getTcMapeo() {
		return tcMapeo;
	}
	public void setTcMapeo(Map<String, Long> tcMapeo) {
		this.tcMapeo = tcMapeo;
	}
	
	
}
