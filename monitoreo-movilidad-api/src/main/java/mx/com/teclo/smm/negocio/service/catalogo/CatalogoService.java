package mx.com.teclo.smm.negocio.service.catalogo;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import mx.com.teclo.smm.persistencia.vo.JsonObjectVO;
import mx.com.teclo.smm.persistencia.vo.catalogo.CatalogoVO;
import mx.com.teclo.smm.persistencia.vo.catalogo.ItemCatalogoVO;
import mx.com.teclo.smm.persistencia.vo.geocerca.GeoNotiOpcionVO;

public interface CatalogoService {
	public List<JsonObjectVO> buscarCatalogo();
	public CatalogoVO buscarCatalogoPorId(int id);
	public List<CatalogoVO> buscarCatalogosPorId(Integer[] id);
	
	//Catalogo para busqueda de Monitoreo
	public List<CatalogoVO> catalogoMonitoreo();
	
	//Catalogo para busqueda de Geocercas
	public List<CatalogoVO>catalogoBusquedaGeocerca();
	
	//Catalogo para busqueda de Notificaciones
	public List<CatalogoVO> catalogoBusquedaNotificacion();
	
	//Catalogo para busqueda de Grupo
	public List<CatalogoVO> catalogoBusquedaGrupo();
	
	//Catalogo para Tipo de Dispositivos
	public List<CatalogoVO> catalogoTipoDispositivo();
	
	public List<CatalogoVO> catalogoTipoMedio();
	
	//Catalogo para SubTipo de Dispositivos
	public List<CatalogoVO> catalogoSubTipoDispositivo(Long idTipo);
	
	public List<CatalogoVO> catalogoSubTipoDispositivoTodos();
	
	//Catalogo para Marca de Dispositivos
	public List<CatalogoVO> catalogoMarcaDispositivo(Long idTipo);
	
	//Catalogo para los Tipos de Geocercas
	public List<CatalogoVO> catalogoTipoGeocerca();
	
	public List<CatalogoVO> catalogoMarca();
	public List<CatalogoVO> catalogoBusquedaDispositivo();
	
	public List<CatalogoVO> catalogoTipoEvento();
	public List<CatalogoVO> catalogoTipoComando();
	public List<CatalogoVO> catalogoTipoGrupo();
	
	public List<CatalogoVO> catalogoModeloPorMarca(Long idMarca);
	
	//Catalogo para los Tipos de Notificacion
	public List<CatalogoVO> catalogoTipoNotificacion();
	
	//Catalogo para los Tipos de Envio de Notificaciones en Geocerca
	public List<CatalogoVO> catalogoTipoNotificaGeocerca();
	
	//Catalogo para la Config de Envio de Notificaciones en Geocerca
	public List<GeoNotiOpcionVO> catalogoConfigNotiGeocerca();
	
	//Catalogo para Modelo de Dispositivos
	public List<CatalogoVO> catalogoModeloDispositivo(Long idTipo, Long idMarca, Long idSubTipo);
	
	public List<Map<String, Object>> catalogos(int idCatalog) throws ParseException;
	
	public Boolean actualizaCatalogos(ItemCatalogoVO itemCatalogoVO);
	public Boolean agregaCatalogos(ItemCatalogoVO itemCatalogoVO);
	
	
	public ItemCatalogoVO catalogoId(long id, int idCatalog);
	
	public Boolean activarDesactivarCatalogo(long id,long idCatalogo, boolean activar);
}
