package mx.com.teclo.smm.negocio.service.catalogo;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import mx.com.teclo.smm.negocio.service.empresa.EmpresaService;
import mx.com.teclo.smm.persistencia.hibernate.dao.catalogo.BusquedaDispositivoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.catalogo.CatalogoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.catalogo.TipoEventosDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.catalogo.TipoGruposDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.comandos.ComandoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.comandos.MedioSubtipoCmdDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.comandos.TipoComandoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo.EventoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo.MarcaDispositivoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo.ModeloDispositivoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo.SubTipoDispositivoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo.TipoDispositivoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.geocerca.ConfigNotificaGeocercaDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.geocerca.TipoGeocercaDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.grupo.GrupoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.notificacion.MedioComunicacionDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.notificacion.MedioNotificacionDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.notificacion.TipoNotificacionDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.catalogo.BusquedaDispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.catalogo.CatalogoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.ComandoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.MedioSubtipoCmdDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.TipoComandoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.EventoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.MarcaDispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.ModeloDispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.SubTipoDispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.TipoDispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.TipoEventoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.geocerca.ConfigNotificaGeocercaDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.geocerca.TipoGeocercaDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.grupo.GrupoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.grupo.TipoGrupoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.MedioComunicacionDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.MedioNotificacionDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.TipoNotificacionDTO;
import mx.com.teclo.smm.persistencia.vo.JsonObjectVO;
import mx.com.teclo.smm.persistencia.vo.catalogo.CatalogoVO;
import mx.com.teclo.smm.persistencia.vo.catalogo.ItemCatalogoVO;
import mx.com.teclo.smm.persistencia.vo.geocerca.GeoNotiOpcionVO;
import mx.com.teclo.smm.util.enumerados.CatalogoGeocercaEnum;
import mx.com.teclo.smm.util.enumerados.CatalogoGrupoEnum;
import mx.com.teclo.smm.util.enumerados.CatalogoMonitoreoEnum;
import mx.com.teclo.smm.util.enumerados.CatalogoNotificacionEnum;


@Service
public class CatalogoServiceImpl implements CatalogoService {

	@Autowired
	TipoDispositivoDAO tipoDispositivoDAO;

	@Autowired
	SubTipoDispositivoDAO subTipoDispositivoDAO;

	@Autowired
	MarcaDispositivoDAO marcaDispositivoDAO;

	@Autowired
	ModeloDispositivoDAO modeloDispositivoDAO;

	@Autowired
	CatalogoDAO catalogoDAO;

	@Autowired
	TipoGeocercaDAO tipoGeocercaDAO;

	@Autowired
	TipoEventosDAO tipoEventosDAO;

	@Autowired
	TipoGruposDAO tipoGruposDAO;

	@Autowired
	EventoDAO eventoDAO;

	@Autowired
	GrupoDAO grupoDAO;

	@Autowired
	ComandoDAO comandoDAO;

	@Autowired
	TipoComandoDAO tipoComandoDAO;

	@Autowired
	MedioSubtipoCmdDAO medioSubtipoCmdDAO;

	@Autowired
	MedioComunicacionDAO medioComunicacionDAO;

	@Autowired
	TipoNotificacionDAO tipoNotificacionDAO;

	@Autowired
	MedioNotificacionDAO medioNotificacionDAO;

	@Autowired
	ConfigNotificaGeocercaDAO configNotificaGeocercaDAO;

	@Autowired
	BusquedaDispositivoDAO busquedaDispositivoDAO;

	@Autowired
	EmpresaService empresaService;

	public static final String JSON = "[{\"id\":\"0\",\"nombre\":\"Menu\",\"param\":{\"id\":\"id personalizado\",\"nombre\":\"nombre personalizado\"},"
			+ "\"datos\":[{\"id\":\"1\",\"nombre\":\"Principal\"}," + "{\"id\":\"2\",\"nombre\":\"Dinamico\"}]},"
			+ " {\"id\":\"1\",\"nombre\":\"Tipo Grupos\",\"param\":{\"id\":\"id personalizado\",\"nombre\":\"Dato2 personalizado\",\"codigo\":\"codigo personalizado\"},"
			+ "\"datos\":[{\"id\":\"1\",\"nombre\":\"Dispositivos\",\"codigo\":\"DVC\"},"
			+ "{\"id\":\"2\",\"nombre\":\"Geocercas\", \"codigo\":\"GCA\"},"
			+ "{\"id\":\"3\",\"nombre\":\"Notificacion\",\"codigo\":\"NTC\"}]},"
			+ "{\"id\":\"2\",\"nombre\":\"Notificaciones\",\"param\":{\"id\":\"id personalizado\",\"tipo\":\"tipo personalizado\"},"
			+ "\"datos\":[{\"id\":\"1\",\"tipo\":\"Dispositivos\"}," + "{\"id\":\"2\",\"tipo\":\"Geocercas\"},"
			+ "{\"id\":\"3\",\"tipo\":\"Notificacion\"}]},"
			+ "{\"id\":\"3\",\"nombre\":\"Tipo Geocercas\",\"param\":{\"id\":\"dato1 personalizado\",\"nombre\":\"Dato2 personalizado\",\"codigo\":\"codigo personalizado\"},"
			+ "\"datos\":[{\"id\":\"1\",\"nombre\":\"Circular\",\"codigo\":\"CIR\"},"
			+ "{\"id\":\"2\",\"nombre\":\"Polylinean\",\"codigo\":\"PYL\"},"
			+ "{\"id\":\"3\",\"nombre\":\"Polygonal\",\"codigo\":\"PYG\"}]},"
			+ "{\"id\":\"4\",\"nombre\":\"Dispositivos\",\"param\":{\"id\":\"dato1 personalizado\",\"nombre\":\"Dato2 personalizado\"},"
			+ "\"datos\":[{\"id\":\"1\",\"nombre\":\"Movil\"}," + "{\"id\":\"2\",\"nombre\":\"Vehiculo\"},"
			+ "{\"id\":\"3\",\"nombre\":\"Dron\"}]},"
			+ "{\"id\":\"5\",\"nombre\":\"Comandos\",\"param\":{\"datos1\":\"dato1 personalizado\", \"datos2\":\"Dato2 personalizado\"},"
			+ "\"datos\":[{\"dato1\":\"1\",\"datos2\":\"Circular\"}," + "{\"dato1\":\"2\",\"datos2\":\"Polylinean\"},"
			+ "{\"dato1\":\"3\",\"datos2\":\"Polygonal\"}]}]";

	List<JsonObjectVO> listaObject;

	@Override
	public List<JsonObjectVO> buscarCatalogo() {
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			listaObject = objectMapper.readValue(JSON, new TypeReference<List<JsonObjectVO>>() {
			});
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listaObject;
	}

	@Override
	public List<CatalogoVO> catalogoMonitoreo() {
		return CatalogoMonitoreoEnum.getCatalogo();
	}

	@Override
	public CatalogoVO buscarCatalogoPorId(int id) {
		List<JsonObjectVO> x = buscarCatalogo();
		CatalogoVO catVo = null;

		for (JsonObjectVO i : x) {
			if (Integer.parseInt(i.getId()) == id) {
				catVo = new CatalogoVO();
				catVo.setId(Long.parseLong(i.getId()));
				catVo.setNombre(i.getNombre());
			}
		}
		return catVo;
	}

	@Override
	public List<CatalogoVO> buscarCatalogosPorId(Integer[] id) {
		List<JsonObjectVO> x = buscarCatalogo();
		List<CatalogoVO> y = null;
		CatalogoVO catVo = null;
		for (int i : id) {
			y = y != null ? y : new ArrayList<CatalogoVO>();
			catVo = new CatalogoVO();
			catVo.setId(Long.parseLong(x.get(i).getId()));
			catVo.setNombre(x.get(i).getNombre());
			y.add(catVo);
		}
		return y;
	}

	@Override
	public List<CatalogoVO> catalogoBusquedaGeocerca() {
		return CatalogoGeocercaEnum.getCatalogo();
	}

	@Override
	public List<CatalogoVO> catalogoBusquedaNotificacion(){
		return CatalogoNotificacionEnum.getCatalogo();
	}

	@Override
	public List<CatalogoVO> catalogoBusquedaGrupo(){
		return CatalogoGrupoEnum.getCatalogo();
	}

	@Override
	@Transactional(readOnly = true)
	public List<CatalogoVO> catalogoTipoDispositivo() {
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		CatalogoVO cat;

		List<TipoDispositivoDTO> listaDTO = tipoDispositivoDAO.buscarCatalogo();

		for (int i = 0; i < listaDTO.size(); i++) {
			cat = new CatalogoVO(listaDTO.get(i).getIdTipoDispositivo(), listaDTO.get(i).getNbTipoDispositivo());
			listaCat.add(cat);
		}
		return listaCat;
	}

	@Override
	@Transactional(readOnly = true)
	public List<CatalogoVO> catalogoTipoMedio() {
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		CatalogoVO cat;

		List<MedioComunicacionDTO> listaDTO = medioComunicacionDAO.obtenerCatalogo();

		for (int i = 0; i < listaDTO.size(); i++) {
			cat = new CatalogoVO(listaDTO.get(i).getIdMedio(), listaDTO.get(i).getNbMedio());
			listaCat.add(cat);
		}
		return listaCat;
	}

	@Override
	@Transactional(readOnly = true)
	public List<CatalogoVO> catalogoSubTipoDispositivo(Long idTipo) {
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		CatalogoVO cat;
		TipoDispositivoDTO tdDTO = null;

		/*
		if (idTipo == TipoDispositivoEnum.MOVIL.getTipoDispositivo()) {
			tdDTO = tipoDispositivoDAO.buscarCatalogoPorTipo(TipoDispositivoEnum.MOVIL.getTipoDispositivo());
		} else if (idTipo == TipoDispositivoEnum.VEHICULO.getTipoDispositivo()) {
			tdDTO = tipoDispositivoDAO.buscarCatalogoPorTipo(TipoDispositivoEnum.VEHICULO.getTipoDispositivo());
		}*/
		tdDTO = tipoDispositivoDAO.buscarCatalogoPorTipo(idTipo);

		List<SubTipoDispositivoDTO> listaDTO = subTipoDispositivoDAO.buscarCatalogoPorTipo(tdDTO);

		for (int i = 0; i < listaDTO.size(); i++) {
			cat = new CatalogoVO(listaDTO.get(i).getIdSubTipoDispositivo(), listaDTO.get(i).getNbSubTipoDispositivo());
			listaCat.add(cat);
		}
		return listaCat;
	}

	@Override
	@Transactional(readOnly = true)
	public List<CatalogoVO> catalogoSubTipoDispositivoTodos() {
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		CatalogoVO cat;

		List<SubTipoDispositivoDTO> listaDTO = subTipoDispositivoDAO.buscarCatalogoTodos();

		for (int i = 0; i < listaDTO.size(); i++) {
			cat = new CatalogoVO(listaDTO.get(i).getIdSubTipoDispositivo(), listaDTO.get(i).getNbSubTipoDispositivo());
			listaCat.add(cat);
		}
		return listaCat;
	}

	@Override
	@Transactional(readOnly = true)
	public List<CatalogoVO> catalogoTipoGeocerca() {
		CatalogoVO catVO = null;
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();

		List<TipoGeocercaDTO> listaTGDTO = tipoGeocercaDAO.buscarTipoGeocerca();
		//listaCat = TipoGeocercaEnum.getCatalogo();

		for(TipoGeocercaDTO  tGDTO : listaTGDTO)
		{
			catVO = new CatalogoVO();
			catVO.setId(tGDTO.getIdTipoGeocerca());
			catVO.setNombre(tGDTO.getNbTipoGeocerca());
			listaCat.add(catVO);
		}

		return listaCat;
	}

	@Override
	@Transactional(readOnly = true)
	public List<CatalogoVO> catalogoTipoNotificacion()
	{
		CatalogoVO catVO = null;
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		TipoNotificacionDTO tNDTO = tipoNotificacionDAO.buscarTipoNotificacionPorId(1L);

		List<MedioNotificacionDTO> listaMNDTO =  medioNotificacionDAO.buscarCatalogo(tNDTO);

		for(MedioNotificacionDTO  mNDTO : listaMNDTO)
		{
			catVO = new CatalogoVO();
			catVO.setId(mNDTO.getIdMedioTipoNotifi());
			catVO.setNombre(mNDTO.getIdMedio().getNbMedio());
			listaCat.add(catVO);
		}

		return listaCat;
	}

	@Override
	@Transactional(readOnly = true)
	public List<CatalogoVO> catalogoTipoNotificaGeocerca()
	{
		CatalogoVO catVO = null;
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		TipoNotificacionDTO tNDTO = tipoNotificacionDAO.buscarTipoNotificacionPorId(2L);

		List<MedioNotificacionDTO> listaMNDTO =  medioNotificacionDAO.buscarCatalogo(tNDTO);

		for(MedioNotificacionDTO  mNDTO : listaMNDTO)
		{
			catVO = new CatalogoVO();
			catVO.setId(mNDTO.getIdMedioTipoNotifi());
			catVO.setNombre(mNDTO.getIdMedio().getNbMedio());
			listaCat.add(catVO);
		}

		return listaCat;
	}

	@Override
	@Transactional(readOnly = true)
	public List<GeoNotiOpcionVO> catalogoConfigNotiGeocerca()
	{
		GeoNotiOpcionVO catVO = null;
		List<GeoNotiOpcionVO> listaCat = new ArrayList<GeoNotiOpcionVO>();
		List<ConfigNotificaGeocercaDTO> listaCNDTO = configNotificaGeocercaDAO.buscarCatalogo();

		for(ConfigNotificaGeocercaDTO  cNDTO : listaCNDTO)
		{
			catVO = new GeoNotiOpcionVO();
			catVO.setId(cNDTO.getIdConfigGeoNoti());
			catVO.setNombre(cNDTO.getTxConfigGeoNoti());
			catVO.setValor(Boolean.FALSE);
			listaCat.add(catVO);
		}

		return listaCat;
	}

	@Override
	@Transactional(readOnly = true)
	public List<CatalogoVO> catalogoMarcaDispositivo(Long idTipo){
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		List<ModeloDispositivoDTO> modeloDispositivoDTOs = modeloDispositivoDAO.buscarCatalogoPorSubTipo(idTipo);

		for (ModeloDispositivoDTO m : modeloDispositivoDTOs) {
			CatalogoVO cat = new CatalogoVO(m.getIdMarca().getIdMarca(), m.getIdMarca().getNbMarca());
			boolean b = false;
			for (CatalogoVO c : listaCat) {
				if (c.getId() == cat.getId()) {
					b = true;
				}
			}
			if (!b)
				listaCat.add(cat);
		}
		return listaCat;
	}

	@Override
	@Transactional(readOnly = true)
	public List<CatalogoVO> catalogoMarca() {
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		List<MarcaDispositivoDTO> marcaDispositivoDTOs = marcaDispositivoDAO.catalogoMarca();

		for (MarcaDispositivoDTO m : marcaDispositivoDTOs) {
			CatalogoVO cat = new CatalogoVO(m.getIdMarca(), m.getNbMarca());
			listaCat.add(cat);
		}
		return listaCat;
	}

	@Override
	@Transactional(readOnly = true)
	public List<CatalogoVO> catalogoBusquedaDispositivo() {
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		List<BusquedaDispositivoDTO> busquedaDispositivoDTOs = busquedaDispositivoDAO.obtenerCatalogos();

		for (BusquedaDispositivoDTO b : busquedaDispositivoDTOs) {
			CatalogoVO cat = new CatalogoVO(b.getIdBusquedaDispositivo(), b.getTxBusquedaDispositivo());
			listaCat.add(cat);
		}
		return listaCat;
	}

	@Override
	@Transactional(readOnly = true)
	public List<CatalogoVO> catalogoModeloPorMarca(Long idMarca) {
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		List<ModeloDispositivoDTO> modeloDispositivoDTOs = modeloDispositivoDAO.catalogoPorMarca(idMarca);

		for (ModeloDispositivoDTO m : modeloDispositivoDTOs) {
			CatalogoVO cat = new CatalogoVO(m.getIdModelo(), m.getNbModelo());
			listaCat.add(cat);
		}
		return listaCat;
	}

	@Override
	@Transactional(readOnly = true)
	public List<CatalogoVO> catalogoModeloDispositivo(Long idTipo, Long idMarca, Long idSubTipo) {

		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		CatalogoVO cat;
		TipoDispositivoDTO tipoDispositivoDTO = null;
		SubTipoDispositivoDTO subTipoDispositivoDTO = null;
		List<SubTipoDispositivoDTO> subTipoDispositivoDTOs;
		List<MarcaDispositivoDTO> marcaDispositivoDTOs;
		List<ModeloDispositivoDTO> modeloDispositivoDTOs;

		// Busca el tipo de dispositivo
		/*
		if (idTipo == TipoDispositivoEnum.MOVIL.getTipoDispositivo()) {
			tipoDispositivoDTO = tipoDispositivoDAO
					.buscarCatalogoPorTipo(TipoDispositivoEnum.MOVIL.getTipoDispositivo());
		} else if (idTipo == TipoDispositivoEnum.VEHICULO.getTipoDispositivo()) {
			tipoDispositivoDTO = tipoDispositivoDAO
					.buscarCatalogoPorTipo(TipoDispositivoEnum.VEHICULO.getTipoDispositivo());
		}*/

		tipoDispositivoDTO = tipoDispositivoDAO
				.buscarCatalogoPorTipo(idTipo);

		subTipoDispositivoDTO = subTipoDispositivoDAO.buscarCatalogoPorTipoAndIdSubtipo(tipoDispositivoDTO, idSubTipo);

		// Busca la marca de dispositivo
		marcaDispositivoDTOs = marcaDispositivoDAO.buscarCatalogoPorTipo(new Long[] { idMarca }, tipoDispositivoDTO);

		if(marcaDispositivoDTOs != null && !marcaDispositivoDTOs.isEmpty()){
			
			// Busca el modelo de acuerdo a la marca y Subtipo
			modeloDispositivoDTOs = modeloDispositivoDAO.buscarCatalogoPorTipo(marcaDispositivoDTOs.get(0),
					subTipoDispositivoDTO);

			for (int i = 0; i < modeloDispositivoDTOs.size(); i++) {
				cat = new CatalogoVO(modeloDispositivoDTOs.get(i).getIdModelo(),
						modeloDispositivoDTOs.get(i).getNbModelo());
				listaCat.add(cat);
			}
			
		}
		
		return listaCat;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Map<String, Object>> catalogos(int idCatalog) throws ParseException {

		List<Map<String, Object>> list = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		switch (idCatalog) {

		// TMM059C_CT_CATALOGO
		case 1:
			for (CatalogoDTO c : catalogoDAO.obtenerCatalogos()) {
				Map<String, Object> m = new HashMap<>();
				m.put("id", c.getIdCatalogo());
				m.put("nombre", c.getTxCatalogo());
				m.put("tabla", c.getTxCatalogoTabla());
				m.put("active", c.getStActivo());
				m.put("fhCreacion", " "+sdf.format(c.getFhCreacion()));
				m.put("fhModificacion", " "+sdf.format(c.getFhModificacion()));
				list.add(m);
			}
			break;

		// TMM002C_CT_TIPO_DISPOSITIVOS
		case 2:
			for (TipoDispositivoDTO c : tipoDispositivoDAO.activarDesactivarBuscarCatalogo()) {
				Map<String, Object> m = new HashMap<>();
				m.put("id", c.getIdTipoDispositivo());
				m.put("tx", c.getTxTipoDispositivo());
				m.put("nb", c.getNbTipoDispositivo());
				m.put("img", c.getImgTipoDispositivo());
				m.put("active", c.getStActivo());
				m.put("fhCreacion", " "+sdf.format(c.getFhCreacion()));
				m.put("fhModificacion", " "+sdf.format(c.getFhModificacion()));
				list.add(m);
			}
			break;

		// TMM004C_CT_MARCAS
		case 3:
			for (MarcaDispositivoDTO c :  marcaDispositivoDAO.activarDesactivarCatalogoMarca()) {
				Map<String, Object> m = new HashMap<>();
				m.put("id", c.getIdMarca());
				m.put("idTipoDispositivo", c.getIdTipoDispositivo().getCdTipoDispositivo());
				m.put("cd", c.getCdMarca());
				m.put("nb", c.getNbMarca());
				m.put("tx", c.getTxMarca());
				m.put("active", c.getStActivo());
				m.put("fhCreacion", " "+sdf.format(c.getFhCreacion()));
				m.put("fhModificacion", " "+sdf.format(c.getFhModificacion()));
				list.add(m);
			}
			break;

		// TMM005C_CT_MODELOS
		case 4:
			for (ModeloDispositivoDTO c :  modeloDispositivoDAO.activarDesactivarObtenerModelos()) {
				Map<String, Object> m = new HashMap<>();
				m.put("id", c.getIdModelo());
				m.put("idMarca", c.getIdMarca().getCdMarca());
				m.put("idSubtipoDispositivo", c.getIdSubtipoDispositivo().getCdSubTipoDispositivo());
				m.put("cd", c.getCdModelo());
				m.put("nb", c.getNbModelo());
				m.put("tx", c.getTxModelo());
				m.put("active", c.getStActivo());
				m.put("fhCreacion", " "+sdf.format(c.getFhCreacion()));
				m.put("fhModificacion", " "+sdf.format(c.getFhModificacion()));
				list.add(m);
			}
			break;

		// TMM006C_CT_TIPO_EVENTOS
		case 5:
			for (TipoEventoDTO c :  tipoEventosDAO.activarDesactivarObtenerTipoEventos()) {
				Map<String, Object> m = new HashMap<>();
				m.put("id", c.getIdTipoEvento());
				m.put("cd", c.getCdTipoEvento());
				m.put("nb", c.getNbTipoEvento());
				m.put("tx", c.getTxTipoEvento());
				m.put("img", c.getImgTipoEvento());
				m.put("active", c.getStActivo());
				m.put("fhCreacion", " "+sdf.format(c.getFhCreacion()));
				m.put("fhModificacion", " "+sdf.format(c.getFhModifica()));
				list.add(m);
			}
			break;

		// TMM007C_CT_TIPO_GEOCERCAS
		case 6:
			for (TipoGeocercaDTO c :  tipoGeocercaDAO.activarDesactivarBuscarTipoGeocerca()){
				Map<String, Object> m = new HashMap<>();
				m.put("id", c.getIdTipoGeocerca());
				m.put("cd", c.getCdTipoGeocerca());
				m.put("nb", c.getNbTipoGeocerca());
				m.put("tx", c.getTxTipoGeocerca());
				m.put("active", c.getStActivo());
				m.put("fhCreacion", " "+sdf.format(c.getFhCreacion()));
				m.put("fhModificacion", " "+sdf.format(c.getFhModificacion()));
				list.add(m);
			}
			break;

		// TMM012C_CT_EVENTOS
		case 7:
			for (EventoDTO c :  eventoDAO.activarDesactivarBuscarCatalogoEventos()) {
				Map<String, Object> m = new HashMap<>();
				m.put("id", c.getIdEvento());
				m.put("idTipoEvento", c.getIdTipoEvento().getCdTipoEvento());
				m.put("idSubtipoDispositivo", c.getIdSubtipoDispositivo().getCdSubTipoDispositivo());
				m.put("cd", c.getCdEvento());
				m.put("nb", c.getNbEvento());
				m.put("tx", c.getTxEvento());
				m.put("active", c.getStActivo());
				m.put("fhCreacion", " "+sdf.format(c.getFhCreacion()));
				m.put("fhModificacion", " "+sdf.format(c.getFhModificacion()));
				list.add(m);
			}
			break;

		// TMM020D_CT_GRUPOS
		case 8:
			for (GrupoDTO c :  grupoDAO.activarDesactivarBuscarGrupos()) {
				Map<String, Object> m = new HashMap<>();
				m.put("id", c.getIdGrupo());
				m.put("idTipoGrupo", c.getIdTipoGrupo().getCdTipoGrupo());
				m.put("nb", c.getNbGrupo());
				m.put("tx", c.getTxGrupo());
				m.put("active", c.getStActivo());
				m.put("fhCreacion", " "+sdf.format(c.getFhCreacion()));
				m.put("fhModificacion", " "+sdf.format(c.getFhModificacion()));
				list.add(m);
			}
			break;

		// TMM030C_CT_TIPO_GRUPOS
		case 9:
			for (TipoGrupoDTO c :  tipoGruposDAO.activarDesactivarObtenerTipoGrupos()) {
				Map<String, Object> m = new HashMap<>();
				m.put("id", c.getIdTipoGrupo());
				m.put("cd", c.getCdTipoGrupo());
				m.put("nb", c.getNbTipoGrupo());
				m.put("tx", c.getTxTipoGrupo());
				m.put("active", c.getStActivo());
				m.put("fhCreacion", " "+sdf.format(c.getFhCreacion()));
				m.put("fhModificacion", " "+sdf.format(c.getFhModificacion()));
				list.add(m);
			}
			break;

		// TMM003C_CT_TIPO_COMANDOS
		case 10:
			for (TipoComandoDTO c :  tipoComandoDAO.activarDesactivarObtenerTipoComando()) {
				Map<String, Object> m = new HashMap<>();
				m.put("id", c.getIdTipoComando());
				m.put("cd", c.getCdTipoComando());
				m.put("nb", c.getNbTipoComando());
				m.put("tx", c.getTxTipoComando());
				m.put("active", c.getStActivo());
				m.put("fhCreacion", " "+sdf.format(c.getFhCreacion()));
				m.put("fhModificacion", " "+sdf.format(c.getFhModificacion()));
				list.add(m);
			}
			break;

		// TMM025D_CT_COMANDOS
		case 11:
			for (ComandoDTO c :  comandoDAO.activarDesactivarObtenerComando()) {
				Map<String, Object> m = new HashMap<>();
				m.put("id", c.getIdComando());
				m.put("idTipoComando", c.getIdTipoComando().getCdTipoComando());
				m.put("idSubtipoDispositivo", c.getIdMedioSubtipoCmdDTO().getIdSubTipoDispositivoDTO().getNbSubTipoDispositivo());
				m.put("idMedioSubtipoCmd", c.getIdMedioSubtipoCmdDTO().getIdMedioComunicacionDTO().getNbMedio());
				m.put("cd", c.getCdComando());
				m.put("active", c.getStActivo());
				m.put("fhCreacion"," "+sdf.format(c.getFhCreacion()));
				m.put("fhModificacion", " "+sdf.format(c.getFhModificacion()));
				list.add(m);
			}
			break;

		}

		return list;
	}

	@Override
	@Transactional(readOnly = true)
	public List<CatalogoVO> catalogoTipoEvento(){
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		List<TipoEventoDTO> tipoEventoDTOs = tipoEventosDAO.obtenerTipoEventos();

		for (TipoEventoDTO m : tipoEventoDTOs) {
			CatalogoVO cat = new CatalogoVO(m.getIdTipoEvento(), m.getNbTipoEvento());
			listaCat.add(cat);
		}
		return listaCat;
	}

	@Override
	@Transactional(readOnly = true)
	public List<CatalogoVO> catalogoTipoComando(){
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		List<TipoComandoDTO> tipoComandoDTOs = tipoComandoDAO.obtenerTipoComando();

		for (TipoComandoDTO m : tipoComandoDTOs) {
			CatalogoVO cat = new CatalogoVO(m.getIdTipoComando(), m.getNbTipoComando());
			listaCat.add(cat);
		}
		return listaCat;
	}

	@Override
	@Transactional(readOnly = true)
	public List<CatalogoVO> catalogoTipoGrupo(){
		List<CatalogoVO> listaCat = new ArrayList<CatalogoVO>();
		List<TipoGrupoDTO> tipoGrupoDTOs = tipoGruposDAO.obtenerTipoGrupos();

		for (TipoGrupoDTO m : tipoGrupoDTOs) {
			CatalogoVO cat = new CatalogoVO(m.getIdTipoGrupo(), m.getNbTipoGrupo());
			listaCat.add(cat);
		}
		return listaCat;
	}

	@Override
	@Transactional
	public Boolean actualizaCatalogos(ItemCatalogoVO itemCatalogoVO) {

		switch (itemCatalogoVO.getIdCatalogo().intValue()) {

		// TMM059C_CT_CATALOGO
		case 1:

			break;

		// TMM002C_CT_TIPO_DISPOSITIVOS
		case 2:
			TipoDispositivoDTO tipoDispositivoDTO = tipoDispositivoDAO.activarDesactivarBuscarCatalogoPorTipo(itemCatalogoVO.getId());
			tipoDispositivoDTO.setIdUsrModifica(1L);
			tipoDispositivoDTO.setFhModificacion(new Date());

			tipoDispositivoDTO.setCdTipoDispositivo(itemCatalogoVO.getCd());
			tipoDispositivoDTO.setNbTipoDispositivo(itemCatalogoVO.getNb());
			tipoDispositivoDTO.setTxTipoDispositivo(itemCatalogoVO.getTx());
			tipoDispositivoDTO.setImgTipoDispositivo(itemCatalogoVO.getImg());
			tipoDispositivoDAO.save(tipoDispositivoDTO);

			break;

		// TMM004C_CT_MARCAS
		case 3:
			MarcaDispositivoDTO marcaDispositivoDTO = marcaDispositivoDAO.activarDesactivarObtenerMarca(itemCatalogoVO.getId());
			marcaDispositivoDTO.setIdUsrModifica(1L);
			marcaDispositivoDTO.setFhModificacion(new Date());

			marcaDispositivoDTO.setIdTipoDispositivo(tipoDispositivoDAO.buscarCatalogoPorTipo(itemCatalogoVO.getIdTipoDispositivo()));
			marcaDispositivoDTO.setCdMarca(itemCatalogoVO.getCd());
			marcaDispositivoDTO.setNbMarca(itemCatalogoVO.getNb());
			marcaDispositivoDTO.setTxMarca(itemCatalogoVO.getTx());
			marcaDispositivoDAO.save(marcaDispositivoDTO);

			break;

		// TMM005C_CT_MODELOS
		case 4:
			ModeloDispositivoDTO modeloDispositivoDTO = modeloDispositivoDAO.activarDesactivarObtenerModelosId(itemCatalogoVO.getId());
			modeloDispositivoDTO.setIdUsrModifica(1L);
			modeloDispositivoDTO.setFhModificacion(new Date());

			modeloDispositivoDTO.setIdMarca(marcaDispositivoDAO.buscarCatalogo(new Long[] {itemCatalogoVO.getIdMarca().longValue()}).get(0));
			modeloDispositivoDTO.setIdSubtipoDispositivo(subTipoDispositivoDAO.buscarCatalogo(new Long[] {itemCatalogoVO.getIdSubTipoDispositivo().longValue()}).get(0));
			modeloDispositivoDTO.setCdModelo(itemCatalogoVO.getCd());
			modeloDispositivoDTO.setNbModelo(itemCatalogoVO.getNb());
			modeloDispositivoDTO.setTxModelo(itemCatalogoVO.getTx());

			modeloDispositivoDAO.save(modeloDispositivoDTO);

			break;

		// TMM006C_CT_TIPO_EVENTOS
		case 5:
			TipoEventoDTO tipoEventoDTO = tipoEventosDAO.activarDesactivarObtenerTipoEventos(itemCatalogoVO.getId());
			tipoEventoDTO.setIdUsrModifica(1L);
			tipoEventoDTO.setFhModifica(new Date());

			tipoEventoDTO.setCdTipoEvento(itemCatalogoVO.getCd());
			tipoEventoDTO.setNbTipoEvento(itemCatalogoVO.getNb());
			tipoEventoDTO.setTxTipoEvento(itemCatalogoVO.getTx());
			tipoEventoDTO.setImgTipoEvento(itemCatalogoVO.getImg());

			tipoEventosDAO.save(tipoEventoDTO);
			break;

		// TMM007C_CT_TIPO_GEOCERCAS
		case 6:
			TipoGeocercaDTO tipoGeocercaDTO = tipoGeocercaDAO.activarDesactivarBuscarTipoGeocercaPorId(itemCatalogoVO.getId());
			tipoGeocercaDTO.setIdUsrModifica(1L);
			tipoGeocercaDTO.setFhModificacion(new Date());

			tipoGeocercaDTO.setCdTipoGeocerca(itemCatalogoVO.getCd());
			tipoGeocercaDTO.setNbTipoGeocerca(itemCatalogoVO.getNb());
			tipoGeocercaDTO.setTxTipoGeocerca(itemCatalogoVO.getTx());
			tipoGeocercaDAO.save(tipoGeocercaDTO);
			break;

		// TMM012C_CT_EVENTOS
		case 7:
			EventoDTO eventoDTO = eventoDAO.activarDesactivarBuscarCatalogoEventosId(itemCatalogoVO.getId());
			eventoDTO.setIdUsrModifica(1L);
			eventoDTO.setFhModificacion(new Date());

			eventoDTO.setIdTipoEvento(tipoEventosDAO.obtenerTipoEventos(itemCatalogoVO.getIdTipoEvento()));
			eventoDTO.setIdSubtipoDispositivo(subTipoDispositivoDAO.buscarCatalogo(new Long[] {itemCatalogoVO.getIdSubTipoDispositivo().longValue()}).get(0));
			eventoDTO.setCdEvento(itemCatalogoVO.getCd());
			eventoDTO.setNbEvento(itemCatalogoVO.getNb());
			eventoDTO.setTxEvento(itemCatalogoVO.getTx());
			eventoDTO.setTxParametro(itemCatalogoVO.getParam());
			eventoDAO.save(eventoDTO);
			break;

		// TMM020D_CT_GRUPOS
		case 8:
			GrupoDTO grupoDTO = grupoDAO.activarDesactivarBuscarGrupoPorId(itemCatalogoVO.getId());
			grupoDTO.setIdUsrModifica(1L);
			grupoDTO.setFhModificacion(new Date());

			grupoDTO.setIdTipoGrupo(tipoGruposDAO.obtenerTipoGrupos(itemCatalogoVO.getIdTipoGrupo()));
			grupoDTO.setNbGrupo(itemCatalogoVO.getNb());
			grupoDTO.setTxGrupo(itemCatalogoVO.getTx());
			grupoDAO.save(grupoDTO);
			break;

		// TMM030C_CT_TIPO_GRUPOS
		case 9:
			TipoGrupoDTO tipoGrupoDTO = tipoGruposDAO.activarDesactivarObtenerTipoGrupos(itemCatalogoVO.getId());
			tipoGrupoDTO.setIdUsrModifica(1L);
			tipoGrupoDTO.setFhModificacion(new Date());

			tipoGrupoDTO.setCdTipoGrupo(itemCatalogoVO.getCd());
			tipoGrupoDTO.setNbTipoGrupo(itemCatalogoVO.getNb());
			tipoGrupoDTO.setTxTipoGrupo(itemCatalogoVO.getTx());
			tipoGruposDAO.save(tipoGrupoDTO);
			break;


		// TMM003C_CT_TIPO_COMANDOS
		case 10:
			TipoComandoDTO tipoComandoDTO = tipoComandoDAO.activarDesactivarObtenerTipoComando(itemCatalogoVO.getId());
			tipoComandoDTO.setIdUsrModifica(1L);
			tipoComandoDTO.setFhModificacion(new Date());

			tipoComandoDTO.setCdTipoComando(itemCatalogoVO.getCd());
			tipoComandoDTO.setNbTipoComando(itemCatalogoVO.getNb());
			tipoComandoDTO.setTxTipoComando(itemCatalogoVO.getTx());
			tipoComandoDAO.save(tipoComandoDTO);
			break;

		// TMM025D_CT_COMANDOS
		case 11:
			ComandoDTO comandoDTO = comandoDAO.activarDesactivarObtenerComando(itemCatalogoVO.getId());
			comandoDTO.setIdUsrModifica(1L);
			comandoDTO.setFhModificacion(new Date());

			comandoDTO.setIdTipoComando(tipoComandoDAO.obtenerTipoComando(itemCatalogoVO.getIdTipoComando()));
			comandoDTO.setCdComando(itemCatalogoVO.getCd());

			// buscar idMedioSubtipoCmd con id_medio y id_subtipo_dispositivo
			MedioSubtipoCmdDTO medioSubtipoCmdDTO = medioSubtipoCmdDAO.obtenerMedioSubtipoCmdPorIdMedioIdSubtipoDispositivo(itemCatalogoVO.getIdTipoMedio(),itemCatalogoVO.getIdSubTipoDispositivo());
			// existe idMedioSubtipoCmd
			if(medioSubtipoCmdDTO != null) {
				comandoDTO.setIdMedioSubtipoCmdDTO(medioSubtipoCmdDTO);
			}
			// Si no existe entonces crear idMedioSubtipo
			else {
				MedioSubtipoCmdDTO medioSubtipoCmdDTO2 = new MedioSubtipoCmdDTO();
				medioSubtipoCmdDTO2.setIdMedioComunicacionDTO(medioComunicacionDAO.buscarMedioPorId(itemCatalogoVO.getIdTipoMedio()));
				medioSubtipoCmdDTO2.setIdSubTipoDispositivoDTO(subTipoDispositivoDAO.buscarCatalogo(new Long[]{itemCatalogoVO.getIdSubTipoDispositivo()}).get(0));

				medioSubtipoCmdDTO2.setStActivo(true);
				medioSubtipoCmdDTO2.setIdUsrCreacion(1L);
				medioSubtipoCmdDTO2.setIdUsrModifica(1L);
				medioSubtipoCmdDTO2.setFhModificacion(new Date());
				medioSubtipoCmdDTO2.setFhCreacion(new Date());
				medioSubtipoCmdDAO.save(medioSubtipoCmdDTO2);

				comandoDTO.setIdMedioSubtipoCmdDTO(medioSubtipoCmdDTO2);
			}
			comandoDAO.save(comandoDTO);

			break;

		}

		return true;
	}

	@Override
	@Transactional
	public Boolean agregaCatalogos(ItemCatalogoVO itemCatalogoVO) {

		switch (Integer.parseInt(String.valueOf(itemCatalogoVO.getIdCatalogo()))) {
		// TMM059C_CT_CATALOGO
		case 1:
			break;

		// TMM002C_CT_TIPO_DISPOSITIVOS
		case 2:
			TipoDispositivoDTO tipoDispositivoDTO = new TipoDispositivoDTO();

			tipoDispositivoDTO.setStActivo(true);
			tipoDispositivoDTO.setIdUsrCreacion(1L);
			tipoDispositivoDTO.setIdUsrModifica(1L);
			tipoDispositivoDTO.setFhModificacion(new Date());
			tipoDispositivoDTO.setFhCreacion(new Date());

			tipoDispositivoDTO.setCdTipoDispositivo(itemCatalogoVO.getCd());
			tipoDispositivoDTO.setNbTipoDispositivo(itemCatalogoVO.getNb());
			tipoDispositivoDTO.setTxTipoDispositivo(itemCatalogoVO.getTx());
			tipoDispositivoDTO.setImgTipoDispositivo(itemCatalogoVO.getImg());
			tipoDispositivoDTO.setIdEmpresa(empresaService.getIdEmpresa());
			tipoDispositivoDAO.save(tipoDispositivoDTO);
			break;

		// TMM004C_CT_MARCAS
		case 3:
			MarcaDispositivoDTO marcaDispositivoDTO = new MarcaDispositivoDTO();

			marcaDispositivoDTO.setStActivo(true);
			marcaDispositivoDTO.setIdUsrCreacion(1L);
			marcaDispositivoDTO.setIdUsrModifica(1L);
			marcaDispositivoDTO.setFhModificacion(new Date());
			marcaDispositivoDTO.setFhCreacion(new Date());

			marcaDispositivoDTO.setIdTipoDispositivo(tipoDispositivoDAO.buscarCatalogoPorTipo(itemCatalogoVO.getIdTipoDispositivo()));
			marcaDispositivoDTO.setCdMarca(itemCatalogoVO.getCd());
			marcaDispositivoDTO.setNbMarca(itemCatalogoVO.getNb());
			marcaDispositivoDTO.setTxMarca(itemCatalogoVO.getTx());
			marcaDispositivoDTO.setIdEmpresa(empresaService.getIdEmpresa());
			marcaDispositivoDAO.save(marcaDispositivoDTO);
			break;

		// TMM005C_CT_MODELOS
		case 4:
			ModeloDispositivoDTO modeloDispositivoDTO = new ModeloDispositivoDTO();

			modeloDispositivoDTO.setStActivo(true);
			modeloDispositivoDTO.setIdUsrCreacion(1L);
			modeloDispositivoDTO.setIdUsrModifica(1L);
			modeloDispositivoDTO.setFhModificacion(new Date());
			modeloDispositivoDTO.setFhCreacion(new Date());

			modeloDispositivoDTO.setIdMarca(marcaDispositivoDAO.buscarCatalogo(new Long[] {itemCatalogoVO.getIdMarca().longValue()}).get(0));
			modeloDispositivoDTO.setIdSubtipoDispositivo(subTipoDispositivoDAO.buscarCatalogo(new Long[] {itemCatalogoVO.getIdSubTipoDispositivo().longValue()}).get(0));
			modeloDispositivoDTO.setCdModelo(itemCatalogoVO.getCd());
			modeloDispositivoDTO.setNbModelo(itemCatalogoVO.getNb());
			modeloDispositivoDTO.setTxModelo(itemCatalogoVO.getTx());
			modeloDispositivoDTO.setIdEmpresa(empresaService.getIdEmpresa());

			modeloDispositivoDAO.save(modeloDispositivoDTO);
			break;

		// TMM006C_CT_TIPO_EVENTOS
		case 5:
			TipoEventoDTO tipoEventoDTO = new TipoEventoDTO();

			tipoEventoDTO.setStActivo(true);
			tipoEventoDTO.setIdUsrCreacion(1L);
			tipoEventoDTO.setIdUsrModifica(1L);
			tipoEventoDTO.setFhModifica(new Date());
			tipoEventoDTO.setFhCreacion(new Date());

			tipoEventoDTO.setCdTipoEvento(itemCatalogoVO.getCd());
			tipoEventoDTO.setNbTipoEvento(itemCatalogoVO.getNb());
			tipoEventoDTO.setTxTipoEvento(itemCatalogoVO.getTx());
			tipoEventoDTO.setImgTipoEvento(itemCatalogoVO.getImg());

			tipoEventosDAO.save(tipoEventoDTO);
			break;

		// TMM007C_CT_TIPO_GEOCERCAS
		case 6:
			TipoGeocercaDTO tipoGeocercaDTO = new TipoGeocercaDTO();

			tipoGeocercaDTO.setStActivo(true);
			tipoGeocercaDTO.setIdUsrCreacion(1L);
			tipoGeocercaDTO.setIdUsrModifica(1L);
			tipoGeocercaDTO.setFhModificacion(new Date());
			tipoGeocercaDTO.setFhCreacion(new Date());

			tipoGeocercaDTO.setCdTipoGeocerca(itemCatalogoVO.getCd());
			tipoGeocercaDTO.setNbTipoGeocerca(itemCatalogoVO.getNb());
			tipoGeocercaDTO.setTxTipoGeocerca(itemCatalogoVO.getTx());

			tipoGeocercaDAO.save(tipoGeocercaDTO);
			break;

		// TMM012C_CT_EVENTOS
		case 7:
			EventoDTO eventoDTO = new EventoDTO();

			eventoDTO.setStActivo(true);
			eventoDTO.setIdUsrCreacion(1L);
			eventoDTO.setIdUsrModifica(1L);
			eventoDTO.setFhModificacion(new Date());
			eventoDTO.setFhCreacion(new Date());

			eventoDTO.setIdTipoEvento(tipoEventosDAO.obtenerTipoEventos(itemCatalogoVO.getIdTipoEvento()));
			eventoDTO.setIdSubtipoDispositivo(subTipoDispositivoDAO.buscarCatalogo(new Long[] {itemCatalogoVO.getIdSubTipoDispositivo().longValue()}).get(0));
			eventoDTO.setCdEvento(itemCatalogoVO.getCd());
			eventoDTO.setNbEvento(itemCatalogoVO.getNb());
			eventoDTO.setTxEvento(itemCatalogoVO.getTx());
			eventoDTO.setTxParametro(itemCatalogoVO.getParam());
			eventoDTO.setIdEmpresa(empresaService.getIdEmpresa());

			eventoDAO.save(eventoDTO);

			break;

		// TMM020D_CT_GRUPOS
		case 8:
			GrupoDTO grupoDTO = new GrupoDTO();

			grupoDTO.setStActivo(true);
			grupoDTO.setIdUsrCreacion(1L);
			grupoDTO.setIdUsrModifica(1L);
			grupoDTO.setFhModificacion(new Date());
			grupoDTO.setFhCreacion(new Date());

			grupoDTO.setIdTipoGrupo(tipoGruposDAO.obtenerTipoGrupos(itemCatalogoVO.getIdTipoGrupo()));
			grupoDTO.setNbGrupo(itemCatalogoVO.getNb());
			grupoDTO.setTxGrupo(itemCatalogoVO.getTx());
			grupoDTO.setIdEmpresa(empresaService.getIdEmpresa());

			grupoDAO.save(grupoDTO);
			break;

		// TMM030C_CT_TIPO_GRUPOS
		case 9:
			TipoGrupoDTO tipoGrupoDTO = new TipoGrupoDTO();

			tipoGrupoDTO.setStActivo(true);
			tipoGrupoDTO.setIdUsrCreacion(1L);
			tipoGrupoDTO.setIdUsrModifica(1L);
			tipoGrupoDTO.setFhModificacion(new Date());
			tipoGrupoDTO.setFhCreacion(new Date());

			tipoGrupoDTO.setCdTipoGrupo(itemCatalogoVO.getCd());
			tipoGrupoDTO.setNbTipoGrupo(itemCatalogoVO.getNb());
			tipoGrupoDTO.setTxTipoGrupo(itemCatalogoVO.getTx());
			tipoGruposDAO.save(tipoGrupoDTO);
			break;

		// TMM003C_CT_TIPO_COMANDOS
		case 10:
			TipoComandoDTO tipoComandoDTO = new TipoComandoDTO();

			tipoComandoDTO.setStActivo(true);
			tipoComandoDTO.setIdUsrCreacion(1L);
			tipoComandoDTO.setIdUsrModifica(1L);
			tipoComandoDTO.setFhModificacion(new Date());
			tipoComandoDTO.setFhCreacion(new Date());

			tipoComandoDTO.setCdTipoComando(itemCatalogoVO.getCd());
			tipoComandoDTO.setNbTipoComando(itemCatalogoVO.getNb());
			tipoComandoDTO.setTxTipoComando(itemCatalogoVO.getTx());
			tipoComandoDTO.setIdEmpresa(empresaService.getIdEmpresa());

			tipoComandoDAO.save(tipoComandoDTO);

			break;

		// TMM025D_CT_COMANDOS
		case 11:
			ComandoDTO comandoDTO = new ComandoDTO();

			comandoDTO.setStActivo(true);
			comandoDTO.setIdUsrCreacion(1L);
			comandoDTO.setIdUsrModifica(1L);
			comandoDTO.setFhModificacion(new Date());
			comandoDTO.setFhCreacion(new Date());

			comandoDTO.setIdTipoComando(tipoComandoDAO.obtenerTipoComando(itemCatalogoVO.getIdTipoComando()));
			comandoDTO.setCdComando(itemCatalogoVO.getCd());
			comandoDTO.setIdEmpresa(empresaService.getIdEmpresa());

			// buscar idMedioSubtipoCmd con id_medio y id_subtipo_dispositivo
			MedioSubtipoCmdDTO medioSubtipoCmdDTO = medioSubtipoCmdDAO.obtenerMedioSubtipoCmdPorIdMedioIdSubtipoDispositivo(itemCatalogoVO.getIdTipoMedio(),itemCatalogoVO.getIdSubTipoDispositivo());
			// existe idMedioSubtipoCmd
			if(medioSubtipoCmdDTO != null) {
				comandoDTO.setIdMedioSubtipoCmdDTO(medioSubtipoCmdDTO);
			}
			// Si no existe entonces crear idMedioSubtipo
			else {
				MedioSubtipoCmdDTO medioSubtipoCmdDTO2 = new MedioSubtipoCmdDTO();
				medioSubtipoCmdDTO2.setIdMedioComunicacionDTO(medioComunicacionDAO.buscarMedioPorId(itemCatalogoVO.getIdTipoMedio()));
				medioSubtipoCmdDTO2.setIdSubTipoDispositivoDTO(subTipoDispositivoDAO.buscarCatalogo(new Long[]{itemCatalogoVO.getIdSubTipoDispositivo()}).get(0));

				medioSubtipoCmdDTO2.setStActivo(true);
				medioSubtipoCmdDTO2.setIdUsrCreacion(1L);
				medioSubtipoCmdDTO2.setIdUsrModifica(1L);
				medioSubtipoCmdDTO2.setFhModificacion(new Date());
				medioSubtipoCmdDTO2.setFhCreacion(new Date());
				medioSubtipoCmdDTO2.setIdEmpresa(empresaService.getIdEmpresa());
				medioSubtipoCmdDAO.save(medioSubtipoCmdDTO2);

				comandoDTO.setIdMedioSubtipoCmdDTO(medioSubtipoCmdDTO2);
			}

			comandoDAO.save(comandoDTO);
			break;

		}

		return true;
	}

	@Override
	@Transactional(readOnly = true)
	public ItemCatalogoVO catalogoId(long id, int idCatalog) {

		ItemCatalogoVO itemCatalogoVO = new ItemCatalogoVO();

		switch (idCatalog) {

		// TMM059C_CT_CATALOGO
		case 1:

			break;

		// TMM002C_CT_TIPO_DISPOSITIVOS
		case 2:
			TipoDispositivoDTO tipoDispositivoDTO = tipoDispositivoDAO.activarDesactivarBuscarCatalogoPorTipo(id);
			itemCatalogoVO.setId(id);
			itemCatalogoVO.setCd(tipoDispositivoDTO.getCdTipoDispositivo());
			itemCatalogoVO.setNb(tipoDispositivoDTO.getNbTipoDispositivo());
			itemCatalogoVO.setTx(tipoDispositivoDTO.getTxTipoDispositivo());
			itemCatalogoVO.setImg(tipoDispositivoDTO.getImgTipoDispositivo() == null ? "" : tipoDispositivoDTO.getImgTipoDispositivo());
			break;

		// TMM004C_CT_MARCAS
		case 3:
			MarcaDispositivoDTO marcaDispositivoDTO = marcaDispositivoDAO.activarDesactivarObtenerMarca(id);
			itemCatalogoVO.setId(id);
			itemCatalogoVO.setIdTipoDispositivo(marcaDispositivoDTO.getIdTipoDispositivo().getIdTipoDispositivo());
			itemCatalogoVO.setCd(marcaDispositivoDTO.getCdMarca());
			itemCatalogoVO.setNb(marcaDispositivoDTO.getNbMarca());
			itemCatalogoVO.setTx(marcaDispositivoDTO.getTxMarca());

			break;

		// TMM005C_CT_MODELOS
		case 4:
			ModeloDispositivoDTO modeloDispositivoDTO = modeloDispositivoDAO.activarDesactivarObtenerModelosId(id);
			itemCatalogoVO.setId(id);
			itemCatalogoVO.setIdMarca(modeloDispositivoDTO.getIdMarca().getIdMarca());
			itemCatalogoVO.setIdSubTipoDispositivo(modeloDispositivoDTO.getIdSubtipoDispositivo().getIdSubTipoDispositivo());
			itemCatalogoVO.setCd(modeloDispositivoDTO.getCdModelo());
			itemCatalogoVO.setNb(modeloDispositivoDTO.getNbModelo());
			itemCatalogoVO.setTx(modeloDispositivoDTO.getTxModelo());

			break;

		// TMM006C_CT_TIPO_EVENTOS
		case 5:
			TipoEventoDTO tipoEventoDTO = tipoEventosDAO.activarDesactivarObtenerTipoEventos(id);
			itemCatalogoVO.setId(id);
			itemCatalogoVO.setCd(tipoEventoDTO.getCdTipoEvento());
			itemCatalogoVO.setNb(tipoEventoDTO.getNbTipoEvento());
			itemCatalogoVO.setTx(tipoEventoDTO.getTxTipoEvento());
			itemCatalogoVO.setImg(tipoEventoDTO.getImgTipoEvento() == null ? "" : tipoEventoDTO.getImgTipoEvento());

			break;

		// TMM007C_CT_TIPO_GEOCERCAS
		case 6:
			TipoGeocercaDTO tipoGeocercaDTO = tipoGeocercaDAO.activarDesactivarBuscarTipoGeocercaPorId(id);
			itemCatalogoVO.setId(id);
			itemCatalogoVO.setCd(tipoGeocercaDTO.getCdTipoGeocerca());
			itemCatalogoVO.setNb(tipoGeocercaDTO.getNbTipoGeocerca());
			itemCatalogoVO.setTx(tipoGeocercaDTO.getTxTipoGeocerca());
			break;

		// TMM012C_CT_EVENTOS
		case 7:
			EventoDTO eventoDTO = eventoDAO.activarDesactivarBuscarCatalogoEventosId(id);
			itemCatalogoVO.setId(id);
			itemCatalogoVO.setIdTipoEvento(eventoDTO.getIdTipoEvento().getIdTipoEvento());
			itemCatalogoVO.setIdSubTipoDispositivo(eventoDTO.getIdSubtipoDispositivo().getIdSubTipoDispositivo());
			itemCatalogoVO.setCd(eventoDTO.getCdEvento());
			itemCatalogoVO.setNb(eventoDTO.getNbEvento());
			itemCatalogoVO.setTx(eventoDTO.getTxEvento());
			break;

		// TMM020D_CT_GRUPOS
		case 8:
			GrupoDTO grupoDTO = grupoDAO.activarDesactivarBuscarGrupoPorId(id);
			itemCatalogoVO.setId(id);
			itemCatalogoVO.setIdTipoGrupo(grupoDTO.getIdGrupo());
			itemCatalogoVO.setNb(grupoDTO.getNbGrupo());
			itemCatalogoVO.setTx(grupoDTO.getTxGrupo());
			break;

		// TMM030C_CT_TIPO_GRUPOS
		case 9:
			TipoGrupoDTO tipoGrupoDTO = tipoGruposDAO.activarDesactivarObtenerTipoGrupos(id);
			itemCatalogoVO.setId(id);
			itemCatalogoVO.setCd(tipoGrupoDTO.getCdTipoGrupo());
			itemCatalogoVO.setNb(tipoGrupoDTO.getNbTipoGrupo());
			itemCatalogoVO.setTx(tipoGrupoDTO.getTxTipoGrupo());
			break;


		// TMM003C_CT_TIPO_COMANDOS
		case 10:
			TipoComandoDTO tipoComandoDTO = tipoComandoDAO.activarDesactivarObtenerTipoComando(id);
			itemCatalogoVO.setId(id);
			itemCatalogoVO.setCd(tipoComandoDTO.getCdTipoComando());
			itemCatalogoVO.setNb(tipoComandoDTO.getNbTipoComando());
			itemCatalogoVO.setTx(tipoComandoDTO.getTxTipoComando());
			break;

		// TMM025D_CT_COMANDOS
		case 11:
			ComandoDTO comandoDTO = comandoDAO.activarDesactivarObtenerComando(id);
			itemCatalogoVO.setId(id);
			itemCatalogoVO.setIdTipoComando(comandoDTO.getIdTipoComando().getIdTipoComando());
			itemCatalogoVO.setIdTipoMedio(comandoDTO.getIdTipoComando().getIdTipoComando());
			itemCatalogoVO.setIdSubTipoDispositivo(comandoDTO.getIdMedioSubtipoCmdDTO().getIdSubTipoDispositivoDTO().getIdSubTipoDispositivo());
			itemCatalogoVO.setCd(comandoDTO.getCdComando());
			break;

		}

		return itemCatalogoVO;
	}

	@Override
	@Transactional
	public Boolean activarDesactivarCatalogo(long id,long idCatalogo, boolean activar) {

		switch (Integer.parseInt(String.valueOf(idCatalogo))) {

		// TMM059C_CT_CATALOGO
		case 1:
			CatalogoDTO catalogoDTO = catalogoDAO.activarDesactivarobtenerCatalogo(id);
			catalogoDTO.setStActivo(new Boolean(activar));
			catalogoDAO.save(catalogoDTO);
			break;

		// TMM002C_CT_TIPO_DISPOSITIVOS
		case 2:
			TipoDispositivoDTO tipoDispositivoDTO = tipoDispositivoDAO.activarDesactivarBuscarCatalogoPorTipo(id);
			tipoDispositivoDTO.setStActivo(new Boolean(activar));
			tipoDispositivoDAO.save(tipoDispositivoDTO);

			break;

		// TMM004C_CT_MARCAS
		case 3:
			MarcaDispositivoDTO marcaDispositivoDTO = marcaDispositivoDAO.activarDesactivarBuscarCatalogo(new Long[] {id}).get(0);
			marcaDispositivoDTO.setStActivo(new Boolean(activar));
			marcaDispositivoDAO.save(marcaDispositivoDTO);

			break;

		// TMM005C_CT_MODELOS
		case 4:
			ModeloDispositivoDTO modeloDispositivoDTO = modeloDispositivoDAO.activarDesactivarObtenerModelosId(id);
			modeloDispositivoDTO.setStActivo(new Boolean(activar));
			modeloDispositivoDAO.save(modeloDispositivoDTO);

			break;

		// TMM006C_CT_TIPO_EVENTOS
		case 5:
			TipoEventoDTO tipoEventoDTO = tipoEventosDAO.activarDesactivarObtenerTipoEventos(id);

			tipoEventoDTO.setStActivo(new Boolean(activar));
			tipoEventosDAO.save(tipoEventoDTO);
			break;

		// TMM007C_CT_TIPO_GEOCERCAS
		case 6:
			TipoGeocercaDTO tipoGeocercaDTO = tipoGeocercaDAO.activarDesactivarBuscarTipoGeocercaPorId(id);

			tipoGeocercaDTO.setStActivo(new Boolean(activar));
			tipoGeocercaDAO.save(tipoGeocercaDTO);
			break;

		// TMM012C_CT_EVENTOS
		case 7:
			EventoDTO eventoDTO = eventoDAO.activarDesactivarBuscarCatalogoEventosId(id);
			eventoDTO.setStActivo(new Boolean(activar));

			eventoDAO.save(eventoDTO);
			break;

		// TMM020D_CT_GRUPOS
		case 8:
			GrupoDTO grupoDTO = grupoDAO.activarDesactivarBuscarGrupoPorId(id);
			grupoDTO.setStActivo(new Boolean(activar));

			grupoDAO.save(grupoDTO);
			break;

		// TMM030C_CT_TIPO_GRUPOS
		case 9:
			TipoGrupoDTO tipoGrupoDTO = tipoGruposDAO.activarDesactivarObtenerTipoGrupos(id);
			tipoGrupoDTO.setStActivo(new Boolean(activar));

			tipoGruposDAO.save(tipoGrupoDTO);
			break;

		// TMM003C_CT_TIPO_COMANDOS
		case 10:
			TipoComandoDTO tipoComandoDTO = tipoComandoDAO.activarDesactivarObtenerTipoComando(id);
			tipoComandoDTO.setStActivo(new Boolean(activar));

			tipoComandoDAO.save(tipoComandoDTO);
			break;

		// TMM025D_CT_COMANDOS
		case 11:
			ComandoDTO comandoDTO = comandoDAO.activarDesactivarObtenerComando(id);
			comandoDTO.setStActivo(new Boolean(activar));

			comandoDAO.save(comandoDTO);
			break;
		}

		return true;
	}
}
