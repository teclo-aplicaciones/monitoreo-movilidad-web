package mx.com.teclo.smm.persistencia.hibernate.dao.perfil;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.empresa.EmpresaPerfilDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.perfil.PerfilDTO;

public interface EmpresaPerfilDAO extends BaseDAO<EmpresaPerfilDTO>{
	public List<PerfilDTO> findProfilesByApp(String cdApplication);
	public PerfilDTO findProfileById(Long id, String cdApplication, boolean isByApplication);
	public List<PerfilDTO> findAllProfilesByApp(String cdApplication);
	public PerfilDTO findUniqueProfileById(Long id, String cdApplication, boolean isByApplication);
}
