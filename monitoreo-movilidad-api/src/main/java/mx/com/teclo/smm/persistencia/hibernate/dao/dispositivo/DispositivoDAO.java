package mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispositivoDTO;

public interface DispositivoDAO extends BaseDAO<DispositivoDTO>{
	public List<DispositivoDTO> buscarDispositivos();
	public void guardarDispositivo(DispositivoDTO dispDTO);
	public DispositivoDTO buscarDispositivosPorId(Long id);
	public List<DispositivoDTO> buscarDispositivosPorId(Long[] id);
	public Boolean crearTablasDispositivo(Long numId);
	public void dropTableNbTableDispPosici(String table);
	public void dropTableNbTableDispEvento(String table);
	public void dropTableNbTableDispComand(String table);
	public List<DispositivoDTO> buscarDispositivosPorTipo(String tipo, String valor);
	public List<DispositivoDTO> buscarDispositivoPorNombre(String nombre);
}
