package mx.com.teclo.smm.negocio.service.perfil;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.arquitectura.ortogonales.service.comun.UsuarioFirmadoService;
import mx.com.teclo.arquitectura.ortogonales.util.ResponseConverter;
import mx.com.teclo.smm.negocio.service.empresa.EmpresaService;
import mx.com.teclo.smm.persistencia.hibernate.dao.perfil.EmpresaPerfilDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.perfil.PerfilDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.perfil.PerfilUsuarioDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.usuario.AplicacionDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.usuario.UsuarioDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.empresa.EmpresaPerfilDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.perfil.PerfilDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.perfil.PerfilUsuarioDTO;
import mx.com.teclo.smm.persistencia.vo.perfil.ConsultaPerfilVO;
import mx.com.teclo.smm.persistencia.vo.perfil.PerfilVO;
import mx.com.teclo.smm.persistencia.vo.usuario.UsuarioVO;

@Service
public class EmpresaPerfilServiceImpl implements EmpresaPerfilService {


	@Autowired
	private PerfilDAO perfilDAO;
	@Autowired
	private EmpresaPerfilDAO empresaPerfilDAO;
	@Autowired
	@Qualifier("aplicacionesDAOImpl")
	private AplicacionDAO aplicacionDAO;
	@Autowired
	private PerfilUsuarioDAO perfilUsuarioDAO;
	@Autowired
	private UsuarioFirmadoService usuarioFirmadoService;
	@Autowired
	@Qualifier("usuariosDAO")
	private UsuarioDAO usuarioDAO;

	@Value("${app.config.codigo}")
    private String codeApplication;

	@Autowired
	private EmpresaService empresaService;

	@Override
	@Transactional(readOnly = true)
	public List<PerfilVO> findProfilesByApp() {

		List<PerfilDTO> perfilesDTO = null;
		List<PerfilVO> profiles = new LinkedList<PerfilVO>();
		List<PerfilVO> resultQuery = new LinkedList<PerfilVO>();
		PerfilVO withoutProfile = new PerfilVO();

		withoutProfile.setIdPerfil(new Long(0));
		withoutProfile.setAplicacion(null);
		withoutProfile.setCdPerfil("SINPERFIL");
		withoutProfile.setNbPerfil("SIN PERFIL");
		withoutProfile.setTxPerfil("SIN PERFIL");
		withoutProfile.setStActivo(1);

		profiles.add(withoutProfile);

		perfilesDTO = empresaPerfilDAO.findProfilesByApp(codeApplication);
		resultQuery = ResponseConverter.converterLista(new ArrayList<>(), perfilesDTO, PerfilVO.class);

		profiles.addAll(resultQuery);
		return profiles;
	}

	@Override
	@Transactional(readOnly = true)
	public List<ConsultaPerfilVO> consultaPerfiles(){
		List<PerfilDTO> perfilesDTO = empresaPerfilDAO.findAllProfilesByApp(codeApplication);
		List<PerfilVO> perfiles = ResponseConverter.converterLista(new ArrayList<UsuarioVO>(), perfilesDTO, PerfilVO.class);
		List<ConsultaPerfilVO> consultaPerfiles = ResponseConverter.converterLista(new ArrayList<UsuarioVO>(), perfiles, ConsultaPerfilVO.class);

		return consultaPerfiles;
	}


	@Override
	@Transactional
	public boolean habilitaDeshabilitaPerfil(Long idPerfil, Boolean activo){
		PerfilDTO pDTO = empresaPerfilDAO.findUniqueProfileById(idPerfil, codeApplication, true);
		pDTO.setStActivo(activo ? 1:0);
		perfilDAO.saveOrUpdate(pDTO);

		List<PerfilUsuarioDTO> usuarios = perfilUsuarioDAO.findAll();

		//Iteramos todos las relaciones de perfiles con usuarios, si dan positivo con el perfil
		//Se deshabilitan
		if(usuarios.size()>0){
			for(PerfilUsuarioDTO user: usuarios){
				if(user.getPerfil().getIdPerfil() == pDTO.getIdPerfil()){
					user.setStActivo(activo ? 1:0);
					//perfilUsuarioDAO.saveOrUpdate(user);
				}
			}
		}

		return pDTO.getStActivo() != (!activo ? 1:0) ? true:false;
	}


	@Override
	@Transactional
	public PerfilDTO crearPerfil(PerfilVO pVO){

		Long idUsuario = usuarioFirmadoService.getUsuarioFirmadoVO().getId();

		PerfilDTO pDTO = new PerfilDTO();
		pDTO.setIdPerfil(1L);
		pDTO.setCdPerfil(pVO.getCdPerfil());
		pDTO.setNbPerfil(pVO.getNbPerfil());
		pDTO.setTxPerfil(pVO.getTxPerfil());
		pDTO.setAplicacion(aplicacionDAO.findAplicationById(codeApplication));
		pDTO.setStActivo(1);
		pDTO.setFhCreacion(new Date());
		pDTO.setIdUsrCreacion(idUsuario);
		pDTO.setFhModificacion(new Date());
		pDTO.setIdUsrModifica(idUsuario);

		perfilDAO.save(pDTO);

		return pDTO;
	}


	@Override
	@Transactional
	public boolean crearEmpresaPerfil(PerfilDTO perfilDTO){
		Long idUsuario = usuarioFirmadoService.getUsuarioFirmadoVO().getId();
		PerfilDTO perfilDTO2 = perfilDAO.findProfileByPerfil(perfilDTO);

		EmpresaPerfilDTO empresaPerfil = new EmpresaPerfilDTO();
		empresaPerfil.setIdEmpresa(empresaService.getIdEmpresa());
		empresaPerfil.setIdPerfil(perfilDTO2);
		empresaPerfil.setStActivo(1);
		empresaPerfil.setFhCreacion(new Date());
		empresaPerfil.setIdUsrCreacion(idUsuario);
		empresaPerfil.setFhModificacion(new Date());
		empresaPerfil.setIdUsrModifica(idUsuario);

		empresaPerfilDAO.save(empresaPerfil);

		return true;
	}

}