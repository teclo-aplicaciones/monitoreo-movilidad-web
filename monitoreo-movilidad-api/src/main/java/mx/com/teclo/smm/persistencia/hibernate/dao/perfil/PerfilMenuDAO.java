package mx.com.teclo.smm.persistencia.hibernate.dao.perfil;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.perfil.PerfilMenuDTO;

public interface PerfilMenuDAO extends BaseDAO<PerfilMenuDTO> {

	public Long findNextVal();
	public PerfilMenuDTO findProfilesByIds(Long idPerfil, Long idMenu);
	public List<PerfilMenuDTO> findProfilesByIdPerfil(Long idPerfil);
}
