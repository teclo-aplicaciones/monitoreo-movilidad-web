package mx.com.teclo.smm.persistencia.hibernate.dao.logs;

import java.util.List;

import mx.com.teclo.arquitectura.persistencia.comun.dao.BaseDao;
import mx.com.teclo.smm.persistencia.hibernate.dto.logs.WebLogsDTO;
/**
 *  Copyright (c) 2018, Teclo Mexicana. 
 * 
 *  Descripcion					: WebLogsDAO
 *  Historial de Modificaciones	: 
 *  Descripcion del Cambio 		: Modificación
 *  
 *  @author 					: fjmb
 *  @version 					: 1.0 
 *  Fecha 						: 02/Abril/2019
 */
public interface WebLogsDAO extends BaseDao<WebLogsDTO>{
	
	/**
     * Consulta los <b>Logs</b> por Identificador de log.
     * 
     * @param idLog 			Id de Log.
     * @return WebLogsDTO 		Regresa un objeto de tipo <b>WebLogsDTO</b>.
     * 
     * @author 				    javier07, fmartinez 
     * @version 			    2.0
     * Fecha				    01/Abril/2019
     */
    WebLogsDTO busquedaLogPorId(Long idLog);
	
    /**
     * Consulta todos los <b>Logs</b> por aplicación .
     * 
     * @param cdAplicacion 			Codigo de la aplicación.
     * @return List<WebLogsDTO> 	Regresa una lista de objetos de de tipo <b>List<WebLogsDTO></b>.
     * 
     * @author 				        javier07, fmartinez 
     * @version 			        2.0
     * Fecha				        01/Abril/2019
     */
    List<WebLogsDTO> busquedaLogsTodos(String cdAplicacion);
    
    /**
     * Consulta todos los <b>Logs</b> por aplicación .
     * 
     * @param descripcion 			Descripcion del log.
     * @param logNombre 			Nombre de log.
     * @param logTipoArch 			Tipo de archivo de log.
     * @param rutaArchivo 			Ruta del log.
     * 
     * @return Long				 	Regresa un tipo de dato Long
     * 
     * @author 				        javier07 
     * @version 			        2.0
     * Fecha				        01/Abril/2019
     */
    Long obtenerLog(String descripcion, String logNombre,String logTipoArch, String rutaArchivo);
      
}
