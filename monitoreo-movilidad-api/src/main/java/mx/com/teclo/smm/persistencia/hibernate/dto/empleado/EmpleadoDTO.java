package mx.com.teclo.smm.persistencia.hibernate.dto.empleado;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;

@Entity
@Table(name = "TMM015D_EP_EMPLEADOS")
public class EmpleadoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2538009126263023241L;

	@Id
	@Column(name="ID_EMPLEADO")
	private Long idEmpleado;
	@Column(name="ID_EMPRESA")
	private long idEmpresa;
	@Column(name="CD_EMPLEADO")
	private String cdEmpleado;
	@Column(name="NB_EMPLEADO")
	private String nbEmpleado;
	@Column(name="NB_PATERNO")
	private String nbPaterno;
	@Column(name="NB_MATERNO")
	private String nbMaterno;
	@Column(name="NB_MAIL")
	private String nbMail;
	@Column(name="NU_TELEFONO")
	private Long nuTelefono;
	@Column(name="FH_INGRESO")
	private Date fhIngreso;
	@Column(name="FH_BAJA")
	private Date fhBaja;
	@Column(name="ST_ACTIVO")
	private Boolean stActivo;
	@Column(name="FH_CREACION")
	private Date fhCreacion;
	@Column(name="ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name="FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name="ID_USR_MODIFICA")
	private Long idUsrModifica;
	
	public Long getIdEmpleado() {
		return idEmpleado;
	}
	public void setIdEmpleado(Long idEmpleado) {
		this.idEmpleado = idEmpleado;
	}
	public String getCdEmpleado() {
		return cdEmpleado;
	}
	public void setCdEmpleado(String cdEmpleado) {
		this.cdEmpleado = cdEmpleado;
	}
	public String getNbEmpleado() {
		return nbEmpleado;
	}
	public void setNbEmpleado(String nbEmpleado) {
		this.nbEmpleado = nbEmpleado;
	}
	public String getNbPaterno() {
		return nbPaterno;
	}
	public void setNbPaterno(String nbPaterno) {
		this.nbPaterno = nbPaterno;
	}
	public String getNbMaterno() {
		return nbMaterno;
	}
	public void setNbMaterno(String nbMaterno) {
		this.nbMaterno = nbMaterno;
	}
	public String getNbMail() {
		return nbMail;
	}
	public void setNbMail(String nbMail) {
		this.nbMail = nbMail;
	}
	public Long getNuTelefono() {
		return nuTelefono;
	}
	public void setNuTelefono(Long nuTelefono) {
		this.nuTelefono = nuTelefono;
	}
	public Date getFhIngreso() {
		return fhIngreso;
	}
	public void setFhIngreso(Date fhIngreso) {
		this.fhIngreso = fhIngreso;
	}
	public Date getFhBaja() {
		return fhBaja;
	}
	public void setFhBaja(Date fhBaja) {
		this.fhBaja = fhBaja;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
	public long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
}
