package mx.com.teclo.smm.negocio.service.empresa;

public interface EmpresaService {
	public Long buscarEmpresaUsuario(Long idUsuario);
	public long getIdEmpresa();
}
