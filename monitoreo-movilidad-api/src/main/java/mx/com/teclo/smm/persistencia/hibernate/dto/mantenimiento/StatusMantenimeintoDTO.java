package mx.com.teclo.smm.persistencia.hibernate.dto.mantenimiento;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TMM069M_CT_ST_MANTENIMIENTO")
public class StatusMantenimeintoDTO  implements Serializable{
	private static final long serialVersionUID = -4672969946131767954L;

	@Id
	@Column(name="ID_ST_MANTENIMIENTO")
	private Long idStatusMantenimiento;


	@Column(name="NB_ST_MANTENIMIENTO")
	private String nbStatusMantenimiento;
	@Column(name="TX_ST_MANTENIMIENTO")
	private String txStatusMantenimiento;
	@Column(name="TX_COLOR")
	private String txColor;

	@Column(name="ST_ACTIVO")
	private Boolean stActivo;
	@Column(name="FH_CREACION")
	private Date fhCreacion;
	@Column(name="ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name="FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name="ID_USR_MODIFICA")
	private Long idUsrModifica;
	public Long getIdStatusMantenimiento() {
		return idStatusMantenimiento;
	}
	public void setIdStatusMantenimiento(Long idStatusMantenimiento) {
		this.idStatusMantenimiento = idStatusMantenimiento;
	}
	public String getNbStatusMantenimiento() {
		return nbStatusMantenimiento;
	}
	public void setNbStatusMantenimiento(String nbStatusMantenimiento) {
		this.nbStatusMantenimiento = nbStatusMantenimiento;
	}
	public String getTxStatusMantenimiento() {
		return txStatusMantenimiento;
	}
	public void setTxStatusMantenimiento(String txStatusMantenimiento) {
		this.txStatusMantenimiento = txStatusMantenimiento;
	}
	public String getTxColor() {
		return txColor;
	}
	public void setTxColor(String txColor) {
		this.txColor = txColor;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}




}