package mx.com.teclo.smm.api.rest.grupo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.com.teclo.arquitectura.ortogonales.exception.NotFoundException;
import mx.com.teclo.smm.negocio.service.dispositivo.DispositivoService;
import mx.com.teclo.smm.negocio.service.empleado.EmpleadoService;
import mx.com.teclo.smm.negocio.service.grupo.GrupoService;
import mx.com.teclo.smm.persistencia.hibernate.dto.grupo.GrupoDTO;
import mx.com.teclo.smm.persistencia.vo.catalogo.CatalogoConSubtipoVO;
import mx.com.teclo.smm.persistencia.vo.catalogo.CatalogoVO;
import mx.com.teclo.smm.persistencia.vo.dispositivo.DispositivoVO;
import mx.com.teclo.smm.persistencia.vo.geocerca.DispositivoEnGeocercaVO;
import mx.com.teclo.smm.persistencia.vo.geocerca.GeocercaVO;
import mx.com.teclo.smm.persistencia.vo.grupo.ConsultaGrupoVO;
import mx.com.teclo.smm.persistencia.vo.grupo.GrupoVO;
import mx.com.teclo.smm.util.enumerados.CatalogoGeocercaEnum;

@RestController
public class GrupoRestController {
	
	@Autowired
	GrupoService grupoService;
	
	@Autowired
	EmpleadoService empleadoService;
	
	@Autowired
	DispositivoService dispositivoService;
	
	@RequestMapping("/consultarGrupos")
	public ResponseEntity<List<ConsultaGrupoVO>> consultarGrupos(@RequestParam("idTipoBusq") Long idTipoBusq, @RequestParam("valor") String valor)throws NotFoundException{
		List<ConsultaGrupoVO> lista = grupoService.consultaGrupos(idTipoBusq, valor);
		return new ResponseEntity<List<ConsultaGrupoVO>>(lista, HttpStatus.OK);
	}
	
	@RequestMapping("/buscarDispositivosEnGrupo")
	@PreAuthorize("hasAnyAuthority('BUSCA_DISPO_EN_GRUPO')")
	public ResponseEntity<List<CatalogoConSubtipoVO>> buscarDispositivoEnGrupo(@RequestParam("id")String tipo)throws NotFoundException{
		List<CatalogoConSubtipoVO> listaDispos = new ArrayList<CatalogoConSubtipoVO>();
		
		Long[] listaIds = grupoService.obtenerDispositivosEnGrupo(Long.parseLong(tipo));
		
		if(listaIds.length > 0)
			listaDispos = dispositivoService.buscarDispositivoConSubTipoDispositivo(listaIds);
		
		return new ResponseEntity<List<CatalogoConSubtipoVO>>(listaDispos, HttpStatus.OK);
	}
	
	@RequestMapping("/buscarGrupoAgrupamiento")
	//@PreAuthorize("hasAnyAuthority('BUSCA_GPO_AGRUPAR')")
	public ResponseEntity<List<CatalogoVO>> buscarCatalogoAgrupamiento(@RequestParam("id")String tipo){
		List<CatalogoVO> x = new ArrayList<CatalogoVO>();
		
		if(Long.parseLong(tipo) == CatalogoGeocercaEnum.DISPOSITIVO.getIdCat()){
			x = dispositivoService.buscarDispositivo(null);
		}else if(Long.parseLong(tipo) == CatalogoGeocercaEnum.USUARIOS.getIdCat()){
			x = empleadoService.buscarEmpleado();
		}
		
		return new ResponseEntity<List<CatalogoVO>>(x, HttpStatus.OK);
	}
	
	@RequestMapping("/buscarDispositivosEnGrupoParaGeocerca")
	@PreAuthorize("hasAnyAuthority('BUSCA_DISPO_EN_GRUPO_PARA_GEO')")
	public ResponseEntity<List<DispositivoEnGeocercaVO>> buscarDispositivoEnGrupoParaGeocerca(@RequestParam("id")String tipo)throws NotFoundException{
		List<DispositivoEnGeocercaVO> listaDispos = new ArrayList<DispositivoEnGeocercaVO>();
		
		Long[] listaIds = grupoService.obtenerDispositivosEnGrupo(Long.parseLong(tipo));
		
		if(listaIds.length > 0)
			listaDispos = dispositivoService.buscarDispositivoParaGeocerca(listaIds);
		
		return new ResponseEntity<List<DispositivoEnGeocercaVO>>(listaDispos, HttpStatus.OK);
	}
	
	@RequestMapping("/buscarDispositivosEnGrupoParaNotificacion")
	@PreAuthorize("hasAnyAuthority('BUSCA_DISPO_EN_GRUPO_PARA_NOTI')")
	public ResponseEntity<List<DispositivoEnGeocercaVO>> buscarDispositivoEnGrupoParaNotificacion(@RequestParam("id")String tipo)throws NotFoundException{
		List<DispositivoEnGeocercaVO> listaDispos = new ArrayList<DispositivoEnGeocercaVO>();
		
		Long[] listaIds = grupoService.obtenerDispositivosEnGrupo(Long.parseLong(tipo));
		
		if(listaIds.length > 0)
			listaDispos = dispositivoService.buscarDispositivoParaGeocerca(listaIds);
		
		return new ResponseEntity<List<DispositivoEnGeocercaVO>>(listaDispos, HttpStatus.OK);
	}
	
	@RequestMapping("/buscarGrupoDispositivos")
	//@PreAuthorize("hasAnyAuthority('BUSCA_DISPO_PARA_NOTI')")
	public ResponseEntity<List<DispositivoEnGeocercaVO>> buscarDispositivosParaGrupo(){
		
		List<DispositivoEnGeocercaVO> lista = dispositivoService.buscarDispositivoParaGeocerca(null);
		
		return new ResponseEntity<List<DispositivoEnGeocercaVO>>(lista, HttpStatus.OK);
	}
	
	@RequestMapping(value="/guardarGrupo", method=RequestMethod.PUT)
	//@PreAuthorize("hasAnyAuthority('BUSCA_DISPO_PARA_NOTI')")
	public ResponseEntity<Boolean> guardarGrupo(@RequestBody GrupoVO grupoVO ){
		
		GrupoDTO grupoDTO = grupoService.guardarGrupo(grupoVO);
		Boolean res = grupoService.guardarGrupoAsociado(grupoDTO,grupoVO);
		
		return new ResponseEntity<Boolean>(res, HttpStatus.OK);
	}
	
	@RequestMapping(value="/actualizarGrupo", method=RequestMethod.PUT)
	//@PreAuthorize("hasAnyAuthority('BUSCA_DISPO_PARA_NOTI')")
	public ResponseEntity<Boolean> actualizarGrupo(@RequestBody GrupoVO grupoVO ){
		
		Boolean res = grupoService.actualizarGrupo(grupoVO);
		
		return new ResponseEntity<Boolean>(res, HttpStatus.OK);
	}
	
	@RequestMapping("/buscarGrupoPorId")
	public ResponseEntity<GrupoVO> buscarGrupoPorId(@RequestParam("id")Long id)
	{
		GrupoVO gpoVO = grupoService.buscarGrupoParaActualizar(id);
		return new ResponseEntity<GrupoVO>(gpoVO, HttpStatus.OK);
	}
	
	@RequestMapping(value="/eliminarGrupo", method=RequestMethod.GET)
	//@PreAuthorize("hasAnyAuthority('BUSCA_DISPO_PARA_NOTI')")
	public ResponseEntity<Boolean> eliminarGrupo(@RequestParam("id") Long idGrupo){
		
		Boolean res = grupoService.eliminarGrupo(idGrupo);
		
		return new ResponseEntity<Boolean>(res, HttpStatus.OK);
	}
}
