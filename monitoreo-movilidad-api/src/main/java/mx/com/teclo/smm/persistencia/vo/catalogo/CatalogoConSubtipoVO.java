package mx.com.teclo.smm.persistencia.vo.catalogo;

public class CatalogoConSubtipoVO {
	private Long id;
	private String nombre;
	private Long subTipoDispositivo;
	
	public CatalogoConSubtipoVO()
	{}
	
	public CatalogoConSubtipoVO(Long id, String nombre)
	{
		this.id = id;
		this.nombre = nombre;
	}
	
	
	
	public Long getSubTipoDispositivo() {
		return subTipoDispositivo;
	}

	public void setSubTipoDispositivo(Long subTipoDispositivo) {
		this.subTipoDispositivo = subTipoDispositivo;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
