package mx.com.teclo.smm.negocio.service.logs;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import mx.com.teclo.smm.persistencia.hibernate.dao.logs.PerfilLogsDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.logs.WebLogsDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.logs.PerfilLogsDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.logs.WebLogsDTO;
import mx.com.teclo.smm.persistencia.vo.logs.ComboVO;
import mx.com.teclo.smm.persistencia.vo.logs.LogsBusquedaPorIdVO;
import mx.com.teclo.smm.persistencia.vo.logs.LogsBusquedaVO;
import mx.com.teclo.smm.persistencia.vo.logs.LogsConsultaComboVO;
import mx.com.teclo.smm.persistencia.vo.logs.LogsVO;

@Service
public class LogsConsultaServiceImpl implements LogsConsultaService {

	@Autowired
	private WebLogsDAO webLogsDAO;
	@Autowired
	private PerfilLogsDAO perfilLogsDAO;
	
	@Value("${app.config.codigo}")
	private String cdApplication;
	
	@Override
	@Transactional
	public LogsBusquedaPorIdVO busquedaLogPorId(Long idLog) {
		LogsBusquedaPorIdVO logVO = null;
		WebLogsDTO logDTO = webLogsDAO.busquedaLogPorId(idLog);

		if(logDTO != null) {
			logVO = new LogsBusquedaPorIdVO();
			logVO.setLogId(logDTO.getIdLogWeb());
			logVO.setLogNombre(logDTO.getNbLog());
			logVO.setLogDescripcion(logDTO.getTxLog());
			logVO.setLogEstatus(logDTO.getStActivo().toString());
			logVO.setRutaArchivo(logDTO.getNbRutaArchivo());
			logVO.setTipoExtensiones(logDTO.getNbExtension());
		}
		return logVO;
	}

	@Override
	@Transactional
	public List<LogsBusquedaVO> busquedaLogsTodos() {

		List<LogsBusquedaVO> listaLogsVO = null;
		List<WebLogsDTO> listWebLogsDTO =  webLogsDAO.busquedaLogsTodos(cdApplication);
		if(listWebLogsDTO != null) {
			listaLogsVO = new ArrayList<LogsBusquedaVO>();
			for (WebLogsDTO webLogDTO : listWebLogsDTO) {
	
				LogsBusquedaVO logVO = new LogsBusquedaVO();
				logVO.setLogId(webLogDTO.getIdLogWeb());
				logVO.setLogNombre(webLogDTO.getNbLog());
				logVO.setLogDescripcion(webLogDTO.getNbLog());
				logVO.setLogEstatus(webLogDTO.getStActivo().toString());
				logVO.setRutaArchivos(webLogDTO.getNbRutaArchivo());
				listaLogsVO.add(logVO);
			}
		}
		return listaLogsVO;
	}

	@Override
	@Transactional
	public List<LogsConsultaComboVO> busquedaLogsPorPerfil(Long perfilId) {
 
		List<LogsConsultaComboVO> logsConsultaComboVO = null;
		List<PerfilLogsDTO> listaPerfilesLogs = perfilLogsDAO.busquedaPorPerfil(perfilId);
		if(listaPerfilesLogs != null) {
			logsConsultaComboVO = new ArrayList<LogsConsultaComboVO>();
			
			for (PerfilLogsDTO perfilLogsDTO : listaPerfilesLogs) {
				LogsConsultaComboVO logVO = new LogsConsultaComboVO();
				logVO.setLogId(perfilLogsDTO.getWebLogsDTO().getIdLogWeb());
				logVO.setLogDescripcion(perfilLogsDTO.getWebLogsDTO().getTxLog());
				logVO.setLogEstatus(perfilLogsDTO.getWebLogsDTO().getStActivo().toString());
				logVO.setLogNombre(perfilLogsDTO.getWebLogsDTO().getNbLog());
				logVO.setRutaArchivos(perfilLogsDTO.getWebLogsDTO().getNbRutaArchivo());
				logsConsultaComboVO.add(logVO);
			}
		}
		return logsConsultaComboVO;
	}

	@Override
	@Transactional
	public List<ComboVO> perfilesAsignadosPorLog(Long idLog) {
 
		List<ComboVO> comboVO = null;
		List<PerfilLogsDTO> listaPerfilesLogs = perfilLogsDAO.perfilesAsignadosPorLog(idLog, cdApplication);
		if(listaPerfilesLogs != null) {
			comboVO = new ArrayList<ComboVO>();
			for (PerfilLogsDTO perfilLogsDTO : listaPerfilesLogs) {
				ComboVO combo = new ComboVO();
				combo.setComboValor(perfilLogsDTO.getPerfilDTO().getIdPerfil().toString());
				combo.setComboEtiqueta(perfilLogsDTO.getPerfilDTO().getNbPerfil().toString());
				combo.setComboValorExtra(null);
				comboVO.add(combo);
			}
		}

		return comboVO;
	}

	@Override
	@Transactional
	public List<ComboVO> perfilesNoAsignadosPorLog(Long idLog) {

		List<ComboVO> comboVO = new ArrayList<ComboVO>();
		comboVO = perfilLogsDAO.consultaPerfilesNoAsignados(idLog, cdApplication);
		return comboVO;
	}

	@Override
	public List<LogsVO> obtenerListaArchivosLogs(File file, LogsBusquedaPorIdVO logInfo, Long logId) {

		File[] listFiles = file.listFiles();

		List<LogsVO> listaArchivos = new ArrayList<LogsVO>();

		try {
			if (listFiles.length > 0) {

				LogsVO log;
				for (File archivo : listFiles) {
					if (archivo.isFile() && compruebaExtension(archivo, logInfo.getTipoExtensiones())) {
						log = new LogsVO();
						log.setLogId(logId);
						Calendar calendar = new GregorianCalendar(TimeZone.getTimeZone("America/Mexico_City"));
						calendar.setTimeInMillis(archivo.lastModified());
						log.setLogNombre(archivo.getName());
						log.setRutaArchivo(logInfo.getRutaArchivo());
						log.setFechaModficacion(calendar);
						log.setUltimaFechaModificacion(obtenerFechaString(calendar));
						listaArchivos.add(log);
					}
				}
				Collections.sort(listaArchivos, Collections.reverseOrder());
			}
		} catch (Exception exception) {
			System.out.println("ERROR EN LA CLASE [" + LogsConsultaServiceImpl.class + "]:" + exception.getMessage());
			exception.printStackTrace();
		}
		return listaArchivos;

	}

	/**
	 * Da formato a la <b>Fecha de Modificaci�n</> de un archivo.
	 *
	 * @return Regresa un objeto de tipo <b>String<b/>.
	 */
	public String obtenerFechaString(@RequestParam(value = "fecha") Calendar calendar) {
		String dia = Integer.toString(calendar.get(Calendar.DATE));
		String mes = Integer.toString(calendar.get(Calendar.MONTH) + 1);
		String annio = Integer.toString(calendar.get(Calendar.YEAR));
		String fecha = dia + "/" + mes + "/" + annio;
		return fecha;
	}

	/**
	 * Valida la extensión del archivo recibido
	 * 
	 * @param archivo
	 * @param campoExtensiones
	 * @return boolean
	 */
	public boolean compruebaExtension(File archivo, String campoExtensiones) {
		String[] extensiones = campoExtensiones.split("\\|");
		int indiceExtension = archivo.getName().lastIndexOf(".");
		String extensionArchivo = archivo.getName().substring(indiceExtension + 1);

		for (int contador = 0; contador < extensiones.length; contador++) {
			if (extensionArchivo.equalsIgnoreCase(extensiones[contador])) {
				return true;
			}
		}
		return false;
	}

}
