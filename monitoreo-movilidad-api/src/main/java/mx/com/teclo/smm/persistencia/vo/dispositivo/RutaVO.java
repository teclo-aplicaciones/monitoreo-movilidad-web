package mx.com.teclo.smm.persistencia.vo.dispositivo;

import java.util.List;

import mx.com.teclo.smm.persistencia.vo.monitoreo.EventoVO;

public class RutaVO {
	private String fecha;
	private List<EventoVO> events;
	private List<Double[]> coords;
	
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public List<EventoVO> getEvents() {
		return events;
	}
	public void setEvents(List<EventoVO> events) {
		this.events = events;
	}
	public List<Double[]> getCoords() {
		return coords;
	}
	public void setCoords(List<Double[]> coords) {
		this.coords = coords;
	}
}
