package mx.com.teclo.smm.negocio.service.evento;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.teclo.arquitectura.ortogonales.exception.NotFoundException;
import mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo.DispositivoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo.EventoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo.PosicionActualDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.grupo.GrupoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dao.grupo.GrupoDispositivoDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.EventoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.PosicionActualDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.grupo.GrupoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.grupo.GrupoDispositivoDTO;
import mx.com.teclo.smm.persistencia.vo.catalogo.CatalogoVO;
import mx.com.teclo.smm.persistencia.vo.evento.EventoAnualVO;
import mx.com.teclo.smm.persistencia.vo.evento.EventoVO;
import mx.com.teclo.smm.persistencia.vo.evento.EventosTablasHistoricoVO;
import mx.com.teclo.smm.persistencia.vo.evento.TCDispositivoDiarioVO;
import mx.com.teclo.smm.persistencia.vo.evento.TCDispositivosDiariosVO;
import mx.com.teclo.smm.persistencia.vo.evento.TCDispositivosVO;
import mx.com.teclo.smm.persistencia.vo.evento.TCEventoVO;
import mx.com.teclo.smm.persistencia.vo.evento.TCEventosDiariosVO;
import mx.com.teclo.smm.persistencia.vo.evento.TCGrupoVO;

@Service
@SuppressWarnings("unchecked")
public class EventoServiceImpl implements EventoService {

	@Autowired
	private EventoDAO eventoDAO;

	@Autowired
	private PosicionActualDAO posicionActualDAO;

	@Autowired
	GrupoDAO grupoDAO;

	@Autowired
	GrupoDispositivoDAO grupoDispositivoDAO;

	@Autowired
	DispositivoDAO dispositivoDAO;

	@Transactional
	@Override
	public List<EventoVO> consultarEventos() {
		List<EventoDTO> eventos = eventoDAO.findAll();
		List<EventoVO> eventosRet = new ArrayList<>();

		if(!eventos.isEmpty()) {
			for(EventoDTO ev : eventos) {
				eventosRet.add(new EventoVO(
						ev.getIdEvento(),
						ev.getIdTipoEvento().getIdTipoEvento(),
						ev.getIdSubtipoDispositivo().getIdSubTipoDispositivo(),
						ev.getCdEvento(),
						ev.getNbEvento(),
						ev.getTxEvento()) {
						private static final long serialVersionUID = -1882788314717975317L;
				});
			}
		}

		return eventosRet;
	}

	@Transactional
	@Override
	public List<EventosTablasHistoricoVO> consultarTiposEventos(Long idGrupo, String fecha, String dia) {
		return eventoDAO.consultarTiposEventos(idGrupo, fecha, dia);

	}

	@Transactional
	@Override
	public List<TCGrupoVO> consultarTiposEventosHistoricos(Long idMes){
		String mes = new Long(idMes+1L).toString();
		String valor = mes.length()>1 ?
				mes + "/" + new SimpleDateFormat("YYYY").format(new Date()):
				"0"+mes + "/" + new SimpleDateFormat("YYYY").format(new Date());

		Long conteo;

		List<TCGrupoVO> lista = new ArrayList<TCGrupoVO>();

		List<GrupoDTO> listaGrupos = grupoDAO.buscarGrupos();
		List<GrupoDispositivoDTO> listaRelacion;
		List<DispositivoDTO> listaDisposSinGrupo = grupoDispositivoDAO.buscarDispositivosSinGrupo();

		TCGrupoVO grupoVO;

		for(GrupoDTO gDTO : listaGrupos){
			listaRelacion = grupoDispositivoDAO.buscarDispositivosEnGrupo(gDTO);

			grupoVO = new TCGrupoVO();
			grupoVO.setTcNombre(gDTO.getNbGrupo());
			if(!listaRelacion.isEmpty()){
				grupoVO.setTcConteo(extraeEventosDispositivoHistorico(listaRelacion, valor));
			}else{
				grupoVO.setTcConteo(0L);
			}

			lista.add(grupoVO);
		}

		//Contar todos los dispositivos sin grupo
		grupoVO = new TCGrupoVO();
		grupoVO.setTcNombre("Sin Grupo");
		grupoVO.setTcConteo(extraeDispositivosSinGrupoHistorico(listaDisposSinGrupo, valor));
		lista.add(grupoVO);

		return lista;
	}

	@Override
	@Transactional
	public List<TCDispositivosVO> consultarEventosDispositivoHistorico(Long idMes){
		TCDispositivosVO dispo;
		List<TCDispositivosVO> listaDispos = new ArrayList<TCDispositivosVO>();

		List<DispositivoDTO> listaDDTOs = dispositivoDAO.buscarDispositivos();
		String mes = new Long(idMes+1L).toString();
		String valor = mes.length()>1 ?
				mes + "/" + new SimpleDateFormat("YYYY").format(new Date()):
				"0"+mes + "/" + new SimpleDateFormat("YYYY").format(new Date());

		for(DispositivoDTO dDTO:listaDDTOs){
			dispo = new TCDispositivosVO();

			dispo.setTcIdDispo(dDTO.getIdDispositivo());
			dispo.setTcNombre(dDTO.getNbDispositivo());
			dispo.setTcConteo(extraerEventosDispositivo(dDTO, valor));
			listaDispos.add(dispo);
		}
		return listaDispos;
	}

	@Override
	@Transactional
	public List<TCEventoVO> consultarGraficaEventosDispositivoHistorico(Long idDispo, Long idMes){
		List<TCEventoVO> listaEventos = new ArrayList<TCEventoVO>();

		String mes = new Long(idMes+1L).toString();
		String criterio = mes.length()>1 ?
				mes + "/" + new SimpleDateFormat("YYYY").format(new Date()):
				"0"+mes + "/" + new SimpleDateFormat("YYYY").format(new Date());

		DispositivoDTO dDTO = dispositivoDAO.buscarDispositivosPorId(idDispo);

		Map<String, Long> mapeo = extraeEventosDispositivoHistorico(criterio, dDTO.getNbTableDispPosici(), dDTO.getIdModelo().getIdSubtipoDispositivo().getIdSubTipoDispositivo());

		for(Map.Entry<String, Long> entry : mapeo.entrySet()){
			TCEventoVO eventoCnt = new TCEventoVO();
			eventoCnt.setTcNombre(entry.getKey());
			eventoCnt.setTcConteo(entry.getValue());

			listaEventos.add(eventoCnt);
		}

		return listaEventos;
	}

	@Transactional
	@Override
	public List<Map<String, Long>> consultarEventosAnualesHistoricos(Long idGrupo, Long idDispo, Long idAnio){

		List<Map<String, Long>> listaMapeo = new ArrayList<Map<String, Long>>();

		//Rellenamos la lista en vacios
		while(listaMapeo.size() < 12) listaMapeo.add(new HashMap<String, Long>());

		List<DispositivoDTO> listaDispos;

		if(idGrupo != 0){
			if(idDispo != 0){
				listaDispos = new ArrayList<>();
				listaDispos.add(dispositivoDAO.buscarDispositivosPorId(idDispo));
			}else{
				listaDispos = grupoDispositivoDAO.buscarDispositivosPorGrupoId(idGrupo);
			}
		}else{
			if(idDispo != 0){
				listaDispos = new ArrayList<>();
				listaDispos.add(dispositivoDAO.buscarDispositivosPorId(idDispo));
			}else{
				listaDispos = dispositivoDAO.findAll();
			}
		}

		for(DispositivoDTO dDTO: listaDispos){
			List<EventoAnualVO> listaEventosPorMes = eventoDAO.consultarEventosPorMesHistorico(dDTO.getNbTableDispPosici(), idAnio.toString());

			listaMapeo = extraerEventosAnualesPorMes(listaEventosPorMes, listaMapeo);
		}

		return listaMapeo;
	}

	@Transactional
	@Override
	public TCEventosDiariosVO consultarTiposEventosDiarios() throws NotFoundException{

		TCEventosDiariosVO eventoDiario;
		List<TCEventoVO> eventosConteo = new ArrayList<TCEventoVO>();

		List<PosicionActualDTO> listaActuales = posicionActualDAO.obtenerRegistrosHoy();
		Map<String,Long> conteo = new HashMap<String,Long>();

		if(!listaActuales.isEmpty()){
			//Contar de la tabla de Posiciones Actuales
			for(PosicionActualDTO paDTO : listaActuales){
				conteo.put(paDTO.getIdEvento().getNbEvento(), conteo.containsKey(paDTO.getIdEvento().getNbEvento())?conteo.get(paDTO.getIdEvento().getNbEvento())+1L:1L) ;
			}

			//Contar de la tabla de Historicos
			for(PosicionActualDTO paDTO : listaActuales){
				Map<String,Long> conteoHist = extraeEventoDiario(paDTO.getIdDispositivo().getNbTableDispPosici());

				for(Map.Entry<String, Long> entry : conteoHist.entrySet()){
					conteo.put(entry.getKey(), conteo.containsKey(entry.getKey()) ? conteo.get(entry.getKey())+entry.getValue():entry.getValue());
				}
			}
		}else{
			throw new NotFoundException("No existen registros del dia de hoy.");
		}

		for(Map.Entry<String, Long> entry : conteo.entrySet()){
			TCEventoVO eventoCnt = new TCEventoVO();
			eventoCnt.setTcNombre(entry.getKey());
			eventoCnt.setTcConteo(entry.getValue());

			eventosConteo.add(eventoCnt);
		}

		eventoDiario = new TCEventosDiariosVO();
		eventoDiario.setDatos(eventosConteo);
		return eventoDiario;
	}

	@Transactional
	@Override
	public TCDispositivoDiarioVO consultarEventosDispositivoDiarios(Long idDispositivo) throws NotFoundException{

		TCDispositivoDiarioVO dispoDiario;
		List<TCEventoVO> eventosConteo = new ArrayList<TCEventoVO>();
		List<PosicionActualDTO> listaActuales;
		if(idDispositivo != -1){
			listaActuales = new ArrayList<>();
			PosicionActualDTO unicoPosAct = posicionActualDAO.obtenerRegistroHoyPorDispositivo(idDispositivo);
			if(unicoPosAct != null)
				listaActuales.add(unicoPosAct);
		}else{
			listaActuales = posicionActualDAO.obtenerRegistrosHoy();
		}

		Map<String,Long> conteo = new HashMap<String,Long>();

		if(!listaActuales.isEmpty()){
			//Contar de la tabla de Posiciones Actuales
			for(PosicionActualDTO paDTO : listaActuales){
				conteo.put(paDTO.getIdEvento().getCdEvento(), conteo.containsKey(paDTO.getIdEvento().getCdEvento())?conteo.get(paDTO.getIdEvento().getCdEvento())+1L:1L) ;
			}

			//Contar de la tabla de Historicos
			for(PosicionActualDTO paDTO : listaActuales){
				Map<String,Long> conteoHist = extraeEventosDispositivoDiario(paDTO.getIdDispositivo().getNbTableDispPosici(), paDTO.getIdDispositivo().getIdModelo().getIdSubtipoDispositivo().getIdSubTipoDispositivo());

				for(Map.Entry<String, Long> entry : conteoHist.entrySet()){
					conteo.put(entry.getKey(), conteo.containsKey(entry.getKey()) ? conteo.get(entry.getKey())+entry.getValue():entry.getValue());
				}
			}
		}else{
			throw new NotFoundException("No existen registros del dia de hoy.");
		}

		for(Map.Entry<String, Long> entry : conteo.entrySet()){
			TCEventoVO eventoCnt = new TCEventoVO();
			eventoCnt.setTcNombre(entry.getKey());
			eventoCnt.setTcConteo(entry.getValue());
			eventosConteo.add(eventoCnt);
		}

		dispoDiario = new TCDispositivoDiarioVO();
		dispoDiario.setDatos(eventosConteo);
		return dispoDiario;
	}

	@Transactional
	@Override
	public TCDispositivosDiariosVO consultarDispositivosDiarios() throws NotFoundException{

		TCDispositivosDiariosVO disposDiarios;
		List<TCDispositivosVO> disposConteo = new ArrayList<TCDispositivosVO>();

		List<PosicionActualDTO> listaActuales = posicionActualDAO.obtenerRegistrosHoy();
		Map<String,Long> conteo = new HashMap<String,Long>();

		if(!listaActuales.isEmpty()){
			//Contar de la tabla de Posiciones Actuales
			for(PosicionActualDTO paDTO : listaActuales){
				conteo.put(paDTO.getIdDispositivo().getNbDispositivo(), conteo.containsKey(paDTO.getIdDispositivo().getNbDispositivo())?conteo.get(paDTO.getIdDispositivo().getNbDispositivo())+1L:1L) ;
			}

			//Contar de la tabla de Historicos
			for(PosicionActualDTO paDTO : listaActuales){
				conteo.put(paDTO.getIdDispositivo().getNbDispositivo(),
						conteo.containsKey(paDTO.getIdDispositivo().getNbDispositivo())
							?conteo.get(paDTO.getIdDispositivo().getNbDispositivo())+extraeDispositivosDiarios(paDTO.getIdDispositivo().getNbTableDispPosici())
							:extraeDispositivosDiarios(paDTO.getIdDispositivo().getNbTableDispPosici()));
			}
		}else{
			throw new NotFoundException("No existen registros del dia de hoy.");
		}

		for(Map.Entry<String, Long> entry : conteo.entrySet()){
			TCDispositivosVO dispoCnt = new TCDispositivosVO();
			dispoCnt.setTcNombre(entry.getKey());
			dispoCnt.setTcConteo(entry.getValue());
			dispoCnt.setTcIdDispo(asignarIdPorNombre(dispoCnt.getTcNombre(), listaActuales));
			disposConteo.add(dispoCnt);
		}

		disposDiarios = new TCDispositivosDiariosVO();
		disposDiarios.setDatos(disposConteo);
		return disposDiarios;
	}

	@Transactional
	@Override
	public List<EventosTablasHistoricoVO> consultarTiposEventosHoy(Long idGrupo, Long idDispositivo) {
		return eventoDAO.consultarTiposEventosHoy(idGrupo, idDispositivo);

	}

	@Transactional
	@Override
	public List<EventosTablasHistoricoVO> consultarTiposEventosDispositivo(Long idDispositivo, String fecha) {
		return eventoDAO.consultarTiposEventosDispositivo(idDispositivo, fecha);

	}

	@Transactional
	@Override
	public List<CatalogoVO> consultarTiposEventosDispoActuales(){
		return eventoDAO.consultarTipoEventosDispoActuales();
	}

	private Map extraeEventoDiario(String tablaDispo){
		Map<String,Long> conteoDispo = new HashMap<String, Long>();
		List<EventoDTO> listaEventos = eventoDAO.findAll();

		for(EventoDTO eDTO:listaEventos){
			Long conteo = eventoDAO.consultarEventosDiarios(tablaDispo,eDTO.getIdEvento());
			conteoDispo.put(eDTO.getNbEvento(), conteoDispo.containsKey(eDTO.getNbEvento())? conteoDispo.get(eDTO.getNbEvento())+conteo :conteo);
		}

		return conteoDispo;
	}

	private Map extraeEventosDispositivoDiario(String tablaDispo, Long idSubTipo){
		Map<String,Long> conteoDispo = new HashMap<String, Long>();
		List<EventoDTO> listaEventos = eventoDAO.buscarCatalogoEventosPorSubtipo(idSubTipo);

		for(EventoDTO eDTO:listaEventos){
			Long conteo = eventoDAO.consultarEventosDiarios(tablaDispo,eDTO.getIdEvento());
			conteoDispo.put(eDTO.getCdEvento(), conteoDispo.containsKey(eDTO.getCdEvento())? conteoDispo.get(eDTO.getCdEvento())+conteo :conteo);
		}

		return conteoDispo;
	}

	private Long extraeDispositivosDiarios(String tablaDispo){
		return eventoDAO.consultarDispositivoDiario(tablaDispo);
	}

	private Long asignarIdPorNombre(String nombre, List<PosicionActualDTO> lista){
		for(PosicionActualDTO pADTO: lista){
			if(pADTO.getIdDispositivo().getNbDispositivo().equals(nombre)){
				return pADTO.getIdDispositivo().getIdDispositivo();
			}
		}
		return -1L;
	}

	private Long extraeEventosDispositivoHistorico(List<GrupoDispositivoDTO> lista, String valor){
		Long conteoTotal = 0L;
		for(GrupoDispositivoDTO gDDTO:lista){
			conteoTotal = conteoTotal + eventoDAO.consultarEventosDispositivoHistorico(gDDTO.getIdDispositivo().getNbTableDispPosici(), valor);
		}
		return conteoTotal;
	}

	private Long extraeDispositivosSinGrupoHistorico(List<DispositivoDTO> lista, String valor){
		Long conteoTotal = 0L;
		for(DispositivoDTO dDTO:lista){
			conteoTotal = conteoTotal + eventoDAO.consultarEventosDispositivoHistorico(dDTO.getNbTableDispPosici(), valor);
		}
		return conteoTotal;
	}

	private Long extraerEventosDispositivo(DispositivoDTO dDTO, String valor){
		return eventoDAO.consultarEventosDispositivoHistorico(dDTO.getNbTableDispPosici(), valor);
	}

	private Map extraeEventosDispositivoHistorico(String criterio, String tablaDispo, Long idSubTipo){
		Map<String,Long> conteoDispo = new HashMap<String, Long>();
		List<EventoDTO> listaEventos = eventoDAO.buscarCatalogoEventosPorSubtipo(idSubTipo);

		for(EventoDTO eDTO:listaEventos){
			Long conteo = eventoDAO.consultarEventosHistorico(tablaDispo,eDTO.getIdEvento(), criterio);
			conteoDispo.put(eDTO.getCdEvento(), conteoDispo.containsKey(eDTO.getCdEvento())? conteoDispo.get(eDTO.getCdEvento())+conteo :conteo);
		}

		return conteoDispo;
	}

	private List<Map<String, Long>> extraerEventosAnualesPorMes(List<EventoAnualVO> listaDispos, List<Map<String, Long>> listaMap){

		for(int i = 0;i<listaDispos.size();i++)
		{
			//Buscamos la posicion que le corresponde
			Map<String, Long> mapa = listaMap.get(listaDispos.get(i).getMes().intValue()) == null ? new HashMap<String, Long>() : listaMap.get(listaDispos.get(i).getMes().intValue());
			mapa.put(listaDispos.get(i).getCdEvento(), mapa.containsKey(listaDispos.get(i).getCdEvento()) ? mapa.get(listaDispos.get(i).getCdEvento())+listaDispos.get(i).getConteo():listaDispos.get(i).getConteo());
			listaMap.set(listaDispos.get(i).getMes().intValue(), mapa);
		}

		return listaMap;
	}
}
