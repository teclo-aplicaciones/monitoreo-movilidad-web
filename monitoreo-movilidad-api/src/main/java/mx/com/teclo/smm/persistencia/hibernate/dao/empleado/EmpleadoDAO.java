package mx.com.teclo.smm.persistencia.hibernate.dao.empleado;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.empleado.EmpleadoDTO;

public interface EmpleadoDAO extends BaseDAO<EmpleadoDTO>{

	public List<EmpleadoDTO> buscarEmpleados(String param, String value);
	public EmpleadoDTO buscarEmpleado(Long id);
}
