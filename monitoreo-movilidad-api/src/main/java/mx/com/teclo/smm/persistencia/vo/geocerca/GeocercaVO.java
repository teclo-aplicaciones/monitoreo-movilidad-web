package mx.com.teclo.smm.persistencia.vo.geocerca;

import java.io.Serializable;
import java.util.List;

public class GeocercaVO implements Serializable{
private static final long serialVersionUID = 1L;
	
	private Long id;
	private String nombre;
	private Long tipo;
	private Long grupo;
	private String wkt;
	private Boolean activo;
	private String color;
	private String descripcion;
	private List<Double[]> coords;
	private List<DispoGeocercaVO> dispositivos;
	private GeoNotificacionVO notificacion;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Long getTipo() {
		return tipo;
	}
	public void setTipo(Long tipo) {
		this.tipo = tipo;
	}
	public String getWkt() {
		return wkt;
	}
	public void setWkt(String wkt) {
		this.wkt = wkt;
	}
	public Boolean getActivo() {
		return activo;
	}
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
	public Long getGrupo() {
		return grupo;
	}
	public void setGrupo(Long grupo) {
		this.grupo = grupo;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public List<Double[]> getCoords() {
		return coords;
	}
	public void setCoords(List<Double[]> coords) {
		this.coords = coords;
	}
	public List<DispoGeocercaVO> getDispositivos() {
		return dispositivos;
	}
	public void setDispositivos(List<DispoGeocercaVO> dispositivos) {
		this.dispositivos = dispositivos;
	}
	public GeoNotificacionVO getNotificacion() {
		return notificacion;
	}
	public void setNotificacion(GeoNotificacionVO notificacion) {
		this.notificacion = notificacion;
	}
}
