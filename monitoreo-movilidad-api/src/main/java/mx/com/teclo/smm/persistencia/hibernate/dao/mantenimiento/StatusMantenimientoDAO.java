package mx.com.teclo.smm.persistencia.hibernate.dao.mantenimiento;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.mantenimiento.StatusMantenimeintoDTO;


public interface StatusMantenimientoDAO extends BaseDAO<StatusMantenimeintoDTO>{	
	public StatusMantenimeintoDTO obtenerStatusMantenimientoPorId(Long id);
}
	