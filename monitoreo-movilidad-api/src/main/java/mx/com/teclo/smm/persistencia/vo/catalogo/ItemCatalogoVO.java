package mx.com.teclo.smm.persistencia.vo.catalogo;

public class ItemCatalogoVO {

	private Long idCatalogo;
	
	private Long id;
	private String cd;
	private String nb;
	private String tx;
	private String img;
	private String param;
	private String fhCreacion;

	private Long idMarca;
	private Long idTipoDispositivo;
	private Long idSubTipoDispositivo;
	private Long idTipoEvento;
	private Long idTipoGrupo;
	private Long idTipoComando;	
	private Long idTipoMedio;
	
	
	
			
	public Long getIdTipoMedio() {
		return idTipoMedio;
	}

	public void setIdTipoMedio(Long idTipoMedio) {
		this.idTipoMedio = idTipoMedio;
	}

	public String getFhCreacion() {
		return fhCreacion;
	}

	public void setFhCreacion(String fhCreacion) {
		this.fhCreacion = fhCreacion;
	}

	public Long getIdTipoComando() {
		return idTipoComando;
	}

	public void setIdTipoComando(Long idTipoComando) {
		this.idTipoComando = idTipoComando;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public Long getIdCatalogo() {
		return idCatalogo;
	}

	public void setIdCatalogo(Long idCatalogo) {
		this.idCatalogo = idCatalogo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCd() {
		return cd;
	}

	public void setCd(String cd) {
		this.cd = cd;
	}

	public String getNb() {
		return nb;
	}

	public void setNb(String nb) {
		this.nb = nb;
	}

	public String getTx() {
		return tx;
	}

	public void setTx(String tx) {
		this.tx = tx;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public Long getIdMarca() {
		return idMarca;
	}

	public void setIdMarca(Long idMarca) {
		this.idMarca = idMarca;
	}

	public Long getIdTipoDispositivo() {
		return idTipoDispositivo;
	}

	public void setIdTipoDispositivo(Long idTipoDispositivo) {
		this.idTipoDispositivo = idTipoDispositivo;
	}

	public Long getIdSubTipoDispositivo() {
		return idSubTipoDispositivo;
	}

	public void setIdSubTipoDispositivo(Long idSubTipoDispositivo) {
		this.idSubTipoDispositivo = idSubTipoDispositivo;
	}

	public Long getIdTipoEvento() {
		return idTipoEvento;
	}

	public void setIdTipoEvento(Long idTipoEvento) {
		this.idTipoEvento = idTipoEvento;
	}

	public Long getIdTipoGrupo() {
		return idTipoGrupo;
	}

	public void setIdTipoGrupo(Long idTipoGrupo) {
		this.idTipoGrupo = idTipoGrupo;
	}

}
