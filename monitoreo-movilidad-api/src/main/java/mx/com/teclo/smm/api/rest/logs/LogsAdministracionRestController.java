package mx.com.teclo.smm.api.rest.logs;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.com.teclo.arquitectura.ortogonales.seguridad.vo.UsuarioFirmadoVO;
import mx.com.teclo.arquitectura.ortogonales.service.comun.UsuarioFirmadoService;
import mx.com.teclo.smm.negocio.service.logs.LogsAdministracionService;
import mx.com.teclo.smm.negocio.service.usuario.UsuarioService;
import mx.com.teclo.smm.persistencia.vo.logs.LogsVO;
import mx.com.teclo.smm.util.enumerados.LogsEnum;

@RestController
@RequestMapping("/logs")
public class LogsAdministracionRestController {

	@Autowired
	private LogsAdministracionService logsAdministracionService;
	@Autowired
	private UsuarioFirmadoService usuarioFirmadoService;
	@Autowired
	UsuarioService usuarioService;

	
	@RequestMapping(value = "/crear", method = RequestMethod.POST)
	@PreAuthorize("hasAnyAuthority('LOGS_ALTA')")
	public LogsVO crearLog(@RequestBody LogsVO logVO) throws ParseException {
		UsuarioFirmadoVO usuarioFirmado = usuarioFirmadoService.getUsuarioFirmadoVO();
 		logVO.setCreadoPor(usuarioFirmado.getId());
		return logsAdministracionService.crudLog(logVO, LogsEnum.NUEVO_LOG.getAction());
	}

	/**
	 * Modifica <b>Logs</>.
	 * 
	 * @throws ParseException
	 */
	@RequestMapping(value = "/actualizar", method = RequestMethod.POST)
	@PreAuthorize("hasAnyAuthority('LOGS_ACTUALIZA')")
	public LogsVO modificarLog(@RequestBody LogsVO logVO) throws ParseException {
 		UsuarioFirmadoVO usuarioFirmado = usuarioFirmadoService.getUsuarioFirmadoVO();
 		logVO.setCreadoPor(usuarioFirmado.getId());
		return logsAdministracionService.crudLog(logVO, LogsEnum.MODIFICAR_LOG.getAction());
	}

	/**
	 * Agrega un <b>Perfil a un Log</>.
	 * 
	 * @throws ParseException
	 *
	 */
	@RequestMapping(value = "/agregarPerfil", method = RequestMethod.POST)
	@PreAuthorize("hasAnyAuthority('LOGS_ALTA_PERFIL')")
	public void agregarPerfil(@RequestBody LogsVO logVO) throws ParseException {
		UsuarioFirmadoVO usuarioFirmado = usuarioFirmadoService.getUsuarioFirmadoVO();
 		logVO.setCreadoPor(usuarioFirmado.getId());
 		logsAdministracionService.crudLog(logVO,  LogsEnum.AGREGAR_PERFIL.getAction());
	}

	/**
	 * Elimina un <b>Perfil de un Log</>.
	 * 
	 * @throws ParseException
	 *
	 */
	@RequestMapping(value = "/eliminarPerfil", method = RequestMethod.POST)
	@PreAuthorize("hasAnyAuthority('LOGS_BAJA_PERFIL')")
	public void eliminarPerfil(@RequestBody LogsVO logVO) throws ParseException {
		UsuarioFirmadoVO usuarioFirmado = usuarioFirmadoService.getUsuarioFirmadoVO();
 		logVO.setCreadoPor(usuarioFirmado.getId()); 
		logsAdministracionService.crudLog(logVO, LogsEnum.ELIMINAR_PERFIL.getAction());
	}

	/**
	 * Habilita o deshabilita la <b>Consulta de Logs</> dependiendo del log
	 * especificado.
	 *
	 */
	@RequestMapping(value = "/cambiarEstatus", method = RequestMethod.GET)
	@PreAuthorize("hasAnyAuthority('LOGS_CAMBIA_ESTATUS')")
	public void cambioDeEstatus(@RequestParam(value = "id") Long logId, @RequestParam(value = "accion") String accion) {
		logsAdministracionService.cambioDeEstatus(logId, accion);
	}
}
