package mx.com.teclo.smm.persistencia.hibernate.dto.componeteweb;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


import mx.com.teclo.smm.persistencia.hibernate.dto.menu.MenuDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.perfil.PerfilMenuDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.usuario.UsuarioDTO;

/**
 * @author sinuhe
 *
 */
@Entity
@Table(name = "TAQ050D_SE_USRPERMENU")
public class UsrPerMenuCompWebDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1789052192994848250L;
	

	@Id
	@GeneratedValue(generator="id_usrpermenu_generator",strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="id_usrpermenu_generator", sequenceName = "SQTAQ050D_SE_USRPERMENU",allocationSize=1)
	@Column(name = "ID_USRPERMENU")
	private Long idUsrPerMenu;	
	
	@ManyToOne
	@JoinColumn(name = "ID_USUARIO")
	private UsuarioDTO idUsuarioDTO; 
	
	@ManyToOne
	@JoinColumn(name = "ID_PERFIL_MENU")
	private PerfilMenuDTO idPerfilMenuDTO;
	
	@ManyToOne
	@JoinColumn(name = "ID_MENU")
	private MenuDTO idMenuDTO;
	
	/*
	@ManyToOne
	@JoinColumn(name = "ID_PAGINA")
	private MenuDTO idPaginaDTO;
	*/
	
	@Column(name = "ST_ACTIVO")
	private Boolean stActivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;
	public Long getIdUsrPerMenu() {
		return idUsrPerMenu;
	}
	public void setIdUsrPerMenu(Long idUsrPerMenu) {
		this.idUsrPerMenu = idUsrPerMenu;
	}
	public UsuarioDTO getIdUsuarioDTO() {
		return idUsuarioDTO;
	}
	public void setIdUsuarioDTO(UsuarioDTO idUsuarioDTO) {
		this.idUsuarioDTO = idUsuarioDTO;
	}
	public PerfilMenuDTO getIdPerfilMenuDTO() {
		return idPerfilMenuDTO;
	}
	public void setIdPerfilMenuDTO(PerfilMenuDTO idPerfilMenuDTO) {
		this.idPerfilMenuDTO = idPerfilMenuDTO;
	}
	public MenuDTO getIdMenuDTO() {
		return idMenuDTO;
	}
	public void setIdMenuDTO(MenuDTO idMenuDTO) {
		this.idMenuDTO = idMenuDTO;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}


	
	
	
}
