package mx.com.teclo.smm.persistencia.hibernate.dao.geocerca;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.geocerca.ConfigNotificaGeocercaDTO;

public interface ConfigNotificaGeocercaDAO extends BaseDAO<ConfigNotificaGeocercaDTO>{
	public List<ConfigNotificaGeocercaDTO> buscarCatalogo();
	public ConfigNotificaGeocercaDTO buscaCatalogoPorId(Long id);
}
