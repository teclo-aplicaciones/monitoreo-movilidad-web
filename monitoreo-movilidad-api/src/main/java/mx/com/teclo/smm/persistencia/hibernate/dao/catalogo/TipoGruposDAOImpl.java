package mx.com.teclo.smm.persistencia.hibernate.dao.catalogo;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.catalogo.CatalogoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.grupo.TipoGrupoDTO;

@SuppressWarnings("unchecked")
@Repository("tipoGruposDAO")
public class TipoGruposDAOImpl extends BaseDAOImpl<TipoGrupoDTO> implements TipoGruposDAO{

	@Override
	@Transactional(readOnly = true)
	public List<TipoGrupoDTO> obtenerTipoGrupos() {		
		Criteria query = getCurrentSession().createCriteria(TipoGrupoDTO.class);
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		//idEmpresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));

		return (List<TipoGrupoDTO>)query.list();
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<TipoGrupoDTO> activarDesactivarObtenerTipoGrupos() {		
		Criteria query = getCurrentSession().createCriteria(TipoGrupoDTO.class);
		//idEmpresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));

		query.addOrder(Order.asc("idTipoGrupo"));
		return (List<TipoGrupoDTO>)query.list();
	}
	
	@Override
	@Transactional(readOnly = true)
	public TipoGrupoDTO obtenerTipoGrupos(Long id) {		
		Criteria query = getCurrentSession().createCriteria(TipoGrupoDTO.class);
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		query.add(Restrictions.eq("idTipoGrupo", id));
		//idEmpresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));

		return (TipoGrupoDTO)query.uniqueResult();
	}
	
	@Override
	@Transactional(readOnly = true)
	public TipoGrupoDTO activarDesactivarObtenerTipoGrupos(Long id) {		
		Criteria query = getCurrentSession().createCriteria(TipoGrupoDTO.class);		
		query.add(Restrictions.eq("idTipoGrupo", id));
		//idEmpresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));

		return (TipoGrupoDTO)query.uniqueResult();
	}

}
