package mx.com.teclo.smm.persistencia.hibernate.dao.perfil;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.perfil.PerfilUsuarioDTO;
import mx.com.teclo.smm.persistencia.vo.perfil.PerfilVO;

/**
 *  Copyright (c) 2018, Teclo Mexicana. 
 * 
 *  Descripcion					: UsuarioDAO
 *  Historial de Modificaciones	: 
 *  Descripcion del Cambio 		: Creacion
 *  @author 					: fjmb
 *  @version 					: 1.0 
 *  Fecha 						: 05/Diciembre/2018
 */

public interface PerfilUsuarioDAO extends BaseDAO<PerfilUsuarioDTO> {
	
	public PerfilUsuarioDTO getPerfilUsuario(Long idUsuario,Long idPerfil);
	
//
//	public boolean savePerfilUsuario(String username,PerfilVO perfilVO);
//	public boolean updatePerfilUsuario(String username,PerfilVO perfilVO);
}
