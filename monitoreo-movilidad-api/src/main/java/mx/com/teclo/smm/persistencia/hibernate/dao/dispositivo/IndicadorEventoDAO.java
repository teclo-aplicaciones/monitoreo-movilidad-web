package mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.IndicadorEventoDTO;

public interface IndicadorEventoDAO extends BaseDAO<IndicadorEventoDTO>{
	public List<IndicadorEventoDTO> buscarIndicadores();
}
