package mx.com.teclo.smm.persistencia.hibernate.dao.componenteweb;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.componeteweb.CompWebPoliticaDTO;

@SuppressWarnings("unchecked")
@Repository("CompWebPoliticaDAO")
public class CompWebPoliticaDAOImpl extends BaseDAOImpl<CompWebPoliticaDTO> implements CompWebPoliticaDAO{

	@Override
	public List<CompWebPoliticaDTO> obtenerCompWebPoliticaPorUsrPerMenu(Long idUsrPerMenu) {
		Criteria query = getCurrentSession().createCriteria(CompWebPoliticaDTO.class);
		query.add(Restrictions.eq("idUsrPerMenuCompWebDTO.idUsrPerMenu", idUsrPerMenu));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return query.list();
	}

}
