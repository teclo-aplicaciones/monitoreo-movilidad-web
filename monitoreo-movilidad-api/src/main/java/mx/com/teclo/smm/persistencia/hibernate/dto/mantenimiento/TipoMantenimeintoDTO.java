package mx.com.teclo.smm.persistencia.hibernate.dto.mantenimiento;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TMM008C_CT_TIPO_MANTENIMIENTOS")
public class TipoMantenimeintoDTO  implements Serializable{

	private static final long serialVersionUID = -680820005775932871L;

	@Id
	@Column(name="ID_TIPO_MANTENIMIENTO")
	private Long idTipoMantenimiento;



	@Column(name="NB_TIPO_MANTENIMIENTO")
	private String nbTipoMantenimiento;
	@Column(name="TX_TIPO_MANTENIMIENTO")
	private String txTipoMantenimiento;
	@Column(name="ST_ACTIVO")
	private Boolean stActivo;
	@Column(name="FH_CREACION")
	private Date fhCreacion;
	@Column(name="ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name="FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name="ID_USR_MODIFICA")
	private Long idUsrModifica;
	public Long getIdTipoMantenimiento() {
		return idTipoMantenimiento;
	}
	public void setIdTipoMantenimiento(Long idTipoMantenimiento) {
		this.idTipoMantenimiento = idTipoMantenimiento;
	}
	public String getNbTipoMantenimiento() {
		return nbTipoMantenimiento;
	}
	public void setNbTipoMantenimiento(String nbTipoMantenimiento) {
		this.nbTipoMantenimiento = nbTipoMantenimiento;
	}
	public String getTxTipoMantenimiento() {
		return txTipoMantenimiento;
	}
	public void setTxTipoMantenimiento(String txTipoMantenimiento) {
		this.txTipoMantenimiento = txTipoMantenimiento;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}



}