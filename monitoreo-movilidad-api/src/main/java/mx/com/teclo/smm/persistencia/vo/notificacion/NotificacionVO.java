package mx.com.teclo.smm.persistencia.vo.notificacion;

import java.util.List;

import mx.com.teclo.smm.persistencia.vo.geocerca.DispoGeocercaVO;
import mx.com.teclo.smm.persistencia.vo.monitoreo.CoordVO;

public class NotificacionVO {
	private Long id;
	private String nombre;
	private Long tipo;
	private Long grupo;
	private Boolean activo;
	private Boolean enviado;
	private String base64;
	private Boolean isNow;
	private List<DispoGeocercaVO> dispositivos;
	private String mensaje;
	private String fecha;
	private String fechaCreacion;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Long getTipo() {
		return tipo;
	}
	public void setTipo(Long tipo) {
		this.tipo = tipo;
	}
	public Long getGrupo() {
		return grupo;
	}
	public void setGrupo(Long grupo) {
		this.grupo = grupo;
	}
	public Boolean getActivo() {
		return activo;
	}
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
	public Boolean getEnviado() {
		return enviado;
	}
	public void setEnviado(Boolean enviado) {
		this.enviado = enviado;
	}
	public String getBase64() {
		return base64;
	}
	public void setBase64(String base64) {
		this.base64 = base64;
	}
	public Boolean getIsNow() {
		return isNow;
	}
	public void setIsNow(Boolean isNow) {
		this.isNow = isNow;
	}
	public List<DispoGeocercaVO> getDispositivos() {
		return dispositivos;
	}
	public void setDispositivos(List<DispoGeocercaVO> dispositivos) {
		this.dispositivos = dispositivos;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
}
