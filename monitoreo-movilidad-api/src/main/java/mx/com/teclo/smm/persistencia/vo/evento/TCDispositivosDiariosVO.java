package mx.com.teclo.smm.persistencia.vo.evento;

import java.util.List;

public class TCDispositivosDiariosVO {
	private List<TCDispositivosVO> datos;

	public List<TCDispositivosVO> getDatos() {
		return datos;
	}

	public void setDatos(List<TCDispositivosVO> datos) {
		this.datos = datos;
	}
}
