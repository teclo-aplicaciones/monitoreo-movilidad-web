package mx.com.teclo.smm.persistencia.hibernate.dto.notificacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="TMM060C_CT_MEDIO_TIPO_NOTIFI")
public class MedioNotificacionDTO implements Serializable{

	private static final long serialVersionUID = 5507002531860075047L;
	@Id
	@Column(name="ID_MEDIO_TIPO_NOTIFI")
	private Long idMedioTipoNotifi;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_MEDIO")
	private MedioComunicacionDTO idMedio;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_TIPO_NOTIFICACION")
	private TipoNotificacionDTO idTipoNotificacion;
	@Column(name="ST_ACTIVO")
	private Boolean stActivo;
	@Column(name="FH_CREACION")
	private Date fhCreacion;
	@Column(name="ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name="FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name="ID_USR_MODIFICA")
	private Long idUsrModifica;

	public Long getIdMedioTipoNotifi() {
		return idMedioTipoNotifi;
	}
	public void setIdMedioTipoNotifi(Long idMedioTipoNotifi) {
		this.idMedioTipoNotifi = idMedioTipoNotifi;
	}
	public MedioComunicacionDTO getIdMedio() {
		return idMedio;
	}
	public void setIdMedio(MedioComunicacionDTO idMedio) {
		this.idMedio = idMedio;
	}
	public TipoNotificacionDTO getIdTipoNotificacion() {
		return idTipoNotificacion;
	}
	public void setIdTipoNotificacion(TipoNotificacionDTO idTipoNotificacion) {
		this.idTipoNotificacion = idTipoNotificacion;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}

}