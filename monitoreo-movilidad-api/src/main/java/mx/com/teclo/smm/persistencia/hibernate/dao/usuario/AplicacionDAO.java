package mx.com.teclo.smm.persistencia.hibernate.dao.usuario;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.usuario.AplicacionDTO;

public interface AplicacionDAO extends BaseDAO<AplicacionDTO>{
	public AplicacionDTO findAplicationById(String cdAplicacion);
}
