package mx.com.teclo.smm.persistencia.hibernate.dao.notificacion;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.MedioComunicacionDTO;

public interface MedioComunicacionDAO extends BaseDAO<MedioComunicacionDTO>{
	public MedioComunicacionDTO buscarMedioPorId(Long id);
	public List<MedioComunicacionDTO> obtenerCatalogo();
}
