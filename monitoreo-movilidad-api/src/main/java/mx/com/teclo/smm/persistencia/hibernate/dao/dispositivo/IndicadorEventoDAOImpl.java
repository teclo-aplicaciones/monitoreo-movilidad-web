package mx.com.teclo.smm.persistencia.hibernate.dao.dispositivo;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.IndicadorEventoDTO;

@SuppressWarnings("unchecked")
@Repository("indicadorEventoDAO")
public class IndicadorEventoDAOImpl extends BaseDAOImpl<IndicadorEventoDTO> implements IndicadorEventoDAO{

	@Override
	public List<IndicadorEventoDTO> buscarIndicadores()	{
		Criteria query = getCurrentSession().createCriteria(IndicadorEventoDTO.class);
		query.addOrder(Order.asc("idIndicadorGps"));
		return query.list();
	}
}
