package mx.com.teclo.smm.api.rest.dispositivo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.com.teclo.arquitectura.ortogonales.exception.NotFoundException;
import mx.com.teclo.smm.negocio.service.dispositivo.DispositivoService;
import mx.com.teclo.smm.persistencia.vo.dispositivo.DispositivoActivar;
import mx.com.teclo.smm.persistencia.vo.dispositivo.DispositivoGuardarVO;
import mx.com.teclo.smm.persistencia.vo.dispositivo.DispositivoSubtipoVO;
import mx.com.teclo.smm.persistencia.vo.dispositivo.DispositivoTableVO;
import mx.com.teclo.smm.persistencia.vo.dispositivo.DispositivoVO;
import mx.com.teclo.smm.persistencia.vo.dispositivo.InfoDispositivoVO;

@RestController
public class DispositivoRestController {

	@Autowired
	DispositivoService dispositivoService;
	
	/**
	 * 
	 * @param tipo {1:Nombre,2:IMEI,3:Marca,4:Modelo}
	 * @param valor
	 * @return
	 * @throws NotFoundException
	 */
	@RequestMapping("/consultarDispositivos")
	@PreAuthorize("hasAnyAuthority('CONSULTA_DISPO')")
	public ResponseEntity<List<DispositivoTableVO>> consultarDispositivos(@RequestParam("tipo")String tipo, @RequestParam("valor")String valor)throws NotFoundException
	{
		valor = valor == null ? "" : valor;
		List<DispositivoTableVO> x = dispositivoService.buscarDispositivosPorTipo(tipo, valor);
		return new ResponseEntity<List<DispositivoTableVO>>(x, HttpStatus.OK);
	}
	
	/**
	 *
	 * @param idDispositivo
	 * @return 
	 * @throws NotFoundException
	 */
	@RequestMapping("/consultarDispositivo")
	public ResponseEntity<DispositivoGuardarVO>consultarDispositivo(@RequestParam("id")Long idDispositivo)throws NotFoundException{
	
		DispositivoGuardarVO dispositivoGuardarVO = dispositivoService.buscarDispositivoPorId(idDispositivo);
		return new ResponseEntity<DispositivoGuardarVO>(dispositivoGuardarVO, HttpStatus.OK);
	}
	
	@RequestMapping("/consultarTodosLosDispositivos")
	public ResponseEntity<List<DispositivoSubtipoVO>>consultarDispositivos()throws NotFoundException{	
		List<DispositivoSubtipoVO> dispositivoTableVOs = dispositivoService.buscarDispositivos();
		return new ResponseEntity<List<DispositivoSubtipoVO>>(dispositivoTableVOs, HttpStatus.OK);
	}
		
	@RequestMapping(value="/guardarDispositivo", method=RequestMethod.PUT)
	@PreAuthorize("hasAnyAuthority('GUARDA_DISPO')")
	public ResponseEntity<DispositivoGuardarVO> guardarDispositivo(@RequestBody DispositivoGuardarVO dispositivoGuardarVO)
	{
		DispositivoGuardarVO dgVO = new DispositivoGuardarVO();
		Boolean status = true;
		try {
			dgVO = dispositivoService.guardarDispositivo(dispositivoGuardarVO, 1L);
			if(dgVO != null)
				status = dispositivoService.crearTablasDispositivo(dgVO.getIdDispositivo());
			else {
				dgVO = new DispositivoGuardarVO();
				dgVO.setIdDispositivo(0L); // El Imei ya esta registrado
			}
		}catch(Exception  e) {
			status = false;
			dgVO.setIdDispositivo(-1L); // Error de Inserccion
			e.printStackTrace();
		}
		
		if(status)
			return new ResponseEntity<DispositivoGuardarVO>(dgVO, HttpStatus.OK);
		return new ResponseEntity<DispositivoGuardarVO>(dgVO, HttpStatus.BAD_REQUEST);
		
	}
	
	@RequestMapping(value="/actualizarDispositivo", method=RequestMethod.PUT)
	@PreAuthorize("hasAnyAuthority('ACTUALIZA_DISP')")
	public ResponseEntity<DispositivoGuardarVO> actualizarDispositivo(@RequestBody DispositivoGuardarVO dispositivoGuardarVO){
		
		DispositivoGuardarVO dispositivoGuardarVO2 =  dispositivoService.actualizarDispositivo(dispositivoGuardarVO, 1L);
		
		return new ResponseEntity<DispositivoGuardarVO>(dispositivoGuardarVO2, HttpStatus.OK);
	}
	
	
	@RequestMapping(value="/infoDispositivos", method=RequestMethod.GET)
	@PreAuthorize("hasAnyAuthority('OBTENER_DISP')")
	public ResponseEntity<List<InfoDispositivoVO>> infoDispositivos()throws NotFoundException
	{
		List<InfoDispositivoVO> x = dispositivoService.infoDispositivos();
		return new ResponseEntity<List<InfoDispositivoVO>>(x, HttpStatus.OK);
	}
	
	@RequestMapping(value="/infoDispositivosDiarios", method=RequestMethod.GET)
	//@PreAuthorize("hasAnyAuthority('OBTENER_DISP')")
	public ResponseEntity<List<InfoDispositivoVO>> infoDispositivosDiarios()throws NotFoundException
	{
		List<InfoDispositivoVO> x = dispositivoService.infoDispositivosDiarios();
		return new ResponseEntity<List<InfoDispositivoVO>>(x, HttpStatus.OK);
	}
	
	@RequestMapping(value="/activarDesactivarDispositivo", method=RequestMethod.PUT)
	@PreAuthorize("hasAnyAuthority('HAB_DES_DISPO')")
	public ResponseEntity<Boolean> activarDesactivarDispositivo(@RequestBody DispositivoActivar dispositivoActivar)throws NotFoundException{
		Boolean status;
		status = dispositivoService.activarDesactivarDispositivo(dispositivoActivar.getId(),dispositivoActivar.getActivar());
		
		return new ResponseEntity<Boolean>(status, HttpStatus.OK);
	}
	
	@RequestMapping(value="/eliminarDispositivo", method=RequestMethod.PUT)
	@PreAuthorize("hasAnyAuthority('ELIMINA_DISPO_PERMA')")
	public ResponseEntity<Boolean> eliminarDispositivo(@RequestBody DispositivoVO dispositivoVO)throws NotFoundException{
		
		if(dispositivoService.eliminarDispositivo(dispositivoVO.getId())) {		
			return new ResponseEntity<Boolean>(true, HttpStatus.OK);
		}
		
		return new ResponseEntity<Boolean>(true, HttpStatus.INTERNAL_SERVER_ERROR);		
	}
}