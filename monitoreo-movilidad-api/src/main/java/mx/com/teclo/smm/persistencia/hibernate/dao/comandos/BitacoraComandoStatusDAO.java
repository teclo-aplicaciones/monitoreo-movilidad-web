package mx.com.teclo.smm.persistencia.hibernate.dao.comandos;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.BitacoraComandoDetalleDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.BitacoraStatusDTO;


public interface BitacoraComandoStatusDAO extends BaseDAO<BitacoraStatusDTO>{
	public BitacoraStatusDTO obtenerBitacoraStatus(Long idStatus);
}
