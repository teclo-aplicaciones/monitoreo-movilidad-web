package mx.com.teclo.smm.persistencia.hibernate.dao.catalogo;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.TipoEventoDTO;

@SuppressWarnings("unchecked")
@Repository("tipoEventosDAO")
public class TipoEventosDAOImpl extends BaseDAOImpl<TipoEventoDTO> implements TipoEventosDAO{

	@Override
	@Transactional(readOnly = true)
	public List<TipoEventoDTO> obtenerTipoEventos(){
		Criteria query = getCurrentSession().createCriteria(TipoEventoDTO.class);
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return query.list();
	}

	@Override
	@Transactional(readOnly = true)
	public List<TipoEventoDTO> activarDesactivarObtenerTipoEventos(){
		Criteria query = getCurrentSession().createCriteria(TipoEventoDTO.class);
		query.addOrder(Order.asc("idTipoEvento"));
		return query.list();
	}

	@Override
	@Transactional(readOnly = true)
	public TipoEventoDTO obtenerTipoEventos(Long id){
		Criteria query = getCurrentSession().createCriteria(TipoEventoDTO.class);
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		query.add(Restrictions.eq("idTipoEvento", id));
		return (TipoEventoDTO)query.uniqueResult();
	}

	@Override
	@Transactional(readOnly = true)
	public TipoEventoDTO activarDesactivarObtenerTipoEventos(Long id){
		Criteria query = getCurrentSession().createCriteria(TipoEventoDTO.class);
		query.add(Restrictions.eq("idTipoEvento", id));
		return (TipoEventoDTO)query.uniqueResult();
	}
}
