package mx.com.teclo.smm.persistencia.hibernate.dao.mantenimiento;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.mantenimiento.BusqMantenimeintoDTO;


public interface BusqMantenimientoDAO extends BaseDAO<BusqMantenimeintoDTO>{
	public List<BusqMantenimeintoDTO> obtenerBusqMantenimeinto();
	
}
