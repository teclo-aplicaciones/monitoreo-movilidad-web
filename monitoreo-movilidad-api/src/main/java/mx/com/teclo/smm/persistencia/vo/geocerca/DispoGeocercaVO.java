package mx.com.teclo.smm.persistencia.vo.geocerca;

public class DispoGeocercaVO {
	private Long idItem;
	private String srItem;
	private String txtItem;
	
	public Long getIdItem() {
		return idItem;
	}
	public void setIdItem(Long idItem) {
		this.idItem = idItem;
	}
	public String getTxtItem() {
		return txtItem;
	}
	public void setTxtItem(String txtItem) {
		this.txtItem = txtItem;
	}
	public String getSrItem() {
		return srItem;
	}
	public void setSrItem(String srItem) {
		this.srItem = srItem;
	}
}
