package mx.com.teclo.smm.persistencia.hibernate.dao.notificacion;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.MedioNotificacionDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.NotificacionDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.TipoNotificacionDTO;

public interface NotificacionDAO extends BaseDAO<NotificacionDTO>{
	public Long buscarSiguienteIdentificador();
	public NotificacionDTO buscarNotificacionPorId(Long id);
	public List<NotificacionDTO> buscarNotificacionPorNombre(String nombre, TipoNotificacionDTO tNDTO, MedioNotificacionDTO mnDTO);
}
