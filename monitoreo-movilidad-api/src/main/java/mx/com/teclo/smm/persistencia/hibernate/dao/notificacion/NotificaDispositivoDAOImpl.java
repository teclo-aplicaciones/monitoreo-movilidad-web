package mx.com.teclo.smm.persistencia.hibernate.dao.notificacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.MedioComunicacionDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.MedioNotificacionDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.NotificaDispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.NotificacionDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.TipoNotificacionDTO;

@SuppressWarnings("unchecked")
@Repository("notificaDispositivoDAO")
public class NotificaDispositivoDAOImpl extends BaseDAOImpl<NotificaDispositivoDTO> implements NotificaDispositivoDAO{

	@Autowired
	MedioNotificacionDAO medioNotificacionDAO;
	
	@Override
	public Long buscarSiguienteIdentificador(){
		SQLQuery query = getCurrentSession().createSQLQuery("SELECT SQTMM024D_DP_DISP_NOT.nextval FROM DUAL");
		BigDecimal bd = (BigDecimal)query.uniqueResult();
		
		return (Long)bd.longValueExact();
	}
	
	@Override
	public Boolean validaPushEnviados(NotificacionDTO nDTO){
		Criteria query = getCurrentSession().createCriteria(NotificaDispositivoDTO.class);
		query.add(Restrictions.eq("idNotificacion", nDTO));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		
		List<NotificaDispositivoDTO> lista= (List<NotificaDispositivoDTO>)query.list();
		return lista.isEmpty();
	}
	
	@Override
	public List<NotificaDispositivoDTO> buscarDispositivosGeocerca(DispositivoDTO dDTO){
		Criteria query = getCurrentSession().createCriteria(NotificaDispositivoDTO.class);
		query.add(Restrictions.eq("idDispositivo", dDTO));
		return (List<NotificaDispositivoDTO>) query.list();
	}
	
	@Override
	public List<NotificaDispositivoDTO> buscarDispositivosNotificacion(DispositivoDTO dDTO){
		Criteria query = getCurrentSession().createCriteria(NotificaDispositivoDTO.class);
		query.add(Restrictions.eq("idDispositivo", dDTO));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return (List<NotificaDispositivoDTO>) query.list();
	}
	
	@Override
	public List<NotificaDispositivoDTO> buscarTodoDispositivosNotificacion(DispositivoDTO dDTO, TipoNotificacionDTO tNDTO, MedioNotificacionDTO mCDTO){
		Criteria query = getCurrentSession().createCriteria(NotificaDispositivoDTO.class);
		query.createAlias("idNotificacion", "idNotificacion");
		query.createAlias("idNotificacion.idMedioTipoNotifi", "idMedioTipoNotifi");
		query.createAlias("idNotificacion.idMedioTipoNotifi.idTipoNotificacion", "idTipoNotificacion");
		query.add(Restrictions.eq("idDispositivo", dDTO));
		query.add(Restrictions.eq("idTipoNotificacion.idTipoNotificacion", tNDTO.getIdTipoNotificacion()));
		if(mCDTO != null)
			query.add(Restrictions.eq("idMedioTipoNotifi.idMedioTipoNotifi", mCDTO.getIdMedioTipoNotifi()));
		
		return (List<NotificaDispositivoDTO>) query.list();
	}
	
	@Override
	public List<NotificaDispositivoDTO> buscarDispositivosEnNotificacion(NotificacionDTO nDTO){
		Criteria query = getCurrentSession().createCriteria(NotificaDispositivoDTO.class);
		query.add(Restrictions.eq("idNotificacion", nDTO));
		//query.add(Restrictions.eq("stActivo", Boolean.TRUE));

		return (List<NotificaDispositivoDTO>) query.list();
	}
	
	@Override
	public List<NotificaDispositivoDTO> buscarTodoDispositivosEnNotificacion(NotificacionDTO nDTO){
		Criteria query = getCurrentSession().createCriteria(NotificaDispositivoDTO.class);
		query.add(Restrictions.eq("idNotificacion", nDTO));

		return (List<NotificaDispositivoDTO>) query.list();
	}
}
