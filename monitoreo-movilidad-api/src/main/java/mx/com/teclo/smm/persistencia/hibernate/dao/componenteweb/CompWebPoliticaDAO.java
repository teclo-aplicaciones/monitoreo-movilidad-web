package mx.com.teclo.smm.persistencia.hibernate.dao.componenteweb;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.componeteweb.CompWebPoliticaDTO;

public interface CompWebPoliticaDAO extends BaseDAO<CompWebPoliticaDTO> {
	List<CompWebPoliticaDTO> obtenerCompWebPoliticaPorUsrPerMenu(Long idUsrPerMenu);
}
