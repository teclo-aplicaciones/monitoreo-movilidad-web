package mx.com.teclo.smm.persistencia.hibernate.dao.perfil;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.perfil.PerfilUsuarioDTO;

/**
 *  Copyright (c) 2018, Teclo Mexicana.
 *
 *  Descripcion					: UsuarioService
 *  Historial de Modificaciones	:
 *  Descripcion del Cambio 		: Creacion
 *  @author 					: fjmb
 *  @version 					: 1.0
 *  Fecha 						: 05/Diciembre/2018
 */

@Repository
public class PerfilUsuarioDAOImpl extends BaseDAOImpl<PerfilUsuarioDTO> implements PerfilUsuarioDAO {

	@Override
	public PerfilUsuarioDTO getPerfilUsuario(Long idUsuario, Long idPerfil) {
		Criteria c =getCurrentSession().createCriteria(PerfilUsuarioDTO.class);
		c.createAlias("perfil", "p");
		c.createAlias("usuario", "usr");

		c.add(Restrictions.eq("usr.idUsuario", idUsuario));
		c.add(Restrictions.eq("p.idPerfil", idPerfil));

		// idEmpresa
		//c.add(Restrictions.eq("usr.idEmpresa", getIdEmpresa()));
		//c.add(Restrictions.eq("p.idEmpresa", getIdEmpresa()));

		return (PerfilUsuarioDTO)c.uniqueResult();
	}

}
