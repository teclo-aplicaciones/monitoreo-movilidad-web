package mx.com.teclo.smm.persistencia.hibernate.dto.mantenimiento;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TMM068C_CT_BUSQ_MANTENIMIENTO")
public class BusqMantenimeintoDTO  implements Serializable{


	/**
	 *
	 */
	private static final long serialVersionUID = 9104348182201133721L;

	@Id
	@Column(name="ID_BUSQ_MANTENIMIENTO")
	private Long idBusqMantenimiento;

	@Column(name="NB_BUSQ_MANTENIMIENTO")
	private String nbBusqMantenimiento;
	@Column(name="ST_ACTIVO")
	private Boolean stActivo;
	@Column(name="FH_CREACION")
	private Date fhCreacion;
	@Column(name="ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name="FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name="ID_USR_MODIFICA")
	private Long idUsrModifica;
	public Long getIdBusqMantenimiento() {
		return idBusqMantenimiento;
	}
	public void setIdBusqMantenimiento(Long idBusqMantenimiento) {
		this.idBusqMantenimiento = idBusqMantenimiento;
	}
	public String getNbBusqMantenimiento() {
		return nbBusqMantenimiento;
	}
	public void setNbBusqMantenimiento(String nbBusqMantenimiento) {
		this.nbBusqMantenimiento = nbBusqMantenimiento;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}





}