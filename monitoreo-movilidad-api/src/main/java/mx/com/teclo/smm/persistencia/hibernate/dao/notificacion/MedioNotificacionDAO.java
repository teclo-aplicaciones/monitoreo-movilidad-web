package mx.com.teclo.smm.persistencia.hibernate.dao.notificacion;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.MedioComunicacionDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.MedioNotificacionDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.notificacion.TipoNotificacionDTO;

public interface MedioNotificacionDAO extends BaseDAO<MedioNotificacionDTO>{
	public List<MedioNotificacionDTO> buscarCatalogo(TipoNotificacionDTO tnDTO);
	public MedioNotificacionDTO buscarMedioPorId(Long id);
}
