package mx.com.teclo.smm.persistencia.hibernate.dto.geocerca;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TMM063C_GL_CONFIG_GEO_NOTI")
public class ConfigNotificaGeocercaDTO implements Serializable{

	private static final long serialVersionUID = -3571303350791524318L;
	
	@Id
	@Column(name="ID_CONFIG_GEO_NOTI")
	private Long idConfigGeoNoti;
	@Column(name="TX_CONFIG_GEO_NOTI")
	private String txConfigGeoNoti;
	@Column(name="CD_PARAMETRO")
	private String cdParametro;
	@Column(name="NB_TITULO")
	private String nbTitulo;
	@Column(name="TX_MENSAJE")
	private String txMensaje;
	@Column(name="ST_ACTIVO")
	private Boolean stActivo;
	@Column(name="FH_CREACION")
	private Date fhCreacion;
	@Column(name="ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name="FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name="ID_USR_MODIFICA")
	private Long idUsrModifica;
	
	public Long getIdConfigGeoNoti() {
		return idConfigGeoNoti;
	}
	public void setIdConfigGeoNoti(Long idConfigGeoNoti) {
		this.idConfigGeoNoti = idConfigGeoNoti;
	}
	public String getTxConfigGeoNoti() {
		return txConfigGeoNoti;
	}
	public void setTxConfigGeoNoti(String txConfigGeoNoti) {
		this.txConfigGeoNoti = txConfigGeoNoti;
	}
	public String getCdParametro() {
		return cdParametro;
	}
	public void setCdParametro(String cdParametro) {
		this.cdParametro = cdParametro;
	}
	public String getNbTitulo() {
		return nbTitulo;
	}
	public void setNbTitulo(String nbTitulo) {
		this.nbTitulo = nbTitulo;
	}
	public String getTxMensaje() {
		return txMensaje;
	}
	public void setTxMensaje(String txMensaje) {
		this.txMensaje = txMensaje;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
}
