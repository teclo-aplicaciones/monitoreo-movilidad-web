package mx.com.teclo.smm.persistencia.hibernate.dao.comandos;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.BitacoraComandoDetalleDTO;


public interface BitacoraComandoDetalleDAO extends BaseDAO<BitacoraComandoDetalleDTO>{
	public List<BitacoraComandoDetalleDTO> obtenerBitacoraComandoDetalle(Long idBitacora);
}
