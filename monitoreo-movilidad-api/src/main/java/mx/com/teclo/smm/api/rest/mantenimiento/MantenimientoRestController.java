package mx.com.teclo.smm.api.rest.mantenimiento;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.com.teclo.smm.negocio.service.mantenimiento.MantenimientoService;
import mx.com.teclo.smm.persistencia.hibernate.dto.mantenimiento.BusqMantenimeintoDTO;
import mx.com.teclo.smm.persistencia.vo.mantenimiento.MantenimientoVO;

@RestController
public class MantenimientoRestController {
	
	@Autowired
	MantenimientoService mantenimientoService;

	@RequestMapping("/consultaMantenimientoBusqueda")
	public ResponseEntity<List<MantenimientoVO>> consultarMantenimiento(@RequestParam("tipo") String tipo, @RequestParam("valor") String valor){
		List<MantenimientoVO> x = new ArrayList<MantenimientoVO>();		
		x = mantenimientoService.consultarMantenimiento(tipo, valor);		
		return new ResponseEntity<List<MantenimientoVO>>(x, HttpStatus.OK);
	}
	
	@RequestMapping("/buscarMantenimientoPorId")
	public ResponseEntity<MantenimientoVO> buscarMantenimientoPorId(@RequestParam("id")String id){
		MantenimientoVO manVO = null;		
		manVO = mantenimientoService.buscarMantenimientoPorId(Long.parseLong(id));		
		return new ResponseEntity<MantenimientoVO>(manVO, HttpStatus.OK);
	}
		
	@RequestMapping("/listaDispositivos")
	public ResponseEntity<List<MantenimientoVO>> listaDispositivos(){
		List<MantenimientoVO> x = mantenimientoService.listaDispositivos();		
		return new ResponseEntity<List<MantenimientoVO>>(x, HttpStatus.OK);
	}
	
	@RequestMapping("/tipoMantenimiento")
	public ResponseEntity<List<MantenimientoVO>> tipoMantenimiento(){
		List<MantenimientoVO> x = mantenimientoService.tipoMantenimiento();		
		return new ResponseEntity<List<MantenimientoVO>>(x, HttpStatus.OK);
	}

	@RequestMapping("/busqMantenimiento")
	public ResponseEntity<List<Map<String,String>>> busqMantenimiento(){		
		List<Map<String,String>> list = new ArrayList<>();		
		for(BusqMantenimeintoDTO x : mantenimientoService.busqMantenimiento()) {
			Map<String,String> map = new HashMap<>();
			map.put("id",""+x.getIdBusqMantenimiento());
			map.put("nombre",""+x.getNbBusqMantenimiento());			
			list.add(map);
		}
		
		return new ResponseEntity<List<Map<String,String>>>(list, HttpStatus.OK);
	}
	
	
	@PostMapping("/agregaMantenimiento")
	public ResponseEntity<String> agregaMantenimiento(@RequestBody MantenimientoVO mantenimientoVO) {		
		if (mantenimientoService.agregaMantenimiento(mantenimientoVO)) {
			return new ResponseEntity<String>("", HttpStatus.OK);
		}
		return new ResponseEntity<String>("", HttpStatus.BAD_REQUEST);
	}
	
	@RequestMapping("/actualizaMantenimiento")
	public ResponseEntity<String> actualizaMantenimiento(@RequestBody MantenimientoVO mantenimientoVO) {
		if (mantenimientoService.actualizaMantenimiento(mantenimientoVO)) {
			return new ResponseEntity<String>("", HttpStatus.OK);
		}
		return new ResponseEntity<String>("", HttpStatus.BAD_REQUEST);
	}
	
	
	@RequestMapping("/eliminarMantenimiento")
	public ResponseEntity<String> eliminarMantenimiento(@RequestParam("idMantenimiento") Long idMantenimiento) {
		if (mantenimientoService.eliminarMantenimiento(idMantenimiento)) {
			return new ResponseEntity<String>("", HttpStatus.OK);
		}
		return new ResponseEntity<String>("", HttpStatus.BAD_REQUEST);
	}
	
	@RequestMapping("/timelineMantenimiento")
	public ResponseEntity<List<List<MantenimientoVO>>> timelineMantenimiento(@RequestParam("idMantenimiento") Long idMantenimiento,@RequestParam("idUsuarioOIMEI") String idUsuarioOIMEI,@RequestParam("tipoTimeline") int tipoTimeline) {		
		List<List<MantenimientoVO>> x = mantenimientoService.timelineMantenimiento(idMantenimiento,idUsuarioOIMEI,tipoTimeline);
		return new ResponseEntity<List<List<MantenimientoVO>>>(x, HttpStatus.OK);
	}
	
	@RequestMapping("/finalizarMantenimiento")
	public ResponseEntity<String> finalizarMantenimiento(@RequestParam("idMantenimiento") Long idMantenimiento, @RequestParam("observaciones")String observaciones) {
		if (mantenimientoService.finalizarMantenimiento(idMantenimiento,observaciones)) {
			return new ResponseEntity<String>("", HttpStatus.OK);
		}
		return new ResponseEntity<String>("", HttpStatus.BAD_REQUEST);
	}
	
}
