package mx.com.teclo.smm.persistencia.hibernate.dao.logs;


import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.com.teclo.arquitectura.persistencia.comun.dao.BaseDaoHibernate;
import mx.com.teclo.smm.negocio.service.empresa.EmpresaService;
import mx.com.teclo.smm.persistencia.hibernate.dto.logs.WebLogsDTO;

@SuppressWarnings("unchecked")
@Repository
public class WebLogsDAOImpl extends BaseDaoHibernate<WebLogsDTO> implements WebLogsDAO{


	@Autowired
	private EmpresaService empresaService;

	@Override
	public WebLogsDTO busquedaLogPorId(Long idLog) {
		Criteria criteria =  getCurrentSession().createCriteria(WebLogsDTO.class);
		criteria.add(Restrictions.eq("idLogWeb", idLog));
		return (WebLogsDTO) criteria.uniqueResult();
	}

	@Override
	public List<WebLogsDTO> busquedaLogsTodos(String cdAplicacion) {

		Criteria criteria = getCurrentSession().createCriteria(WebLogsDTO.class);
		criteria.createAlias("aplicacion", "aplicacion");
		criteria.add(Restrictions.eq("aplicacion.cdAplicacion", cdAplicacion));

		//idEmpresa
		//criteria.add(Restrictions.eq("idEmpresa", empresaService.getIdEmpresa()));

		criteria.addOrder(Order.asc("idLogWeb"));
        return  criteria.list();
	}

 	@Override
	public Long obtenerLog(String descripcion, String logNombre, String logTipoArch, String rutaArchivo) {
		List<WebLogsDTO> lista = null;
		Criteria criteria = getCurrentSession().createCriteria(WebLogsDTO.class);
		criteria.add(Restrictions.eq("txLog", descripcion));
		criteria.add(Restrictions.eq("nbLog", logNombre));
		criteria.add(Restrictions.eq("nbExtension", logTipoArch));
		criteria.add(Restrictions.eq("nbRutaArchivo", rutaArchivo));

		lista  = criteria.list();
		return lista.get(0).getIdLogWeb();
	}

}
