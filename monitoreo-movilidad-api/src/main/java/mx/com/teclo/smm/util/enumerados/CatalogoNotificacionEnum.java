package mx.com.teclo.smm.util.enumerados;

import java.util.ArrayList;
import java.util.List;

import mx.com.teclo.smm.persistencia.vo.catalogo.CatalogoVO;

public enum CatalogoNotificacionEnum {
	NOMBRE_NOTIFICACION(1L, "Nombre Notificacion"), DISPOSITIVO(2L, "Dispositivo"), USUARIOS(3L, "Usuario");
	
	private Long idCat;
	private String nombreCat;
	
	private CatalogoNotificacionEnum(Long idCat, String nombreCat){
		this.idCat = idCat;
		this.nombreCat = nombreCat;
	}

	public Long getIdCat() {
		return idCat;
	}

	public void setIdCat(Long idCat) {
		this.idCat = idCat;
	}

	public String getNombreCat() {
		return nombreCat;
	}

	public void setNombreCat(String nombreCat) {
		this.nombreCat = nombreCat;
	}
	
	public static List<CatalogoVO> getCatalogo(){
		CatalogoNotificacionEnum[] listaEnum = CatalogoNotificacionEnum.values();
		List<CatalogoVO> listaCatVO = new ArrayList<CatalogoVO>(); 
		
		for(CatalogoNotificacionEnum cat : listaEnum)
		{
			CatalogoVO catVO = new CatalogoVO();
			catVO.setId(cat.getIdCat());
			catVO.setNombre(cat.getNombreCat());
			listaCatVO.add(catVO);
		}
		return listaCatVO;
	}
}
