package mx.com.teclo.smm.persistencia.hibernate.dao.comandos;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.smm.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.BitacoraComandoDetalleDTO;
import mx.com.teclo.smm.persistencia.hibernate.dto.comando.BitacoraStatusDTO;

@SuppressWarnings("unchecked")
@Repository("BitacoraComandoStatusDAO")
public class BitacoraComandoStatusDAOImpl extends BaseDAOImpl<BitacoraStatusDTO> implements BitacoraComandoStatusDAO{



	@Override
	@Transactional(readOnly = true)
	public BitacoraStatusDTO obtenerBitacoraStatus(Long idStatus) {
		Criteria query = getCurrentSession().createCriteria(BitacoraStatusDTO.class);
		query.add(Restrictions.eq("idBitacoraStatus", idStatus));
		
		//idEmpresa
		//query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		
		return (BitacoraStatusDTO) query.uniqueResult();		
	}
	
}
