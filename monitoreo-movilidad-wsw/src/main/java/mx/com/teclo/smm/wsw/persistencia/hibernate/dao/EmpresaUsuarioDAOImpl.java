package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.EmpresaUsuarioDTO;

@SuppressWarnings("unchecked")
@Repository("empresaUsuarioDAO")
public class EmpresaUsuarioDAOImpl  extends BaseDAOImpl<EmpresaUsuarioDTO> implements EmpresaUsuarioDAO{



	@Override
	public Long findEmpresaByUsuario(Long idUsuario) {
		Criteria query = getCurrentSession().createCriteria(EmpresaUsuarioDTO.class);
		query.add(Restrictions.eq("idUsuario",idUsuario));
		EmpresaUsuarioDTO empresaUsuarioDTO = (EmpresaUsuarioDTO)query.uniqueResult();
		return empresaUsuarioDTO.getIdEmpresa();
	}

}
