package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.ComandoDTO;


public interface ComandoDAO extends BaseDAO<ComandoDTO>{
	public List<ComandoDTO> obtenerComando();
	public ComandoDTO obtenerComando(Long id);
	public ComandoDTO activarDesactivarObtenerComando(Long id);
	public List<ComandoDTO> activarDesactivarObtenerComando();
	public List<ComandoDTO> obtenerComandosPorTipo(Long idSubTipoDispositivo,Long idTipoMedioComunicacion);
}
