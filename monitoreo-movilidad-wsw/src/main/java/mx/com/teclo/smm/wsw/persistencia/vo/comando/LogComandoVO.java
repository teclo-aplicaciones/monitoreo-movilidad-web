package mx.com.teclo.smm.wsw.persistencia.vo.comando;

public class LogComandoVO {
	private Long idBitacora;
	private String dispositivo;
	private String comando;
	private String logs;
	private String tipoComando;
	private String tipoComunicacion;
	private String fecha;
	private String status;
	private String color;
	private Long idArchivo;
	private String archivo;
	private String gpsActual;

	
	public String getGpsActual() {
		return gpsActual;
	}
	public void setGpsActual(String gpsActual) {
		this.gpsActual = gpsActual;
	}
	public String getArchivo() {
		return archivo;
	}
	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}
	public Long getIdArchivo() {
		return idArchivo;
	}
	public void setIdArchivo(Long idArchivo) {
		this.idArchivo = idArchivo;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public Long getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(Long idBitacora) {
		this.idBitacora = idBitacora;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDispositivo() {
		return dispositivo;
	}
	public void setDispositivo(String dispositivo) {
		this.dispositivo = dispositivo;
	}
	public String getComando() {
		return comando;
	}
	public void setComando(String comando) {
		this.comando = comando;
	}
	public String getLogs() {
		return logs;
	}
	public void setLogs(String logs) {
		this.logs = logs;
	}
	public String getTipoComunicacion() {
		return tipoComunicacion;
	}
	public void setTipoComunicacion(String tipoComunicacion) {
		this.tipoComunicacion = tipoComunicacion;
	}
	

	public String getTipoComando() {
		return tipoComando;
	}
	public void setTipoComando(String tipoComando) {
		this.tipoComando = tipoComando;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	
	
}
