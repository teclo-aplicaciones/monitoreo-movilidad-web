package mx.com.teclo.smm.wsw.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

import mx.com.teclo.arquitectura.ortogonales.seguridad.vo.UsuarioFirmadoVO;
import mx.com.teclo.smm.wsw.negocio.service.empresa.EmpresaService;

@Component
public class IdentificaEmpresaFilter implements Filter {

	@Autowired
	private EmpresaService empresaService;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		SecurityContext sc = SecurityContextHolder.getContext();
		if(sc.getAuthentication() != null){
			AbstractAuthenticationToken aat = (AbstractAuthenticationToken)sc.getAuthentication();
			WebAuthenticationDetails obj =(WebAuthenticationDetails) aat.getDetails();
			if(aat.getPrincipal() instanceof UsuarioFirmadoVO){
				UsuarioFirmadoVO ufVO = (UsuarioFirmadoVO)aat.getPrincipal();
				aat.setDetails(empresaService.buscarEmpresaUsuario(ufVO.getId()));
				SecurityContextHolder.getContext().setAuthentication(aat);
			}
		}
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
	}

}
