package mx.com.teclo.smm.wsw.persistencia.vo.evento;

import java.util.ArrayList;
import java.util.List;



public class EventosTablasHistoricoVO extends EventoVO{

	private static final long serialVersionUID = -3812966413369835965L;
	private List<EventoDispositivoVO> datos;
	
	
	
	public EventosTablasHistoricoVO() {
		super();
		this.datos = new ArrayList<>();
	}

	public List<EventoDispositivoVO> getDatos() {
		return datos;
	}

	public void setDatos(List<EventoDispositivoVO> datos) {
		this.datos = datos;
	}
	
	public void addEventoDispositivoVO(EventoDispositivoVO evento) {
		this.datos.add(evento);
	}

}
