package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.DispositivoComandoDTO;


public interface DispositivoComandoDAO extends BaseDAO<DispositivoComandoDTO>{
	public List<DispositivoComandoDTO> obtenerComandoPorDispositivo(Long idDispositivo);		
	public DispositivoComandoDTO obtenerComandoPorDispositivoYComando(Long idDispositivo,Long idComando);
}
