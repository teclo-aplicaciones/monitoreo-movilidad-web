package mx.com.teclo.smm.wsw.negocio.service.notificacion;

import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.NotificacionDTO;

public interface NotificacionSMSService {
	public Boolean enviaNotificacionSMSInmediata(String nTelefonico, NotificacionDTO notificacion);
}
