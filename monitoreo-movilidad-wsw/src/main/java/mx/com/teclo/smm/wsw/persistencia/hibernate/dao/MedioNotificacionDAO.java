package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.MedioNotificacionDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.TipoNotificacionDTO;

public interface MedioNotificacionDAO extends BaseDAO<MedioNotificacionDTO>{
	public List<MedioNotificacionDTO> buscarCatalogo(TipoNotificacionDTO tnDTO);
	public MedioNotificacionDTO buscarMedioPorId(Long id);
}
