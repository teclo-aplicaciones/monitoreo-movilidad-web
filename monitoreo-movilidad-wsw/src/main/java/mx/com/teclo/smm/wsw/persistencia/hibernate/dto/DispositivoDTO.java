package mx.com.teclo.smm.wsw.persistencia.hibernate.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="TMM011D_DP_DISPOSITIVOS")
public class DispositivoDTO  implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = -3580351234719735364L;

	@Id
	@Column(name="ID_DISPOSITIVO", nullable = false, unique = true)
	private Long idDispositivo;
	@Column(name="ID_EMPRESA")
	private long idEmpresa;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_MODELO")
	private ModeloDispositivoDTO idModelo;
	@Column(name="CD_DISPOSITIVO")
	private String cdDispositivo;
	@Column(name="NB_DISPOSITIVO")
	private String nbDispositivo;
	@Column(name="NB_TABLE_DISP_POSICI")
	private String nbTableDispPosici;
	@Column(name="NB_TABLE_DISP_EVENTO")
	private String nbTableDispEvento;
	@Column(name="NB_TABLE_DISP_COMAND")
	private String nbTableDispComand;
	@Column(name="TX_DISPOSITIVOS")
	private String txDispositivos;
	@Column(name="FB_DISPOSITIVO")
	private String fbDispositivo;
	@Column(name="ST_ACTIVO")
	private Boolean stActivo;
	@Column(name="ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name="FH_CREACION")
	private Date fhCreacion;
	@Column(name="ID_USR_MODIFICA")
	private Long idUsrModifica;
	@Column(name="FH_MODIFICACION")
	private Date fhModificacion;

	@OneToOne(mappedBy = "idDispositivo", fetch = FetchType.LAZY, optional = false)
    private InformacionDispDTO idInformacionDisp;


	public Long getIdDispositivo() {
		return idDispositivo;
	}
	public void setIdDispositivo(Long idDispositivo) {
		this.idDispositivo = idDispositivo;
	}
	public ModeloDispositivoDTO getIdModelo() {
		return idModelo;
	}
	public void setIdModelo(ModeloDispositivoDTO idModelo) {
		this.idModelo = idModelo;
	}
	public String getCdDispositivo() {
		return cdDispositivo;
	}
	public void setCdDispositivo(String cdDispositivo) {
		this.cdDispositivo = cdDispositivo;
	}
	public String getNbDispositivo() {
		return nbDispositivo;
	}
	public void setNbDispositivo(String nbDispositivo) {
		this.nbDispositivo = nbDispositivo;
	}
	public String getNbTableDispPosici() {
		return nbTableDispPosici;
	}
	public void setNbTableDispPosici(String nbTableDispPosici) {
		this.nbTableDispPosici = nbTableDispPosici;
	}
	public String getNbTableDispEvento() {
		return nbTableDispEvento;
	}
	public void setNbTableDispEvento(String nbTableDispEvento) {
		this.nbTableDispEvento = nbTableDispEvento;
	}
	public String getNbTableDispComand() {
		return nbTableDispComand;
	}
	public void setNbTableDispComand(String nbTableDispComand) {
		this.nbTableDispComand = nbTableDispComand;
	}
	public String getTxDispositivos() {
		return txDispositivos;
	}
	public void setTxDispositivos(String txDispositivos) {
		this.txDispositivos = txDispositivos;
	}
	public String getFbDispositivo() {
		return fbDispositivo;
	}
	public void setFbDispositivo(String fbDispositivo) {
		this.fbDispositivo = fbDispositivo;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public InformacionDispDTO getIdInformacionDisp() {
		return idInformacionDisp;
	}
	public void setIdInformacionDisp(InformacionDispDTO idInformacionDisp) {
		this.idInformacionDisp = idInformacionDisp;
	}
	public long getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
}
