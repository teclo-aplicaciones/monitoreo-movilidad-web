package mx.com.teclo.smm.wsw.persistencia.vo;

import java.io.Serializable;

public class UserMod implements Serializable{
	
	private static final long serialVersionUID = -3582409334379156351L;
	private String username;
    private String password;
	
    public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "UserMod [username=" + username + ", password=" + password + "]";
	}
	
	
    
    
}
