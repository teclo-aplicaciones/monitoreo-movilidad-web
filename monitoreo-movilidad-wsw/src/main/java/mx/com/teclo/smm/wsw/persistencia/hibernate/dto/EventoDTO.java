package mx.com.teclo.smm.wsw.persistencia.hibernate.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TMM012C_CT_EVENTOS")
public class EventoDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6362693026483934358L;
	@Id
	@Column(name = "ID_EVENTO")
	private Long idEvento;
	@ManyToOne
    @JoinColumn(name="ID_TIPO_EVENTO", nullable=false)
	private TipoEventoDTO idTipoEvento;
	@ManyToOne
    @JoinColumn(name="ID_SUBTIPO_DISPOSITIVO", nullable=false)
	private SubtipoDispDTO idSubtipoDispositivo;
	@Column(name = "CD_EVENTO")
	private String cdEvento;
	@Column(name = "NB_EVENTO")
	private String nbEvento;
	@Column(name = "TX_EVENTO")
	private String txEvento;
	
	@Column(name = "ST_ACTIVO")
	private Boolean stActivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;
	@Column(name = "TX_PARAMETRO")
	private String txParametro;
	
	public String getTxParametro() {
		return txParametro;
	}
	public void setTxParametro(String txParametro) {
		this.txParametro = txParametro;
	}
	public Long getIdEvento() {
		return idEvento;
	}
	public void setIdEvento(Long idEvento) {
		this.idEvento = idEvento;
	}
	public TipoEventoDTO getIdTipoEvento() {
		return idTipoEvento;
	}
	public void setIdTipoEvento(TipoEventoDTO idTipoEvento) {
		this.idTipoEvento = idTipoEvento;
	}
	public SubtipoDispDTO getIdSubtipoDispositivo() {
		return idSubtipoDispositivo;
	}
	public void setIdSubtipoDispositivo(SubtipoDispDTO idSubtipoDispositivo) {
		this.idSubtipoDispositivo = idSubtipoDispositivo;
	}
	public String getCdEvento() {
		return cdEvento;
	}
	public void setCdEvento(String cdEvento) {
		this.cdEvento = cdEvento;
	}
	public String getNbEvento() {
		return nbEvento;
	}
	public void setNbEvento(String nbEvento) {
		this.nbEvento = nbEvento;
	}
	public String getTxEvento() {
		return txEvento;
	}
	public void setTxEvento(String txEvento) {
		this.txEvento = txEvento;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}

	
}
