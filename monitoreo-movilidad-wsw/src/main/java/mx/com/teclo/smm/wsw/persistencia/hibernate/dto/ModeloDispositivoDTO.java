package mx.com.teclo.smm.wsw.persistencia.hibernate.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TMM005C_CT_MODELOS")
public class ModeloDispositivoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1503137120804185830L;

	@Id
	@GeneratedValue(generator="id_modelo_generator",strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="id_modelo_generator", sequenceName = "SQTMM005C_CT_MODELOS",allocationSize=1)
	@Column(name = "ID_MODELO", nullable = false, unique = true)	
	private Long idModelo;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_MARCA")
	private MarcaDispositivoDTO idMarca;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_SUBTIPO_DISPOSITIVO")
	private SubTipoDispositivoDTO idSubtipoDispositivo;
	@Column(name = "CD_MODELO")
	private String cdModelo;
	@Column(name = "NB_MODELO")
	private String nbModelo;
	@Column(name = "TX_MODELO")
	private String txModelo;
	@Column(name = "ST_ACTIVO")
	private Boolean stActivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;
	
	public Long getIdModelo() {
		return idModelo;
	}
	public void setIdModelo(Long idModelo) {
		this.idModelo = idModelo;
	}
	public MarcaDispositivoDTO getIdMarca() {
		return idMarca;
	}
	public void setIdMarca(MarcaDispositivoDTO idMarca) {
		this.idMarca = idMarca;
	}
	public SubTipoDispositivoDTO getIdSubtipoDispositivo() {
		return idSubtipoDispositivo;
	}
	public void setIdSubtipoDispositivo(SubTipoDispositivoDTO idSubtipoDispositivo) {
		this.idSubtipoDispositivo = idSubtipoDispositivo;
	}
	public String getCdModelo() {
		return cdModelo;
	}
	public void setCdModelo(String cdModelo) {
		this.cdModelo = cdModelo;
	}
	public String getNbModelo() {
		return nbModelo;
	}
	public void setNbModelo(String nbModelo) {
		this.nbModelo = nbModelo;
	}
	public String getTxModelo() {
		return txModelo;
	}
	public void setTxModelo(String txModelo) {
		this.txModelo = txModelo;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
}
