package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.DispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.InformacionDispDTO;

@Repository("informacionDispDAO")
public class InformacionDispDAOImpl extends BaseDAOImpl<InformacionDispDTO> implements InformacionDispDAO {

	@Override
	public InformacionDispDTO buscarRelacionInformacionDispositivo(DispositivoDTO dDTO){
		Criteria query = getCurrentSession().createCriteria(InformacionDispDTO.class);
		query.add(Restrictions.eq("idDispositivo", dDTO));	
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return (InformacionDispDTO) query.uniqueResult();
	}
	
	@Override
	public InformacionDispDTO buscarRelacionPorDispositivo(DispositivoDTO dispDTO){
		Criteria query = getCurrentSession().createCriteria(InformacionDispDTO.class);
		query.add(Restrictions.eq("idDispositivo", dispDTO));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		
		return (InformacionDispDTO) query.uniqueResult();
	} 
}
