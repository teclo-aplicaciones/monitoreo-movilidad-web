package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.GrupoDTO;

@SuppressWarnings("unchecked")
@Repository("grupoDAO")
public class GrupoDAOImpl extends BaseDAOImpl<GrupoDTO> implements GrupoDAO {

	@Override
	public Long buscarSiguienteValor(){
		SQLQuery query = getCurrentSession().createSQLQuery("SELECT SQTMM020D_CT_GRUPOS.NEXTVAL FROM DUAL");

		BigDecimal res = (BigDecimal)query.uniqueResult();

		return res.longValueExact();
	}

	@Override
	public List<GrupoDTO> buscarGrupos() {
		Criteria query = getCurrentSession().createCriteria(GrupoDTO.class);
		query.createAlias("idTipoGrupo", "idTipoGrupo");
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		query.addOrder(Order.asc("idTipoGrupo"));

		//idEmpresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		query.add(Restrictions.eq("idTipoGrupo.idEmpresa", getIdEmpresa()));

		return query.list();
	}

	@Override
	public List<GrupoDTO> buscarGruposPorNombre(String nombre){
		Criteria query = getCurrentSession().createCriteria(GrupoDTO.class);
		nombre = nombre.replaceAll("%", "");
		query.add(Restrictions.like("nbGrupo", nombre, MatchMode.ANYWHERE).ignoreCase());
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));

		//idEmpresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		query.add(Restrictions.eq("idTipoGrupo.idEmpresa", getIdEmpresa()));

		return query.list();
	}

	@Override
	public List<GrupoDTO> activarDesactivarBuscarGrupos() {
		Criteria query = getCurrentSession().createCriteria(GrupoDTO.class);
		query.addOrder(Order.asc("idTipoGrupo"));

		//idEmpresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));

		return query.list();
	}

	@Override
	public GrupoDTO buscarGrupoPorId(Long id) {
		Criteria query = getCurrentSession().createCriteria(GrupoDTO.class);
		query.createAlias("idTipoGrupo", "idTipoGrupo");
		query.add(Restrictions.eq("idGrupo", id));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));

		//idEmpresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		query.add(Restrictions.eq("idTipoGrupo.idEmpresa", getIdEmpresa()));

		return (GrupoDTO) query.uniqueResult();
	}

	@Override
	public GrupoDTO activarDesactivarBuscarGrupoPorId(Long id) {
		Criteria query = getCurrentSession().createCriteria(GrupoDTO.class);
		query.createAlias("idTipoGrupo", "idTipoGrupo");
		query.add(Restrictions.eq("idGrupo", id));

		//idEmpresa
		query.add(Restrictions.eq("idEmpresa", getIdEmpresa()));
		query.add(Restrictions.eq("idTipoGrupo.idEmpresa", getIdEmpresa()));

		return (GrupoDTO) query.uniqueResult();
	}
}
