package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.MedioNotificacionDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.NotificacionDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.TipoNotificacionDTO;

@SuppressWarnings("unchecked")
@Repository("notificacionDAO")
public class NotificacionDAOImpl extends BaseDAOImpl<NotificacionDTO> implements NotificacionDAO{

	@Autowired
	MedioNotificacionDAO medioNotificacionDAO;
	
	@Override
	public Long buscarSiguienteIdentificador(){
		SQLQuery query = getCurrentSession().createSQLQuery("SELECT SQTMM023D_CT_NOTIFI.nextval FROM DUAL");
		BigDecimal bd = (BigDecimal)query.uniqueResult();
		
		return (Long)bd.longValueExact();
	}
	
	@Override
	public NotificacionDTO buscarNotificacionPorId(Long id){
		Criteria query = getCurrentSession().createCriteria(NotificacionDTO.class);
		query.add(Restrictions.eq("idNotificacion", id));
		//query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		
		return (NotificacionDTO)query.uniqueResult();
	}
	
	@Override
	public List<NotificacionDTO> buscarNotificacionPorNombre(String nombre, TipoNotificacionDTO tNDTO, MedioNotificacionDTO mnDTO){
		Criteria query = getCurrentSession().createCriteria(NotificacionDTO.class);
		nombre = nombre.replaceAll("%", "");
		query.createAlias("idMedioTipoNotifi", "idMedioTipoNotifi");
		query.createAlias("idMedioTipoNotifi.idTipoNotificacion", "idTipoNotificacion");
		query.add(Restrictions.like("nbNotificacion", nombre, MatchMode.ANYWHERE).ignoreCase());
		query.add(Restrictions.eq("idTipoNotificacion.idTipoNotificacion", tNDTO.getIdTipoNotificacion()));
		if(mnDTO != null)
			query.add(Restrictions.eq("idMedioTipoNotifi.idMedioTipoNotifi", mnDTO.getIdMedioTipoNotifi()));
		
		return (List<NotificacionDTO>) query.list();
	}
}
