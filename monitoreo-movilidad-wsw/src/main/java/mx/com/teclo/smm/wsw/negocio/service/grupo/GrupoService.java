package mx.com.teclo.smm.wsw.negocio.service.grupo;

import java.util.List;

import mx.com.teclo.arquitectura.ortogonales.exception.NotFoundException;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.GrupoDTO;
import mx.com.teclo.smm.wsw.persistencia.vo.catalogo.CatalogoConSubtipoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.catalogo.CatalogoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.grupo.ConsultaGrupoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.grupo.GrupoVO;

public interface GrupoService {
	public List<CatalogoVO> buscarGrupos();
	public List<CatalogoConSubtipoVO> buscarGruposConIdSubtipoDispositivo();
	public List<ConsultaGrupoVO> consultaGrupos(Long tipo, String valor) throws NotFoundException;
	public GrupoDTO buscarGrupoPorId(Long id);
	public GrupoVO buscarGrupoParaActualizar(Long id);
	public Long[] obtenerDispositivosEnGrupo(Long id);
	public GrupoDTO guardarGrupo(GrupoVO grupoVO);
	public Boolean guardarGrupoAsociado(GrupoDTO grupoDTO,GrupoVO grupoVO);
	public Boolean actualizarGrupo(GrupoVO grupoVO);
	public Boolean eliminarGrupo(Long idGrupo);
}
