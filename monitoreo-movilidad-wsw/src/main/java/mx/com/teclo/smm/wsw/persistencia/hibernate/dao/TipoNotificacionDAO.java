package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.TipoNotificacionDTO;

public interface TipoNotificacionDAO extends BaseDAO<TipoNotificacionDTO> {
	public TipoNotificacionDTO buscarTipoNotificacionPorId(Long id);
}
