package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.DispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.PosicionActualDTO;

public interface PosicionActualDAO extends BaseDAO<PosicionActualDTO>{
	public List<PosicionActualDTO> obtenerCoordenadaActual(DispositivoDTO dispoDTO);
	public List<PosicionActualDTO> obtenerRegistrosHoy();
	public PosicionActualDTO obtenerRegistroHoyPorDispositivo(Long idDispo);
}
