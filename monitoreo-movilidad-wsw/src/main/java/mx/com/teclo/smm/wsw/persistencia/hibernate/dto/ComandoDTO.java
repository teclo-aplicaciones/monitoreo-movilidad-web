package mx.com.teclo.smm.wsw.persistencia.hibernate.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TMM025D_CT_COMANDOS")
public class ComandoDTO implements Serializable {


	/**
	 *
	 */
	private static final long serialVersionUID = -2253404711211356028L;

	@Id
	@GeneratedValue(generator="id_cmd_generator",strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="id_cmd_generator", sequenceName = "SQTMM025D_CT_COMANDOS",allocationSize=1)
	@Column(name = "ID_COMANDO")
	private Long idComando;
	@Column(name="ID_EMPRESA")
	private long idEmpresa;
	@ManyToOne
	@JoinColumn(name = "ID_TIPO_COMANDO")
	private TipoComandoDTO idTipoComando;
	@ManyToOne
	@JoinColumn(name = "ID_MEDIO_SUBTIPO_COMANDO")
	private MedioSubtipoCmdDTO idMedioSubtipoCmdDTO;
	@Column(name = "CD_COMANDO")
	private String cdComando;
	@Column(name = "ST_ACTIVO")
	private Boolean stActivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;
	public Long getIdComando() {
		return idComando;
	}
	public void setIdComando(Long idComando) {
		this.idComando = idComando;
	}
	public TipoComandoDTO getIdTipoComando() {
		return idTipoComando;
	}
	public void setIdTipoComando(TipoComandoDTO idTipoComando) {
		this.idTipoComando = idTipoComando;
	}
	public MedioSubtipoCmdDTO getIdMedioSubtipoCmdDTO() {
		return idMedioSubtipoCmdDTO;
	}
	public void setIdMedioSubtipoCmdDTO(MedioSubtipoCmdDTO idMedioSubtipoCmdDTO) {
		this.idMedioSubtipoCmdDTO = idMedioSubtipoCmdDTO;
	}
	public String getCdComando() {
		return cdComando;
	}
	public void setCdComando(String cdComando) {
		this.cdComando = cdComando;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}

	public long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

}
