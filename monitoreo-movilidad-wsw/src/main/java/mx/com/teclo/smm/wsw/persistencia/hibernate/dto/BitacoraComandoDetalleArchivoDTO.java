package mx.com.teclo.smm.wsw.persistencia.hibernate.dto;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author sinuhe
 *
 */
@Entity
@Table(name = "TMM070B_CM_ARCHIVO")
public class BitacoraComandoDetalleArchivoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "id_archivo_generator", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "id_archivo_generator", sequenceName = "SQTMM070B_CM_ARCHIVO", allocationSize = 1)
	@Column(name = "ID_ARCHIVO")
	private Long idArchivo;
	@ManyToOne()
	@JoinColumn(name = "ID_BITACORA")
	private BitacoraComandoDTO bitacoraComandoDTO;
	@Column(name = "TX_TIPO_ARCHIVO")
	private String txTipoArchivo;
	@Column(name = "TX_FORMATO_ARCHIVO")
	private String txFormatoArchivo;
	@Column(name = "NB_ARCHIVO")
	private String nombreArchivo;
	@Column(name = "ARCHIVO")
	private byte[] archivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;

	public Long getIdArchivo() {
		return idArchivo;
	}

	public void setIdArchivo(Long idArchivo) {
		this.idArchivo = idArchivo;
	}

	public BitacoraComandoDTO getBitacoraComandoDTO() {
		return bitacoraComandoDTO;
	}

	public void setBitacoraComandoDTO(BitacoraComandoDTO bitacoraComandoDTO) {
		this.bitacoraComandoDTO = bitacoraComandoDTO;
	}

	public String getTxTipoArchivo() {
		return txTipoArchivo;
	}

	public void setTxTipoArchivo(String txTipoArchivo) {
		this.txTipoArchivo = txTipoArchivo;
	}

	public String getTxFormatoArchivo() {
		return txFormatoArchivo;
	}

	public void setTxFormatoArchivo(String txFormatoArchivo) {
		this.txFormatoArchivo = txFormatoArchivo;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public byte[] getArchivo() {
		return archivo;
	}

	public void setArchivo(byte[] archivo) {
		this.archivo = archivo;
	}

	public Date getFhCreacion() {
		return fhCreacion;
	}

	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}

}
