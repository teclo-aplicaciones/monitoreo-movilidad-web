package mx.com.teclo.smm.wsw.rest.evento;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import mx.com.teclo.arquitectura.ortogonales.responsehttp.ConflictHttpResponse;
import mx.com.teclo.arquitectura.ortogonales.responsehttp.OKHttpResponse;
import mx.com.teclo.smm.wsw.negocio.service.evento.EventoService;
import mx.com.teclo.smm.wsw.persistencia.vo.EventoVO;

@RestController
public class EventoRestController {

	@Autowired 
	private EventoService eventoService;
	
	@RequestMapping(value="/eventos", method=RequestMethod.GET)
	public ResponseEntity<List<EventoVO>> eventos() throws OKHttpResponse, ConflictHttpResponse{
		
		List<EventoVO> eventoVO = eventoService.consultaEventos();
//		System.out.println(" : " + eventoVO.size());
		if(eventoVO.size() > 0)
			throw new OKHttpResponse("Consulta de registro evento.","TCL20001", eventoVO);
		else
			throw new ConflictHttpResponse("No se encontraron registros.","TCL40901", eventoVO);
	}

}
