package mx.com.teclo.smm.wsw.negocio.service.empleado;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.EmpleadoDTO;
import mx.com.teclo.smm.wsw.persistencia.vo.catalogo.CatalogoConSubtipoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.catalogo.CatalogoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.empleado.EmpleadoVO;

public interface EmpleadoService {
	public List<CatalogoVO> buscarEmpleado();
	public List<CatalogoConSubtipoVO> buscarEmpleadoConIdSubtipoDispositivo();
	public EmpleadoDTO buscarEmpleado(Long id);
	public List<EmpleadoVO> buscarEmpleado(String param, String value);
	public Long[] obtenerDispositivosEnUsuario(Long id);

}
