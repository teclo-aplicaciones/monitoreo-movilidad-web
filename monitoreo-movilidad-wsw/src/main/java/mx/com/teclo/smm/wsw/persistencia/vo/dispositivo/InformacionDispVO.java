package mx.com.teclo.smm.wsw.persistencia.vo.dispositivo;

import java.io.Serializable;

public class InformacionDispVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long idInformacionDisp;
	private Long nuImei;
	private String nuSerie;
	private String nuIp;
	private String nuCelularEmpresa;
	private String nuCelularPersonal;
	private Boolean stActivo;
	
	public Long getIdInformacionDisp() {
		return idInformacionDisp;
	}
	public void setIdInformacionDisp(Long idInformacionDisp) {
		this.idInformacionDisp = idInformacionDisp;
	}
	public Long getNuImei() {
		return nuImei;
	}
	public void setNuImei(Long nuImei) {
		this.nuImei = nuImei;
	}
	public String getNuSerie() {
		return nuSerie;
	}
	public void setNuSerie(String nuSerie) {
		this.nuSerie = nuSerie;
	}
	public String getNuIp() {
		return nuIp;
	}
	public void setNuIp(String nuIp) {
		this.nuIp = nuIp;
	}
	public String getNuCelularEmpresa() {
		return nuCelularEmpresa;
	}
	public void setNuCelularEmpresa(String nuCelularEmpresa) {
		this.nuCelularEmpresa = nuCelularEmpresa;
	}
	public String getNuCelularPersonal() {
		return nuCelularPersonal;
	}
	public void setNuCelularPersonal(String nuCelularPersonal) {
		this.nuCelularPersonal = nuCelularPersonal;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	
	
}
