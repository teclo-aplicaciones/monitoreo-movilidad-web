package mx.com.teclo.smm.wsw.negocio.service.catalogo;

import java.util.List;

import org.springframework.stereotype.Service;

import mx.com.teclo.smm.wsw.persistencia.vo.catalogo.CatalogoVO;
import mx.com.teclo.smm.wsw.util.enumerable.CatalogoMonitoreoEnum;

@Service
public class CatalogoServiceImpl implements CatalogoService {

	@Override
	public List<CatalogoVO> catalogoMonitoreo() {
		return CatalogoMonitoreoEnum.getCatalogo();
	}

}
