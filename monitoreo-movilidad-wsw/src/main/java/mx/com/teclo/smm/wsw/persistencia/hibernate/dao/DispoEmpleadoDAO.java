package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.DispoEmpleadoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.DispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.EmpleadoDTO;

public interface DispoEmpleadoDAO extends BaseDAO<DispoEmpleadoDTO>{
	public DispoEmpleadoDTO buscarRelacionDispositivoEmpleado(DispositivoDTO dDTO);
	public List<DispoEmpleadoDTO> buscarRelacionDispoEmpleado();
	public List<EmpleadoDTO> buscarRelacionDispoEmpleadoUnicos();
	public DispoEmpleadoDTO buscarRelacionDispoEmpleadoPorDispositivo(DispositivoDTO dispoDTO);
	public List<DispoEmpleadoDTO> buscarRelacionDispoEmpleadoPorEmpleado(EmpleadoDTO empleadoDTO);
	public DispoEmpleadoDTO buscarRelacionIdDispoEmpleadoPorEmpleado(Long idEmpleadoDTO);
	public List<DispoEmpleadoDTO> buscarRelacionIdDisposEmpleadoPorEmpleado(Long idEmpleadoDTO);
}
