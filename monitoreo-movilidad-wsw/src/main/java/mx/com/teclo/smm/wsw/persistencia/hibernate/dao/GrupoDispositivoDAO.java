package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.DispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.GrupoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.GrupoDispositivoDTO;

public interface GrupoDispositivoDAO extends BaseDAO<GrupoDispositivoDTO> {
	public Long buscarSiguienteValor();
	public List<GrupoDispositivoDTO> buscarTodoDispositivosEnGrupos();
	public List<GrupoDispositivoDTO> buscarDispositivosEnGrupo(GrupoDTO grupo);
	public List<DispositivoDTO> buscarDispositivosPorGrupoId(Long idGrupo);
	public GrupoDispositivoDTO buscarGrupoDeDispositivo(DispositivoDTO idDispositivo);
	public List<GrupoDispositivoDTO> buscarGruposPorDispositivo(Long idDispositivo);
	public List<DispositivoDTO> buscarDispositivosSinGrupo();
	public Boolean eliminarRelacionGrupo(Long idGrupo);
}
