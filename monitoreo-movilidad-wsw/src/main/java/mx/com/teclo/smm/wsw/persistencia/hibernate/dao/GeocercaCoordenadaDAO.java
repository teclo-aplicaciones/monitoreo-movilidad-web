package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.GeocercaCoordenadaDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.GeocercaDTO;

public interface GeocercaCoordenadaDAO extends BaseDAO<GeocercaCoordenadaDTO>{
	public Long buscarSiguenteValor();
	public List<GeocercaCoordenadaDTO> buscarRelacionGeocercaCoordenadas(GeocercaDTO gDTO);
	public Boolean eliminarRelacionGeocercaCoordenada(GeocercaDTO gDTO);
}
