package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.BitacoraComandoDetalleDTO;

@SuppressWarnings("unchecked")
@Repository("BitacoraComandoDetalleDAO")
public class BitacoraComandoDetalleDAOImpl extends BaseDAOImpl<BitacoraComandoDetalleDTO> implements BitacoraComandoDetalleDAO{

	
	@Override
	@Transactional(readOnly = true)
	public List<BitacoraComandoDetalleDTO> obtenerBitacoraComandoDetalle(Long idBitacora){
		Criteria query = getCurrentSession().createCriteria(BitacoraComandoDetalleDTO.class);
		query.add(Restrictions.eq("idBitacora", idBitacora));	
		query.addOrder(Order.asc("idBitacoraDetalle"));
		return (List<BitacoraComandoDetalleDTO>)query.list();
	}
	
}
