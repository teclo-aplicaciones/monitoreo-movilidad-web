package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.EmpresaUsuarioDTO;

public interface EmpresaUsuarioDAO extends BaseDAO<EmpresaUsuarioDTO>{
	public Long findEmpresaByUsuario(Long idUsuario);
}
