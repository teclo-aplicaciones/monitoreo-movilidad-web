package mx.com.teclo.smm.wsw.persistencia.vo.grupo;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.vo.geocerca.DispoGeocercaVO;

public class ConsultaGrupoVO {
	private Long id;
	private String nombre;
	private String tipo;
	private String descripcion;
	private Long fechaCreacion;
	private List<DispoGeocercaVO> dispositivos;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Long getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Long fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public List<DispoGeocercaVO> getDispositivos() {
		return dispositivos;
	}
	public void setDispositivos(List<DispoGeocercaVO> dispositivos) {
		this.dispositivos = dispositivos;
	}
}
