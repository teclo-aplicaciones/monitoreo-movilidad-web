package mx.com.teclo.smm.wsw.util.enumerable;

public enum ComandoEnum {

	ALERTA_ON("ALERTA_ON","sound_on"),
	ALERTA_OFF("ALERTA_OFF","sound_off"),
	WIFI_ON("WIFI_ON","wifi_on"),
	WIFI_OFF("WIFI_OFF","wifi_off"),	
	GPS_ACTUAL("GPS_ACTUAL","gps_actual"),
	VIBRAR("VIBRAR","vibrate"),
	CAPTURA_PANTALLA("CAPTURA_PANTALLA","screenshot"),
	TOMAR_FOTO("TOMAR_FOTO","take_rear_picture"),
	TOMAR_FOTO_DELANTERA("TOMAR_FOTO_DELANTERA","take_front_picture"),
	TOMAR_VIDEO("TOMAR_VIDEO_TRASERA","take_rear_video"),
	TOMAR_VIDEO_DELANTERA("TOMAR_VIDEO_DELANTERA","take_front_video"),
	GRABAR_AUDIO("GRABAR_AUDIO","audio_recording");
	
	private String nombre;
	private String action;
	
	private ComandoEnum(String nombre,String action) {
		this.nombre= nombre;
		this.action= action;
	}

	public String getNombre() {
		return nombre;
	}

	public String getAction() {
		return action;
	}

		
}
