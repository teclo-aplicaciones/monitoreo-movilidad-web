package mx.com.teclo.smm.wsw.negocio.service.dispositivo;

import java.util.List;
import java.util.Map;

import mx.com.teclo.arquitectura.ortogonales.exception.NotFoundException;
import mx.com.teclo.smm.wsw.persistencia.vo.catalogo.CatalogoConSubtipoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.catalogo.CatalogoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.dispositivo.DispositivoGuardarVO;
import mx.com.teclo.smm.wsw.persistencia.vo.dispositivo.DispositivoHistoricoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.dispositivo.DispositivoSubtipoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.dispositivo.DispositivoTableVO;
import mx.com.teclo.smm.wsw.persistencia.vo.dispositivo.DispositivoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.dispositivo.InfoDispositivoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.geocerca.DispositivoEnGeocercaVO;
import mx.com.teclo.smm.wsw.persistencia.vo.monitoreo.ConfigMonitoreoDispoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.monitoreo.DispositivoRTDetalleVO;

public interface DispositivoService {

	public DispositivoVO updateTokenFirebaseDispositivo(Long nuImei,String tokenFirebase);
	
	List<CatalogoVO> buscarDispositivo(Long[] ids);
	List<CatalogoConSubtipoVO> buscarDispositivoConIdSubtipoDispositivo(Long[] ids);	
	public List<DispositivoVO> buscarDispositivos(Long[] ids)throws NotFoundException;
	public List<DispositivoEnGeocercaVO> buscarDispositivoParaGeocerca(Long[] ids);
	public DispositivoHistoricoVO buscarRutaDispositivoPorId(String id, Long tipoBusq, String fechaInicio, String fechaFin, Boolean filterByCero);
	public DispositivoGuardarVO guardarDispositivo(DispositivoGuardarVO dgVO, Long userId);
	public DispositivoGuardarVO actualizarDispositivo(DispositivoGuardarVO dgVO, Long userId);
	public DispositivoRTDetalleVO cargarDetalleDispositivo(Long id);
	public List<InfoDispositivoVO> infoDispositivos();
	public List<InfoDispositivoVO> infoDispositivosDiarios();
	public Map generarReporteRutaHistorico(DispositivoHistoricoVO dispHistorico, String fechaInicio, String fechaFin);
	public Boolean crearTablasDispositivo(Long idDispositivo);
	public List<DispositivoTableVO> buscarDispositivosPorTipo(String tipo, String valor);
	public List<DispositivoSubtipoVO> buscarDispositivos();
	public DispositivoGuardarVO buscarDispositivoPorId(Long id);
	public Boolean activarDesactivarDispositivo(Long id,boolean activar);	
	public Boolean eliminarDispositivo(Long id);
	public List<CatalogoConSubtipoVO> buscarDispositivoConSubTipoDispositivo(Long[] ids);
	public List<ConfigMonitoreoDispoVO> buscarParamConfigMonitoreo(Long imei);

}
