package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.MarcaDispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.ModeloDispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.SubTipoDispositivoDTO;

public interface ModeloDispositivoDAO extends BaseDAO<ModeloDispositivoDTO>{

	public List<ModeloDispositivoDTO> buscarCatalogoPorTipo(MarcaDispositivoDTO mdDTO, SubTipoDispositivoDTO stdDTO);
	public List<ModeloDispositivoDTO> buscarCatalogoPorSubTipo(Long subTipoDispositivo );
	public List<ModeloDispositivoDTO> catalogoPorMarca(Long idMarca );
	public List<ModeloDispositivoDTO> obtenerModelos();
	public ModeloDispositivoDTO obtenerModelosId(long id);
	public ModeloDispositivoDTO activarDesactivarObtenerModelosId(long id);
	public List<ModeloDispositivoDTO> activarDesactivarObtenerModelos();
}
