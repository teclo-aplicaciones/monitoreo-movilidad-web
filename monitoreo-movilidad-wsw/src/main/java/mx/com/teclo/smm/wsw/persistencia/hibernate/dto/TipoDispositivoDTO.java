package mx.com.teclo.smm.wsw.persistencia.hibernate.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TMM002C_CT_TIPO_DISPOSITIVOS")
public class TipoDispositivoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4210517542223069927L;
	
	@Id
	@GeneratedValue(generator="id_tipodispositivo_generator",strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="id_tipodispositivo_generator", sequenceName = "SQTMM002C_CT_TIPO_DIS",allocationSize=1)
	@Column(name = "ID_TIPO_DISPOSITIVO", nullable = false, unique = true)
	private Long idTipoDispositivo;
	@Column(name = "CD_TIPO_DISPOSITIVO")
	private String cdTipoDispositivo;
	@Column(name = "NB_TIPO_DISPOSITIVO")
	private String nbTipoDispositivo;
	@Column(name = "TX_TIPO_DISPOSITIVO")
	private String txTipoDispositivo;
	@Column(name = "IMG_TIPO_DISPOSITIVO")
	private String imgTipoDispositivo;
	@Column(name = "ST_ACTIVO")
	private Boolean stActivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;
	
	public Long getIdTipoDispositivo() {
		return idTipoDispositivo;
	}
	public void setIdTipoDispositivo(Long idTipoDispositivo) {
		this.idTipoDispositivo = idTipoDispositivo;
	}
	public String getCdTipoDispositivo() {
		return cdTipoDispositivo;
	}
	public void setCdTipoDispositivo(String cdTipoDispositivo) {
		this.cdTipoDispositivo = cdTipoDispositivo;
	}
	public String getNbTipoDispositivo() {
		return nbTipoDispositivo;
	}
	public void setNbTipoDispositivo(String nbTipoDispositivo) {
		this.nbTipoDispositivo = nbTipoDispositivo;
	}
	public String getTxTipoDispositivo() {
		return txTipoDispositivo;
	}
	public void setTxTipoDispositivo(String txTipoDispositivo) {
		this.txTipoDispositivo = txTipoDispositivo;
	}
	public String getImgTipoDispositivo() {
		return imgTipoDispositivo;
	}
	public void setImgTipoDispositivo(String imgTipoDispositivo) {
		this.imgTipoDispositivo = imgTipoDispositivo;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
}
