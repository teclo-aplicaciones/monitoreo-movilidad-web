package mx.com.teclo.smm.wsw.util.enumerable;

public enum ConfigGeoNotificaEnum {
	FUERA(1L), DENTRO(2L), METROS100(3L);
	
	private Long id;
	
	private ConfigGeoNotificaEnum(Long id){
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
