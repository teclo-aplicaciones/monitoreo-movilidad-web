package mx.com.teclo.smm.wsw.persistencia.vo.dispositivo;

import java.io.Serializable;
import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.vo.empleado.InfoEmpleadoVO;

public class DispositivoVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String cdDispositivo;
	private String nbDsipositivo;
	private String txDispositivos;
	private String fbDispositivo;
	private Boolean stActivo;
	private Long id;
	private List<Double[]> coords;
	private InfoEmpleadoVO info;

	private InformacionDispVO idInformacionDisp;

	public String getCdDispositivo() {
		return cdDispositivo;
	}

	public void setCdDispositivo(String cdDispositivo) {
		this.cdDispositivo = cdDispositivo;
	}

	public String getNbDsipositivo() {
		return nbDsipositivo;
	}

	public void setNbDsipositivo(String nbDsipositivo) {
		this.nbDsipositivo = nbDsipositivo;
	}

	public String getTxDispositivos() {
		return txDispositivos;
	}

	public void setTxDispositivos(String txDispositivos) {
		this.txDispositivos = txDispositivos;
	}

	public String getFbDispositivo() {
		return fbDispositivo;
	}

	public void setFbDispositivo(String fbDispositivo) {
		this.fbDispositivo = fbDispositivo;
	}

	public Boolean getStActivo() {
		return stActivo;
	}

	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}

	public InformacionDispVO getIdInformacionDisp() {
		return idInformacionDisp;
	}

	public void setIdInformacionDisp(InformacionDispVO idInformacionDisp) {
		this.idInformacionDisp = idInformacionDisp;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Double[]> getCoords() {
		return coords;
	}

	public void setCoords(List<Double[]> coords) {
		this.coords = coords;
	}

	public InfoEmpleadoVO getInfo() {
		return info;
	}

	public void setInfo(InfoEmpleadoVO info) {
		this.info = info;
	}
}
