package mx.com.teclo.smm.wsw.negocio.service.notificacion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.teclo.notificacionespush.mail.service.NotificationMailService;
import mx.com.teclo.notificacionespush.mail.vo.NotificacionVelocity;
import mx.com.teclo.notificacionespush.mail.vo.NotificationMail;
import mx.com.teclo.notificacionespush.mail.vo.ResponseNotificationMail;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.NotificacionDTO;

@Service
public class NotificacionMailServiceImpl implements NotificacionMailService{

private static final String CHARSET_UTF8 = "UTF-8";
	
	@Autowired
	NotificationMailService notificationMailService;
	
	@Override
	@SuppressWarnings("finally")
	public Boolean enviaNotificacioMailInmediata(String nCorreo, NotificacionDTO notificacion){
		ResponseNotificationMail rnm = new ResponseNotificationMail();
		Map<String, Object> attrMail = new HashMap<String, Object>();
		ClassLoader ctxLoader = getClass().getClassLoader();
		
		List<String> listaCorreo = new ArrayList<String>();
		listaCorreo.add(nCorreo);
		
		/*File req1 = new File(ctxLoader.getResource("files/firebase_example.json").getFile());
		File req2 = new File(ctxLoader.getResource("templates/velocity/isotipo.png").getFile());
		
		List<File> tmplMail = new ArrayList<File>();
		tmplMail.add(req1);
		tmplMail.add(req2);*/
		
		NotificationMail notMail = new NotificationMail("teclo.desarrollo.movil@gmail.com","[M&M] Notificación", listaCorreo, new ArrayList<String>(), null, notificacion.getNbNotificacion(), notificacion.getTxMensaje(), null, null);
		
		attrMail.put("titulo", "Notificación de Monitoreo & Movilidad");
		attrMail.put("body", notificacion.getTxMensaje());
		attrMail.put("nombreEmpresa", "2018. Teclo Mexicana S.A de C.V.");
		attrMail.put("aplicacion", "Monitoreo & Movilidad");
		
		NotificacionVelocity notiVelocity = new NotificacionVelocity(attrMail,
				"notificaction-localizacion.vm", "Velocity Template", CHARSET_UTF8);
		try{
			rnm = notificationMailService.sendVelocityMail(notMail, notiVelocity);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			return rnm != null ? rnm.getStatus().equals("ok") ? Boolean.FALSE : Boolean.TRUE : Boolean.TRUE;
		}
	}
}
