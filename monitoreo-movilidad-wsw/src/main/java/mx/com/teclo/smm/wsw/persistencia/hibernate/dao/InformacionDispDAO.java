package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.DispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.InformacionDispDTO;

public interface InformacionDispDAO extends BaseDAO<InformacionDispDTO>{
	public InformacionDispDTO buscarRelacionInformacionDispositivo(DispositivoDTO dDTO);
	public InformacionDispDTO buscarRelacionPorDispositivo(DispositivoDTO dispDTO);
}
