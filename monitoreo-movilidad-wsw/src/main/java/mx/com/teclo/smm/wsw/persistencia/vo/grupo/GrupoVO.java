package mx.com.teclo.smm.wsw.persistencia.vo.grupo;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.vo.geocerca.DispoGeocercaVO;

public class GrupoVO {
	private Long id;
	private String nombre;
	private Long tipo;
	private String descripcion;
	private List<DispoGeocercaVO> dispositivos;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Long getTipo() {
		return tipo;
	}
	public void setTipo(Long tipo) {
		this.tipo = tipo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public List<DispoGeocercaVO> getDispositivos() {
		return dispositivos;
	}
	public void setDispositivos(List<DispoGeocercaVO> dispositivos) {
		this.dispositivos = dispositivos;
	}
}
