package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.ComandoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.MedioSubtipoCmdDTO;

@SuppressWarnings("unchecked")
@Repository("ComandoDAO")
public class ComandoDAOImpl extends BaseDAOImpl<ComandoDTO> implements ComandoDAO{

	@Override
	@Transactional(readOnly = true)
	public List<ComandoDTO> obtenerComando(){	
		Criteria query = getCurrentSession().createCriteria(ComandoDTO.class);
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return (List<ComandoDTO>)query.list();
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<ComandoDTO> activarDesactivarObtenerComando(){	
		Criteria query = getCurrentSession().createCriteria(ComandoDTO.class);	
		query.addOrder(Order.asc("idComando"));
		return (List<ComandoDTO>)query.list();
	}

	@Override
	@Transactional(readOnly = true)
	public ComandoDTO obtenerComando(Long id){
		Criteria query = getCurrentSession().createCriteria(ComandoDTO.class);
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		query.add(Restrictions.eq("idComando", id));
		return (ComandoDTO)query.uniqueResult();
	}
			
	@Override
	@Transactional(readOnly = true)
	public ComandoDTO activarDesactivarObtenerComando(Long id){
		Criteria query = getCurrentSession().createCriteria(ComandoDTO.class);		
		query.add(Restrictions.eq("idComando", id));
		return (ComandoDTO)query.uniqueResult();
	}
	
	
	@Override
	@Transactional(readOnly = true)
	public List<ComandoDTO> obtenerComandosPorTipo(Long idSubTipoDispositivo,Long idTipoMedioComunicacion){
		
		Criteria query2 = getCurrentSession().createCriteria(MedioSubtipoCmdDTO.class);
		query2.add(Restrictions.eq("stActivo", Boolean.TRUE));
		query2.add(Restrictions.eq("idMedioComunicacionDTO.idMedio", idTipoMedioComunicacion));
		query2.add(Restrictions.eq("idSubTipoDispositivoDTO.idSubTipoDispositivo", idSubTipoDispositivo));
		MedioSubtipoCmdDTO medioSubtipoCmdDTO = (MedioSubtipoCmdDTO)query2.uniqueResult();
		
		if(medioSubtipoCmdDTO != null) {
			Criteria query = getCurrentSession().createCriteria(ComandoDTO.class);
			query.add(Restrictions.eq("stActivo", Boolean.TRUE));
			//query.add(Restrictions.eq("idTipoComando.idTipoComando", idTipoComando));
			query.add(Restrictions.eq("idMedioSubtipoCmdDTO.idMedioSubtipoComando", medioSubtipoCmdDTO.getIdMedioSubtipoComando()));
			return (List<ComandoDTO>)query.list();
		}
		return new ArrayList<ComandoDTO>();
	}
}
