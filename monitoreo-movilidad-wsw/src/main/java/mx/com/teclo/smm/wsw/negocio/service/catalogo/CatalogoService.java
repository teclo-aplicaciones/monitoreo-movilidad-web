package mx.com.teclo.smm.wsw.negocio.service.catalogo;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.vo.catalogo.CatalogoVO;

public interface CatalogoService {
	public List<CatalogoVO> catalogoMonitoreo();
	
}
