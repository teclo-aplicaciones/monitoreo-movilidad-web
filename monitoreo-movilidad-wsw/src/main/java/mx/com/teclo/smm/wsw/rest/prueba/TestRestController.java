package mx.com.teclo.smm.wsw.rest.prueba;

 
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ser.std.StdKeySerializers.Default;

import mx.com.teclo.arquitectura.ortogonales.exception.NotFoundException;
import mx.com.teclo.smm.wsw.persistencia.vo.UserMod;
import mx.com.teclo.smm.wsw.persistencia.vo.UserResponseMod;

@RestController
public class TestRestController {
	
	@RequestMapping(value="/consultaWs", method=RequestMethod.GET)
	public ResponseEntity<String> consultarProspectos(@RequestParam(name="parametro") String parametro) throws NotFoundException{
		return new ResponseEntity<String>("Hola Mundo", HttpStatus.OK);
	}
	
	//@RequestMapping(value="/login", method=RequestMethod.POST)
	public ResponseEntity<UserResponseMod> login(@RequestBody UserMod userMod) throws NotFoundException{
		UserResponseMod user = new UserResponseMod();
		System.out.println("::>" + userMod);
		user.setUserId(1L);
		
		
		switch(userMod.getUsername()){
			case "fmartinez":
				user.setToken("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJmbWFydGluZXoiLCJhdWRpZW5jZSI6IndlYiIsInBhZ2VzIjoiL1ViaWNhY2nDs24vUnV0YXMvTWVuc2FqZXMvQ29uZmlndXJhY2nDs24iLCJjcmVhdGVkIjoxNTQ4NzAxNzQ3NDk0LCJpbmFjdGl2aXR5IjozMCwibmFtZSI6IkZSQU5DSVNDTyBNQVJUSU5FWiIsImV4cCI6MTU0OTMwMTc0NywicGVyZmlsIjoiQURNSU5JU1RSQURPUiJ9.0f17ayimPz34ugD4AgQAsPUyGrWrubNGvEDMs5SL6Z0_m0G--zInmUE3R1ADLOPbf-Tb_bLvZ0sLJEvbv2b_lg");
				break;
			case "dgarcia":
				user.setToken("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJkZ2FyY2lhIiwiYXVkaWVuY2UiOiJ3ZWIiLCJwYWdlcyI6Ii9SdXRhcy9Db25maWd1cmFjacOzbiIsImNyZWF0ZWQiOjE1NDg3MDE3NDc0OTQsImluYWN0aXZpdHkiOjMwLCJuYW1lIjoiREFOSUVMIEdBUkNJQSIsImV4cCI6MTU0OTMwMTc0NywicGVyZmlsIjoiQURNSU5JU1RSQURPUiJ9.p3L7cPaoWnnu6AvdCYpMar8TJXoeELW4QZ4PV5TLAVR9g-weZDxWt3fkiTcJkh40uGH0kaFYt9y6qOnimt80hw");
				break;
			default:
				user.setToken(null);
				break;
			
		}
		
		return new ResponseEntity<UserResponseMod>(user, HttpStatus.OK);
	}
	
}