package mx.com.teclo.smm.wsw.rest.monitoreo;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.com.teclo.arquitectura.ortogonales.exception.NotFoundException;
import mx.com.teclo.arquitectura.ortogonales.responsehttp.BadRequestHttpResponse;
import mx.com.teclo.smm.wsw.negocio.service.dispositivo.DispositivoService;
import mx.com.teclo.smm.wsw.negocio.service.empleado.EmpleadoService;
import mx.com.teclo.smm.wsw.negocio.service.geocerca.GeocercaService;
import mx.com.teclo.smm.wsw.negocio.service.grupo.GrupoService;
import mx.com.teclo.smm.wsw.persistencia.vo.catalogo.CatalogoConSubtipoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.catalogo.CatalogoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.dispositivo.DispositivoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.monitoreo.ConfigMonitoreoDispoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.monitoreo.DispositivoRTDetalleVO;
import mx.com.teclo.smm.wsw.util.enumerable.CatalogoMonitoreoEnum;

@RestController
public class TiempoRealRestController {

	@Autowired
	GrupoService grupoService;
	
	@Autowired
	GeocercaService geocercaService;
	
	@Autowired
	DispositivoService dispositivoService;
	
	@Autowired
	EmpleadoService empleadoService;
	
	private static final Log log = LogFactory.getLog(TiempoRealRestController.class);
	
	@RequestMapping("/buscarAgrupamiento")	
	public ResponseEntity<List<CatalogoVO>> buscarCatalogo(@RequestParam("tipo")String tipo){
		List<CatalogoVO> x = new ArrayList<CatalogoVO>();
		
		if(Long.parseLong(tipo) == CatalogoMonitoreoEnum.GRUPOS.getIdCat()){
			x = grupoService.buscarGrupos();
		}else if(Long.parseLong(tipo) == CatalogoMonitoreoEnum.GEOCERCAS.getIdCat()){
			x = geocercaService.buscarGeocercas();
		}else if(Long.parseLong(tipo) == CatalogoMonitoreoEnum.DISPOSITIVOS.getIdCat()){
			x = dispositivoService.buscarDispositivo(null);
		}else if(Long.parseLong(tipo) == CatalogoMonitoreoEnum.USUARIOS.getIdCat()){
			x = empleadoService.buscarEmpleado();
		}
		
		return new ResponseEntity<List<CatalogoVO>>(x, HttpStatus.OK);
	}
	
	@RequestMapping("/buscarAgrupamientoConIdSubtipoDispositivo")	
	public ResponseEntity<List<CatalogoConSubtipoVO>> buscarAgrupamiento(@RequestParam("tipo")String tipo){
		List<CatalogoConSubtipoVO> x = new ArrayList<CatalogoConSubtipoVO>();
		
		if(Long.parseLong(tipo) == CatalogoMonitoreoEnum.GRUPOS.getIdCat()){
			x = grupoService.buscarGruposConIdSubtipoDispositivo();
		}else if(Long.parseLong(tipo) == CatalogoMonitoreoEnum.GEOCERCAS.getIdCat()){
			x = geocercaService.buscarGeocercasConIdSubtipoDispositivo();
		}else if(Long.parseLong(tipo) == CatalogoMonitoreoEnum.DISPOSITIVOS.getIdCat()){
			x = dispositivoService.buscarDispositivoConIdSubtipoDispositivo(null);
		}else if(Long.parseLong(tipo) == CatalogoMonitoreoEnum.USUARIOS.getIdCat()){
			x = empleadoService.buscarEmpleadoConIdSubtipoDispositivo();
		}
		
		return new ResponseEntity<List<CatalogoConSubtipoVO>>(x, HttpStatus.OK);
	}
	
	@RequestMapping("/cargarDetalleDispositivo")	
	public ResponseEntity<DispositivoRTDetalleVO> cargarDetalleDispositivo(@RequestParam("id")Long id){
		DispositivoRTDetalleVO drtdVO = dispositivoService.cargarDetalleDispositivo(id);
		return new ResponseEntity<DispositivoRTDetalleVO>(drtdVO, HttpStatus.OK);
	}
	
	@RequestMapping("/localizar")	
	public ResponseEntity<List<DispositivoVO>> localizarTiempoReal(@RequestParam("id")Long[] ids)throws NotFoundException{
		List<DispositivoVO> listaDispositivos = null;
		
		// todos		
		if(ids[0] == 0) {
			List<CatalogoVO> lsCatalogoVOs = dispositivoService.buscarDispositivo(null); 
			Long idsAll[] = new Long[lsCatalogoVOs.size()];
			int i=0;
			for(CatalogoVO c : lsCatalogoVOs) {
				idsAll[i]=c.getId();
				i++;
			}
			listaDispositivos = dispositivoService.buscarDispositivos(idsAll);
		}
		// por id
		else {			
			listaDispositivos = dispositivoService.buscarDispositivos(ids);		
		}			
		
		if(listaDispositivos != null){
			return new ResponseEntity<List<DispositivoVO>>(listaDispositivos, HttpStatus.OK);	
		}else{
			throw new NotFoundException("No se encontro registros.");
		}
	}
	
	@RequestMapping("/getParamsMonitoreo")	
	public ResponseEntity<List<ConfigMonitoreoDispoVO>> getParamsMonitoreo(@RequestParam("imei")Long imei)
			throws NotFoundException,BadRequestHttpResponse{
		
		if(imei == null){
			throw new BadRequestHttpResponse("El imei es requerido.");
		}
		
		List<ConfigMonitoreoDispoVO> listParamConfg = dispositivoService.buscarParamConfigMonitoreo(imei);
		
		if(listParamConfg == null || listParamConfg.isEmpty())
			throw new NotFoundException("No se encontro registros.");
			
		return new ResponseEntity<List<ConfigMonitoreoDispoVO>>(listParamConfg, HttpStatus.OK);
	}
	
	
	
}
