package mx.com.teclo.smm.wsw.rest.empleado;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.com.teclo.arquitectura.ortogonales.exception.NotFoundException;
import mx.com.teclo.smm.wsw.negocio.service.dispositivo.DispositivoService;
import mx.com.teclo.smm.wsw.negocio.service.empleado.EmpleadoService;
import mx.com.teclo.smm.wsw.persistencia.vo.catalogo.CatalogoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.empleado.EmpleadoVO;

@RestController
public class EmpleadoRestController {
	
	@Autowired
	private EmpleadoService empleadoService;
	
	@Autowired
	private DispositivoService dispositivoService;
	
	
	@RequestMapping("/validarUsuario")	
	public ResponseEntity<Boolean> validarUsuario(@RequestParam("id") Long id){
		
		//Validar si existe el usuario en BD
		
		return new ResponseEntity<Boolean>(Boolean.TRUE, HttpStatus.OK);
	}
	
	@RequestMapping(value="/buscarDispositivosEnUsuario", method=RequestMethod.GET)	
	public ResponseEntity<List<CatalogoVO>> buscarDispositivosEnUsuario(@RequestParam("id")String tipo)throws NotFoundException{
		List<CatalogoVO> listaDispos = new ArrayList<CatalogoVO>();
		
		Long[] listaIds = empleadoService.obtenerDispositivosEnUsuario(Long.parseLong(tipo));
		
		listaDispos = dispositivoService.buscarDispositivo(listaIds);
		
		return new ResponseEntity<List<CatalogoVO>>(listaDispos, HttpStatus.OK);
	}
	
	@RequestMapping(value="/buscarUsuario", method=RequestMethod.GET)	
	public ResponseEntity<List<EmpleadoVO>> buscarUsuario(@RequestParam("param") String param, @RequestParam("value") String value){
		List<EmpleadoVO> empl = empleadoService.buscarEmpleado(param, value);
		System.out.println("consulta " + param + " : " + value);
		return new ResponseEntity<List<EmpleadoVO>>(empl, HttpStatus.OK);
	}
}
