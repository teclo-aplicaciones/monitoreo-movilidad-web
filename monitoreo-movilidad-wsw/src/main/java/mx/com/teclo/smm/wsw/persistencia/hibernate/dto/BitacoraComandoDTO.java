package mx.com.teclo.smm.wsw.persistencia.hibernate.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TMM065B_CM_BITACORA")
public class BitacoraComandoDTO implements Serializable {

	private static final long serialVersionUID = -6001285286691260290L;

	@Id
	@GeneratedValue(generator="id_bitacora_generator",strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="id_bitacora_generator", sequenceName = "SQTMM065B_CM_BITACORA",allocationSize=1)
	@Column(name = "ID_BITACORA")
	private Long idBitacora;
	@Column(name="ID_EMPRESA")
	private long idEmpresa;
	@ManyToOne()
	@JoinColumn(name = "ID_DISPOSITIVO")
	private DispositivoDTO idDispositivoDTO;
	@ManyToOne()
	@JoinColumn(name = "ID_COMANDO")
	private ComandoDTO idComandoDTO;
	@ManyToOne()
	@JoinColumn(name = "ID_TIPO_COMANDO")
	private TipoComandoDTO idTipoComando;
	@ManyToOne()
	@JoinColumn(name = "ID_MEDIO_COMUNICACION")
	private MedioComunicacionDTO idMedioComunicacionDTO;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;

	public Long getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(Long idBitacora) {
		this.idBitacora = idBitacora;
	}
	public DispositivoDTO getIdDispositivoDTO() {
		return idDispositivoDTO;
	}
	public void setIdDispositivoDTO(DispositivoDTO idDispositivoDTO) {
		this.idDispositivoDTO = idDispositivoDTO;
	}
	public ComandoDTO getIdComandoDTO() {
		return idComandoDTO;
	}
	public void setIdComandoDTO(ComandoDTO idComandoDTO) {
		this.idComandoDTO = idComandoDTO;
	}
	public TipoComandoDTO getIdTipoComando() {
		return idTipoComando;
	}
	public void setIdTipoComando(TipoComandoDTO idTipoComando) {
		this.idTipoComando = idTipoComando;
	}

	public MedioComunicacionDTO getIdMedioComunicacionDTO() {
		return idMedioComunicacionDTO;
	}
	public void setIdMedioComunicacionDTO(MedioComunicacionDTO idMedioComunicacionDTO) {
		this.idMedioComunicacionDTO = idMedioComunicacionDTO;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
	public long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
}
