package mx.com.teclo.smm.wsw.negocio.service.notificacion;

import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.NotificacionDTO;

public interface NotificacionCorreoService {
	public Boolean enviaNotificacionMailInmediata(String nTelefonico, NotificacionDTO notificacion);
	public Boolean enviaNotificacionMailVelocityInmediata(String nCorreo, NotificacionDTO notificacion);
}
