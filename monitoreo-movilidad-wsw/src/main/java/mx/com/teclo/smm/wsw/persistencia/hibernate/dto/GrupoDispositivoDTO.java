package mx.com.teclo.smm.wsw.persistencia.hibernate.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TMM021D_DP_DISPOSITIVO_GRUPOS")
public class GrupoDispositivoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6345448840661015768L;

	@Id
	@Column(name = "ID_DISP_GRUPO")
	private Long idDispGrupo;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_GRUPO")
	private GrupoDTO idGrupo;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_DISPOSITIVO")
	private DispositivoDTO idDispositivo;
	@Column(name = "ST_ACTIVO")
	private Boolean stActivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;

	public Long getIdDispGrupo() {
		return idDispGrupo;
	}

	public void setIdDispGrupo(Long idDispGrupo) {
		this.idDispGrupo = idDispGrupo;
	}

	public GrupoDTO getIdGrupo() {
		return idGrupo;
	}

	public void setIdGrupo(GrupoDTO idGrupo) {
		this.idGrupo = idGrupo;
	}

	public DispositivoDTO getIdDispositivo() {
		return idDispositivo;
	}

	public void setIdDispositivo(DispositivoDTO idDispositivo) {
		this.idDispositivo = idDispositivo;
	}

	public Boolean getStActivo() {
		return stActivo;
	}

	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}

	public Date getFhCreacion() {
		return fhCreacion;
	}

	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}

	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}

	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}

	public Date getFhModificacion() {
		return fhModificacion;
	}

	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}

	public Long getIdUsrModifica() {
		return idUsrModifica;
	}

	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
}
