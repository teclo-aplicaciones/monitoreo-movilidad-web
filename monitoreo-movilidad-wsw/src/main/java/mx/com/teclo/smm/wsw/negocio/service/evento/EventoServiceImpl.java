package mx.com.teclo.smm.wsw.negocio.service.evento;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.teclo.smm.wsw.persistencia.hibernate.dao.EventoDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.EventoDTO;
import mx.com.teclo.smm.wsw.persistencia.vo.EventoVO;

@Service
public class EventoServiceImpl implements EventoService {

	@Autowired
	private EventoDAO eventoDAO;
	
	@Transactional
	@Override
	public List<EventoVO> consultaEventos() {
		List<EventoDTO> eventos = eventoDAO.findAll();
		List<EventoVO> eventosRet = new ArrayList<>();
		
		if(!eventos.isEmpty()) {
			for(EventoDTO ev : eventos) {
				eventosRet.add(new EventoVO(
						ev.getIdEvento(),
						ev.getIdTipoEvento().getIdTipoEvento(),
						ev.getIdSubtipoDispositivo().getIdSubtipoDispositivo(),
						ev.getCdEvento(),
						ev.getNbEvento(),
						ev.getTxEvento(),
						ev.getTxParametro(),
						ev.getStActivo()
					) {
						private static final long serialVersionUID = -1882788314717975317L;
				});
			}
		}
		
		return eventosRet;
	}

}
