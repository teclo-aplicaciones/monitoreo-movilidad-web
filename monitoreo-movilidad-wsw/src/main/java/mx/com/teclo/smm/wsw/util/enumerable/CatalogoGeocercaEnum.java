package mx.com.teclo.smm.wsw.util.enumerable;

import java.util.ArrayList;
import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.vo.catalogo.CatalogoVO;

public enum CatalogoGeocercaEnum {
	NOMBRE_GEOCERCA(1L, "Nombre Geocerca"), DISPOSITIVO(2L, "Dispositivo"), USUARIOS(3L, "Usuario");
	
	private Long idCat;
	private String nombreCat;
	
	private CatalogoGeocercaEnum(Long idCat, String nombreCat){
		this.idCat = idCat;
		this.nombreCat = nombreCat;
	}

	public Long getIdCat() {
		return idCat;
	}

	public void setIdCat(Long idCat) {
		this.idCat = idCat;
	}

	public String getNombreCat() {
		return nombreCat;
	}

	public void setNombreCat(String nombreCat) {
		this.nombreCat = nombreCat;
	}
	
	public static List<CatalogoVO> getCatalogo(){
		CatalogoGeocercaEnum[] listaEnum = CatalogoGeocercaEnum.values();
		List<CatalogoVO> listaCatVO = new ArrayList<CatalogoVO>(); 
		
		for(CatalogoGeocercaEnum cat : listaEnum)
		{
			CatalogoVO catVO = new CatalogoVO();
			catVO.setId(cat.getIdCat());
			catVO.setNombre(cat.getNombreCat());
			listaCatVO.add(catVO);
		}
		return listaCatVO;
	}
}
