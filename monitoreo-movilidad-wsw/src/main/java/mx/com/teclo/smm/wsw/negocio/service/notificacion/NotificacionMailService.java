package mx.com.teclo.smm.wsw.negocio.service.notificacion;

import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.NotificacionDTO;

public interface NotificacionMailService {
	public Boolean enviaNotificacioMailInmediata(String nCorreo, NotificacionDTO notificacion);
}
