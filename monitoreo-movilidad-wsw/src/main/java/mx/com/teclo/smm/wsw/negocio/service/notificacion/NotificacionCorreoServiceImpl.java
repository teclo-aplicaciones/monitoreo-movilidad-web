package mx.com.teclo.smm.wsw.negocio.service.notificacion;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.teclo.notificacionespush.mail.service.NotificationMailService;
import mx.com.teclo.notificacionespush.mail.vo.NotificacionVelocity;
import mx.com.teclo.notificacionespush.mail.vo.NotificationMail;
import mx.com.teclo.notificacionespush.mail.vo.ResponseNotificationMail;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.NotificacionDTO;

@Service
public class NotificacionCorreoServiceImpl implements NotificacionCorreoService{

	@Autowired
	private NotificationMailService notificationMailService;
	 
	private static final String CHARSET_UTF8 = "UTF-8";
	
	private static final String SUBJECT_MAIL_REGISTRATION_CONFIRMATION = "Prueba Envio de correos";
	
	@Override
	@SuppressWarnings("finally")
	public Boolean enviaNotificacionMailInmediata(String nCorreo, NotificacionDTO notificacion){
		ResponseNotificationMail rnm = null;
		List<String> listaCorreo = new ArrayList<String>();
		
		listaCorreo.add(nCorreo);
		
		NotificationMail correo = new NotificationMail(null,"[M&M] Notificación", listaCorreo, null, null, notificacion.getNbNotificacion(),notificacion.getTxMensaje(), null, null);
		try{
			rnm = notificationMailService.sendMail(correo);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			return rnm != null ? rnm.getStatus().equals("ok") ? Boolean.FALSE : Boolean.TRUE : Boolean.TRUE;
		}
	}
	
	@Override
	@SuppressWarnings("finally")
	public Boolean enviaNotificacionMailVelocityInmediata(String nCorreo, NotificacionDTO notificacion){
		ResponseNotificationMail rnm = new ResponseNotificationMail();
		Map<String, Object> attrMail = new HashMap<String, Object>();
		ClassLoader ctxLoader = getClass().getClassLoader();
		
		List<String> listaCorreo = new ArrayList<String>();
		listaCorreo.add(nCorreo);
		
		File req1 = new File(ctxLoader.getResource("files/firebase_example.json").getFile());
		File req2 = new File(ctxLoader.getResource("templates/velocity/isotipo.png").getFile());
		
		List<File> tmplMail = new ArrayList<File>();
		tmplMail.add(req1);
		tmplMail.add(req2);
		
		NotificationMail notMail = new NotificationMail("teclo.desarrollo.movil@gmail.com","[M&M] Notificación", listaCorreo, new ArrayList<String>(), null, notificacion.getNbNotificacion(), notificacion.getTxMensaje(), null, null);
		
		attrMail.put("titulo", "Notificación de Monitoreo & Movilidad");
		attrMail.put("body", notificacion.getTxMensaje());
		attrMail.put("nombreEmpresa", "2018. Teclo Mexicana S.A de C.V.");
		attrMail.put("aplicacion", "Monitoreo & Movilidad");
		
		NotificacionVelocity notiVelocity = new NotificacionVelocity(attrMail,
				"notificaction-localizacion.vm", "Velocity Template", CHARSET_UTF8);
		try{
			rnm = notificationMailService.sendVelocityMail(notMail, notiVelocity);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			return rnm != null ? rnm.getStatus().equals("ok") ? Boolean.FALSE : Boolean.TRUE : Boolean.TRUE;
		}
		
		/*ArrayList<String> mailTo = new ArrayList<String>();
		ArrayList<String> mailCc = new ArrayList<String>();
		ArrayList<String> mailBcc = new ArrayList<String>();

		/*mailTo.add("fjb.mx@gm2ail.com");
		mailTo.add("francisco.martinez@teclo.mx");
		mailTo.add("cesar.gomez@teclo.mx");*/
		//mailTo.add(nCorreo);
		//
		/*mailCc.add("fjmb.mx@gmail.com");
		mailCc.add("francisco.martinez@teclo.mx");
		mailCc.add("cesar.gomez@teclo.mx");
		mailCc.add(nCorreo);
		// NotificationMail notificacionMail = new
		// NotificationMail("aplicaciones.teclo@teclo.mx", "Mail Teclo ", mailTo, null, mailBcc, 
		//SUBJECT_MAIL_REGISTRATION_CONFIRMATION, "Se ha generado una nueva contraseña de acceso.", null, null);

		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("files/firebase_example.json").getFile());
		File file2 = new File(classLoader.getResource("templates/velocity/isotipo.png").getFile());

		ArrayList<File> mailAttachments = new ArrayList<File>();
		mailAttachments.add(file);
		mailAttachments.add(file2);

		NotificationMail notificacionMail = new NotificationMail("teclo.desarrollo.movil@gmail.com", "Mail Teclo ", mailTo,
				null, mailBcc, SUBJECT_MAIL_REGISTRATION_CONFIRMATION, "Se ha generado una nueva contraseña de acceso.", null, null);

		Map<String, Object> velAttributes = new HashMap<String, Object>();

		velAttributes.put("titulo", notificacion.getNbNotificacion());
		velAttributes.put("body", notificacion.getTxMensaje());
		velAttributes.put("nombreEmpresa", "2018. Teclo Mexicana S.A de C.V.");
 		velAttributes.put("aplicacion", "Monitoreo & Movilidad");
		//

		NotificacionVelocity notificacionVelocity = new NotificacionVelocity(velAttributes,
				"notificaction-localizacion.vm", "Velocity Template", CHARSET_UTF8);
		
		try{
			rnm = notificationMailService.sendVelocityMail(notificacionMail, notificacionVelocity);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			return rnm != null ? rnm.getStatus().equals("ok") ? Boolean.FALSE : Boolean.TRUE : Boolean.TRUE;
		}*/
		
	}
}
