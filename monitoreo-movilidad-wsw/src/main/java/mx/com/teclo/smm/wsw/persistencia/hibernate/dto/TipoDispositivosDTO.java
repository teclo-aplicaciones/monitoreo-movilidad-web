package mx.com.teclo.smm.wsw.persistencia.hibernate.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TMM002C_CT_TIPO_DISPOSITIVOS")
public class TipoDispositivosDTO implements Serializable{

	private static final long serialVersionUID = 2881821732970605709L;
	@Id
	@Column(name = "ID_TIPO_DISPOSITIVO")
	private Long idTipoDispositivo;
	@Column(name = "CD_TIPO_DISPOSITIVO")
	private String cdTipoDispositivo;
	@Column(name = "NB_TIPO_DISPOSITIVO")
	private String nbTipoDispositivo;
	@Column(name = "TX_TIPO_DISPOSITIVO")
	private String txTipoDispositivo;
	
	@Column(name = "ST_ACTIVO")
	private Boolean stActivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;
	
	public Long getIdTipoDispositivo() {
		return idTipoDispositivo;
	}
	public void setIdTipoDispositivo(Long idTipoDispositivo) {
		this.idTipoDispositivo = idTipoDispositivo;
	}
	public String getCdTipoDispositivo() {
		return cdTipoDispositivo;
	}
	public void setCdTipoDispositivo(String cdTipoDispositivo) {
		this.cdTipoDispositivo = cdTipoDispositivo;
	}
	public String getNbTipoDispositivo() {
		return nbTipoDispositivo;
	}
	public void setNbTipoDispositivo(String nbTipoDispositivo) {
		this.nbTipoDispositivo = nbTipoDispositivo;
	}
	public String getTxTipoDispositivo() {
		return txTipoDispositivo;
	}
	public void setTxTipoDispositivo(String txTipoDispositivo) {
		this.txTipoDispositivo = txTipoDispositivo;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
	@Override
	public String toString() {
		return "TipoDispositivosDTO [idTipoDispositivo=" + idTipoDispositivo + ", cdTipoDispositivo="
				+ cdTipoDispositivo + ", nbTipoDispositivo=" + nbTipoDispositivo + ", txTipoDispositivo="
				+ txTipoDispositivo + ", stActivo=" + stActivo + ", fhCreacion=" + fhCreacion + ", idUsrCreacion="
				+ idUsrCreacion + ", fhModificacion=" + fhModificacion + ", idUsrModifica=" + idUsrModifica + "]";
	}
	
	
}
