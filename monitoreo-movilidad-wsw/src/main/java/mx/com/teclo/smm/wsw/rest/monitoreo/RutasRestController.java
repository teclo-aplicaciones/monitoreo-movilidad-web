package mx.com.teclo.smm.wsw.rest.monitoreo;

import java.io.ByteArrayOutputStream;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.com.teclo.smm.wsw.negocio.service.dispositivo.DispositivoService;
import mx.com.teclo.smm.wsw.persistencia.vo.dispositivo.DispositivoHistoricoVO;

@RestController
public class RutasRestController {

	
	@Autowired
	DispositivoService dispositivoService;
	
	@RequestMapping("/buscarRutaDispositivo")
	public ResponseEntity<DispositivoHistoricoVO> buscarCatalogo(@RequestParam("id")String id, @RequestParam("tipoBusq")Long tipoBusq, 
			@RequestParam("fechaInicio")String fechaInicio, @RequestParam("fechaFin")String fechaFin){
		DispositivoHistoricoVO x = dispositivoService.buscarRutaDispositivoPorId(id, tipoBusq, fechaInicio, fechaFin, true);
		return new ResponseEntity<DispositivoHistoricoVO>(x, HttpStatus.OK);
	}
	
	@RequestMapping("/generarExcel")	
	public ResponseEntity<byte[]> generarExcel(@RequestParam("id")String id, @RequestParam("tipoBusq")Long tipoBusq, 
			@RequestParam("fechaInicio")String fechaInicio, @RequestParam("fechaFin")String fechaFin){
		
		DispositivoHistoricoVO dispHistVO = dispositivoService.buscarRutaDispositivoPorId(id, tipoBusq, fechaInicio, fechaFin, false);
		
		Map data =  dispositivoService.generarReporteRutaHistorico(dispHistVO, fechaInicio, fechaFin);
    	final byte[] bytes = ((ByteArrayOutputStream) data.get("archivo")).toByteArray();
    	String filename = (String) data.get("nombre");
    	
    	HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/vnd.ms-exce"));
    	headers.add("Content-Disposition", "attachment; filename=" + filename);
    	headers.add("filename",   filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        headers.setContentLength(bytes.length);
        
        return new ResponseEntity<byte[]>(bytes, headers, HttpStatus.OK);
	}
}
