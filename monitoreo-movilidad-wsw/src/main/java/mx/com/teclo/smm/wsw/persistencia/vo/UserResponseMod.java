package mx.com.teclo.smm.wsw.persistencia.vo;

import java.io.Serializable;

public class UserResponseMod implements Serializable{
	
	private static final long serialVersionUID = -4814599767888567138L;
	private Long userId;
    private String username;
    private String user;
    private String token;
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
    
}
