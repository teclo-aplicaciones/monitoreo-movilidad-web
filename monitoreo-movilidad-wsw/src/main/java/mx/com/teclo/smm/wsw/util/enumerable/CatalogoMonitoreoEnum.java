package mx.com.teclo.smm.wsw.util.enumerable;

import java.util.ArrayList;
import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.vo.catalogo.CatalogoVO;

public enum CatalogoMonitoreoEnum {
	GRUPOS(1L, "Grupos"), GEOCERCAS(2L, "Geocercas"), DISPOSITIVOS(3L, "Dispositivos"), USUARIOS(4L, "Usuarios");
	
	private Long idCat;
	private String nombreCat;
	
	private CatalogoMonitoreoEnum(Long idCat, String nombreCat){
		this.idCat = idCat;
		this.nombreCat = nombreCat;
	}

	public Long getIdCat() {
		return idCat;	
	}

	public void setIdCat(Long idCat) {
		this.idCat = idCat;
	}

	public String getNombreCat() {
		return nombreCat;
	}

	public void setNombreCat(String nombreCat) {
		this.nombreCat = nombreCat;
	}
	
	@SuppressWarnings("unchecked")
	public static List<CatalogoVO> getCatalogo(){
		CatalogoMonitoreoEnum[] listaEnum = CatalogoMonitoreoEnum.values();
		List<CatalogoVO> listaCatVO = new ArrayList<CatalogoVO>(); 
		
		for(CatalogoMonitoreoEnum cat : listaEnum)
		{
			CatalogoVO catVO = new CatalogoVO();
			catVO.setId(cat.getIdCat());
			catVO.setNombre(cat.getNombreCat());
			listaCatVO.add(catVO);
		}
		return listaCatVO;
	}
}
