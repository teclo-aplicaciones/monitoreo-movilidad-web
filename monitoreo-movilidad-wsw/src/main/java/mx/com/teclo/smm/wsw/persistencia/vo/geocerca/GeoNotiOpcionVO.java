package mx.com.teclo.smm.wsw.persistencia.vo.geocerca;

public class GeoNotiOpcionVO {
	private Long id;
	private String nombre;
	private Boolean valor;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Boolean getValor() {
		return valor;
	}
	public void setValor(Boolean valor) {
		this.valor = valor;
	}
}
