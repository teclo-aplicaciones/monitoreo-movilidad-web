package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.MedioNotificacionDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.TipoNotificacionDTO;

@Repository("medioNotificacionDAO")
@SuppressWarnings("unchecked")
public class MedioNotificacionDAOImpl extends BaseDAOImpl<MedioNotificacionDTO> implements MedioNotificacionDAO {
	
	@Override
	public List<MedioNotificacionDTO> buscarCatalogo(TipoNotificacionDTO tnDTO){
		Criteria query = getCurrentSession().createCriteria(MedioNotificacionDTO.class);
		query.add(Restrictions.eq("idTipoNotificacion", tnDTO));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return (List<MedioNotificacionDTO>) query.list();
	}
	
	@Override
	public MedioNotificacionDTO buscarMedioPorId(Long id){
		Criteria query = getCurrentSession().createCriteria(MedioNotificacionDTO.class);
		query.add(Restrictions.eq("idMedioTipoNotifi", id));
		return (MedioNotificacionDTO) query.uniqueResult();
	}
}
