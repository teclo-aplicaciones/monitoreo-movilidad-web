package mx.com.teclo.smm.wsw.rest.dispositivos;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.com.teclo.arquitectura.ortogonales.responsehttp.ConflictHttpResponse;
import mx.com.teclo.arquitectura.ortogonales.responsehttp.OKHttpResponse;
import mx.com.teclo.smm.wsw.negocio.service.dispositivo.DispositivoService;
import mx.com.teclo.smm.wsw.persistencia.vo.dispositivo.DispositivoVO;
import mx.com.teclo.smm.wsw.rest.coordenada.CoordenadaRestController;

@RestController
public class DispositivosRestController {

	@Autowired 
	private DispositivoService dispositivoService;
	
	private static final Logger logger = Logger.getLogger(CoordenadaRestController.class);

	@RequestMapping(value="/dispositivos/firebase/token", method=RequestMethod.PUT)
	public ResponseEntity<DispositivoVO> actulizarTokenFirebaseDipositivo(@RequestParam Long nuImei, @RequestParam String tokenFirebase) throws OKHttpResponse, ConflictHttpResponse{
		DispositivoVO  dispositivoVO = null;
//		logger.debug("{} Imei Disposti [{}] ",threadResult.getName(),smsQueue.toString());
		logger.info("IMEI:"+nuImei+" TOken"+tokenFirebase);
		dispositivoVO = dispositivoService.updateTokenFirebaseDispositivo(nuImei,tokenFirebase);
 
	   if(dispositivoVO!=null)
			throw new OKHttpResponse("El token del dispositivo se actualizó correctamente.","TCL20003", dispositivoVO);
		else
			throw new ConflictHttpResponse("No se pudo actualizar el token del dispositivo.","TCL40903", dispositivoVO);
	}

}
