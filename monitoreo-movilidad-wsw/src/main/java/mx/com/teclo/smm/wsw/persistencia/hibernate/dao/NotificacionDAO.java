package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.MedioNotificacionDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.NotificacionDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.TipoNotificacionDTO;

public interface NotificacionDAO extends BaseDAO<NotificacionDTO>{
	public Long buscarSiguienteIdentificador();
	public NotificacionDTO buscarNotificacionPorId(Long id);
	public List<NotificacionDTO> buscarNotificacionPorNombre(String nombre, TipoNotificacionDTO tNDTO, MedioNotificacionDTO mnDTO);
}
