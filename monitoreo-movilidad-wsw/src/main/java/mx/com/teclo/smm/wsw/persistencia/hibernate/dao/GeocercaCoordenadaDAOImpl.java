package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.GeocercaCoordenadaDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.GeocercaDTO;

@SuppressWarnings("unchecked")
@Repository("geocercaCoordenadaDAO")
public class GeocercaCoordenadaDAOImpl extends BaseDAOImpl<GeocercaCoordenadaDTO> implements GeocercaCoordenadaDAO{

	@Override
	public Long buscarSiguenteValor(){
		Criteria query = getCurrentSession().createCriteria(GeocercaCoordenadaDTO.class);
		query.setProjection(Projections.max("idCordenadasGeo"));
		Long res = (Long)query.uniqueResult();
		
		return res != null? res+1:1;
	}
	
	@Override
	public List<GeocercaCoordenadaDTO> buscarRelacionGeocercaCoordenadas(GeocercaDTO gDTO){
		Criteria query = getCurrentSession().createCriteria(GeocercaCoordenadaDTO.class);
		query.add(Restrictions.eq("idGeocercas", gDTO));
		
		return (List<GeocercaCoordenadaDTO>) query.list();
	}
	
	@Override
	public Boolean eliminarRelacionGeocercaCoordenada(GeocercaDTO gDTO){
		Criteria query = getCurrentSession().createCriteria(GeocercaCoordenadaDTO.class);
		query.add(Restrictions.eq("idGeocercas", gDTO));
		
		List<GeocercaCoordenadaDTO> gcDTO = (List<GeocercaCoordenadaDTO>) query.list();
		
		for(GeocercaCoordenadaDTO x: gcDTO){
			this.delete(x);
		}
		
		return Boolean.TRUE;
		
	}
}
