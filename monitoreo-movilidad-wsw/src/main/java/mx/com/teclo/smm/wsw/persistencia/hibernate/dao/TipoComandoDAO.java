package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.TipoComandoDTO;


public interface TipoComandoDAO extends BaseDAO<TipoComandoDTO>{
	public List<TipoComandoDTO> obtenerTipoComando();
	public TipoComandoDTO activarDesactivarObtenerTipoComando(Long id);
	public TipoComandoDTO obtenerTipoComando(Long id);
	public List<TipoComandoDTO> activarDesactivarObtenerTipoComando() ;
}
	