package mx.com.teclo.smm.wsw.negocio.service.coordenadas;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.smm.wsw.negocio.service.geocerca.GeocercaService;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dao.DispositivosDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dao.EventoDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dao.PosicionesActDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.DispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.EventoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.PosicionesActDTO;
import mx.com.teclo.smm.wsw.persistencia.myBatis.PosicionesActMapper;
import mx.com.teclo.smm.wsw.persistencia.vo.PosicionesActVO;
import mx.com.teclo.smm.wsw.util.google.ServicioGoogle;

@Service
public class CoordenadasServiceImpl implements CoordenadaService {

	@Autowired
	private PosicionesActDAO posicionesActDAO;
	@Autowired
	private DispositivosDAO dispositivosDAO;
	@Autowired
	private PosicionesActMapper posicionesActMapper;
	@Autowired
	private EventoDAO eventoDAO;
	@Autowired
	private GeocercaService geocercaService;
	
	@Transactional
	@Override
	public PosicionesActVO agregarCoordenada(PosicionesActVO posicionesActVO) {
		DispositivoDTO disp;
		PosicionesActDTO pos, posConsulta;
		Serializable s ;
				
		/*Buscar los datos del dispositivo que tiene asignado el numero imei que reporta las coordenadas*/
		disp = dispositivosDAO.findByImei(posicionesActVO.getNuImei());
		if(disp != null) {
			
			/* Obtener la direccion y la altitud a travez de los servicios de google*/
			String direccion = ServicioGoogle.buscarDireccion(posicionesActVO.getNuLatitud(), posicionesActVO.getNuLongitud());
			Double altitud = ServicioGoogle.buscarAltitud(posicionesActVO.getNuLatitud(), posicionesActVO.getNuLongitud());
			Date date = new Date();
			try {
				date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(posicionesActVO.getFhRegistro());
			} catch (ParseException e) {}

			/*Buscar si existe registro de posiciones actuales del dispositivo encontrado con el numero imei */
			posConsulta = posicionesActDAO.findByIdDispositivoAndImei(disp.getIdDispositivo(),posicionesActVO.getNuImei());
//			if(pos == null)
			
			EventoDTO eventoDTO = eventoDAO.findByIdEvento(posicionesActVO.getIdEvento());
			
			pos = new PosicionesActDTO();
			pos.setIdDispositivo(disp);
			pos.setNuLatitud(posicionesActVO.getNuLatitud());
			pos.setNuLongitud(posicionesActVO.getNuLongitud());	
			pos.setNuAccuracy(posicionesActVO.getNuAccuracy());
			pos.setNuAltitud(altitud);
			pos.setNuImei(posicionesActVO.getNuImei());
			pos.setNuIp(posicionesActVO.getNuIp());
			pos.setTxDireccion(direccion);
			pos.setFhRegistro(date);
			pos.setStActivo(true);
			pos.setIdUsrCreacion(1L);
			pos.setFhCreacion(new Date());
			pos.setIdUsrModifica(1L);
			pos.setFhModificacion(new Date());
			pos.setIdEvento(eventoDTO);
			
			if(posConsulta == null) {
				posicionesActMapper.insertarPosicionesActuales(pos);
//				posicionesActDAO.guardarPosicionActualCompleto(pos);
//				s = posicionesActDAO.save(pos);
//				posicionesActVO.setIdPosicionesAct((Long)s);
			}else {
				pos.setIdPosicionesAct(posConsulta.getIdPosicionesAct());
				posicionesActMapper.actualizarPosicionesActuales(pos);
//				posicionesActDAO.actualizarPosicionActualCompleto(pos);
//				posicionesActDAO.update(pos);
			}
			
			//Despues de capturar el evento, validar si pertenece a una geocerca
			geocercaService.validarDispositivoEnGeocerca(disp, posicionesActVO.getNuLongitud(), posicionesActVO.getNuLatitud());
			
			return posicionesActVO;
		} // si no existe el dispositivo registrado no se agregan los datos en la tabla de posiciones actuales ...
		else return null;
		
	}

}
