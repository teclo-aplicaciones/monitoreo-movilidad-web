package mx.com.teclo.smm.wsw.persistencia.hibernate.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="TMM062D_GL_GEO_NOTIFICA")
public class GeocercaNotificacionDTO implements Serializable{

	private static final long serialVersionUID = 3459461602314747454L;

	@Id
	@Column(name="ID_GEO_NOTIFICA")
	private Long idGeoNotifica;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_GEOCERCA")
	private GeocercaDTO idGeocerca;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_NOTIFICACION")
	private NotificacionDTO idNotificacion;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CONFIG_GEO_NOTI")
	private ConfigNotificaGeocercaDTO idConfigGeoNoti;
	@Column(name="ST_ACTIVO")
	private Boolean stActivo;
	@Column(name="FH_CREACION")
	private Date fhCreacion;
	@Column(name="ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name="FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name="ID_USR_MODIFICA")
	private Long idUsrModifica;
	
	public Long getIdGeoNotifica() {
		return idGeoNotifica;
	}
	public void setIdGeoNotifica(Long idGeoNotifica) {
		this.idGeoNotifica = idGeoNotifica;
	}
	public GeocercaDTO getIdGeocerca() {
		return idGeocerca;
	}
	public void setIdGeocerca(GeocercaDTO idGeocerca) {
		this.idGeocerca = idGeocerca;
	}
	public NotificacionDTO getIdNotificacion() {
		return idNotificacion;
	}
	public void setIdNotificacion(NotificacionDTO idNotificacion) {
		this.idNotificacion = idNotificacion;
	}
	public ConfigNotificaGeocercaDTO getIdConfigGeoNoti() {
		return idConfigGeoNoti;
	}
	public void setIdConfigGeoNoti(ConfigNotificaGeocercaDTO idConfigGeoNoti) {
		this.idConfigGeoNoti = idConfigGeoNoti;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
}

