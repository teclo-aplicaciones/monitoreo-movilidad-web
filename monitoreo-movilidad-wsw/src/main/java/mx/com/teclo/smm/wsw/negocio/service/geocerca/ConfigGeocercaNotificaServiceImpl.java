package mx.com.teclo.smm.wsw.negocio.service.geocerca;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.teclo.smm.wsw.persistencia.myBatis.GeocercaSpatialMapper;

@Service
public class ConfigGeocercaNotificaServiceImpl implements ConfigGeocercaNotificaService{

	@Autowired
	GeocercaSpatialMapper geocercaSpatialMapper;
	
	@Override
	public Boolean validarPuntoDentroDeGeocerca(Long id, Double lng, Double lat){
		return geocercaSpatialMapper.validaPuntoEnForma(lng, lat, id).equals("TRUE") ? Boolean.TRUE:Boolean.FALSE;
	}
	
	@Override
	public Boolean validarPuntoFueraDeGeocerca(Long id, Double lng, Double lat){
		return geocercaSpatialMapper.validaPuntoEnForma(lng, lat, id).equals("FALSE") ? Boolean.TRUE:Boolean.FALSE;
	}
}
