package mx.com.teclo.smm.wsw.negocio.service.dispositivo;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.arquitectura.ortogonales.exception.NotFoundException;
import mx.com.teclo.arquitectura.ortogonales.util.ResponseConverter;
import mx.com.teclo.generaexcel.reporteexcel.PeticionReporteBOImpl;
import mx.com.teclo.generaexcel.vo.PeticioReporteVO;
import mx.com.teclo.generaexcel.vo.PropiedadesReporte;
import mx.com.teclo.smm.wsw.negocio.service.empresa.EmpresaService;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dao.DispoEmpleadoDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dao.DispositivoDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dao.DispositivosDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dao.EmpleadoDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dao.EventoDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dao.GeocercaDispositivoDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dao.GrupoDispositivoDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dao.IndicadorEventoDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dao.InformacionDispDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dao.MarcaDispositivoDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dao.ModeloDispositivoDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dao.PosicionActualDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dao.SubTipoDispositivoDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dao.TipoDispositivoDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.DispoEmpleadoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.DispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.EmpleadoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.EventoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.GeocercaDispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.GrupoDispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.IndicadorEventoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.InformacionDispDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.MarcaDispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.ModeloDispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.PosicionActualDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.SubTipoDispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.TipoDispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.myBatis.DispositivoMyBatisDAO;
import mx.com.teclo.smm.wsw.persistencia.vo.catalogo.CatalogoConSubtipoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.catalogo.CatalogoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.dispositivo.DispositivoGuardarVO;
import mx.com.teclo.smm.wsw.persistencia.vo.dispositivo.DispositivoHistoricoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.dispositivo.DispositivoSubtipoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.dispositivo.DispositivoTableVO;
import mx.com.teclo.smm.wsw.persistencia.vo.dispositivo.DispositivoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.dispositivo.InfoDispositivoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.dispositivo.RutaVO;
import mx.com.teclo.smm.wsw.persistencia.vo.empleado.InfoEmpleadoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.geocerca.DispositivoEnGeocercaVO;
import mx.com.teclo.smm.wsw.persistencia.vo.monitoreo.ConfigMonitoreoDispoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.monitoreo.CoordVO;
import mx.com.teclo.smm.wsw.persistencia.vo.monitoreo.DispositivoRTDetalleVO;
import mx.com.teclo.smm.wsw.persistencia.vo.monitoreo.EventoVO;
import mx.com.teclo.smm.wsw.util.comun.ConvertCoordUtils;
import mx.com.teclo.smm.wsw.util.enumerable.TipoBusquedaRuta;

@Service
public class DispositivoServiceImpl implements DispositivoService {
	@Autowired
	private DispositivosDAO dispositivosDAO;

	@Autowired
	DispositivoDAO dispositivoDAO;

	@Autowired
	InformacionDispDAO informacionDispDAO;

	@Autowired
	PosicionActualDAO posicionActualDAO;

	@Autowired
	DispositivoMyBatisDAO dispositivoMyBatisDAO;

	@Autowired
	TipoDispositivoDAO tipoDispositivoDAO;
		@Autowired
	SubTipoDispositivoDAO subTipoDispositivoDAO;

	@Autowired
	MarcaDispositivoDAO marcaDispositivoDAO;

	@Autowired
	ModeloDispositivoDAO modeloDispositivoDAO;

	@Autowired
	DispoEmpleadoDAO dispoEmpleadoDAO;

	@Autowired
	IndicadorEventoDAO indicadorEventoDAO;

	ConvertCoordUtils convertCoordUtils = new ConvertCoordUtils();

	@Autowired
	EventoDAO eventoDAO;

	@Autowired
	EmpleadoDAO empleadoDAO;

	@Autowired
	GeocercaDispositivoDAO geocercaDispositivoDAO;


	@Autowired
	private GrupoDispositivoDAO grupoDispositivoDAO;

	@Autowired
	private EmpresaService empresaService;

	@Override
	@Transactional
	public DispositivoVO updateTokenFirebaseDispositivo(Long nuImei,String tokenFirebase) {
		DispositivoDTO dispositivo;
		DispositivoVO dispositivoVO = null;
		dispositivo = dispositivosDAO.findByImei(nuImei);
		if(dispositivo!=null){
			dispositivo.setFbDispositivo(tokenFirebase);
			dispositivosDAO.saveOrUpdate(dispositivo);
			dispositivoVO= ResponseConverter.copiarPropiedadesFull(dispositivo, DispositivoVO.class);
		}
		return dispositivoVO;
	}

	@Override
	@Transactional(readOnly=true)
	public List<CatalogoVO> buscarDispositivo(Long[] ids) {
		//ObjectMapper objectMapper = new ObjectMapper();
		CatalogoVO dispo = null;
		List<CatalogoVO> listaDisp = new ArrayList<CatalogoVO>();
		List<DispositivoDTO> dispDTOs = null;
		if(ids != null){
			dispDTOs = dispositivoDAO.buscarDispositivosPorId(ids);
		}else{
			dispDTOs = dispositivoDAO.buscarDispositivos();
		}


		if(!dispDTOs.isEmpty()){
			for(DispositivoDTO dispDTO : dispDTOs){
				dispo = new CatalogoVO();
				dispo.setId(dispDTO.getIdDispositivo());
				dispo.setNombre(dispDTO.getNbDispositivo());
				listaDisp.add(dispo);
			}
		}else{
			listaDisp = null;
		}

		/**try {
			listaDispositivos = objectMapper.readValue(JSON, new TypeReference<List<JsonObjectVO>>(){});
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for(JsonObjectVO object: listaDispositivos)
		{
			CatalogoVO dispVo = new CatalogoVO();
			dispVo.setId(Long.parseLong(object.getId()));
			dispVo.setNombre(object.getNombre());
			listaDisp.add(dispVo);
		}*/

		return listaDisp;
	}

	@Override
	@Transactional(readOnly=true)
	public List<CatalogoConSubtipoVO> buscarDispositivoConSubTipoDispositivo(Long[] ids) {
		//ObjectMapper objectMapper = new ObjectMapper();
		CatalogoConSubtipoVO dispo = null;
		List<CatalogoConSubtipoVO> listaDisp = new ArrayList<CatalogoConSubtipoVO>();
		List<DispositivoDTO> dispDTOs = null;
		if(ids != null){
			dispDTOs = dispositivoDAO.buscarDispositivosPorId(ids);
		}else{
			dispDTOs = dispositivoDAO.buscarDispositivos();
		}


		if(!dispDTOs.isEmpty()){
			for(DispositivoDTO dispDTO : dispDTOs){
				dispo = new CatalogoConSubtipoVO();
				dispo.setId(dispDTO.getIdDispositivo());
				dispo.setNombre(dispDTO.getNbDispositivo());
				dispo.setSubTipoDispositivo(dispDTO.getIdModelo().getIdSubtipoDispositivo().getIdSubTipoDispositivo());
				listaDisp.add(dispo);
			}
		}else{
			listaDisp = null;
		}

		/**try {
			listaDispositivos = objectMapper.readValue(JSON, new TypeReference<List<JsonObjectVO>>(){});
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for(JsonObjectVO object: listaDispositivos)
		{
			CatalogoVO dispVo = new CatalogoVO();
			dispVo.setId(Long.parseLong(object.getId()));
			dispVo.setNombre(object.getNombre());
			listaDisp.add(dispVo);
		}*/

		return listaDisp;
	}

	@Override
	@Transactional(readOnly=true)
	public List<CatalogoConSubtipoVO> buscarDispositivoConIdSubtipoDispositivo(Long[] ids) {
		//ObjectMapper objectMapper = new ObjectMapper();
		CatalogoConSubtipoVO dispo = null;
		List<CatalogoConSubtipoVO> listaDisp = new ArrayList<CatalogoConSubtipoVO>();
		List<DispositivoDTO> dispDTOs = null;
		if(ids != null){
			dispDTOs = dispositivoDAO.buscarDispositivosPorId(ids);
		}else{
			dispDTOs = dispositivoDAO.buscarDispositivos();
		}


		if(!dispDTOs.isEmpty()){
			for(DispositivoDTO dispDTO : dispDTOs){
				dispo = new CatalogoConSubtipoVO();
				dispo.setId(dispDTO.getIdDispositivo());
				dispo.setNombre(dispDTO.getNbDispositivo());
				dispo.setSubTipoDispositivo(dispDTO.getIdModelo().getIdSubtipoDispositivo().getIdSubTipoDispositivo());
				listaDisp.add(dispo);
			}
		}else{
			listaDisp = null;
		}

		/**try {
			listaDispositivos = objectMapper.readValue(JSON, new TypeReference<List<JsonObjectVO>>(){});
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for(JsonObjectVO object: listaDispositivos)
		{
			CatalogoVO dispVo = new CatalogoVO();
			dispVo.setId(Long.parseLong(object.getId()));
			dispVo.setNombre(object.getNombre());
			listaDisp.add(dispVo);
		}*/

		return listaDisp;
	}



	@Override
	@Transactional(readOnly=true)
	public List<DispositivoEnGeocercaVO> buscarDispositivoParaGeocerca(Long[] ids) {
		//ObjectMapper objectMapper = new ObjectMapper();
		DispositivoEnGeocercaVO dispo = null;
		List<DispositivoEnGeocercaVO> listaDisp = new ArrayList<DispositivoEnGeocercaVO>();
		List<DispositivoDTO> dispDTOs = null;

		if(ids != null){
			dispDTOs = dispositivoDAO.buscarDispositivosPorId(ids);
		}else{
			dispDTOs = dispositivoDAO.buscarDispositivos();
		}

		if(!dispDTOs.isEmpty()){
			for(DispositivoDTO dispDTO : dispDTOs){
				dispo = new DispositivoEnGeocercaVO();
				dispo.setId(dispDTO.getIdDispositivo());
				dispo.setNombre(dispDTO.getNbDispositivo());
				dispo.setSerial(informacionDispDAO.buscarRelacionPorDispositivo(dispDTO).getNuSerie());
				listaDisp.add(dispo);
			}
		}else{
			listaDisp = null;
		}

		return listaDisp;
	}

	@Override
	@Transactional(readOnly=true)
	public List<DispositivoVO> buscarDispositivos(Long[] ids) throws NotFoundException {
		List<DispositivoVO> dispVOs = new ArrayList<DispositivoVO>();
		DispositivoVO dispo = null;
		InfoEmpleadoVO info = null;
		DispoEmpleadoDTO deDTO = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		List<DispositivoDTO> dispDTOs = dispositivoDAO.buscarDispositivosPorId(ids);

		if(!dispDTOs.isEmpty()){
			for(DispositivoDTO dispDTO : dispDTOs){
				dispo = new DispositivoVO();
				info = new InfoEmpleadoVO();
				deDTO = dispoEmpleadoDAO.buscarRelacionDispoEmpleadoPorDispositivo(dispDTO);
				List<PosicionActualDTO> posActDTO = posicionActualDAO.obtenerCoordenadaActual(dispDTO);
				//Set a la información
				//Si no tiene posiciones, mandar que no existe
				if(!posActDTO.isEmpty()){
					Long diferencia = new Date().getTime() - posActDTO.get(0).getFhRegistro().getTime();
					String color = obtenerImagenPorIndicador(((diferencia/1000)/60));

					if(deDTO != null){
						info.setImg(dispDTO.getIdModelo().getIdMarca().getIdTipoDispositivo().getImgTipoDispositivo().replace("&", color));
						info.setNombre(dispDTO.getNbDispositivo());
						info.setUsuario(obtenerNombreEmpleado(dispDTO));
						info.setDireccion(posActDTO.get(0).getTxDireccion());
						info.setUltimaFecha(sdf.format(posActDTO.get(0).getFhRegistro()));
						info.setTipo(dispDTO.getIdModelo().getIdSubtipoDispositivo().getIdTipoDispositivo().getNbTipoDispositivo());
						info.setImei(dispDTO.getCdDispositivo());
					}else{
						info.setImg(dispDTO.getIdModelo().getIdMarca().getIdTipoDispositivo().getImgTipoDispositivo().replace("&", color));
						info.setNombre(dispDTO.getNbDispositivo());
						info.setUsuario("Sin usuario asociado");
						info.setDireccion(posActDTO.get(0).getTxDireccion());
						info.setUltimaFecha(sdf.format(posActDTO.get(0).getFhRegistro()));
						info.setTipo(dispDTO.getIdModelo().getIdSubtipoDispositivo().getIdTipoDispositivo().getNbTipoDispositivo());
						info.setImei(dispDTO.getCdDispositivo());
					}

					//Set a dispositivos
					dispo.setId(dispDTO.getIdDispositivo());
					dispo.setCoords(obtenerCoordenadaActualPorDispositivo(dispDTO));
					dispo.setInfo(info);
					dispVOs.add(dispo);
				}else{
					dispo.setId(dispDTO.getIdDispositivo());
					dispo.setCoords(new ArrayList<Double[]>());
					dispo.setInfo(info);
					dispVOs.add(dispo);
				}
			}
		}else{
			dispVOs =null;
		}

		/*ObjectMapper objectMapper = new ObjectMapper();

		try {
			listaDispositivos = objectMapper.readValue(JSON, new TypeReference<List<JsonObjectVO>>(){});
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if(ids == null){
			ids = new int[listaDispositivos.size()];
			for(int i = 0;i<listaDispositivos.size();i++){
				ids[i]=i+1;
			}
		}

		for(JsonObjectVO obj: listaDispositivos)
		{
			for(int idx : ids){
				if(Integer.parseInt(obj.getId()) == idx){
					DispositivoVO dispVo = new DispositivoVO();
					puntosLoc = new ArrayList<Double[]>();
					dispVo.setId(Long.parseLong(obj.getId()));
					dispVo.setNombre(obj.getNombre());
					dispVo.setImei("12345678901112");
					dispVo.setTipo(dispVo.getId()%2==0?1L:2L);
					dispVo.setMarca(dispVo.getId()%2==0?"Samsung Galaxy":"Mitsubishi");
					dispVo.setModelo(dispVo.getId()%2==0?"A7 2017":"Endeavor");
					List<Map<String,String>> puntos = obj.getDatos();
					for(Map<String, String> punto : puntos)
					{
						Double[] coord ={Double.parseDouble(punto.get("lat")), Double.parseDouble(punto.get("lng"))};
						puntosLoc.add(coord);
					}
					dispVo.setCoords(puntosLoc);
					dispVOs.add(dispVo);
				}
			}
		}*/

		return dispVOs;
	}

	@Override
	@Transactional(readOnly=true)
	public DispositivoHistoricoVO buscarRutaDispositivoPorId(String id, Long tipoBusq, String fechaInicio, String fechaFin, Boolean filterByCero) {

		DispositivoHistoricoVO dhVO = new DispositivoHistoricoVO();
		DispositivoDTO disp = dispositivoDAO.buscarDispositivosPorId(Long.parseLong(id));
		List<CoordVO> listaCoords = null;
		List<String> listaFechas = null;
		RutaVO rVO = null;
		List<RutaVO> listaruta = new ArrayList<RutaVO>();
		List<EventoVO> listaEventos = null;
		if(fechaInicio != null && !fechaInicio.trim().equals("") || fechaFin != null && !fechaFin.trim().equals("")){

			if(tipoBusq == TipoBusquedaRuta.RUTAS.getTipoBusquedaRuta()){
				//Busqueda por Rutas
				//listaFechas = convertCoordUtils.obtenerDiasPorRangoFecha(fechaInicio, fechaFin);
				if(filterByCero){
					listaCoords = dispositivoMyBatisDAO.consultaRutaHistoricoPorFechaFiltrada(disp.getNbTableDispPosici(),fechaInicio, fechaFin);
					listaEventos = dispositivoMyBatisDAO.consultaEventoRutaHistoricoPorFechaFiltrada(disp.getNbTableDispPosici(), fechaInicio, fechaFin);
				}else{
					listaCoords = dispositivoMyBatisDAO.consultaRutaHistoricoPorFecha(disp.getNbTableDispPosici(),fechaInicio, fechaFin);
					listaEventos = dispositivoMyBatisDAO.consultaEventoRutaHistoricoPorFecha(disp.getNbTableDispPosici(), fechaInicio, fechaFin);
				}
				rVO = new RutaVO();
				rVO.setFecha(fechaInicio);
				rVO.setCoords(convertCoordUtils.convertirCoordenadasVO(listaCoords));

				//Asigna los eventos correspondientes
				//listaEvento = asignarEventoPorTipoEvento(listaEventos);

				rVO.setEvents(listaEventos);
				if(!listaCoords.isEmpty())
					listaruta.add(rVO);

			} else if(tipoBusq == TipoBusquedaRuta.DIAS.getTipoBusquedaRuta()){
				//Busqueda por Dias
				listaFechas = convertCoordUtils.obtenerDiasPorRangoFecha(fechaInicio, fechaFin);

				for(String fecha:listaFechas)
				{
					if(filterByCero){
						listaCoords = dispositivoMyBatisDAO.consultaRutaHistoricoPorDiaFiltrada(disp.getNbTableDispPosici(),fecha);
						listaEventos = dispositivoMyBatisDAO.consultaEventoRutaHistoricoPorDiaFiltrada(disp.getNbTableDispPosici(), fecha);
					}else{
						listaCoords = dispositivoMyBatisDAO.consultaRutaHistoricoPorDia(disp.getNbTableDispPosici(),fecha);
						listaEventos = dispositivoMyBatisDAO.consultaEventoRutaHistoricoPorDia(disp.getNbTableDispPosici(), fecha);
					}

					if(listaCoords.size() > 0){
						rVO = new RutaVO();
						rVO.setFecha(fecha);
						rVO.setCoords(convertCoordUtils.convertirCoordenadasVO(listaCoords));

						//Asigna los eventos correspondientes
						//listaEvento = asignarEventoPorTipoEvento(listaEventos);
						/*for(Double[] d : rVO.getCoords())
						{
							listaEvento.add(0);
						}*/
						rVO.setEvents(listaEventos);
						if(!listaCoords.isEmpty())
							listaruta.add(rVO);
					}
				}
			}
		}/*else{
			listaCoords = dispositivoMyBatisDAO.consultaRutaHistorico();
			List<Double[]> listaCrds = convertCoordUtils.convertirCoordenadasVO(listaCoords);

			//Simular evento
			for(Double[] d : listaCrds)
			{
				listaEvento.add(0);
			}
			rVO = new RutaVO();
			rVO.setFecha("10/09/2018");
			rVO.setEvents(listaEvento);
			rVO.setCoords(listaCrds);

			listaruta.add(rVO);
		}*/

		dhVO.setId(disp.getIdDispositivo());
		dhVO.setNombre(disp.getNbDispositivo());
		dhVO.setRutas(listaruta);

		return dhVO;

		/*ObjectMapper objectMapper = new ObjectMapper();

		try {
			dhVO = objectMapper.readValue(JSON2, new TypeReference<DispositivoHistoricoVO>(){});
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return dhVO != null ? dhVO : null;*/
	}

	@Override
	@Transactional
	public DispositivoGuardarVO  guardarDispositivo(DispositivoGuardarVO dgVO, Long userId) throws RuntimeException
	{

		//Nombres de las tablas de posicion, evento y comando
		String tablaPosicion = "TMM_DP_DISP_#_POSICION";
		String tablaEvento = "TMM_DP_DISP_#_EVENTO";
		String tablaComando = "TMM_DP_DISP_#_COMANDO";

		/* Validaciones de datos del dispositivo */
		// Validacion de imei unico
		Long existeIMEI = dispositivoMyBatisDAO.existeIMEI(dgVO.getImei());

		if( existeIMEI == 0 ) {

			//Validacion de nombre de dispositivo, cd dispositivo

			DispositivoDTO nuevoDispositivo = new DispositivoDTO();
			nuevoDispositivo.setIdDispositivo(dispositivoMyBatisDAO.generarSiguienteId());

			//BUSCAR EL MODELO DE ACUERDO A LOS DATOS DEL FORMULARIO
			//Busca el tipo de dispositivo
			TipoDispositivoDTO tdDTO = tipoDispositivoDAO.buscarCatalogoPorTipo(dgVO.getTipo());

			//Busca la subTipo de dispositivo
			SubTipoDispositivoDTO subTipoDispositivoDTO = subTipoDispositivoDAO.buscarCatalogoPorTipoAndIdSubtipo(tdDTO,dgVO.getSubtipo());

			//Busca la marca de dispositivo
			List<MarcaDispositivoDTO> madDTO = marcaDispositivoDAO.buscarCatalogoPorTipo(new Long[]{dgVO.getMarca()}, tdDTO);

			//Busca el modelo de acuerdo a la marca y Subtipo
			List<ModeloDispositivoDTO> moDTO = modeloDispositivoDAO.buscarCatalogoPorTipo(madDTO.get(0),subTipoDispositivoDTO);

			nuevoDispositivo.setIdModelo(moDTO.get(0));
			nuevoDispositivo.setCdDispositivo(dgVO.getImei());
			nuevoDispositivo.setNbDispositivo(dgVO.getNombre());
			nuevoDispositivo.setNbTableDispPosici(tablaPosicion.replace("#", nuevoDispositivo.getIdDispositivo().toString()));
			nuevoDispositivo.setNbTableDispEvento(tablaEvento.replace("#", nuevoDispositivo.getIdDispositivo().toString()));
			nuevoDispositivo.setNbTableDispComand(tablaComando.replace("#", nuevoDispositivo.getIdDispositivo().toString()));
			nuevoDispositivo.setTxDispositivos(dgVO.getTxDispositivo());
			nuevoDispositivo.setFbDispositivo(null);
			nuevoDispositivo.setStActivo(Boolean.TRUE);
			nuevoDispositivo.setIdUsrCreacion(userId);
			nuevoDispositivo.setFhCreacion(new Date());
			nuevoDispositivo.setIdUsrModifica(userId);
			nuevoDispositivo.setFhModificacion(new Date());

			Serializable s = dispositivoDAO.save(nuevoDispositivo);

			if(s != null) {
				nuevoDispositivo.setIdDispositivo((Long)s);
				InformacionDispDTO info = new InformacionDispDTO();
				info.setIdInformacionDisp(dispositivoMyBatisDAO.generarSiguienteIdDispInfo());
				info.setIdDispositivo(nuevoDispositivo);
				info.setNuSerie(dgVO.getSerie());
				info.setNuCelularEmpresa(dgVO.getNuCelularEmpresa());
				info.setNuCelularPersonal(dgVO.getNuCelularPersonal());
				info.setNuImei( Long.valueOf(dgVO.getImei()));
				info.setNuIp(dgVO.getNuIp());

				info.setStActivo(Boolean.TRUE);
				info.setIdUsrCreacion(userId);
				info.setFhCreacion(new Date());
				info.setIdUsrModifica(userId);
				info.setFhModificacion(new Date());
				s = informacionDispDAO.save(info);

				if(s != null) {

					// relacion dispositivos - Empleados
					if(dgVO.getIdEmpleado() > 0) {
						DispoEmpleadoDTO dispoEmpleadoDTO = new DispoEmpleadoDTO();
						dispoEmpleadoDTO.setIdDispEmp(dispositivoMyBatisDAO.generarSiguienteIdDispEmpl());
						dispoEmpleadoDTO.setIdDispositivo(nuevoDispositivo);
						dispoEmpleadoDTO.setIdEmpleado(empleadoDAO.findOne(dgVO.getIdEmpleado()));

						dispoEmpleadoDTO.setStActivo(Boolean.TRUE);
						dispoEmpleadoDTO.setIdUsrCreacion(userId);
						dispoEmpleadoDTO.setFhCreacion(new Date());
						dispoEmpleadoDTO.setIdUsrModifica(userId);
						dispoEmpleadoDTO.setFhModificacion(new Date());

						s = dispoEmpleadoDAO.save(dispoEmpleadoDTO);
					}

					dgVO.setIdDispositivo(nuevoDispositivo.getIdDispositivo());
					return dgVO;
				}
			}
		}
		return null;
	}


	@Override
	@Transactional
	public DispositivoGuardarVO  actualizarDispositivo(DispositivoGuardarVO dgVO, Long userId) throws RuntimeException
	{

		DispositivoDTO dispositivoDTO = dispositivoDAO.buscarDispositivosPorId(dgVO.getIdDispositivo());
		InformacionDispDTO  informacionDispDTO = dispositivoDTO.getIdInformacionDisp();

		// validar cambio de IMEI
		String oldIMEI = dispositivoDTO.getCdDispositivo();

		// validar si es necesario cambiar IMEI
		if(!oldIMEI.equals(dgVO.getImei())) {
			if( dispositivoMyBatisDAO.existeIMEI(dgVO.getImei()) !=0 )
				return null;
		}

		//BUSCAR EL MODELO DE ACUERDO A LOS DATOS DEL FORMULARIO
		//Busca el tipo de dispositivo
		TipoDispositivoDTO tdDTO = tipoDispositivoDAO.buscarCatalogoPorTipo(dgVO.getTipo());

		//Busca la subTipo de dispositivo
		SubTipoDispositivoDTO subTipoDispositivoDTO = subTipoDispositivoDAO.buscarCatalogoPorTipoAndIdSubtipo(tdDTO,dgVO.getSubtipo());

		//Busca la marca de dispositivo
		List<MarcaDispositivoDTO> madDTO = marcaDispositivoDAO.buscarCatalogoPorTipo(new Long[]{dgVO.getMarca()}, tdDTO);

		//Busca el modelo de acuerdo a la marca y Subtipo
		List<ModeloDispositivoDTO> moDTO = modeloDispositivoDAO.buscarCatalogoPorTipo(madDTO.get(0),subTipoDispositivoDTO);

		dispositivoDTO.setIdModelo(moDTO.get(0));
		dispositivoDTO.setCdDispositivo(dgVO.getImei());
		dispositivoDTO.setNbDispositivo(dgVO.getNombre());
		dispositivoDTO.setTxDispositivos(dgVO.getTxDispositivo());
		dispositivoDTO.setFhModificacion(new Date());

		Serializable s = dispositivoDAO.save(dispositivoDTO);

		if(s != null) {
			informacionDispDTO.setNuImei( Long.valueOf(dgVO.getImei()));
			informacionDispDTO.setNuIp(dgVO.getNuIp());
			informacionDispDTO.setNuCelularEmpresa(dgVO.getNuCelularEmpresa());
			informacionDispDTO.setNuCelularPersonal(dgVO.getNuCelularPersonal());
			informacionDispDTO.setNuSerie(dgVO.getSerie());
			informacionDispDTO.setFhModificacion(new Date());
			s = informacionDispDAO.save(informacionDispDTO);
			if(s != null) {

				DispoEmpleadoDTO dispoEmpleadoDTO = dispoEmpleadoDAO.buscarRelacionDispoEmpleadoPorDispositivo(dispositivoDTO);

				// Eliminar empleado
				if(dgVO.getIdEmpleado() == null && dispoEmpleadoDTO != null) {
					dispoEmpleadoDAO.delete(dispoEmpleadoDTO);
				}

				// Cambiar empleado
				else if(dgVO.getIdEmpleado() != null && dispoEmpleadoDTO != null ) {

					// obtener empleado anteiror
					Long oldIdEmpleado = null;
					EmpleadoDTO empleadoDTO = dispoEmpleadoDTO.getIdEmpleado();
					if(empleadoDTO.getIdEmpleado() != null) {
						oldIdEmpleado = empleadoDTO.getIdEmpleado();
					}

					// relacion dispositivos - Empleados
					// solo si es un nuevo empleado
					if(oldIdEmpleado == null || oldIdEmpleado != dgVO.getIdEmpleado()) {
						dispoEmpleadoDTO.setIdEmpleado(empleadoDAO.findOne(dgVO.getIdEmpleado()));
						dispoEmpleadoDTO.setFhModificacion(new Date());
						s = dispoEmpleadoDAO.save(dispoEmpleadoDTO);
					}
				}

				// Agregar empleado
				else if(dgVO.getIdEmpleado() != null && dispoEmpleadoDTO == null) {

					dispoEmpleadoDTO = new DispoEmpleadoDTO();
					dispoEmpleadoDTO.setIdDispEmp(dispositivoMyBatisDAO.generarSiguienteIdDispEmpl());
					dispoEmpleadoDTO.setIdDispositivo(dispositivoDTO);
					dispoEmpleadoDTO.setIdEmpleado(empleadoDAO.findOne(dgVO.getIdEmpleado()));

					dispoEmpleadoDTO.setStActivo(Boolean.TRUE);
					dispoEmpleadoDTO.setIdUsrCreacion(userId);
					dispoEmpleadoDTO.setFhCreacion(new Date());
					dispoEmpleadoDTO.setIdUsrModifica(userId);
					dispoEmpleadoDTO.setFhModificacion(new Date());

					s = dispoEmpleadoDAO.save(dispoEmpleadoDTO);
				}

				return dgVO;
			}
		}

		return null;
	}



	@Override
	@Transactional
	public Boolean crearTablasDispositivo(Long idDispositivo)
	{
		return dispositivoDAO.crearTablasDispositivo(idDispositivo);
	}



	@Override
	@Transactional(readOnly=true)
	public DispositivoRTDetalleVO cargarDetalleDispositivo(Long id)
	{
		DispositivoRTDetalleVO drtdVO = new DispositivoRTDetalleVO();

		DispositivoDTO dDTO = dispositivoDAO.buscarDispositivosPorId(id);
		DispoEmpleadoDTO deDTO = dispoEmpleadoDAO.buscarRelacionDispoEmpleadoPorDispositivo(dDTO);

		List<PosicionActualDTO> posActDTO = posicionActualDAO.obtenerCoordenadaActual(dDTO);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		drtdVO.setNombre(dDTO.getNbDispositivo());
		drtdVO.setImei(dDTO.getCdDispositivo());
		drtdVO.setDescripcion(dDTO.getTxDispositivos());
		if(deDTO != null){
			drtdVO.setUsuario(deDTO.getIdEmpleado().getNbEmpleado()+" "+deDTO.getIdEmpleado().getNbPaterno()+" "+deDTO.getIdEmpleado().getNbMaterno());
			drtdVO.setCodigo(deDTO.getIdEmpleado().getCdEmpleado());
			drtdVO.setCorreo(deDTO.getIdEmpleado().getNbMail());
			drtdVO.setTelefono(deDTO.getIdEmpleado().getNuTelefono().toString());
		}else{
			drtdVO.setUsuario("Sin usuario asociado");
			drtdVO.setCodigo("N/A");
			drtdVO.setCorreo("N/A");
			drtdVO.setTelefono("N/A");
		}
		drtdVO.setActividad(sdf.format(posActDTO.get(0).getFhRegistro()));
		drtdVO.setDireccion(posActDTO.get(0).getTxDireccion());
		drtdVO.setLat(String.format("%1$,.6f", posActDTO.get(0).getNuLatitud()));
		drtdVO.setLon(String.format("%1$,.6f", posActDTO.get(0).getNuLongitud()));
		return drtdVO;
	}

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Transactional(readOnly=true)
	public Map generarReporteRutaHistorico(DispositivoHistoricoVO dispHistorico, String fechaInicio, String fechaFin){

		Map response = new HashMap();
		ByteArrayOutputStream reporte = new ByteArrayOutputStream();

		PeticioReporteVO peticionReporteVO = new PeticioReporteVO();
		PropiedadesReporte propiedadesReporte = new PropiedadesReporte();
		PeticionReporteBOImpl peticionReporteBOImpl = new PeticionReporteBOImpl();

		List<EventoVO> listaEventos = dispHistorico.getRutas().get(0).getEvents();
		DispositivoDTO dispo = dispositivoDAO.buscarDispositivosPorId(dispHistorico.getId());
		DispoEmpleadoDTO dispoEmp = dispoEmpleadoDAO.buscarRelacionDispoEmpleadoPorDispositivo(dispo);

		// Resultados de la tabla
		List<Object> contenido = new ArrayList<Object>();
		List<Object> contenido1 = new ArrayList<Object>();

		// Leyendas de las columnas de las tablas
		List<Object> encabezadoTitulo = new ArrayList<Object>();
		List<String> titulos = new ArrayList<String>();
		List<String> subtitulos = new ArrayList<String>();

		//Titulos de cada columna
		titulos.add("Latitud");
		titulos.add("Longitud");
		titulos.add("Dirección");
		titulos.add("Fecha de Registro");
		titulos.add("Tipo");
		titulos.add("Evento");
		titulos.add("Enlace");

		encabezadoTitulo.add(titulos);

		subtitulos.add("Dispositivo: "+dispHistorico.getNombre());
		subtitulos.add("Propietario: "+dispoEmp.getIdEmpleado().getNbEmpleado()+" "+dispoEmp.getIdEmpleado().getNbPaterno()+" "+dispoEmp.getIdEmpleado().getNbMaterno());

		propiedadesReporte.setFechaI(fechaInicio);
		propiedadesReporte.setFechaF(fechaFin);

		String namereal = "ReporteRutasHistoricas"+dispHistorico.getId().toString();

		propiedadesReporte.setNombreReporte(namereal);
		propiedadesReporte.setTituloExcel("Reporte de Rutas Historicas");
		propiedadesReporte.setExtencionArchvio(".xls");

		//Cuerpo del reporte
		List<String> listaContenido1;
		if (dispHistorico != null) {

			for (int i = 0;i<listaEventos.size();i++) {
				listaContenido1 = new ArrayList<String>();
				listaContenido1.add(listaEventos.get(i).getLat());
				listaContenido1.add(listaEventos.get(i).getLng());
				listaContenido1.add(listaEventos.get(i).getDireccion());
				listaContenido1.add(listaEventos.get(i).getFecha());
				listaContenido1.add(listaEventos.get(i).getTipo());
				listaContenido1.add(listaEventos.get(i).getDescripcion());
				listaContenido1.add("https://www.google.com/maps/search/?api=1&query="+listaEventos.get(i).getLat().toString()+","+listaEventos.get(i).getLng().toString());
				contenido1.add(listaContenido1);
			}

			contenido.add(contenido1);

			propiedadesReporte.setSubtitulos(subtitulos);
			peticionReporteVO.setPropiedadesReporte(propiedadesReporte);
			peticionReporteVO.setEncabezado(encabezadoTitulo);
			peticionReporteVO.setContenido(contenido);

			try {
				reporte = peticionReporteBOImpl.generaReporteExcel(peticionReporteVO);
				response.put("archivo", reporte);

				response.put("nombre", propiedadesReporte.getNombreReporte());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return response;
	}

	private List<Double[]> obtenerCoordenadaActualPorDispositivo(DispositivoDTO dispoDTO) throws NotFoundException
	{
		List<Double[]> listaCoords = new ArrayList<Double[]>();
		List<PosicionActualDTO> posActDTO = posicionActualDAO.obtenerCoordenadaActual(dispoDTO);

		if(posActDTO != null){
			Double [] coords = new Double[]{posActDTO.get(0).getNuLatitud(), posActDTO.get(0).getNuLongitud()};
			listaCoords.add(coords);
		}else{
			throw new NotFoundException("No existe ningun registro capturado.");
		}

		return listaCoords;
	}

	private String obtenerNombreEmpleado(DispositivoDTO dispoDTO){
		//Busca el idEmpleado asociado al Dispositivo
		DispoEmpleadoDTO  deDTO = dispoEmpleadoDAO.buscarRelacionDispoEmpleadoPorDispositivo(dispoDTO);

		return deDTO.getIdEmpleado().getNbEmpleado()+" "+deDTO.getIdEmpleado().getNbPaterno()+" "+deDTO.getIdEmpleado().getNbMaterno();
	}

	private String obtenerImagenPorIndicador(Long diferenciaTiempo){
		List<IndicadorEventoDTO> listaEventos = indicadorEventoDAO.buscarIndicadores();
		for(IndicadorEventoDTO evento:listaEventos)
		{
			if(Integer.parseInt(diferenciaTiempo.toString())>=evento.getNuTiempoInicial() &&
					Integer.parseInt(diferenciaTiempo.toString())<=evento.getNuTiempoFinal())
			{
				return evento.getCdIndicador();
			}
		}
		//return "#607D8B";
		return listaEventos.get(listaEventos.size()-1).getCdIndicador();
	}

	private List<Integer> asignarEventoPorTipoEvento(List<Long> listaEventos){
		List<Integer> listaTipoEventos = Arrays.asList(new Integer[listaEventos.size()]);
		//List<Long> listaTmp = new ArrayList<Long>(Arrays.asList(listaEventos));

		List<EventoDTO> listaTipos = eventoDAO.buscarCatalogoEventos();

		int contador = 0;

		for(EventoDTO id : listaTipos)
		{
			contador = 0;

			/*if(listaTipoEventos.size() == listaEventos.size())
				break;*/

			while(contador<listaEventos.size()){
				if(id.getIdEvento() == listaEventos.get(contador))
					listaTipoEventos.set(contador, Integer.parseInt(id.getIdTipoEvento().getIdTipoEvento().toString()));
				contador++;
			}
		}

		return listaTipoEventos;
	}

	@Transactional(readOnly=true)
	@Override
	public List<InfoDispositivoVO> infoDispositivos() {
		List<InfoDispositivoVO> listRet = new ArrayList<>();
		List<DispositivoDTO> lista = dispositivoDAO.findAll();
		EmpleadoDTO empl;
		DispoEmpleadoDTO disEmpl;
		List<PosicionActualDTO> paDTO;
		GrupoDispositivoDTO gdDTO = null;

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY H:mm");

		for(DispositivoDTO disp : lista) {

			disEmpl = dispoEmpleadoDAO.buscarRelacionDispoEmpleadoPorDispositivo(disp);
			paDTO = posicionActualDAO.obtenerCoordenadaActual(disp);
			List<GrupoDispositivoDTO> listaGDDTO = grupoDispositivoDAO.buscarGruposPorDispositivo(disp.getIdDispositivo());

			if(listaGDDTO.size()>0){
				gdDTO = listaGDDTO.get(0);
			}

			InfoDispositivoVO idVO = new InfoDispositivoVO();
			idVO.setIdDispositivo(disp.getIdDispositivo());
			idVO.setIdGrupo(gdDTO != null ? listaGDDTO.size()>1 ? 0L: gdDTO.getIdDispGrupo() : 0L);
			idVO.setNbGrupo(gdDTO != null ? listaGDDTO.size()>1 ? "Mas de un grupo" :gdDTO.getIdGrupo().getNbGrupo() : "Sin Grupo");
			idVO.setNbDispositivo(disp.getNbDispositivo());
			idVO.setNbEmplead(disEmpl != null ? disEmpl.getIdEmpleado().getNbEmpleado() + " " + disEmpl.getIdEmpleado().getNbPaterno() + " " + disEmpl.getIdEmpleado().getNbMaterno():"Sin Asignar");
			idVO.setNbUbicacion(!paDTO.isEmpty() ? paDTO.get(0).getTxDireccion(): "Sin registro");
			idVO.setFhRegistro(!paDTO.isEmpty() ? sdf.format(paDTO.get(0).getFhRegistro()): "Sin registro");
			idVO.setStActivo(disp.getStActivo());
			listRet.add(idVO);

		}
		return listRet;
	}

	@Transactional(readOnly=true)
	@Override
	public List<InfoDispositivoVO> infoDispositivosDiarios() {
		List<InfoDispositivoVO> listRet = new ArrayList<>();
		List<PosicionActualDTO> lista = posicionActualDAO.findAll();
		EmpleadoDTO empl;
		DispoEmpleadoDTO disEmpl;
		GrupoDispositivoDTO gdDTO = null;

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY H:mm");

		for(PosicionActualDTO disp : lista) {

			disEmpl = dispoEmpleadoDAO.buscarRelacionDispoEmpleadoPorDispositivo(disp.getIdDispositivo());
			List<GrupoDispositivoDTO> listaGDDTO = grupoDispositivoDAO.buscarGruposPorDispositivo(disp.getIdDispositivo().getIdDispositivo());

			if(listaGDDTO.size()>0){
				gdDTO = listaGDDTO.get(0);
			}

			InfoDispositivoVO idVO = new InfoDispositivoVO();
			idVO.setIdDispositivo(disp.getIdDispositivo().getIdDispositivo());
			idVO.setIdGrupo(gdDTO != null ? listaGDDTO.size()>1 ? 0L: gdDTO.getIdDispGrupo() : 0L);
			idVO.setNbGrupo(gdDTO != null ? listaGDDTO.size()>1 ? "Mas de un grupo" :gdDTO.getIdGrupo().getNbGrupo() : "Sin Grupo");
			idVO.setNbDispositivo(disp.getIdDispositivo().getNbDispositivo());
			idVO.setNbEmplead(disEmpl != null ? disEmpl.getIdEmpleado().getNbEmpleado() + " " + disEmpl.getIdEmpleado().getNbPaterno() + " " + disEmpl.getIdEmpleado().getNbMaterno():"Sin Asignar");
			idVO.setFhRegistro(sdf.format(disp.getFhRegistro()));
			idVO.setNbUbicacion(disp.getTxDireccion());
			idVO.setStActivo(disp.getStActivo());
			listRet.add(idVO);

		}
		return listRet;
	}

	@Transactional(readOnly=true)
	@Override
	public List<DispositivoTableVO> buscarDispositivosPorTipo(String tipo, String valor){

		List<DispositivoDTO> dispositivoDTOs = dispositivoDAO.buscarDispositivosPorTipo(tipo, valor);
		List<DispositivoTableVO> dispositivoTableVOs = new ArrayList<>();

		for (DispositivoDTO dispositivoDTO : dispositivoDTOs) {
			DispositivoTableVO vo = new DispositivoTableVO();

			vo.setId(dispositivoDTO.getIdDispositivo());
			vo.setNombre(dispositivoDTO.getNbDispositivo());
			vo.setImei(dispositivoDTO.getCdDispositivo());
			vo.setMarca(dispositivoDTO.getIdModelo().getIdMarca().getNbMarca());
			vo.setModelo(dispositivoDTO.getIdModelo().getNbModelo());
			vo.setActive(dispositivoDTO.getStActivo());

			InformacionDispDTO informacionDispDTO =dispositivoDTO.getIdInformacionDisp();
			if(informacionDispDTO != null) {
				vo.setIp(informacionDispDTO.getNuIp() != null ? informacionDispDTO.getNuIp() : "");
				vo.setCelular(informacionDispDTO.getNuCelularPersonal() != null ? informacionDispDTO.getNuCelularPersonal() : "");
			}

			DispoEmpleadoDTO disEmpl = dispoEmpleadoDAO.buscarRelacionDispoEmpleadoPorDispositivo(dispositivoDTO);
			if(disEmpl != null) {
				EmpleadoDTO empleadoDTO = disEmpl.getIdEmpleado();
				if(empleadoDTO != null) {
					vo.setUsuario(""+empleadoDTO.getNbEmpleado()+" "+empleadoDTO.getNbPaterno()+" "+empleadoDTO.getNbMaterno());
					vo.setEmail(empleadoDTO.getNbMail() != null ? empleadoDTO.getNbMail() : "");
				}
			}

			dispositivoTableVOs.add(vo);
		}


		return dispositivoTableVOs;
	}


	@Transactional(readOnly=true)
	@Override
	public List<DispositivoSubtipoVO> buscarDispositivos(){

		List<DispositivoDTO> dispositivoDTOs = dispositivoDAO.buscarDispositivos();
		List<DispositivoSubtipoVO> dispositivoTableVOs = new ArrayList<>();

		for (DispositivoDTO dispositivoDTO : dispositivoDTOs) {
			DispositivoSubtipoVO vo = new DispositivoSubtipoVO();

			vo.setId(dispositivoDTO.getIdDispositivo());
			vo.setNombre(dispositivoDTO.getNbDispositivo());

			dispositivoTableVOs.add(vo);
		}


		return dispositivoTableVOs;
	}

	@Transactional(readOnly=true)
	@Override
	public DispositivoGuardarVO buscarDispositivoPorId(Long id){

		Long[] ids = new Long[] {new Long(id)};
		DispositivoDTO dispDTO = dispositivoDAO.buscarDispositivosPorId(ids).get(0);
		DispoEmpleadoDTO deDTO = dispoEmpleadoDAO.buscarRelacionDispoEmpleadoPorDispositivo(dispDTO);

		DispositivoGuardarVO dispositivoGuardarVO = new DispositivoGuardarVO();
		dispositivoGuardarVO.setIdDispositivo(dispDTO.getIdDispositivo());
		dispositivoGuardarVO.setNombre(dispDTO.getNbDispositivo());


		dispositivoGuardarVO.setTipo(dispDTO.getIdModelo().getIdSubtipoDispositivo().getIdTipoDispositivo().getIdTipoDispositivo());
		dispositivoGuardarVO.setSubtipo(dispDTO.getIdModelo().getIdSubtipoDispositivo().getIdSubTipoDispositivo());
		dispositivoGuardarVO.setMarca(dispDTO.getIdModelo().getIdMarca().getIdMarca());
		dispositivoGuardarVO.setModelo(dispDTO.getIdModelo().getIdModelo());
		if(deDTO != null) {
			dispositivoGuardarVO.setIdEmpleado(deDTO.getIdEmpleado().getIdEmpleado());
		}


		dispositivoGuardarVO.setImei(dispDTO.getCdDispositivo());
		dispositivoGuardarVO.setNuIp(dispDTO.getIdInformacionDisp().getNuIp());
		dispositivoGuardarVO.setTxDispositivo(dispDTO.getTxDispositivos());
		dispositivoGuardarVO.setNuCelularEmpresa(dispDTO.getIdInformacionDisp().getNuCelularEmpresa());
		dispositivoGuardarVO.setNuCelularPersonal(dispDTO.getIdInformacionDisp().getNuCelularPersonal());
		dispositivoGuardarVO.setSerie(dispDTO.getIdInformacionDisp().getNuSerie());
		if(deDTO != null) {
			dispositivoGuardarVO.setNombreEmpleado(deDTO.getIdEmpleado().getNbEmpleado()+ " "+deDTO.getIdEmpleado().getNbPaterno() + " "+deDTO.getIdEmpleado().getNbMaterno());
		}


		return dispositivoGuardarVO;
	}

	@Transactional
	@Override
	public Boolean activarDesactivarDispositivo(Long id,boolean activar) {
		Long[] ids = new Long[] {new Long(id)};
		DispositivoDTO dispDTO = dispositivoDAO.buscarDispositivosPorId(ids).get(0);
		dispDTO.setStActivo(activar);
		Serializable s = dispositivoDAO.save(dispDTO);

		if(s != null) {
			return true;
		}

		return false;
	}

	@Transactional
	@Override
	public Boolean eliminarDispositivo(Long id) {
		Long[] ids = new Long[] {new Long(id)};
		DispositivoDTO dispositivoDTO = dispositivoDAO.buscarDispositivosPorId(ids).get(0);

		if(dispositivoDTO != null) {
			try {
				// TMM053D_DP_INFORMACION_DISP
				InformacionDispDTO informacionDispDTO = dispositivoDTO.getIdInformacionDisp();
				if(informacionDispDTO != null) {
					informacionDispDAO.delete(informacionDispDTO);
				}

				// TMM016D_DP_DISP_EMPLEADOS
				DispoEmpleadoDTO dispoEmpleadoDTO = dispoEmpleadoDAO.buscarRelacionDispoEmpleadoPorDispositivo(dispositivoDTO);
				if(dispoEmpleadoDTO != null) {
					dispoEmpleadoDAO.delete(dispoEmpleadoDTO);
				}

				// TMM021D_DP_DISPOSITIVO_GRUPOS
				GrupoDispositivoDTO grupoDispositivoDTO = grupoDispositivoDAO.buscarGrupoDeDispositivo(dispositivoDTO);
				if(grupoDispositivoDTO != null) {
					grupoDispositivoDAO.delete(grupoDispositivoDTO);
				}

				// TMM054D_GL_POSICIONES_ACT
				List<PosicionActualDTO> posicionActualDTOs= posicionActualDAO.obtenerCoordenadaActual(dispositivoDTO);
				if(!posicionActualDTOs.isEmpty()) {
					for(PosicionActualDTO p : posicionActualDTOs) {
						posicionActualDAO.delete(p);
					}
				}

				// TMM014D_DP_DISP_GEOCERCAS
				List<GeocercaDispositivoDTO> geocercaDispositivoDTOs = geocercaDispositivoDAO.buscarDispositivosEnGeocerca(dispositivoDTO);
				if(!geocercaDispositivoDTOs.isEmpty()) {
					for(GeocercaDispositivoDTO g : geocercaDispositivoDTOs ) {
						geocercaDispositivoDAO.delete(g);
					}
				}

				// comentar la eliminacion de esta tabla TMM026D_DP_DISP_COMANDO

				// TMM011D_DP_DISPOSITIVOS
				dispositivoDAO.delete(dispositivoDTO);

				dispositivoDAO.dropTableNbTableDispPosici(dispositivoDTO.getNbTableDispPosici());
				dispositivoDAO.dropTableNbTableDispEvento(dispositivoDTO.getNbTableDispEvento());
				dispositivoDAO.dropTableNbTableDispComand(dispositivoDTO.getNbTableDispComand());

				return true;

			}catch (HibernateException e) {
				e.printStackTrace();
	        }
		}
		return false;
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<ConfigMonitoreoDispoVO> buscarParamConfigMonitoreo(Long imei){
		DispositivoDTO dispositivo;
		dispositivo = dispositivosDAO.findByImei(imei);
		if(dispositivo != null){
			
			//DAO PARA OBTENER LOS DATOS DE BD
			List<ConfigMonitoreoDispoVO> listConf = new ArrayList<>();
			
			ConfigMonitoreoDispoVO confgMonDisp = new ConfigMonitoreoDispoVO();
			confgMonDisp.setCdParamMonitoreo("key_interval_location_update");//TIEMPO DE OBTEBER LA COORDENADA EN MINUTOS
			confgMonDisp.setValorParamMonotoreo("5");
			confgMonDisp.setIdDispoSitivo(dispositivo.getIdDispositivo());
			confgMonDisp.setNbDispositivo(dispositivo.getNbDispositivo());
			listConf.add(confgMonDisp);
			
			confgMonDisp= new ConfigMonitoreoDispoVO();
			confgMonDisp.setCdParamMonitoreo("key_distance_location_update");// METROS PARA OBTENER LA COORDENADA EN METROS
			confgMonDisp.setValorParamMonotoreo("15");
			confgMonDisp.setIdDispoSitivo(dispositivo.getIdDispositivo());
			confgMonDisp.setNbDispositivo(dispositivo.getNbDispositivo());
			listConf.add(confgMonDisp);
			return listConf;
		}
		
		return null;
	}

}
