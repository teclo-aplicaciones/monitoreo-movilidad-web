package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.GeocercaDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.GeocercaNotificacionDTO;

public interface GeocercaNotificacionDAO extends BaseDAO<GeocercaNotificacionDTO>{
	public Long buscarSiguienteIdentificador();	
	public List<GeocercaNotificacionDTO> buscaRelacionGeocercaNotificacionPorGeocerca(GeocercaDTO gDTO);
	public Boolean eliminaRelacionGeocercaNotificacionPorGeocerca(GeocercaDTO gDTO);
}
