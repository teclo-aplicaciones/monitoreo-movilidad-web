package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.MedioComunicacionDTO;
@SuppressWarnings("unchecked")
@Repository("medioComunicacionDAO")
public class MedioComunicacionDAOImpl extends BaseDAOImpl<MedioComunicacionDTO> implements MedioComunicacionDAO{
	
	
	@Override
	@Transactional(readOnly = true)
	public List<MedioComunicacionDTO> obtenerCatalogo() {
		Criteria query = getCurrentSession().createCriteria(MedioComunicacionDTO.class);	
		query.add(Restrictions.eq("stActivo", true));				
		return (List<MedioComunicacionDTO>)query.list();		
	}
	
	@Override
	public MedioComunicacionDTO buscarMedioPorId(Long id){
		Criteria query = getCurrentSession().createCriteria(MedioComunicacionDTO.class);
		query.add(Restrictions.eq("idMedio", id));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		
		return (MedioComunicacionDTO) query.uniqueResult();
	}
}
