package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.BitacoraComandoDetalleDTO;


public interface BitacoraComandoDetalleDAO extends BaseDAO<BitacoraComandoDetalleDTO>{
	public List<BitacoraComandoDetalleDTO> obtenerBitacoraComandoDetalle(Long idBitacora);
}
