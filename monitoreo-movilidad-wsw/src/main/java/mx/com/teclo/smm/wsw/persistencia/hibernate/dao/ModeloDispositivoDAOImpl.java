package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.MarcaDispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.ModeloDispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.SubTipoDispositivoDTO;

@SuppressWarnings("unchecked")
@Repository("modeloDispositivoDAO")
public class ModeloDispositivoDAOImpl extends BaseDAOImpl<ModeloDispositivoDTO> implements ModeloDispositivoDAO{

	@Override
	public List<ModeloDispositivoDTO> buscarCatalogoPorTipo(MarcaDispositivoDTO mdDTO, SubTipoDispositivoDTO stdDTO)
	{
		Criteria query = getCurrentSession().createCriteria(ModeloDispositivoDTO.class);
		query.add(Restrictions.eq("idMarca", mdDTO));
		query.add(Restrictions.eq("idSubtipoDispositivo", stdDTO));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return (List<ModeloDispositivoDTO>) query.list();
	}
	
	@Override
	public List<ModeloDispositivoDTO> buscarCatalogoPorSubTipo(Long subTipoDispositivo ){
		Criteria query = getCurrentSession().createCriteria(ModeloDispositivoDTO.class);		
		query.add(Restrictions.eq("idSubtipoDispositivo.idSubTipoDispositivo", subTipoDispositivo));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return (List<ModeloDispositivoDTO>) query.list();
	}
	
	@Override
	public List<ModeloDispositivoDTO> catalogoPorMarca(Long idMarca ){
		Criteria query = getCurrentSession().createCriteria(ModeloDispositivoDTO.class);		
		query.add(Restrictions.eq("idMarca.idMarca", idMarca));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return (List<ModeloDispositivoDTO>) query.list();
	}
	
	@Override	
	public List<ModeloDispositivoDTO> obtenerModelos() {		
		Criteria query = getCurrentSession().createCriteria(ModeloDispositivoDTO.class);
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return (List<ModeloDispositivoDTO>)query.list();
	}
	
	@Override	
	public List<ModeloDispositivoDTO> activarDesactivarObtenerModelos() {		
		Criteria query = getCurrentSession().createCriteria(ModeloDispositivoDTO.class);	
		query.addOrder(Order.asc("idModelo"));
		return (List<ModeloDispositivoDTO>)query.list();
	}
	
	@Override	
	public ModeloDispositivoDTO obtenerModelosId(long id) {
		Criteria query = getCurrentSession().createCriteria(ModeloDispositivoDTO.class);
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		query.add(Restrictions.eq("idModelo", id));
		return (ModeloDispositivoDTO)query.uniqueResult();
	}
	
	@Override	
	public ModeloDispositivoDTO activarDesactivarObtenerModelosId(long id) {
		Criteria query = getCurrentSession().createCriteria(ModeloDispositivoDTO.class);		
		query.add(Restrictions.eq("idModelo", id));
		return (ModeloDispositivoDTO)query.uniqueResult();
	}
}
