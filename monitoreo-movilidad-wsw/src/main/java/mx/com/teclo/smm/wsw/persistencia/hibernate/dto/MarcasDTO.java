package mx.com.teclo.smm.wsw.persistencia.hibernate.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TMM004C_CT_MARCAS")
public class MarcasDTO implements Serializable{

	private static final long serialVersionUID = 3503961843873980668L;
	@Id
	@Column(name = "ID_MARCA")
	private Long idMarca;
	@ManyToOne
    @JoinColumn(name="ID_TIPO_DISPOSITIVO")
	private TipoDispositivosDTO idTipoDispositivo;
	@Column(name = "CD_MARCA")
	private String cdMarca;
	@Column(name = "NB_MARCA")
	private String nbMarca;
	@Column(name = "TX_MARCA")
	private String txMarca;
	
	@Column(name = "ST_ACTIVO")
	private Boolean stActivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;
	
	public Long getIdMarca() {
		return idMarca;
	}
	public void setIdMarca(Long idMarca) {
		this.idMarca = idMarca;
	}
	public TipoDispositivosDTO getIdTipoDispositivo() {
		return idTipoDispositivo;
	}
	public void setIdTipoDispositivo(TipoDispositivosDTO idTipoDispositivo) {
		this.idTipoDispositivo = idTipoDispositivo;
	}
	public String getCdMarca() {
		return cdMarca;
	}
	public void setCdMarca(String cdMarca) {
		this.cdMarca = cdMarca;
	}
	public String getNbMarca() {
		return nbMarca;
	}
	public void setNbMarca(String nbMarca) {
		this.nbMarca = nbMarca;
	}
	public String getTxMarca() {
		return txMarca;
	}
	public void setTxMarca(String txMarca) {
		this.txMarca = txMarca;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
	@Override
	public String toString() {
		return "MarcasDTO [idMarca=" + idMarca + ", idTipoDispositivo=" + idTipoDispositivo + ", cdMarca=" + cdMarca
				+ ", nbMarca=" + nbMarca + ", txMarca=" + txMarca + ", stActivo=" + stActivo + ", fhCreacion="
				+ fhCreacion + ", idUsrCreacion=" + idUsrCreacion + ", fhModificacion=" + fhModificacion
				+ ", idUsrModifica=" + idUsrModifica + "]";
	}	
	
	
	
}
