package mx.com.teclo.smm.wsw.util.enumerable;

import java.util.ArrayList;
import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.vo.catalogo.CatalogoVO;

public enum TipoGeocercaEnum {
	CIRCULAR(1L, "Circular"), POLYLINEAL(2L, "Lineal"), POLYGONAL(3L, "Poligonal");
	
	private Long  tipoGeocerca;
	private String nombreGeocerca;
	
	private TipoGeocercaEnum(Long tipoGeocerca, String nombreGeocerca){
		this.tipoGeocerca = tipoGeocerca;
		this.nombreGeocerca = nombreGeocerca;
	}
	
	public Long getTipoGeocerca()
	{
		return tipoGeocerca;
	}
	
	public void setTipoGeocerca(Long tipoGeocerca){
		this.tipoGeocerca = tipoGeocerca;
	}
	
	public String getNombreGeocerca()
	{
		return nombreGeocerca;
	}
	
	public void setNombreGeocerca(String nombreGeocerca){
		this.nombreGeocerca = nombreGeocerca;
	}
	
	public static List<CatalogoVO> getCatalogo(){
		TipoGeocercaEnum[] listaEnum = TipoGeocercaEnum.values();
		List<CatalogoVO> listaCatVO = new ArrayList<CatalogoVO>(); 
		
		for(TipoGeocercaEnum cat : listaEnum)
		{
			CatalogoVO catVO = new CatalogoVO();
			catVO.setId(cat.getTipoGeocerca());
			catVO.setNombre(cat.getNombreGeocerca());
			listaCatVO.add(catVO);
		}
		return listaCatVO;
	}
}