package mx.com.teclo.smm.wsw.negocio.service.empresa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.smm.wsw.persistencia.hibernate.dao.EmpresaUsuarioDAO;

/**
 * @author sinuhe
 *
 */
@Service
public class EmpresaServiceImpl implements EmpresaService {

	@Autowired
	private EmpresaUsuarioDAO empresaUsuarioDAO;

	private long idEmpresa;

	@Override
	public long getIdEmpresa() {
		return idEmpresa;
	}

	@Override
	@Transactional(readOnly=true)
	public Long buscarEmpresaUsuario(Long idUsuario) {
		Long idEmpresaByUsuario =empresaUsuarioDAO.findEmpresaByUsuario(idUsuario);
		this.idEmpresa = idEmpresaByUsuario;
		return idEmpresaByUsuario;
	}

}
