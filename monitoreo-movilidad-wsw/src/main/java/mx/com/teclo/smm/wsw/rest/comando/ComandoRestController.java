package mx.com.teclo.smm.wsw.rest.comando;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import mx.com.teclo.arquitectura.ortogonales.exception.NotFoundException;
import mx.com.teclo.arquitectura.ortogonales.responsehttp.ConflictHttpResponse;
import mx.com.teclo.arquitectura.ortogonales.responsehttp.OKHttpResponse;
import mx.com.teclo.smm.wsw.negocio.service.comando.BitacoraComandoDetalleService;
import mx.com.teclo.smm.wsw.negocio.service.comando.ComandoService;
import mx.com.teclo.smm.wsw.negocio.service.dispositivo.DispositivoService;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.BitacoraComandoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.BitacoraComandoDetalleArchivoDTO;
import mx.com.teclo.smm.wsw.persistencia.vo.PosicionesActVO;
import mx.com.teclo.smm.wsw.persistencia.vo.comando.ComandoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.comando.LogComandoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.dispositivo.DispositivoVO;

@RestController
public class ComandoRestController {	
	@Autowired
	DispositivoService dispositivoService;
	@Autowired
	ComandoService comandoService;
	@Autowired 
	BitacoraComandoDetalleService bitacoraComandoDetalleService;
	
	Logger log = LogManager.getLogger(ComandoRestController.class);

	@RequestMapping(value="/comando/bitacora", method=RequestMethod.PUT)
	public ResponseEntity<PosicionesActVO> comandoBitacora(@RequestParam Long idBitacora, @RequestParam String txBitacoraDetalle) throws OKHttpResponse, ConflictHttpResponse{ 
		if(bitacoraComandoDetalleService.guardarBitacoraComandoDetalle(idBitacora,txBitacoraDetalle)) {
			throw new OKHttpResponse("Se registró correctamente el comando de bitacora detalle ","TCL20001",true);	
		}else {
			throw new ConflictHttpResponse("No se encontró el registro del comando de bitacora detalle.","TCL40901",false);	
		}
	}

	@RequestMapping(value = "/comando/bitacora/file", method = RequestMethod.POST, consumes = "multipart/form-data")
	public void comandoBitacoraFile(@RequestParam Long idBitacora, @RequestParam String txBitacoraDetalle, @RequestParam("file") MultipartFile multipartFile) throws Exception {
		if (bitacoraComandoDetalleService.guardarBitacoraComandoDetalleArchivo(idBitacora, txBitacoraDetalle, multipartFile)) {
			throw new OKHttpResponse("Se registró correctamente el comando de bitacora detalle ", "TCL20001", true);
		} else {
			throw new ConflictHttpResponse("No se encontró el registro del comando de bitacora detalle.", "TCL40901", false);
		}
	}

	private void createFile(MultipartFile multipartFile) throws IOException {
		File convFile = new File("/home/sinuhe/Desktop/" + multipartFile.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(multipartFile.getBytes());
		fos.close();
	}
	
	@RequestMapping("/consultaComandos")
	public ResponseEntity<List<DispositivoVO>> buscarDispositivoEnGeocerca(@RequestParam("tipo") String tipo, @RequestParam("valor") String valor) throws NotFoundException {
		List<DispositivoVO> x = new ArrayList<DispositivoVO>();
		x = dispositivoService.buscarDispositivos(null);
		return new ResponseEntity<List<DispositivoVO>>(x, HttpStatus.OK);
	}

	@RequestMapping("/buscarComandoPorId")
	public ResponseEntity<DispositivoVO> buscarComandoPorId(@RequestParam("id") String id) throws NotFoundException {
		List<DispositivoVO> x = new ArrayList<DispositivoVO>();

		Long[] data = new Long[] { Long.parseLong(id) };

		x = dispositivoService.buscarDispositivos(data);

		DispositivoVO disp = x.get(0);

		return new ResponseEntity<DispositivoVO>(disp, HttpStatus.OK);
	}

	@RequestMapping("/obtenerComandosDispositivo")
	public ResponseEntity<List<ComandoVO>> obtenerComandosDispositivo(@RequestParam("idDispositivo") Long idDispositivo) {
		List<ComandoVO> x = new ArrayList<ComandoVO>();
		x = comandoService.obtenerComandosDispositivo(idDispositivo);
		return new ResponseEntity<List<ComandoVO>>(x, HttpStatus.OK);
	}

	@RequestMapping("/catalogoComandos")
	public ResponseEntity<List<ComandoVO>> catalogoComandos(@RequestParam("idDispositivo") Long idDispositivo, @RequestParam("idTipoMedioComunicacion") Long idTipoMedioComunicacion)
			throws NotFoundException {
		List<ComandoVO> comandoVOs = new ArrayList<>();
		comandoVOs = comandoService.obtenerCatalogoComandos(idDispositivo, idTipoMedioComunicacion);
		return new ResponseEntity<List<ComandoVO>>(comandoVOs, HttpStatus.OK);
	}

	@RequestMapping("/activarDesactivarComandoPorUsuario")
	public ResponseEntity<Boolean> activarDesactivarComandoPorUsuario(@RequestParam("idDispositivo") Long idDispositivo, @RequestParam("idComando") Long idComando,
			@RequestParam("activo") Boolean activo) throws NotFoundException {
		Boolean result = comandoService.insertarOactualizarComando(idDispositivo, idComando, activo);
		return new ResponseEntity<Boolean>(result, HttpStatus.OK);
	}

	@RequestMapping("/execCommand")
	public ResponseEntity<Boolean> execCommand(@RequestParam("idDispositivo") Long idDispositivo, @RequestParam("idComando") Long idComando) throws NotFoundException {
		BitacoraComandoDTO bitacoraComandoDTO = comandoService.executeComando(idDispositivo, idComando);
		bitacoraComandoDTO.setIdBitacora(bitacoraComandoDTO.getIdBitacora()+1);
		Boolean result = comandoService.executeComandoDetalle(bitacoraComandoDTO,idDispositivo, idComando);
		return new ResponseEntity<Boolean>(result, HttpStatus.OK);
	}

	@RequestMapping("/logsCommand")
	public ResponseEntity<List<LogComandoVO>> logsCommand(@RequestParam("idDispositivo") Long idDispositivo) throws NotFoundException {
		List<LogComandoVO> logComandoVOs = comandoService.obtenerLogComando(idDispositivo);
		return new ResponseEntity<List<LogComandoVO>>(logComandoVOs, HttpStatus.OK);
	}

	@RequestMapping("/logsCommandDetalle")
	public ResponseEntity<List<LogComandoVO>> logsCommandDetalle(@RequestParam("idBitacora") Long idBitacora) throws NotFoundException {
		List<LogComandoVO> logComandoVOs = comandoService.obtenerLogComandoDetalle(idBitacora);
		return new ResponseEntity<List<LogComandoVO>>(logComandoVOs, HttpStatus.OK);
	}
	
	@RequestMapping("/comandoArchivo")
	public HttpEntity<byte[]> comandoArchivo(@RequestParam("idArchivo") Long idArchivo) throws IOException {
		BitacoraComandoDetalleArchivoDTO bitacoraComandoDetalleArchivoDTO = comandoService.obtenerArchivo(idArchivo);
		// Image
		if (bitacoraComandoDetalleArchivoDTO.getTxFormatoArchivo().equals(MediaType.IMAGE_JPEG.toString())) {
			return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(bitacoraComandoDetalleArchivoDTO.getArchivo());
		} 
		// audio/3gpp
		else if (bitacoraComandoDetalleArchivoDTO.getTxFormatoArchivo().equals("audio/3gpp")) {
			HttpHeaders header = new HttpHeaders();
			header.setContentType(MediaType.parseMediaType("application/octet-stream"));
			header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + bitacoraComandoDetalleArchivoDTO.getNombreArchivo());
			header.setContentLength(bitacoraComandoDetalleArchivoDTO.getArchivo().length);
			return new HttpEntity<byte[]>(bitacoraComandoDetalleArchivoDTO.getArchivo(), header);
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
	}
	
}
