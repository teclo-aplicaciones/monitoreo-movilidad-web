package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.TipoGrupoDTO;

@Repository("tipoGrupoDAO")
public class TipoGrupoDAOImpl extends BaseDAOImpl<TipoGrupoDTO> implements TipoGrupoDAO{

	@Override
	public TipoGrupoDTO buscarTipoGrupoPorId(Long idTipoGrupo){
		Criteria query = getCurrentSession().createCriteria(TipoGrupoDTO.class);
		query.add(Restrictions.eq("idTipoGrupo", idTipoGrupo));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return (TipoGrupoDTO) query.uniqueResult();
	}
}
