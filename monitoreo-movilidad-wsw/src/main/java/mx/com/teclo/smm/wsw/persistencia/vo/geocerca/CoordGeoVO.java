package mx.com.teclo.smm.wsw.persistencia.vo.geocerca;

public class CoordGeoVO {
	private Double lat;
	private Double lng;
	private Double distance;
	
	public Double getLat() {
		return lat;
	}
	public void setLat(Double lat) {
		this.lat = lat;
	}
	public Double getLng() {
		return lng;
	}
	public void setLng(Double lng) {
		this.lng = lng;
	}
	public Double getDistance() {
		return distance;
	}
	public void setDistance(Double distance) {
		this.distance= distance;
	}
	
	public Double[] getCoord(){
		return new Double[]{lng, lat};
	}
}
