package mx.com.teclo.smm.wsw.persistencia.vo.evento;

public class EventoDispositivoVO{
	private Long idDispositivo;
	private Long countEvento;
	
	public EventoDispositivoVO(Long idDispositivo, Long countEvento) {
		this.idDispositivo = idDispositivo;
		this.countEvento = countEvento;
	}
	public Long getIdDispositivo() {
		return idDispositivo;
	}
	public void setIdDispositivo(Long idDispositivo) {
		this.idDispositivo = idDispositivo;
	}
	public Long getCountEvento() {
		return countEvento;
	}
	public void setCountEvento(Long countEvento) {
		this.countEvento = countEvento;
	}
	@Override
	public String toString() {
		return "[idDispositivo=" + idDispositivo + ", countEvento=" + countEvento + "]";
	}
	
	
}