package mx.com.teclo.smm.wsw.util.enumerable;

import java.util.ArrayList;
import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.vo.catalogo.CatalogoVO;

public enum CatalogoGrupoEnum {
	NOMBRE_GRUPO(1L, "Nombre Grupo"), DISPOSITIVO(2L, "Dispositivo"), USUARIOS(3L, "Usuario");
	
	private Long idCat;
	private String nombreCat;
	
	private CatalogoGrupoEnum(Long idCat, String nombreCat){
		this.idCat = idCat;
		this.nombreCat = nombreCat;
	}

	public Long getIdCat() {
		return idCat;
	}

	public void setIdCat(Long idCat) {
		this.idCat = idCat;
	}

	public String getNombreCat() {
		return nombreCat;
	}

	public void setNombreCat(String nombreCat) {
		this.nombreCat = nombreCat;
	}
	
	public static List<CatalogoVO> getCatalogo(){
		CatalogoGrupoEnum[] listaEnum = CatalogoGrupoEnum.values();
		List<CatalogoVO> listaCatVO = new ArrayList<CatalogoVO>(); 
		
		for(CatalogoGrupoEnum cat : listaEnum)
		{
			CatalogoVO catVO = new CatalogoVO();
			catVO.setId(cat.getIdCat());
			catVO.setNombre(cat.getNombreCat());
			listaCatVO.add(catVO);
		}
		return listaCatVO;
	}
}
