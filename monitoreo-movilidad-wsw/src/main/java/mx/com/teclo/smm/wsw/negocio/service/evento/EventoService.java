package mx.com.teclo.smm.wsw.negocio.service.evento;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.vo.EventoVO;

public interface EventoService {

	public List<EventoVO> consultaEventos();

}
