package mx.com.teclo.smm.wsw.persistencia.myBatis;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;

import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.PosicionesActDTO;

@Mapper
public interface PosicionesActMapper {
 
	@Insert("INSERT INTO TMM054D_GL_POSICIONES_ACT(" + 
			"ID_POSICIONES_ACT, ID_DISPOSITIVO, NU_LATITUD, NU_LONGITUD, NU_ALTITUD, THE_GEOM, NU_IMEI, " + 
			"NU_IP, TX_DIRECCION, FH_REGISTRO, ST_ACTIVO, FH_CREACION, ID_USR_CREACION, FH_MODIFICACION, ID_USR_MODIFICA, ID_EVENTO, NU_ACCURACY) " + 
			"VALUES( SQTMM054D_GL_POSI.NEXTVAL, #{idDispositivo.idDispositivo}, #{nuLatitud}, #{nuLongitud}, #{nuAltitud},SDO_GEOMETRY(2001, NULL, SDO_POINT_TYPE(#{nuLatitud},#{nuLongitud},#{nuAltitud}), NULL, NULL), #{nuImei}, "
			+" #{nuIp}, #{txDireccion}, #{fhRegistro}, #{stActivo}, #{fhCreacion}, #{idUsrCreacion}, #{fhModificacion}, #{idUsrModifica}, #{idEvento.idEvento}, #{nuAccuracy})")
	@SelectKey(statement = "SELECT SQTMM054D_GL_POSI.CURRVAL FROM DUAL", keyProperty = "idPosicionesAct", before = false, resultType = Long.class)
	public void insertarPosicionesActuales(PosicionesActDTO posicionesActDTO);
	
	@Update("UPDATE TMM054D_GL_POSICIONES_ACT"
			+ " SET ID_DISPOSITIVO = #{idDispositivo.idDispositivo},"
			+ " NU_LATITUD = #{nuLatitud},"
			+ " NU_LONGITUD = #{nuLongitud},"
			+ " NU_ALTITUD = #{nuAltitud},"
			+ " THE_GEOM = SDO_GEOMETRY(2001, NULL, SDO_POINT_TYPE(#{nuLatitud}, #{nuLongitud}, #{nuAltitud}), NULL, NULL),"
			+ " NU_IMEI = #{nuImei},"
			+ " NU_IP = #{nuIp},"
			+ " TX_DIRECCION = #{txDireccion},"
			+ " FH_REGISTRO = #{fhRegistro},"
			+ " ST_ACTIVO = #{stActivo},"
			+ " FH_CREACION = #{fhCreacion},"
			+ " ID_USR_CREACION = #{idUsrCreacion},"
			+ " FH_MODIFICACION = #{fhModificacion},"
			+ " ID_USR_MODIFICA = #{idUsrModifica}, " 
			+ " ID_EVENTO = #{idEvento.idEvento}, "
			+ " NU_ACCURACY = #{nuAccuracy} "
			+ " WHERE ID_POSICIONES_ACT = #{idPosicionesAct}")
	public void actualizarPosicionesActuales(PosicionesActDTO posicionesActDTO);
	
}
