package mx.com.teclo.smm.wsw.negocio.service.comando;

import org.springframework.web.multipart.MultipartFile;

public interface BitacoraComandoDetalleService {
	public boolean guardarBitacoraComandoDetalle(Long dBitacora,String txBitacoraDetalle);
	public boolean guardarBitacoraComandoDetalleArchivo(Long idBitacora,String txBitacoraDetalle,MultipartFile multipartFile) throws Exception ;
}
