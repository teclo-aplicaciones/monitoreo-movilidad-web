package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.BitacoraStatusDTO;

@SuppressWarnings("unchecked")
@Repository("BitacoraComandoStatusDAO")
public class BitacoraComandoStatusDAOImpl extends BaseDAOImpl<BitacoraStatusDTO> implements BitacoraComandoStatusDAO{



	@Override
	@Transactional(readOnly = true)
	public BitacoraStatusDTO obtenerBitacoraStatus(Long idStatus) {
		Criteria query = getCurrentSession().createCriteria(BitacoraStatusDTO.class);
		query.add(Restrictions.eq("idBitacoraStatus", idStatus));
		return (BitacoraStatusDTO) query.uniqueResult();		
	}
	
}
