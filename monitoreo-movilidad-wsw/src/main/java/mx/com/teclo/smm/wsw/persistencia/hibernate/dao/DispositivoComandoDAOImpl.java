package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.DispositivoComandoDTO;

@SuppressWarnings("unchecked")
@Repository("dispositivoComandoDAO")
public class DispositivoComandoDAOImpl extends BaseDAOImpl<DispositivoComandoDTO> implements DispositivoComandoDAO{

	@Override
	@Transactional(readOnly = true)
	public List<DispositivoComandoDTO> obtenerComandoPorDispositivo(Long idDispositivo) {
		Criteria query = getCurrentSession().createCriteria(DispositivoComandoDTO.class);	
		query.add(Restrictions.eq("idDispositivo.idDispositivo", idDispositivo));
		return (List<DispositivoComandoDTO>)query.list();		
	}

	@Override
	@Transactional(readOnly = true)
	public DispositivoComandoDTO obtenerComandoPorDispositivoYComando(Long idDispositivo,Long idComando) {
		Criteria query = getCurrentSession().createCriteria(DispositivoComandoDTO.class);	
		query.add(Restrictions.eq("idDispositivo.idDispositivo", idDispositivo));
		query.add(Restrictions.eq("idComando.idComando", idComando));		
		return (DispositivoComandoDTO)query.uniqueResult();		
	}
}
