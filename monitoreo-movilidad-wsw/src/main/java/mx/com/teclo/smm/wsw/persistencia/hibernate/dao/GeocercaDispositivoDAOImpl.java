package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.DispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.GeocercaDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.GeocercaDispositivoDTO;

@SuppressWarnings("unchecked")
@Repository("geocercaDispositivoDAO")
public class GeocercaDispositivoDAOImpl extends BaseDAOImpl<GeocercaDispositivoDTO> implements GeocercaDispositivoDAO{


	@Override
	public List<GeocercaDispositivoDTO> buscarRelacionGeocercasDispositivo(DispositivoDTO dDTO){
		Criteria query = getCurrentSession().createCriteria(GeocercaDispositivoDTO.class);
		query.add(Restrictions.eq("idDispositivo", dDTO));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));

		return query.list();
	}

	@Override
	public Long buscarSiguenteValor(){
		SQLQuery query = getCurrentSession().createSQLQuery("SELECT SQTMM014D_DP_DISP_GEO.nextval FROM DUAL");
		BigDecimal bd = (BigDecimal)query.uniqueResult();

		return bd.longValueExact();
	}

	@Override
	public List<GeocercaDispositivoDTO> buscarTodoDispositivosEnGeocercas(){
		Criteria query = getCurrentSession().createCriteria(GeocercaDispositivoDTO.class);
		query.createAlias("idDispositivo", "idDispositivo");
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		query.add(Restrictions.eq("idDispositivo.idEmpresa", getIdEmpresa()));
		return query.list();
	}

	@Override
	public List<GeocercaDispositivoDTO> buscarDispositivosEnGeocerca(GeocercaDTO geocerca) {
		Criteria query = getCurrentSession().createCriteria(GeocercaDispositivoDTO.class);
		query.add(Restrictions.eq("idGeocerca", geocerca));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));

		return query.list();
	}

	@Override
	public List<GeocercaDispositivoDTO> buscarDispositivosEnGeocerca(DispositivoDTO dispositivoDTO){
		Criteria query = getCurrentSession().createCriteria(GeocercaDispositivoDTO.class);
		query.add(Restrictions.eq("idDispositivo", dispositivoDTO));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return query.list();
	}

	@Override
	public List<GeocercaDispositivoDTO> buscarDispositivosGeocerca(DispositivoDTO dispositivoDTO){
		Criteria query = getCurrentSession().createCriteria(GeocercaDispositivoDTO.class);
		query.add(Restrictions.eq("idDispositivo", dispositivoDTO));
		//query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return query.list();
	}

	@Override
	public Boolean eliminarRelacionGeocercaDispositivo(GeocercaDTO geocerca){
		Criteria query = getCurrentSession().createCriteria(GeocercaDispositivoDTO.class);
		query.add(Restrictions.eq("idGeocerca", geocerca));
		List<GeocercaDispositivoDTO> gdDTO = query.list();

		for(GeocercaDispositivoDTO x: gdDTO){
			this.delete(x);
		}

		return Boolean.TRUE;
	}
}
