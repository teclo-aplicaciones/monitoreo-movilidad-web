package mx.com.teclo.smm.wsw.persistencia.vo.monitoreo;

public class ConfigMonitoreoDispoVO {

	private Long idDispoSitivo;
	private String nbDispositivo;
	private String cdParamMonitoreo;
	private String valorParamMonotoreo;
	/**
	 * @return the idDispoSitivo
	 */
	public Long getIdDispoSitivo() {
		return idDispoSitivo;
	}
	/**
	 * @param idDispoSitivo the idDispoSitivo to set
	 */
	public void setIdDispoSitivo(Long idDispoSitivo) {
		this.idDispoSitivo = idDispoSitivo;
	}
	/**
	 * @return the nbDispositivo
	 */
	public String getNbDispositivo() {
		return nbDispositivo;
	}
	/**
	 * @param nbDispositivo the nbDispositivo to set
	 */
	public void setNbDispositivo(String nbDispositivo) {
		this.nbDispositivo = nbDispositivo;
	}
	/**
	 * @return the cdParamMonitoreo
	 */
	public String getCdParamMonitoreo() {
		return cdParamMonitoreo;
	}
	/**
	 * @param cdParamMonitoreo the cdParamMonitoreo to set
	 */
	public void setCdParamMonitoreo(String cdParamMonitoreo) {
		this.cdParamMonitoreo = cdParamMonitoreo;
	}
	/**
	 * @return the valorParamMonotoreo
	 */
	public String getValorParamMonotoreo() {
		return valorParamMonotoreo;
	}
	/**
	 * @param valorParamMonotoreo the valorParamMonotoreo to set
	 */
	public void setValorParamMonotoreo(String valorParamMonotoreo) {
		this.valorParamMonotoreo = valorParamMonotoreo;
	}
	
}
