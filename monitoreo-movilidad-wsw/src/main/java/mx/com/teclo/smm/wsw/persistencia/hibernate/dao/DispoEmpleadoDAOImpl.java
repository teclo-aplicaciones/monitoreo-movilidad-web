package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.DispoEmpleadoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.DispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.EmpleadoDTO;

@Repository("dispoEmpleadoDAO")
public class DispoEmpleadoDAOImpl extends BaseDAOImpl<DispoEmpleadoDTO> implements DispoEmpleadoDAO{

	@Override
	public DispoEmpleadoDTO buscarRelacionDispositivoEmpleado(DispositivoDTO dDTO){
		Criteria query = getCurrentSession().createCriteria(DispoEmpleadoDTO.class);
		query.createAlias("idDispositivo", "idDispositivo");
		query.createAlias("idEmpleado", "idEmpleado");
		query.add(Restrictions.eq("idDispositivo", dDTO));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));

		//idEmpresa
		query.add(Restrictions.eq("idDispositivo.idEmpresa", getIdEmpresa()));
		query.add(Restrictions.eq("idEmpleado.idEmpresa", getIdEmpresa()));

		return (DispoEmpleadoDTO) query.uniqueResult();
	}

	@Override
	public List<DispoEmpleadoDTO> buscarRelacionDispoEmpleado()
	{
		Criteria query = getCurrentSession().createCriteria(DispoEmpleadoDTO.class);
		query.createAlias("idDispositivo", "idDispositivo");
		query.createAlias("idEmpleado", "idEmpleado");
		//idEmpresa
		query.add(Restrictions.eq("idDispositivo.idEmpresa", getIdEmpresa()));
		query.add(Restrictions.eq("idEmpleado.idEmpresa", getIdEmpresa()));

		query.add(Restrictions.eq("stActivo", Boolean.TRUE));

		return query.list();
	}

	@Override
	public List<EmpleadoDTO> buscarRelacionDispoEmpleadoUnicos()
	{
		Criteria query = getCurrentSession().createCriteria(DispoEmpleadoDTO.class);
		query.createAlias("idDispositivo", "idDispositivo");
		query.createAlias("idEmpleado", "idEmpleado");
		query.setProjection(Projections.distinct(Projections.property("idEmpleado")));
		//idEmpresa
		query.add(Restrictions.eq("idDispositivo.idEmpresa", getIdEmpresa()));
		query.add(Restrictions.eq("idEmpleado.idEmpresa", getIdEmpresa()));


		query.add(Restrictions.eq("stActivo", Boolean.TRUE));

		return query.list();
	}



	@Override
	public DispoEmpleadoDTO buscarRelacionDispoEmpleadoPorDispositivo(DispositivoDTO dispoDTO)
	{
		Criteria query = getCurrentSession().createCriteria(DispoEmpleadoDTO.class);
		query.createAlias("idDispositivo", "idDispositivo");
		query.createAlias("idEmpleado", "idEmpleado");
		query.add(Restrictions.eq("idDispositivo", dispoDTO));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		//idEmpresa
		query.add(Restrictions.eq("idDispositivo.idEmpresa", getIdEmpresa()));
		query.add(Restrictions.eq("idEmpleado.idEmpresa", getIdEmpresa()));


		List<DispoEmpleadoDTO> lista = query.list();
		if(lista.size()>0)
			return lista.get(0);
		return null;
	}

	@Override
	public List<DispoEmpleadoDTO> buscarRelacionDispoEmpleadoPorEmpleado(EmpleadoDTO empleadoDTO)
	{
		Criteria query = getCurrentSession().createCriteria(DispoEmpleadoDTO.class);
		query.createAlias("idDispositivo", "idDispositivo");
		query.createAlias("idEmpleado", "idEmpleado");
		query.add(Restrictions.eq("idEmpleado", empleadoDTO));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));

		//idEmpresa
		query.add(Restrictions.eq("idDispositivo.idEmpresa", getIdEmpresa()));
		query.add(Restrictions.eq("idEmpleado.idEmpresa", getIdEmpresa()));


		/*List<DispoEmpleadoDTO> lista = (List<DispoEmpleadoDTO>)query.list();
		if(lista.size()>0)
			return lista.get(0);*/
		return query.list();
	}


	@Override
	public DispoEmpleadoDTO buscarRelacionIdDispoEmpleadoPorEmpleado(Long idEmpleadoDTO)
	{
		Criteria query = getCurrentSession().createCriteria(DispoEmpleadoDTO.class);
		query.createAlias("idDispositivo", "idDispositivo");
		query.createAlias("idEmpleado", "idEmpleado");

		//idEmpresa
		query.add(Restrictions.eq("idDispositivo.idEmpresa", getIdEmpresa()));
		query.add(Restrictions.eq("idEmpleado.idEmpresa", getIdEmpresa()));


		query.add(Restrictions.eq("stActivo", Boolean.TRUE));

		List<DispoEmpleadoDTO> lista = query.list();
		return lista.get(0);
	}

	@Override
	public List<DispoEmpleadoDTO> buscarRelacionIdDisposEmpleadoPorEmpleado(Long idEmpleadoDTO){
		Criteria query = getCurrentSession().createCriteria(DispoEmpleadoDTO.class);
		query.createAlias("idDispositivo", "idDispositivo");
		query.createAlias("idEmpleado", "idEmpleado");

		//idEmpresa
		query.add(Restrictions.eq("idDispositivo.idEmpresa", getIdEmpresa()));
		query.add(Restrictions.eq("idEmpleado.idEmpresa", getIdEmpresa()));


		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return query.list();
	}
}
