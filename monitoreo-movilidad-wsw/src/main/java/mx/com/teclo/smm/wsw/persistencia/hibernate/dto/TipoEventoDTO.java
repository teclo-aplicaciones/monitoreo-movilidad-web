package mx.com.teclo.smm.wsw.persistencia.hibernate.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TMM006C_CT_TIPO_EVENTOS")
public class TipoEventoDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2176886043599375424L;
	@Id
	@Column(name = "ID_TIPO_EVENTO")
	private Long idTipoEvento;
	@Column(name = "CD_TIPO_EVENTO")
	private String cdTipoEvento;
	@Column(name = "NB_TIPO_EVENTO")
	private String nbTipoEvento;
	@Column(name = "TX_TIPO_EVENTO")
	private String txTipoEvento;
	@Column(name = "ST_ACTIVO")
	private Boolean stActivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICA")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;
	
	
	public Long getIdTipoEvento() {
		return idTipoEvento;
	}
	public void setIdTipoEvento(Long idTipoEvento) {
		this.idTipoEvento = idTipoEvento;
	}
	public String getCdTipoEvento() {
		return cdTipoEvento;
	}
	public void setCdTipoEvento(String cdTipoEvento) {
		this.cdTipoEvento = cdTipoEvento;
	}
	public String getNbTipoEvento() {
		return nbTipoEvento;
	}
	public void setNbTipoEvento(String nbTipoEvento) {
		this.nbTipoEvento = nbTipoEvento;
	}
	public String getTxTipoEvento() {
		return txTipoEvento;
	}
	public void setTxTipoEvento(String txTipoEvento) {
		this.txTipoEvento = txTipoEvento;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}

	
}
