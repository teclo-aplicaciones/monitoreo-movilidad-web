package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.SubTipoDispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.TipoDispositivoDTO;

public interface SubTipoDispositivoDAO extends BaseDAO<SubTipoDispositivoDTO>{

	public List<SubTipoDispositivoDTO> buscarCatalogo(Long[] id);
	public List<SubTipoDispositivoDTO> buscarCatalogoTodos();
	public List<SubTipoDispositivoDTO> buscarCatalogoPorTipo(TipoDispositivoDTO tdDTO);
	public SubTipoDispositivoDTO buscarCatalogoPorTipoAndIdSubtipo(TipoDispositivoDTO tdDTO,Long idSubTipo);	
}
