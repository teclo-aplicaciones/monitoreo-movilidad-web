package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.EventoDTO;
import mx.com.teclo.smm.wsw.persistencia.vo.catalogo.CatalogoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.evento.EventoAnualVO;
import mx.com.teclo.smm.wsw.persistencia.vo.evento.EventoDispositivoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.evento.EventosTablasHistoricoVO;
@Repository
public class EventoDAOImpl extends BaseDAOImpl<EventoDTO> implements EventoDAO {
	
	@Override
	public EventoDTO findByIdEvento(Long idEvento) {
		Criteria query = getCurrentSession().createCriteria(EventoDTO.class);
		query.add(Restrictions.eq("idEvento",idEvento));		
		return (EventoDTO) query.uniqueResult();
	}

	

	@Override
	public List<EventoDTO> buscarCatalogoEventos() {
		Criteria query = getCurrentSession().createCriteria(EventoDTO.class);
		query.add(Restrictions.eq("stActivo",Boolean.TRUE));
		return (List<EventoDTO>)query.list();
	}
	
	@Override
	public List<EventoDTO> activarDesactivarBuscarCatalogoEventos() {
		Criteria query = getCurrentSession().createCriteria(EventoDTO.class);	
		query.addOrder(Order.asc("idEvento"));
		return (List<EventoDTO>)query.list();
	}
	
	@Override
	public EventoDTO buscarCatalogoEventosId(long id) {
		Criteria query = getCurrentSession().createCriteria(EventoDTO.class);
		query.add(Restrictions.eq("stActivo",Boolean.TRUE));
		query.add(Restrictions.eq("idEvento",id));
		return (EventoDTO)query.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<EventoDTO> findAll(){
		Criteria query = getCurrentSession().createCriteria(EventoDTO.class);
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		List<EventoDTO> list = (List<EventoDTO>) query.list(); 
		return list;
	}
	
		
	@Override
	public List<EventosTablasHistoricoVO> consultarTiposEventos(Long idGrupo, String fecha, String dia) {
		List<EventosTablasHistoricoVO> listaRet = new ArrayList<>();
		
		String consulta = "select disp.id_dispositivo, disp.nb_table_disp_posici " + 
				"from tmm011d_dp_dispositivos disp ";
		SQLQuery query;
		if(idGrupo !=  0){
			consulta = consulta +"inner join tmm021d_dp_dispositivo_grupos di_gr on disp.id_dispositivo = di_gr.id_dispositivo " + 
					"where disp.st_activo = 1 and di_gr.id_grupo = ? ";
			query = getSession().createSQLQuery(consulta);
			query.setParameter(0, idGrupo);
		}else{
			consulta = consulta +"where disp.st_activo = 1 ";
			query = getSession().createSQLQuery(consulta);
		}
		
		List<Object[]> result = query.list();
		
		if(!result.isEmpty()) {
			Criteria queryEventos = getCurrentSession().createCriteria(EventoDTO.class);
			List<EventoDTO> list = (List<EventoDTO>) queryEventos.list();
			
			for(EventoDTO evento : list) {
				EventosTablasHistoricoVO ev = new EventosTablasHistoricoVO();	
				ev.setIdEvento(evento.getIdEvento());
				ev.setCdEvento(evento.getCdEvento());
				ev.setNbEvento(evento.getNbEvento());
				ev.setTxEvento(evento.getTxEvento());
				listaRet.add(ev);
			}
			
			for(Object[] obj : result) {
				SQLQuery query2 = getSession().createSQLQuery(crearConsultaTabla(obj[1].toString(),fecha, dia));
				List<Object[]> result2 = query2.list();
				for(Object[] o : result2) {
					
					for(int i = 0 ; i < listaRet.size(); i++) {
						if(listaRet.get(i).getIdEvento().equals(Long.parseLong(o[0].toString()))) {
							listaRet.get(i).addEventoDispositivoVO(new EventoDispositivoVO(Long.valueOf(obj[0].toString()),Long.valueOf(o[3].toString())));
							break;
						}else{}
					}
					
				}
			}
			
		}
		return listaRet;
	}
	
	@Override
	public Long consultarEventosDispositivoHistorico(String tabla, String criterio) {
		SQLQuery query = getCurrentSession().createSQLQuery("select count(*) from "+tabla+" disp where TO_CHAR(FH_REGISTRO, 'MM/YYYY') = ?");
		query.setParameter(0, criterio);
		BigDecimal number = (BigDecimal) query.uniqueResult();
		return (Long) number.longValueExact();
	}
	
	@Override
	public Long consultarEventosHistorico(String tabla, Long idEvento, String criterio) {
		SQLQuery query = getSession().createSQLQuery("select count(disp.ID_EVENTO) from "+tabla+" disp WHERE TO_CHAR(FH_REGISTRO,'MM/YYYY') = ? AND disp.ID_EVENTO = ?");
		query.setParameter(0, criterio);
		query.setParameter(1, idEvento);
		BigDecimal number = (BigDecimal)query.uniqueResult(); 
		return (Long)  number.longValueExact();
	}
	
	@Override
	public List<EventoAnualVO> consultarEventosPorMesHistorico(String tabla, String anio){
		SQLQuery query = getCurrentSession().createSQLQuery(
				"select TO_CHAR(pos.FH_REGISTRO, 'MM')-1, ev.CD_EVENTO, COUNT(*) "
				+ "from "+tabla+" pos, TMM012C_CT_EVENTOS ev "
				+ "WHERE pos.ID_EVENTO = ev.ID_EVENTO AND TO_CHAR(pos.FH_REGISTRO, 'YYYY') = ? "
				+ "GROUP BY TO_CHAR(pos.FH_REGISTRO, 'MM'), ev.CD_EVENTO "
				+" ORDER BY TO_CHAR(pos.FH_REGISTRO, 'MM') ASC");
		
		query.setParameter(0, anio);
		
		List<Object[]> lista = (List<Object[]>) query.list();
		
		List<EventoAnualVO> listaEventosPorMes = new ArrayList<EventoAnualVO>();
		for(Object[] obj:lista){
			BigDecimal mes = (BigDecimal)obj[0];
			BigDecimal conteo = (BigDecimal)obj[2];
			listaEventosPorMes.add(new EventoAnualVO(mes.longValueExact(), (String)obj[1], conteo.longValueExact()));
		}
		
		return listaEventosPorMes;
	}
	
	@Override
	public Long consultarEventosDiarios(String tabla, Long idEvento) {
		SQLQuery query = getSession().createSQLQuery("select count(disp.ID_EVENTO) from "+tabla+" disp WHERE disp.FH_REGISTRO BETWEEN TRUNC(SYSDATE) AND TRUNC(SYSDATE+1) AND disp.ID_EVENTO = ?");
		query.setParameter(0, idEvento);
		BigDecimal number = (BigDecimal)query.uniqueResult(); 
		return (Long)  number.longValueExact();
	}
	
	@Override
	public Long consultarDispositivoDiario(String tabla) {
		SQLQuery query = getSession().createSQLQuery("select count(*) from "+tabla+" disp WHERE disp.FH_REGISTRO BETWEEN TRUNC(SYSDATE) AND TRUNC(SYSDATE+1)");
		BigDecimal number = (BigDecimal)query.uniqueResult(); 
		return (Long)  number.longValueExact();
	}
	
	private String crearConsultaTabla(String str, String fecha, String dia) {
		
		String consulta = "select " + 
				"    pos_1.id_evento," + 
				"	 (select eve.cd_evento from TMM012C_CT_EVENTOS eve where eve.id_evento = pos_1.id_evento) codigo, " + 
				"	 (select eve.tx_evento from TMM012C_CT_EVENTOS eve where eve.id_evento = pos_1.id_evento) nombre, "; 
				
		if(dia.equals("0")){
			consulta = consulta +
				"	 (select  count(*) from $Tabla pos_2 where pos_2.id_evento = pos_1.id_evento and trunc(pos_2.fh_registro,'MM') = trunc(TO_DATE('$fecha','MM/YYYY'),'MM')) as cont " + 
				"	 from $Tabla pos_1  where trunc(pos_1.fh_registro,'MM') = trunc(TO_DATE('$fecha','MM/YYYY'),'MM') and pos_1.st_activo = 1 group by pos_1.id_evento order by pos_1.id_evento asc";
		}else{
			consulta = consulta +
				"	 (select  count(*) from $Tabla pos_2 where pos_2.id_evento = pos_1.id_evento and trunc(pos_2.fh_registro) = trunc(TO_DATE('$fecha','DD/MM/YYYY'))) as cont " + 
				"	 from $Tabla pos_1  where trunc(pos_1.fh_registro) = trunc(TO_DATE('$fecha','DD/MM/YYYY')) and pos_1.st_activo = 1 group by pos_1.id_evento order by pos_1.id_evento asc";
		}
				
		
		return consulta.replace("$Tabla", str).replace("$fecha", dia.equals("0") ? fecha: dia+"/"+fecha);
	}
	

	@Override
	public List<EventosTablasHistoricoVO> consultarTiposEventosDispositivo(Long idDispositivo, String fecha) {
		List<EventosTablasHistoricoVO> listaRet = new ArrayList<>();
		
		SQLQuery query = getSession().createSQLQuery("select disp.nb_table_disp_posici from tmm011d_dp_dispositivos disp where disp.id_dispositivo = ?");
		query.setParameter(0, idDispositivo);
		List<Object> result = query.list();
		
		if(!result.isEmpty()) {
			Criteria queryEventos = getCurrentSession().createCriteria(EventoDTO.class);
			List<EventoDTO> list = (List<EventoDTO>) queryEventos.list();
			
			for(EventoDTO evento : list) {
				EventosTablasHistoricoVO ev = new EventosTablasHistoricoVO();	
				ev.setIdEvento(evento.getIdEvento());
				ev.setCdEvento(evento.getCdEvento());
				ev.setNbEvento(evento.getNbEvento());
				ev.setTxEvento(evento.getTxEvento());
				listaRet.add(ev);
			}
			String[] fechas = "01/:02/:03/:04/:05/:06/:07/:08/:09/:10/:11/:12".split(":");
			Long mes = 1L;
			for(String fh : fechas) {
				SQLQuery query2 = getSession().createSQLQuery(crearConsultaTabla(result.get(0).toString(),fh + fecha, "0"));
				List<Object[]> result2 = query2.list();
				for(Object[] o : result2) {
					
					for(int i = 0 ; i < listaRet.size(); i++) {
						if(listaRet.get(i).getIdEvento().equals(Long.parseLong(o[0].toString()))) {
							listaRet.get(i).addEventoDispositivoVO(new EventoDispositivoVO(mes, Long.valueOf(o[3].toString())));
							break;
						}
					}
					
				}
				mes++;
			}
			
		}
		return listaRet;
	}
	
	@Override
	public List<EventosTablasHistoricoVO> consultarTiposEventosHoy(Long idGrupo, Long idDispositivo)
	{
		List<EventosTablasHistoricoVO> listaRet = new ArrayList<>();
		Date hoy = new Date();
		SimpleDateFormat sdfecha = new SimpleDateFormat("MM/YYYY");
		SimpleDateFormat sddia = new SimpleDateFormat("d");
		
		String sql = "select disp.id_dispositivo, disp.nb_table_disp_posici " + 
				"from tmm011d_dp_dispositivos disp "; 
		
		SQLQuery query;
		
		if(idGrupo != 0){
			sql = sql+"inner join tmm021d_dp_dispositivo_grupos di_gr on disp.id_dispositivo = di_gr.id_dispositivo ";
			if(idDispositivo != 0){
				sql = sql + "where disp.id_dispositivo = ? and disp.st_activo = 1 and di_gr.id_grupo = ?";
				query = getSession().createSQLQuery(sql);
				query.setParameter(0, idDispositivo);
				query.setParameter(1, idGrupo);
			}else{
				sql = sql + "where disp.st_activo = 1 and di_gr.id_grupo = ?";
				query = getSession().createSQLQuery(sql);
				query.setParameter(0, idGrupo);
			}
		}else{
			if(idDispositivo != 0){
				sql = sql + "where disp.id_dispositivo = ? and disp.st_activo = 1 ";
				query = getSession().createSQLQuery(sql);
				query.setParameter(0, idDispositivo);
			}else{
				sql = sql + "where disp.st_activo = 1 ";
				query = getSession().createSQLQuery(sql);
			}
		}
		
		List<Object[]> result = query.list();
		
		if(!result.isEmpty()) {
			Criteria queryEventos = getCurrentSession().createCriteria(EventoDTO.class);
			List<EventoDTO> list = (List<EventoDTO>) queryEventos.list();
			
			for(EventoDTO evento : list) {
				EventosTablasHistoricoVO ev = new EventosTablasHistoricoVO();	
				ev.setIdEvento(evento.getIdEvento());
				ev.setCdEvento(evento.getCdEvento());
				ev.setNbEvento(evento.getNbEvento());
				ev.setTxEvento(evento.getTxEvento());
				listaRet.add(ev);
			}
			
			for(Object[] obj : result) {
				SQLQuery query2 = getSession().createSQLQuery(crearConsultaTabla(obj[1].toString(),sdfecha.format(hoy), sddia.format(hoy)));
				List<Object[]> result2 = query2.list();
				for(Object[] o : result2) {
					
					for(int i = 0 ; i < listaRet.size(); i++) {
						if(listaRet.get(i).getIdEvento().equals(Long.parseLong(o[0].toString()))) {
							listaRet.get(i).addEventoDispositivoVO(new EventoDispositivoVO(Long.valueOf(obj[0].toString()),Long.valueOf(o[3].toString())));
							break;
						}
					}
					
				}
			}
			
		}
		return listaRet;
	}
	
	@Override
	public List<CatalogoVO>consultarTipoEventosDispoActuales(){
		List<CatalogoVO> listaRet = new ArrayList<>();
		Date hoy = new Date();
		SimpleDateFormat sdfecha = new SimpleDateFormat("MM/YYYY");
		SimpleDateFormat sddia = new SimpleDateFormat("d");
		
		String sql = "select disp.id_dispositivo, disp.nb_dispositivo, disp.nb_table_disp_posici " + 
				"from tmm011d_dp_dispositivos disp "; 
		
		SQLQuery query = getSession().createSQLQuery(sql);
				
		List<Object[]> result = query.list();
		
		CatalogoVO dispVO = null;
		
		if(!result.isEmpty()) {
			for(Object[] obj : result) {
				SQLQuery query2 = getSession().createSQLQuery(crearConsultaTabla(obj[2].toString(),sdfecha.format(hoy), sddia.format(hoy)));
				List<Object[]> result2 = query2.list();
				if(!result2.isEmpty()){
					dispVO = new CatalogoVO();
					dispVO.setId(Long.parseLong(obj[0].toString()));
					dispVO.setNombre(obj[1].toString());
					listaRet.add(dispVO);
				}
			}
			
		}
		return listaRet;
	}
	
	@Override
	public List<EventoDTO> buscarCatalogoEventosPorSubtipo(Long id){
		Criteria query = getCurrentSession().createCriteria(EventoDTO.class);
		query.createAlias("idSubtipoDispositivo", "subtipo");
		query.add(Restrictions.eq("subtipo.idSubTipoDispositivo", id));
		
		return (List<EventoDTO>) query.list();
	}
	
	@Override
	public EventoDTO activarDesactivarBuscarCatalogoEventosId(long id) {
		Criteria query = getCurrentSession().createCriteria(EventoDTO.class);		
		query.add(Restrictions.eq("idEvento",id));
		return (EventoDTO)query.uniqueResult();
	}
	
}
