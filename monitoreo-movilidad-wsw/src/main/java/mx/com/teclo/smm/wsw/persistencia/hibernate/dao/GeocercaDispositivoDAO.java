package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.DispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.GeocercaDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.GeocercaDispositivoDTO;

public interface GeocercaDispositivoDAO extends BaseDAO<GeocercaDispositivoDTO>{
	public List<GeocercaDispositivoDTO> buscarRelacionGeocercasDispositivo(DispositivoDTO dDTO);
	
	public Long buscarSiguenteValor();
	public List<GeocercaDispositivoDTO> buscarTodoDispositivosEnGeocercas();
	public List<GeocercaDispositivoDTO> buscarDispositivosEnGeocerca(GeocercaDTO geocerca);
	public List<GeocercaDispositivoDTO> buscarDispositivosEnGeocerca(DispositivoDTO dispositivoDTO);
	public List<GeocercaDispositivoDTO> buscarDispositivosGeocerca(DispositivoDTO dispositivoDTO);
	public Boolean eliminarRelacionGeocercaDispositivo(GeocercaDTO geocerca);
}
