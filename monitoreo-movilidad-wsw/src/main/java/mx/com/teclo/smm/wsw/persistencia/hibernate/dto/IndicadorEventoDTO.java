package mx.com.teclo.smm.wsw.persistencia.hibernate.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TMM055C_INDICADORES_EVENTOS")
public class IndicadorEventoDTO implements Serializable{

	private static final long serialVersionUID = -4446735727595987439L;
	
	@Id
	@Column(name="ID_INDICADOR_GPS")
	private Long idIndicadorGps;
	@Column(name="NU_TIEMPO_INICIAL")
	private Integer nuTiempoInicial;
	@Column(name="NU_TIEMPO_FINAL")
	private Integer nuTiempoFinal;
	@Column(name="CD_INDICADOR")
	private String cdIndicador;
	@Column(name="TX_INDICADOR")
	private String txIndicador;
	@Column(name="ST_ACTIVO")
	private Boolean stActivo;
	@Column(name="ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name="FH_CREACION")
	private Date fhCreacion;
	@Column(name="ID_USR_MODIFICA")
	private Long idUsrModifica;
	@Column(name="FH_MODIFICACION")
	private Date fhModificacion;
	
	public Long getIdIndicadorGps() {
		return idIndicadorGps;
	}
	public void setIdIndicadorGps(Long idIndicadorGps) {
		this.idIndicadorGps = idIndicadorGps;
	}
	public Integer getNuTiempoInicial() {
		return nuTiempoInicial;
	}
	public void setNuTiempoInicial(Integer nuTiempoInicial) {
		this.nuTiempoInicial = nuTiempoInicial;
	}
	public Integer getNuTiempoFinal() {
		return nuTiempoFinal;
	}
	public void setNuTiempoFinal(Integer nuTiempoFinal) {
		this.nuTiempoFinal = nuTiempoFinal;
	}
	public String getCdIndicador() {
		return cdIndicador;
	}
	public void setCdIndicador(String cdIndicador) {
		this.cdIndicador = cdIndicador;
	}
	public String getTxIndicador() {
		return txIndicador;
	}
	public void setTxIndicador(String txIndicador) {
		this.txIndicador = txIndicador;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
}
