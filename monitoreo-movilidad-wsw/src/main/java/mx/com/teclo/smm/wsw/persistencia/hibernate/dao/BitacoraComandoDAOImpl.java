package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.BitacoraComandoDTO;

@SuppressWarnings("unchecked")
@Repository("BitacoraComandoDAO")
public class BitacoraComandoDAOImpl extends BaseDAOImpl<BitacoraComandoDTO> implements BitacoraComandoDAO{

	@Override
	@Transactional(readOnly = true)
	public List<BitacoraComandoDTO> obtenerLogComando(Long idDispositivo){
		Criteria query = getCurrentSession().createCriteria(BitacoraComandoDTO.class);
		query.add(Restrictions.eq("idDispositivoDTO.idDispositivo", idDispositivo));	
		query.addOrder(Order.desc("idBitacora"));
		return (List<BitacoraComandoDTO>)query.list();
	}
	
	@Override
	@Transactional(readOnly = true)
	public BitacoraComandoDTO findbyIdBitacora(Long idBitacora) {
		Criteria query = getCurrentSession().createCriteria(BitacoraComandoDTO.class);
		query.add(Restrictions.eq("idBitacora", idBitacora));
		return (BitacoraComandoDTO)query.uniqueResult();
	}
}
