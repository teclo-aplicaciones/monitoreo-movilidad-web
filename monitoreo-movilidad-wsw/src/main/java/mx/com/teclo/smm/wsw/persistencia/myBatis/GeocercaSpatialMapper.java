package mx.com.teclo.smm.wsw.persistencia.myBatis;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface GeocercaSpatialMapper {
	String SDO_UTIL_DENTRO="SELECT sdo_geom.relate(THE_GEOM, 'ANYINTERACT', SDO_GEOMETRY(2001, 4326, NULL, "
						  +"SDO_ELEM_INFO_ARRAY(1,1,1), "
						  +"SDO_ORDINATE_ARRAY(#{lng},#{lat})), 0.05) "
						  +"FROM TMM013D_GL_GEOCERCAS WHERE ID_GEOCERCA = #{idGeo}";
	
	@Select(SDO_UTIL_DENTRO)
	public String validaPuntoEnForma(@Param("lng")Double lon, @Param("lat")Double lat, @Param("idGeo")Long idGeo);
}
