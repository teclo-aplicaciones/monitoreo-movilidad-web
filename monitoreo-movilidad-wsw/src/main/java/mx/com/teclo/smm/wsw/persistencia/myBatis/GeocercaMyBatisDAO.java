package mx.com.teclo.smm.wsw.persistencia.myBatis;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import mx.com.teclo.smm.wsw.persistencia.vo.geocerca.CoordGeoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.geocerca.GeocercaVO;

@Mapper
public interface GeocercaMyBatisDAO {
	
	//String SELECT_NEXT_VAL = "SELECT NVL(MAX(ID_GEOCERCA),0)+1 FROM TMM013D_GL_GEOCERCAS";
	
	String SELECT_NEXT_VAL = "SELECT SQTMM013D_GL_GEO.nextval FROM DUAL";
	
	
	//String VALIDATE_GEOM = "SELECT SDO_GEOM.VALIDATE_GEOMETRY_WITH_CONTEXT(THE_GEOM,) FROM TMM013D_GL_GEOCERCAS WHERE ID_GEOCERCA"

	String INSERT_CIRCLE = "INSERT INTO TMM013D_GL_GEOCERCAS VALUES(#{id}, #{tipo}, #{id}, #{nombre}, "
			  			 + "SDO_UTIL.CIRCLE_POLYGON(${coords}), "
			  			 + "#{descripcion}, #{color}, NULL, 1, SYSDATE, #{usuario}, SYSDATE, #{usuario})"; 
		
	String INSERT_POLYGON = "INSERT INTO TMM013D_GL_GEOCERCAS VALUES(#{id}, #{tipo}, #{id}, #{nombre}, "
			 			  + "SDO_GEOMETRY(2003,4326,NULL,SDO_ELEM_INFO_ARRAY(${config}), "
			 			  + "SDO_ORDINATE_ARRAY(${coords}))," 
			 			  + "#{descripcion}, #{color}, NULL, 1, SYSDATE, #{usuario}, SYSDATE, #{usuario})";

	String BUSCA_MODIFICA = "SELECT geo.ID_GEOCERCA as id, geo.NB_GEOCERCA as nombre, geo.ID_TIPO_GEOCERCAS as tipo, "
						  + "null as grupo, null as wkt, geo.ST_ACTIVO as activo, GEO.CD_COLOR as color, "
						  + "GEO.TX_GEOCERCA as descripcion, null as coords, null as dispositivos FROM TMM013D_GL_GEOCERCAS geo WHERE GEO.ID_GEOCERCA=#{id}";
	String BUSCA_MODIFICA_COORDS = "SELECT NU_LATITUD as lat, NU_LONGITUD as lng, NU_DISTANCIA as distance FROM TMM022D_GL_CORDENAS_GEO WHERE ID_GEOCERCAS =#{id} ORDER BY ID_CORDENADAS_GEO ASC";
	/*String INSERT_POLY = "INSERT INTO TMM013D_GL_GEOCERCAS VALUES(#{id}, #{tipo}, #{id}, #{nombre}, "
			  		   + "SDO_UTIL.FROM_WKTGEOMETRY('${wkt}'), " 
			  		   + "#{descripcion}, #{color}, 1, SYSDATE, #{usuario}, SYSDATE, #{usuario})";*/
	String INSERT_POLY = "INSERT INTO TMM013D_GL_GEOCERCAS VALUES(#{id}, #{tipo}, #{id}, #{nombre}, "
					   + "SDO_GEOMETRY(2003,4326,NULL,SDO_ELEM_INFO_ARRAY(1,1003,4), SDO_ORDINATE_ARRAY(1,1,2,2,3,3)), " 
					   + "#{descripcion}, #{color}, #{clob}, 1, SYSDATE, #{usuario}, SYSDATE, #{usuario})";
	
	//String INSERT_GEOM = "UPDATE TMM013D_GL_GEOCERCAS SET THE_GEOM = SDO_UTIL.FROM_WKTGEOMETRY(CD_CLOB_GEOM) WHERE ID_GEOCERCA = #{id}";
	
	String INSERT_GEOM = "UPDATE TMM013D_GL_GEOCERCAS SET THE_GEOM = SDO_GEOMETRY(CD_CLOB_GEOM, 4326)  WHERE ID_GEOCERCA = #{id}";
	
	String SRID_GEOM = "UPDATE TMM013D_GL_GEOCERCAS SET THE_GEOM = SDO_CS.TRANSFORM(THE_GEOM, 4326) WHERE ID_GEOCERCA = #{id}";
	
	String UPDATE_CIRCLE = "UPDATE TMM013D_GL_GEOCERCAS SET THE_GEOM = SDO_UTIL.CIRCLE_POLYGON(${coords}) WHERE ID_GEOCERCA = #{id}";
	
	String UPDATE_POLY = "UPDATE TMM013D_GL_GEOCERCAS SET CD_CLOB_GEOM = #{clob} WHERE ID_GEOCERCA = #{id}";
	
	
	//String UPDATE_SRID = "UPDATE TMM013D_GL_GEOCERCAS SET THE_GEOM.SDO_SRID = 4326 WHERE ID_GEOCERCA = #{id}";
	
	@Select(SELECT_NEXT_VAL)
	public Long buscarSiguienteIdGeocerca();
	
	@Select(BUSCA_MODIFICA)
	public GeocercaVO buscarGeocercaPorModificar(@Param("id") Long id);
	
	@Select(BUSCA_MODIFICA_COORDS)
	public List<CoordGeoVO> buscaGeocercaPorModificarCoord(@Param("id") Long id);
	
	@Insert(INSERT_CIRCLE)
	public void insertarGeocercaCirculo(@Param("id") Long id, @Param("tipo") Long tipo, 
										 @Param("nombre") String nombre, @Param("coords") String coords, 
										 @Param("descripcion") String descripcion, @Param("color") String color, @Param("usuario") Long usuario);
	
	@Insert(UPDATE_CIRCLE)
	public void actualizarGeocercaCirculo(@Param("id") Long id, @Param("coords") String coords);
	
	@Insert(UPDATE_POLY)
	public void actualizaGeocercaForma(@Param("id") Long id, @Param("clob")String clob);
	
	@Update(INSERT_GEOM)
	public void insertarGeom(@Param("id") Long id);
	
	@Update(SRID_GEOM)
	public void proyeccionGeom(@Param("id") Long id);
	
	@Update(INSERT_POLY)
	public void insertarGeocercaForma(@Param("id") Long id, @Param("tipo") Long tipo, 
										 @Param("nombre") String nombre, /*@Param("wkt") byte[] wkt,*/ 
										 @Param("descripcion") String descripcion, @Param("color") String color, @Param("usuario") Long usuario,@Param("clob")String clob);
	
	/*@Update(UPDATE_SRID)
	public void updateSRID(@Param("id") Long id);*/
	
	/*@Insert(INSERT_POLYGON)
	public void insertarGeocercaForma(@Param("id") Long id, @Param("tipo") Long tipo, 
			 						  @Param("nombre") String nombre, @Param("config") String config, @Param("coords") String coords, 
			 						  @Param("descripcion") String descripcion, @Param("color") String color, @Param("usuario") Long usuario);*/
	
	//public Integer validaFormaGeometrica
}
