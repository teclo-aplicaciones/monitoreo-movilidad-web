package mx.com.teclo.smm.wsw.negocio.service.notificacion;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import mx.com.teclo.notificacionespush.cliente.firebase.ClienteFirebaseService;
import mx.com.teclo.notificacionespush.message.firebase.Client;
import mx.com.teclo.notificacionespush.message.firebase.Push;
import mx.com.teclo.notificacionespush.message.firebase.ResponseFirebaseMessage;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.DispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.NotificacionDTO;
import mx.com.teclo.smm.wsw.util.enumerable.TemplatePushEnum;

@Service
public class NotificacionPushServiceImpl implements NotificacionPushService{
	
	@Value("${firebase.apikey}")
	private String API_KEY;
	
	@Autowired
	ClienteFirebaseService clienteFirebaseService; 

	@Override
	@SuppressWarnings("finally")
	public Boolean enviaNotificacionPushInmediata(DispositivoDTO dispo, NotificacionDTO notificacion, String base64){
		
		ResponseFirebaseMessage rfbm = null;
		
		Client cliente = new Client(dispo.getFbDispositivo());
		
		Push push = TemplatePushEnum.MENSAJE.getPush();
		
		//Agregamos los Clientes(Como solo hay 1 no es necesario la lista)
		push.setListDevice(null);
		
		//Agregamos la API KEY
		push.setApiKey(API_KEY);
		
		//Agregamos el Titulo
		push.getData().setTittle(notificacion.getNbNotificacion());
		//push.getData().setImage(base64);
		//push.getNotification().setTittle(notificacion.getNbNotificacion());
		//push.getNotification().setImage(base64);
		
		//Agregamos el Mensaje
		push.getData().setBody(notificacion.getTxMensaje());
		//push.getData().setMessage(notificacion.getTxMensaje());
		//push.getNotification().setBody(notificacion.getTxMensaje());
		//push.getNotification().setMessage(notificacion.getTxMensaje());
		
		//Agregamos el Action
		//push.getData().setAction("MessagesFragment");
		push.getData().setPriority("high");
		push.getData().setClickAction("OPEN_MAIN_ACTIVITY");
		push.getData().setAction(null);
		push.getData().setFragment("MessagesFragment");
		//push.getNotification().setAction("OPEN_MAIN_ACTIVITY");
		 
		try{
			rfbm = clienteFirebaseService.FBMNotificationClient(cliente, push, push.getApiKey());
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			return rfbm != null ? rfbm.getSuccess().equals("1") ? Boolean.FALSE: Boolean.TRUE : Boolean.TRUE;
		}
	}

	@Override
	public Boolean enviaNotificacionPushBatchInmediata(List<DispositivoDTO> listaDispos, NotificacionDTO notificacion){
		
		List<Client> listaClientes = new ArrayList<Client>();
		
		for(DispositivoDTO dispo : listaDispos)
		{
			Client cliente = new Client(dispo.getFbDispositivo());
			listaClientes.add(cliente);
		}
		Push push = TemplatePushEnum.MENSAJE.getPush();
		
		//Agregamos los Clientes
		push.setListDevice(listaClientes);
		
		//Agregamos la API KEY
		push.setApiKey(API_KEY);
		
		//Agregamos el Titulo
		push.getData().setTittle("[M&M] Monitoreo & Movilidad");
		//push.getNotification().setTittle("[M&M] Monitoreo & Movilidad");
		
		//Agregamos el Mensaje
		push.getData().setBody(notificacion.getTxMensaje());
		push.getData().setPriority("high");
		push.getData().setClickAction("OPEN_MAIN_ACTIVITY");
		push.getData().setFragment("MessagesFragment");
		//push.getData().setMessage(notificacion.getTxMensaje());
		//push.getNotification().setBody(notificacion.getTxMensaje());
		//push.getNotification().setMessage(notificacion.getTxMensaje());;
		
		ResponseFirebaseMessage rfbm = clienteFirebaseService.FBMNotificationBroadcastGroup(push.getListDevice(), push, push.getApiKey());
		
		return rfbm.getSuccess() != null ? Boolean.TRUE: Boolean.FALSE;
	}
}