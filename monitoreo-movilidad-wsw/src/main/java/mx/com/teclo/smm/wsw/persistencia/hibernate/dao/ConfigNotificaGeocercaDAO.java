package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.ConfigNotificaGeocercaDTO;

public interface ConfigNotificaGeocercaDAO extends BaseDAO<ConfigNotificaGeocercaDTO>{
	public List<ConfigNotificaGeocercaDTO> buscarCatalogo();
	public ConfigNotificaGeocercaDTO buscaCatalogoPorId(Long id);
}
