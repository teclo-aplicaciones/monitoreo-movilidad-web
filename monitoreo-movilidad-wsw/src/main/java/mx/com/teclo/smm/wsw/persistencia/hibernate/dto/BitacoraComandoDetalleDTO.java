package mx.com.teclo.smm.wsw.persistencia.hibernate.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * @author sinuhe
 *
 */
@Entity
@Table(name = "TMM066B_CM_BITACORA_DETALLE")
public class BitacoraComandoDetalleDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator="id_bitacora_detalle_generator",strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="id_bitacora_detalle_generator", sequenceName = "SQTMM066B_CM_BITA_DETA",allocationSize=1)
	@Column(name = "ID_BITACORA_DETALLE")	
	private Long idBitacoraDetalle;
	@Column(name = "ID_BITACORA")	
	private Long idBitacora;	
	@Column(name ="TX_BITACORA_DETALLE")
	private String txBitacora;
	@Column(name ="ID_BITACORA_STATUS")
	private Long idBitacoraStatus;	
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;
		
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
	
	public Long getIdBitacoraDetalle() {
		return idBitacoraDetalle;
	}
	public void setIdBitacoraDetalle(Long idBitacoraDetalle) {
		this.idBitacoraDetalle = idBitacoraDetalle;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}

	public Long getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(Long idBitacora) {
		this.idBitacora = idBitacora;
	}
	public String getTxBitacora() {
		return txBitacora;
	}
	public void setTxBitacora(String txBitacora) {
		this.txBitacora = txBitacora;
	}
	public Long getIdBitacoraStatus() {
		return idBitacoraStatus;
	}
	public void setIdBitacoraStatus(Long idBitacoraStatus) {
		this.idBitacoraStatus = idBitacoraStatus;
	}
	
}
	
	
