package mx.com.teclo.smm.wsw.persistencia.hibernate.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TMM005C_CT_MODELOS")
public class ModelosDTO implements Serializable{

	private static final long serialVersionUID = 4589080163376056752L;

	@Id
	@Column(name = "ID_MODELO")
	private Long idModelo;
	@ManyToOne
    @JoinColumn(name="ID_MARCA", nullable=false)
	private MarcasDTO idMarca;
	@ManyToOne
    @JoinColumn(name="ID_SUBTIPO_DISPOSITIVO", nullable=false)
	private SubtipoDispDTO idSubtipoDispositivo;
	@Column(name = "CD_MODELO")
	private String cdModelo;
	@Column(name = "NB_MODELO")
	private String nbModelo;
	@Column(name = "TX_MODELO")
	private String txModelo;
	
	@Column(name = "ST_ACTIVO")
	private Boolean stActivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;
	
	public Long getIdModelo() {
		return idModelo;
	}
	public void setIdModelo(Long idModelo) {
		this.idModelo = idModelo;
	}
	public MarcasDTO getIdMarca() {
		return idMarca;
	}
	public void setIdMarca(MarcasDTO idMarca) {
		this.idMarca = idMarca;
	}
	public SubtipoDispDTO getIdSubtipoDispositivo() {
		return idSubtipoDispositivo;
	}
	public void setIdSubtipoDispositivo(SubtipoDispDTO idSubtipoDispositivo) {
		this.idSubtipoDispositivo = idSubtipoDispositivo;
	}
	public String getCdModelo() {
		return cdModelo;
	}
	public void setCdModelo(String cdModelo) {
		this.cdModelo = cdModelo;
	}
	public String getNbModelo() {
		return nbModelo;
	}
	public void setNbModelo(String nbModelo) {
		this.nbModelo = nbModelo;
	}
	public String getTxModelo() {
		return txModelo;
	}
	public void setTxModelo(String txModelo) {
		this.txModelo = txModelo;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
	@Override
	public String toString() {
		return "ModelosDTO [idModelo=" + idModelo + ", idMarca=" + idMarca + ", idSubtipoDispositivo="
				+ idSubtipoDispositivo + ", cdModelo=" + cdModelo + ", nbModelo=" + nbModelo + ", txModelo=" + txModelo
				+ ", stActivo=" + stActivo + ", fhCreacion=" + fhCreacion + ", idUsrCreacion=" + idUsrCreacion
				+ ", fhModificacion=" + fhModificacion + ", idUsrModifica=" + idUsrModifica + "]";
	}
	
	
	
}
