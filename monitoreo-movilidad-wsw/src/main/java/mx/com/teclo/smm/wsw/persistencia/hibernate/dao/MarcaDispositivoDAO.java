package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.MarcaDispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.TipoDispositivoDTO;

public interface MarcaDispositivoDAO extends BaseDAO<MarcaDispositivoDTO>{

	public List<MarcaDispositivoDTO> catalogoMarca();
	public List<MarcaDispositivoDTO> buscarCatalogo(Long[] id);
	public MarcaDispositivoDTO activarDesactivarObtenerMarca(Long id);
	public MarcaDispositivoDTO obtenerMarca(Long id);
	public List<MarcaDispositivoDTO> buscarCatalogoPorTipo(Long[] id, TipoDispositivoDTO tdDTO);
	public List<MarcaDispositivoDTO> activarDesactivarBuscarCatalogo(Long[] id);
	public List<MarcaDispositivoDTO> activarDesactivarCatalogoMarca();
}
