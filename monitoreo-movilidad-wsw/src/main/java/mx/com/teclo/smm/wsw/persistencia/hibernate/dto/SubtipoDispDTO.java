package mx.com.teclo.smm.wsw.persistencia.hibernate.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TMM058D_DP_SUBTIPO_DIPOSITIVOS")
public class SubtipoDispDTO implements Serializable{

	private static final long serialVersionUID = 4688989702547142268L;
	@Id
	@Column(name = "ID_SUBTIPO_DISPOSITIVO")
	private Long idSubtipoDispositivo; 
	@ManyToOne
    @JoinColumn(name="ID_TIPO_DISPOSITIVO", nullable=false)
	private TipoDispositivosDTO idTipoDispositivo;
	@Column(name = "CD_SUBTIPO_DISPOSITIVO")
	private String cdSubtipoDispositivo;
	@Column(name = "NB_SUBTIPO_DISPOSITIVO")
	private String nbSubtipoDispositivo;
	@Column(name = "TX_SUBTIPO_DISPOSITIVO")
	private String txSubtipoDispositivo;
	
	@Column(name = "ST_ACTIVO")
	private Boolean stActivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;
	
	public Long getIdSubtipoDispositivo() {
		return idSubtipoDispositivo;
	}
	public void setIdSubtipoDispositivo(Long idSubtipoDispositivo) {
		this.idSubtipoDispositivo = idSubtipoDispositivo;
	}
	public TipoDispositivosDTO getIdTipoDispositivo() {
		return idTipoDispositivo;
	}
	public void setIdTipoDispositivo(TipoDispositivosDTO idTipoDispositivo) {
		this.idTipoDispositivo = idTipoDispositivo;
	}
	public String getCdSubtipoDispositivo() {
		return cdSubtipoDispositivo;
	}
	public void setCdSubtipoDispositivo(String cdSubtipoDispositivo) {
		this.cdSubtipoDispositivo = cdSubtipoDispositivo;
	}
	public String getNbSubtipoDispositivo() {
		return nbSubtipoDispositivo;
	}
	public void setNbSubtipoDispositivo(String nbSubtipoDispositivo) {
		this.nbSubtipoDispositivo = nbSubtipoDispositivo;
	}
	public String getTxSubtipoDispositivo() {
		return txSubtipoDispositivo;
	}
	public void setTxSubtipoDispositivo(String txSubtipoDispositivo) {
		this.txSubtipoDispositivo = txSubtipoDispositivo;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
	@Override
	public String toString() {
		return "SubtipoDispDTO [idSubtipoDispositivo=" + idSubtipoDispositivo + ", idTipoDispositivo="
				+ idTipoDispositivo + ", cdSubtipoDispositivo=" + cdSubtipoDispositivo + ", nbSubtipoDispositivo="
				+ nbSubtipoDispositivo + ", txSubtipoDispositivo=" + txSubtipoDispositivo + ", stActivo=" + stActivo
				+ ", fhCreacion=" + fhCreacion + ", idUsrCreacion=" + idUsrCreacion + ", fhModificacion="
				+ fhModificacion + ", idUsrModifica=" + idUsrModifica + "]";
	}
	
	
}
