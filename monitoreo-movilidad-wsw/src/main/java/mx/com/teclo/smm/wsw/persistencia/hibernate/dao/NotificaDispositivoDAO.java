package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.DispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.MedioNotificacionDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.NotificaDispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.NotificacionDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.TipoNotificacionDTO;

public interface NotificaDispositivoDAO extends BaseDAO<NotificaDispositivoDTO>{
	public List<NotificaDispositivoDTO> buscaRelacionNotificacionDispositivos(NotificacionDTO nDTO);
	public List<NotificaDispositivoDTO> buscaRelacionNotificacionDispositivo(NotificacionDTO nDTO, DispositivoDTO dDTO);
	
	public Long buscarSiguienteIdentificador();
	public Boolean validaPushEnviados(NotificacionDTO nDTO);
	public List<NotificaDispositivoDTO> buscarDispositivosGeocerca(DispositivoDTO dDTO);
	public List<NotificaDispositivoDTO> buscarDispositivosNotificacion(DispositivoDTO dDTO);
	public List<NotificaDispositivoDTO> buscarTodoDispositivosNotificacion(DispositivoDTO dDTO, TipoNotificacionDTO tNDTO, MedioNotificacionDTO mCDTO);
	public List<NotificaDispositivoDTO> buscarDispositivosEnNotificacion(NotificacionDTO nDTO);
	public List<NotificaDispositivoDTO> buscarTodoDispositivosEnNotificacion(NotificacionDTO nDTO);
}
