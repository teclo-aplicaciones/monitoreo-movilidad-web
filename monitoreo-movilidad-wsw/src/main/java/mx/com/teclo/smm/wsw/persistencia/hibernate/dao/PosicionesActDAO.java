package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.PosicionesActDTO;

public interface PosicionesActDAO extends BaseDAO<PosicionesActDTO>{
	
	public PosicionesActDTO findByIdDispositivo(Long idDispositivo);
	public PosicionesActDTO findByIdDispositivoAndImei(Long idDispositivo, Long nuImei);
	public PosicionesActDTO guardarPosicionActualCompleto(PosicionesActDTO posicionesActDTO);
	public PosicionesActDTO actualizarPosicionActualCompleto(PosicionesActDTO posicionesActDTO);
	
}
