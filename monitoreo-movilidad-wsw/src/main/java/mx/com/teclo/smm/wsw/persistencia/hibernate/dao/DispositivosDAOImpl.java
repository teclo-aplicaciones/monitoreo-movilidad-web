package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.arquitectura.persistencia.comun.dao.BaseDaoHibernate;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.DispositivoDTO;
@Repository
public class DispositivosDAOImpl extends BaseDaoHibernate<DispositivoDTO> implements DispositivosDAO{

	@Override
	public DispositivoDTO findByImei(Long nuImei) {
		Criteria query = getCurrentSession().createCriteria(DispositivoDTO.class);
		query.createAlias("idInformacionDisp", "idInformacionDisp");
		query.add(Restrictions.eq("idInformacionDisp.nuImei", nuImei));
		query.add(Restrictions.eq("idInformacionDisp.stActivo", Boolean.TRUE));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));

		List<DispositivoDTO> list = (List<DispositivoDTO>) query.list(); 
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

}
