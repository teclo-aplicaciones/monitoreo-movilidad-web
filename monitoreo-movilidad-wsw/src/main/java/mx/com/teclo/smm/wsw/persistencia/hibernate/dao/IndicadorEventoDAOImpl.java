package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.IndicadorEventoDTO;

@SuppressWarnings("unchecked")
@Repository("indicadorEventoDAO")
public class IndicadorEventoDAOImpl extends BaseDAOImpl<IndicadorEventoDTO> implements IndicadorEventoDAO{

	public List<IndicadorEventoDTO> buscarIndicadores()
	{
		Criteria query = getCurrentSession().createCriteria(IndicadorEventoDTO.class);
		query.addOrder(Order.asc("idIndicadorGps"));
		return (List<IndicadorEventoDTO>) query.list();
	}
}
