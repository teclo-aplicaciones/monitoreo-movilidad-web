package mx.com.teclo.smm.wsw.persistencia.hibernate.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="TMM007C_CT_TIPO_GEOCERCAS")
public class TipoGeocercaDTO implements Serializable{

	/**/
	private static final long serialVersionUID = 1940864744943832996L;

	@Id
	@GeneratedValue(generator="id_tgeo_generator",strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="id_tgeo_generator", sequenceName = "SQTMM007C_CT_TIPO_GEO",allocationSize=1)
	@Column(name="ID_TIPO_GEOCERCA")
	private Long idTipoGeocerca;
	@Column(name="CD_TIPO_GEOCERCA")
	private String cdTipoGeocerca;
	@Column(name="NB_TIPO_GEOCERCA")
	private String nbTipoGeocerca;
	@Column(name="TX_TIPO_GEOCERCA")
	private String txTipoGeocerca;
	@Column(name="ST_ACTIVO")
	private Boolean stActivo;
	@Column(name="FH_CREACION")
	private Date fhCreacion;
	@Column(name="ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name="FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name="ID_USR_MODIFICA")
	private Long idUsrModifica;
	
	public Long getIdTipoGeocerca() {
		return idTipoGeocerca;
	}
	public void setIdTipoGeocerca(Long idTipoGeocerca) {
		this.idTipoGeocerca = idTipoGeocerca;
	}
	public String getCdTipoGeocerca() {
		return cdTipoGeocerca;
	}
	public void setCdTipoGeocerca(String cdTipoGeocerca) {
		this.cdTipoGeocerca = cdTipoGeocerca;
	}
	public String getNbTipoGeocerca() {
		return nbTipoGeocerca;
	}
	public void setNbTipoGeocerca(String nbTipoGeocerca) {
		this.nbTipoGeocerca = nbTipoGeocerca;
	}
	public String getTxTipoGeocerca() {
		return txTipoGeocerca;
	}
	public void setTxTipoGeocerca(String txTipoGeocerca) {
		this.txTipoGeocerca = txTipoGeocerca;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
}

