package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.TipoGeocercaDTO;

public interface TipoGeocercaDAO extends BaseDAO<TipoGeocercaDTO>{
	public TipoGeocercaDTO buscarTipoGeocercaPorId(Long id);
	public List<TipoGeocercaDTO> buscarTipoGeocerca();
	public TipoGeocercaDTO activarDesactivarBuscarTipoGeocercaPorId(Long id);
	public List<TipoGeocercaDTO> activarDesactivarBuscarTipoGeocerca();
}
