package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.IndicadorEventoDTO;

public interface IndicadorEventoDAO extends BaseDAO<IndicadorEventoDTO>{
	public List<IndicadorEventoDTO> buscarIndicadores();
}
