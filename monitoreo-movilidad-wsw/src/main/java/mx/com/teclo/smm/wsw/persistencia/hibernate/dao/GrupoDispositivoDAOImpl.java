package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.DispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.GrupoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.GrupoDispositivoDTO;

@SuppressWarnings("unchecked")
@Repository("grupoDispositivoDAO")
public class GrupoDispositivoDAOImpl extends BaseDAOImpl<GrupoDispositivoDTO> implements GrupoDispositivoDAO {

	@Override
	public Long buscarSiguienteValor(){
		Criteria query = getCurrentSession().createCriteria(GrupoDispositivoDTO.class);
		query.setProjection(Projections.max("idDispGrupo"));
		Long res = (Long)query.uniqueResult();

		return res != null? res+1:1;
	}

	@Override
	public List<GrupoDispositivoDTO> buscarTodoDispositivosEnGrupos(){
		Criteria query = getCurrentSession().createCriteria(GrupoDispositivoDTO.class);
		query.createAlias("idGrupo", "idGrupo");
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		query.add(Restrictions.eq("idGrupo.idEmpresa", getIdEmpresa()));

		return query.list();
	}

	@Override
	public List<GrupoDispositivoDTO> buscarDispositivosEnGrupo(GrupoDTO grupo) {
		Criteria query = getCurrentSession().createCriteria(GrupoDispositivoDTO.class);
		query.add(Restrictions.eq("idGrupo", grupo));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));

		return query.list();
	}

	@Override
	public List<DispositivoDTO> buscarDispositivosPorGrupoId(Long idGrupo) {
		SQLQuery query = getCurrentSession().createSQLQuery("SELECT ID_DISPOSITIVO FROM TMM021D_DP_DISPOSITIVO_GRUPOS WHERE ID_GRUPO = ?");
		query.setParameter(0, idGrupo);

		List<BigDecimal> listaIds = query.list();

		//Lose, es incecesario pero que puedo decir, cosas de Hibernate...
		List<Long> listaLong = new ArrayList<Long>(listaIds.size());
		for(BigDecimal current:listaIds){
			listaLong.add(current.longValueExact());
		}

		if(!listaLong.isEmpty()){
			Criteria query2 = getCurrentSession().createCriteria(DispositivoDTO.class);
			query2.add(Restrictions.in("idDispositivo", listaLong));

			return query2.list();
		}else{
			return new ArrayList<DispositivoDTO>();
		}
	}

	@Override
	public GrupoDispositivoDTO buscarGrupoDeDispositivo(DispositivoDTO idDispositivo) {
		Criteria query = getCurrentSession().createCriteria(GrupoDispositivoDTO.class);
		query.add(Restrictions.eq("idDispositivo", idDispositivo));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));

		return (GrupoDispositivoDTO) query.uniqueResult();
	}

	@Override
	public List<GrupoDispositivoDTO> buscarGruposPorDispositivo(Long idDispositivo){
		Criteria query = getCurrentSession().createCriteria(GrupoDispositivoDTO.class);
		query.createAlias("idDispositivo", "dispo");
		query.createAlias("idGrupo", "grupo");
		query.add(Restrictions.eq("dispo.idDispositivo", idDispositivo));
		query.add(Restrictions.eq("grupo.stActivo", Boolean.TRUE));

		return query.list();
	}

	@Override
	public List<DispositivoDTO> buscarDispositivosSinGrupo(){
		SQLQuery query = getCurrentSession().createSQLQuery("SELECT ID_DISPOSITIVO FROM TMM021D_DP_DISPOSITIVO_GRUPOS");
		List<BigDecimal> listaIds = query.list();

		//Lose, es incecesario pero que puedo decir, cosas de Hibernate...
		List<Long> listaLong = new ArrayList<Long>(listaIds.size());
		for(BigDecimal current:listaIds){
			listaLong.add(current.longValueExact());
		}

		Criteria query2 = getCurrentSession().createCriteria(DispositivoDTO.class);
		if(!listaLong.isEmpty()) {
			query2.add(Restrictions.not(
				Restrictions.in("idDispositivo", listaLong)
			));
		}

		return query2.list();
	}

	@Override
	public Boolean eliminarRelacionGrupo(Long idGrupo){
		Criteria query = getCurrentSession().createCriteria(GrupoDispositivoDTO.class);
		query.createAlias("idGrupo", "grupo");
		query.add(Restrictions.eq("grupo.idGrupo", idGrupo));

		List<GrupoDispositivoDTO> lista = query.list();

		for (GrupoDispositivoDTO gDDTO : lista){
			this.delete(gDDTO);
		}
		return lista.isEmpty();
	}
}
