package mx.com.teclo.smm.wsw.negocio.service.notificacion;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.DispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.NotificacionDTO;

public interface NotificacionPushService {
	public Boolean enviaNotificacionPushInmediata(DispositivoDTO dispo, NotificacionDTO notificacion, String base64);
	public Boolean enviaNotificacionPushBatchInmediata(List<DispositivoDTO> listaDispos, NotificacionDTO notificacion);
}
