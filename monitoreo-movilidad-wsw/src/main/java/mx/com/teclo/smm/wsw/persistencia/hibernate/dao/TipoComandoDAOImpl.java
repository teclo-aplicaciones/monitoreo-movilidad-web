package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.TipoComandoDTO;

@SuppressWarnings("unchecked")
@Repository("TipoComandoDAO")
public class TipoComandoDAOImpl extends BaseDAOImpl<TipoComandoDTO> implements TipoComandoDAO{

	@Override
	@Transactional(readOnly = true)
	public List<TipoComandoDTO> obtenerTipoComando() {		
		Criteria query = getCurrentSession().createCriteria(TipoComandoDTO.class);
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return (List<TipoComandoDTO>)query.list();
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<TipoComandoDTO> activarDesactivarObtenerTipoComando() {		
		Criteria query = getCurrentSession().createCriteria(TipoComandoDTO.class);	
		query.addOrder(Order.asc("idTipoComando"));
		return (List<TipoComandoDTO>)query.list();
	}
	
	@Override
	@Transactional(readOnly = true)
	public TipoComandoDTO obtenerTipoComando(Long id) {		
		Criteria query = getCurrentSession().createCriteria(TipoComandoDTO.class);
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		query.add(Restrictions.eq("idTipoComando", id));
		return (TipoComandoDTO)query.uniqueResult();
	}
	
	@Override
	@Transactional(readOnly = true)
	public TipoComandoDTO activarDesactivarObtenerTipoComando(Long id) {		
		Criteria query = getCurrentSession().createCriteria(TipoComandoDTO.class);		
		query.add(Restrictions.eq("idTipoComando", id));
		return (TipoComandoDTO)query.uniqueResult();
	}

}
