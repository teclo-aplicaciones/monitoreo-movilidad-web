package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.BitacoraStatusDTO;


public interface BitacoraComandoStatusDAO extends BaseDAO<BitacoraStatusDTO>{
	public BitacoraStatusDTO obtenerBitacoraStatus(Long idStatus);
}
