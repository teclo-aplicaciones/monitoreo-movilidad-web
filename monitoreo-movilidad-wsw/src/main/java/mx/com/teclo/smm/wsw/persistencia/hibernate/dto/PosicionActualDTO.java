package mx.com.teclo.smm.wsw.persistencia.hibernate.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Column;
import org.hibernate.annotations.Type;
import org.springframework.data.geo.Point;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TMM054D_GL_POSICIONES_ACT")
public class PosicionActualDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1556276560931676031L;
	
	@Id
	@Column(name="ID_POSICIONES_ACT", nullable=false, unique=true)
	private Long idPosicionesAct;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_DISPOSITIVO")
	private DispositivoDTO idDispositivo;
	@Column(name="NU_LATITUD")
	private Double nuLatitud;
	@Column(name="NU_LONGITUD")
	private Double nuLongitud;
	@Column(name="NU_ALTITUD")
	private Double nuAltitud;
	//@Column(name="THE_GEOM")
	//@Type(type="org.hibernate.spatial.GeometryType") -2001 oracle locator
	//private Point theGeom;
	@Column(name="NU_IMEI")
	private String nuImei;
	@Column(name="NU_IP")
	private String nuIp;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_EVENTO")
	private EventoDTO idEvento;
	@Column(name="TX_DIRECCION")
	private String txDireccion;
	@Column(name="FH_REGISTRO")
	private Date fhRegistro;
	@Column(name="ST_ACTIVO")
	private Boolean stActivo;
	@Column(name="FH_CREACION")
	private Date fhCreacion;
	@Column(name="ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name="FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name="ID_USR_MODIFICA")
	private Long idUsrModifica;
	
	public Long getIdPosicionesAct() {
		return idPosicionesAct;
	}
	public String getNuImei() {
		return nuImei;
	}
	public void setNuImei(String nuImei) {
		this.nuImei = nuImei;
	}
	public String getNuIp() {
		return nuIp;
	}
	public void setNuIp(String nuIp) {
		this.nuIp = nuIp;
	}
	public EventoDTO getIdEvento() {
		return idEvento;
	}
	public void setIdEvento(EventoDTO idEvento) {
		this.idEvento = idEvento;
	}
	public String getTxDireccion() {
		return txDireccion;
	}
	public void setTxDireccion(String txDireccion) {
		this.txDireccion = txDireccion;
	}
	public Date getFhRegistro() {
		return fhRegistro;
	}
	public void setFhRegistro(Date fhRegistro) {
		this.fhRegistro = fhRegistro;
	}
	public void setIdPosicionesAct(Long idPosicionesAct) {
		this.idPosicionesAct = idPosicionesAct;
	}
	public DispositivoDTO getIdDispositivo() {
		return idDispositivo;
	}
	public void setIdDispositivo(DispositivoDTO idDispositivo) {
		this.idDispositivo = idDispositivo;
	}
	public Double getNuLatitud() {
		return nuLatitud;
	}
	public void setNuLatitud(Double nuLatitud) {
		this.nuLatitud = nuLatitud;
	}
	public Double getNuLongitud() {
		return nuLongitud;
	}
	public void setNuLongitud(Double nuLongitud) {
		this.nuLongitud = nuLongitud;
	}
	public Double getNuAltitud() {
		return nuAltitud;
	}
	public void setNuAltitud(Double nuAltitud) {
		this.nuAltitud = nuAltitud;
	}
	/*public Point getTheGeom() {
		return theGeom;
	}
	public void setTheGeom(Point theGeom) {
		this.theGeom = theGeom;
	}*/
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
}
