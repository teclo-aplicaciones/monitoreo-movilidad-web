package mx.com.teclo.smm.wsw.persistencia.hibernate.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TMM045D_EM_EMPRESA_USUARIO")
public class EmpresaUsuarioDTO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5596148704529318540L;


	@Id
	@GeneratedValue(generator="id_em_usuario",strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="id_em_usuario", sequenceName = "SQMM074B_EMPRESA_USUARIO",allocationSize=1)
	@Column(name = "ID_EM_USUARIO")
	private Long idEmUsuario;

	@Column(name = "ID_EMPRESA")
	private Long idEmpresa;

	@Column(name = "ID_USUARIO")
	private Long idUsuario;

	@Column(name = "ST_ACTIVO")
	private Integer stActivo;
	@Column(name = "FH_CREACION")
	private Date fhCreacion;
	@Column(name = "ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name = "FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name = "ID_USR_MODIFICA")
	private Long idUsrModifica;
	public Long getIdEmUsuario() {
		return idEmUsuario;
	}
	public void setIdEmUsuario(Long idEmUsuario) {
		this.idEmUsuario = idEmUsuario;
	}
	public Long getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public Integer getStActivo() {
		return stActivo;
	}
	public void setStActivo(Integer stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}




}
