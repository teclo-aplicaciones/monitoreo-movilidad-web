package mx.com.teclo.smm.wsw.rest.geocerca;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.com.teclo.arquitectura.ortogonales.exception.NotFoundException;
import mx.com.teclo.smm.wsw.negocio.service.dispositivo.DispositivoService;
import mx.com.teclo.smm.wsw.negocio.service.empleado.EmpleadoService;
import mx.com.teclo.smm.wsw.negocio.service.geocerca.GeocercaService;
import mx.com.teclo.smm.wsw.persistencia.vo.catalogo.CatalogoConSubtipoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.catalogo.CatalogoVO;
import mx.com.teclo.smm.wsw.persistencia.vo.geocerca.ConsultaGeocercaVO;
import mx.com.teclo.smm.wsw.persistencia.vo.geocerca.DetalleGeocercaVO;
import mx.com.teclo.smm.wsw.persistencia.vo.geocerca.DispositivoEnGeocercaVO;
import mx.com.teclo.smm.wsw.persistencia.vo.geocerca.GeocercaVO;
import mx.com.teclo.smm.wsw.util.enumerable.CatalogoGeocercaEnum;


@RestController
public class GeocercaRestController {

	@Autowired
	GeocercaService geocercaService;
	
	@Autowired
	EmpleadoService empleadoService;
	
	@Autowired
	DispositivoService dispositivoService;
	
	@RequestMapping("/buscarDispositivosEnGeocerca")	
	public ResponseEntity<List<CatalogoConSubtipoVO>> buscarDispositivoEnGeocerca(@RequestParam("id")String tipo)throws NotFoundException{
		List<CatalogoConSubtipoVO> listaDispos = new ArrayList<CatalogoConSubtipoVO>();
		
		Long[] listaIds = geocercaService.obtenerDispositivosEnGeocerca(Long.parseLong(tipo));
		
		if(listaIds.length > 0)
			listaDispos = dispositivoService.buscarDispositivoConSubTipoDispositivo(listaIds);
		
		return new ResponseEntity<List<CatalogoConSubtipoVO>>(listaDispos, HttpStatus.OK);
	}
	
	@RequestMapping("/buscarGeocercaDispositivos")	
	public ResponseEntity<List<DispositivoEnGeocercaVO>> buscarDispositivoParaGeocerca(){
		
		List<DispositivoEnGeocercaVO> lista = dispositivoService.buscarDispositivoParaGeocerca(null);
		
		return new ResponseEntity<List<DispositivoEnGeocercaVO>>(lista, HttpStatus.OK);
	}
	
	@RequestMapping("/buscarGeocercaAgrupamiento")
	public ResponseEntity<List<CatalogoVO>> buscarCatalogoAgrupamiento(@RequestParam("id")String tipo){
		List<CatalogoVO> x = new ArrayList<CatalogoVO>();
		
		if(Long.parseLong(tipo) == CatalogoGeocercaEnum.DISPOSITIVO.getIdCat()){
			x = dispositivoService.buscarDispositivo(null);
		}else if(Long.parseLong(tipo) == CatalogoGeocercaEnum.USUARIOS.getIdCat()){
			x = empleadoService.buscarEmpleado();
		}
		
		return new ResponseEntity<List<CatalogoVO>>(x, HttpStatus.OK);
	}
	
	@RequestMapping("/buscarGeocerca")
	public ResponseEntity<List<ConsultaGeocercaVO>> buscarGeocerca(@RequestParam("tipo")Long tipo, @RequestParam("valor")String valor) throws NotFoundException{
		List<ConsultaGeocercaVO> x = new ArrayList<ConsultaGeocercaVO>();
		
		x = geocercaService.consultarGeocercas(tipo, valor);
		
		return new ResponseEntity<List<ConsultaGeocercaVO>>(x, HttpStatus.OK);
	}
	
	@RequestMapping(value="/buscarGeocercaPorId", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GeocercaVO> buscarGeocercaPorId(@RequestParam("id")String id){
		GeocercaVO x = new GeocercaVO();
		//Se modificó la funcion de buscarGeocercaPorId de GeocercaVO a GeocercaDTO
		x = geocercaService.buscarGeocercaPorModificar(Long.parseLong(id));
		
		return new ResponseEntity<GeocercaVO>(x, HttpStatus.OK);
	}
	
	@RequestMapping("/cambiarEstadoGeocerca")
	public ResponseEntity<Boolean> cambiarEstadoGeocerca(@RequestParam("id")Long id){
		Boolean res = geocercaService.cambiarEstadoGeocerca(id);
		return new ResponseEntity<Boolean>(res, HttpStatus.OK);
	}
	
	@RequestMapping("/buscarDetalleGeocerca")
	public ResponseEntity<List<DetalleGeocercaVO>> buscaDetalleGeocerca(@RequestParam("id")Long id){
		List<DetalleGeocercaVO> res = geocercaService.buscaDetalleGeocerca(id);
		return new ResponseEntity<List<DetalleGeocercaVO>>(res, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value="/guardarGeocerca", method = RequestMethod.PUT)
	public ResponseEntity<Boolean> guardarGeocerca(@RequestBody GeocercaVO geocercaVO)throws ParseException{
		Boolean res = geocercaService.guardarGeocerca(geocercaVO);
		return new ResponseEntity<Boolean>(res, HttpStatus.OK);
	}
	
	@RequestMapping(value="/actualizarGeocerca", method = RequestMethod.PUT)
	public ResponseEntity<Boolean> actualizarGeocerca(@RequestBody GeocercaVO geocercaVO)throws ParseException{
		Boolean res = geocercaService.actualizarGeocerca(geocercaVO);
		return new ResponseEntity<Boolean>(res, HttpStatus.OK);
	}
	
	@RequestMapping("/eliminaGeocercaPerma")
	public ResponseEntity<Boolean> eliminaGeocercaPerma(@RequestParam("id")Long id) throws NotFoundException{
		Boolean res = geocercaService.eliminaGeocercaPerma(id);
		return new ResponseEntity<Boolean>(res, HttpStatus.OK);
	}
}
