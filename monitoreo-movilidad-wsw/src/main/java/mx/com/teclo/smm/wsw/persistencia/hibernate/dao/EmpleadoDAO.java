package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.EmpleadoDTO;

public interface EmpleadoDAO extends BaseDAO<EmpleadoDTO>{

	public List<EmpleadoDTO> buscarEmpleados(String param, String value);
	public EmpleadoDTO buscarEmpleado(Long id);
}
