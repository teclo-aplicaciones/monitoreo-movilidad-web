package mx.com.teclo.smm.wsw.persistencia.hibernate.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TMM016D_DP_DISP_EMPLEADOS")
public class DispoEmpleadoDTO implements Serializable{

	private static final long serialVersionUID = 3163448139883705256L;

	
	@Id
	@Column(name="ID_DISP_EMP")
	private Long idDispEmp;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_DISPOSITIVO")
	private DispositivoDTO idDispositivo;
	@ManyToOne
	@JoinColumn(name="ID_EMPLEADO")
	private EmpleadoDTO idEmpleado;
	@Column(name="ST_ACTIVO")
	private Boolean stActivo;
	@Column(name="FH_CREACION")
	private Date fhCreacion;
	@Column(name="ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name="FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name="ID_USR_MODIFICA")
	private Long idUsrModifica;
	
	public Long getIdDispEmp() {
		return idDispEmp;
	}
	public void setIdDispEmp(Long idDispEmp) {
		this.idDispEmp = idDispEmp;
	}
	public DispositivoDTO getIdDispositivo() {
		return idDispositivo;
	}
	public void setIdDispositivo(DispositivoDTO idDispositivo) {
		this.idDispositivo = idDispositivo;
	}
	public EmpleadoDTO getIdEmpleado() {
		return idEmpleado;
	}
	public void setIdEmpleado(EmpleadoDTO idEmpleado) {
		this.idEmpleado = idEmpleado;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
}

