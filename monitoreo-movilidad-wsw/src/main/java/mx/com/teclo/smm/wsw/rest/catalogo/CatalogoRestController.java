package mx.com.teclo.smm.wsw.rest.catalogo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import mx.com.teclo.smm.wsw.negocio.service.catalogo.CatalogoService;
import mx.com.teclo.smm.wsw.persistencia.vo.catalogo.CatalogoVO;

@RestController
public class CatalogoRestController {

	@Autowired
	CatalogoService catalogoService;
	
	@RequestMapping(value="/catalogo/monitoreo",method = RequestMethod.GET)
	public ResponseEntity<List<CatalogoVO>> catalogoMonitoreo() {
		List<CatalogoVO> catalogo = this.catalogoService.catalogoMonitoreo();

		return new ResponseEntity<List<CatalogoVO>>(catalogo, HttpStatus.OK);
	}
}
