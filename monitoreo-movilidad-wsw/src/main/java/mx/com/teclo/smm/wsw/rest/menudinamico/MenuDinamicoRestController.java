package mx.com.teclo.smm.wsw.rest.menudinamico;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import mx.com.teclo.arquitectura.ortogonales.persistencia.hibernate.dto.seguridad.MenusDTO;
import mx.com.teclo.arquitectura.ortogonales.seguridad.vo.UsuarioFirmadoVO;
import mx.com.teclo.arquitectura.ortogonales.service.comun.UsuarioFirmadoService;
import mx.com.teclo.arquitectura.ortogonales.service.menus.MenusService;
import mx.com.teclo.smm.wsw.persistencia.vo.MenuDinamicoVO;

@RestController
@RequestMapping("/menudinamico")
public class MenuDinamicoRestController {

	@Autowired
	private MenusService menusService;
	
	@Autowired
	private UsuarioFirmadoService usuarioFirmadoService;
	
	@Value("${app.config.codigo}")
	private String codeApplication;

		@RequestMapping(value = "/menus", method = RequestMethod.GET)
		public ResponseEntity<List<MenuDinamicoVO>> buscarMenuUsuario() throws NotFoundException {
			
			UsuarioFirmadoVO usuarioFirmado = usuarioFirmadoService.getUsuarioFirmadoVO();
			List<MenusDTO> menus = menusService.buscarMenuUsuario(usuarioFirmado.getPerfilId(),codeApplication);
			//List<MenuDinamicoVO> menusVO = ResponseConverter.converterLista(new ArrayList<>(), menus, MenuDinamicoVO.class);
			List<MenuDinamicoVO> menusVO = new ArrayList<>();
			
			for(MenusDTO menuDTO:menus){
				
				MenuDinamicoVO menuVO=new MenuDinamicoVO();
				
				menuVO.setId(menuDTO.getIdMenu().intValue());
				menuVO.setMenuIcono(menuDTO.getNbMenuIcono());
				menuVO.setMenuSuperior(menuDTO.getNuMenuSuperior().intValue());
				menuVO.setMenuTexto(menuDTO.getTxMenu());
				menuVO.setMenuTextoEn(menuDTO.getTxMenuEn());
				menuVO.setMenuUri(menuDTO.getNbMenuUri());
				menuVO.setNbMenuUrl(menuDTO.getNbMenuUrl());
				menusVO.add(menuVO);
			}
			
			if (menus.isEmpty()) {
				throw new NotFoundException("No se encontraron menus con los datos informados.");
			}	
			
			return new ResponseEntity<List<MenuDinamicoVO>>(menusVO, HttpStatus.OK);
		}
	
}
