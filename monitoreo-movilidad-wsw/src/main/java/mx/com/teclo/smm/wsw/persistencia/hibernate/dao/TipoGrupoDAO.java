package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.TipoGrupoDTO;

public interface TipoGrupoDAO extends BaseDAO<TipoGrupoDTO>{
	public TipoGrupoDTO buscarTipoGrupoPorId(Long idTipoGrupo);
}
