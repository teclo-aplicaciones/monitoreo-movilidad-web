package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.BitacoraComandoDTO;


public interface BitacoraComandoDAO extends BaseDAO<BitacoraComandoDTO>{
	public List<BitacoraComandoDTO> obtenerLogComando(Long idDispositivo);	
	public BitacoraComandoDTO findbyIdBitacora(Long idBitacora);	
}
