package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.BitacoraComandoDetalleArchivoDTO;


public interface BitacoraComandoDetalleArchivoDAO extends BaseDAO<BitacoraComandoDetalleArchivoDTO>{

	public BitacoraComandoDetalleArchivoDTO obtenerArchivo(Long idBitacora);
	public BitacoraComandoDetalleArchivoDTO obtenerArchivoPorIdArchivo(Long idArchivo);	
}
