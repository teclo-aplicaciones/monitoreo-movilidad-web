package mx.com.teclo.smm.wsw.rest.notificacion;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import mx.com.teclo.smm.wsw.negocio.service.notificacion.NotificacionService;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.NotificacionDTO;
import mx.com.teclo.smm.wsw.persistencia.vo.notificacion.NotificacionVO;

@RestController
public class NotificacionRestController {
	
	@Autowired
	NotificacionService notificacionService;

	@RequestMapping(value = "/guardarNotificacion", method = RequestMethod.PUT)	
	public ResponseEntity<Boolean> guardarNotificacion(@RequestBody NotificacionVO notificacionVO)
			throws ParseException {
		NotificacionDTO res = notificacionService.guardarNotificacion(notificacionVO);
		return new ResponseEntity<Boolean>(res != null ? Boolean.TRUE : Boolean.FALSE, HttpStatus.OK);
	}

}
