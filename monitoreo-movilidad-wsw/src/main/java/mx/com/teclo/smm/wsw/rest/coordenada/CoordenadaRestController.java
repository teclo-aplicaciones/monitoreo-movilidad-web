package mx.com.teclo.smm.wsw.rest.coordenada;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import mx.com.teclo.arquitectura.ortogonales.responsehttp.ConflictHttpResponse;
import mx.com.teclo.arquitectura.ortogonales.responsehttp.OKHttpResponse;
import mx.com.teclo.smm.wsw.negocio.service.coordenadas.CoordenadaService;
import mx.com.teclo.smm.wsw.negocio.service.geocerca.GeocercaService;
import mx.com.teclo.smm.wsw.persistencia.vo.PosicionesActVO;

@RestController
public class CoordenadaRestController {
//	private static final Logger logger = Logger.getLogger(CoordenadaRestController.class);
	Logger logger = LogManager.getLogger(CoordenadaRestController.class);
	@Autowired 
	private CoordenadaService coordenadaService;
	
	@RequestMapping(value="/coordenada", method=RequestMethod.POST)
	public ResponseEntity<PosicionesActVO> agregarCoordenada(@RequestBody PosicionesActVO posicionesActVO) throws OKHttpResponse, ConflictHttpResponse{
		
 
		logger.info("::>" + posicionesActVO);
		PosicionesActVO posicionesActRet = coordenadaService.agregarCoordenada(posicionesActVO);
		if(posicionesActRet == null)
			throw new ConflictHttpResponse("No se encontró el registro del Dispositivo.","TCL40901",posicionesActRet);
		else
			throw new OKHttpResponse("Se registró correctamente las coordenadas.","TCL20001",posicionesActRet);
	}

}
