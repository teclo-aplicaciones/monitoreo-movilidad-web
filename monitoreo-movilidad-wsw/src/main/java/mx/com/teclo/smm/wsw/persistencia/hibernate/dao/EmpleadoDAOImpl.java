package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.EmpleadoDTO;

@Repository("empleadoDAO")
public class EmpleadoDAOImpl extends BaseDAOImpl<EmpleadoDTO> implements EmpleadoDAO{

	@Override
	public List<EmpleadoDTO> buscarEmpleados(String param, String value) {
		Criteria query = getCurrentSession().createCriteria(EmpleadoDTO.class);
		if(param.equals("cdEmpleado"))
			query.add(Restrictions.eq("cdEmpleado", value).ignoreCase());
		else if(param.equals("nombreEmpleado"))
			query.add(Restrictions.like("nbEmpleado", "%" + value + "%").ignoreCase());
		else if(param.equals("apellidoEmpleado")) 
			query.add(Restrictions.or(Restrictions.like("nbPaterno", "%" + value + "%").ignoreCase(),Restrictions.like("nbMaterno", "%" + value + "%").ignoreCase()));
		else if(param.equals("emailEmpleado"))
			query.add(Restrictions.like("nbMail", "%" + value + "%").ignoreCase());		
		else if(param.equals("telefonoEmpleado")) {
			try {
			Long num = Long.parseLong(value);
			query.add(Restrictions.eq("nuTelefono", num));
			}catch(Exception e) {
				return new ArrayList<>();
			}
		}		
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		
		List<EmpleadoDTO> lista = (List<EmpleadoDTO>)query.list();
		return lista;
	}
	
	@Override
	public EmpleadoDTO buscarEmpleado(Long id){
		Criteria query = getCurrentSession().createCriteria(EmpleadoDTO.class);
		query.add(Restrictions.eq("idEmpleado", id));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return (EmpleadoDTO)query.uniqueResult();
		
	}
}
