package mx.com.teclo.smm.wsw.persistencia.vo;

import java.io.Serializable;

public class EventoVO implements Serializable{

	private static final long serialVersionUID = 5300566703092135044L;
	private Long idEvento;
	private Long idTipoEvento;
	private Long idSubtipoDispositivo;
	private String cdEvento;
	private String nbEvento;
	private String txEvento;
	private String txParametro;
	private Boolean stActivo;
	
	public EventoVO() {}
	
	public EventoVO(Long idEvento, Long idTipoEvento, Long idSubtipoDispositivo, String cdEvento, String nbEvento,
			String txEvento, String txParametro, Boolean stActivo) {
		super();
		this.idEvento = idEvento;
		this.idTipoEvento = idTipoEvento;
		this.idSubtipoDispositivo = idSubtipoDispositivo;
		this.cdEvento = cdEvento;
		this.nbEvento = nbEvento;
		this.txEvento = txEvento;
		this.txParametro = txParametro;
		this.stActivo = stActivo;				
	}
	
	public Boolean getStActivo() {
		return stActivo;
	}

	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}

	public String getTxParametro() {
		return txParametro;
	}

	public void setTxParametro(String txParametro) {
		this.txParametro = txParametro;
	}

	public Long getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(Long idEvento) {
		this.idEvento = idEvento;
	}

	public Long getIdTipoEvento() {
		return idTipoEvento;
	}

	public void setIdTipoEvento(Long idTipoEvento) {
		this.idTipoEvento = idTipoEvento;
	}

	public Long getIdSubtipoDispositivo() {
		return idSubtipoDispositivo;
	}

	public void setIdSubtipoDispositivo(Long idSubtipoDispositivo) {
		this.idSubtipoDispositivo = idSubtipoDispositivo;
	}

	public String getCdEvento() {
		return cdEvento;
	}

	public void setCdEvento(String cdEvento) {
		this.cdEvento = cdEvento;
	}

	public String getNbEvento() {
		return nbEvento;
	}

	public void setNbEvento(String nbEvento) {
		this.nbEvento = nbEvento;
	}

	public String getTxEvento() {
		return txEvento;
	}

	public void setTxEvento(String txEvento) {
		this.txEvento = txEvento;
	}
	
	
	
}
