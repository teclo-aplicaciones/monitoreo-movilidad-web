package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.PosicionesActDTO;
@Repository
public class PosicionesActDAOImpl extends BaseDAOImpl<PosicionesActDTO> implements PosicionesActDAO {

	@Override
	public PosicionesActDTO findByIdDispositivo(Long idDispositivo) {
		Criteria query = getCurrentSession().createCriteria(PosicionesActDTO.class);
		query.createAlias("idDispositivo", "idDispositivo");
		query.add(Restrictions.eq("idDispositivo.idDispositivo", idDispositivo));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
	
		List<PosicionesActDTO> list = (List<PosicionesActDTO>) query.list(); 
		if(list.size() > 0)
			return list.get(0);
		return null;
	}
	
	public PosicionesActDTO findByIdDispositivoAndImei(Long idDispositivo, Long nuImei) {
		Criteria query = getCurrentSession().createCriteria(PosicionesActDTO.class);
		query.createAlias("idDispositivo", "idDispositivo");
		query.add(Restrictions.eq("idDispositivo.idDispositivo", idDispositivo));
		query.add(Restrictions.eq("nuImei", nuImei));
		query.add(Restrictions.eq("idDispositivo.stActivo", Boolean.TRUE));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
	
		List<PosicionesActDTO> list = (List<PosicionesActDTO>) query.list(); 
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

	@Override
	public PosicionesActDTO guardarPosicionActualCompleto(PosicionesActDTO posicionesActDTO) {
		
		
		SQLQuery insertQuery = getSession().createSQLQuery("INSERT INTO TMM054D_GL_POSICIONES_ACT("
				+ "ID_POSICIONES_ACT, ID_DISPOSITIVO, ID_EVENTO, NU_LATITUD, NU_LONGITUD, NU_ALTITUD, THE_GEOM, NU_IMEI,"
				+ " NU_IP, TX_DIRECCION, FH_REGISTRO, ST_ACTIVO, FH_CREACION, ID_USR_CREACION, FH_MODIFICACION, ID_USR_MODIFICA) "
				+ "VALUES(SQTMM054D_GL_POSI.NEXTVAL, ?, ?, ?, ?, ?, SDO_GEOMETRY(2001, NULL, SDO_POINT_TYPE(?, ?, ?), NULL, NULL),"
				+ "?, ?, ?, ?, ?, ?, ?, ?, ?)");
		insertQuery.setParameter(0, posicionesActDTO.getIdDispositivo().getIdDispositivo());
		insertQuery.setParameter(1, posicionesActDTO.getIdEvento());
		insertQuery.setParameter(2, posicionesActDTO.getNuLatitud());
		insertQuery.setParameter(3, posicionesActDTO.getNuLongitud());
		insertQuery.setParameter(4, posicionesActDTO.getNuAltitud());
		insertQuery.setParameter(5, posicionesActDTO.getNuLatitud());
		insertQuery.setParameter(6, posicionesActDTO.getNuLongitud());
		insertQuery.setParameter(7, posicionesActDTO.getNuAltitud());
		insertQuery.setParameter(8, posicionesActDTO.getNuImei());
		insertQuery.setParameter(9, posicionesActDTO.getNuIp());
		insertQuery.setParameter(10, posicionesActDTO.getTxDireccion());
		insertQuery.setParameter(11, posicionesActDTO.getFhRegistro());
		insertQuery.setParameter(12, posicionesActDTO.getStActivo());
		insertQuery.setParameter(13, posicionesActDTO.getFhCreacion());
		insertQuery.setParameter(14, posicionesActDTO.getIdUsrCreacion());
		insertQuery.setParameter(15, posicionesActDTO.getFhModificacion());
		insertQuery.setParameter(16, posicionesActDTO.getIdUsrModifica());
		int res = insertQuery.executeUpdate();
		
		if(res == - 1)
			return null;
		posicionesActDTO.setIdPosicionesAct(new Long(res));
		return posicionesActDTO;
		
	}
	
				
	@Override
	public PosicionesActDTO actualizarPosicionActualCompleto(PosicionesActDTO posicionesActDTO) {
		
		SQLQuery insertQuery = getSession().createSQLQuery("UPDATE TMM054D_GL_POSICIONES_ACT"
				+ " SET ID_DISPOSITIVO = ?,"
				+ " ID_EVENTO = ?,"				
				+ " NU_LATITUD = ?,"
				+ " NU_LONGITUD = ?,"
				+ " NU_ALTITUD = ?,"
				+ " THE_GEOM = SDO_GEOMETRY(2001, NULL, SDO_POINT_TYPE(?, ?, ?), NULL, NULL),"
				+ " NU_IMEI = ?,"
				+ " NU_IP = ?,"
				+ " TX_DIRECCION = ?,"
				+ " FH_REGISTRO = ?,"
				+ " ST_ACTIVO = ?,"
				+ " FH_CREACION = ?,"
				+ " ID_USR_CREACION = ?,"
				+ " FH_MODIFICACION = ?,"
				+ " ID_USR_MODIFICA = ? WHERE ID_POSICIONES_ACT = ?");
		insertQuery.setParameter(0, posicionesActDTO.getIdDispositivo().getIdDispositivo());
		insertQuery.setParameter(1, posicionesActDTO.getIdEvento());
		insertQuery.setParameter(2, posicionesActDTO.getNuLatitud());
		insertQuery.setParameter(3, posicionesActDTO.getNuLongitud());
		insertQuery.setParameter(4, posicionesActDTO.getNuAltitud());
		insertQuery.setParameter(5, posicionesActDTO.getNuLatitud());
		insertQuery.setParameter(6, posicionesActDTO.getNuLongitud());
		insertQuery.setParameter(7, posicionesActDTO.getNuAltitud());
		insertQuery.setParameter(8, posicionesActDTO.getNuImei());
		insertQuery.setParameter(9, posicionesActDTO.getNuIp());
		insertQuery.setParameter(10, posicionesActDTO.getTxDireccion());
		insertQuery.setParameter(11, posicionesActDTO.getFhRegistro());
		insertQuery.setParameter(12, posicionesActDTO.getStActivo());
		insertQuery.setParameter(13, posicionesActDTO.getFhCreacion());
		insertQuery.setParameter(14, posicionesActDTO.getIdUsrCreacion());
		insertQuery.setParameter(15, posicionesActDTO.getFhModificacion());
		insertQuery.setParameter(16, posicionesActDTO.getIdUsrModifica());
		insertQuery.setParameter(17, posicionesActDTO.getIdPosicionesAct());
		int ret = insertQuery.executeUpdate();
		
		if(ret == -1)
			return null;
		return posicionesActDTO;
	}

}