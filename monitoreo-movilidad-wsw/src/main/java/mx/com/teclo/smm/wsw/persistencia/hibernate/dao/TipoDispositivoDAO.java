package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.TipoDispositivoDTO;

public interface TipoDispositivoDAO extends BaseDAO<TipoDispositivoDTO>{

	public List<TipoDispositivoDTO> buscarCatalogo();
	public TipoDispositivoDTO buscarCatalogoPorTipo(Long id);
	public TipoDispositivoDTO activarDesactivarBuscarCatalogoPorTipo(Long id);
	public List<TipoDispositivoDTO> activarDesactivarBuscarCatalogo();
}
