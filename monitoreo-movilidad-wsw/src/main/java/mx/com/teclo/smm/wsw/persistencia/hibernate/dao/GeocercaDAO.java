package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.GeocercaDTO;

public interface GeocercaDAO extends BaseDAO<GeocercaDTO>{
	public List<GeocercaDTO> buscarGeocercas();
	public GeocercaDTO buscarGrupoPorId(Long id);
	public GeocercaDTO buscarGeocercaPorId(Long id);
	public List<GeocercaDTO> buscarGeocercasPorNombre(String nombre);
}
