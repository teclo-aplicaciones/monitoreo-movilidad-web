package mx.com.teclo.smm.wsw.negocio.service.comando;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import mx.com.teclo.smm.wsw.persistencia.hibernate.dao.BitacoraComandoDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dao.BitacoraComandoDetalleArchivoDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dao.BitacoraComandoDetalleDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.BitacoraComandoDetalleArchivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.BitacoraComandoDetalleDTO;

@Service
public class BitacoraComandoDetalleServiceImpl implements BitacoraComandoDetalleService{

	@Autowired
	private BitacoraComandoDetalleDAO bitacoraComandoDetalleDAO;
	
	@Autowired
	private BitacoraComandoDetalleArchivoDAO bitacoraComandoDetalleArchivoDAO;

	@Autowired
	private BitacoraComandoDAO bitacoraComandoDAO;
	
	@Override
	@Transactional
	public boolean guardarBitacoraComandoDetalle(Long idBitacora,String txBitacoraDetalle) {

		BitacoraComandoDetalleDTO bitacoraComandoDetalleDTO = new BitacoraComandoDetalleDTO();
		bitacoraComandoDetalleDTO.setFhCreacion(new Date());
		bitacoraComandoDetalleDTO.setIdBitacora(idBitacora);
		bitacoraComandoDetalleDTO.setTxBitacora(txBitacoraDetalle);
		bitacoraComandoDetalleDTO.setIdBitacoraStatus(3L);
		bitacoraComandoDetalleDTO.setFhModificacion(new Date());
		bitacoraComandoDetalleDTO.setIdUsrCreacion(1L);
		bitacoraComandoDetalleDTO.setIdUsrModifica(1L);	
		bitacoraComandoDetalleDAO.save(bitacoraComandoDetalleDTO);
		return true;
	}
	
	@Override
	@Transactional
	public boolean guardarBitacoraComandoDetalleArchivo(Long idBitacora,String txBitacoraDetalle, MultipartFile multipartFile) throws Exception {		
		if(guardarBitacoraComandoDetalle(idBitacora,txBitacoraDetalle)) {
			BitacoraComandoDetalleArchivoDTO bitacoraComandoDetalleArchivoDTO = new BitacoraComandoDetalleArchivoDTO();
			bitacoraComandoDetalleArchivoDTO.setBitacoraComandoDTO(bitacoraComandoDAO.findbyIdBitacora(idBitacora));		
			bitacoraComandoDetalleArchivoDTO.setTxTipoArchivo(multipartFile.getOriginalFilename().split("\\.")[1]);
			bitacoraComandoDetalleArchivoDTO.setTxFormatoArchivo(multipartFile.getContentType());
			bitacoraComandoDetalleArchivoDTO.setNombreArchivo(multipartFile.getOriginalFilename());
			bitacoraComandoDetalleArchivoDTO.setArchivo(multipartFile.getBytes());
			bitacoraComandoDetalleArchivoDTO.setFhCreacion(new Date());
			bitacoraComandoDetalleArchivoDAO.save(bitacoraComandoDetalleArchivoDTO);
			return true;	
		}				
		return false;		
	}

}
