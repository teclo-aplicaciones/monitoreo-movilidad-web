package mx.com.teclo.smm.wsw.persistencia.hibernate.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="TMM022D_GL_CORDENAS_GEO")
public class GeocercaCoordenadaDTO implements Serializable{

	private static final long serialVersionUID = -7890687211809311903L;
	
	@Id
	@Column(name="ID_CORDENADAS_GEO")
	private Long idCordenadasGeo;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_GEOCERCAS")
	private GeocercaDTO idGeocercas;
	@Column(name="NU_LATITUD")
	private Double nuLatitud;
	@Column(name="NU_LONGITUD")
	private Double nuLongitud;
	@Column(name="NU_DISTANCIA")
	private Double nuDistancia;
	@Column(name="ST_ACTIVO")
	private Boolean stActivo;
	@Column(name="FH_CREACION")
	private Date fhCreacion;
	@Column(name="ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name="FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name="ID_USR_MODIFICA")
	private Long idUsrModifica;
	
	public Long getIdCordenadasGeo() {
		return idCordenadasGeo;
	}
	public void setIdCordenadasGeo(Long idCordenadasGeo) {
		this.idCordenadasGeo = idCordenadasGeo;
	}
	public GeocercaDTO getIdGeocercas() {
		return idGeocercas;
	}
	public void setIdGeocercas(GeocercaDTO idGeocercas) {
		this.idGeocercas = idGeocercas;
	}
	public Double getNuLatitud() {
		return nuLatitud;
	}
	public void setNuLatitud(Double nuLatitud) {
		this.nuLatitud = nuLatitud;
	}
	public Double getNuLongitud() {
		return nuLongitud;
	}
	public void setNuLongitud(Double nuLongitud) {
		this.nuLongitud = nuLongitud;
	}
	public Double getNuDistancia() {
		return nuDistancia;
	}
	public void setNuDistancia(Double nuDistancia) {
		this.nuDistancia = nuDistancia;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
}
