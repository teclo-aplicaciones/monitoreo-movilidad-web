package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.TipoDispositivoDTO;

@SuppressWarnings("unchecked")
@Repository("tipoDispositivoDAO")
public class TipoDispositivoDAOImpl extends BaseDAOImpl<TipoDispositivoDTO> implements TipoDispositivoDAO{

	
	@Override
	public List<TipoDispositivoDTO> buscarCatalogo()
	{
		Criteria query = getCurrentSession().createCriteria(TipoDispositivoDTO.class);
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return (List<TipoDispositivoDTO>)query.list();
	}
	
	@Override
	public TipoDispositivoDTO buscarCatalogoPorTipo(Long id)
	{
		Criteria query = getCurrentSession().createCriteria(TipoDispositivoDTO.class);
		query.add(Restrictions.eq("idTipoDispositivo", id));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return (TipoDispositivoDTO)query.uniqueResult();
	}
	
	@Override
	public TipoDispositivoDTO activarDesactivarBuscarCatalogoPorTipo(Long id)
	{
		Criteria query = getCurrentSession().createCriteria(TipoDispositivoDTO.class);
		query.add(Restrictions.eq("idTipoDispositivo", id));		
		return (TipoDispositivoDTO)query.uniqueResult();
	}
	
	@Override
	public List<TipoDispositivoDTO> activarDesactivarBuscarCatalogo()
	{
		Criteria query = getCurrentSession().createCriteria(TipoDispositivoDTO.class);
		query.addOrder(Order.asc("idTipoDispositivo"));
		return (List<TipoDispositivoDTO>)query.list();
	}
}
