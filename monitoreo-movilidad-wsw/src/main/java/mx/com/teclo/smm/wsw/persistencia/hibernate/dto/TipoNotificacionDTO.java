package mx.com.teclo.smm.wsw.persistencia.hibernate.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TMM010C_CT_TIPO_NOTIFICACIONES")
public class TipoNotificacionDTO implements Serializable{

	private static final long serialVersionUID = -4406901921013624600L;
	
	@Id
	@Column(name="ID_TIPO_NOTIFICACION")
	private Long idTipoNotificacion;
	@Column(name="CD_TIPO_NOTIFICACION")
	private String cdTipoNotificacion;
	@Column(name="NB_TIPO_NOTIFICACION")
	private String nbTipoNotificacion;
	@Column(name="TX_TIPO_NOTIFICACION")
	private String txTipoNotificacion;
	@Column(name="ST_ACTIVO")
	private Boolean stActivo;
	@Column(name="FH_CREACION")
	private Date fhCreacion;
	@Column(name="ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name="FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name="ID_USR_MODIFICA")
	private Long idUsrModifica;
	
	public Long getIdTipoNotificacion() {
		return idTipoNotificacion;
	}
	public void setIdTipoNotificacion(Long idTipoNotificacion) {
		this.idTipoNotificacion = idTipoNotificacion;
	}
	public String getCdTipoNotificacion() {
		return cdTipoNotificacion;
	}
	public void setCdTipoNotificacion(String cdTipoNotificacion) {
		this.cdTipoNotificacion = cdTipoNotificacion;
	}
	public String getNbTipoNotificacion() {
		return nbTipoNotificacion;
	}
	public void setNbTipoNotificacion(String nbTipoNotificacion) {
		this.nbTipoNotificacion = nbTipoNotificacion;
	}
	public String getTxTipoNotificacion() {
		return txTipoNotificacion;
	}
	public void setTxTipoNotificacion(String txTipoNotificacion) {
		this.txTipoNotificacion = txTipoNotificacion;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
}

