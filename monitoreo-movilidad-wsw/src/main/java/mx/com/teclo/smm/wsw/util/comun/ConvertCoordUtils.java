package mx.com.teclo.smm.wsw.util.comun;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.vo.monitoreo.CoordVO;

public class ConvertCoordUtils
{
  public List<Double[]> convertirCoordenadasVO(List<CoordVO> listCoords)
  {
    List<Double[]> lista = new ArrayList<Double[]>();
    for (int i = 0; i < listCoords.size(); i++)
    {
      Double[] point = { listCoords.get(i).getLat(), listCoords.get(i).getLon() };
      lista.add(point);
    }
    return lista;
  }
  
  public List<String> obtenerDiasPorRangoFecha(String fechaInicio, String fechaFin)
  {
    List<String> fechas = new ArrayList<String>();
    fechaInicio = fechaInicio.split(" ")[0];
    fechaFin = fechaFin.split(" ")[0];
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    Date dateInicio = null;
    Date dateFin = null;
    Calendar C = Calendar.getInstance();
    try
    {
      dateInicio = formatter.parse(fechaInicio);
      dateFin = formatter.parse(fechaFin);
      while (dateInicio.before(dateFin))
      {
        fechas.add(formatter.format(dateInicio));
        C.setTime(dateInicio);
        C.add(5, 1);
        dateInicio = C.getTime();
      }
      fechas.add(formatter.format(dateFin));
    }
    catch (ParseException e)
    {
      e.printStackTrace();
    }
    return fechas;
  }
}
