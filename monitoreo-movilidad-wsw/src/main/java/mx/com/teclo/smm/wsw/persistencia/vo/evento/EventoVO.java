package mx.com.teclo.smm.wsw.persistencia.vo.evento;

import java.io.Serializable;

public class EventoVO implements Serializable{

	private static final long serialVersionUID = 5300566703092135044L;
	private Long idEvento;
	private Long idTipoEvento;
	private Long idSubtipoDispositivo;
	private String cdEvento;
	private String nbEvento;
	private String txEvento;
	
	public EventoVO() {}
	
	public EventoVO(Long idEvento, Long idTipoEvento, Long idSubtipoDispositivo, String cdEvento, String nbEvento,
			String txEvento) {
		super();
		this.idEvento = idEvento;
		this.idTipoEvento = idTipoEvento;
		this.idSubtipoDispositivo = idSubtipoDispositivo;
		this.cdEvento = cdEvento;
		this.nbEvento = nbEvento;
		this.txEvento = txEvento;
	}

	public Long getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(Long idEvento) {
		this.idEvento = idEvento;
	}

	public Long getIdTipoEvento() {
		return idTipoEvento;
	}

	public void setIdTipoEvento(Long idTipoEvento) {
		this.idTipoEvento = idTipoEvento;
	}

	public Long getIdSubtipoDispositivo() {
		return idSubtipoDispositivo;
	}

	public void setIdSubtipoDispositivo(Long idSubtipoDispositivo) {
		this.idSubtipoDispositivo = idSubtipoDispositivo;
	}

	public String getCdEvento() {
		return cdEvento;
	}

	public void setCdEvento(String cdEvento) {
		this.cdEvento = cdEvento;
	}

	public String getNbEvento() {
		return nbEvento;
	}

	public void setNbEvento(String nbEvento) {
		this.nbEvento = nbEvento;
	}

	public String getTxEvento() {
		return txEvento;
	}

	public void setTxEvento(String txEvento) {
		this.txEvento = txEvento;
	}
	
	
	
}
