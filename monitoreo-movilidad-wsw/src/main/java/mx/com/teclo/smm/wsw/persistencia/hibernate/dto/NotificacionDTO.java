package mx.com.teclo.smm.wsw.persistencia.hibernate.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TMM023D_CT_NOTIFICACIONES")
public class NotificacionDTO implements Serializable{

	private static final long serialVersionUID = 8341224440600316480L;

	@Id
	@Column(name="ID_NOTIFICACION")
	private Long idNotificacion;


	@Column(name="ID_EMPRESA")
	private long idEmpresa;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_MEDIO_TIPO_NOTIFI")
	private MedioNotificacionDTO idMedioTipoNotifi;
	@Column(name="NB_NOTIFICACION")
	private String nbNotificacion;
	@Column(name="TX_MENSAJE")
	private String txMensaje;
	@Column(name="FH_ENVIO")
	private Date fhEnvio;
	@Column(name="ST_ACTIVO")
	private Boolean stActivo;
	@Column(name="ST_ENVIADO")
	private Boolean stEnviado;
	@Column(name="FH_CREACION")
	private Date fhCreacion;
	@Column(name="ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name="FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name="ID_USR_MODIFICA")
	private Long idUsrModifica;

	public Long getIdNotificacion() {
		return idNotificacion;
	}
	public void setIdNotificacion(Long idNotificacion) {
		this.idNotificacion = idNotificacion;
	}
	public MedioNotificacionDTO getIdMedioTipoNotifi() {
		return idMedioTipoNotifi;
	}
	public void setIdMedioTipoNotifi(MedioNotificacionDTO idMedioTipoNotifi) {
		this.idMedioTipoNotifi = idMedioTipoNotifi;
	}
	public String getNbNotificacion() {
		return nbNotificacion;
	}
	public void setNbNotificacion(String nbNotificacion) {
		this.nbNotificacion = nbNotificacion;
	}
	public String getTxMensaje() {
		return txMensaje;
	}
	public void setTxMensaje(String txMensaje) {
		this.txMensaje = txMensaje;
	}
	public Date getFhEnvio() {
		return fhEnvio;
	}
	public void setFhEnvio(Date fhEnvio) {
		this.fhEnvio = fhEnvio;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Boolean getStEnviado() {
		return stEnviado;
	}
	public void setStEnviado(Boolean stEnviado) {
		this.stEnviado = stEnviado;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
	public long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
}
