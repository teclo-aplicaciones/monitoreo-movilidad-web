package mx.com.teclo.smm.wsw.negocio.service.coordenadas;

import mx.com.teclo.smm.wsw.persistencia.vo.PosicionesActVO;

public interface CoordenadaService {

	public PosicionesActVO agregarCoordenada(PosicionesActVO posicionesActVO);

}
