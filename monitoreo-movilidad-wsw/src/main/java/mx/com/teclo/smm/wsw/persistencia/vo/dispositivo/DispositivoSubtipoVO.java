package mx.com.teclo.smm.wsw.persistencia.vo.dispositivo;

public class DispositivoSubtipoVO {
	private Long id;
	private String nombre;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}
