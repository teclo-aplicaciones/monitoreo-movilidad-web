package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.MedioComunicacionDTO;

public interface MedioComunicacionDAO extends BaseDAO<MedioComunicacionDTO>{
	public MedioComunicacionDTO buscarMedioPorId(Long id);
	public List<MedioComunicacionDTO> obtenerCatalogo();
}
