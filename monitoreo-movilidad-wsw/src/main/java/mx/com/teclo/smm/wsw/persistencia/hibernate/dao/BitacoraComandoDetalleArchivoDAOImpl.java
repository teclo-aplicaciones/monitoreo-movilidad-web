package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.BitacoraComandoDetalleArchivoDTO;

@SuppressWarnings("unchecked")
@Repository("BitacoraComandoDetalleArchivoDAO")
public class BitacoraComandoDetalleArchivoDAOImpl extends BaseDAOImpl<BitacoraComandoDetalleArchivoDTO> implements BitacoraComandoDetalleArchivoDAO{

	@Override
	@Transactional(readOnly = true)
	public BitacoraComandoDetalleArchivoDTO obtenerArchivo(Long idBitacora) {
		Criteria query = getCurrentSession().createCriteria(BitacoraComandoDetalleArchivoDTO.class);
		query.add(Restrictions.eq("bitacoraComandoDTO.idBitacora", idBitacora));
		return (BitacoraComandoDetalleArchivoDTO)query.uniqueResult();
	}
	
	@Override
	@Transactional(readOnly = true)
	public BitacoraComandoDetalleArchivoDTO obtenerArchivoPorIdArchivo(Long idArchivo) {
		Criteria query = getCurrentSession().createCriteria(BitacoraComandoDetalleArchivoDTO.class);
		query.add(Restrictions.eq("idArchivo", idArchivo));
		return (BitacoraComandoDetalleArchivoDTO)query.uniqueResult();		
	}
	
}
