package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import mx.com.teclo.arquitectura.persistencia.comun.dao.BaseDao;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.DispositivoDTO;

public interface DispositivosDAO extends BaseDao<DispositivoDTO>{

	public DispositivoDTO findByImei(Long nuImei);

}
