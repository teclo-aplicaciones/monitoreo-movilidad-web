package mx.com.teclo.smm.wsw.persistencia.hibernate.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.wsw.persistencia.hibernate.comun.BaseDAOImpl;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.DispositivoDTO;
import mx.com.teclo.smm.wsw.persistencia.hibernate.dto.PosicionActualDTO;

@SuppressWarnings("unchecked")
@Repository("posicionActualDAO")
public class PosicionActualDAOImpl extends BaseDAOImpl<PosicionActualDTO> implements PosicionActualDAO{

	@Override
	public List<PosicionActualDTO> obtenerCoordenadaActual(DispositivoDTO dispoDTO)
	{
		Criteria query = getCurrentSession().createCriteria(PosicionActualDTO.class);
		query.add(Restrictions.eq("idDispositivo", dispoDTO));
		return (List<PosicionActualDTO>) query.list();
	}
	
	@Override
	public List<PosicionActualDTO> obtenerRegistrosHoy(){
		Criteria query = getCurrentSession().createCriteria(PosicionActualDTO.class);
		query.add(Restrictions.sqlRestriction("FH_REGISTRO BETWEEN TRUNC(SYSDATE) AND TRUNC(SYSDATE+1)"));
		
		return (List<PosicionActualDTO>)query.list();
	}
	
	@Override
	public PosicionActualDTO obtenerRegistroHoyPorDispositivo(Long idDispo){
		Criteria query = getCurrentSession().createCriteria(PosicionActualDTO.class);
		query.createAlias("idDispositivo", "idDispositivo");
		query.add(Restrictions.eq("idDispositivo.idDispositivo",idDispo));
		query.add(Restrictions.sqlRestriction("FH_REGISTRO BETWEEN TRUNC(SYSDATE) AND TRUNC(SYSDATE+1)"));
		
		return (PosicionActualDTO)query.uniqueResult();
	}
}
