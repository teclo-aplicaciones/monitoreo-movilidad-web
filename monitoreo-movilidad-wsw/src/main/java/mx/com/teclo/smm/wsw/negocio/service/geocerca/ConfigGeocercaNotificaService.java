package mx.com.teclo.smm.wsw.negocio.service.geocerca;

public interface ConfigGeocercaNotificaService {
	public Boolean validarPuntoDentroDeGeocerca(Long id, Double lng, Double lat);
	public Boolean validarPuntoFueraDeGeocerca(Long id, Double lng, Double lat);
}
