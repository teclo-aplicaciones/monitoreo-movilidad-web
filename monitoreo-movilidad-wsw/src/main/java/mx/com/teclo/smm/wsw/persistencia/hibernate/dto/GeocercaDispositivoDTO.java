package mx.com.teclo.smm.wsw.persistencia.hibernate.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="TMM014D_DP_DISP_GEOCERCAS")
public class GeocercaDispositivoDTO implements Serializable{

	/**/
	private static final long serialVersionUID = -2039450498258292822L;

	@Id
	@Column(name="ID_DISP_GEO")
	private Long idDispGeo;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_DISPOSITIVO")
	private DispositivoDTO idDispositivo;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_GEOCERCA")
	private GeocercaDTO idGeocerca;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_GRUPO")
	private GrupoDTO idGrupo;
	@Column(name="ST_ACTIVO")
	private Boolean stActivo;
	@Column(name="FH_CREACION")
	private Date fhCreacion;
	@Column(name="ID_USR_CREACION")
	private Long idUsrCreacion;
	@Column(name="FH_MODIFICACION")
	private Date fhModificacion;
	@Column(name="ID_USR_MODIFICA")
	private Long idUsrModifica;
	
	public Long getIdDispGeo() {
		return idDispGeo;
	}
	public void setIdDispGeo(Long idDispGeo) {
		this.idDispGeo = idDispGeo;
	}
	public DispositivoDTO getIdDispositivo() {
		return idDispositivo;
	}
	public void setIdDispositivo(DispositivoDTO idDispositivo) {
		this.idDispositivo = idDispositivo;
	}
	public GeocercaDTO getIdGeocerca() {
		return idGeocerca;
	}
	public void setIdGeocerca(GeocercaDTO idGeocerca) {
		this.idGeocerca = idGeocerca;
	}
	public GrupoDTO getIdGrupo() {
		return idGrupo;
	}
	public void setIdGrupo(GrupoDTO idGrupo) {
		this.idGrupo = idGrupo;
	}
	public Boolean getStActivo() {
		return stActivo;
	}
	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Long getIdUsrCreacion() {
		return idUsrCreacion;
	}
	public void setIdUsrCreacion(Long idUsrCreacion) {
		this.idUsrCreacion = idUsrCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public Long getIdUsrModifica() {
		return idUsrModifica;
	}
	public void setIdUsrModifica(Long idUsrModifica) {
		this.idUsrModifica = idUsrModifica;
	}
}
