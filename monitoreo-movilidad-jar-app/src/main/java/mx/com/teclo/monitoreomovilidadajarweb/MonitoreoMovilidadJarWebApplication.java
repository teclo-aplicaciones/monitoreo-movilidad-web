package mx.com.teclo.monitoreomovilidadajarweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
//import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
@ComponentScan( basePackages = "mx.com")
//@EntityScan(basePackages = "mx.com")
public class MonitoreoMovilidadJarWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonitoreoMovilidadJarWebApplication.class, args);
	}

//
//	@Bean
//	public HibernateJpaSessionFactoryBean sessionFactory() {
//		return new HibernateJpaSessionFactoryBean();
//	}


	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}


}
