package mx.com.teclo.monitoreomovilidadajarweb.smm.api.rest.persistencia.vo;

public class Logs {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
