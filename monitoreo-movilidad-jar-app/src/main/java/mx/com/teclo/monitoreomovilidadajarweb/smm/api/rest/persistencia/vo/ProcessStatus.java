package mx.com.teclo.monitoreomovilidadajarweb.smm.api.rest.persistencia.vo;

import mx.com.teclo.monitoreomovilidadajarweb.smm.api.util.enumeration.StatusEnum;

public class ProcessStatus {

	private StatusEnum status;
	private String name;


	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


}
