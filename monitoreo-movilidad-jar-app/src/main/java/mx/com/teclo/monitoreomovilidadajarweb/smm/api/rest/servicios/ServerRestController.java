package mx.com.teclo.monitoreomovilidadajarweb.smm.api.rest.servicios;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import mx.com.teclo.monitoreomovilidadajarweb.smm.api.rest.persistencia.vo.Logs;
import mx.com.teclo.monitoreomovilidadajarweb.smm.api.rest.persistencia.vo.ProcessStatus;
import mx.com.teclo.monitoreomovilidadajarweb.smm.api.util.enumeration.StatusEnum;

@RestController
@RequestMapping("/server")
public class ServerRestController {

	@Value("${server.path}")
	private String SERVER_PATH = null;

	@Value("${server.javahome}/bin/java")
	private String JAVA_BIN;

	@Value("${server.javahome}/bin/jps")
	private String JPS_BIN;

	private static final Log log = LogFactory.getLog(ServerRestController.class);

	@RequestMapping(value = "/status", method = RequestMethod.GET)
	public ResponseEntity<ProcessStatus> status() throws Exception {

		initJavaHome();

		ProcessStatus processStatus = new ProcessStatus();
		boolean isWindows = System.getProperty("os.name").toLowerCase().startsWith("windows");

		StringBuilder jarname = bash("cd " + SERVER_PATH + "; ls *.jar");

		String id_jps = null;
		if (jarname != null) {
			StringBuilder idJps = bash(JPS_BIN + " -l | grep " + jarname);
			processStatus.setName(jarname.toString());

			if (idJps != null) {
				id_jps = idJps.toString();
				id_jps = id_jps.split("\\s+")[0];
				if (Integer.valueOf(id_jps) > 0) {
					processStatus.setStatus(StatusEnum.START);
				} else {
					processStatus.setStatus(StatusEnum.STOP);
				}
			} else {
				processStatus.setStatus(StatusEnum.STOP);
			}

			return new ResponseEntity<ProcessStatus>(processStatus, HttpStatus.OK);
		} else {
			processStatus.setName(StatusEnum.NO_APP.toString());
			processStatus.setStatus(StatusEnum.NO_APP);
		}

		return new ResponseEntity<ProcessStatus>(processStatus, HttpStatus.OK);
	}

	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST, consumes = "multipart/form-data")
	public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile[] multipartFile) throws Exception {
		initJavaHome();

		if (multipartFile != null) {
			MultipartFile file = multipartFile[0];
			createFile(file);
			return new ResponseEntity<String>("", HttpStatus.OK);
		}
		return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@RequestMapping(value = "/start", method = RequestMethod.POST)
	public ResponseEntity<ProcessStatus> start() throws Exception {
		initJavaHome();

		final StringBuilder jarname = bash("cd " + SERVER_PATH + "; ls *.jar");
		StringBuilder output;
		if (jarname != null) {
			Thread thread = new Thread() {
				@Override
				public void run() {
					String cmd = " cd " + SERVER_PATH + "; nohup " + JAVA_BIN + " -jar " + jarname
							+ " >> log_"+ jarname +"_\"`date +%d-%m-%y`\" ";
					cmd = cmd.replace("\n", "");
					StringBuilder output = bash(cmd);
					log.info("");
				}
			};
			thread.start();
		}
		return new ResponseEntity<ProcessStatus>(new ProcessStatus(), HttpStatus.OK);
	}

	@RequestMapping(value = "/stop", method = RequestMethod.POST)
	public ResponseEntity<ProcessStatus> stop() throws Exception {
		initJavaHome();

		StringBuilder jarname = bash("cd " + SERVER_PATH + "; ls *.jar");

		String id_jps = null;
		StringBuilder killidJps = null;
		if (jarname != null) {
			StringBuilder idJps = bash(JPS_BIN + " -l | grep " + jarname);

			if (idJps != null) {
				id_jps = idJps.toString();
				id_jps = id_jps.split("\\s+")[0];
				if (Integer.valueOf(id_jps) > 0) {
					killidJps = bash("kill -9 " + id_jps);
				}
			}
		}

		return new ResponseEntity<ProcessStatus>(new ProcessStatus(), HttpStatus.OK);
	}

	@RequestMapping(value = "/getlogs", method = RequestMethod.GET)
	public ResponseEntity<List<Logs>> getlogs() throws Exception {
		List<Logs> lsLogs = new ArrayList<Logs>();

		StringBuilder logs = bash("cd " + SERVER_PATH + "; ls log-*");

		String arrayLogs[] = logs.toString().split("\\r?\\n");
		for(String s : arrayLogs) {
			Logs l = new Logs();
			l.setName(s);
			lsLogs.add(l);
		}


		return new ResponseEntity<List<Logs>>(lsLogs, HttpStatus.OK);
	}

	@RequestMapping(value = "/downloadlog", method = RequestMethod.PUT)
	public ResponseEntity<String> downloadlog(@RequestBody String logfile) {
		return new ResponseEntity<String>("", HttpStatus.OK);
	}


	@RequestMapping(path = "/download", method = RequestMethod.GET)
	public ResponseEntity<InputStreamResource> download(@RequestParam(value = "name")String name) throws IOException {

		File file = new File(SERVER_PATH+"/"+name);

	    InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

	    HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        headers.add("Content-disposition", "attachment; filename="+ name);

	    return ResponseEntity.ok()
	            .headers(headers)
	            .contentLength(file.length())
	            .contentType(MediaType.parseMediaType("application/octet-stream"))
	            .body(resource);
	}


	private void createFile(MultipartFile multipartFile) throws IOException {
		if (SERVER_PATH != null) {
			bash("cd " + SERVER_PATH + "; rm -fr *.jar");
			File convFile = new File(SERVER_PATH + multipartFile.getOriginalFilename());
			convFile.createNewFile();
			FileOutputStream fos = new FileOutputStream(convFile);
			fos.write(multipartFile.getBytes());
			fos.close();
		}
	}

	private StringBuilder bash(String command) {
		ProcessBuilder processBuilder = new ProcessBuilder();
		processBuilder.command("bash", "-c", command);
		System.out.println(command);

		try {
			Process process = processBuilder.start();
			StringBuilder output = new StringBuilder();
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

			String line;
			while ((line = reader.readLine()) != null) {
				String out = line + "\n";
				output.append(out);
				System.out.println(out);
			}

			int exitVal = process.waitFor();
			if (exitVal == 0) {
				log.info("Success!");
				return output;
			} else {
				if (output.length() == 0)
					return null;
				return output;
			}

		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void initJavaHome() {

		StringBuilder sbIsJava = bash("hash java");
		StringBuilder sbIsJPS = bash("hash jps");

		if(sbIsJava != null &&  sbIsJPS != null) {
			String isJava = sbIsJava.toString();
			String isJPS = sbIsJPS.toString();

			if( isJava.equals("") && isJPS.equals("")) {
				JAVA_BIN = "java";
				JPS_BIN = "jps";
			}
		}
	}

}
