
//Nombre de la aplicación para control con Angular
var appTeclo = "tecloExposiciones";

angular.module(appTeclo,
	[
		'ngRoute',
		'ngStorage',
		'ngCookies',
		'ngResource',
		'angular-jwt',
		'ng.deviceDetector',
		'ngAnimate',
		'ngTouch',
		'blockUI',
		'myUpload',
		'angularUtils.directives.dirPagination',
		'angularModalService',
		'checklist-model',
		'pascalprecht.translate',
		'ui.bootstrap',
		'angular-responsive',
		'FSAngular',
		'ui.select2',
		'switcher',
		'mgo-angular-wizard',
		'angular-growl',
		'bootstrapLightbox',
		'ae-datetimepicker'
	]
);
