angular.module(appTeclo).run(function($rootScope, configAppService) {
	
//	$rootScope.$on("$routeChangeStart", function(event, next, current) {
		configAppService.configuracionAplicacion().success(function(data) {
 			
			$rootScope.configuracionApp = data;
			
			var html = $('html');
			var fsDefault = $('html').css('font-size');
			var nuPixel = $rootScope.configuracionApp.resolucion.nuPixelesBase;
			var fsApp = nuPixel+'px';
			
			// 	ESTILOS BASE
			$rootScope.estilosCat = [
				{"cdResolucion" : 'GR', "txResolucion" : "Tamaño grande"},
				{"cdResolucion" : 'NM', "txResolucion" : "Tamaño normal"},
				{"cdResolucion" : 'PQ', "txResolucion" : "Tamaño pequeño"}
			];
			
			var resolucion = $rootScope.configuracionApp.resolucion;
			
			$rootScope.estiloSeleccionado = resolucion;
			
			switch(resolucion.cdResolucion) {
				
				case 'GR' :
					html.css('font-size', fsApp);
					$.AdminLTE.layout.fix();
					break;
					
				case 'NM' :
					html.css('font-size', fsApp);
					$.AdminLTE.layout.fix();
					break;
					
				case 'PQ' :
					html.css('font-size', fsApp);
					$.AdminLTE.layout.fix();
					break;
				
				default :
					html.css('font-size', fsDefault);
					$.AdminLTE.layout.fix();
			}
			
		}).error(function(data) {
			
		});
//	});
});