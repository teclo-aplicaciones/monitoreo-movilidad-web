angular.module(appTeclo).run(function($rootScope,$location,$timeout,$route,storageService) {

	removeActive = () => {
		$('.liAccess a').removeClass('active-m');
		$('.liSubAccess a').removeClass('active-subm');
		$('.liAccess').removeClass('active-boxShadow');
	};

	$rootScope.$on("$routeChangeStart", function(event, next, current) {

		if ( ('/' || '/index') == $location.path() ) {
			removeActive();

		} else if ( ('/webapp' ) == $location.path() ) {
			removeActive();
			$timeout( () => {
				$('#orders').addClass('active-boxShadow');
				$('#orders a').addClass('active-m');
			}, true);
		} else if ( ('/log') == $location.path() ) {
			removeActive();
			$timeout( () => {
				$('#sucursals-nav').addClass('active-boxShadow');
				$('#sucursals-nav a').addClass('active-m');
			}, true);
		}

		if($rootScope.menuDinamico !== undefined) {
			$rootScope.menuToRefresh('es_ES', false);
		}

	});
});