angular.module(appTeclo).controller('logController',
		function($scope,$timeout,growl,serverService,config) {

	$scope.urlConfig = config.baseUrl;
	$scope.selectLog=null;

	function getLogs(){
		serverService.getLogs().success(function(data){
			$scope.logs=data;
		});
	}

	/**
	 * init
	 */
	getLogs();

	$timeout(function(){
		angular.element("#select2-typeSearchSeg-container").html("Seleccione log");
	},5);

	/**
	 * events
	 */

	$scope.download = function(){
		serverService.downloadLog($scope.selectLog.name).success(function(data){
			console.info(data);
		});
	}

});