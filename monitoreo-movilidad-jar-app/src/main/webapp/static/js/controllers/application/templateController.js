angular.module(appTeclo).controller("templateController",
function($rootScope,$scope, $window, $location, $timeout,
	storageService,homeService,growl) {

	$scope.fullScreenActive = false;

	$scope.properties = new Object({
		currentPath: function(){return $location.path()},
		currentYear: new Date().getFullYear(),
		versionApp: '1.0.0',
		isToastLogin: false,
		isToastLogout: false,
		isToastSucursal: true,
		sucursals: [],
		isRequestLogin: false,
		isLogin: false,
		isWithPlate: false,
		isHome: true,
		isVehicles: false,
		isCourses: false,
		isAppointments: false
	});
	$scope.headflags = new Object({
		isProfileUser: false,
		isNotification: false
	});

	const initController = function() {

		$rootScope.stg = storageService.initNameStorages('D_O');

		if (storageService.getToken() != undefined) {
			$scope.properties.isLogin = true;
		} else {
			$scope.properties.isLogin = false;
		}

		$rootScope.shoppingcar = {
			isSale: false,
			isShoppingview: false,
			isShowPay: false,
			subtotal: parseFloat(0).toFixed(2),
			total: parseFloat(0).toFixed(2),
			typePay: 'tarjeta',
			changedevol: parseFloat(0).toFixed(2),
			listNodespay: [
				{idNode:1,titleNode:"PRODUCTOS",iconNode:"fa-shopping-cart",isActive:true,isDeploy:true},
				{idNode:2,titleNode:"PAGO",iconNode:"fa-credit-card",isActive:false,isDeploy:false},
				{idNode:3,titleNode:"RECIBO",iconNode:"fa-file",isActive:false,isDeploy:false}
			],
			listProducts: storageService.getShoppingcar() != undefined ? storageService.getShoppingcar() : []
		};

		if ( !storageService.getConfiguration() ) {
			homeService.getSucursals().success( sucursals => {

				$scope.properties.city = "CDMX";
				$scope.properties.sucursals = sucursals;

				let config = angular.copy($scope.properties);

				storageService.setConfiguration(config);

			});
		} else {
			$scope.properties = angular.copy(storageService.getConfiguration());
		}

	};

	$scope.goProducts = sucursal => {

		let sucursalEl = $('#select2-sucursal-container');

		$scope.properties.sucursal = sucursal;
		$scope.properties.isToastSucursal = false;

		sucursalEl.text($scope.properties.sucursal.nbTipoSucursal);

		if ( $scope.properties.remembersel ) { storageService.setConfiguration($scope.properties); }

	};

	// INICIA CARRITO DE COMPRAS
	$scope.flagspay = {
		isCar: true,
		isPay: false,
		isReceit: false,
		isCashPay: false,
		isCardPay: false
	};

	$scope.viewShoppingcar = () => {
		$timeout( () => {
			$('#scrollProductscar').slimScroll({
				height: '100%',
				color: '#00243c',
				opacity: .3,
				size: "4px",
				alwaysVisible: false
			});
		}, 100);
		$rootScope.shoppingcar.isShoppingview=true;
	};

	$scope.removeOfcar = product => {

		let products = $rootScope.shoppingcar.listProducts;

		$rootScope.shoppingcar.subtotal = parseFloat($rootScope.shoppingcar.subtotal) - parseFloat(product.nuPrecio);
		$rootScope.shoppingcar.total = parseFloat($rootScope.shoppingcar.total) - parseFloat(product.nuPrecio);

		if ( products.length === 1 ) {
			let scrollProductscar = document.getElementById('scrollProductscar');

			if( scrollProductscar ) {
				let remove = scrollProductscar.parentNode;
				remove.parentNode.removeChild(remove);
			}
		}

		products.splice(product, 1);

		if ( products.length === 0 ) {
			$rootScope.shoppingcar.isShowPay = false;
			$rootScope.shoppingcar.subtotal = parseFloat(0).toFixed(2);
			$rootScope.shoppingcar.total = parseFloat(0).toFixed(2);
		}

		$rootScope.countTotal(products);
	};

	$rootScope.countTotal = products => {

		let subtotal = parseFloat(0);
		let total = parseFloat(0);

		for ( let pro of products ) {

			subtotal = subtotal+parseFloat(pro.nuPrecio);
			total = total+parseFloat(pro.nuPrecio);

		}

		$rootScope.shoppingcar.subtotal = parseFloat(subtotal).toFixed(2);
		$rootScope.shoppingcar.total = parseFloat(subtotal).toFixed(2);

	};

	$scope.empyingCar = () => {

		let scrollProductscar = document.getElementById('scrollProductscar');
		let remove = scrollProductscar.parentNode;

		remove.parentNode.removeChild(remove);
		$rootScope.shoppingcar.listProducts = [];
		$rootScope.shoppingcar.subtotal = parseFloat(0).toFixed(2);
		$rootScope.shoppingcar.total = parseFloat(0).toFixed(2);

		storageService.setShoppingcar([]);
	};

	$rootScope.toPay = products => {

		$rootScope.shoppingcar.isShoppingview = false;
		$rootScope.shoppingcar.isShowPay = true;

		$timeout( () => {
			let progressWizard = document.getElementById('progress-wizard');

			progressWizard.attributes["aria-valuenow"].value = 20;
			progressWizard.style.width = "20%";
		}, 300);

	};

	$scope.toStep = node => {

		let progressWizard = document.getElementById('progress-wizard');

		if ( node.isDeploy ) {
			if ( node.idNode == 1 ) {
				$scope.flagspay.isCar = true;
				$scope.flagspay.isPay = false;
				$scope.flagspay.isReceit = false;

				progressWizard.attributes["aria-valuenow"].value = 20;
				progressWizard.style.width = "20%";
			} else if ( node.idNode == 2 ) {
				$scope.flagspay.isCar = false;
				$scope.flagspay.isPay = true;
				$scope.flagspay.isReceit = false;

				progressWizard.attributes["aria-valuenow"].value = 50;
				progressWizard.style.width = "50%";
			} else if ( node.idNode == 3 ) {
				$scope.flagspay.isCar = false;
				$scope.flagspay.isPay = false;
				$scope.flagspay.isReceit = true;

				progressWizard.attributes["aria-valuenow"].value = 100;
				progressWizard.style.width = "100%";
			}

			$scope.shoppingcar.listNodespay.map( nd => {
				if ( nd.idNode === node.idNode ) {
					nd.isActive = true;
				} else {
					nd.isActive = false;
				}
			});
		}
	};

	$scope.nextPay = () => {

		let progressWizard = document.getElementById('progress-wizard');

		$scope.flagspay.isCar = false;
		$scope.flagspay.isPay = true;
		$scope.flagspay.isReceit = false;

		if ( $rootScope.shoppingcar.typePay == 'efectivo' ) {
			$scope.flagspay.isCashPay = true;
			$scope.flagspay.isCardPay = false;
		} else if ( $rootScope.shoppingcar.typePay == 'tarjeta' ) {
			$scope.flagspay.isCashPay = false;
			$scope.flagspay.isCardPay = true;
		}

		$scope.shoppingcar.listNodespay[0].isActive = false;
		$scope.shoppingcar.listNodespay[0].isDeploy = true;

		$scope.shoppingcar.listNodespay[1].isActive = true;
		$scope.shoppingcar.listNodespay[1].isDeploy = true;

		progressWizard.attributes["aria-valuenow"].value = 50;
		progressWizard.style.width = "50%";
	};

	$scope.payProducts = () => {
		let progressWizard = document.getElementById('progress-wizard');

		$scope.flagspay.isCar = false;
		$scope.flagspay.isPay = false;
		$scope.flagspay.isReceit = true;

		$scope.shoppingcar.listNodespay[1].isActive = false;
		$scope.shoppingcar.listNodespay[1].isDeploy = true;

		$scope.shoppingcar.listNodespay[2].isActive = true;
		$scope.shoppingcar.listNodespay[2].isDeploy = true;

		progressWizard.attributes["aria-valuenow"].value = 100;
		progressWizard.style.width = "100%";
	};

	$scope.countDevol = receive => {
		let devol = 0;

		if( receive != "" ) { devol = receive - $rootScope.shoppingcar.total; }
		$rootScope.shoppingcar.changedevol = parseFloat(devol).toFixed(2);
	};

	$scope.printReceit = receit => {

		let ficha=document.getElementById(receit);
		let ventimp=window.open(' ','popimpr');

		ventimp.document.write(ficha.innerHTML);
		ventimp.document.close();
		ventimp.print();
		ventimp.close();

	};
	// FIN CARRITO COMPRAS

	$scope.togglePagesArrow = function(direction) {

		const isHome = $scope.properties.isHome;
		const isVehicles = $scope.properties.isVehicles;
		const isCourses = $scope.properties.isCourses;
		const isAppointments = $scope.properties.isAppointments;

		switch (direction) {
			case 'left':
				if (!isHome && !isVehicles && !isCourses && isAppointments) {
					$scope.properties.isHome = false;
					$scope.properties.isVehicles = false;
					$scope.properties.isCourses = true;
					$scope.properties.isAppointments = false;
				} else if (!isHome && !isVehicles && isCourses && !isAppointments) {
					$scope.properties.isHome = false;
					$scope.properties.isVehicles = true;
					$scope.properties.isCourses = false;
					$scope.properties.isAppointments = false;
				} else if (!isHome && isVehicles && !isCourses && !isAppointments) {
					$scope.properties.isHome = true;
					$scope.properties.isVehicles = false;
					$scope.properties.isCourses = false;
					$scope.properties.isAppointments = false;
				}
				break;
			case 'right':
				if (isHome && !isVehicles && !isCourses && !isAppointments) {
					$scope.properties.isHome = false;
					$scope.properties.isVehicles = true;
					$scope.properties.isCourses = false;
					$scope.properties.isAppointments = false;
				} else if (!isHome && isVehicles && !isCourses && !isAppointments) {
					$scope.properties.isHome = false;
					$scope.properties.isVehicles = false;
					$scope.properties.isCourses = true;
					$scope.properties.isAppointments = false;
				} else if (!isHome && !isVehicles && isCourses && !isAppointments) {
					$scope.properties.isHome = false;
					$scope.properties.isVehicles = false;
					$scope.properties.isCourses = false;
					$scope.properties.isAppointments = true;
				}
				break;
		}
	};

	$scope.togglePages = function(page) {
		switch (page) {
			case 'home':
				$scope.properties.isHome = true;
				$scope.properties.isVehicles = false;
				$scope.properties.isCourses = false;
				$scope.properties.isAppointments = false;
				break;
			case 'vehicles':
				$scope.properties.isHome = false;
				$scope.properties.isVehicles = true;
				$scope.properties.isCourses = false;
				$scope.properties.isAppointments = false;
				break;
			case 'courses':
				$scope.properties.isHome = false;
				$scope.properties.isVehicles = false;
				$scope.properties.isCourses = true;
				$scope.properties.isAppointments = false;
				break;
			case 'appointments':
				$scope.properties.isHome = false;
				$scope.properties.isVehicles = false;
				$scope.properties.isCourses = false;
				$scope.properties.isAppointments = true;
				break;
		}
	};

    $window.onbeforeunload = function() {
		localStorage.setItem("statusAlert",false);
 	};

 	$scope.mostrarHamburguer = false;
 	$scope.mostrarCapaHamburguer = false;

// 	Pantalla completa
 	$scope.goFullscreen = function () {
 		$scope.fullScreenActive = !$scope.fullScreenActive;
	};

	// LOGIN
	$scope.loginCC = function(user,pass) {

		if(user === "admin" && pass === "admin"){
			$scope.properties.isRequestLogin = true;
			$timeout(function() {
				$scope.properties.isToastLogin = false;
				$scope.properties.isLogin = true;

				if ( !$scope.properties.remembersel ) {
					$scope.properties.sucursal = undefined;
					$scope.properties.isToastSucursal = true;
				}

				let config = angular.copy($scope.properties);

				storageService.setToken("banana");
				storageService.setConfiguration(config);
				$location.path("/");

				$scope.properties.isToastSucursal = false;

			}, 1500);
		}else{
			growl.error("Usuario o password incorrecto");
		}
	};

	//LOGOUT
	$scope.logoutCC = function() {
		$scope.properties.isToastLogout = false;
		$scope.properties.isLogin = false;
		$scope.properties.isRequestLogin = false;
		$scope.properties.isHome = true;
		$scope.properties.isVehicles = false;
		$scope.properties.isCourses = false;
		$scope.properties.isAppointments = false;
		$scope.properties.isWithPlate = false;

		let config = angular.copy($scope.properties);

		storageService.deleteStorage($rootScope.stg.tokenName);
		storageService.setConfiguration(config);
		$location.path("/");

		$scope.headflags.isProfileUser = false;
	};

 	initController();
});