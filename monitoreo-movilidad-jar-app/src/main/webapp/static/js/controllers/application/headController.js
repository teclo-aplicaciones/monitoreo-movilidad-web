angular.module(appTeclo).controller("headerController", function($rootScope, $scope, $location, $window, $translate, $timeout, menuDinamicoService, growl) {
	
	$rootScope.currentLanguage = 'es_ES';
	$rootScope.breadCrumbs = {};

	$scope.flagshead = {
		isProfileUser: false
	};
	
//	Consumir servicio para obtener los módulos y servicios de la aplicación
	cargarMenu = function() {

		const data = menuDinamicoService.buscarMenuApp();

		$rootScope.menuDinamico = configurarMenu(data);
		$rootScope.menuToRefresh($rootScope.currentLanguage, false);
	};
	
//	Configurar los módulo y servicios para pintarlos en el menu
	configurarMenu = function(menuApp) {
		
		let menus = [];
		
		angular.forEach(menuApp, function(menu, key){
			if (menu.menuSuperior === 0) {
				
				menu.subMenus = [];
				
				angular.forEach(menuApp, function(subMenu, key){
					if (menu.id === subMenu.menuSuperior) {
						menu.subMenus.push(subMenu);
					}
			    });
				menus.push(menu);
			}
		});

		return menus;
	};
	
//	Obtener el submenu actual por medio del path para pintar el breadcrumbs
	$rootScope.menuToRefresh = function(lang, status) {
		
		let path 	  = $location.path();
		let son 	  = "/"+path.split("/")[1];
		let grandSon  = "/"+path.split("/")[2];
		let parameter = "/"+path.split("/")[3];
		let paux 	  = "/"+path.split("/")[4];
		let pinner	  = "/"+path.split("/")[5];
		
		angular.forEach($rootScope.menuDinamico, function(menu, key){
			
			
			menu.menuNombre = languageMenu(lang, menu);
			
			angular.forEach(menu.subMenus, function(subMenu, key){
								
				subMenu.subMenuNombre = languageMenu(lang, subMenu);
				
				switch(path) {
					case subMenu.menuUri :
					case subMenu.menuUri+grandSon :
					case subMenu.menuUri+parameter :
					case subMenu.menuUri+grandSon+parameter :
					case subMenu.menuUri+grandSon+parameter+paux :
					case subMenu.menuUri+grandSon+parameter+paux+pinner :
						$rootScope.breadCrumbs.icono = menu.menuIcono;
						$rootScope.breadCrumbs.modulo = menu.menuNombre;
						$rootScope.breadCrumbs.servicio = subMenu.subMenuNombre;
						$rootScope.breadCrumbs.menuUri = subMenu.menuUri;
						
						if(!status) {
							$timeout(function(){
								
//								$('#'+menu.id).trigger("click");
								
								if(grandSon != undefined){
									$('#'+subMenu.id).addClass("active-subm");
								}else{
									$('#'+subMenu.id).trigger("click");
								}
								
								$scope.menuActive(subMenu, subMenu.id);
							}, 300);
							
						}
					break;
				}
			});
		});
	};

	languageMenu = function(lang, obj) {
		
		let name = "";
		
		switch (lang) {
			case "es_ES" :
				name = obj.menuTexto;
				break;
			case "en_US" :
				name = obj.menuTextoEn;
				break;
		}
		
		return name;
	};
	
//	Leer función al entrar al sistema
	$scope.menuActive = function(subMenu, id) {

		$('.liAccess').removeClass('active-boxShadow');
		$('.liAccess a').removeClass('active-m');

		angular.forEach($('.liAccess a'), function(element, key){
			if(id == $(element).attr('id')) {
				$('#'+id).addClass('active-subm');
				$('#'+subMenu.menuSuperior).addClass('active-m');
				$('#'+subMenu.menuSuperior).parent().addClass('active-boxShadow');
			}else{
				$(element).removeClass('active-subm');
			}
		});
	};
	
	$scope.hamburger = function() {

 		let hamburguer = $(".hamburger");
 		let isActive = "is-active";
 		
 		hamburguer.toggleClass(isActive);
 		
 		if(hamburguer.hasClass(isActive)) {
 			$scope.showCoatingHamburguer = true;
 			$scope.modeMovil = true;
 		}else{
 			$scope.showCoatingHamburguer = false;
 			$scope.modeMovil = false;
 		}
 		
 		$('.liSubAccess').click(function() {
 			$scope.$apply(function() {
 				hamburguer.removeClass(isActive);
 				$scope.showCoatingHamburguer = false;
 				$scope.modeMovil = false;
 			});
 		});
	};
	
	cargarMenu();
});