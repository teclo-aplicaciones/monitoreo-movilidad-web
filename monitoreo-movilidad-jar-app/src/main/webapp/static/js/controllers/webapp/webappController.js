angular.module(appTeclo)
.controller('webappController',
function($scope,$timeout,growl,serverService) {

	$scope.file=null;
	$scope.application={};


	function getStatus(){
		serverService.getStatus().success(function(data){
			$scope.application=data;
		});
	}

	/**
	 * init
	 */
	getStatus();

	/**
	 * Events
	 */

	$scope.refreshApp=function(){
		getStatus();
	}

	$scope.getFile=function(file){
		$timeout(function(){
			$scope.file = file;
		});
	}

	$scope.resetFile= function(file){
		$timeout(function(){
			$scope.file=null;
		});
	};

	$scope.uploadFile=function(){
		serverService.uploadFile($scope.file).success(function (data){
			console.info(data);
			getStatus();
		});
	}


	$scope.start=function(){
		serverService.start().success(function(data){
			getStatus();
		});
	}

	$scope.stop=function(){
		serverService.stop().success(function(data){
			getStatus();
		});
	}


});