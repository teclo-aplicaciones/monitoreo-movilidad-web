angular.module(appTeclo).controller('modalMensajeController', function($scope, $element, message, typeModal, close, actionCancel) {
	
	$scope.message = message;
	$scope.typeModal = typeModal;
	
	$scope.cancelar = function() {
		actionCancel();
	}
	
	$scope.close = function(result){
		close(result, 500);
	};
});