angular.module(appTeclo).factory("errorInterceptor", function ($q, $location) {
	return {
		responseError: function (rejection) {
			if (rejection.status === 500) {
				$location.path("/error");
			}
			if (rejection.status === 401) {
				$location.path("/accesoNegado");
			}
			return $q.reject(rejection);
		}
	};
});