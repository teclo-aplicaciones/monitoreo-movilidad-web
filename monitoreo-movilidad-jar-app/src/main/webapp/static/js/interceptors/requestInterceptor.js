angular.module(appTeclo).factory("requestInterceptor", function ($q, $location, $localStorage) {
	
	return {
		request : function(config) {
			config.headers = config.headers || {};

			if (localStorage.tokenSa) {
				config.headers['X-Auth-Token'] = localStorage.tokenSa;
			}
			if(config.url != "views/templatemodal/templateModalToken.html")
				localStorage.lastRequest=new Date();
			return config;
		},

		responseError : function(response) {
			if (response.status === 401 || response.status === 403) {
				//$location.path('/login');
			}
			return $q.reject(response);
		}
	}
});