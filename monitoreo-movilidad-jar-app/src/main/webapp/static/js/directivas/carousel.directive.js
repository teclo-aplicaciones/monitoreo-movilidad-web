/**
 * @author: César Gómez
 * @version: 1.0.0
 * @name: Carousel
 */

// const APP = angular.module(appTeclo);

angular.module(appTeclo).directive('carouselImgs', ($interval) => {

    return new Object({

        restrict: 'E',
        template: '<div class="contentcarousel fadeInDown a-7ms">'
                +   '<div class="carousel">'
                +       '<ul class="slides">'
                +           '<li class="animateCarousel" ng-repeat="item in arrayimg"'
                +               'ng-if="isActive($index)">'
                +               '<img ng-src="{{ item.blImg }}" />'
                +               '<a class="toSlide" ng-if="redirect" href="#{{item.path}}"></a>'
                +               '<a class="toSlide" ng-if="!redirect"></a>'
                +           '</li>'
                +       '</ul>'    
                +       '<a class="arrowSlide prev" ng-click="showPrev()"><i class="fa fa-chevron-left"></i></a>'
                +       '<a class="arrowSlide next" ng-click="showNext()"><i class="fa fa-chevron-right"></i></a>'
                +       '<a class="arrowSlide pausePlay" ng-click="pausePlay()" ng-if="autoslide && controlsPauseplay">'
                +           '<i class="fa fa-{{_pp == true ? \'play\' : \'pause\'}}"></i>'
                +       '</a>'    
                +       '<ul class="navigation">'
                +           '<li ng-repeat="itemNav in arrayimg"'
                +               'ng-click="showItem($index);"'
                +               'ng-class="{\'active\':isActive($index)}">'
                +               '<img ng-src="{{itemNav.blImg}}" alt="{{itemNav.nbItem}}" tooltip="{{itemNav.nbItem}}" />'
                +           '</li>'
                +       '</ul>'
                +      '</div>'
                +   '</div>',
        replace: false,
        scope: {
            arrayimg: '=',
            autoslide: '=',
            controlsPauseplay: '=?',
            redirect: '=',
            durationslide: '='
        },
        link: preLink = (scope) => {

            if ( scope.autoslide ) { scope.controlsPauseplay = true; }

            scope.pausePlay = function() {
                if (angular.isDefined(intervalSlideShow)) {
                    $interval.cancel(intervalSlideShow);
                    intervalSlideShow = undefined;
                } else {
                    scope.initSlideShow();
                }
                scope._pp = !scope._pp;
             }
        
             scope.initSlideShow = function() {
                 intervalSlideShow = $interval(function() {
                     scope.showNext();
                 }, scope.durationslide);
             }
        
            scope._Index = 0;
        
            scope.isActive = function (index) {
                return scope._Index === index;
            };
        
            scope.showPrev = function () {
                scope._Index = (scope._Index > 0) ? --scope._Index : scope.arrayimg.length - 1;
            };
        
            scope.showNext = function () {
                scope._Index = (scope._Index < scope.arrayimg.length - 1) ? ++scope._Index : 0;
            };
        
            scope.showItem = function (index) {
                scope._Index = index;
            };

            if ( scope.autoslide ) {

                scope.durationslide = scope.durationslide*1000;

                scope.initSlideShow();
            }
        }
    });

});
