angular.module(appTeclo).config(function($routeProvider, $locationProvider) {

	$routeProvider.when("/", {
		templateUrl : "views/home/home.html"

	});

	$routeProvider.when("/error", {
		templateUrl : "views/error.html",
	});

	$routeProvider.when("/index",{
		templateUrl : "views/index.html",
	});

	$routeProvider.when("/accesoNegado", {
		templateUrl : "views/accesoNegado.html",
	});

	$routeProvider.when("/webapp", {
		templateUrl : "views/webapp/webapp.html",
		controller: "webappController"
	});

	$routeProvider.when("/log", {
		templateUrl : "views/log/log.html",
		controller : "logController"
	});

	$routeProvider.otherwise({redirectTo: "/index"});

});