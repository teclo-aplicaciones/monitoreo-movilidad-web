angular.module(appTeclo).constant("constante", {
	urlWs : "/teclocdmxmonimovi-jar-app_v100r1_des_app/"
});
angular.module(appTeclo)
.factory('config', ['$location','$rootScope','constante',
function($location,$rootScope,constante) {

	let protocol = `${$location.protocol()}://`;
	let host = location.host;
	let url = `${protocol}${host}${constante.urlWs}`;
//	let url = `${protocol}${host}`;
	let absUrl = $location.absUrl();
	let context = absUrl.split("/")[3];
	let contextApp = `${protocol}${host}/${context}`;

	return {
		baseUrl: url,
		absUrl: absUrl,
		contextApp: contextApp
	}
}]);