angular.module(appTeclo).service('menuDinamicoService', function($http, config) {
	
	// Temporal
	this.buscarMenuApp = function () {
		
		var menu = [
			{id:1, menuIcono:"fa fa-cog", menuSuperior:0, menuTexto:"Fotocívicas", menuUri:null},
			// {id:2, menuIcono:"fa fa-cog", menuSuperior:0, menuTexto:"Citas", menuUri:null},
			// {id:3, menuIcono:"fa fa-cog", menuSuperior:0, menuTexto:"Cursos", menuUri:null},
			{id:4, menuIcono:null, menuSuperior:1, menuTexto:"Responsable solidario", menuUri:"/responsable-solidario"},
			{id:5, menuIcono:null, menuSuperior:1, menuTexto:"Consultar fotocívicas", menuUri:"/consulta-fotocivicas"},
			// {id:6, menuIcono:null, menuSuperior:2, menuTexto:"Consultar citas", menuUri:"/consulta-citas"},
			// {id:7, menuIcono:null, menuSuperior:3, menuTexto:"Curso en línea", menuUri:"/curso-enlinea"}
			// {id:7, menuIcono:null, menuSuperior:3, menuTexto:"Consultar denuncias", menuUri:"/consulta-denuncias"}
		]
		
		return menu;
	};
	
	// Ajustar a necesidad para consumir menu real desde base
//	this.buscarMenuApp = function () {
//		return $http.get(config.baseUrl + "/login/menus");
//	};
});