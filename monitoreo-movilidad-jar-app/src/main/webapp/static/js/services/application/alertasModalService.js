angular.module(appTeclo).service("showAlert", function($rootScope,ModalService) {
	
	var cancelDefault = function() {
		return;
	}
	
	/* Modal aviso */
	this.aviso = function(messageTo, action, param) {
		ModalService.showModal({
			templateUrl : 'views/templatemodal/templateShowAlertas.html',
			controller : 'modalMensajeController',
			inputs : {
				message : messageTo,
				typeModal : 'aviso',
				actionCancel: cancelDefault
			}
		}).then(function(modal) {
			validateParams(modal, messageTo, action, param);
		});

		return ModalService.showModal;
	}

	/* Modal error */
	this.error = function(messageTo, action, param) {
		ModalService.showModal({
			templateUrl : 'views/templatemodal/templateShowAlertas.html',
			controller : 'modalMensajeController',
			inputs : {
				message : messageTo,
				typeModal : 'error',
				actionCancel: cancelDefault
			}
		}).then(function(modal) {
			validateParams(modal, messageTo, action, param);
		});
		
		return ModalService.showModal;
	}

	/* Modal confirmacion */
	this.confirmacion = function(messageTo, action, param, cancel) {
		
		if(messageTo === undefined) {
			return;
		}else if(typeof param === 'function') {
			cancel = param;
		}else if(cancel === undefined) {
			cancelDefault = function() {
				return;
			}
		}
		
		ModalService.showModal({
			templateUrl : 'views/templatemodal/templateShowAlertas.html',
			controller : 'modalMensajeController',
			inputs : {
				message : messageTo,
				typeModal : 'confirmacion',
				actionCancel: cancel !== undefined ? cancel : cancelDefault
			}
		}).then(function(modal) {
			validateParams(modal, messageTo, action, param);
		});
		
		return ModalService.showModal;
	}
	
	/* Modal aviso */
	this.viewPdf = function(messageTo, action, param) {
		ModalService.showModal({
			templateUrl : 'views/templatemodal/templateShowAlertas.html',
			controller : 'modalMensajeController',
			inputs : {
				message : messageTo,
				typeModal : 'viewPdf',
				actionCancel: cancelDefault
			}
		}).then(function(modal) {
			validateParams(modal, messageTo, action, param);
		});

		return ModalService.showModal;
	}
	
	this.requiredFields = function(formName) {
		angular.forEach(formName.$error, function (field) {
			angular.forEach(field, function(errorField){
				errorField.$setDirty();
			})
		});
	}
	
	this.safeApply = function(fn) {
		var $root = $rootScope,
			phase = $root.$$phase;
    	
    	if(phase == '$apply' || phase == '$digest') {
    		if(fn && (typeof(fn) === 'function')) {
    			fn();
    		}
    	} else {
    		$apply(fn);
    	}
	}

	validateParams = function(modal, m, a, p) {
		modal.element.modal();
		if (a) {
			modal.close.then(function() {
				if(p != undefined) {
					a(p);
				}else{
					a();
				}
			});
		}
	}

});