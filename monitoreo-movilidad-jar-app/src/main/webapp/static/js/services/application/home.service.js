angular.module(appTeclo)
.service('homeService',
function($http,config) {

	const URL = `${config.contextApp}/jsons/home`;

	this.getSucursals = () => { return $http.get(`${URL}/sucursales.json`); };

	this.getPromos = () => { return $http.get(`${URL}/promos.json`); };

	this.getMainStock = () => { return $http.get(`${URL}/mainstock.json`); };

	this.getTypeProduct = () => { return $http.get(`${URL}/typeProduct.json`); };

	this.getStock = () => { return $http.get(`${URL}/productStock.json`); };

});
