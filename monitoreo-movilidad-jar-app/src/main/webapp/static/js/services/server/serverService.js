angular.module(appTeclo).service('serverService', function($http, config) {
	var urlConfig = config.baseUrl;

	this.getStatus = function() {
		return $http.get(urlConfig + "/server/status");
	};

	this.stop = function() {
		return $http.post(urlConfig + "/server/stop");
	};

	this.start = function() {
		return $http.post(urlConfig + "/server/start");
	};

	this.uploadFile = function(file) {
		var fileFormData = new FormData();
		fileFormData.append('file', file.files[0]);

		return $http.post(urlConfig + "/server/uploadFile", fileFormData, {
			transformRequest : angular.identity,
			headers : {
				'Content-Type' : undefined
			}
		})
	};

	this.getLogs = function() {
		return $http.get(urlConfig + "/server/getlogs");
	};

	this.downloadLog = function (logfile){
		return $http.put(config.baseUrl + "/server/downloadlog", logfile);
	};

});