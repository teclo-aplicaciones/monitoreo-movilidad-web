package mx.com.teclo.smm.util.enumerable;

import java.util.ArrayList;
import java.util.List;

public enum TipoNotificacionEnum {
	PUSH(1L, "Push"), SMS(2L, "Mensaje de Texto"), MAIL(3L, "Correo");

	private Long idCat;
	private String nombreCat;
	
	private TipoNotificacionEnum(Long idCat, String nombreCat){
		this.idCat = idCat;
		this.nombreCat = nombreCat;
	}
	
	public Long getIdCat(){
		return this.idCat;
	}
	public void setIdCar(Long idCat){
		this.idCat = idCat;
	}
	public String getNombreCat(){
		return this.nombreCat;
	}
	public void setNombreCat(String nombreCat){
		this.nombreCat = nombreCat;
	}
}
