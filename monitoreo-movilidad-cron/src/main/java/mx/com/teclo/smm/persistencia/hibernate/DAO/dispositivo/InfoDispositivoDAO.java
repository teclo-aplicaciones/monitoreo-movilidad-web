package mx.com.teclo.smm.persistencia.hibernate.DAO.dispositivo;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.Base.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.DTO.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.DTO.dispositivo.InfoDispositivoDTO;

public interface InfoDispositivoDAO extends BaseDAO<InfoDispositivoDTO> {
	public InfoDispositivoDTO buscarRelacionInformacionDispositivo(DispositivoDTO dDTO);
}
