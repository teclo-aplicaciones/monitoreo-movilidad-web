package mx.com.teclo.smm.negocio.service.notificaciones;

import mx.com.teclo.smm.persistencia.hibernate.DTO.notificacion.NotificacionDTO;

public interface NotificacionSMSService {
	public Boolean enviaNotificacionSMSInmediata(String nTelefonico, NotificacionDTO notificacion);
}
