package mx.com.teclo.smm.persistencia.hibernate.DAO.dispositivo;

import mx.com.teclo.smm.persistencia.hibernate.Base.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.DTO.dispositivo.DispoEmpleadoDTO;
import mx.com.teclo.smm.persistencia.hibernate.DTO.dispositivo.DispositivoDTO;

public interface DispoEmpleadoDAO extends BaseDAO<DispoEmpleadoDTO>{
	public DispoEmpleadoDTO buscarRelacionDispositivoEmpleado(DispositivoDTO dDTO);
}
