package mx.com.teclo.smm.negocio.service.notificaciones;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import mx.com.teclo.notificacionespush.cliente.firebase.ClienteFirebaseService;
import mx.com.teclo.notificacionespush.message.firebase.Client;
import mx.com.teclo.notificacionespush.message.firebase.Push;
import mx.com.teclo.notificacionespush.message.firebase.ResponseFirebaseMessage;
import mx.com.teclo.smm.persistencia.hibernate.DTO.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.DTO.notificacion.NotificacionDTO;
import mx.com.teclo.smm.util.enumerable.TemplatePushEnum;

@Service
public class NotificacionPushServiceImpl implements NotificacionPushService{

	@Value("${firebase.apikey}")
	private String API_KEY;
	
	@Autowired
	ClienteFirebaseService clienteFirebaseService; 

	@Override
	@SuppressWarnings("finally")
	public Boolean enviaNotificacionPushInmediata(DispositivoDTO dispo, NotificacionDTO notificacion, String base64){
		
		ResponseFirebaseMessage rfbm = null;
		
		Client cliente = new Client(dispo.getFbDispositivo());
		
		Push push = TemplatePushEnum.MENSAJE.getPush();
		
		//Agregamos los Clientes(Como solo hay 1 no es necesario la lista)
		push.setListDevice(null);
		
		//Agregamos la API KEY
		push.setApiKey(API_KEY);
		
		//Agregamos el Titulo
		push.getData().setTittle(notificacion.getNbNotificacion());
		//push.getData().setImage(base64);
		//push.getNotification().setTittle(notificacion.getNbNotificacion());
		//push.getNotification().setImage(base64);
		
		//Agregamos el Mensaje
		push.getData().setBody(notificacion.getTxMensaje());
		//push.getData().setMessage(notificacion.getTxMensaje());
		//push.getNotification().setBody(notificacion.getTxMensaje());
		//push.getNotification().setMessage(notificacion.getTxMensaje());
		
		//Agregamos el Action
		//push.getData().setAction("MessagesFragment");
		push.getData().setPriority("high");
		push.getData().setClickAction("OPEN_MAIN_ACTIVITY");
		push.getData().setFragment("MessagesFragment");
		//push.getNotification().setAction("OPEN_MAIN_ACTIVITY");
		
		try{
			rfbm = clienteFirebaseService.FBMNotificationClient(cliente, push, push.getApiKey());
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			return rfbm != null ? rfbm.getSuccess().equals("1") ? Boolean.FALSE: Boolean.TRUE : Boolean.TRUE;
		}
		
		
	}
}
