package mx.com.teclo.smm.persistencia.hibernate.DAO.notificacion;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.Base.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.DTO.notificacion.NotificacionDTO;
import mx.com.teclo.smm.persistencia.hibernate.DTO.notificacion.TipoNotificacionDTO;

public interface NotificacionDAO extends BaseDAO<NotificacionDTO>{
	public List<NotificacionDTO> listaNotificacionesProgramadas();
	public List<NotificacionDTO> marcarNotificaciones(List<NotificacionDTO> lista);
}
