package mx.com.teclo.smm.persistencia.mybatis.notificacion;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface NotificaDispositivoMyBatisDAO {

	String SQL_NOTIFICA_IDS = "SELECT ID_DISPOSITIVO FROM TMM024D_DP_DISP_NOTIF WHERE ID_NOTIFICACION = #{idNotificacion} AND ST_ACTIVO = 1";
	
	@Select(SQL_NOTIFICA_IDS)
	public List<Long> buscarIdsRelacionadosANotificacion(@Param("idNotificacion")Long idNotificacion);
}
