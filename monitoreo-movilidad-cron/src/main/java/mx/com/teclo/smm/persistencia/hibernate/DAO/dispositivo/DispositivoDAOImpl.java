package mx.com.teclo.smm.persistencia.hibernate.DAO.dispositivo;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.persistencia.hibernate.Base.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.DTO.dispositivo.DispositivoDTO;

@SuppressWarnings("unchecked")
@Repository("dispositivoDAO")
public class DispositivoDAOImpl extends BaseDAOImpl<DispositivoDTO> implements DispositivoDAO{
	
	@Override
	public List<DispositivoDTO> buscarDispositivos(List<Long> ids){
		Criteria query = getCurrentSession().createCriteria(DispositivoDTO.class);
		Disjunction multi = Restrictions.disjunction();
		if(!ids.isEmpty()){
			for(Long id : ids){
				multi.add(Restrictions.eq("idDispositivo", Long.parseLong(id.toString()))); 
			}
			query.add(multi);
		}		
		return (List<DispositivoDTO>) query.list();
	}
}
