package mx.com.teclo.smm.util.enumerable;

import mx.com.teclo.notificacionespush.message.firebase.Data;
import mx.com.teclo.notificacionespush.message.firebase.Notification;
import mx.com.teclo.notificacionespush.message.firebase.Push;

public enum TemplatePushEnum {
	MENSAJE(1L, new Push(null, null, null, null, null, new Data()));
	private Long id;
	private Push push;
	
	private TemplatePushEnum (Long newId, Push newPush){
		this.id = newId;
		this.push = newPush;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Push getPush() {
		return push;
	}

	public void setPush(Push push) {
		this.push = push;
	}
}