package mx.com.teclo.smm.persistencia.hibernate.DAO.notificacion;

import mx.com.teclo.smm.persistencia.hibernate.Base.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.DTO.notificacion.TipoNotificacionDTO;

public interface TipoNotificacionDAO extends BaseDAO<TipoNotificacionDTO>{
	public TipoNotificacionDTO buscarTipoNotificacionPorId(Long id);
}
