package mx.com.teclo.smm.persistencia.hibernate.DAO.notificacion;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.persistencia.hibernate.Base.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.DTO.notificacion.NotificacionDTO;
import mx.com.teclo.smm.persistencia.hibernate.DTO.notificacion.TipoNotificacionDTO;

@SuppressWarnings("unchecked")
@Repository("notificacionDAO")
public class NotificacionDAOImpl extends BaseDAOImpl<NotificacionDTO> implements NotificacionDAO{
	
	@Override
	public List<NotificacionDTO> listaNotificacionesProgramadas(){
		Criteria query = getCurrentSession().createCriteria(NotificacionDTO.class);
		query.add(Restrictions.eq("stActivo",Boolean.TRUE));
		query.add(Restrictions.eq("stEnviado",Boolean.FALSE));
		query.add(Restrictions.sqlRestriction("FH_ENVIO BETWEEN SYSDATE-365 AND SYSDATE"));
		return (List<NotificacionDTO>)query.list();
	}
	
	@Override
	public List<NotificacionDTO> marcarNotificaciones(List<NotificacionDTO> lista){
		for(NotificacionDTO nDTO :lista){
			nDTO.setStEnviado(Boolean.TRUE);
		}
		return lista;
	}
}
