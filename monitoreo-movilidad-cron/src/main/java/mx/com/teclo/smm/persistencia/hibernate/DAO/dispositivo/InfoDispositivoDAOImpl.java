package mx.com.teclo.smm.persistencia.hibernate.DAO.dispositivo;


import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.persistencia.hibernate.Base.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.DTO.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.DTO.dispositivo.InfoDispositivoDTO;

@Repository("infoDispositivoDAO")
public class InfoDispositivoDAOImpl extends BaseDAOImpl<InfoDispositivoDTO> implements InfoDispositivoDAO{

	@Override
	public InfoDispositivoDTO buscarRelacionInformacionDispositivo(DispositivoDTO dDTO){
		Criteria query = getCurrentSession().createCriteria(InfoDispositivoDTO.class);
		query.add(Restrictions.eq("idDispositivo", dDTO));	
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		return (InfoDispositivoDTO) query.uniqueResult();
	}
}
