package mx.com.teclo.smm.persistencia.hibernate.DAO.notificacion;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.persistencia.hibernate.Base.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.DTO.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.DTO.notificacion.NotificaDispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.DTO.notificacion.NotificacionDTO;

@SuppressWarnings("unchecked")
@Repository("notificaDispositivoDAO")
public class NotificaDispositivoDAOImpl extends BaseDAOImpl<NotificaDispositivoDTO> implements NotificaDispositivoDAO{

	@Override
	public List<NotificaDispositivoDTO> buscaRelacionNotificacionDispositivos(List<DispositivoDTO> dDTOs, NotificacionDTO nDTO){
		Criteria query = getCurrentSession().createCriteria(NotificaDispositivoDTO.class);
		Disjunction multi = Restrictions.disjunction();
		if(!dDTOs.isEmpty()){
			for(DispositivoDTO dDTO: dDTOs){
				multi.add(Restrictions.eq("idDispositivo", dDTO));
			}
			query.add(multi);
		}		
		query.add(Restrictions.eq("idNotificacion", nDTO));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		
		return (List<NotificaDispositivoDTO>)query.list();
	}
	
	@Override
	public Boolean validaPushEnviados(NotificacionDTO nDTO){
		Criteria query = getCurrentSession().createCriteria(NotificaDispositivoDTO.class);
		query.add(Restrictions.eq("idNotificacion", nDTO));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		
		List<NotificaDispositivoDTO> lista= (List<NotificaDispositivoDTO>)query.list();
		return lista.isEmpty();
	}
}
