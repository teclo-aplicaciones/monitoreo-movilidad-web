package mx.com.teclo.smm.negocio.service.notificaciones;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.teclo.smm.persistencia.hibernate.DAO.dispositivo.DispoEmpleadoDAO;
import mx.com.teclo.smm.persistencia.hibernate.DAO.dispositivo.DispositivoDAO;
import mx.com.teclo.smm.persistencia.hibernate.DAO.dispositivo.InfoDispositivoDAO;
import mx.com.teclo.smm.persistencia.hibernate.DAO.notificacion.NotificaDispositivoDAO;
import mx.com.teclo.smm.persistencia.hibernate.DAO.notificacion.NotificacionDAO;
import mx.com.teclo.smm.persistencia.hibernate.DAO.notificacion.TipoNotificacionDAO;
import mx.com.teclo.smm.persistencia.hibernate.DTO.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.DTO.dispositivo.InfoDispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.DTO.notificacion.NotificaDispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.DTO.notificacion.NotificacionDTO;
import mx.com.teclo.smm.persistencia.hibernate.DTO.notificacion.TipoNotificacionDTO;
import mx.com.teclo.smm.persistencia.mybatis.notificacion.NotificaDispositivoMyBatisDAO;
import mx.com.teclo.smm.util.enumerable.TipoNotificacionEnum;

@Service
public class NotificacionesCronServiceImpl implements NotificacionesCronService {
	private static final Logger logger = Logger.getLogger(NotificacionesCronServiceImpl.class);

	@Autowired
	NotificacionDAO notificacionDAO;
	
	@Autowired
	DispositivoDAO dispositivoDAO;
	
	@Autowired
	TipoNotificacionDAO tipoNotificacionDAO;
	
	@Autowired
	NotificaDispositivoDAO notificaDispositivoDAO;
	
	@Autowired
	InfoDispositivoDAO infoDispositivoDAO;
	
	@Autowired
	DispoEmpleadoDAO dispoEmpleadoDAO;
	
	@Autowired
	NotificaDispositivoMyBatisDAO notificaDispositivoMyBatisDAO;  
	
	@Autowired
	NotificacionPushService notificacionPushService;
	
	@Autowired
	NotificacionSMSService notificacionSMSService;
	
	@Autowired
	NotificacionMailService notificacionMailService;
	
	@Override
	@Scheduled(cron = "1 * * * * ?")
	@Transactional
	public void validacionNotificaciones() {
		logger.info("Cron cada minuto");
		List<NotificacionDTO> lista = notificacionDAO.listaNotificacionesProgramadas();
		
		if(!lista.isEmpty()){
			logger.info("Encontrados "+lista.size()+" pendiente(s) para ser enviados.");
			
			//Marcarlas para que no entren en el siguiente proceso(ejecutara cada 1 min)
			lista = notificacionDAO.marcarNotificaciones(lista);
			
			//Iterar cada Notificacion
			for(int i = 0; i < lista.size(); i++){
				//Se busca los ids de los dispositivo(s) correspondientes a esta notificacion
				List<Long> listaIds = notificaDispositivoMyBatisDAO.buscarIdsRelacionadosANotificacion(lista.get(i).getIdNotificacion());
				
				List<NotificaDispositivoDTO> listaNotiDispo = notificaDispositivoDAO.buscaRelacionNotificacionDispositivos(dispositivoDAO.buscarDispositivos(listaIds), lista.get(i));
				
				//Determina si se mantiene el estatus en False, en caso de que no se envien a 
				//todos los dispositivos, se regresa a True
				
				//SI LA NOTIFICACION ES TIPO PUSH
				if(lista.get(i).getIdMedioTipoNotifi().getIdMedioTipoNotifi() == TipoNotificacionEnum.PUSH.getIdCat()){
					lista.get(i).setStEnviado(enviarNotificacionPorPush(listaNotiDispo, lista.get(i)));
				}else if(lista.get(i).getIdMedioTipoNotifi().getIdMedioTipoNotifi() == TipoNotificacionEnum.SMS.getIdCat()){
					//lista.get(i).setStActivo(enviarNotificacionPorSMS(listaNotiDispo, lista.get(i)));
					lista.get(i).setStEnviado(Boolean.TRUE);
				}else if(lista.get(i).getIdMedioTipoNotifi().getIdMedioTipoNotifi() == TipoNotificacionEnum.MAIL.getIdCat()){
					lista.get(i).setStEnviado(enviarNotificacionPorMail(listaNotiDispo, lista.get(i)));
				}
				
				if(!lista.get(i).getStEnviado()){
					logger.info("Mensaje sin posibilidad de envio, retener para siguiente envio");
				}else{
					logger.info("Mensaje enviado a todos los destinatarios.");
				}
				
				notificacionDAO.update(lista.get(i));
			}
			
		}else{
			logger.info("No hay notificaciones pendientes a ser enviadas.");
		}
	}
	
	private Boolean enviarNotificacionPorPush(List<NotificaDispositivoDTO> listaDispos, NotificacionDTO nDTO){
		Boolean isPending = true;
		for(NotificaDispositivoDTO dDTO : listaDispos){
			dDTO.setFhEnvio(new Date());
			dDTO.setStActivo(notificacionPushService.enviaNotificacionPushInmediata(dDTO.getIdDispositivo(), nDTO, ""));
			
			//dDTO.setFhEnvio(dDTO.getStActivo() ? null : new Date());
			dDTO.setFhRecepcion(dDTO.getStActivo() ? null : new Date());
			
			dDTO.setIdUsrModifica(0L);
			dDTO.setFhModificacion(new Date());
		}
		
		//Validamos si todos los mensajes se enviaron, de no ser asi, se modifica la notificacion...
		isPending = notificaDispositivoDAO.validaPushEnviados(nDTO);
		
		return isPending;
	}
	
	private Boolean enviarNotificacionPorSMS(List<NotificaDispositivoDTO> listaDispos, NotificacionDTO nDTO){
		Boolean isPending = true;
		for(NotificaDispositivoDTO dDTO : listaDispos){
			dDTO.setFhEnvio(new Date());
			dDTO.setStActivo(notificacionSMSService.enviaNotificacionSMSInmediata(infoDispositivoDAO.buscarRelacionInformacionDispositivo(dDTO.getIdDispositivo()).getNuCelularEmpresa(), nDTO));
				
			//dDTO.setFhEnvio(dDTO.getStActivo() ? null : new Date());
			dDTO.setFhRecepcion(dDTO.getStActivo() ? null : new Date());
			dDTO.setIdUsrModifica(0L);
			dDTO.setFhModificacion(new Date());
		}
		
		//Validamos si todos los mensajes se enviaron, de no ser asi, se modifica la notificacion...
		isPending = notificaDispositivoDAO.validaPushEnviados(nDTO);
		
		return isPending;
	}
	
	private Boolean enviarNotificacionPorMail(List<NotificaDispositivoDTO> listaDispos, NotificacionDTO nDTO){
		Boolean isPending = true;
		for(NotificaDispositivoDTO dDTO : listaDispos){
			dDTO.setFhEnvio(new Date());
			dDTO.setStActivo(notificacionMailService.enviaNotificacioMailInmediata(dispoEmpleadoDAO.buscarRelacionDispositivoEmpleado(dDTO.getIdDispositivo()).getIdEmpleado().getNbMail(), nDTO));
				
			//dDTO.setFhEnvio(dDTO.getStActivo() ? null : new Date());
			dDTO.setFhRecepcion(dDTO.getStActivo() ? null : new Date());
			dDTO.setIdUsrModifica(0L);
			dDTO.setFhModificacion(new Date());
		}
		
		//Validamos si todos los mensajes se enviaron, de no ser asi, se modifica la notificacion...
		isPending = notificaDispositivoDAO.validaPushEnviados(nDTO);
		
		return isPending;
	}
}
