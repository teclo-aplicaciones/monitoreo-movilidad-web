package mx.com.teclo.smm.persistencia.hibernate.DAO.dispositivo;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.Base.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.DTO.dispositivo.DispositivoDTO;

public interface DispositivoDAO extends BaseDAO<DispositivoDTO>{
	public List<DispositivoDTO> buscarDispositivos(List<Long> ids);
}
