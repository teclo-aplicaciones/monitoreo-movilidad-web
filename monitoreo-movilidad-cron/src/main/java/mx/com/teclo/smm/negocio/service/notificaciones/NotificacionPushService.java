package mx.com.teclo.smm.negocio.service.notificaciones;

import mx.com.teclo.smm.persistencia.hibernate.DTO.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.DTO.notificacion.NotificacionDTO;

public interface NotificacionPushService {
	public Boolean enviaNotificacionPushInmediata(DispositivoDTO dispo, NotificacionDTO notificacion, String base64);
}
