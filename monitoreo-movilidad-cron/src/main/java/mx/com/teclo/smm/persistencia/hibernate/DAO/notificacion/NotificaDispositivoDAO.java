package mx.com.teclo.smm.persistencia.hibernate.DAO.notificacion;

import java.util.List;

import mx.com.teclo.smm.persistencia.hibernate.Base.BaseDAO;
import mx.com.teclo.smm.persistencia.hibernate.DTO.dispositivo.DispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.DTO.notificacion.NotificaDispositivoDTO;
import mx.com.teclo.smm.persistencia.hibernate.DTO.notificacion.NotificacionDTO;

public interface NotificaDispositivoDAO extends BaseDAO<NotificaDispositivoDTO>{
	public List<NotificaDispositivoDTO> buscaRelacionNotificacionDispositivos(List<DispositivoDTO> dDTOs, NotificacionDTO nDTO);
	public Boolean validaPushEnviados(NotificacionDTO nDTO);
}
