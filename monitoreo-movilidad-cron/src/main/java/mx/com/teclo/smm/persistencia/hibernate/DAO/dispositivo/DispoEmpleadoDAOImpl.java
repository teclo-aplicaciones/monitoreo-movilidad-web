package mx.com.teclo.smm.persistencia.hibernate.DAO.dispositivo;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.teclo.smm.persistencia.hibernate.Base.BaseDAOImpl;
import mx.com.teclo.smm.persistencia.hibernate.DTO.dispositivo.DispoEmpleadoDTO;
import mx.com.teclo.smm.persistencia.hibernate.DTO.dispositivo.DispositivoDTO;

@Repository("dispoEmpleadoDAO")
public class DispoEmpleadoDAOImpl extends BaseDAOImpl<DispoEmpleadoDTO> implements DispoEmpleadoDAO {

	@Override
	public DispoEmpleadoDTO buscarRelacionDispositivoEmpleado(DispositivoDTO dDTO){
		Criteria query = getCurrentSession().createCriteria(DispoEmpleadoDTO.class);
		query.add(Restrictions.eq("idDispositivo", dDTO));
		query.add(Restrictions.eq("stActivo", Boolean.TRUE));
		
		return (DispoEmpleadoDTO) query.uniqueResult();
	}
}
