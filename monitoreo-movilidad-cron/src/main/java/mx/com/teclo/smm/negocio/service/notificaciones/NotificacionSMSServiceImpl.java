package mx.com.teclo.smm.negocio.service.notificaciones;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import mx.com.teclo.notificaciones.mail.vo.CredencialsNotificationSMS;
import mx.com.teclo.notificaciones.mail.vo.DetailsResponseSMS;
import mx.com.teclo.notificaciones.mail.vo.NotificationSMS;
import mx.com.teclo.notificaciones.sms.altira.ClientSmsService;
import mx.com.teclo.smm.persistencia.hibernate.DTO.notificacion.NotificacionDTO;

@Service
public class NotificacionSMSServiceImpl implements NotificacionSMSService {

	@Autowired
	@Qualifier("restAltiriaService")
	ClientSmsService clientSmsService;
	
	@Override
	@SuppressWarnings("finally")
	public Boolean enviaNotificacionSMSInmediata(String nTelefonico, NotificacionDTO notificacion){
		DetailsResponseSMS statusCreditProvider = null;
		CredencialsNotificationSMS cnSMS= new CredencialsNotificationSMS(" ","","test","fjmb.mx","rn3hq7xj");
		
		NotificationSMS nSMS = new NotificationSMS(notificacion.getTxMensaje(), "52"+nTelefonico); 
		
		try{
			statusCreditProvider = clientSmsService.sendNotificationSms(nSMS, cnSMS);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			return statusCreditProvider != null ? statusCreditProvider.getStatus().trim().equals("000") ? Boolean.FALSE : Boolean.TRUE: Boolean.TRUE; 
		}
	}
}
