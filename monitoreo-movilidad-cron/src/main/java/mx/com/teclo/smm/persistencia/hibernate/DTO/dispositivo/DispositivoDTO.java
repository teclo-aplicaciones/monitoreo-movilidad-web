package mx.com.teclo.smm.persistencia.hibernate.DTO.dispositivo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;

@Entity
@Table(name="TMM011D_DP_DISPOSITIVOS")
public class DispositivoDTO implements Serializable{

	private static final long serialVersionUID = 103356756201398253L;

	@Id
	@Column(name="ID_DISPOSITIVO")
	private Long idDispositivo;
	@Column(name="FB_DISPOSITIVO")
	private String fbDispositivo;
	
	public Long getIdDispositivo() {
		return idDispositivo;
	}
	public void setIdDispositivo(Long idDispositivo) {
		this.idDispositivo = idDispositivo;
	}
	public String getFbDispositivo() {
		return fbDispositivo;
	}
	public void setFbDispositivo(String fbDispositivo) {
		this.fbDispositivo = fbDispositivo;
	}
}
